﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace KTS.KTSSolution.Framework.SharedType
{
	[Serializable]
	public class CommonDataByMsSql : ISerializable
	{
		#region Fields

		public string strQry = "";
		public OdbcParameter[] parameters;
		public CommandType commandType = CommandType.Text;
		
		private OdbcType[] dbTypeList;
		private object[] valueList;
		private ParameterDirection[] directionList;
		private string[] nameList;
		private int[] sizeList;

		#endregion

		#region CommonDataByMsSql(string strQry)
		/// <summary>
		/// CommonDataByMsSql(string strQry)
		/// </summary>
		/// <param name="strQry">string</param>
		public CommonDataByMsSql(string strQry)
		{
			this.strQry = strQry;
		}
		#endregion

		#region CommonDataByMsSql(string strQry, OdbcParameter[] parameters)
		/// <summary>
		/// CommonDataByMsSql(string strQry, OdbcParameter[] parameters)
		/// </summary>
		/// <param name="strQry">string</param>
		/// <param name="parameters">SqlParameter[]</param>
		public CommonDataByMsSql(string strQry, OdbcParameter[] parameters)
		{
			this.strQry = strQry;
			this.parameters = parameters;
		}
		#endregion

		#region CommonDataByMsSql(SerializationInfo si, StreamingContext context)
		/// <summary>
		/// CommonData(SerializationInfo si, StreamingContext context)
		/// </summary>
		/// <param name="si">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected CommonDataByMsSql(SerializationInfo si, StreamingContext context)
		{
			if (si.GetBoolean("bExist"))
			{
				this.dbTypeList = (OdbcType[])si.GetValue("dbTypeList", typeof(OdbcType[]));
				this.valueList = (object[])si.GetValue("valueList", typeof(object[]));
				this.directionList = (ParameterDirection[])si.GetValue("directionList", typeof(ParameterDirection[]));
				this.nameList = (string[])si.GetValue("nameList", typeof(string[]));
				this.sizeList = (int[])si.GetValue("sizeList", typeof(int[]));

				this.parameters = new OdbcParameter[this.dbTypeList.Length];
				for (int i = 0; i < this.dbTypeList.Length; i++)
				{
					this.parameters[i] = new OdbcParameter();
					this.parameters[i].OdbcType = this.dbTypeList[i];
					this.parameters[i].Value = this.valueList[i];
					this.parameters[i].Direction = this.directionList[i];
					this.parameters[i].ParameterName = this.nameList[i];
					this.parameters[i].Size = this.sizeList[i];
				}
			}

			this.strQry = (string)si.GetValue("strQry", typeof(string));
			this.commandType = (CommandType)si.GetValue("commandType", typeof(CommandType));
		}
		#endregion

		#region GetObjectData(SerializationInfo si, StreamingContext context)
		/// <summary>
		/// GetObjectData(SerializationInfo si, StreamingContext context)
		/// </summary>
		/// <param name="si">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		void ISerializable.GetObjectData(SerializationInfo si, StreamingContext context)
		{
			bool bExist = false;
			if (this.parameters != null)
			{
				dbTypeList = new OdbcType[this.parameters.Length];
				valueList = new object[this.parameters.Length];
				directionList = new ParameterDirection[this.parameters.Length];
				nameList = new string[this.parameters.Length];
				sizeList = new int[this.parameters.Length];

				for (int i = 0; i < this.parameters.Length; i++)
				{
					dbTypeList[i] = this.parameters[i].OdbcType;
					valueList[i] = this.parameters[i].Value;
					directionList[i] = this.parameters[i].Direction;
					nameList[i] = this.parameters[i].ParameterName;
					sizeList[i] = this.parameters[i].Size;
				}
				si.AddValue("dbTypeList", dbTypeList);
				si.AddValue("valueList", valueList);
				si.AddValue("directionList", directionList);
				si.AddValue("nameList", nameList);
				si.AddValue("sizeList", sizeList);
				bExist = true;
			}
			si.AddValue("bExist", bExist);
			si.AddValue("strQry", this.strQry);
			si.AddValue("commandType", this.commandType);
		}
		#endregion
	}
}
