﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Reflection;
using System.Diagnostics;

using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.Util;

namespace KTS.KTSSolution.Framework.ExceptionManager
{
    #region Fields

    public enum SubSystemType { KTSSolution = 100 };
    public enum LayerType { GUI = 10, BSL = 20, SQL = 30, DAL = 40, USR = 50, DSL = 60, ETC = 70, SVC = 80, WEB = 90 };

    #endregion

    public class KTSException
    {
		#region Properties

		#region ClsType()
		/// <summary>
		/// ClsType()
		/// </summary>
		/// <param name="aType">Type</param>
		/// <returns>string</returns>
		public class ClsType
		{
			public ClsType() { }
			public string GetMethodName(Type aType)
			{
				MethodInfo[] mInfo = aType.GetMethods();

				for (int i = 0; i < mInfo.Length; i++)
				{
					if (mInfo[i].DeclaringType == aType &&
						!mInfo[i].IsSpecialName &&
						(this.GetType().ToString() != aType.ToString())
						)
					{
						return mInfo[i].Name.ToString();
					}
				}

				return "";
			}
		}
		#endregion

		#endregion

		#region Constructor

		public KTSException()
        {
        }

        #endregion

        #region Method

        #region Exception

        #region HandleDALException(SubSystemType sub, Exception ex, Type ObjType)
        /// <summary>
        /// HandleDALException(SubSystemType sub, Exception ex, Type ObjType)
        /// </summary>
        /// <param name="sub">SubSystemType</param>
        /// <param name="ex">Exception</param>
        /// <param name="ObjType">Type</param>
        /// <returns>Exception</returns>
        public static Exception HandleDALException(SubSystemType sub, Exception ex, Type ObjType)
		{
			if (ex.GetType().FullName.ToString() == "MySql.Data.MySqlClient.MySqlException")
			{
				MySqlException sqlex = (MySqlException)(ex.GetBaseException());
				string bizName = MessageHandler.ReturnFileName(sub);
				string layerName = GetLayerString(LayerType.SQL);
				WriteLogNew(EventLogEntryType.Error, bizName, layerName, ex);

				KTSServerException svrEx = new KTSServerException(bizName, layerName, sqlex.Number.ToString(), sqlex.Message,
					" <- " + ObjType.ToString() + "." + new ClsType().GetMethodName(ObjType), ex.ToString());

				HandleException(sub, LayerType.DAL, "#", ex, ObjType);


			}
			else
			{	
				HandleException(sub, LayerType.DAL, "#", ex, ObjType);
			}
			return null;
		}
		#endregion

		#region HandleInformation(SubSystemType sub, LayerType Layer, string customMessage, int eventId, short category)
		/// <summary>
		/// HandleInformation(SubSystemType sub, LayerType Layer, string customMessage, int eventId, short category)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="Layer">LayerType</param>
		/// <param name="customMessage">string</param>
		/// <param name="eventId">int</param>
		/// <param name="category">short</param>
		public static void HandleInformation(SubSystemType sub, LayerType Layer, string customMessage, int eventId, short category)
		{
			string systemName = MessageHandler.ReturnFileName(sub);
			string layerName = GetLayerString(Layer);
			WriteLog(systemName, layerName, customMessage, eventId, category);
		}
		#endregion

		#region HandleDALExceptionNew(EventLogEntryType entryType, SubSystemType sub, Exception ex, Type ObjType)
		/// <summary>
		/// HandleDALExceptionNew(EventLogEntryType entryType, SubSystemType sub, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="entryType">EventLogEntryType</param>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <returns>Exception</returns>
		public static Exception HandleDALExceptionNew(EventLogEntryType entryType, SubSystemType sub, Exception ex, Type ObjType)
		{
			if (ex.GetType().FullName.ToString() == "MySql.Data.MySqlClient.MySqlException")
			{
				MySqlException sqlex = (MySqlException)(ex.GetBaseException());
				string bizName = MessageHandler.ReturnFileName(sub);
				string layerName = GetLayerString(LayerType.SQL);
				WriteLogNew(entryType, bizName, layerName, ex);

				KTSServerException svrEx = new KTSServerException(bizName, layerName, sqlex.Number.ToString(), sqlex.Message, " <- " + ObjType.ToString() + "." + new ClsType().GetMethodName(ObjType), ex.ToString());

				HandleException(sub, LayerType.DAL, "#", ex, ObjType);
			}
			else
			{   // 새로운 예외를 발생		
				HandleException(sub, LayerType.DAL, "#", ex, ObjType);
			}
			return null;
		}
		#endregion

		#region HandleBSLException(SubSystemType sub, Exception ex, Type ObjType)
		/// <summary>
		/// HandleBSLException(SubSystemType sub, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		public static void HandleBSLException(SubSystemType sub, Exception ex, Type ObjType)
		{
			HandleException(sub, LayerType.BSL, "#", ex, ObjType);
		}
		#endregion

		#region HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, bool bRethrow)
		/// <summary>
		/// HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, bool bRethrow)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <param name="bRethrow">bool</param>
		public static void HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, bool bRethrow)
		{
			HandleException(sub, LayerType.BSL, "#", ex, ObjType, 0, bRethrow);
		}
		#endregion

		#region HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, int ieventId)
		/// <summary>
		/// HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, int ieventId)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <param name="ieventId">int</param>
		public static void HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, int ieventId)
		{
			HandleException(sub, LayerType.BSL, "#", ex, ObjType, ieventId, true);
		}
		#endregion

		#region HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, int ieventId, bool bRethrow)
		/// <summary>
		/// HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, int ieventId, bool bRethrow)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <param name="ieventId">int</param>
		/// <param name="bRethrow">bool</param>
		public static void HandleBSLException(SubSystemType sub, Exception ex, Type ObjType, int ieventId, bool bRethrow)
		{
			HandleException(sub, LayerType.BSL, "#", ex, ObjType, ieventId, bRethrow);
		}
		#endregion

		#region HandleDSLException(SubSystemType sub, Exception ex, Type ObjType)
		/// <summary>
		/// HandleDSLException(SubSystemType sub, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		public static void HandleDSLException(SubSystemType sub, Exception ex, Type ObjType)
		{
			HandleException(sub, LayerType.DSL, "#", ex, ObjType);
		}
		#endregion

		#region HandleDSLException(SubSystemType sub, Exception ex, Type ObjType, string errorCode)
		/// <summary>
		/// HandleDSLException(SubSystemType sub, Exception ex, Type ObjType, string errorCode)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <param name="errorCode">string</param>
		public static void HandleDSLException(SubSystemType sub, Exception ex, Type ObjType, string errorCode)
		{
			HandleException(sub, LayerType.DSL, errorCode, ex, ObjType);
		}
		#endregion

		#region HandleGUIException(SubSystemType sub, Exception ex, Type ObjType)
		/// <summary>
		/// HandleGUIException(SubSystemType sub, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		public static void HandleGUIException(SubSystemType sub, Exception ex, Type ObjType)
		{
			HandleException(sub, LayerType.GUI, "#", ex, ObjType);
		}
		#endregion

		#region HandleWEBException(SubSystemType sub, Exception ex)
		/// <summary>
		/// HandleWEBException(SubSystemType sub, Exception ex)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		public static void HandleWEBException(SubSystemType sub, Exception ex)
		{
			HandleException(sub, LayerType.WEB, "#", ex, null);
		}
		#endregion

		#region HandleUSRException(SubSystemType sub, Exception ex, Type ObjType)
		/// <summary>
		/// HandleUSRException(SubSystemType sub, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		public static void HandleUSRException(SubSystemType sub, Exception ex, Type ObjType)
		{
			HandleException(sub, LayerType.USR, "#", ex, ObjType);
		}
		#endregion

		#region HandleSVCException(SubSystemType sub, Exception ex, Type ObjType)
		/// <summary>
		/// HandleSVCException(SubSystemType sub, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		public static void HandleSVCException(SubSystemType sub, Exception ex, Type ObjType)
		{
			HandleException(sub, LayerType.SVC, "#", ex, ObjType);
		}
		#endregion

		#region HandleSVCException(SubSystemType sub, Exception ex, Type ObjType, bool bRethrow)
		/// <summary>
		/// HandleSVCException(SubSystemType sub, Exception ex, Type ObjType, bool bRethrow)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <param name="bRethrow">bool</param>
		public static void HandleSVCException(SubSystemType sub, Exception ex, Type ObjType, bool bRethrow)
		{
			HandleException(sub, LayerType.SVC, "#", ex, ObjType, 0, bRethrow);
		}
		#endregion

		#region HandleSVCException(SubSystemType sub, Exception ex, Type ObjType, int ieventId)
		/// <summary>
		/// HandleSVCException(SubSystemType sub, Exception ex, Type ObjType, int ieventId)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <param name="ieventId">int</param>
		public static void HandleSVCException(SubSystemType sub, Exception ex, Type ObjType, int ieventId)
		{
			HandleException(sub, LayerType.SVC, "#", ex, ObjType, ieventId, true);
		}
		#endregion

		#region HandleException(SubSystemType sub, LayerType Layer, string Errcode, Exception ex, Type ObjType)
		/// <summary>
		/// HandleException(SubSystemType sub, LayerType Layer, string Errcode, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="Layer">LayerType</param>
		/// <param name="Errcode">string</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		protected static void HandleException(SubSystemType sub, LayerType Layer, string Errcode, Exception ex, Type ObjType)
		{
			HandleException(sub, Layer, Errcode, ex, ObjType, 0, true);
		}
		#endregion

		#region HandleException(SubSystemType sub, LayerType Layer, string Errcode, Exception ex, Type ObjType, int ieventId, bool bRethrow)
		/// <summary>
		/// HandleException(SubSystemType sub, LayerType Layer, string Errcode, Exception ex, Type ObjType, int ieventId, bool bRethrow)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="Layer">LayerType</param>
		/// <param name="Errcode">string</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <param name="ieventId">int</param>
		/// <param name="bRethrow">bool</param>
		protected static void HandleException(SubSystemType sub, LayerType Layer, string Errcode, Exception ex, Type ObjType, int ieventId, bool bRethrow)
		{
			if (ObjType != null)
			{
				if (ex.Message.IndexOf(".KTSException") >= 0 || (ex is KTSServerException))
				{
					string bizName = MessageHandler.ReturnFileName(sub);
					string layerName = GetLayerString(Layer);

					WriteLogNew(EventLogEntryType.Error, bizName, layerName, ex, ieventId);

					MessageHandler.MessageObject oMsg = MessageHandler.ParseErrorMessage(ex.ToString());

					oMsg.Location += " <- " + ObjType.ToString() + "." + new ClsType().GetMethodName(ObjType);

					if (bRethrow)
					{
						throw new Exception(oMsg.ToString());
					}
				}
				else
				{
					string bizName = MessageHandler.ReturnFileName(sub);
					string layerName = GetLayerString(Layer);

					WriteLogNew(EventLogEntryType.Error, bizName, layerName, ex, ieventId);

					KTSServerException nex = new KTSServerException(bizName, layerName, Errcode, ex.Message, " <- " + ObjType.ToString() + "." + new ClsType().GetMethodName(ObjType), ex.ToString());

					if (bRethrow)
					{
						throw new Exception(nex.ToString());
					}
				}
			}
			else
			{
				string bizName = MessageHandler.ReturnFileName(sub);
				string layerName = GetLayerString(Layer);

				WriteLogNew(EventLogEntryType.Error, bizName, layerName, ex, ieventId);

				MessageHandler.MessageObject oMsg = MessageHandler.ParseErrorMessage(ex.ToString());

				if (bRethrow)
				{
					throw new Exception(oMsg.ToString());
				}
			}
		}
		#endregion

		#region ProcessUIException(SubSystemType sub, Exception ex, Type ObjType)
		/// <summary>
		/// ProcessUIException(SubSystemType sub, Exception ex, Type ObjType)
		/// </summary>
		/// <param name="sub">SubSystemType</param>
		/// <param name="ex">Exception</param>
		/// <param name="ObjType">Type</param>
		/// <returns>KTSServerException</returns>
		protected static KTSServerException ProcessUIException(SubSystemType sub, Exception ex, Type ObjType)
		{
			KTSServerException snex = null;

			if (ex.Message.IndexOf(".KTSException") >= 0 || (ex is KTSServerException))
			{
				MessageHandler.MessageObject oMsg = MessageHandler.ParseErrorMessage(ex.ToString());
				oMsg.Location += " <- " + ObjType.ToString() + "." + new ClsType().GetMethodName(ObjType);

				snex = new KTSServerException(oMsg.BizName, oMsg.LayerName, "#", oMsg.Msg, " <- " + ObjType.ToString() + "." + new ClsType().GetMethodName(ObjType), ex.ToString());
			}
			else
			{
				string bizName = MessageHandler.ReturnFileName(sub);
				string layerName = GetLayerString(LayerType.GUI);

				WriteLog(bizName, layerName, ex);

				snex = new KTSServerException(bizName, layerName, "#", ex.Message, " <- " + ObjType.ToString() + "." + new ClsType().GetMethodName(ObjType), ex.ToString());
			}

			return snex;
		}
		#endregion

        #endregion

        #region WriteLog

        #region WriteLog(string systemName, string layerName, string customMessage, int eventId, short category)
        /// <summary>
        /// WriteLog(string systemName, string layerName, string customMessage, int eventId, short category)
        /// </summary>
        /// <param name="SystemName">string</param>
        /// <param name="layerName">string</param>
        /// <param name="customMessage">string</param>
        /// <param name="eventId">int</param>
        /// <param name="category">short</param>
        protected static void WriteLog(string systemName, string layerName, string customMessage, int eventId, short category)
		{
			LogHandler.Publish(systemName, layerName, customMessage, eventId, category);
		}
		#endregion

		#region WriteLog(string SystemName, string Layer, Exception ex)
		/// <summary>
		/// WriteLog(string SystemName, string Layer, Exception ex)
		/// </summary>
		/// <param name="SystemName">string</param>
		/// <param name="Layer">LayerType</param>
		/// <param name="ex">Exception</param>
		protected static void WriteLog(string SystemName, string Layer, Exception ex)
		{
			LogHandler.Publish(SystemName, Layer, ex);
		}
		#endregion

		#region WriteLogNew(EventLogEntryType entryType, string SystemName, string Layer, Exception ex)
		/// <summary>
		/// WriteLogNew(EventLogEntryType entryType, string SystemName, string Layer, Exception ex)
		/// </summary>
		/// <param name="entryType">EventLogEntryType</param>
		/// <param name="SystemName">string</param>
		/// <param name="Layer">LayerType</param>
		/// <param name="ex">Exception</param>
		protected static void WriteLogNew(EventLogEntryType entryType, string SystemName, string Layer, Exception ex)
		{
			LogHandler.PublishNew(entryType, SystemName, Layer, ex);
		}
		#endregion

		#region WriteLogNew(EventLogEntryType entryType, string SystemName, string Layer, Exception ex, int eventId)
		/// <summary>
		/// WriteLogNew(EventLogEntryType entryType, string SystemName, string Layer, Exception ex, int eventId)
		/// </summary>
		/// <param name="entryType">EventLogEntryType</param>
		/// <param name="SystemName">string</param>
		/// <param name="Layer">LayerType</param>
		/// <param name="ex">Exception</param>
		/// <param name="eventId">int</param>
		protected static void WriteLogNew(EventLogEntryType entryType, string SystemName, string Layer, Exception ex, int eventId)
		{
			LogHandler.PublishNew(entryType, SystemName, Layer, ex, eventId);
		}
		#endregion

		#region HandleException(string strSystem, Exception ex)
		/// <summary>
		/// HandleException
		/// </summary>
		/// <param name="strSystem"></param>
		/// <param name="ex"></param>
		public static void HandleException(object target, Exception ex)
		{
			TimeStamp timestamp = new TimeStamp();
			timestamp.TimeStampStart();
			timestamp.TimeStampEnd(target, "=====오류 발생 >> " + ex.ToString());

		}
		#endregion

		#endregion

		#region GetLayerString(LayerType Layer)
		/// <summary>
		/// GetLayerString(LayerType Layer)
		/// </summary>
		/// <param name="Layer">LayerType</param>
		/// <returns>string</returns>
		public static string GetLayerString(LayerType Layer)
		{
			string LayerName = "";

			switch (Layer)
			{
				case (LayerType.BSL):
					LayerName = "BSL"; break;
				case (LayerType.DAL):
					LayerName = "DAL"; break;
				case (LayerType.GUI):
					LayerName = "GUI"; break;
				case (LayerType.SQL):
					LayerName = "SQL"; break;
				case (LayerType.USR):
					LayerName = "USR"; break;
				case (LayerType.DSL):
					LayerName = "DSL"; break;
				case (LayerType.SVC):
					LayerName = "SVC"; break;
				case (LayerType.WEB):
					LayerName = "WEB"; break;
				default:
					LayerName = "ETC";
					break;
			}
			return LayerName;
		}
		#endregion

		#endregion
	}
}
