﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using System.Text;

using MySql.Data.MySqlClient;

namespace KTS.KTSSolution.Framework.Util
{
    public class TimeStamp
	{
        #region Fields

        private DateTime startTime;

        #endregion

        #region Constructor

        public TimeStamp() 
		{ 
		}

        #endregion

        #region Method

        #region TimeStampStart()
        /// <summary>
        /// TimeStampStart()
        /// </summary>
        public void TimeStampStart()
		{
			startTime = DateTime.Now;
		}
		#endregion

		#region TimeStampEnd(object target, string sServiceName)
		/// <summary>
		/// TimeStampEnd(object target, string sServiceName)
		/// </summary>
		/// <param name="target">object</param>
		/// <param name="sServiceName">string</param>
		public void TimeStampEnd(object target, string sServiceName)
		{
			if (target == null)
				return;

			string sLogDir = ConfigurationSettings.AppSettings["TimeLogPath"];

			DateTime endTime = DateTime.Now;

			TimeSpan timeSpan = new TimeSpan(endTime.Ticks - startTime.Ticks);

			string sFullPath = string.Format(@"{0}\{1}-{2}-{3}TimeLog.txt",	sLogDir, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString());

			FileStream fs = null;
			StreamWriter sWriter = null;

			try
			{
				string sLogInfo = startTime.ToString() + " |" + target.GetType().Namespace + " |" + target.GetType().Name + " |" + sServiceName + " |" + timeSpan.TotalMilliseconds.ToString();

				if (!Directory.Exists(sLogDir))
				{ 
					Directory.CreateDirectory(sLogDir); 
				}

				fs = new FileStream(sFullPath, FileMode.Append, FileAccess.Write, FileShare.Write);
				sWriter = new StreamWriter(fs, System.Text.Encoding.Default);

				sWriter.WriteLine(sLogInfo);
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry("TimeStamp", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
			}
			finally
			{
				if (sWriter != null)
					sWriter.Close();
				if (fs != null)
					fs.Close();
			}
		}
		#endregion

		#region TimeStampEnd(object target, string sServiceName, string strSPName, System.Data.SqlClient.SqlParameter[] pack)
		/// <summary>
		/// TimeStampEnd(object target, string sServiceName, string strSPName, System.Data.SqlClient.SqlParameter[] pack)
		/// </summary>
		/// <param name="target">object</param>
		/// <param name="sServiceName">string</param>
		/// <param name="strSPName">string</param>
		/// <param name="pack"></param>
		public void TimeStampEnd(object target, string sServiceName, string strSPName, MySqlParameter[] pack)
		{
			if (target == null || pack == null)
				return;

			string sLogDir = ConfigurationSettings.AppSettings["TimeLogPath"];

			DateTime endTime = DateTime.Now;

			TimeSpan timeSpan = new TimeSpan(endTime.Ticks - startTime.Ticks);

			string sFullPath = string.Format(@"{0}\{1}-{2}-{3}TimeLog.txt", sLogDir, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString());

			FileStream fs = null;
			StreamWriter sWriter = null;

			StringBuilder stbParameters = new StringBuilder();

			try
			{
				foreach (MySqlParameter p in pack)
				{
					if (p.Value != null)
					{
						stbParameters.Append(p.ParameterName + "=" + p.Value.ToString() + ",");
					}
					else
					{
						stbParameters.Append(p.ParameterName + "=" + ",");
					}
				}

				if (stbParameters.Length > 0)
				{
					stbParameters = stbParameters.Remove(stbParameters.Length - 1, 1);
				}

				string sLogInfo = startTime.ToString() + " |" + target.GetType().Namespace + " |" + target.GetType().Name + " |" + sServiceName + " |" + timeSpan.TotalMilliseconds.ToString() + " |" + strSPName + " |" + stbParameters.ToString();

				if (!Directory.Exists(sLogDir))
				{
					Directory.CreateDirectory(sLogDir);
				}

				fs = new FileStream(sFullPath, FileMode.Append, FileAccess.Write, FileShare.Write);
				sWriter = new StreamWriter(fs, System.Text.Encoding.Default);

				sWriter.WriteLine(sLogInfo);

			}
			catch (Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry("TimeStamp", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
			}
			finally
			{
				if (sWriter != null)
					sWriter.Close();
				if (fs != null)
					fs.Close();
			}
		}
		#endregion

		#endregion
	}
}
