﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;
using System.Collections.Generic;

namespace KTS.KTSSolution.BSL.WorkingClothes
{
    public class WorkingClothesMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public WorkingClothesMgmt()
        {
        }

        #endregion

        #region Method 

        #region GetRequestUserClothesList
        /// <summary>
        /// GetRequestUserClothesList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetRequestUserClothesList(string strEmpNo, string strYears, string strPointSeq)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_REQUESTUSERCLOTHLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strPointSeq;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetRequestCnt
        /// <summary>
        /// GetRequestCnt
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetRequestCnt(string strYears, string strPointSeq, string strEmpNo, string strReqType)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_REQUESTCLOTHCOUNT_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pREQTYPE", MySqlDbType.VarChar, 1);
                parameters[3].Value = strReqType;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetAddClothList
        /// <summary>
        /// GetAddClothList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetAddClothList(string strYears, string strPointSeq, string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_ADDCLOTHLLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetRequestAddClothDetailList
        /// <summary>
        /// GetRequestAddClothDetailList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetRequestAddClothDetailList(string strYears, string strPointSeq, string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_REQUESTUSERADDCLOTHDETAILLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetRequestClothDetailList
        /// <summary>
        /// GetRequestClothDetailList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetRequestClothDetailList(string strYears, string strPointSeq, string strEmpNo, string strReqType)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_REQUESTUSERCLOTHDETAILLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pREQTYPE", MySqlDbType.VarChar, 1);
                parameters[3].Value = strReqType;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsRequestUserCloth
        /// <summary>
        /// InsRequestUserCloth
        /// </summary>
        /// <param name="lstData">List<string></param>
        public void InsRequestUserCloth(List<string> lstData, string strEmpNo)
        {
            List<CommonData> DataList = new List<CommonData>();

            foreach (string strData in lstData)
            {
                string[] strValue = strData.Split('|');
                int nLen = strValue.Length;

                if (nLen < 7)
                    continue;

                string strYears = strValue[0];
                string strPointSeq = strValue[1];
                string strDetailSeq = strValue[2];
                string strReqCnt = strValue[3];
                string strSize = strValue[4];
                string strReqType = strValue[5];
                //2023.09.04 추가신청은 승인없이 요청완료처리 미신청일경우 승인요청
                //string strReqStatus = strReqType == "A" ? "W" : "F";
                string strReqStatus = strValue[6];

                MySqlParameter[] parameters = new MySqlParameter[8];

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strPointSeq;

                parameters[3] = new MySqlParameter("@pDETAILSEQ", MySqlDbType.VarChar, 10);
                parameters[3].Value = strDetailSeq;

                parameters[4] = new MySqlParameter("@pREQTYPE", MySqlDbType.VarChar, 1);
                parameters[4].Value = strReqType;

                parameters[5] = new MySqlParameter("@pREQSTATUS", MySqlDbType.VarChar, 2);
                parameters[5].Value = strReqStatus;

                parameters[6] = new MySqlParameter("@pCLOTHSIZE", MySqlDbType.VarChar, 20);
                parameters[6].Value = strSize;

                parameters[7] = new MySqlParameter("@pREQCNT", MySqlDbType.VarChar, 11);
                parameters[7].Value = strReqCnt;

                CommonData data = new CommonData("UP_REQUESTUSERCLOTH_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                DataList.Add(data);
            }

            using (DSBase dsBase = new DSBase())
            {
                dsBase.MultiRegister(DataList);
            }
        }
        #endregion

        #region UdpRequestUserCloth
        /// <summary>
        /// UdpRequestUserCloth
        /// </summary>
        /// <param name="lstData">List<string></param>
        public void UdpRequestUserCloth(List<string> lstData, string strEmpNo)
        {
            List<CommonData> DataList = new List<CommonData>();

            foreach (string strData in lstData)
            {
                string[] strValue = strData.Split('|');
                int nLen = strValue.Length;

                if (nLen < 8)
                    continue;

                string strYears = strValue[0];
                string strPointSeq = strValue[1];
                string strDetailSeq = strValue[2];
                string strReqCnt = strValue[3];
                string strSize = strValue[4];
                string strReqType = strValue[5];
                //2023.09.04 추가신청은 승인없이 요청완료처리 미신청일경우 승인요청
                //string strReqStatus = strReqType == "A" ? "W" : "F";
                string strReqStatus = strValue[6];
                string strRequestId = strValue[7];

                MySqlParameter[] parameters = new MySqlParameter[9];

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strPointSeq;

                parameters[3] = new MySqlParameter("@pDETAILSEQ", MySqlDbType.VarChar, 10);
                parameters[3].Value = strDetailSeq;

                parameters[4] = new MySqlParameter("@pREQTYPE", MySqlDbType.VarChar, 1);
                parameters[4].Value = strReqType;

                parameters[5] = new MySqlParameter("@pCLOTHSIZE", MySqlDbType.VarChar, 20);
                parameters[5].Value = strSize;

                parameters[6] = new MySqlParameter("@pREQCNT", MySqlDbType.VarChar, 11);
                parameters[6].Value = strReqCnt;

                parameters[7] = new MySqlParameter("@pREQSTATUS", MySqlDbType.VarChar, 2);
                parameters[7].Value = strReqStatus;

                parameters[8] = new MySqlParameter("@pREQUESTID", MySqlDbType.VarChar, 20);
                parameters[8].Value = strRequestId;


                CommonData data = new CommonData("UP_REQUESTUSERCLOTH_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                DataList.Add(data);
            }

            using (DSBase dsBase = new DSBase())
            {
                dsBase.MultiRegister(DataList);
            }
        }
        #endregion

        #region InsRequestUserCloth
        /// <summary>
        /// InsRequestUserCloth
        /// </summary>
        /// <param name="lstData">List<string></param>
        public void DelRequestUserCloth(string strEmpNo, string strYears, string strPointSeq, string strReqType)
        {

            MySqlParameter[] parameters = new MySqlParameter[4];

            parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
            parameters[0].Value = strEmpNo;

            parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
            parameters[1].Value = strYears;

            parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
            parameters[2].Value = strPointSeq;

            parameters[3] = new MySqlParameter("@pREQTYPE", MySqlDbType.VarChar, 1);
            parameters[3].Value = strReqType;

            CommonData data = new CommonData("UP_REQUESTUSERCLOTH_DELETE", parameters);
            data.commandType = CommandType.StoredProcedure;

            using (DSBase dsBase = new DSBase())
            {
                dsBase.Inquiry(data);
            }
        }
        #endregion

        #region GetRequestOperClothesList
        /// <summary>
        /// GetRequestOperClothesList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetRequestOperClothesList(string strOrgCd, string strEmpNo, string strEmpNm, string strYears, string strPointSeq, string strReqType)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_REQUESTOPERCLOTHLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strEmpNo;

                parameters[2] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strEmpNm;

                parameters[3] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[3].Value = strYears;

                parameters[4] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[4].Value = strPointSeq;

                parameters[5] = new MySqlParameter("@pREQTYPE", MySqlDbType.VarChar, 1);
                parameters[5].Value = strReqType;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region UpdRequestClothPermission
        /// <summary>
        /// UpdRequestClothPermission
        /// </summary>
        /// <param name="lstData">List<string></param>
        public void UpdRequestClothPermission(List<string> lstData)
        {
            List<CommonData> DataList = new List<CommonData>();

            foreach (string strData in lstData)
            {
                string[] strValue = strData.Split('|');
                int nLen = strValue.Length;

                if (nLen < 5)
                    continue;

                string strRequestId = strValue[0];
                string strYears = strValue[1];
                string strPointSeq = strValue[2];
                string strDetailSeq = strValue[3];
                string strEmpNo = strValue[4];

                MySqlParameter[] parameters = new MySqlParameter[5];

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strPointSeq;

                parameters[3] = new MySqlParameter("@pDETAILSEQ", MySqlDbType.VarChar, 10);
                parameters[3].Value = strDetailSeq;

                parameters[4] = new MySqlParameter("@pREQUESTID", MySqlDbType.VarChar, 20);
                parameters[4].Value = strRequestId;

                CommonData data = new CommonData("UP_REQUESTCLOTHPERMISSION_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                DataList.Add(data);
            }

            using (DSBase dsBase = new DSBase())
            {
                dsBase.MultiRegister(DataList);
            }
        }
        #endregion

        #region UpdRequestClothPermissionGroup
        /// <summary>
        /// UpdRequestClothPermissionGroup
        /// </summary>
        /// <param name="lstData">List<string></param>
        public void UpdRequestClothPermissionGroup(List<string> lstData)
        {
            List<CommonData> DataList = new List<CommonData>();

            foreach (string strData in lstData)
            {
                string[] strValue = strData.Split('|');
                int nLen = strValue.Length;

                if (nLen < 3)
                    continue;

                string strOrgCd = strValue[0];
                string strYears = strValue[1];
                string strPointSeq = strValue[2];

                MySqlParameter[] parameters = new MySqlParameter[3];

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strPointSeq;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 6);
                parameters[0].Value = strOrgCd;

                CommonData data = new CommonData("UP_REQUESTCLOTHPERMISSION_GROUP_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                DataList.Add(data);
            }

            using (DSBase dsBase = new DSBase())
            {
                dsBase.MultiRegister(DataList);
            }
        }
        #endregion

        #region GetRequestOperClothStat
        /// <summary>
        /// GetRequestOperClothStat
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetRequestOperClothStat(string strOrgCd, string strYears, string strPointSeq)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_REQUESTOPERCLOTHSTAT_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strPointSeq;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region UpdRequestClothStatPermission
        /// <summary>
        /// UpdRequestClothStatPermission
        /// </summary>
        /// <param name="lstData">List<string></param>
        public void UpdRequestClothStatPermission(List<string> lstData)
        {
            List<CommonData> DataList = new List<CommonData>();

            foreach (string strData in lstData)
            {
                string[] strValue = strData.Split('|');
                int nLen = strValue.Length;

                if (nLen < 3)
                    continue;

                string strOrgCd = strValue[0];
                string strYears = strValue[1];
                string strPointSeq = strValue[2];

                MySqlParameter[] parameters = new MySqlParameter[3];

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 6);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                parameters[2] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strPointSeq;

                CommonData data = new CommonData("UP_REQUESTCLOTHSTATPERMISSION_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                DataList.Add(data);
            }

            using (DSBase dsBase = new DSBase())
            {
                dsBase.MultiRegister(DataList);
            }
        }
        #endregion

        #region GetWorkingClothPointInfo
        /// <summary>
        /// GetWorkingClothPointInfo
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetWorkingClothPointInfo(string strOrgCd, string strYears)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_WORKINGCLOTHPOINTINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[1].Value = strYears;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region UdpWorkingClothSize
        /// <summary>
        /// UdpWorkingClothSize
        /// </summary>
        /// <returns>DataSet</returns>
        public void UdpWorkingClothSize(string strRequestId, string strYears, string strPointSeq, string strDetailSeq, string strEmpNo, string strSize)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_WORKINGCLOTHSIZE_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                parameters[2] = new MySqlParameter("@pDETAILSEQ", MySqlDbType.VarChar, 10);
                parameters[2].Value = strDetailSeq;

                parameters[3] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[3].Value = strEmpNo;

                parameters[4] = new MySqlParameter("@pCLOTHSIZE", MySqlDbType.VarChar, 10);
                parameters[4].Value = strSize;

                parameters[5] = new MySqlParameter("@pREQUESTID", MySqlDbType.VarChar, 20);
                parameters[5].Value = strRequestId;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion

    }
}
