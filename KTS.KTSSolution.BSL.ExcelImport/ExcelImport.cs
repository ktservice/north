﻿using System;
using System.Linq;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Util;
using KTS.KTSSolution.Framework.SharedType;
using System.Collections.Generic;
using System.Collections;

namespace KTS.KTSSolution.BSL.ExcelImport
{
    public class ExcelImport : IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public ExcelImport()
        {
        }

        #endregion

        #region Method

        #region
        private DataSet GetExcelUploadTarget()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_EXCELUPLOADTARGET_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion 

        #region
        private DataSet GetExcelUploadResultTarget(string pUpTable)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_EXCELUPLOADRESULT_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pGUBUN", MySqlDbType.VarChar, 10);
                parameters[0].Value = "TARGET";

                parameters[1] = new MySqlParameter("@pUPLOAD_MON", MySqlDbType.VarChar, 8);
                parameters[1].Value = "";

                parameters[2] = new MySqlParameter("@pUPTABLE", MySqlDbType.VarChar, 200);
                parameters[2].Value = pUpTable;

                parameters[3] = new MySqlParameter("@pRESULTYN", MySqlDbType.VarChar, 1);
                parameters[3].Value = "";

                parameters[4] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 1);
                parameters[4].Value = "";

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion 

        #region XlsxImport
        /// <summary>
        /// XlsxImport
        /// </summary>
        /// <returns>bool</returns>
        public bool XlsxImport()
        {
            bool bResult = false;
            TimeStamp timestamp = new TimeStamp();
            DataSet ds = new DataSet();

            try
            {
                timestamp.TimeStampStart();

                string strDir = ConfigurationSettings.AppSettings["XlsxUploadPath"];
                string strCsvTemp = ConfigurationSettings.AppSettings["CsvPath"];

                DirectoryInfo dirinfo = new DirectoryInfo(strDir);

                if (!dirinfo.Exists)
                {
                    bResult = false;
                    timestamp.TimeStampEnd(this, ">> 엑셀 업로드 폴더 없음.");
                }
                else
                {
                    ds = GetExcelUploadTarget();

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string strUploadName = dr["UPLOADNAME"].ToString();
                            string strTargetTable = dr["TARGETTABLE"].ToString();
                            int nStartRow = int.Parse(dr["STARTROW"].ToString());
                            int nColCnt = int.Parse(dr["COLCNT"].ToString());
                            string strDelYn = dr["DELYN"].ToString();
                            string strDelTargetColNm = dr["DELTARGET"].ToString();

                            if (strDelYn.Equals("Y"))
                            {
                                if (strDelTargetColNm.Length == 0)
                                {
                                    {
                                        timestamp.TimeStampEnd(this, "=====오류 발생 >> " + strUploadName + " : 삭제 타겟 컬럼 미지정");

                                        InsExcelUploadHist(strUploadName, "N", strUploadName + " : 엑셀 양식 오류(컬럼수)");
                                        //컬럼수 오류 저장필요
                                        continue;
                                    }
                                }
                            }

                            string strUploadPath = dr["UPLOADPATH"].ToString().Replace("|", "\\");
                            string strDirExcel = strDir + "\\" + strUploadPath;

                            DirectoryInfo dirExcelinfo = new DirectoryInfo(strDirExcel);

                            if (!dirExcelinfo.Exists)
                            {
                                continue;
                            }

                            FileInfo[] xlFiles = dirExcelinfo.GetFiles("*.xlsx", SearchOption.TopDirectoryOnly);
                            
                            foreach (FileInfo file in xlFiles)
                            {

                                bool bValChk = true;
                                int nRow = 0;
                                int nCol = 0;

                                try
                                {
                                    using (ExcelPackage excelPackage = new ExcelPackage(file))
                                    {
                                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                        int endRowNum = worksheet.Dimension.End.Row;

                                        if (nColCnt != worksheet.Dimension.End.Column)
                                        {
                                            timestamp.TimeStampEnd(this, "=====오류 발생 >> " + file.FullName + " : 엑셀 양식 오류(컬럼수)");

                                            InsExcelUploadHist(strUploadName, "N", strUploadName + " : 엑셀 양식 오류(컬럼수)");
                                            //컬럼수 오류 저장필요
                                            continue;
                                        }

                                        string rowVal = "";

                                        int nQuery = (endRowNum / 1000) + 1;
                                        int nVal = 0;

                                        string[] strQuerys = new string[nQuery];

                                        for (int rowNumber = nStartRow; rowNumber <= endRowNum; rowNumber++)
                                        {
                                            nRow = rowNumber;
                                            nCol = 0;

                                            var row = worksheet.Cells[rowNumber, 1, rowNumber, worksheet.Dimension.End.Column];

                                            string strRowVal = "";

                                            //foreach (ExcelRangeBase cell in row)
                                            for (int i = 0; i < row.Columns; i++)
                                            {
                                                nCol += 1;
                                                string strVal = ((object[,])row.Value)[0, i] == null ? "0" : ((object[,])row.Value)[0, i].ToString().Trim();

                                                if (strVal.Equals("-"))
                                                    strVal = strVal.Replace("-", "0");

                                                //이부분에서 데이터 검증 가능
                                                DateTime DateChk;
                                                float fVal = 0;

                                                if (float.TryParse(strVal, out fVal))
                                                {
                                                    //숫자 체크
                                                    strRowVal += "'" + strVal + "',";
                                                }
                                                else if (DateTime.TryParse(strVal, out DateChk))
                                                {
                                                    if (DateChk.ToString("HH:mm:ss").Equals(strVal))
                                                    {
                                                        //시간 일 경우는 텍스트로 저장
                                                        strRowVal += "'" + strVal + "',";
                                                    }
                                                    else
                                                    {
                                                        //날짜체크
                                                        strRowVal += "'" + DateChk.ToString("yyyy-MM-dd") + "',";
                                                    }
                                                }
                                                else
                                                {
                                                    strVal = strVal.Replace("'", "´"); //' 입력 시 오류해결
                                                    strRowVal += "'" + strVal + "',";
                                                }

                                                if (i == 0)
                                                {
                                                    strRowVal = "'" + rowNumber.ToString() + "'," + strRowVal;
                                                }
                                            }

                                            strRowVal = strRowVal.Substring(0, strRowVal.Length - 1);

                                            if (strRowVal.Replace(",", "").Length == 0)
                                            {
                                                continue;
                                            }

                                            if (rowVal.Length > 0)
                                                rowVal += ",";

                                            rowVal += "(" + strRowVal + ")";

                                            if (((rowNumber - (nStartRow - 1)) % 1000) == 0)
                                            {
                                                strQuerys[nVal] = rowVal;

                                                rowVal = "";
                                                nVal += 1;
                                            }

                                            if (rowNumber == endRowNum)
                                            {
                                                strQuerys[nVal] = rowVal;
                                            }
                                        }

                                        List<CommonData> DataList = new List<CommonData>();

                                        CommonData data = null;

                                        if (strDelYn.Equals("Y"))
                                        {
                                            //대상테이블 중복삭제
                                            string strDelete = " DELETE FROM " + strTargetTable.Replace("_temp", "") + " WHERE " + strDelTargetColNm + " = '" + DateTime.Now.AddMonths(-1).ToString("yyyyMM") + "'";

                                            data = new CommonData(strDelete);
                                            data.commandType = CommandType.Text;

                                            using (DSBase dsBase = new DSBase())
                                            {
                                                dsBase.Inquiry(data);
                                            }
                                        }

                                        string strQuery = "DELETE FROM " + strTargetTable + " WHERE 1 = 1 ; ";
                                        string strInsert = " INSERT INTO " + strTargetTable + " VALUES ";

                                        foreach (string strInsQuery in strQuerys)
                                        {
                                            //임시테이블 데이터 삭제
                                            data = new CommonData(strQuery);
                                            data.commandType = CommandType.Text;

                                            DataList.Add(data);

                                            data = new CommonData(strInsert + strInsQuery);
                                            data.commandType = CommandType.Text;

                                            DataList.Add(data);
                                        }

                                        using (DSBase dsBase = new DSBase())
                                        {
                                            dsBase.MultiRegister(DataList, true);
                                        }

                                        InsExcelUploadHist(strUploadName, "Y", "업로드 성공");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    timestamp.TimeStampEnd(this, "=====오류 발생 >> " + ex.ToString());

                                    string strFailMsg = "";

                                    //업로드 에러 로그 DB저장 추가
                                    if (!bValChk)
                                    {
                                        //유효성오류체크
                                        //nRow, nCol 오류 위치
                                        strFailMsg = nRow.ToString() + " 줄 " + nCol + " 컬럼 데이터 이상.";
                                    }
                                    else
                                    {
                                        strFailMsg = strUploadName + " 업로드 실패";
                                    }

                                    InsExcelUploadHist(strUploadName, "N", strFailMsg);

                                    continue;
                                }
                            }

                            //작업이 끝난 파일 삭제
                            foreach (FileInfo file in xlFiles)
                            {
                                if (file.Exists)
                                {
                                    file.Delete();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                timestamp = new TimeStamp();
                timestamp.TimeStampStart();
                timestamp.TimeStampEnd(this, "=====오류 발생 >> " + ex.ToString());


                //string strFailMsg = "시스템오류 : " + ex.Message.ToString();
                InsExcelUploadHist("엑셀 업로드 시스템 오류", "N", "");
            }            

            return bResult;
        }
        #endregion

        #region ExcelUpLoadResult


        /// <summary>
        /// ExcelUpLoadResult
        /// </summary>
        /// <param name="strTableNm"></param>
        /// <param name="worksheet"></param>
        /// <param name="empno"></param>
        /// <param name="regacyYn"></param>
        /// <param name="strDelVal"></param>
        /// <returns></returns>
        public string ExcelUpLoadResult(string strTableNm, ExcelWorksheet worksheet, string empno = "-1", string regacyYn = "N", string strDelVal = "")
        {
            string strUploadName = string.Empty;
            string strTargetTable = string.Empty;
            string strDelYn = string.Empty;
            string strDelTargetColNm = string.Empty;
            string strErrMsg = string.Empty;
            string uploadKey = string.Empty;
            string strErrRow = "0";
            int nStartRow = 0;
            int nColCnt = 0;
            int nAddRowCnt = 0;

            DateTime now = DateTime.Now;
            uploadKey = now.ToString("yyyyMMddHHmmss");

            DataSet ds = new DataSet();
            ds = GetExcelUploadResultTarget(strTableNm);

            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    strUploadName = dr["UPLOADNAME"].ToString();
                    strTargetTable = dr["TARGETTABLE"].ToString();
                    nStartRow = int.Parse(dr["STARTROW"].ToString());
                    nColCnt = int.Parse(dr["COLCNT"].ToString());
                    strDelYn = dr["DELYN"].ToString();
                    strDelTargetColNm = dr["DELTARGET"].ToString();
                }
            }

            if(nColCnt > 0)
            {
                try
                {
                    int endRowNum = worksheet.Dimension.End.Row;
                    int endColNum = worksheet.Dimension.End.Column;

                    List<string> lstAdd = new List<string>();
                    List<string> lstRow = new List<string>();

                    if (endRowNum >= nStartRow)
                    {
                        for (int rowNumber = nStartRow; rowNumber <= endRowNum; rowNumber++)
                        {
                            string rowVal = string.Empty;
                            string strValChk = string.Empty;

                            for (int colNumber = 1; colNumber <= endColNum; colNumber++)
                            {
                                string val = string.Empty;

                                val = worksheet.Cells[rowNumber, colNumber].Value == null ? "" : worksheet.Cells[rowNumber, colNumber].Value.ToString();

                                //이부분에서 데이터 검증 가능
                                DateTime DateChk;
                                float fVal = 0;

                                if (float.TryParse(val, out fVal)) //숫자 체크
                                {
                                    val = val.Replace("'", "´"); //' 입력 시 오류해결
                                }
                                else if (DateTime.TryParse(val, out DateChk)) //날짜 체크
                                {
                                    if (!DateChk.ToString("HH:mm:ss").Equals(val))
                                    {
                                        val = DateChk.ToString("yyyy-MM-dd");
                                    }
                                    else
                                    {
                                        val = val.Replace("'", "´"); //' 입력 시 오류해결
                                    }
                                }
                                else
                                {
                                    val = val.Replace("'", "´"); //' 입력 시 오류해결
                                }

                                rowVal += "'" + val + "',";
                                strValChk += val;

                            }

                            if (strValChk.Length == 0)
                            {
                                continue;
                            }
                            else
                            {
                                if(regacyYn.Equals("Y"))
                                {
                                    rowVal = string.Format("( '{0}', {1} )", rowNumber.ToString(), rowVal.Substring(0, rowVal.Length - 1));
                                }
                                else
                                {
                                    rowVal = string.Format("( '{0}','{1}','{2}','{3}',{4} )", rowNumber.ToString(), uploadKey, empno, rowNumber.ToString(), rowVal.Substring(0, rowVal.Length - 1));
                                }

                                lstAdd.Add(rowVal);
                                lstRow.Add(rowNumber.ToString());

                                nAddRowCnt++;
                            }
                        }

                        string strDelQuery = string.Empty;

                        if (strDelVal.Length > 0 && strDelTargetColNm.Length > 0 && strDelYn.Equals("Y"))
                        {
                            strDelQuery = "DELETE FROM " + strTableNm + " WHERE " + strDelTargetColNm + " = '" + strDelVal + "'; ";
                        }
                        else
                        {
                            strDelQuery = "DELETE FROM " + strTableNm + " WHERE 1 = 1;";
                        }

                        CommonData dataDel = new CommonData(strDelQuery);
                        dataDel.commandType = CommandType.Text;

                        //temp 삭제
                        using (DSBase dsBase = new DSBase())
                        {
                            dsBase.Register(dataDel);
                        }

                        string strInsert = string.Format(" INSERT INTO {0} VALUES ", strTableNm);

                        using (DSBase dsBase = new DSBase())
                        {
                            for (int i = 0; i < lstAdd.Count; i++)
                            {
                                strErrRow = lstRow[i];
                                dsBase.ExecRegister(strInsert + lstAdd[i]);
                            }
                        }

                        InsExcelUploadHist(strUploadName, "Y", "업로드 성공");
                        InsExcelUploadResult("I", strUploadName, uploadKey, strErrRow, "Y", nAddRowCnt.ToString(), "업로드 성공", empno);
                    }
                }
                catch (Exception ex)
                {
                    strErrMsg = string.Format("업로드 실패(엑셀 행[{0}]) : {1}", strErrRow, ex.Message.ToString());
                    InsExcelUploadHist(strUploadName, "N", strErrMsg);

                    strErrMsg = string.Format("업로드 실패 : 엑셀 행({0}) \n {1}", strErrRow, ex.Message.ToString());
                    InsExcelUploadResult("I", strUploadName, uploadKey, strErrRow, "N", nAddRowCnt.ToString(), strErrMsg, empno);

                    strErrMsg = string.Format("업로드 실패 : 엑셀 행({0})", strErrRow); //return msg용
                }
            }
            else
            {
                strErrMsg = "업로드 파일 양식이 잘못되었습니다.";
            }

            return strErrMsg;
        }
        #endregion

        #region ExcelUpLoad
        /// <summary>
        /// ExcelUpLoad
        /// </summary>
        /// <param name="stream">Stream</param>
        public void ExcelUpLoad(string strUploadName, ExcelWorksheet worksheet, string strTableNm, string strDelCol = "", string strDelVal = "")
        {
            try
            {

                int endRowNum = worksheet.Dimension.End.Row;
                int endColNum = worksheet.Dimension.End.Column;

                StringBuilder sb = new StringBuilder();

                sb.Append("INSERT INTO " + strTableNm + " VALUES ");

                if (endRowNum >= 2)
                {
                    //1 = header 
                    //2 >= data
                    for (int rowNumber = 2; rowNumber <= endRowNum; rowNumber++)
                    {
                        string rowVal = "";
                        string strValChk = "";

                        var row = worksheet.Cells[rowNumber, 1, rowNumber, worksheet.Dimension.End.Column];

                        for (int colNumber = 1; colNumber <= endColNum; colNumber++)
                        {
                            string Val = worksheet.Cells[rowNumber, colNumber].Value == null ? "" : worksheet.Cells[rowNumber, colNumber].Value.ToString();
                            //이부분에서 데이터 검증 가능
                            rowVal += "'" + Val + "',";
                            strValChk += Val;
                        }

                        if (strValChk.Length == 0)
                        {
                            continue;
                        }
                        else
                        {
                            rowVal = "(" + rowVal.Substring(0, rowVal.Length - 1) + ") ";

                            if (rowNumber < endRowNum)
                            {
                                rowVal += ",";
                            }
                        }

                        sb.Append(rowVal);
                    }

                    try
                    {
                        string strQuery = sb.ToString();

                        if (strQuery.Substring(strQuery.Length - 1).Equals(","))
                            strQuery = strQuery.Replace("\r\n", "").Substring(0, strQuery.Length - 2);

                        string strDelQuery = "";

                        if (strDelVal.Length > 0 && strDelCol.Length > 0)
                        {
                            strDelQuery = "DELETE FROM " + strTableNm + " WHERE " + strDelCol + " = '" + strDelVal + "'; ";
                        }
                        else
                        {
                            strDelQuery = "DELETE FROM " + strTableNm + " WHERE 1 = 1;";
                        }

                        CommonData dataDel = new CommonData("DELETE FROM " + strTableNm + " WHERE 1 = 1;");
                        dataDel.commandType = CommandType.Text;

                        CommonData data = new CommonData(strQuery);
                        data.commandType = CommandType.Text;

                        using (DSBase dsBase = new DSBase())
                        {
                            dsBase.Register(dataDel);

                            dsBase.Register(data);
                        }

                        InsExcelUploadHist(strUploadName, "Y", "업로드 성공");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                string strFailMsg = "업로드 실패 : " + ex.Message.ToString();
                InsExcelUploadHist(strUploadName, "N", strFailMsg);
            }
        }
        #endregion

        #region
        private void InsExcelUploadResult(string pGubun, string pUploadNm, string pUploadKey, string pUploadRow, string pResultYn, string pTotCnt, string pMessage, string pEmpno)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[8];

                CommonData data = new CommonData("UP_EXCELUPLOADRESULT_TRANSACTION", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pGUBUN", MySqlDbType.VarChar, 1);
                parameters[0].Value = pGubun;

                parameters[1] = new MySqlParameter("@pUPLOADNM", MySqlDbType.VarChar, 200);
                parameters[1].Value = pUploadNm;

                parameters[2] = new MySqlParameter("@pUPLOAD_KEY", MySqlDbType.VarChar, 20);
                parameters[2].Value = pUploadKey;

                parameters[3] = new MySqlParameter("@pUPLOAD_ROW", MySqlDbType.VarChar, 20);
                parameters[3].Value = pUploadRow;

                parameters[4] = new MySqlParameter("@pRESULTYN", MySqlDbType.VarChar, 1);
                parameters[4].Value = pResultYn;

                parameters[5] = new MySqlParameter("@pTOT_CNT", MySqlDbType.VarChar, 12);
                parameters[5].Value = pTotCnt;
                
                parameters[6] = new MySqlParameter("@pMESSAGE", MySqlDbType.VarChar, 8000);
                parameters[6].Value = pMessage;

                parameters[7] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[7].Value = pEmpno;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region
        private void InsExcelUploadHist(string strUploadName, string strResultYn, string strMessage)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_EXCELUPLOADHIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pUPLOADNAME", MySqlDbType.VarChar, 200);
                parameters[0].Value = strUploadName;

                parameters[1] = new MySqlParameter("@pRESULTYN", MySqlDbType.VarChar, 1);
                parameters[1].Value = strResultYn;

                parameters[2] = new MySqlParameter("@pMESSAGE", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strMessage;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
