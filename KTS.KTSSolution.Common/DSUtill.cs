﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using System;
using System.Data;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace KTS.KTSSolution.Common.DAL
{
    public class DSUtill
    {
        #region Constructor

        DSUtill()
        {
        }

        #endregion

        #region Method

        #region GetDataTable(MySqlDataReader reader)
        /// <summary>
        /// GetDataTable(MySqlDataReader reader)
        /// </summary>
        /// <param name="reader">MySqlDataReader</param>
        /// <returns>DataTable</returns>
        public static DataTable GetDataTable(MySqlDataReader reader)
        {
            DataTable schemaTable = reader.GetSchemaTable();
            DataTable dt = new DataTable();

            for (int i = 0; i < schemaTable.Rows.Count; i++)
            {
                DataRow row = schemaTable.Rows[i];
                string colNm = row["ColumnName"].ToString();

                DataColumn col = new DataColumn(colNm, (Type)row["DataType"]);
                dt.Columns.Add(col);
            }

            while (reader.Read())
            {
                DataRow dr = dt.NewRow();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    dr[i] = reader.GetValue(i);
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        #endregion

        #region GetDataSet(MySqlDataReader reader)
        /// <summary>
        /// GetDataSet(MySqlDataReader reader)
        /// </summary>
        /// <param name="reader">MySqlDataReader</param>
        /// <returns>GetDataSet</returns>
        public static DataSet GetDataSet(MySqlDataReader reader)
        {
            DataSet ds = new DataSet();

            ds.Tables.Add(GetDataTable(reader));
            while (reader.NextResult())
            {
                ds.Tables.Add(GetDataTable(reader));
            }

            return ds;
        }
        #endregion

        #region DbStringReplace(string str)
        /// <summary>
        /// DB 파라미터 특수 문자 치환
        /// </summary>
        /// <param name="str">파라미터값</param>
        /// <returns>치환 된 문자 반환</returns>
        public static string DbStringReplace(string str)
        {
            if (str == null)
            {
                return "";
            }
            else
            {
                return str.Replace("'", "′").Replace(",", "¸").Replace("{", "｛").Replace("}", "｝").Replace("\\", "");
            }
        }

        #endregion

        #endregion
    }
}
