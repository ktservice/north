﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Web.UI.WebControls;

namespace KTSSolutionWeb
{
    public partial class InspectionInfoStsBybComp : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPageInit();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetPageInit()
        {
            try
            {
                SetDDLLv1Grp();

                this.txbStDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");

                string strMonth = this.txbEnDt.Text.Replace("-", "").Substring(0, 6);
                
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLLv1Grp()
        {
            DataSet ds = null;

            try
            {
                ddlLv1Grp.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "INSPECTIONLV1GRPLIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    ddlLv1Grp.Items.Add(new ListItem("전체", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["LV1GRPNM"].ToString().Equals(""))
                        {
                            continue;
                        }

                        ddlLv1Grp.Items.Add(new ListItem(ds.Tables[0].Rows[i]["LV1GRPNM"].ToString(), ds.Tables[0].Rows[i]["LV1GRPCD"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("대분류를 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLv2GrpList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONLV2GRPLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLv3GrpList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONLV3GRPLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONINFOLISTBYBCOMP";
            string jobGb = "GET";

            string strAuthID = HttpContext.Current.Session["AUTHID"].ToString();
            string strEmpNo = string.Empty;

            if (HttpContext.Current.Session["AUTHID"].Equals("AUTH0001") || HttpContext.Current.Session["AUTHID"].Equals("AUTH0002") || HttpContext.Current.Session["AUTHID"].Equals("AUTH0003")
                || HttpContext.Current.Session["AUTHID"].Equals("AUTH0004") || HttpContext.Current.Session["AUTHID"].Equals("AUTH0005"))
            {
                strEmpNo = "";
            }
            else
            {
                strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();
            }

            //aData[0] = StDate;
            //aData[1] = EnDate;
            //aData[2] = ActCd;
            //aData[3] = GrpCd_Lv1;
            //aData[4] = GrpCd_Lv2;
            //aData[5] = GrpCd_Lv3;
            //aData[6] = SearchType;

            aData.Add(strEmpNo); //파라미터 추가 시

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}