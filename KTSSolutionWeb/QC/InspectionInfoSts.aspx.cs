﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Web.UI.WebControls;

namespace KTSSolutionWeb
{
    public partial class InspectionInfoSts : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPageInit();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetPageInit()
        {
            try
            {
                SetDDLLv1Grp();

                this.txbStDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");

                string strEmpNo = "";
                string strMonth = this.txbEnDt.Text.Replace("-", "").Substring(0, 6);

                this.hfOrgCd.Value = Session["ORGCD"].ToString();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Attributes.Add("onClick", "fn_PopupOrgTree('', '');");
                    this.txbTeam.Attributes.Add("onClick", "fn_PopupOrgTree('', '');");

                    strEmpNo = "";
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    strEmpNo = this.Session["EMPNO"].ToString();
                }

                DataSet ds = new DataSet();

                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList(strEmpNo, strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, ds.Tables[0]);
                }
                
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLLv1Grp()
        {
            DataSet ds = null;

            try
            {
                ddlLv1Grp.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "INSPECTIONLV1GRPLIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    ddlLv1Grp.Items.Add(new ListItem("전체", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["LV1GRPNM"].ToString().Equals(""))
                        {
                            continue;
                        }

                        ddlLv1Grp.Items.Add(new ListItem(ds.Tables[0].Rows[i]["LV1GRPNM"].ToString(), ds.Tables[0].Rows[i]["LV1GRPCD"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("대분류를 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLv2GrpList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONLV2GRPLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLv3GrpList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONLV3GRPLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();
            DataSet dsOrg = new DataSet();

            string result = string.Empty;

            string strOrgCd = "";
            string strMonth = aData[1];

            using (OrgCodeTree org = new OrgCodeTree())
            {
                DateTime chkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime month = new DateTime(int.Parse(strMonth.Substring(0, 4)), int.Parse(strMonth.Substring(4, 2)), 1);

                if (chkDate > month)
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }
                else
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (aData[0].Length == 0)
                {
                    using (KTSUser user = new KTSUser())
                    {
                        dsOrg = user.GetUserViewOrgMonth2(HttpContext.Current.Session["EMPNO"].ToString(), strMonth);
                    }

                    for (int i = 0; i < dsOrg.Tables[0].Rows.Count; i++)
                    {
                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += dsOrg.Tables[0].Rows[i]["ORGCD"].ToString();
                    }
                }
                else
                {
                    strOrgCd = aData[0];
                }
            }

            result = Utility.GetOrgNm(strOrgCd, ds.Tables[0]);

            return strOrgCd + "|" + result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONINFOLIST";
            string jobGb = "GET";

            string strAuthID = HttpContext.Current.Session["AUTHID"].ToString();
            string strEmpNo = string.Empty;

            //if (!strAuthID.Equals("AUTH0001") && !strAuthID.Equals("AUTH0002") && !strAuthID.Equals("AUTH0003")
            //    && !strAuthID.Equals("AUTH0004") && !strAuthID.Equals("AUTH0005"))
            //{
            //    strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();
            //}
            strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            //aData[0] = OrgCd.replace(/,/ gi, "^"); ;
            //aData[1] = StDate;
            //aData[2] = EnDate;
            //aData[3] = ChkCd;
            //aData[4] = GrpCd_Lv1;
            //aData[5] = GrpCd_Lv2;
            //aData[6] = GrpCd_Lv3;
            //aData[7] = SearchType;

            aData.Add(strEmpNo); //파라미터 추가 시

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}