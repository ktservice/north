﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegInspectionInfo.aspx.cs" Inherits="KTSSolutionWeb.RegInspectionInfo"  EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title id="title" runat="server">점검표 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>
        .scrollbox {overflow-x:hidden; overflow-y:auto; width:100%;}
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .div-Screenlock {
            position: absolute;
            top: 580px;
            width: 100%;
            height: 255px;
            background-color: #ffffff !important;
            z-index: 99;
            opacity: 0.7;
            overflow: hidden;
            display:none;
        }

        .td-button {
            width: 20px !important;
            padding:0px !important;
        }
        
        .th-line {
            text-align:left !important;
            border-right: 1px solid #dfdfdf;
        }

        .th-line.last {
            border-right: none;
        }

        .div-radio {
            border: 1px solid darkgray;
            width: 70px;
            height: 24px;
            cursor: pointer;
            padding-top: 1px;
            padding-left: 5px;
            margin-right: 4px;
            text-align: left;
            border-radius: 3px;
            background-color: white;
            float: left;
        }

        .btn-imgSave {
            width: 40px;
            min-width: 60px;
            height: 34px;
            padding: 0 3px;
            background: #2CBBB7;
            text-align: center;
            border-radius: 2px;
            color: #fff;
            font-size: 14px;
            font-weight: 800;
            margin-top: -5px;
            margin-bottom: -5px;
            border-style: none;
            float: right;
        }
    </style>
    <script type="text/javascript">

        var grid;
        var callGb;
        var reloadGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
            page.fn_Load();
        });

        var page = {
            init: function () {
            },
            eventbind: function () {
                $("#<%= ddlCompany.ClientID %>").change(function (e) {
                    $("#<%= txbPartnersEmpNm.ClientID %>").val("");
                    $("#<%= hdfPartnersEmpNo.ClientID %>").val("");

                    var CompCd = $("#<%= ddlCompany.ClientID %> option:selected").val();
                    var delegate = "";

                    if (CompCd != "") {
                        delegate = CompCd.split('_')[2];
                    }

                    if (delegate == "Y") {
                        $("#<%= txbPartnersEmpNm.ClientID %>").val("협력사 대표자");
                    }
                });

                 $("#<%= ddlLv1Grp.ClientID %>").change(function (e) {
                     var lv1GrpCd = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();

                    if (lv1GrpCd == "") {
                        $("#ddlLv2Grp").attr("disabled", "disabled");
                        $("#ddlLv3Grp").attr("disabled", "disabled");
                        fn_ControlBind_Lv2Grp(null, false);
                        fn_ControlBind_Lv3Grp(null);;
                    }
                    else {
                        $("#ddlLv2Grp").removeAttr("disabled");
                        fn_ControlBind_Lv3Grp(null);

                        fn_GetLv2Grp(lv1GrpCd);
                    }
                });

                $("#ddlLv2Grp").change(function (e) {
                    var lv2GrpCd = $("#ddlLv2Grp").val();
                    $("#ddlLv3Grp").removeAttr("disabled");

                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = "";
                    options.text = "전체";
                    options.selected = true;

                    if (lv2GrpCd == "") {
                        $("#ddlLv3Grp").attr("disabled", "disabled");
                        fn_ControlBind_Lv3Grp(null);;
                    }
                    else {
                        fn_GetLv3Grp(lv2GrpCd);
                    }
                });

                $("#btnReg").click(function (e) {
                    fn_RegInspectionInfo();
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });
            },
            fn_Load: function () {
                var chkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
                var actStatus = $("#<%= hdfActStatus.ClientID %>").val();

                if (chkStatus != "") {
                    if (chkStatus == 'F' && actStatus == 'U') {
                        $("#hReq").hide();
                        $("#tableReq").hide();
                    }
                    else {
                        $("#hReq").show();
                        $("#tableReq").show();
                    }

                    if (actStatus == 'F') {
                        $("#hAct").show();
                        $("#tableAct").show();
                    }
                    else {
                        $("#hAct").hide();
                        $("#tableAct").hide();
                    }

                    if (chkStatus == 'F') {
                        $("#stitle").text('품질검사 조치요청서 [완료]');
                    }
                    else if (chkStatus == 'T') {
                        $("#stitle").text('품질검사 조치요청서 [등록중]');
                    }
                    else if (chkStatus == 'R' || chkStatus == 'B') {
                        $("#stitle").text('품질검사 조치요청서 [조치요청중]');
                    }
                    else {
                        $("#stitle").text('품질검사 조치요청서');
                    }
                }
            }
        };
        
        var fn_SetControls = function (bRegInfo, bRegImg) {
            var chkList = $("#<%= hdfResult.ClientID %>").val();
            var lv2 = $("#<%= hdfLv2.ClientID %>").val();
            var lv3 = $("#<%= hdfLv3.ClientID %>").val();
            var imgReqData = $("#<%= hdfReqImg.ClientID %>").val();
            var imgActData = $("#<%= hdfActImg.ClientID %>").val();

            fn_ControlBind_ChkList(chkList, bRegInfo);
            fn_ControlBind_Lv2Grp(lv2, false);
            fn_ControlBind_Lv3Grp(lv3);

            if (imgReqData != '') {
                fn_ControlBind_Img(imgReqData, bRegImg, 'R');
            }

            if (imgActData != '') {
                fn_ControlBind_Img(imgActData, false, 'A');
            }

            if (!bRegInfo) {
                $("#ddlLv2Grp").attr("disabled", "disabled");
                $("#ddlLv3Grp").attr("disabled", "disabled");
            }
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://bss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetLv2Grp') {
                    callGb = '';

                    fn_ControlBind_Lv2Grp(json, true);
                }
                else if (callGb == 'GetLv3Grp') {
                    callGb = '';

                    fn_ControlBind_Lv3Grp(json);
                }
                else if (callGb == 'GetChkList') {
                    callGb = '';

                    fn_ControlBind_ChkList(json, true);
                }
                else if (callGb == 'GetImgData') {
                    callGb = '';
                    var status = $("#<%= hdfChkStatus.ClientID %>").val();
                    var bEnable = false;

                    if (status == 'T') {
                        bEnable = true;
                    }

                    fn_ControlBind_Img(json, bEnable, 'R');
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'InsOk') {
                            alert('신규 조치요청서 등록이 완료되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'UpdOk') {
                            alert('조치요청서 수정이 완료되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'DelOk') {
                            alert('조치요청서가 삭제 되었습니다.');
                            fn_ParentSearch(true);

                            callGb = '';
                        }
                        else if (callGb == 'FileInsOk') {
                            alert('요청사항 이미지가 등록 되었습니다.');
                            $("#filebox").empty();
                            $("#divReqImg").empty();
                            callGb = '';

                            fn_GegImgData("R");
                        }
                        else if (callGb == 'FileDelOk') {
                            alert('요청사항 이미지가 삭제 되었습니다.');
                            $("#filebox").empty();
                            $("#divReqImg").empty();
                            callGb = '';

                            fn_GegImgData("R");
                        }
                        else if (callGb == 'SetStatusOK_R') {
                            alert('점검결과 추가 조치사항을 요청하였습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'SetStatusOK_B') {
                            alert('조치사항을 반려하고 재요청 하였습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'SetStatusOK_F') {
                            alert('완료 되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }                        

                        if (reloadGb == "Y") {
                            var inspection_No = "";

                            fn_ParentSearch(false);

                            if (item.INSPECTION_NO != undefined && item.INSPECTION_NO != '') {
                                inspection_No = item.INSPECTION_NO;
                                
                            }
                            else {
                                inspection_No = $("#<%= hdfInspection_No.ClientID %>").val();
                            }

                            if (inspection_No != '' && inspection_No != undefined) {
                                fn_Reload(inspection_No);
                            }

                            reloadGb = '';
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var fn_GetLv2Grp = function (lv1GrpCd) {
            if (lv1GrpCd != "") {
                var aData = [];

                aData[0] = lv1GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv2GrpList');

                callGb = 'GetLv2Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetLv3Grp = function (lv2GrpCd) {

            if (lv2GrpCd != "") {

                var aData = [];

                aData[0] = lv2GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv3GrpList');

                callGb = 'GetLv3Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetChkList = function () {
            var lv1GrpCd = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();

            if (lv1GrpCd != "" && lv1GrpCd != undefined) {
                //입력 파라미터
                var aData = [];
                aData[0] = lv1GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetChkListData');

                callGb = 'GetChkList';

                ajaxCall(jsonData, ajaxUrl, false);
            }
            else {
                $("#divReg").hide();
                $("#btnReg").hide();
                $("#tList").empty();

                window.resizeTo(896, 796);

                $("#hList").hide();
                $("#tableList").hide();

                //$("#divScreenLock").show();
            }
        };

        var fn_RegInspectionInfo = function () {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var status = $("#<%= hdfChkStatus.ClientID %>").val();
            var CompanyCd = $("#<%= ddlCompany.ClientID %>").val().replace("_Y", "").replace("_N", "");
            var DelegateChk = $("#<%= ddlCompany.ClientID %>").val().split('_')[2];
            
            var confMsg = "";

            var bChk = false;
            var sUrl = "";
            var aData = [];

            var ChkDt = $("#<%= txbChkDt.ClientID %>").val();
            var ChkOrgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();
            var ChkUser = $("#<%= hdfChkEmpNo.ClientID %>").val();
            var ActUser = $("#<%= hdfPartnersEmpNo.ClientID %>").val();
            var Noti = $("#<%= txbNoti.ClientID %>").val();
            var LineNo = $("#<%= txbLineNo.ClientID %>").val();
            var CustNm = $("#<%= txbCustNm.ClientID %>").val();
            var Lv1Grp = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();
            var Lv2Grp = $("#ddlLv2Grp option:selected").val();
            var Lv3Grp = $("#ddlLv3Grp option:selected").val();

            var tList = $("#tList");

            if (inspectionNo == "") {
                confMsg = "조치요청서를 등록 하시겠습니까?"
                callGb = "InsOk";
                sUrl = "InsInspectionInfo";
            }
            else {
                confMsg = "등록된 조치요청서를 수정 하시겠습니까?"
                callGb = "UpdOk";
                sUrl = "UpdInspectionInfo";
            }
            if (confirm(confMsg)) {
                var message = "";
                var ChkResult = "";

                var rbNm = "";

                tList.find("input[type='radio']").each(function (index, item) {                        
                    if (rbNm != $(item).attr("name")) {
                        rbNm = $(item).attr("name");

                        if (ChkResult != "") {
                            ChkResult += "^";
                        }

                        var val = $("input[name='" + rbNm + "']:checked").val();

                        if (val == undefined) {
                            ChkResult += " ";
                        } else {
                            ChkResult += val;
                        }
                    }
                });

                ChkResult = ChkResult.replaceAll(' ', '') + "^";

                if (ChkDt == "") {
                    message = "검사일을 입력해주세요.";
                }
                else if (ChkOrgCd == "" || ChkUser == "") {
                    message = "검사원 정보를 입력해주세요.";
                }
                else if (CompanyCd == "" || CompanyCd == undefined || CompanyCd == null) {
                    message = "업체 정보를 선택해주세요.";
                }
                else if (DelegateChk == "N" && ActUser == "") {
                    message = "등록된 협력사 대표자가 없습니다. 현장대리인 정보를 입력해주세요.";
                }
                else if (Lv1Grp == "" || Lv2Grp == "" || Lv3Grp == "") {
                    message = "조치항목 분류를 입력해주세요.";
                }
                else {
                    bChk = true;
                }

                aData[0] = ChkDt;
                aData[1] = ChkOrgCd;
                aData[2] = ChkUser;
                aData[3] = CompanyCd;
                aData[4] = ActUser;
                aData[5] = Noti;
                aData[6] = LineNo;
                aData[7] = CustNm;
                aData[8] = Lv1Grp;
                aData[9] = Lv2Grp;
                aData[10] = Lv3Grp;
                aData[11] = ChkResult;

                if (callGb == "UpdOk") {
                    aData[12] = inspectionNo;
                }
                    
                if (bChk) {
                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
                else {
                    alert(message);
                }
            }
        };

        var fn_DelInspectionInfo = function (inspection_no) {
            if (inspection_no == '' || inspection_no == undefined) {
                alert("잘못된 접근입니다.");
            }
            else {
                var aData = [];

                callGb = "DelOk";

                aData[0] = inspection_no;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "DelInspectionInfo");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        };

        var fn_RegImgData = function (typeCd) {
            var imgFiles = $("#filebox").find("input[type='file']");
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo == "") {
                alert("잘못된 접근입니다.");
            }
            else if (imgFiles.length == 0) {
                alert("선택된 파일이 없습니다.");
            }
            else {
                var formData = new FormData();

                for (var i = 0; i < imgFiles.length; i++) {
                    var keyNm = inspectionNo + "_" + typeCd + "_" + i;

                    formData.append(keyNm, imgFiles[i].files[0]);
                }

                var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/QC/FileUploadHandler.ashx");

                callGb = "FileInsOk";

                ajaxCallFileUpload(formData, ajaxUrl);
            }
        };

        var fn_DelImgData = function (imgNo, typeCd) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var status = $("#<%= hdfChkStatus.ClientID %>").val();
            var message = "";

            var bChk = false;
            var aData = [];

            if (inspectionNo == "" || imgNo.length == 0 || imgNo == undefined || typeCd == "" || typeCd == undefined) {
                message = "잘못된 접근입니다.";
            }
            else {
                bChk = true;

                callGb = 'FileDelOk';

                aData[0] = inspectionNo;
                aData[1] = typeCd;
                aData[2] = imgNo;
            }

            if (bChk) {
                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "DelImgData");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            else {
                alert(message);
            }
        };

        var fn_GegImgData = function (typeCd) {
            
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo != "" && inspectionNo != undefined) {
                //입력 파라미터
                var aData = [];
                aData[0] = inspectionNo;
                aData[1] = typeCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetImgData');

                callGb = 'GetImgData';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_SetStatus = function (status) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var ChkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var ActStatus = $("#<%= hdfActStatus.ClientID %>").val();
            var reqNoti = $("#<%= txbReqNoti.ClientID %>").val();
            var rejectNoti = $("#<%= txbReject.ClientID %>").val();
                
            var message = '';

            var bChk = false;
            var aData = [];

            if (inspectionNo != "") {

                if (status == 'R') {
                    message = "요청사항에 대한 조치요청을 하시겠습니까?";
                    callGb = 'SetStatusOK_R';

                    if (reqNoti != '') {
                        bChk = true;
                    }
                    else {
                        message = "요청사항을 입력해주세요.";
                    }
                }
                else if (status == 'B') {

                    message = "조치완료 사항을 반려하고 재요청하시겠습니까?";
                    callGb = 'SetStatusOK_B';

                    if (rejectNoti == '') {
                        message = "반려사유을 입력해주세요.";
                    }
                    else if (reqNoti == '') {
                        message = "요청사항을 입력해주세요.";
                    }
                    else {
                        bChk = true;
                    }
                }
                else if (status == 'F') {
                    message = "완료 처리 하시겠습니까?";
                    callGb = 'SetStatusOK_F';
                    bChk = true;
                }
                else {
                    message = "잘못된 접근입니다.";
                }
            }
            else {
                message = "잘못된 접근입니다.";
            }

            if (bChk) {
                if (confirm(message)) {
                    bChk = true;

                    aData[0] = inspectionNo;
                    aData[1] = status;
                    aData[2] = reqNoti;
                    aData[3] = rejectNoti;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetInspectionStatus");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
            else {
                alert(message);
            }
        };
        
        var fn_ControlBind_Lv2Grp = function (json, bList) {
            var ChkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var Lv2GrpCd = $("#<%= hdfLv2Grp.ClientID %>").val();

            var selector = $("#ddlLv2Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV2GRPCD;
                options.text = item.LV2GRPNM;

                if (item.LV2GRPNM == "") {
                    options.style.display = "none";
                }

                if (item.LV2GRPCD == Lv2GrpCd) {
                    options.selected = true;
                }

                selector.append(options);
            });

            if (bList) {
                fn_GetChkList();
            }
        };

        var fn_ControlBind_Lv3Grp = function (json) {
            var ChkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var Lv3GrpCd = $("#<%= hdfLv3Grp.ClientID %>").val();

            var selector = $("#ddlLv3Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV3GRPCD;
                options.text = item.LV3GRPNM;

                if (item.LV3GRPNM == "") {
                    options.style.display = "none";
                }

                if (item.LV3GRPCD == Lv3GrpCd) {
                    options.selected = true;
                }

                selector.append(options);
            });
        };

        var fn_ControlBind_ChkList = function (json, bEnabled) {
            //감리항목 바인드
            var tList = $("#tList");

            tList.empty();

            var i = 0;
            var preUpperCd = "";

            $.each(JSON.parse(json), function (idx, item) {
                i++;

                var tr = document.createElement("tr");
                var topLine = false;

                //if (item.UPPERLISTCD != "E00000" && preUpperCd != item.UPPERLISTCD) {
                if (preUpperCd != item.UPPERLISTCD) {
                    //var trTitle = document.createElement("tr");

                    var tdTitle = document.createElement("td");
                    tdTitle.style.width = "120px";
                    tdTitle.style.height = "28px";
                    tdTitle.style.border = "none";
                    tdTitle.style.borderRight = "1px solid #dfdfdf";
                    tdTitle.style.borderBottom = "1px solid #dfdfdf";
                    tdTitle.style.whiteSpace = "break-spaces";
                    tdTitle.rowSpan = item.ROWCNT;

                    tdTitle.innerText = item.UPPERLISTNM;

                    tr.append(tdTitle);
                    tList.append(tr);

                    topLine = true;
                }

                preUpperCd = item.UPPERLISTCD;


                var td1 = document.createElement("td");
                
                td1.style.textAlign = "left";
                td1.style.height = "28px";
                td1.style.border = "none";
                td1.style.borderRight = "1px solid #dfdfdf";
                td1.style.whiteSpace = "break-spaces";

                if (topLine) {
                    td1.style.borderTop = "1px solid #dfdfdf";
                }

                td1.innerText = "- " + item.CHKLIST;

                tr.append(td1);

                var td2 = document.createElement("td");

                td2.style.height = "28px";
                td2.style.border = "none";
                td2.style.textAlign = "left";

                if (topLine) {
                    td2.style.borderTop = "1px solid #dfdfdf";
                }

                var divS = document.createElement("div");
                divS.className = "div-radio";

                if (bEnabled) {
                    divS.setAttribute("onclick", "fn_radioBoxChk('rbS" + i + "')");
                }

                var lblChkS = document.createElement("label");
                lblChkS.style.cursor = "pointer";

                var rbChkS = document.createElement("input");

                rbChkS.type = "radio";
                rbChkS.name = "rbChkList" + i;
                rbChkS.id = "rbS" + i;
                rbChkS.value = "S";
                rbChkS.style.marginRight = "3px";

                if (item.RESULTCD == "S") {
                    rbChkS.checked = true;
                }
                if (!bEnabled) {
                    rbChkS.setAttribute("disabled", "disabled");
                }

                lblChkS.append(rbChkS);
                lblChkS.append("정상");

                divS.append(lblChkS);

                td2.append(divS);

                var divF = document.createElement("div");
                divF.className = "div-radio";

                if (bEnabled) {
                    divF.setAttribute("onclick", "fn_radioBoxChk('rbF" + i + "')");
                }

                var lblChkF = document.createElement("label");
                lblChkF.style.cursor = "pointer";

                var rbChkF = document.createElement("input");

                rbChkF.type = "radio";
                rbChkF.name = "rbChkList" + i;
                rbChkF.id = "rbF" + i;
                rbChkF.value = "F";
                rbChkF.style.marginRight = "3px";

                if (item.RESULTCD == "F") {
                    rbChkF.checked = true;
                }

                if (!bEnabled) {
                    rbChkF.setAttribute("disabled", "disabled");
                }

                lblChkF.append(rbChkF);
                lblChkF.append("불량");

                divF.append(lblChkF);

                td2.append(divF);

                var divN = document.createElement("div");
                divN.className = "div-radio";

                if (bEnabled) {
                    divN.setAttribute("onclick", "fn_radioBoxChk('rbN" + i + "')");
                }

                var lblChkN = document.createElement("label");
                lblChkN.style.cursor = "pointer";

                var rbChkN = document.createElement("input");

                rbChkN.type = "radio";
                rbChkN.name = "rbChkList" + i;
                rbChkN.id = "rbN" + i;
                rbChkN.value = "N";
                rbChkN.style.marginRight = "3px";

                if (item.RESULTCD == "N") {
                    rbChkN.checked = true;
                }

                if (!bEnabled) {
                    rbChkN.setAttribute("disabled", "disabled");
                }

                lblChkN.append(rbChkN);
                lblChkN.append("미대상");

                divN.append(lblChkN);

                td2.append(divN);

                tr.append(td2);

                tList.append(tr);
            });

            if (i == 0) {
                var tr = document.createElement("tr");

                var td = document.createElement("td");
                td.style.textAlign = "left";
                td.colSpan = 2;
                td.innerText = "등록된 점검항목이 없습니다.";

                tr.append(td);

                tList.append(tr);
            }

            //$("#divScreenLock").hide();

            window.resizeTo(896, 1000);

            $("#hList").show();
            $("#tableList").show();
            if (bEnabled) {
                $("#divReg").show();
                $("#btnReg").show();
                $("#ddlLv2Grp").change();
            }
            else {
                $("#divReg").hide();
                $("#btnReg").hide();
                $("#ddlLv2Grp").change();
            }
        };

        var fn_ControlBind_Img = function (json, bEnabled, typeCd) {
            var tdImg;
            var divImgList;

            if (typeCd == 'R') {
                tdImg = $("#<%= tdReqImg.ClientID %>");
                divImgList = $("#divReqImg");
            }
            else {
                tdImg = $("#tdActImg");
            }

            tdImg.empty();

            $.each(JSON.parse(json), function (idx, item) {
                var img = document.createElement("img");
                var imgData = fn_ArrayToBase64(item.IMGDATA);

                img.setAttribute("id", "img" + item.IMG_NO);
                img.src = "data:" + item.IMGTYPE + "; base64," + imgData;
                img.style.maxWidth = "100%";

                tdImg.append(img);

                if (typeCd == 'R') {
                    var p = document.createElement("p");
                    p.className = "inpbox";
                    p.style.display = "block";
                    p.style.textAlign = "left";

                    var label = document.createElement("label");
                    label.setAttribute("id", "lblImg" + item.IMG_NO);
                    label.setAttribute("name", "lblReqFileNm");
                    label.style.height = "40px";
                    label.style.paddingTop = "10px";
                    label.style.maxWidth = "80%";
                    label.innerText = item.IMGNAME;

                    p.append(label);

                    if (bEnabled) {
                        var button = document.createElement("button");
                        button.type = "button";
                        button.setAttribute("id", "btnImgDel" + item.IMG_NO);
                        button.setAttribute("onclick", "javascript:fn_DelFileCheck(" + item.IMG_NO + ", 'R');");
                        button.style.width = "30px";
                        button.style.height = "30px";
                        button.style.lineHeight = "0";
                        button.style.color = "black";
                        button.style.padding = "0";
                        button.style.margin = "0";
                        button.style.border = "none";
                        button.innerText = "X";

                        p.append(button);
                    }

                    divImgList.append(p);
                }
            });
        };

        var fn_ArrayToBase64 = function (buffer) {

            var binary = '';

            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;

            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }

            return window.btoa(binary);
        };

        var fn_radioBoxChk = function (val) {
            $("#" + val).prop('checked', true);
            $("#" + val).change();
        };

        var fn_PopupKtsUser = function () {
            var orgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();

            if (orgCd != '' && orgCd != undefined) {
                var form = "/Common/Search_ktsUser";
                var target = "Search_ktsUser";

                var param = {
                    pORGCD: orgCd
                };

                var nWidth = 1050;
                var nHeight = 515;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
            else {
                alert('소속 조직을 먼저 선택해주세요');
            }
        };

        var fn_PopupOrgCd = function () {

            var form = "/Common/Search_OrgCd";
            var target = "Search_OrgCd";

            var param = {};

            var nWidth = 950;
            var nHeight = 515;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_PopupPartnersUser = function () {
            var CompCd = $("#<%= ddlCompany.ClientID %> option:selected").val();

            if (CompCd != '' && CompCd != undefined) {
                var form = "/Common/Search_PartnersUser";
                var target = "Search_PartnersUser";

                var param = {
                    pCOMPCD: CompCd
                };

                var nWidth = 950;
                var nHeight = 515;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
            else {
                alert('업체를 선택해주세요');
            }
        };

        var PopupEmpUser = function () {

            var nWidth = 700;
            var nHeight = 500;

            var param = {};

            var form = "/Common/SearchUser";
            var target = "SearchUser";

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_SetOrg = function (orgcd, orglv2nm, orglv3nm, orglv4nm, orgnm) {
            var preOrgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();

            if (orgcd != preOrgCd) {
                var orgNm = orglv2nm;

                if (orglv3nm != '') {
                    orgNm += " " + orglv3nm;
                }

                if (orglv4nm != '') {
                    orgNm += " " + orglv4nm;
                }

                if (orgnm != '' && orglv2nm != orgnm && orglv3nm != orgnm && orglv4nm != orgnm  ) {
                    orgNm += " " + orgnm;
                }

                $("#<%= txbChkOrgNm.ClientID %>").val(orgNm);
                $("#<%= txbChkEmpNm.ClientID %>").val('');
                $("#<%= hdfChkOrgCd.ClientID %>").val(orgcd);
                $("#<%= hdfChkEmpNo.ClientID %>").val('');
            }
        };

        var fn_SetEmpUser = function (empno, empnm) {
            $("#<%= txbChkEmpNm.ClientID %>").val(empnm);
            $("#<%= hdfChkEmpNo.ClientID %>").val(empno);
        };

        var fn_SetPartnersUser = function (empno, empnm) {

            var CompCd = $("#<%= ddlCompany.ClientID %> option:selected").val();
            var delegate = "";

            if (CompCd != "") {
                delegate = CompCd.split('_')[2];
            }

            if (empno != '') {
                $("#<%= txbPartnersEmpNm.ClientID %>").val(empnm);
            } else {
                if (delegate == "Y") {
                    $("#<%= txbPartnersEmpNm.ClientID %>").val("협력사 대표자");
                }
                else {
                    $("#<%= txbPartnersEmpNm.ClientID %>").val("");
                }
            }

            $("#<%= hdfPartnersEmpNo.ClientID %>").val(empno);
        };        

        var fn_ParentSearch = function (bClose) {
            opener.fn_Search();

            if (bClose) {
                window.close();
            }
        };

        var fn_DelInfoChk = function (inspection_no) {
            if (confirm("조회된 조치요청서를 삭제 하시겠습니까?")) {
                fn_DelInspectionInfo(inspection_no);
            }
        };

        var fn_RegFileCheck = function (typeCd) {
            if (confirm("참조 파일들 저장 하시겠습니까?")) {
                fn_RegImgData(typeCd);
            }
        };

        var fn_DelFileCheck = function (imgNo, typeCd) {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                fn_DelImgData(imgNo, typeCd);
            }
        };

        var fn_Reload = function (inspection_no) {
            var form;

            form = "/QC/RegInspectionInfo";

            var param = { pINSPECTION_NO: inspection_no };

            var nWidth = 880;
            var nHeight = 720;

            cmPostPopup(form, "_self", param, nWidth, nHeight);
        };

        var AddFileUpload = function () {
            var cnt1 = $("#filebox").find("input[type='file']").length;
            var cnt2 = $("#divReqImg").find("label[name='lblReqFileNm']").length;

            if (cnt1 + cnt2 >= 4) {
                return;
            }

            if (!document.getElementById && !document.createElement)
                return;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-right:0;");
            newFileBox.setAttribute("name", "reqfile");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:140px;");
            newFileText.style.marginLeft = "0";

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");
            newFileSpan.style.width = "30px";

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "fileTypeChk('fu" + AddFileUpload.lastAssignedId + "', this.value);");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("accept", "image/*");

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.style.width = "30px";
            newFileLabel.innerText = "...";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fileTypeChk = function (id, val) {
            var filechk = false;

            var extIndex = val.lastIndexOf('.');

            var extList = ["tiff", "pjp", "jfif", "bmp", "gif", "svg", "png", "xbm", "dib", "jxl", "jpeg", "svgz", "jpg", "webp", "ico", "tif", "pjpeg", "avif"];

            if (extIndex > 1) {

                var fileExt = val.substring(extIndex + 1, val.length).toLowerCase();

                if (extList.indexOf(fileExt) > 0) {
                    filechk = true;
                }
            }
            if (filechk) {
                document.getElementById(id + "-value").value = val;
            }
            else {
                alert("이미지파일만 업로드 가능합니다.");

                document.getElementById(id).value = "";
                document.getElementById(id + "-value").value = "";
            }
        };
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong id="stitle">품질검사 조치요청서</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents" style="padding-top: 10px;">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <div id="divDel" runat="server" style="text-align:right; margin-bottom:10px;" visible="false">
                                <input id="btnDel" runat="server" type="button" class="btn-del" style="height:25px;" value="삭제" />
                            </div>
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <h4>kt service 북부 검사원</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:130px;" />
                                        <col style="width:410px;" />
                                        <col style="width:200px;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">검사일&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line">소속</th>
                                            <th class="th-line last">검사원&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbChkDt" runat="server" Width="120px" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbChkOrgNm" runat="server" Width="380px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbChkEmpNm" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfChkOrgCd" runat="server" />
                                                    <asp:HiddenField ID="hdfChkEmpNo" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4>검사대상</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:180px;" />
                                        <col style="width:180px;" />
                                        <col style="width:370px;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">업체명&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line">현장대리인&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line last">주요내용</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" style="width:160px" AutoPostBack="false"></asp:DropDownList>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbPartnersEmpNm" runat="server" Width="160px" ReadOnly="true"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfPartnersEmpNo" runat="server" />
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbNoti" runat="server" Width="350px"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4>고객정보</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:50%;" />
                                        <col style="width:50%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">회선번호</th>
                                            <th class="th-line last">고객명</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbLineNo" runat="server" Width="100%"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCustNm" runat="server" Width="100%"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4>조치항목 분류</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:33%;" />
                                        <col style="width:33%;" />
                                        <col style="width:33%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">대분류&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line">중분류</th>
                                            <th class="th-line last">소분류</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID ="ddlLv1Grp" runat="server" style="width:95%" AutoPostBack="false"></asp:DropDownList>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <select id="ddlLv2Grp" style="width:95%" disabled="disabled">
                                                        <option value="">선택하세요</option>
                                                    </select>
                                                    <asp:HiddenField ID="hdfLv2Grp" runat="server" />
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <select id="ddlLv3Grp" style="width:95%" disabled="disabled">
                                                        <option value="">선택하세요</option>
                                                    </select>
                                                    <asp:HiddenField ID="hdfLv3Grp" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4 id="hList" style="display:none;">검사목록</h4>
                                <table id="tableList" style="display:none;" >
                                    <thead>
                                        <tr>
                                            <th class="th-line" colspan="2" style="min-width:510px; max-width:510px; width:510px;">항목</th>
                                            <th class="th-line last" style="min-width:275px; max-width:275px; width:275px">결과</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tList">

                                    </tbody>
                                </table>
                                <div id="divReg" style="height:24px; margin-top:3px;text-align:center;display:none;">
                                    <input id="btnReg" type="button" class="btn-green" style="display:none;float:right" value="저장" />
                                </div>
                                <hr class="blank" />
                                <h4 id="hReq" style="display:none;">조치요청</h4>
                                <table id="tableReq" style="display:none;">
                                    <colgroup>
                                        <col style="width:74%;" />
                                        <col style="width:26%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line last" colspan="2">요청사항
                                                <button type="button" id="btnReqImg" runat="server" class="btn-imgSave">사진저장</button>
                                                <button type="button" id="btnReqAdd" runat="server" class="btn-replysave" style="width: 30px;height:34px;margin-top:-5px;margin-bottom:-5px;margin-right:10px;float:right;">추가</button>
    
                                            </th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="tdReqImg" runat="server" style="min-height:300px;height:300px;vertical-align:top;white-space:normal">
                                            </td>
                                            <td id="tdReqImgNm" runat="server" style="min-height:300px;height:300px;vertical-align:top;padding:5px 10px 5px;">
                                                <div>
                                                    <div id="filebox">
                                                    </div>                                                    
                                                </div>
                                                <div id="divReqImg" style="width:100%;overflow-x:hidden;overflow-y:auto; margin-top:2px; margin-bottom:2px">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p class="inpbox">                                                    
                                                    <asp:TextBox ID="txbReqNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="divReqAct" runat="server" style="height:24px; margin-top:3px;text-align:center;" visible="false">
                                    <input id="btnReqAct" runat="server" type="button" class="btn-green" style="float:right" visible="false" value="조치요청" />
                                </div>
                                <hr class="blank" />
                                <h4 id="hAct" style="display:none;">요구사항 조치결과</h4>
                                <table id="tableAct" style="display:none;">
                                    <colgroup>
                                        <col style="width:100%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line last">조치결과</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="tdActImg" style="min-height:300px;height:300px;vertical-align:top;white-space:normal">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="inpbox">                                                    
                                                    <asp:TextBox ID="txbActNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4 id="hReject" runat="server" visible="false">반려사유</h4>
                                <table id="tableReject" runat="server" visible="false">
                                    <colgroup>
                                        <col style="width:100%;" />
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <p class="inpbox">                                                    
                                                    <asp:TextBox ID="txbReject" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <asp:HiddenField ID="hdfInspection_No" runat="server"/>
                                <asp:HiddenField ID="hdfChkStatus" runat="server"/>
                                <asp:HiddenField ID="hdfActStatus" runat="server"/>
                                <asp:HiddenField ID="hdfResult" runat="server"/>
                                <asp:HiddenField ID="hdfLv2" runat="server"/>
                                <asp:HiddenField ID="hdfLv3" runat="server"/>
                                <asp:HiddenField ID="hdfReqImg" runat="server"/>
                                <asp:HiddenField ID="hdfActImg" runat="server"/>                                
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력값입니다.
                        <div class="btncenter">
                            <input id="btnChkFin" runat="server" type="button" class="btn-green" visible="false" value="완료" />
                            <input id="btnReject" runat="server" type="button" class="btn-black" visible="false" value="반려" />
                            <input id="btnClose" runat="server" type="button" class="btn-gray" value="닫기" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <div id="divScreenLock" class="div-Screenlock"></div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
