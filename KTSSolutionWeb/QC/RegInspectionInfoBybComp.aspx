﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegInspectionInfoBybComp.aspx.cs" Inherits="KTSSolutionWeb.RegInspectionInfoBybComp"  EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title id="title" runat="server">점검표 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>
        .scrollbox {overflow-x:hidden; overflow-y:auto; width:100%;}
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .div-Screenlock {
            position: absolute;
            top: 580px;
            width: 100%;
            height: 255px;
            background-color: #ffffff !important;
            z-index: 99;
            opacity: 0.7;
            overflow: hidden;
            display:none;
        }

        .td-button {
            width: 20px !important;
            padding:0px !important;
        }
        
        .th-line {
            text-align:left !important;
            border-right: 1px solid #dfdfdf;
        }

        .th-line.last {
            border-right: none;
        }

        .div-radio {
            border: 1px solid darkgray;
            width: 70px;
            height: 24px;
            cursor: pointer;
            padding-top: 1px;
            padding-left: 5px;
            margin-right: 4px;
            text-align: left;
            border-radius: 3px;
            background-color: white;
            float: left;
        }

        .btn-imgSave {
            width: 40px;
            min-width: 60px;
            height: 34px;
            padding: 0 3px;
            background: #2CBBB7;
            text-align: center;
            border-radius: 2px;
            color: #fff;
            font-size: 14px;
            font-weight: 800;
            margin-top: -5px;
            margin-bottom: -5px;
            border-style: none;
            float: right;
        }
    </style>
    <script type="text/javascript">

        var grid;
        var callGb;
        var reloadGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
            page.fn_Load();
        });

        var page = {
            init: function () {
            },
            eventbind: function () {
                $("#btnClose").click(function (e) {
                    window.close();
                });
            },
            fn_Load: function () {
                var chkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
                var actStatus = $("#<%= hdfActStatus.ClientID %>").val();

                if (chkStatus != "") {
                    if (chkStatus == 'F' && actStatus == 'U') {
                        $("#hReq").hide();
                        $("#tableReq").hide();
                        $("#hAct").hide();
                        $("#tableAct").hide();                        
                    }
                    else {
                        $("#hReq").show();
                        $("#tableReq").show();
                        $("#hAct").show();
                        $("#tableAct").show();
                    }

                    if (actStatus == 'F') {
                        $("#stitle").text('품질검사 조치내역서 [조치완료]');
                    }
                    else if (actStatus == 'T') {
                        $("#stitle").text('품질검사 조치내역서 [등록중]');
                    }
                    else if (actStatus == 'R') {
                        $("#stitle").text('품질검사 조치내역서 [조치대기중]');
                    }
                    else if (chkStatus == 'F') {
                        $("#stitle").text('품질검사 조치내역서 [완료]');
                    }
                    else {
                        $("#stitle").text('품질검사 조치요청서');
                    }
                }
            }
        };
        
        var fn_SetControls = function (bRegImg) {
            var chkList = $("#<%= hdfResult.ClientID %>").val();
            var imgReqData = $("#<%= hdfReqImg.ClientID %>").val();
            var imgActData = $("#<%= hdfActImg.ClientID %>").val();

            fn_ControlBind_ChkList(chkList);

            if (imgReqData != '') {
                fn_ControlBind_Img(imgReqData, false, 'R');
            }

            if (imgActData != '') {
                fn_ControlBind_Img(imgActData, bRegImg, 'A');
            }
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetImgData') {
                    callGb = '';
                    var status = $("#<%= hdfActStatus.ClientID %>").val();
                    var bEnable = false;

                    if (status == 'T' || status == 'R') {
                        bEnable = true;
                    }

                    fn_ControlBind_Img(json, bEnable, 'A');
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'FileInsOk') {
                            alert('요청사항 이미지가 등록 되었습니다.');
                            $("#filebox").empty();
                            $("#divActImg").empty();
                            callGb = '';

                            fn_GegImgData("A");
                        }
                        else if (callGb == 'FileDelOk') {
                            alert('요청사항 이미지가 삭제 되었습니다.');
                            $("#filebox").empty();
                            $("#divActImg").empty();
                            callGb = '';

                            fn_GegImgData("A");
                        }
                        else if (callGb == 'SetStatusOK_T') {
                            alert('조치내역을 임시저장 하였습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'SetStatusOK_F') {
                            alert('조치완료 처리 되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }

                        if (reloadGb == "Y") {
                            var inspection_No = "";

                            fn_ParentSearch(false);

                            if (item.INSPECTION_NO != undefined && item.INSPECTION_NO != '') {
                                inspection_No = item.INSPECTION_NO;
                            }
                            else {
                                inspection_No = $("#<%= hdfInspection_No.ClientID %>").val();
                            }

                            if (inspection_No != '' && inspection_No != undefined) {
                                fn_Reload(inspection_No);
                            }

                            reloadGb = '';
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://bss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var fn_GetLv2Grp = function (lv1GrpCd) {
            if (lv1GrpCd != "") {
                var aData = [];

                aData[0] = lv1GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv2GrpList');

                callGb = 'GetLv2Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetLv3Grp = function (lv2GrpCd) {

            if (lv2GrpCd != "") {

                var aData = [];

                aData[0] = lv2GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv3GrpList');

                callGb = 'GetLv3Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_RegImgData = function (typeCd) {
            var imgFiles = $("#filebox").find("input[type='file']");
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo == "") {
                alert("잘못된 접근입니다.");
            }
            else if (imgFiles.length == 0) {
                alert("선택된 파일이 없습니다.");
            }
            else {
                var formData = new FormData();

                for (var i = 0; i < imgFiles.length; i++) {
                    var keyNm = inspectionNo + "_" + typeCd + "_" + i;

                    formData.append(keyNm, imgFiles[i].files[0]);
                }

                var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/QC/FileUploadHandler.ashx");

                callGb = "FileInsOk";

                ajaxCallFileUpload(formData, ajaxUrl);
            }
        };

        var fn_DelImgData = function (imgNo, typeCd) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var status = $("#<%= hdfActStatus.ClientID %>").val();
            var message = "";

            var bChk = false;
            var aData = [];

            if (inspectionNo == "" || imgNo.length == 0 || imgNo == undefined || typeCd == "" || typeCd == undefined) {
                message = "잘못된 접근입니다.";
            }
            else {
                bChk = true;

                callGb = 'FileDelOk';

                aData[0] = inspectionNo;
                aData[1] = typeCd;
                aData[2] = imgNo;
            }

            if (bChk) {
                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "DelInspectionInfo");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            else {
                alert(message);
            }
        };

        var fn_GegImgData = function (typeCd) {
            
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo != "" && inspectionNo != undefined) {
                //입력 파라미터
                var aData = [];
                aData[0] = inspectionNo;
                aData[1] = typeCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetImgData');

                callGb = 'GetImgData';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_SetActStatus = function (status) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var ActStatus = $("#<%= hdfActStatus.ClientID %>").val();
            var actNoti = $("#<%= txbActNoti.ClientID %>").val();
            var message = '';

            var bChk = false;
            var aData = [];

            if (inspectionNo != "" && (ActStatus == 'R' || ActStatus == 'T')) {
                if (status == 'T') {
                    if (actNoti != '') {
                        message = "조치내역을 임시 저장하시겠습니까?";
                        callGb = 'SetStatusOK_T';
                        bChk = true;
                    }
                    else {
                        message = "조치결과을 입력해주세요.";
                    }
                }
                else if (status == 'F') {
                    if (actNoti != '') {
                        message = "조치완료 처리 하시겠습니까?";
                        callGb = 'SetStatusOK_F';
                        bChk = true;
                    }
                    else {
                        message = "조치결과을 입력해주세요.";
                    }
                }
                else {
                    message = "잘못된 접근입니다.";
                }
            }
            else {
                message = "잘못된 접근입니다.";
            }

            if (bChk) {
                if (confirm(message)) {
                    bChk = true;

                    aData[0] = inspectionNo;
                    aData[1] = status;
                    aData[2] = actNoti;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetInspectionActStatus");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
            else {
                alert(message);
            }
        };

        var fn_ControlBind_ChkList = function (json) {
            //감리항목 바인드
            var tList = $("#tList");

            tList.empty();

            var i = 0;
            var preUpperCd = "";

            $.each(JSON.parse(json), function (idx, item) {
                i++;

                var tr = document.createElement("tr");
                var topLine = false;

                //if (item.UPPERLISTCD != "E00000" && preUpperCd != item.UPPERLISTCD) {
                if (preUpperCd != item.UPPERLISTCD) {
                    //var trTitle = document.createElement("tr");

                    var tdTitle = document.createElement("td");
                    tdTitle.style.width = "120px";
                    tdTitle.style.height = "28px";
                    tdTitle.style.border = "none";
                    tdTitle.style.borderRight = "1px solid #dfdfdf";
                    tdTitle.style.borderBottom = "1px solid #dfdfdf";
                    tdTitle.style.whiteSpace = "break-spaces";
                    tdTitle.rowSpan = item.ROWCNT;

                    tdTitle.innerText = item.UPPERLISTNM;

                    tr.append(tdTitle);
                    tList.append(tr);

                    bottomLine = true;
                }

                preUpperCd = item.UPPERLISTCD;

                var td1 = document.createElement("td");
                
                td1.style.textAlign = "left";
                td1.style.height = "28px";
                td1.style.border = "none";
                td1.style.borderRight = "1px solid #dfdfdf";
                td1.style.whiteSpace = "break-spaces";

                if (topLine) {
                    td1.style.borderTop = "1px solid #dfdfdf";
                }


                td1.innerText = "- " + item.CHKLIST;

                tr.append(td1);

                var td2 = document.createElement("td");

                td2.style.height = "28px";
                td2.style.border = "none";
                td2.style.textAlign = "left";

                if (topLine) {
                    td2.style.borderTop = "1px solid #dfdfdf";
                }


                var divS = document.createElement("div");
                divS.className = "div-radio";

                var lblChkS = document.createElement("label");
                lblChkS.style.cursor = "pointer";

                var rbChkS = document.createElement("input");

                rbChkS.type = "radio";
                rbChkS.name = "rbChkList" + i;
                rbChkS.id = "rbS" + i;
                rbChkS.value = "S";
                rbChkS.style.marginRight = "3px";
                rbChkS.setAttribute("disabled", "disabled");

                if (item.RESULTCD == "S") {
                    rbChkS.checked = true;
                }

                lblChkS.append(rbChkS);
                lblChkS.append("정상");

                divS.append(lblChkS);

                td2.append(divS);

                var divF = document.createElement("div");
                divF.className = "div-radio";

                var lblChkF = document.createElement("label");
                lblChkF.style.cursor = "pointer";

                var rbChkF = document.createElement("input");

                rbChkF.type = "radio";
                rbChkF.name = "rbChkList" + i;
                rbChkF.id = "rbF" + i;
                rbChkF.value = "F";
                rbChkF.style.marginRight = "3px";
                rbChkF.setAttribute("disabled", "disabled");

                if (item.RESULTCD == "F") {
                    rbChkF.checked = true;
                }

                lblChkF.append(rbChkF);
                lblChkF.append("불량");

                divF.append(lblChkF);

                td2.append(divF);

                var divN = document.createElement("div");
                divN.className = "div-radio";

                var lblChkN = document.createElement("label");
                lblChkN.style.cursor = "pointer";

                var rbChkN = document.createElement("input");

                rbChkN.type = "radio";
                rbChkN.name = "rbChkList" + i;
                rbChkN.id = "rbN" + i;
                rbChkN.value = "N";
                rbChkN.style.marginRight = "3px";
                rbChkN.setAttribute("disabled", "disabled");

                if (item.RESULTCD == "N") {
                    rbChkN.checked = true;
                }

                lblChkN.append(rbChkN);
                lblChkN.append("미대상");

                divN.append(lblChkN);

                td2.append(divN);

                tr.append(td2);

                tList.append(tr);
            });

            if (i == 0) {
                var tr = document.createElement("tr");

                var td = document.createElement("td");
                td.style.textAlign = "left";
                td.colSpan = 2;
                td.innerText = "등록된 점검항목이 없습니다.";

                tr.append(td);

                tList.append(tr);
            }

            //$("#divScreenLock").hide();

            window.resizeTo(896, 1000);

            $("#hList").show();
            $("#tableList").show();
        };

        var fn_ControlBind_Img = function (json, bEnabled, typeCd) {
            var tdImg;
            var divImgList;

            if (typeCd == 'A') {
                tdImg = $("#<%= tdActImg.ClientID %>");
                divImgList = $("#divActImg");
            }
            else {
                tdImg = $("#tdReqImg");
            }

            tdImg.empty();

            $.each(JSON.parse(json), function (idx, item) {
                var img = document.createElement("img");
                var imgData = fn_ArrayToBase64(item.IMGDATA);

                img.setAttribute("id", "img" + item.IMG_NO);
                img.src = "data:" + item.IMGTYPE + "; base64," + imgData;
                img.style.maxWidth = "100%";

                tdImg.append(img);

                if (typeCd == 'A') {
                    var p = document.createElement("p");
                    p.className = "inpbox";
                    p.style.display = "block";
                    p.style.textAlign = "left";

                    var label = document.createElement("label");
                    label.setAttribute("id", "lblImg" + item.IMG_NO);
                    label.setAttribute("name", "lblReqFileNm");
                    label.style.height = "40px";
                    label.style.paddingTop = "10px";
                    label.style.maxWidth = "80%";
                    label.innerText = item.IMGNAME;
                
                    p.append(label);

                    if (bEnabled) {
                        var button = document.createElement("button");
                        button.type = "button";
                        button.setAttribute("id", "btnImgDel" + item.IMG_NO);
                        button.setAttribute("onclick", "javascript:fn_DelFileCheck(" + item.IMG_NO + ", 'A');");
                        button.style.width = "30px";
                        button.style.height = "30px";
                        button.style.lineHeight = "0";
                        button.style.color = "black";
                        button.style.padding = "0";
                        button.style.margin = "0";
                        button.style.border = "none";
                        button.innerText = "X";

                        p.append(button);
                    }

                    divImgList.append(p);
                }
            });
        };

        var fn_ArrayToBase64 = function (buffer) {

            var binary = '';

            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;

            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }

            return window.btoa(binary);
        };

        var fn_radioBoxChk = function (val) {
            $("#" + val).prop('checked', true);
            $("#" + val).change();
        };

        var fn_ParentSearch = function (bClose) {
            opener.fn_Search();

            if (bClose) {
                window.close();
            }
        };

        var fn_RegFileCheck = function (typeCd) {
            if (confirm("참조 파일들 저장 하시겠습니까?")) {
                fn_RegImgData(typeCd);
            }
        };

        var fn_DelFileCheck = function (imgNo, typeCd) {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                fn_DelImgData(imgNo, typeCd);
            }
        };

        var fn_Reload = function (inspection_no) {
            var form;

            form = "/QC/RegInspectionInfoBybComp";

            var param = { pINSPECTION_NO: inspection_no };

            var nWidth = 880;
            var nHeight = 720;

            cmPostPopup(form, "_self", param, nWidth, nHeight);
        };

        var AddFileUpload = function () {
            var cnt1 = $("#filebox").find("input[type='file']").length;
            var cnt2 = $("#divActImg").find("label[name='lblActFileNm']").length;

            if (cnt1 + cnt2 >= 4) {
                return;
            }

            if (!document.getElementById && !document.createElement)
                return;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-right:0;");
            newFileBox.setAttribute("name", "reqfile");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:140px;");
            newFileText.style.marginLeft = "0";

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");
            newFileSpan.style.width = "30px";

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "fileTypeChk('fu" + AddFileUpload.lastAssignedId + "', this.value);");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("accept", "image/*");

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.style.width = "30px";
            newFileLabel.innerText = "...";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fileTypeChk = function (id, val) {
            var filechk = false;

            var extIndex = val.lastIndexOf('.');

            var extList = ["tiff", "pjp", "jfif", "bmp", "gif", "svg", "png", "xbm", "dib", "jxl", "jpeg", "svgz", "jpg", "webp", "ico", "tif", "pjpeg", "avif"];

            if (extIndex > 1) {

                var fileExt = val.substring(extIndex + 1, val.length).toLowerCase();

                if (extList.indexOf(fileExt) > 0) {
                    filechk = true;
                }
            }
            if (filechk) {
                document.getElementById(id + "-value").value = val;
            }
            else {
                alert("이미지파일만 업로드 가능합니다.");

                document.getElementById(id).value = "";
                document.getElementById(id + "-value").value = "";
            }
        };
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong id="stitle">품질검사 조치요청서</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents" style="padding-top: 10px;">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <h4>ktsservice 북부 검사원</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:130px;" />
                                        <col style="width:410px;" />
                                        <col style="width:200px;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">검사일&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line">소속</th>
                                            <th class="th-line last">검사원&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbChkDt" runat="server" Width="120px" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbChkOrgNm" runat="server" Width="380px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbChkEmpNm" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4>검사대상</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:180px;" />
                                        <col style="width:180px;" />
                                        <col style="width:370px;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">업체명&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line">현장대리인&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line last">주요내용</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCompany" runat="server" Width="160px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbPartnersEmpNm" runat="server" Width="160px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbNoti" runat="server" Width="350px"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4>고객정보</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:50%;" />
                                        <col style="width:50%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">회선번호</th>
                                            <th class="th-line last">고객명</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbLineNo" runat="server" Width="100%"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCustNm" runat="server" Width="100%"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4>조치항목 분류</h4>
                                <table>
                                    <colgroup>
                                        <col style="width:33%;" />
                                        <col style="width:33%;" />
                                        <col style="width:33%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line">대분류&nbsp;<span style="color:red;text-align:center;">*</span></th>
                                            <th class="th-line">중분류</th>
                                            <th class="th-line last">소분류</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbLv1Grp" runat="server" Width="95%" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbLv2Grp" runat="server" Width="95%" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbLv3Grp" runat="server" Width="95%" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4 id="hList" style="display:none;">검사목록</h4>
                                <table id="tableList" style="display:none;" >
                                    <thead>
                                        <tr>
                                            <th class="th-line" colspan="2" style="min-width:510px; max-width:510px; width:510px;">항목</th>
                                            <th class="th-line last" style="min-width:275px; max-width:275px; width:275px">결과</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tList">

                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4 id="hReq" style="display:none;">조치요청</h4>
                                <table id="tableReq" style="display:none;">
                                    <colgroup>
                                        <col style="width:100%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line last">요청사항</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="tdReqImg" style="min-height:300px;height:300px;vertical-align:top;white-space:normal">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="inpbox">                                                    
                                                    <asp:TextBox ID="txbReqNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr class="blank" />
                                <h4 id="hAct" style="display:none;">요청사항 조치결과</h4>
                                <table id="tableAct" style="display:none;">
                                    <colgroup>
                                        <col style="width:74%;" />
                                        <col style="width:26%;" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="th-line last" colspan="2">조치결과
                                                <button type="button" id="btnActImg" runat="server" class="btn-imgSave">사진저장</button>
                                                <button type="button" id="btnActAdd" runat="server" class="btn-replysave" style="width: 30px;height:34px;margin-top:-5px;margin-bottom:-5px;margin-right:10px;float:right;">추가</button>
    
                                            </th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="tdActImg" runat="server" style="min-height:300px;height:300px;vertical-align:top;white-space:normal">
                                            </td>
                                            <td id="tdActImgNm" runat="server" style="min-height:300px;height:300px;vertical-align:top;padding:5px 10px 5px;">
                                                <div>
                                                    <div id="filebox">
                                                    </div>                                                    
                                                </div>
                                                <div id="divActImg" style="width:100%;overflow-x:hidden;overflow-y:auto; margin-top:2px; margin-bottom:2px">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p class="inpbox">                                                    
                                                    <asp:TextBox ID="txbActNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="divRegAct" runat="server" style="height:24px; margin-top:3px;text-align:center;" visible="false">
                                    <input id="btnRegAct" runat="server" type="button" class="btn-green" style="float:right" visible="false" value="저장" />
                                </div>
                                <hr class="blank" />
                                <h4 id="hReject" runat="server" visible="false">반려사유</h4>
                                <table id="tableReject" runat="server" visible="false">
                                    <colgroup>
                                        <col style="width:100%;" />
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <p class="inpbox">                                                    
                                                    <asp:TextBox ID="txbReject" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <asp:HiddenField ID="hdfInspection_No" runat="server"/>
                                <asp:HiddenField ID="hdfChkStatus" runat="server"/>
                                <asp:HiddenField ID="hdfActStatus" runat="server"/>
                                <asp:HiddenField ID="hdfResult" runat="server"/>
                                <asp:HiddenField ID="hdfReqImg" runat="server"/>
                                <asp:HiddenField ID="hdfActImg" runat="server"/>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력값입니다.
                        <div class="btncenter">
                            <input id="btnActFin" runat="server" type="button" class="btn-green" visible="false" value="조치완료" />
                            <input id="btnClose" runat="server" type="button" class="btn-gray" value="닫기" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <div id="divScreenLock" class="div-Screenlock"></div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
