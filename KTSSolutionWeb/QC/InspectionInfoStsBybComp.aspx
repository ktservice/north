﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InspectionInfoStsBybComp.aspx.cs" Inherits="KTSSolutionWeb.InspectionInfoStsBybComp" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240624" rel="stylesheet" />    
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .tabulator-row > div {
            height:32px !important;
            padding-top:7px !important;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbEnDt.ClientID %>").change(function (e) {
                    fn_GetTeam();
                });

                $("#<%= ddlLv1Grp.ClientID %>").change(function (e) {
                    var lv1GrpCd = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();
                    $("#ddlLv2Grp").removeAttr("disabled");
                    $("#ddlLv3Grp").removeAttr("disabled");

                    if (lv1GrpCd == "") {
                        $("#ddlLv2Grp").attr("disabled", "disabled");
                        $("#ddlLv3Grp").attr("disabled", "disabled");
                        fn_ControlBind_Lv2Grp(null);
                        fn_ControlBind_Lv3Grp(null);;
                    }
                    else {
                        fn_ControlBind_Lv3Grp(null);;

                        fn_GetLv2Grp(lv1GrpCd);
                    }
                });

                $("#ddlLv2Grp").change(function (e) {
                    var lv2GrpCd = $("#ddlLv2Grp").val();
                    $("#ddlLv3Grp").removeAttr("disabled");

                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = "";
                    options.text = "전체";
                    options.selected = true;

                    if (lv2GrpCd == "") {
                        $("#ddlLv3Grp").attr("disabled", "disabled");
                        fn_ControlBind_Lv3Grp(null);;
                    }
                    else {
                        fn_GetLv3Grp(lv2GrpCd);
                    }
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "조치요구서 현황_협력사.xlsx", { sheetName: "조치요구서 현황_협력사" });

                });
            }
        };

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "조치ID", field: "INSPECTION_ID", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "CREATE_DATE", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "검사일", field: "CHK_DATE", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM_LV1", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM_LV2", sorter: "string", minWidth: 100, maxWidth: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM_LV3", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENM", sorter: "string", minWidth: 100, maxWidth: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GRPNM_LV1", sorter: "string", minWidth: 100, maxWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류", field: "GRPNM_LV2", sorter: "string", minWidth: 100, maxWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "소분류", field: "GRPNM_LV3", sorter: "string", minWidth: 100, maxWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "회선번호", field: "LINENO", sorter: "string", minWidth: 100, maxWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string", minWidth: 100, maxWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "점검상태", field: "CHK_NM", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "조치상태", field: "ACT_NM", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "경과일", field: "ELAPSED_DATE", sorter: "string", minWidth: 100, maxWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "조치일", field: "ACT_DATE", sorter: "string", minWidth: 100, maxWidth: 160 },
                { headerHozAlign: "center", hozAlign: "center", title: "주요내용", field: "NOTE", sorter: "string", minWidth: 100, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "불량내용", field: "FAULTY_NOTE", sorter: "string", minWidth: 100, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "조치요구서번호", field: "INSPECTION_NO", sorter: "string", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            var rowDoubleClick = function (e, row) {
                var rowData = row.getData();
                var inspection_no = rowData.INSPECTION_NO;

                fn_PopupRegInspection(inspection_no);
            };

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("sField");
            var ValueEl = document.getElementById("sValue");

            $("#sField").change(function (e) {
                ValueEl.value = '';

                ktsGrid.setFilter(FieldEl.value, 'like', '');

                if (FieldEl.value == "") {
                    $("#sValue").attr('disabled', 'disabled');
                }
                else {
                    $("#sValue").removeAttr('disabled');
                }
            });

            $("#sValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $(".table-search").css('display', 'none');
        };

        var fn_Load = function () {
        
        };

        var fn_GetLv2Grp = function (lv1GrpCd) {
            if (lv1GrpCd != "") {

                var aData = [];

                aData[0] = lv1GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv2GrpList');

                callGb = 'GetLv2Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetLv3Grp = function (lv2GrpCd) {

            if (lv2GrpCd != "") {

                var aData = [];

                aData[0] = lv2GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv3GrpList');

                callGb = 'GetLv3Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var successCallBack = function (json) {
            if (callGb == 'GetLv2Grp') {
                callGb = '';

                fn_ControlBind_Lv2Grp(json);
            }
            else if (callGb == 'GetLv3Grp') {
                callGb = '';

                fn_ControlBind_Lv3Grp(json);
            }
        };

        var fn_Search = function () {
            var StDate = $("#<%= txbStDt.ClientID %>").val();
            var EnDate = $("#<%= txbEnDt.ClientID %>").val();

            if (StDate.length == 0 || EnDate.length == 0) {
                alert("등록일자를 선택해주세요.");
            } else {
                var ActCd = $("#ddlActCode option:selected").val();
                var GrpCd_Lv1 = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();
                var GrpCd_Lv2 = $("#ddlLv2Grp option:selected").val();
                var GrpCd_Lv3 = $("#ddlLv3Grp option:selected").val();
                var SearchType = $("#ddlSearchType option:selected").val();

                //입력 파라미터
                var aData = [];
                aData[0] = StDate;
                aData[1] = EnDate;
                aData[2] = ActCd;
                aData[3] = GrpCd_Lv1;
                aData[4] = GrpCd_Lv2;
                aData[5] = GrpCd_Lv3;
                aData[6] = SearchType;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        };

        var fn_PopupRegInspection = function (inspection_no) {
            var form;

            form = "/QC/RegInspectionInfoBybComp";
            var target = "RegInspectionInfoBybComp";

            var param = { pINSPECTION_NO: inspection_no };

            var nWidth = 880;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_ControlBind_Lv2Grp = function (json) {
            var selector = $("#ddlLv2Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "전체";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV2GRPCD;
                options.text = item.LV2GRPNM;

                if (item.LV2GRPNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });
        };

        var fn_ControlBind_Lv3Grp = function (json) {
            var selector = $("#ddlLv3Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "전체";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV3GRPCD;
                options.text = item.LV3GRPNM;

                if (item.LV3GRPNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });
        };
    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
				<span class="optionbox first">
                    <select id="ddlSearchType" style="width:100px;font-weight:bold;" >
                        <option value="CHK_DATE" selected="selected" style="font-weight: bold">검사일자</option>
                        <option value="ACT_DATE" style="font-weight: bold">조치일자</option>
                        <option value="SUCC_DATE" style="font-weight: bold">완료일자</option>
                    </select>
                </span>
                <span class="inpbox ">
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span>
				<span class="optionbox" style="margin-right:20px">
					<label>조치상태</label> 
                    <select id="ddlActCode" style="width:110px;" >
                        <option value="" selected="selected">전체</option>
                        <option value="U">미발행</option>
                        <option value="R">조치대기중</option>
                        <option value="T">등록중</option>
                        <option value="F">조치완료</option>
                    </select>
                </span>
                <hr class="blank" />
				<span class="optionbox" style="margin-right:20px">
					<label>대분류</label> 
                    <asp:DropDownList ID="ddlLv1Grp" style="width:200px;" runat="server">
                    </asp:DropDownList>
                </span>
				<span class="optionbox" style="margin-right:20px">
					<label>중분류</label> 
                    <select id="ddlLv2Grp" style="width:200px;" disabled="disabled" >
                        <option value="">전체</option>
                    </select>
                </span>
				<span class="optionbox" style="margin-right:20px">
					<label>소분류</label> 
                    <select id="ddlLv3Grp" style="width:200px;" disabled="disabled" >
                        <option value="">전체</option>
                    </select>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조치요구서 현황</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="sField" style="font-weight:bold">
                            <option value="">선택</option>
                            <option value="LINENO">회선번호</option>
                            <option value="CUSTNM">고객명</option>
                        </select>
                         :
                        <input id="sValue" type="text" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
