﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolCodePopup.aspx.cs" Inherits="KTSSolutionWeb.ToolCodePopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>공기구코드정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var chkItemCd = '';
        var chkItemNm = '';
        var gubun;
        var rtn;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });

                $("#btnConfirm").click(function (e) {
                    SendParentsForm(chkItemCd, chkItemNm);
                });

                $("#txbOrgNm").keydown(function (e) {
                    if (e.keyCode == 13) {
                        fn_Search();
                        return false;
                    }
                });

                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            gubun = $("#<%= hdnGubun.ClientID %>").val();

            if (gubun == "1") {
                $("#btnConfirm").show();
                $("#btnClose").hide();
            }
            else {
                $("#btnConfirm").hide();
                $("#btnClose").show();
            }
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_checkBox = function (cell, formatterParams, onRendered) { //plain text value
            cellNo = cell.getRow().getData().id;

            chkItemCd = '';
            chkItemNm = '';

            var checkbox = document.createElement("input");
            checkbox.type = 'checkbox';
            checkbox.name = 'chk';

            checkbox.addEventListener("change", function (e) {
                var rows = ktsGrid.getRows();
                rows.forEach(function (row) {
                    row.deselect();
                });

                cell.getRow().select();
                $('input[type="checkbox"][name="chk"]').prop('checked', false);
                $(this).prop('checked', true);

                chkItemCd = cell.getRow().getData().id;
                chkItemNm = cell.getRow().getData().ITEMNM;

            });

            return checkbox;
        };

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            if (gubun == "1") {
                Columns = [
                    { formatter: fn_checkBox, hozAlign: "center", headerSort: false, width: 30, frozen: true },//, titleFormatter: "rowSelection" : 전체 선택 기능 추가 시
                    { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "id", sorter: "number", width: 100, frozen: true },
                    { headerHozAlign: "center", hozAlign: "left", title: "품목명", field: "ITEMNM", sorter: "string", width: 200, frozen: true },
                    { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80 },
                    { headerHozAlign: "center", hozAlign: "center", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 90 },
                    { headerHozAlign: "center", hozAlign: "right", title: "지급수량", field: "GIVECNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    { headerHozAlign: "center", hozAlign: "left", title: "규격", field: "STANDARD", sorter: "string", width: 200 }
                ];
            }
            else if (gubun == "2") {
                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "id", sorter: "number", width: 100, frozen: true },
                    { headerHozAlign: "center", hozAlign: "left", title: "품목명", field: "ITEMNM", sorter: "string", width: 200, frozen: true },
                    { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80 },
                    { headerHozAlign: "center", hozAlign: "center", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 90 },
                    { headerHozAlign: "center", hozAlign: "right", title: "지급수량", field: "GIVECNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    { headerHozAlign: "center", hozAlign: "left", title: "규격", field: "STANDARD", sorter: "string", width: 200 }
                ];
            }

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("62vh");

        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_Search = function () {
            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm(itemCd, itemNm) {

            if (gubun == "1") {
                if (itemCd == '') {
                    ktsGridAlert('체크 박스를 선택 후 진행하세요.');
                    return;
                }
                else {
                    opener.SetDispItemCd(itemCd, itemNm);
                }
            }

            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">공기구코드정보</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <input type="hidden" id="hdnGubun" runat="server" />
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>조회 결과</strong>
                        <label class="searchrowcount"></label>
			            <div class="pull-right">
				            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				            </div>
			            </div>
                    </div>
                    <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none;">
                                <span>
                                    <select id="selField">
                                        <option value="">선택</option>
                                        <option value="id">품목코드</option>
                                        <option value="ITEMNM">품목명</option>
                                    </select> : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnConfirm" type="button" class="btn-green last" value="선택" />
                    </div>
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>