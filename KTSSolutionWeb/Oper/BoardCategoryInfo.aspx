﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BoardCategoryInfo.aspx.cs" Inherits="KTSSolutionWeb.BoardCategoryInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
            },
            eventbind: function () {
            }
        };

        var fn_SaveChk = function () {
            if (confirm("항목 정보를 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        };

        var fn_DeleteChk = function () {
            if (confirm("항목 정보를 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
    <div class="contentsarea">
    </div>
    <div class="datalist">
        <div class="list-top">
            <strong>기술공유 항목 관리</strong>
            <label class="searchrowcount"></label>
        </div>
        <div class="scrollbox">
            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <thead>
                            <tr>
                                <th>순번</th>
                                <th>항목명</th>
                                <th>항목순서</th>
                                <th>사용여부</th>
                                <th>저장/삭제</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptResult" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblBoardType" runat="server" Text='<%# Eval("BOARDTYPE") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <div class="txtbox">
                                                <asp:TextBox id="txbCategory" runat="server" Width="100%" Height="40px" MaxLength="50" Text='<%# Eval("CATEGORY") %>'></asp:TextBox>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOrder" runat="server" Visible="false" Text='<%# Eval("CATEGORYORDER") %>'></asp:Label>
                                            <span class="optionbox">
                                                <asp:DropDownList ID="ddlOrder" runat="server" Width="80px">
                                                </asp:DropDownList>
                                            </span>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblUseYn" runat="server" Visible="false" Text='<%# Eval("USEYN") %>'></asp:Label>
                                            <asp:CheckBox ID="chkUseYn" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button id="btnSave" runat="server" OnClientClick="return fn_SaveChk();" OnClick="btnSave_Click" class="btn-save" Text="저장" />
                                            <asp:Button id="btnDelete" runat="server" OnClientClick="return fn_DeleteChk();" OnClick="btnDelete_Click" Visible="false" class="btn-del" Text="삭제" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="margin-bottom: 20px;"></div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>
