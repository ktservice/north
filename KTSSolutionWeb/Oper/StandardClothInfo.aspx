﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StandardClothInfo.aspx.cs" Inherits="KTSSolutionWeb.StandardClothInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function SearchChk() {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();

            if (years.length == 0) {
                alert("연도를 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SaveChk() {
            if (confirm("저장하시겠습니까?")) {
                return true;
            } else {
                return false;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
                <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox">
                            <label>연도</label>
                            <asp:DropDownList ID="ddlYears" runat="server" Width="150px"></asp:DropDownList>
			            </span>
                        <div class="btnSearchWrap">
                            <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" Text="조회" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <!-- S:leftbox -->
            <div class="leftbox">    
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>기준년도</strong>
                        <div class="pull-right">
                            <div class="btnset">
                                <asp:Button id="btnExcel1" runat="server" class="btn-green" OnClientClick="return SearchChk();"  OnClick="btnExcel1_ServerClick" Text="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->
    
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th rowspan="2" colspan="3">구분</th>
                                            <th colspan="5" style="border-right:solid 1px;">작업복</th>
                                            <th colspan="4">방한복</th>
                                        </tr>
                                        <tr>
                                            <th style="width:50px">점퍼</th>
                                            <th style="width:50px">조끼</th>
                                            <th style="width:50px">티셔츠</th>
                                            <th style="width:50px">바지</th>
                                            <th style="width:50px;border-right:solid 1px;">모자</th>
                                            
                                            <th style="width:50px">외피</th>
                                            <th style="width:50px">내피</th>
                                            <th style="width:50px">방한바지</th>
                                            <th style="width:50px">버프</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult1" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width:50px"><%# Eval("JOBNM") %></td>
                                                    <td style="width:50px"><%# Eval("PROVIDETYPE") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHGBN") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE1") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE2") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE3") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE4") %></td>
                                                    <td style="width:50px;border-right:solid 1px;"><%# Eval("CLOTHTYPE5") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE6") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE7") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE8") %></td>
                                                    <td style="width:50px"><%# Eval("CLOTHTYPE9") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>    
                    <!-- E:scrollbox -->
                </div>
            </div>
            <!-- E:leftbox -->
            <!-- S:rightbox -->
            <div class="rightbox">
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>연도별 등록정보</strong>
                        <div class="pull-right">
                            <div class="btnset">
                                <asp:Button id="btnSave" runat="server" class="btn-green" OnClientClick="return SaveChk();"  OnClick="btnSave_Click" Text="저장" />
                                <asp:Button id="btnExcel2" runat="server" class="btn-green" OnClientClick="return SearchChk();"  OnClick="btnExcel2_ServerClick" Text="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->                    
                        
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <asp:UpdatePanel ID="updPanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th rowspan="2" colspan="3">구분</th>
                                            <th colspan="5" style="border-right:solid 1px;">작업복</th>
                                            <th colspan="4">방한복</th>
                                        </tr>
                                        <tr>
                                            <th style="width:50px">점퍼</th>
                                            <th style="width:50px">조끼</th>
                                            <th style="width:50px">티셔츠</th>
                                            <th style="width:50px">바지</th>
                                            <th style="width:50px;border-right:solid 1px;">모자</th>
                                            
                                            <th style="width:50px">외피</th>
                                            <th style="width:50px">내피</th>
                                            <th style="width:50px">방한바지</th>
                                            <th style="width:50px">버프</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult2" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width:50px">
                                                        <asp:Label ID="lblYears" runat="server" Text='<%# Eval("YEARS") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblJobNm" runat="server" Text='<%# Eval("JOBNM") %>'></asp:Label>
                                                    </td>
                                                    <td style="width:50px">
                                                        <asp:Label ID="lblProvideType" runat="server" Text='<%# Eval("PROVIDETYPE") %>'></asp:Label>
                                                    </td>
                                                    <td style="width:50px">
                                                        <asp:Label ID="lblClothGbn" runat="server" Text='<%# Eval("CLOTHGBN") %>'></asp:Label>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType1" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE1") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType2" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE2") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType3" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE3") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType4" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE4") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px;border-right:solid 1px;">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType5" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE5") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType6" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE6") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType7" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE7") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType8" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE8") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td style="width:50px">
                                                        <div class="txtbox">
                                                            <asp:TextBox ID="txbClothType9" runat="server"  MaxLength="10" style="width:50px;text-align:center;" onkeypress="return OnlyNumber2(event, this);" onkeydown="ReplaceKorean(this);" Text='<%# Eval("CLOTHTYPE9") %>'></asp:TextBox>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <!-- E:scrollbox -->
                </div>
                <!-- E:datalist -->
            </div>                
            <!-- E:rightbox -->
        </div>
        <!-- E:multiplebox -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>