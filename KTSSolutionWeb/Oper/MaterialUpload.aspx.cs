﻿using System;
using System.Web.UI;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Web;
using System.Configuration;
using System.Text.RegularExpressions;

namespace KTSSolutionWeb
{
    public partial class MaterialUpload : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnUpload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HttpFileCollection uploadFiles = Request.Files;

                string strUploadPath = ConfigurationSettings.AppSettings["MaterialPath"].ToString();
                string strFailFileName = "";

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];

                    if (postedFIle.ContentLength == 0)
                        continue;

                    string strUploadType = Regex.Replace(uploadFiles.AllKeys[i].ToString(), "[^a-zA-Zㄱ-힗]+", "");
                    string strFilePath = "";
                    string strDir = "";

                    if (strUploadType.Equals("fuMaster"))
                    {
                        strDir = "Master";
                    }
                    else if (strUploadType.Equals("fuUseWorker"))
                    {
                        strDir = "UseWorker";
                    }
                    else if (strUploadType.Equals("fuOut"))
                    {
                        strDir = "Out";
                    }
                    else if (strUploadType.Equals("fuTerminal"))
                    {
                        strDir = "Terminal";
                    }

                    strFilePath = strUploadPath + "\\" + strDir;

                    DirectoryInfo di = new DirectoryInfo(strFilePath);

                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    FileInfo[] xlFiles = di.GetFiles("*.xlsx", SearchOption.TopDirectoryOnly);

                    foreach (FileInfo fi in xlFiles)
                    {
                        if (fi.Exists)
                        {
                            bool bFileRun = false;

                            using (FileUtil fileUtil = new FileUtil())
                            {
                                bFileRun = fileUtil.IsFileRun(fi.FullName);
                            }

                            if (bFileRun)
                            {
                                if (strFailFileName.Length > 0)
                                    strFailFileName += ", ";

                                strFailFileName += fi.Name;

                                continue;
                            }
                            else
                            {
                                fi.Delete();
                            }
                        }
                    }
                    //FileInfo fi = new FileInfo(strUploadPath + "\\" + Path.GetFileName(postedFIle.FileName));

                    if (strFailFileName.Length > 0)
                    {
                        continue;
                    }

                    postedFIle.SaveAs(strFilePath + "\\" + Path.GetFileName(postedFIle.FileName));
                }

                string strMsg = "";

                if (strFailFileName.Length > 0)
                {
                    strMsg = strFailFileName + " 파일은 이미 업로드 진행중이므로 업로드가 불가합니다. 잠시후 다시 시도해주세요.";
                }
                else
                {
                    strMsg = "파일 업로드가 완료되었습니다. 업로드된 데이터가 반영되기까지는 시간이 걸릴 수 있습니다.";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "UploadOk", "alert('" + strMsg + "')", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('파일 업로드 중 오류가 발생했습니다.');", true);
            }

        }
    }
}