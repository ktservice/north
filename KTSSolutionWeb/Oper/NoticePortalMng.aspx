﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NoticePortalMng.aspx.cs" Inherits="KTSSolutionWeb.NoticePortalMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .line {
            border-bottom: 2px solid #00000082;
         }

        .searchbox {
            margin-top:0px !important;
            height:92px !important;
        }
        .tabcontent {
          display: inline;
        }

        .inpbox input {
            display: inline-block;
            height: 40px;
            width: 100%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 38px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }


        .inpbox textarea {
            display: inline-block;
            height: 100%;
            width: 100%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 38px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }

        .searchbox {
            height:150px;
        }

        .plusicon {
            background:#fff url('/Resource/Mobile_images/icon_plus_01.png')no-repeat 50%;
            width:11px;
            height:11px;
        }
        .btn-gray
        {
            min-width:60px;
            padding:0px;
        }

        .classReg {
            background:#ccd3cd;
            position:absolute;
        }
        
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var startDt = '';
        var endDt = '';
        var noticeUse = 'N';
        var noticeTitle = '';
        var noticeContent = '';
        var moduleCd = '';
        var moduleNm = '';
        var classCd = '';
        var classNm = '';

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');


                    if (this.text === 'MOT실적관리') {
                        fn_NoticeInfo('MOTR');
                    }
                    else if (this.text === 'MOT정산관리') {
                        fn_NoticeInfo('MOTJ');
                    }
                    else if (this.text === '판매정책관리') {
                        fn_SalesClass('selClass');
                    }
                    else {
                        fn_NoticeInfo('MOTR');
                    }

                    return false;
                }).filter(':eq(0)').click();

                $("#selClass").change(function (e) {

                    if (this.value === 'all') {
                        $("#btnClassReg").val('분류등록');
                    }
                    else {
                        $("#btnClassReg").val('분류편집');
                    }

                    fn_SalesSearch(this.value);
                });

                $("#btnClassReg").click(function (e) {
                    if (this.value === '분류편집') {
                        $("#lblClassTitle").text('분류편집');

                        $("#txtClassNm").val($("#selClass option:selected").text());
                        $("#txtClassCd").val($("#selClass").val());

                        $("#txtClassCd").attr('readonly', true);
                        $("#txtClassCd").attr('style', 'background: #d1d1d1');

                        $("#btnClassSave").hide();
                        $("#btnClassMod").show();
                        $("#btnClassDel").show();
                    }
                    else {
                        $("#lblClassTitle").text('분류등록');

                        $("#txtClassNm").val('');
                        $("#txtClassCd").val('');

                        $("#txtClassCd").attr('readonly', false);
                        $("#txtClassCd").attr('style', 'background: #fff');

                        $("#btnClassSave").show();
                        $("#btnClassMod").hide();
                        $("#btnClassDel").hide();
                    }

                    $(".classReg").offset({ top: $('.line').offset().top });
                    $(".classReg").show();
                });

                $("#btnReg").click(function (e) {
                    fn_SalesPopupReg('I','');
                });

                $("#btnClassSave").click(function (e) {
                    fn_ClassRegTran();
                });

                $("#btnClassMod").click(function (e) {
                    fn_ClassModTran();
                });

                $("#btnClassDel").click(function (e) {
                    fn_ClassDelTran();
                });

                $("#btnClassClose").click(function (e) {
                    $(".classReg").hide();
                });

                $("#btnSave2").click(function (e) {
                    fn_ModTran('MOTR');
                });

                $("#btnSave3").click(function (e) {
                    fn_ModTran('MOTJ');
                });
                
            }
        };


        var fn_Load = function () {
            
        }

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            $("#txtStartDt2").attr('readonly', true);
            $("#txtEndDt2").attr('readonly', true);
            $("#txtStartDt3").attr('readonly', true);
            $("#txtEndDt3").attr('readonly', true);

            $(".classReg").hide();
        }

        var fn_NoticeInfo = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];
            aData[0] = gb;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetNoticeInfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellFunction = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnGridDel' type='button' onclick='fn_SalesDelTran(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnGridMo' type='button' onclick='fn_SalesPopupReg(\"M\",\"" + cellNo + "\");' value='보기' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "공지ID", field: "NOTICEID", sorter: "string", width: 100, visible:false },
                { headerHozAlign: "center", hozAlign: "center", title: "분류코드", field: "CLASSCD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "분류명", field: "CLASSNM", sorter: "string", width: 180 },
                { headerHozAlign: "center", hozAlign: "center", title: "이미지파일명", field: "FILENM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "REGDT", sorter: "string", width: 140 },
                { formatter: fn_CellFunction, width: 220, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("46vh");
        }

        var fn_SalesFileDelTran = function (id) {
            if (confirm("파일삭제를 하시겠습니까?")) {
                callGb = 'SalesDelTran';

                fn_SalesTransaction('F', id);
            }
        }

        var fn_SalesDelTran = function (id) {
            if (confirm("삭제를 하시겠습니까?")) {
                callGb = 'SalesDelTran';

                fn_SalesTransaction('D', id);
            }
        }

        var fn_SalesPopupReg = function (gubun, id) {
            var classCd = '';

            if (gubun === 'M') {
                var row = ktsGrid.getRow(id);
                var rowData = row.getData();

                classCd = rowData.CLASSCD;

                if (classCd === null) classCd = '';
            }

            var form = "/Oper/PortalSalesPolicyPopup";
            var target = "PortalSalesPolicyPopup";
            var param = {
                pGubun: gubun,
                pClassCd: classCd
            };

            var nWidth = 720;
            var nHeight = 700;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_ModTran = function (gubun) {
            if (fn_RegChk(gubun)) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }


        var fn_ClassRegTran = function () {
            if (fn_ClassRegChk()) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'ClassRegTran';

                    fn_ClassTransaction('I');
                }
            }
        }

        var fn_ClassModTran = function () {
            if (fn_ClassRegChk()) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ClassModTran';

                    fn_ClassTransaction('M');
                }
            }
        }

        var fn_ClassDelTran = function () {
            if (confirm("삭제를 진행하시겠습니까?")) {
                callGb = 'ClassDelTran';

                classCd = $("#txtClassCd").val();
                classNm = $("#txtClassNm").val();

                fn_ClassTransaction('D');
            }
        }

        var fn_RegChk = function (gubun) {
            startDt = '';
            endDt = '';
            noticeUse = 'N';
            noticeTitle = '';
            noticeContent = '';
            moduleCd = '';
            moduleNm = '';

            if (gubun === 'MOTR') {
                startDt = $("#txtStartDt2").val();
                endDt = $("#txtEndDt2").val();
                noticeTitle = $("#txtNoticeTitle2").val();
                noticeContent = $("#txtNoticeContent2").val();

                if ($('input:checkbox[id="chkNoticeUse2"]').is(":checked") == true) {
                    noticeUse = 'Y';
                }

                moduleCd = $("#hdnModuleCd2").val();
                moduleNm = $("#hdnModuleNm2").val();
            }
            else if (gubun === 'MOTJ') {
                startDt = $("#txtStartDt3").val();
                endDt = $("#txtEndDt3").val();
                noticeTitle = $("#txtNoticeTitle3").val();
                noticeContent = $("#txtNoticeContent3").val();

                if ($('input:checkbox[id="chkNoticeUse3"]').is(":checked") == true) {
                    noticeUse = 'Y';
                }

                moduleCd = $("#hdnModuleCd3").val();
                moduleNm = $("#hdnModuleNm3").val();
            }

            if (startDt.length == 0) {
                ktsGridAlert("시작일을 입력하세요.");
                return false;
            }
            if (endDt.length == 0) {
                ktsGridAlert("종료일을 입력하세요.");
                return false;
            }
            if (noticeTitle.length == 0) {
                ktsGridAlert("제목을 입력하세요.");
                return false;
            }
            if (noticeContent.length == 0) {
                ktsGridAlert("내용을 입력하세요.");
                return false;
            }

            return true;
        }


        var fn_ClassRegChk = function () {
            classCd = $("#txtClassCd").val();
            classNm = $("#txtClassNm").val();

            if (classNm.length == 0) {
                ktsGridAlert("분류명을 입력하세요.");
                return false;
            }
            if (classNm.length == 0) {
                ktsGridAlert("분류코드를 입력하세요.");
                return false;
            }

            return true;
        }

        var fn_SalesClass = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetSalesClass');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_SalesSearch = function (classCd) {
            //입력 파라미터
            var aData = [];
            aData[0] = classCd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetSalesGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        var fn_SalesTransaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var classCd = rowData.CLASSCD;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = classCd;
            aData[2] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetSalesTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);

        }

        var fn_ClassTransaction = function (gubun) {
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = classCd;
            aData[2] = classNm;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetClassTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun) {

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = moduleCd;
            aData[2] = moduleNm;
            aData[3] = noticeTitle;
            aData[4] = noticeContent;
            aData[5] = startDt;
            aData[6] = endDt;
            aData[7] = noticeUse;


            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetNoticeInfoTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {

            if (callGb == 'MOTR') {
                var useYN = 'N';

                $.each(JSON.parse(json), function (idx, item) {
                    $('#txtStartDt2').val(item.STDT);
                    $('#txtEndDt2').val(item.ENDT);
                    $('#txtNoticeTitle2').val(item.TITLE);
                    $('#txtNoticeContent2').val(item.CONTENTS);
                    $('#hdnModuleCd2').val(item.MODULECD);
                    $('#hdnModuleNm2').val(item.MODULENM);
                    useYN = item.USEYN;
                });

                if (useYN === 'Y') {
                    $("input:checkbox[id='chkNoticeUse2']").prop("checked", true);
                }
                else {
                    $("input:checkbox[id='chkNoticeUse2']").prop("checked", false);
                }
            }
            else if (callGb == 'MOTJ') {
                var useYN = 'N';

                $.each(JSON.parse(json), function (idx, item) {
                    $('#txtStartDt3').val(item.STDT);
                    $('#txtEndDt3').val(item.ENDT);
                    $('#txtNoticeTitle3').val(item.TITLE);
                    $('#txtNoticeContent3').val(item.CONTENTS);
                    $('#hdnModuleCd3').val(item.MODULECD);
                    $('#hdnModuleNm3').val(item.MODULENM);
                    useYN = item.USEYN;
                });

                if (useYN === 'Y') {
                    $("input:checkbox[id='chkNoticeUse3']").prop("checked", true);
                }
                else {
                    $("input:checkbox[id='chkNoticeUse3']").prop("checked", false);
                }
            }
            else if (callGb == 'SALEP') {
                var useYN = 'N';
            }
            else if (callGb == 'selClass') {

                obj = $('#' + callGb);
                obj.empty();

                obj.append("<option value='all'>전체</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CLASSCD + "'>" + item.CLASSNM + "</option>");
                });

                obj.val(obj.val()).trigger('change');
            }
            else if (callGb == 'ClassRegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');
                    }
                });

                callGb = '';

                $(".classReg").hide();
                fn_SalesClass('selClass');
            }
            else if (callGb == 'ClassModTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('변경되었습니다.');
                    }
                });

                callGb = '';

                $(".classReg").hide();
                fn_SalesClass('selClass');
            }
            else if (callGb == 'ClassDelTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('삭제되었습니다.');
                    }
                });

                callGb = '';

                $(".classReg").hide();
                fn_SalesClass('selClass');
            }
            else if (callGb == 'ModTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        alert('변경되었습니다');
                    }
                });

                callGb = '';
            }
            else if (callGb == 'SalesDelTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('삭제되었습니다');
                    }
                });

                callGb = '';

                fn_SalesSearch($('#selClass').val());
            }
            
        }

        var SetDispItem = function (check) {

            if (check == 'ok') {
                fn_SalesSearch($('#selClass').val());
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
    <div class="contentsarea">
        <ul class="tabnav">
            <li><a href="#tab01">MOT실적관리</a></li>
            <li><a href="#tab02">MOT정산관리</a></li>
            <li><a href="#tab03">판매정책관리</a></li>
        </ul>
    </div>
    <div class="datalist">
        <div class="tabcontent">
            <div id="tab01">
                <div class="list-top">
                    <strong>MOT실적관리</strong>
                    <label class="histrowcount"></label>
                </div>
                <div class="scrollbox">
                    <table>
                        <tbody>
                            <tr>
                                <th style="width: 120px;">공지사항 기간</th>
                                <td class="td-left">
                                    <p class="inpbox" style="width:120px">
                                        <input id="txtStartDt2" type="text" class="date" />
                                    </p>
                                    ~
                                    <p class="inpbox" style="width:120px">
                                        <input id="txtEndDt2" type="text" class="date" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 사용</th>
                                <td class="td-left">
                                    <p class="ickbox">
                                        <label class="checkbox" for="chkNoticeUse1">
                                            사용
                                        </label>
                                        <input id="chkNoticeUse2" type="checkbox" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 제목</th>
                                <td class="td-left">
                                    <p class="inpbox">
                                        <input id="txtNoticeTitle2" type="text" maxlength="50" placeholder="공지 제목 입력" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 내용</th>
                                <td class="td-left">
                                    <p class="inpbox">
                                        <textarea id="txtNoticeContent2" rows="5" maxlength="2665" placeholder="공지 내용 입력"></textarea>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" id="hdnModuleCd2" />
                                    <input type="hidden" id="hdnModuleNm2" />
                                    <input id="btnSave2" type="button" class="btn-save" value="저장" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="tab02">
                <div class="list-top">
                    <strong>MOT정산관리</strong>
                    <label class="histrowcount"></label>
                </div>
                <div class="scrollbox">
                    <table>
                        <tbody>
                            <tr>
                                <th style="width: 120px;">공지사항 기간</th>
                                <td class="td-left">
                                    <p class="inpbox" style="width:120px">
                                        <input id="txtStartDt3" type="text" class="date" />
                                    </p>
                                    ~
                                    <p class="inpbox" style="width:120px">
                                        <input id="txtEndDt3" type="text" class="date" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 사용</th>
                                <td class="td-left">
                                    <p class="ickbox">
                                        <label class="checkbox" for="chkNoticeUse1">
                                            사용
                                        </label>
                                        <input id="chkNoticeUse3" type="checkbox" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 제목</th>
                                <td class="td-left">
                                    <p class="inpbox">
                                        <input id="txtNoticeTitle3" type="text" maxlength="50" placeholder="공지 제목 입력" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 내용</th>
                                <td class="td-left">
                                    <p class="inpbox">
                                        <textarea id="txtNoticeContent3" rows="5" maxlength="2665" placeholder="공지 내용 입력"></textarea>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" id="hdnModuleCd3" />
                                    <input type="hidden" id="hdnModuleNm3" />
                                    <input id="btnSave3" type="button" class="btn-save" value="저장" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="tab03">
                <div class="list-top">
                    <strong>판매실적관리</strong>
                    <label class="searchrowcount"></label>
                </div>
                <div class="line"></div>
                <div class="scrollbox">
                    <div class="searchbox">
                        <fieldset>
                            <span class="optionbox">
                                <label>분류</label>
                                <select id="selClass">
                                    <option selected="selected" value="all">전체</option>
                                </select>
                            </span>
                            <input id="btnClassReg" type="button" class="btn-green last" value="분류등록" />
                            <input id="btnReg" type="button" class="btn-green last" style="float: right;" value="등록" />
                        </fieldset>
                    </div>
                    <div style="margin-bottom: 10px;"></div>
                    <div id="ktsGrid"></div>
                </div>

                <div class="classReg">
                    <table>
                        <tbody>
                            <tr>
                                <th colspan="4" style="background: #429487;">
                                    <p class="inpbox">
                                        <label id="lblClassTitle" style="color:#fff">편집</label>
                                    </p>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 80px;">분류명</th>
                                <td class="td-left">
                                    <p class="inpbox" style="width:200px">
                                        <input id="txtClassNm" type="text" maxlength="60" />
                                    </p>
                                </td>
                                <th style="width: 80px;">분류코드</th>
                                <td class="td-left">
                                    <p class="inpbox" style="width:120px">
                                        <input id="txtClassCd" type="text" maxlength="10" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <input id="btnClassSave" type="button" class="btn-save" value="저장" />
                                    <input id="btnClassMod" type="button" class="btn-save" value="변경" />
                                    <input id="btnClassDel" type="button" class="btn-del" value="삭제" />
                                    <input id="btnClassClose" type="button" class="btn-gray" value="닫기" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
        <div style="margin-bottom: 20px;"></div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>