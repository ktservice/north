﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegInspectionList.aspx.cs" Inherits="KTSSolutionWeb.RegInspectionList"  EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title id="title" runat="server">점검항목 관리</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240624" rel="stylesheet" />

    <style>
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .th-line {width: 100px;border-right: 1px solid #dfdfdf;text-align:left;}
        .div-Screenlock {position: absolute;top:0px;width: 100%;height: 100%;background-color: #ffffff !important;z-index: 99;opacity: 0.7;overflow: hidden;display:none}
        .div-modal {position: absolute;width: 90%; height: 95px;top: 30%;left: 5%; background: rgb(255, 255, 255); border-radius: 10px; border: 1px solid gray;z-index:100;}
        .div-modal-inner {width:90%;text-align:center;margin-left:5%;}
        .div-modal-content {margin-top:30px;padding-bottom:5px;float:left;}
        .div-modal-content > p {width: 70px; height: 30px; padding-top: 5px;float: left;font-weight:bold;}
        .div-modal-content > input[type="text"] {width:320px;height:30px;padding-left:5px;padding-right:5px;}
        .btn-add {display: inline-block;width: 40px;height: 30px;margin-top:30px;background: #fff;border: 1px solid #2CBBB7;border-radius: 2px;color: #2CBBB7;font-size: 14px;}
        .btn-close {display: inline-block;width: 40px;height: 30px;margin-top:30px;background: #fff;border: 1px solid gray;border-radius: 2px;color: gray;font-size: 14px;}
        .inpbox > textarea {width: 100%;height: 100px;border: 1px solid #dfdfdf;padding: 5px;}
    </style>
    <script type="text/javascript">
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        $(window).bind("beforeunload", function (e) {
            opener.fn_SearchChkList();
        });

        var page = {
            init: function () {
                fn_SetControls();
            },
            eventbind: function () {

                $("#btnSave").click(function (e) {
                    fn_RegChkList();
                });
                
                $("#btnClose").click(function (e) {
                    window.close();
                });
            } 
        };

        var fn_Load = function () {
        }

        var fn_SetControls = function () {
            fn_GetLv1Grp();
        };

        var fn_GetLv1Grp = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv1GrpList');

            callGb = 'GetLv1Grp';

            ajaxCall(jsonData, ajaxUrl, false);
        };
        
        var fn_GetChkType = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetChkType');

            callGb = 'GetChkType';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetUpperList = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUpperList');

            callGb = 'GetUpperList';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetChkList = function () {
            var chkListCd = $("#<%= hdfChkList.ClientID %>").val();
            var aData = [];

            aData[0] = '';
            aData[1] = chkListCd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetChkListData');

            callGb = 'GetChkList';

            ajaxCall(jsonData, ajaxUrl, false);
        };        

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetLv1Grp') {
                    callGb = '';
                    fn_ControlBind_Lv1Grp(json);

                    fn_GetChkType();
                }
                else if (callGb == 'GetChkType') {
                    callGb = '';
                    fn_ControlBind_ChkType(json);

                    fn_GetUpperList();
                }
                else if (callGb == 'GetUpperList') {
                    callGb = '';
                    fn_ControlBind_UpperList(json);

                    var chkListCd = $("#<%= hdfChkList.ClientID %>").val();

                    if (chkListCd != '') {
                        fn_GetChkList();
                    }
                }
                else if (callGb == 'GetChkList') {
                    callGb = '';
                    fn_ControlBind_ChkList(json);
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'InsOk') {
                            callGb = '';
                            alert('점검항목 등록이 완료되었습니다.');
                            $("#txaChkList").val('');
                            fn_GetLv1Grp();
                        }
                        else if (callGb == 'UpdOk') {
                            callGb = '';
                            alert('점검항목 수정이 완료되었습니다.');
                        }
                        else if (callGb == 'DelOk') {
                            callGb = '';
                            alert('점검항목 삭제가 완료되었습니다.');
                            window.close();
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var fn_ControlBind_Lv1Grp = function (json) {
            var selector = $("#ddlLv1Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV1GRPCD;
                options.text = item.LV1GRPNM;

                if (item.LV1GRPNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });
        };

        var fn_ControlBind_ChkType = function (json) {

            var selector = $("#ddlChkType");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.CHKTYPECD;
                options.text = item.CHKTYPENM;

                if (item.CHKTYPENM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });
        };

        var fn_ControlBind_UpperList = function (json) {
            var selector = $("#ddlUpperList");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.UPPERLISTCD;
                options.text = item.UPPERLISTNM;

                if (item.UPPERLISTNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });
        };

        var fn_ControlBind_ChkList = function (json) {

            $.each(JSON.parse(json), function (idx, item) {
                $("#ddlLv1Grp").val(item.LV1GRPCD);
                $("#ddlChkType").val(item.CHKTYPECD);
                $("#ddlUpperList").val(item.UPPERLISTCD);
                $("#txaChkList").val(item.CHKLIST);
                $("#btnDel").show();
                $("#btnDel").click(function (e) {
                    fn_DelChkList();
                });
            });
        };

        var fn_DelChkList = function () {
            var chkListCd = $("#<%= hdfChkList.ClientID %>").val();

            if (chkListCd == "" || chkListCd == undefined) {
                alert('잘못된 접근입니다.');
                return false;
            }
            else {

                callGb = 'DelOk';

                if (confirm('해당 점검항목을 삭제하시겠습니까?')) {
                    var aData = [];

                    aData[0] = chkListCd;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "DelChkList");

                    ajaxCallEx(jsonData, ajaxUrl, false);
                }
                else {
                    return false;
                }
            }
        };

        var fn_RegChkList = function () {
            var ConfMsg = "";
            var chkListCd = $("#<%= hdfChkList.ClientID %>").val();
            var chkList = $("#txaChkList").val();

            var sUrl = '';

            var aData = [];

            if (chkList.replace(" ", "") == "") {
                alert('점검항목을 입력해주세요');
                return false;
            }
            else {
                var lv1GrpCd = $("#ddlLv1Grp").val();
                var chkTypeCd = $("#ddlChkType").val();
                var upperList = $("#ddlUpperList").val();

                aData[0] = lv1GrpCd;
                aData[1] = chkTypeCd;
                aData[2] = upperList;
                aData[3] = chkList;

                if (chkListCd == "") {
                    callGb = "InsOk";
                    sUrl = "InsChkList";
                    ConfMsg ="점검항목을 신규 추가 하시겠습니까?";
                }
                else {
                    aData[4] = chkListCd;

                    callGb = "UpdOk";
                    sUrl = "UpdChkList";
                    ConfMsg = "점검항목을 수정 하시겠습니까?";
                }

                if (confirm(ConfMsg)) {
                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
                else {
                    return false;
                }
            }
        };
    </script>
</head>
<body style="overflow:hidden;">
    <form id="form1" runat="server" >
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>점검항목 관리</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self);">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents" style="padding-top: 10px;">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th class="th-line">점검대상</th>
                                            <td style="text-align:left;">
                                                <p class="optionbox">
                                                    <select id="ddlLv1Grp" style="width:200px;"></select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">검사구분</th>
                                            <td style="text-align:left;">
                                                <p class="optionbox">
                                                    <select id="ddlChkType" style="width:200px;"></select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">점검구분</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <select id="ddlUpperList" style="width:200px;"></select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line" style="border:none;height:20px;padding-bottom:0;padding-top:10px;" colspan="2">점검항목</th>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;border: none;" colspan="2">
                                                <p class="inpbox">
                                                    <textarea id="txaChkList" rows="5" maxlength="1000"></textarea>
                                                    <asp:HiddenField ID="hdfChkList" runat="server" Value="" />
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <div class="btncenter">
                            <input id="btnSave" type="button" class="btn-green" value="저장" />
                            <input id="btnDel" type="button" class="btn-black" style="display:none;" value="삭제" />
                            <input id="btnClose" type="button" class="btn-gray" value="닫기" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
