﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeverancePayUpload.aspx.cs" Inherits="KTSSolutionWeb.SeverancePayUpload" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-error {
          border: 4px solid #D00;
          color: #590000;
        }

    </style>

    <script type="text/javascript">
		$(document).ready(function () {
            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var page = {
                init: function () {
                    fn_Init();
                    fn_GridInit();
                },
                eventbind: function () {
                    $("#inputValue").keyup(function (e) {
                        var fieldEl = document.getElementById("selField");
                        var valueEl = document.getElementById("inputValue");

                        ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                    });

                    $("#selField").change(function (e) {
                        fn_Field();
                    });

                    $("#btnModTemp").click(function (e) {
                        fn_ModExcel();
                    });

                    $("#btnNewTemp").click(function (e) {
                        fn_NewExcel();
                    });

                    $("#btnNotTemp").click(function (e) {
                        fn_NotExcel();
                    });

                    $("#btnSelect").click(function (e) {
                        fn_Search();
                    });

                    $("#gridExcel").click(function (e) {
                        ktsGrid.download("xlsx", "퇴직금추계 최종결과.xlsx", { sheetName: "퇴직금추계" });
                    });
                }
            };

		});

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#btnModTemp").hide();
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "기준월", field: "BASEMON", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "퇴직금형태", field: "SEVERANCE_TYPE", sorter: "string", width: 160 },
                { headerHozAlign: "center", hozAlign: "right", title: "퇴직금추계", field: "ESTIMATE_AMT", sorter: "number", width: 160, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 180 },
                { headerHozAlign: "center", hozAlign: "center", title: "최종변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 180 },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_NewExcel = function () {
            callGb = 'newExcel';

            //입력 파라미터
            var aData = [];
            aData[0] = 'NEWEXCEL';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ModExcel = function () {
            callGb = 'modExcel';

            //입력 파라미터
            var aData = [];
            aData[0] = 'MODEXCEL';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_NotExcel = function () {
            callGb = 'notExcel';

            //입력 파라미터
            var aData = [];
            aData[0] = 'NOTEXCEL';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_excelDown = function (gubun, json, sheetNm, fileNm, type) {
            var headers;

            // Custom headers mapping   
            if (gubun === 'not') {
                headers = ['기준월', '사원번호', '퇴직금형태', '추계금액', '오류메시지', '발생일'];
            }
            else {
                headers = ['기준월', '사원번호', '퇴직금형태', '추계금액'];
            }

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            if (gubun === 'not') {
                data = jsonData.map(item => [item.BASEMON, item.EMPNO, item.SEVERANCE_TYPE, item.ESTIMATE_AMT, item.EMSG, item.DBCREATEDT]);
            }
            else {
                data = jsonData.map(item => [item.BASEMON, item.EMPNO, item.SEVERANCE_TYPE, item.ESTIMATE_AMT]);
            }

            // Add headers as the first row
            //data.unshift(headers);
            data.unshift(headers); // Add the first row of headers

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            if (gubun === 'not') {
                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 12.89 },
                    { wch: 10.89 },
                    { wch: 30.89 },
                    { wch: 10.89 }
                ];

            }
            else {
                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 12.89 },
                    { wch: 10.89 }
                ];
            }

            if (gubun === 'mod') {
                if (!ws.A1.c) ws.A1.c = [];
                ws.A1.c.push({ a: "사원번호", t: "업로드 제외 DATA는\n행을 삭제하시면 됩니다." });
            }

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            // Apply styles to headers
            for (var C = 0; C < headers.length; ++C) {
                var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                if (!ws[cell_address]) continue;
                ws[cell_address].s = headerStyle;
            }

            //0~6  7~20   21_ 26
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 0) {
                        if (C == 0) {
                            if (gubun == 'mod') {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    },
                                    fill: { fgColor: { rgb: "A6A6A6" } }
                                };
                            }
                            else {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    }
                                };
                            }
                        }
                        else if (C == 3) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

        }

        var ajaxCallBack = function (json) {
            var errCd = '';
            var errMsg = '';

            if (callGb == 'newExcel') {
                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridId = 'contentsarea1';
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                    ktsGridId = 'ktsGrid';
                }
                else {
                    fn_excelDown('new', json, 'Sheet1', '퇴직금추계 신규양식 일괄등록 Template', 'xlsx');
                }
            }
            else if (callGb == 'modExcel') {

                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridId = 'contentsarea1';
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                    ktsGridId = 'ktsGrid';
                }
                else {
                    fn_excelDown('mod', json, 'Sheet1', '퇴직금추계 변경양식 일괄등록 Template', 'xlsx');
                }
            }
            else if (callGb == 'notExcel') {

                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridId = 'contentsarea1';
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                    ktsGridId = 'ktsGrid';
                }
                else {
                    fn_excelDown('not', json, 'Sheet1', '퇴직금추계 사번불일치', 'xlsx');
                }
            }
        }

        var fn_UploadCheck = function () {
            var fileInput = document.getElementById('fuPay');

            // 파일이 선택된 경우
            if (fileInput.files.length > 0) {
                return true;
            } else {
                ktsGridId = 'contentsarea1';
                ktsGridAlert("파일이 없습니다.");
                ktsGridId = 'ktsGrid';
                return false
            }
        }

        var fn_Search = function () {
            callGb = 'SEARCH';

            //입력 파라미터
            var aData = [];
            aData[0] = callGb;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetExcelData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
    <div class="pagenavi">
        <ul>
            <li><a id="aMenuTree1" runat="server"></a></li>
            <li class="last"><a id="aMenuTree2" runat="server"></a></li>
            <!-- 현재 페이지에 last클래스 추가 -->
        </ul>
    </div>
    <!--end subTitle-->

    <!-- S: contentsarea -->
    <div id="contentsarea1" class="contentsarea">
        <!-- S:page-upload-management -->
        <div class="page-upload-management">
            <div class="list-top">
                <strong>퇴직금추계관리</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="btnNotTemp" type="button" class="btn-green last" style="float: right; margin: 0px 10px" value="사번불일치다운로드" />
                        <input id="btnNewTemp" type="button" class="btn-green last" style="float: right; margin: 0px 10px" value="신규양식다운로드" />
                        <input id="btnModTemp" type="button" class="btn-green last" style="float: right; margin: 0px 10px" value="변경양식다운로드" />
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="upload-list">
                        <ul>
                            <li style="text-align: left;">
                                <strong>퇴직금추계</strong>
                                <div class="file-field">
                                    <p class="filebox">
                                        <input type="text" id="txtfuPay" />
                                        <span class="file">
                                            <input type="file" id="fuPay" name="fuPay" onchange="document.getElementById('txtfuPay').value=this.value;" />
                                            <label for="fuPay">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
                                        </span>
                                        <asp:Button ID="btnUpload" runat="server" OnClientClick="return fn_UploadCheck();" OnClick="btnUpload_ServerClick" class="btn-black" Text="최종등록완료" />
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </asp:UpdatePanel>
            <div style="margin-bottom: 20px;"></div>
            <!-- S:datalist -->
            <div class="datalist">
                <div class="list-top">
                    <strong>최종결과</strong>
                    <label class="searchrowcount"></label>
                    <div class="pull-right">
                        <div class="btnset">
                            <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            <input id="btnSelect" type="button" class="btn-green last" style="margin-right: 12px;" value="조회" />
                        </div>
                    </div>
                </div>
                <div class="scrollbox">
                    <div class="table-search" style="display: none;">
                        <span>
                            <select id="selField">
                                <option value="">선택</option>
                                <option value="EMPNO">사번</option>
                                <option value="EMPNM">이름</option>
                                <option value="SEVERANCE_TYPE">퇴직금형태</option>
                            </select>
                            : 
                    <input id="inputValue" disabled="disabled" type="text" />
                        </span>
                    </div>
                    <div id="ktsGrid"></div>
                </div>
            </div>
            <!-- E:datalist -->
        </div>
        <!-- E:page-upload-management -->
    </div>
    <!--//E: contentsarea -->
</asp:Content>