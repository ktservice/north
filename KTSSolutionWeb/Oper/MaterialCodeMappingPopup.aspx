﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MaterialCodeMappingPopup.aspx.cs" Inherits="KTSSolutionWeb.MaterialCodeMappingPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>서비스코드</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnDel").click(function (e) {
                    fn_DelTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            fn_Unit();
        }

        var fn_DelTran = function () {
            if (confirm("삭제시 상품코드 매핑정보도 삭제 됩니다.\n계속 진행하시겠습니까?")) {
                callGb = 'DelTran';

                fn_Transaction('D');
            }
        }

        var fn_ModTran = function () {
            if (fn_ModChk()) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_ModChk = function () {
            var unitCnt = $("#txtUnitCntM").val();
            var selUnit = $("#selUnitM").val();

            if (unitCnt.length == 0) {
                ktsGridAlert("변경하려는 수량을 입력하세요.");
                return false;
            }
            if (selUnit.length == 0) {
                ktsGridAlert("변경하려는 단위를 선택하세요.");
                return false;
            }

            return true;
        }

        var fn_RegChk = function () {
            var classNm1 = $("#txtClassNm1").val();
            var classNm2 = $("#txtClassNm2").val();
            var classNm3 = $("#txtClassNm3").val();
            var classCd3 = $("#txtClassCd3").val();
            var productNm = $("#txtProductNm").val();
            var productCd = $("#txtProductCd").val();
            var unitCnt = $("#txtUnitCntI").val();
            var selUnit = $("#selUnitI").val();

            if (classNm1.length == 0) {
                ktsGridAlert("대분류명을 입력하세요.");
                return false;
            }
            if (classNm2.length == 0) {
                ktsGridAlert("중분류명을 입력하세요.");
                return false;
            }
            if (classNm3.length == 0) {
                ktsGridAlert("소분류명을 입력하세요.");
                return false;
            }
            if (classCd3.length == 0) {
                ktsGridAlert("소분류코드를 입력하세요.");
                return false;
            }
            if (productNm.length == 0) {
                ktsGridAlert("상품명을 입력하세요.");
                return false;
            }
            if (productCd.length == 0) {
                ktsGridAlert("상품코드를 입력하세요.");
                return false;
            }
            if (unitCnt.length == 0) {
                ktsGridAlert("변경하려는 수량을 입력하세요.");
                return false;
            }
            if (selUnit.length == 0) {
                ktsGridAlert("변경하려는 단위를 선택하세요.");
                return false;
            }

            return true;
        }

        var fn_Unit = function () {
            callGb = 'selUnit';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'G04';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun, productCd) {

            var lv3Cd = '';
            var unit = '';
            var unitCnt = '';

            if (gubun == 'I') {
                lv3Cd = $("#txtClassCd3").val();
                unit = $("#selUnitI").val();
                unitCnt = $("#txtUnitCntI").val();
            }
            if (gubun == 'M') {
                lv3Cd = $("#hdnCode").val();
                unit = $("#selUnitM").val();
                unitCnt = $("#txtUnitCntM").val();
            }
            else if (gubun == 'D') {
                lv3Cd = $("#hdnCode").val();
            }
            
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = $("#txtClassNm1").val();
            aData[2] = $("#txtClassNm2").val();
            aData[3] = $("#txtClassNm3").val();
            aData[4] = lv3Cd;
            aData[5] = $("#txtProductNm").val();
            aData[6] = $("#txtProductCd").val();
            aData[7] = unit;
            aData[8] = unitCnt;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;

            if (callGb == 'selUnit') {
                obj1 = $('#selUnitI');
                obj1.empty();
                obj2 = $('#selUnitM');
                obj2.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                    obj2.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                $("#selUnitM").val($("#hdnUnit").val()).prop("selected", true);
                $("#txtUnitCntM").val($("#hdnUnitCnt").val());
                
                callGb = '';
            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
            else if (callGb == 'ModTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('변경되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
            else if (callGb == 'DelTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('삭제되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {
            
            opener.SetDispClass(check);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>서비스코드 등록</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblDel">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">서비스코드</th>
                                            <td colspan="2" class="td-left">
                                                <asp:Label id="lblClass" runat="server"></asp:Label>
                                                 <input type="hidden" id="hdnCode" runat="server" />
                                                 <input type="hidden" id="hdnUnit" runat="server" />
                                                 <input type="hidden" id="hdnUnitCnt" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">단위</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUnitM">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">수량</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtUnitCntM" maxlength="11" tabindex="1" style="width:100px;" placeholder="수량 입력" class="text-number" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                                <input id="btnDel" type="button" class="btn-del" value="삭제" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">대분류명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtClassNm1" maxlength="200" tabindex="1" style="width:400px;" placeholder="대분류명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">중분류명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtClassNm2" maxlength="200" tabindex="1" style="width:400px;" placeholder="중분류명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">소분류명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtClassNm3" maxlength="200" tabindex="1" style="width:400px;" placeholder="소분류명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">소분류코드</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtClassCd3" maxlength="7" tabindex="1" style="width:400px;" placeholder="소분류코드 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductNm" maxlength="200" tabindex="1" style="width:400px;" placeholder="상품명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품코드</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductCd" maxlength="10" tabindex="1" style="width:400px;" placeholder="상품코드 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">단위</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUnitI">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">수량</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtUnitCntI" maxlength="11" tabindex="1" style="width:100px;" placeholder="수량 입력" class="text-number" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
