﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ToolNoteBookBo.aspx.cs" Inherits="KTSSolutionWeb.ToolNoteBookBo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_HistGridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    $('.tabnav a').filter(':eq(0)').click();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "노트북 보정대상.xlsx", { sheetName: "노트북 보정대상" });
                });

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    if (this.text === '이력 조회 결과') {
                        fn_Hist();
                    }
                    else {
                        fn_Search();
                    }

                    return false;
                }).filter(':eq(0)').click();

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='조직변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";

            return html;
        }

        var fn_GridInit = function () {

            var Columns = [
                { formatter: fn_CellReg, width: 80, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "팀코드", field: "ORGCD4", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "팀", field: "ORGNM4", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", width: 100 },
                {
                    title: '모델정보', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "S/N", field: "SN", sorter: "string", width: 200 },
                        { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 60 },
                        { headerHozAlign: "center", hozAlign: "center", title: "속도", field: "SPEED", sorter: "string", width: 80 },
                        {
                            title: 'MAC주소', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "유선", field: "MAC_CABLE", sorter: "string", width: 160 },
                                { headerHozAlign: "center", hozAlign: "center", title: "무선", field: "MAC_WIRELESS", sorter: "string", width: 160 },
                            ]
                        },
                    ]
                },
                {
                    title: '체크', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "직영구분코드", field: "COMPGBCD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "직영구분", field: "COMPGB", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "사용여부코드", field: "USECD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "사용여부", field: "USENM", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "상태코드", field: "STATUSCD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "STATUSNM", sorter: "string", width: 100 },
                        {
                            title: '보유여부', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "전원어댑터", field: "HOLD_ADAPTER", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "노트북가방", field: "HOLD_BAG", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "5G젠더", field: "HOLD_GENDER5G", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "USB젠더", field: "HOLD_GENDERUSB", sorter: "string", width: 100 },
                            ]
                        }
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string", width: 300 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "REG_DATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "CHANGE_DATE", sorter: "string", width: 120 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_HistGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "S/N", field: "SN", sorter: "string", width: 200 },
                {
                    title: '변경전', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "BEF_ORGCD", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "BEF_ORGNM", sorter: "string", width: 140 }
                    ]
                },
                {
                    title: '변경후', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", width: 140 }
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "이력일", field: "DBCREATEDT", sorter: "string", width: 120 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsHistGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }


        var fn_Reg = function (id) {
            fn_PopupReg(id);
        }

        var ajaxCallBack = function (json) {
            var obj;

        }

        var fn_PopupReg = function (id) {
            var row = '';
            var rowData = '';
            var sn = '';

            if (id != '') {
                row = ktsGrid.getRow(id);
                rowData = row.getData();
                sn = rowData.SN;
            }

            var form = "/Oper/ToolNoteBookBoPopup";
            var target = "ToolNoteBookBoPopup";
            var param = {
                pCompGb:'BO',
                pSn: sn,
                pId: id
            };

            var nWidth = 920;
            var nHeight = 564;

            if (id == '') nHeight = 904;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Hist = function () {
            //입력 파라미터
            var aData = [];
            aData[0] = 'HIST';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            fn_HistGridInit();

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            ktsRowCountClass = 'histrowcount';

            $(".table-search").css('display', 'block');
        }

        var fn_Search = function () {
            //입력 파라미터
            var aData = [];
            aData[0] = 'DATA';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            fn_GridInit();

            ktsRowCountClass = 'searchrowcount';

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        var SetDispMng = function (sn, check) {

            if (check == 'ok') {
                $("#btnSelect").trigger("click");
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <ul class="tabnav">
                <li><a href="#tab01">보정대상 조회 결과</a></li>
                <li><a href="#tab02">이력 조회 결과</a></li>
            </ul>
            <div class="tabcontent">
                <div id="tab01">
                    <div class="list-top">
                        <strong>보정대상 조회 결과</strong>
                        <label class="searchrowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField">
                                    <option value="">선택</option>
                                    <option value="GIVE_DATE">지급일자</option>
                                    <option value="ORGNM">소속</option>
                                    <option value="EMPNM">이름</option>
                                    <option value="EMPNO">사번</option>
                                    <option value="MODELNM">모델명</option>
                                    <option value="SN">S/N</option>
                                    <option value="MAKERNM">제조사</option>
                                </select>
                                : 
                        <input id="inputValue" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid"></div>
                    </div>
                </div>
                <div id="tab02">
                    <div class="list-top">
                        <strong>이력 조회 결과</strong>
                        <label class="histrowcount"></label>
                    </div>
                    <div class="scrollbox">
                        <div id="ktsHistGrid"></div>
                    </div>
                </div>
            </div>

            <!-- S:list-top -->

            <!-- //E:list-top -->
            
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
