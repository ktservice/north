﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ToolInfo.aspx.cs" Inherits="KTSSolutionWeb.ToolInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .searchbox {
            height:140px;
        }

        .plusicon {
            background:#fff url('/Resource/Mobile_images/icon_plus_01.png')no-repeat 50%;
            width:11px;
            height:11px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var ktsGrid1;
        var ktsGrid2;
        var ktsGrid2RowCnt = 0;
        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var cellUnit;
        var cellUnitCnt;

        var page = {
            init: function () {
                fn_Init();
                fn_LeftGridInit();
                fn_RightGridInit();
                fn_Load();
            },
            eventbind: function () {
  
                $("#selField").change(function (e) {
                    fn_Field1();
                });

                $("#selField2").change(function (e) {
                    fn_Field2();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#inputValue2").keyup(function (e) {
                    var fieldEl = document.getElementById("selField2");
                    var valueEl = document.getElementById("inputValue2");

                    ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    var nm = $("#txtItemNm").val();
                    fn_Search('1', nm);
                });

                $("#btnSelect2").click(function (e) {
                    var nm = $("#txtItemNm").val();
                    fn_Search('2', '');
                });

                $("#gridExcel").click(function (e) {
                    var name = "신규직원 지급 물품";
                    ktsGrid2.download("xlsx", name + ".xlsx", { sheetName: name });
                });

                $("#btnItemReg").click(function (e) {
                    fn_PopupReg('');
                });
                
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnMappingReg' type='button' onclick='fn_RegTran(\"" + cellNo + "\");' value='변경' class='btn-gray' style='min-width:80px;height:23px;line-height:0px;font-size:15px;'>";
            return html;
        }
        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);

            return value;
        }

        var fn_LeftGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { formatter: fn_CellReg, width: 80, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "id", sorter: "number", width: 100, frozen:true },
                { headerHozAlign: "center", hozAlign: "left", title: "품목명", field: "ITEMNM", sorter: "string", width: 200, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 90 },
                { headerHozAlign: "center", hozAlign: "center", title: "신규지급", field: "GIVEYN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "신규지급수량", field: "GIVECNT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "사용", field: "USEYN", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "left", title: "규격", field: "STANDARD", sorter: "string", width: 200 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid1 = ktsGrid;
        }

        var fn_RightGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "id", sorter: "number", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "left", title: "품목명", field: "ITEMNM", sorter: "string", width: 200, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 90 },
                { headerHozAlign: "center", hozAlign: "right", title: "지급수량", field: "GIVECNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "left", title: "규격", field: "STANDARD", sorter: "string", width: 200 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid2', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid2 = ktsGrid;
        }

        var fn_Load = function () {
        }

        var fn_RegTran = function (id) {
            fn_PopupReg(id);
        }

        var fn_Search = function (gubun, nm) {
            if (gubun == '1') {
                fn_LeftGridInit();
            }
            else {
                fn_RightGridInit();
            }
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = nm;
            aData[2] = '';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            if (gubun == '1' || gubun == '11') {
                ktsRowCountClass = 'searchrowcount';
                grid.getDataBind(jsonData, ajaxUrl);
            }
            else {
                ktsRowCountClass = 'searchrowcount2';
                grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid2');
            }

            $(".table-option").css('display', 'block');
        }

        var fn_Transaction = function (gubun, itemCd) {
            var param = '';
            if (gubun == "M") {
                var row = ktsGrid2.getRow(itemCd);
                var rowData = row.getData();
                param = rowData.UNITCNT;
            }
            else {
                param = $("#selClass3").val();
            }

            //입력 파라미터
            var aData = [];
            aData[0] = param;
            aData[1] = itemCd;
            aData[2] = gubun;
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetServiceCfgTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'RegTran') {
                ktsGridId = 'ktsGrid';
                callGb = '';
            }
        }

        var fn_PopupReg = function (id) {
            var form = "/Oper/ToolInfoPopup";
            var target = "ToolInfoPopup";
            var param = {
                pItemCd: id
            };

            var nWidth = 720;
            var nHeight = 560;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }
   
        var SetDispItem = function (check) {

            if (check == 'ok') {
                var nm = $("#txtItemNm").val();
                fn_Search('1', nm);
            }
        }

        var fn_Field1 = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid1.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Field2 = function () {
            var fieldEl = document.getElementById("selField2");
            var valueEl = document.getElementById("inputValue2");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid2.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }


    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <ul>
                <li>
                    <!-- S:leftbox -->
                    <div class="leftbox">
                        <!-- S:searchbox -->
                        <div class="searchbox">
                            <fieldset>
                                <span class="inpbox first">
                                    <label>품목명</label>
                                    <input type="text" id="txtItemNm" maxlength="50" tabindex="1" style="width: 300px;" placeholder="값 입력" />
                                </span>
                                <div class="btnSearchWrap">
                                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                                </div>
                                <hr />
                                <div class="btnSearchWrap">
                                    <input id="btnItemReg" type="button" class="btn-green last" value="품목코드 등록" />
                                </div>
                            </fieldset>
                        </div>
                        <!-- E:searchbox -->
                         <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <!-- S:datalist -->
                                <div class="datalist">
                                    <!-- S:list-top -->
                                    <div class="list-top">
                                        <strong>품목코드 관리</strong>
                                        <label class="searchrowcount"></label>
                                    </div>
                                    <!-- //E:list-top -->
                                    <!-- S:scrollbox -->
                                    <div class="scrollbox">
                                        <div class="table-search" style="display: none;">
                                            <span>
                                                <select id="selField">
                                                    <option value="">선택</option>
                                                    <option value="id">품목코드</option>
                                                    <option value="ITEMNM">품목명</option>
                                                </select>
                                                : 
                                        <input id="inputValue" disabled="disabled" type="text" />
                                            </span>
                                        </div>
                                        <div id="ktsGrid"></div>
                                    </div>
                                    <!-- E:scrollbox -->
                                </div>
                                <!-- E:datalist -->
                            </div>
                        </div>
                    </div>
                    <!-- E:leftbox -->
                </li>
                <li>
                    <!-- S:rightbox -->
                    <div class="rightbox">
                        <!-- S:searchbox -->
                        <div class="searchbox">
                            <fieldset>
                                <div class="btnSearchWrap">
                                    <input id="btnSelect2" type="button" class="btn-green last" value="조회" />
                                    <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                                </div>
                            </fieldset>
                        </div>
                        <!-- E:searchbox -->
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:list-top -->
                            <div class="list-top">
                                <strong>신규직원 지급 물품 조회</strong>
                                <label class="searchrowcount2"></label>
                            </div>
                            <!-- //E:list-top -->
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <div class="table-search" style="display: none;">
                                    <span>
                                        <select id="selField2">
                                            <option value="">선택</option>
                                            <option value="id">품목코드</option>
                                            <option value="ITEMNM">품목명</option>
                                        </select>
                                        : 
                                <input id="inputValue2" disabled="disabled" type="text" />
                                    </span>
                                </div>
                                <div id="ktsGrid2"></div>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:rightbox -->
                </li>
            </ul>
        </div>
        <!-- E:multiplebox -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>
