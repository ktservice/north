﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EvalUpload.aspx.cs" Inherits="KTSSolutionWeb.EvalUpload" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>

    <script type="text/javascript">
		$(document).ready(function () {
            $(document).ready(function () {
                page.eventbind();
            });

            var page = {
                eventbind: function () {
                    $("#btnTemp1").click(function (e) {
                        fn_DownloadTemplate('1');
                    });

                    $("#btnTemp11").click(function (e) {
                        fn_DownloadTemplate('11');
                    });

                    $("#btnTemp2").click(function (e) {
                        fn_DownloadTemplate('2');
                    });

                    $("#btnTemp3").click(function (e) {
                        fn_DownloadTemplate('3');
                    });

                    $("#btnTemp4").click(function (e) {
                        fn_DownloadTemplate('4');
                    });

                    $("#btnTemp5").click(function (e) {
                        fn_DownloadTemplate('5');
                    });
                    
                }
            };

		});


        var fn_DownloadTemplate = function (gubun) {
            if (gubun === '1') {
                window.location.href = '/Template/평가관리 품질VOC Excel Upload Template.xlsx';
            }
            else if (gubun === '11') {
                window.location.href = '/Template/평가관리 품질VOC 유지가입자 Excel Upload Template.xlsx';
            }
            else if (gubun === '2') {
                window.location.href = '/Template/평가관리 개통AS작업후고장 Excel Upload Template.xlsx';
            }
            else if (gubun === '3') {
                window.location.href = '/Template/평가관리 KPI Excel Upload Template.xlsx';
            }
            else if (gubun === '4') {
                window.location.href = '/Template/평가관리 SLA Excel Upload Template.xlsx';
            }
            else if (gubun === '5') {
                window.location.href = '/Template/평가관리 생산성 Excel Upload Template.xlsx';
            }
                
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-upload-management -->
        <div class="page-upload-management">
            <div class="list-top">
                <strong>평가관리</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" type="button" title="최종 등록 완료" class="submit-btn btn-green">최종 등록 완료</button>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="upload-list">
                        <ul>
                            <li>
					            <strong>품질VOC접수건</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuVoc"/>
										<span class="file">
											<input type="file" id="fuVoc" name="fuVoc" onchange="document.getElementById('txtfuVoc').value=this.value;" /> 
											<label for="fuVoc">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
                                        <div class="fileBtnWrap">
                                            <input id="btnTemp1" type="button" class="btn-green last" value="양식다운로드" />
                                        </div>
									</p>
								</div>
							</li>
                            <li>
					            <strong>품질VOC유지가입자</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuVocJoin"/>
										<span class="file">
											<input type="file" id="fuVocJoin" name="fuVocJoin" onchange="document.getElementById('txtfuVocJoin').value=this.value;" /> 
											<label for="fuVocJoin">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
                                        <div class="fileBtnWrap">
                                            <input id="btnTemp11" type="button" class="btn-green last" value="양식다운로드" />
                                        </div>
									</p>
								</div>
							</li>
                            <li>
					            <strong>개통/AS작업후고장</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuGtAs"/>
										<span class="file">
											<input type="file" id="fuGtAs" name="fuGtAs" onchange="document.getElementById('txtfuGtAs').value=this.value;" /> 
											<label for="fuGtAs">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
                                        <div class="fileBtnWrap">
                                            <input id="btnTemp2" type="button" class="btn-green last" value="양식다운로드" />
                                        </div>
									</p>
								</div>
							</li>
                            <li>
					            <strong>KPI</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuKpi"/>
										<span class="file">
											<input type="file" id="fuKpi" name="fuKpi" onchange="document.getElementById('txtfuKpi').value=this.value;" /> 
											<label for="fuKpi">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
                                        <div class="fileBtnWrap">
                                            <input id="btnTemp3" type="button" class="btn-green last" value="양식다운로드" />
                                        </div>
									</p>
								</div>
							</li>
                            <li>
					            <strong>SLA</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							            <input type="text" id="txtfuSla"/>
							            <span class="file">
								            <input type="file" id="fuSla" name="fuSla" onchange="document.getElementById('txtfuSla').value=this.value;" /> 
								            <label for="fuSla">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							            </span>
                                        <div class="fileBtnWrap">
                                            <input id="btnTemp4" type="button" class="btn-green last" value="양식다운로드" />
                                        </div>
                                    </p>
                                </div>
                            </li>
                            <li>
					            <strong>개통/AS 생산성</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							            <input type="text" id="txtfuProductivity"/>
							            <span class="file">
								            <input type="file" id="fuProductivity" name="fuProductivity" onchange="document.getElementById('txtfuProductivity').value=this.value;" /> 
								            <label for="fuProductivity">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							            </span>
                                        <div class="fileBtnWrap">
                                            <input id="btnTemp5" type="button" class="btn-green last" value="양식다운로드" />
                                        </div>
                                    </p>
                                </div>
                            </li>
						</ul>
					</div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </asp:UpdatePanel>
		</div>
		<!-- E:page-upload-management -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>