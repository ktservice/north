﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PortalMotCodeMappingPopup.aspx.cs" Inherits="KTSSolutionWeb.PortalMotCodeMappingPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>코드등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';
        var serviceCd = '';
        var servicecNm = '';
        var productCd = '';
        var productNm = '';
        var productDesc = '';
        var motPoint = '';
        var useYn = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';

            var obj1 = $('#selUse');
            obj1.empty();

            obj1.append("<option value='Y'>Y</option>");
            obj1.append("<option value='N'>N</option>");


            if ($("#hdnStatus").val() === 'M') {
                $("#txtProductCd").attr('readonly', true);
                $("#txtProductCd").attr('style', 'background: #d1d1d1 !important;width: 400px');

                $("#btnReg").hide();
                $("#btnMod").show();

                $("#thService").show();
                $("#tdService").show();
                $("#thServiceNm").hide();
                $("#tdServiceNm").hide();
                $("#thServiceCd").hide();
                $("#tdServiceCd").hide();
            }
            else {
                $("#btnReg").show();
                $("#btnMod").hide();

                $("#thService").hide();
                $("#tdService").hide();
                $("#thServiceNm").show();
                $("#tdServiceNm").show();
                $("#thServiceCd").show();
                $("#tdServiceCd").show();
            }
            
        }

        var fn_Load = function () {
            fn_Class();
        }

        var fn_RegTran = function () {
            if (fn_RegChk('I')) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_ModTran = function () {
            if (fn_RegChk('M')) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegChk = function (gubun) {
            productCd = $("#txtProductCd").val();
            productNm = $("#txtProductNm").val();
            productDesc = $("#txtProductDesc").val();
            //motPoint = $("#txtMotPoint").val();
            motPoint = 0;
            useYn = $("#selUse").val();
            

            if (gubun === 'M') {
                serviceCd = $("#selClass").val();
                servicecNm = $("#selClass option:selected").text();
            }
            else {
                serviceCd = $("#txtClassCd").val();
                servicecNm = $("#txtClassNm").val();
            }

            
            if (servicecNm.length == 0) {
                ktsGridAlert("서비스명을 입력하세요.");
                return false;
            }
            if (serviceCd.length == 0) {
                ktsGridAlert("서비스코드를 입력하세요.");
                return false;
            }
            if (productNm.length == 0) {
                ktsGridAlert("상품명을 입력하세요.");
                return false;
            }
            if (productCd.length == 0) {
                ktsGridAlert("상품코드를 입력하세요.");
                return false;
            }
            //if (motPoint.length == 0) {
            //    ktsGridAlert("판매실적급포인트 값을 입력하세요.");
            //    return false;
            //}
            if (selUse.length == 0) {
                ktsGridAlert("변경하려는 사용을 선택하세요.");
                return false;
            }

            return true;
        }

        var fn_Class = function () {
            callGb = 'selClass';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


        var fn_Transaction = function (gubun) {
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = serviceCd;
            aData[2] = servicecNm;
            aData[3] = productCd;
            aData[4] = productNm;
            aData[5] = productDesc;
            aData[6] = motPoint;
            aData[7] = useYn;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {

            if (callGb == 'selClass') {
                obj1 = $('#selClass');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.SERVICECD + "'>" + item.SERVICECNM + "</option>");
                });

                callGb = '';

                if ($("#hdnStatus").val().length != 0) {

                    if ($("#hdnStatus").val() === 'M') {
                        fn_SetData();
                    }
                    
                }

            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
            else if (callGb == 'ModTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('변경되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function fn_SetData() {
            $("#txtClassNm").val($("#hdnClassNm").val());
            $("#txtClassCd").val($("#hdnClassCd").val());
            $("#txtProductNm").val($("#hdnProductNm").val());
            $("#txtProductCd").val($("#hdnProductCd").val());
            $("#txtProductDesc").val($("#hdnProductDesc").val());
            //$("#txtMotPoint").val($("#hdnMotPoint").val());

            $("#selClass").val($("#txtClassCd").val()).prop("selected", true);
            $("#selUse").val($("#hdnUse").val()).prop("selected", true);
        }

        function SendParentsForm() {
            var cellNo = $("#hdnCellNo").val();

            opener.SetDispClass(check, cellNo, serviceCd, servicecNm, productCd, productNm, productDesc, motPoint, useYn);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>코드 등록</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th id="thService" style="width: 120px;">서비스</th>
                                            <td id="tdService" colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selClass">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thServiceNm" style="width: 120px;">서비스명</th>
                                            <td id="tdServiceNm" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtClassNm" maxlength="60" tabindex="1" style="width:400px;" placeholder="서비스명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thServiceCd" style="width: 120px;">서비스코드</th>
                                            <td id="tdServiceCd" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtClassCd" maxlength="7" tabindex="1" style="width:400px;" placeholder="서비스코드 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductNm" maxlength="60" tabindex="1" style="width:400px;" placeholder="상품명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품코드</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductCd" maxlength="10" tabindex="1" style="width:400px;" placeholder="상품코드 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품설명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductDesc" maxlength="132" tabindex="1" style="width:400px;" placeholder="상품설명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <!--
                                        <tr>
                                            <th style="width: 120px;">판매실적급포인트</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtMotPoint" maxlength="11" tabindex="1" style="width:400px;" placeholder="포인트 입력" class="text-number" onkeypress="return OnlyNumber2(event, this);" />
                                                </p>
                                            </td>
                                        </tr>
                                        -->
                                       <tr>
                                            <th style="width: 120px;">사용</th>
                                            <td colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUse">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <input type="hidden" id="hdnCellNo" runat="server" />
                            <input type="hidden" id="hdnStatus" runat="server" />
                            <input type="hidden" id="hdnClassNm" runat="server" />
                            <input type="hidden" id="hdnClassCd" runat="server" />
                            <input type="hidden" id="hdnProductNm" runat="server" />
                            <input type="hidden" id="hdnProductCd" runat="server" />
                            <input type="hidden" id="hdnProductDesc" runat="server" />
                            <input type="hidden" id="hdnMotPoint" runat="server" />
                            <input type="hidden" id="hdnUse" runat="server" />
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>