﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolNoteBookBoPopup.aspx.cs" Inherits="KTSSolutionWeb.ToolNoteBookBoPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>노트북 조직 변경</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/lengthCheck.js"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    

    <style type="text/css">
        input[type="button"], input[type="submit"] {
            cursor: pointer;
        }

        .ui-datepicker {
            height: 260px;
        }

        label{
            color: #f00;
        }

        .inpbox textarea {
            display: inline-block;
            height: 100%;
            width: 94%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 18px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }

        .td-button {
            width: 20px !important;
            padding:0px !important;
        }

        .td-left {
            text-align: left !important;
        }

        .td-width1 {
            width: 180px !important;
        }

        .td-width2 {
            width: 240px !important;
        }

        .td-width3 {
            width: 150px !important;
        }

        .td-width4 {
            width: 220px !important;
        }

        .td-width5 {
            width: 180px !important;
            text-align: center;
            letter-spacing: 0.5px;
        }

        .td-widthAI {
            width: 18.4px !important;
            padding: 0 !important;
            border-left: 0px solid !important;
            border-right: 0px solid !important;
            text-align: center;
        }

        .td-widthAS {
            width: 34px !important;
            padding: 0px 0px 0px 14px !important;
            border-right: 0px solid !important;
            text-align: center;
        }

        .td-widthAE {
            width: 41px !important;
            padding: 0px 21px 0px 0px !important;
            border-left: 0px solid !important;
            text-align: center;
        }
        
        .td-widthA {
            width: 6px !important;
            padding: 0px !important;
            border-left: 0px solid !important;
            border-right: 0px solid !important;
            text-align: center;
        }

        .text-number {
            text-align: right !important;
        }

        .tabulator-alert {
            position: absolute;
            display: flex;
            align-items: center;
            top: 0;
            left: 0;
            z-index: 100;
            height: 100%;
            width: 100%;
            background: rgba(0, 0, 0, 0.4);
            text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
            display: inline-block;
            margin: 0 auto;
            padding: 10px 20px;
            border-radius: 10px;
            background: #fff;
            font-weight: bold;
            font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
            border: 4px solid #333;
            color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#selCompGb").change(function (e) {

                    $("#txtOrgNm").val('');
                    $("#hdnOrgCd").val('');
                    

                    if (this.value == '01') {
                        $("#btnOrgNm").attr("onclick", "return fn_PopupOrg()");
                    }
                    else {
                        $("#btnOrgNm").attr("onclick", "return fn_PopupPartner()");
                    }

                });

                $("#btnEmpNm").click(function (e) {
                    fn_PopupUser();
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnDel").click(function (e) {
                    fn_DelTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm('', '');
                });
            }
        };


        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';

            fn_CtrlInit();
        }

        var fn_CtrlInit = function () {
            $("#txtGiveDate").attr('readonly', true);

            $("#txtOrgNm").attr('readonly', true);
            $("#txtOrgNm").attr('style', 'background: #d1d1d1 !important');
            
            $("#txtSn").attr('readonly', true);

            $("#txtMaker").attr('readonly', true);
            $("#txtModelNm").attr('readonly', true);

            $("#txtColor").attr('readonly', true);
            $("#txtSpeed").attr('readonly', true);


            $("#btnMod").show();
        }

        var fn_Load = function () {
            fn_CompGb();
        }

        var fn_PopupPartner = function () {
            var form = "/Common/OrgPartners";
            var target = "OrgPartners";
            var param = {
                pCheckType: 'R',
                pType: 'OFFICE',
                pOfficeCd: '',
                pTeamCd: '',
                pKtsYn: ''
            };
            var nWidth = 580;
            var nHeight = 580;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrg = function () {
            var form = "/Common/SearchOrgCd";
            var target = "SearchOrgCd";
            var param = {
                pORGLV: "F"
            };
            var nWidth = 600;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupUser = function () {
            var form = "/Common/SearchUser";
            var target = "SearchUser";
            var param = {
            };
            var nWidth = 700;
            var nHeight = 500;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var fn_DelTran = function () {
            if (confirm("정보가 삭제 됩니다.\n계속 진행하시겠습니까?")) {
                callGb = 'DelTran';

                fn_Transaction('D');
            }
        }

        var fn_ModTran = function () {
            if (fn_RegChk()) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    var tranGb = $("#hdnCompGb").val();

                    if (tranGb == 'BO') {
                        fn_Transaction('BM');
                    }
                    else {
                        fn_Transaction('M');
                    }
                }
            }
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }


        var fn_RegChk = function () {
            var pOrgCd = $("#hdnOrgCd").val();

            if (pOrgCd.length == 0) {
                ktsGridAlert('조직값을 입력하세요.');
                return false;
            }


            return true;
        }

        var fn_CompGb = function () {
            callGb = 'selCompGb';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T21';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function (sn) {
            callGb = 'Data';

            //입력 파라미터
            var aData = [];
            aData[0] = sn;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun) {
            var pGiveDate = $("#txtGiveDate").val();
            var pOrgCd = $("#hdnOrgCd").val();
            var pCompGb = $("#selCompGb").val();
            var pEmpno = $("#txtEmpno").val();
            var pKtsEmpno = $("#txtKtsEmpno").val();
            var pIdmsEmpno = $("#txtIdmsEmpno").val();
            var pEmpNm = $("#txtEmpNm").val();
            var pUse = $("#hdnUse").val();
            var pSn = $("#txtSn").val();
            var pMacCable = $("#hdnMacCable").val();
            var pMacWireless = $("#hdnMacWireless").val();
            var pMakerNm = $("#txtMaker").val();
            var pModelNm = $("#txtModelNm").val();
            var pColor = $("#txtColor").val();
            var pSpeed = $("#txtSpeed").val();
            var pHoldAdapter = $("#hdnHoldAdapter").val();
            var pHoldBag = $("#hdnHoldBag").val();
            var pHoldGender5g = $("#hdnHoldGender5g").val();
            var pHoldGenderusb = $("#hdnHoldGenderusb").val();
            var pStatus = $("#hdnStatus").val();
            var pMemo = $("#hdnMemo").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pGiveDate;
            aData[2] = pOrgCd;
            aData[3] = pCompGb;
            aData[4] = pEmpno;
            aData[5] = pKtsEmpno;
            aData[6] = pIdmsEmpno;
            aData[7] = pEmpNm;
            aData[8] = pUse;
            aData[9] = pSn;
            aData[10] = pMacCable;
            aData[11] = pMacWireless;
            aData[12] = pMakerNm;
            aData[13] = pModelNm;
            aData[14] = pColor;
            aData[15] = pSpeed;
            aData[16] = pHoldAdapter;
            aData[17] = pHoldBag;
            aData[18] = pHoldGender5g;
            aData[19] = pHoldGenderusb;
            aData[20] = pStatus;
            aData[21] = pMemo;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;

            if (callGb == 'selCompGb') {
                obj1 = $('#selCompGb');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });
                
                callGb = '';

                if ($("#hdnSn").val().length != 0) {
                    $("#txtSn").val($("#hdnSn").val());
                    fn_Data($("#hdnSn").val());
                }
            }
            else if (callGb == 'Emp') {
                $.each(JSON.parse(json), function (idx, item) {
                    $("#txtKtsEmpno").val(item.KTSEMPNO);
                    $("#txtIdmsEmpno").val(item.IDMSEMPNO);

                });

                callGb = '';
            }
            else if (callGb == 'Data') {

                var vModelNm = '';
                var vMakerNm = '';
                var vColor = '';
                var vSpeed = '';
                var vGiveDate = '';
                var vMacCable = '';
                var vMacWireless = '';
                var vOrgNm = '';
                var vOrgCd = '';
                var vEmpno = '';
                var vEmpNm = '';
                var vKtsEmpno = '';
                var vIdmsEmpno = '';
                var vMemo = '';
                var vCompGb = '';
                var vUse = '';
                var vStatus = '';
                var vHoldAdapter = '';
                var vHoldBag = '';
                var vHoldGender5g = '';
                var vHoldGenderusb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    vModelNm = item.MODELNM;
                    vMakerNm = item.MAKERNM;
                    vColor = item.COLOR;
                    vSpeed = item.SPEED;
                    vGiveDate = item.GIVE_DATE;
                    vMacCable = item.MAC_CABLE;
                    vMacWireless = item.MAC_WIRELESS;
                    vOrgNm = item.ORGNM;
                    vOrgCd = item.ORGCD;
                    vEmpno = item.EMPNO;
                    vEmpNm = item.EMPNM;
                    vKtsEmpno = item.KTSEMPNO;
                    vIdmsEmpno = item.IDMSEMPNO;
                    vMemo = item.MEMO;
                    vCompGb = item.COMPGBCD;
                    vUse = item.USECD;
                    vStatus = item.STATUSCD;
                    vHoldAdapter = item.HOLD_ADAPTER;
                    vHoldBag = item.HOLD_BAG;
                    vHoldGender5g = item.HOLD_GENDER5G;
                    vHoldGenderusb = item.HOLD_GENDERUSB;
                });

                $("#txtModelNm").val(vModelNm);
                $("#txtColor").val(vColor);
                $("#txtSpeed").val(vSpeed);
                $("#txtGiveDate").val(vGiveDate);

                $("#hdnMacCable").val(vMacCable);
                $("#hdnMacWireless").val(vMacWireless);

                $("#txtOrgNm").val(vOrgNm);
                $("#hdnOrgCd").val(vOrgCd);
                $("#txtEmpno").val(vEmpno);
                $("#txtEmpNm").val(vEmpNm);
                $("#txtKtsEmpno").val(vKtsEmpno);
                $("#txtIdmsEmpno").val(vIdmsEmpno);
                $("#hdnMemo").val(vMemo);

                if (vMakerNm == null) vMakerNm = '';
                if (vCompGb == null) vCompGb = '';
                if (vUse == null) vUse = '';
                if (vStatus == null) vStatus = '';
                if (vHoldAdapter == null) vHoldAdapter = '';
                if (vHoldBag == null) vHoldBag = '';
                if (vHoldGender5g == null) vHoldGender5g = '';
                if (vHoldGenderusb == null) vHoldGenderusb = '';

                $("#txtMaker").val(vMakerNm);
                $("#hdnUse").val(vUse);
                $("#hdnStatus").val(vStatus);
                $("#hdnHoldAdapter").val(vHoldAdapter);
                $("#hdnHoldBag").val(vHoldBag);
                $("#hdnHoldGender5g").val(vHoldGender5g);
                $("#hdnHoldGenderusb").val(vHoldGenderusb).prop("selected", true);

                $("#selCompGb").val(vCompGb).prop("selected", true);

                if ($("#selCompGb").val() == '01') {
                    $("#btnOrgNm").attr("onclick", "return fn_PopupOrg()");
                }
                else {
                    $("#btnOrgNm").attr("onclick", "return fn_PopupPartner()");
                }

                callGb = '';
            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        check = 'ok';
                        ktsGridAlert('등록되었습니다.');
                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
                
                callGb = '';
            }
            else if (callGb == 'ModTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        check = 'ok';
                        ktsGridAlert('변경되었습니다.');
                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });

                callGb = '';
            }
            else if (callGb == 'DelTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        check = 'ok';
                        ktsGridAlert('삭제되었습니다.');
                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
                
                callGb = '';
            }
        }
        var fn_Emp = function (gubun, emp) {
            callGb = 'Emp';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = emp;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetEmp');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            if (empno != '') {
                $("#txtEmpno").val(empno);
                $("#txtEmpNm").val(empnm);

                fn_Emp('NO', empno);
            }
            else {
                $("#txtEmpno").val('');
                $("#txtEmpNm").val('');
                $("#txtKtsEmpno").val('');
                $("#txtIdmsEmpno").val('');
            }
        }

        function SetDispOrgCd(orgcd, orgnm) {
            $("#txtOrgNm").val(orgnm);
            $("#hdnOrgCd").val(orgcd);
        }

        function SetOrgPartner(officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm) {
            $("#txtOrgNm").val(officeNm);
            $("#hdnOrgCd").val(officeCd);
        }

        var ktsGridAlert = function (msg, timeout) {
            var time = 1300;
            if (timeout != undefined) time = timeout;
            $("#" + ktsGridId).append('<div class="tabulator-alert"><div class="tabulator-alert-msg tabulator-alert-state-msg" role="alert">' + msg + '</div></div>');

            setTimeout(function () {
                $(".tabulator-alert").remove();
                if (check == 'ok') {
                    var id = $("#hdnId").val();
                    SendParentsForm(id, check);
                }
                
            }, time);
        }

        function SendParentsForm(id, check) {
            opener.SetDispMng(id, check);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>노트북 조직 변경</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">S/N</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtSn" maxlength="50" tabindex="1" class="td-width2" placeholder="S/N 입력" />
                                                    <input type="hidden" id="hdnSn" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                    <input type="hidden" id="hdnCompGb" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" maxlength="10" class="td-width1" tabindex="1" placeholder="지급일자 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">모델명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtModelNm" maxlength="20" tabindex="1" class="td-width1" placeholder="모델명 입력" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">제조사</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtMaker" maxlength="20" tabindex="1" class="td-width1" placeholder="제조사 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">색상</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtColor" maxlength="10" tabindex="1" class="td-width1" placeholder="색상 입력" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">속도</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtSpeed" maxlength="10" tabindex="1" class="td-width1" placeholder="속도 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">직영구분</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selCompGb" class="td-width1">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">조직</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" maxlength="50" tabindex="1" class="td-width1" placeholder="조직명 입력" />
                                                    <input type="hidden" id="hdnOrgCd" runat="server" />
                                                    <input type="button" id="btnOrgNm" class="td-button" value=".." onclick="return fn_PopupOrg()"/>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">성명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" maxlength="50" tabindex="1" class="td-width3" placeholder="성명 입력" />
                                                    <input type="button" id="btnEmpNm" class="td-button" value=".."/>
                                                </p>
                                            </td>
                                            <th style="width: 120px;">사번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpno" maxlength="20" tabindex="1" class="td-width3" placeholder="사번 입력" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">KTS사번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtKtsEmpno" maxlength="20" tabindex="1" class="td-width3" placeholder="KTS사번 입력" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">IDMS사번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtIdmsEmpno" maxlength="20" tabindex="1" class="td-width3" placeholder="IDMS사번 입력" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                                <input type="hidden" id="hdnMacCable" />
                                                <input type="hidden" id="hdnMacWireless" />
                                                <input type="hidden" id="hdnUse" />
                                                <input type="hidden" id="hdnStatus" />
                                                <input type="hidden" id="hdnHoldAdapter" />
                                                <input type="hidden" id="hdnHoldBag" />
                                                <input type="hidden" id="hdnHoldGender5g" />
                                                <input type="hidden" id="hdnHoldGenderusb" />
                                                <input type="hidden" id="hdnMemo" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>