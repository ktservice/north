﻿using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Framework.Web;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class PortalSalesPolicyPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    string pGubun = Request.Form["pGubun"] == null ? "" : Request.Form["pGubun"].ToString();
                    string pClassCd = Request.Form["pClassCd"] == null ? "" : Request.Form["pClassCd"].ToString();

                    hdnGubun.Value = pGubun;
                    hdnClassCd.Value = pClassCd;
                    hdnOClassCd.Value = pClassCd;
                }
            }

            if(!IsPostBack)
            {
                imageFileBind();
            }
        }


        DataSet GetDbFile(string strClassCd)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pCLASSCD", MySqlDbType.VarChar, 20);
                parameters[0].Value = strClassCd;

                CommonData data = new CommonData("UP_PORTALSALESPOLICY_FILE_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }


        DataSet SetDbFile(string strGubun, string strSeq, string strClassCd, string strFileNm, byte[] bfile, string strFileType, int nFileSize, string strEmpno)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[8];
                parameters[0] = new MySqlParameter("@pGUBUN", MySqlDbType.VarChar, 1);
                parameters[0].Value = strGubun;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 4000);
                parameters[1].Value = strSeq;
                parameters[2] = new MySqlParameter("@pCLASSCD", MySqlDbType.VarChar, 20);
                parameters[2].Value = strClassCd;
                parameters[3] = new MySqlParameter("@pFILENM", MySqlDbType.VarChar, 150);
                parameters[3].Value = strFileNm;
                parameters[4] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[4].Value = bfile;
                parameters[5] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[5].Value = strFileType;
                parameters[6] = new MySqlParameter("@pFILESIZE", MySqlDbType.VarChar, 20);
                parameters[6].Value = nFileSize;
                parameters[7] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[7].Value = strEmpno;

                CommonData data = new CommonData("UP_PORTALSALESPOLICY_FILE_TRANSACTION", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        private void imageFileBind()
        {
            string classCd = hdnClassCd.Value;

            DataSet dsFile = new DataSet();
            DataSet dsSeq = new DataSet();

            dsFile = GetDbFile(classCd);

            if (dsFile.Tables[0].Rows.Count > 0)
            {
                string regDt = "";
                string fileNmList = "";

                for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                {
                    if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                    {
                        HtmlImage ImgBoard = new HtmlImage();

                        string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                        byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                        ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                        ImgBoard.Attributes.Add("style", "max-width:100%");

                        divImg.Controls.Add(ImgBoard);
                        divImg.Controls.Add(new LiteralControl("<br />"));


                        regDt = dsFile.Tables[0].Rows[i]["REGDT"].ToString();

                        if (fileNmList.Equals(""))
                        {
                            fileNmList = dsFile.Tables[0].Rows[i]["FILENM"].ToString();
                        }
                        else
                        {
                            fileNmList += ", " + dsFile.Tables[0].Rows[i]["FILENM"].ToString();
                        }

                    }
                }

                lblFileNmList.Text = fileNmList;
                lblFileRegDt.Text = regDt;
            }

        }


        private void imageFileUpload()
        {
            string result = string.Empty;
            bool bAdd = false;
            string classCd = hdnClassCd.Value;

            DataSet ds = new DataSet();
            DataSet dsSeq = new DataSet();

            try
            {
                //첨부 전 삭제 seq 목록을 가져온다.
                dsSeq = SetDbFile("S", "", classCd, "", null, "", 0, Session["EMPNO"].ToString());

                for (int i=0;i< HttpContext.Current.Request.Files.Count;i++)
                {
                    HttpPostedFile postedFIle = HttpContext.Current.Request.Files[i];

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    if(postedFIle.FileName != "")
                    {
                        //type체크 필요 
                        if (strFileType.Contains("image"))
                        {
                            Stream stream = null;
                            stream = postedFIle.InputStream;

                            int nFileLen = Convert.ToInt32(stream.Length);

                            byte[] byteFile = new byte[nFileLen];

                            stream.Read(byteFile, 0, nFileLen);

                            ds = SetDbFile("I", "", classCd, strFileNm, byteFile, strFileType, nFileLen, Session["EMPNO"].ToString());

                            stream.Close();
                            stream.Dispose();

                            bAdd = true;
                        }
                    }
                    
                }

            }
            catch (Exception ex)
            {
                bAdd = false;
                ex.ToString();
            }
            finally
            {
            }

            //첨부 완료 후 기존 파일 삭제 처리
            if(bAdd)
            {
                string strSeq = "";
                for(int i=0;i<dsSeq.Tables[0].Rows.Count;i++)
                {
                    if(i == 0)
                    {
                        strSeq = dsSeq.Tables[0].Rows[i][0].ToString();
                    }
                    else
                    {
                        strSeq += "^" + dsSeq.Tables[0].Rows[i][0].ToString();
                    }
                }

                if(strSeq != "")
                {
                    try
                    {
                        dsSeq = SetDbFile("D", strSeq, classCd, "", null, "", 0, Session["EMPNO"].ToString());
                    }
                    catch(Exception ex)
                    {
                        ex.ToString();
                    }
                    finally
                    {
                    }
                    
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Reg", "fn_RegOk();", true);

            }

        }

        protected void btnRegTran_Click(object sender, EventArgs e)
        {
            try
            {
                imageFileUpload();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetSalesClass(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PORTALSALESCLASS";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetSalesTransaction(List<string> aData)
        {
            string result = string.Empty;

            string resultSession = string.Empty;
            string rptCd = "PORTALSALESPOLICYTRAN";
            string jobGb = "SET";
            string empNo = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }


            return result;
        }
 
    }
}