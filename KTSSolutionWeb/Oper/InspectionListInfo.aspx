﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InspectionListInfo.aspx.cs" Inherits="KTSSolutionWeb.InspectionListInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240624" rel="stylesheet" />    
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .tabulator-row > div {
            height:32px !important;
            padding-top:7px !important;
        }
    </style>
    <script type="text/javascript">

        var ktsGrid1;
        var ktsGrid2;
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_SetControls();
                fn_LeftGridInit();
                fn_RightGridInit();
                fn_Load();
            },
            eventbind: function () {

                $("#sField").change(function (e) {
                    fn_Field1();
                });

                $("#sField2").change(function (e) {
                    fn_Field2();
                });

                $("#sValue").keyup(function (e) {
                    var fieldEl = document.getElementById("sField");
                    var valueEl = document.getElementById("sValue");

                    ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#sValue2").keyup(function (e) {
                    var fieldEl = document.getElementById("sField2");
                    var valueEl = document.getElementById("sValue2");

                    ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnRegGroup").click(function (e) {
                    fn_PopupRegGroup('', '', '');
                });                               

                $("#btnRegCode").click(function (e) {
                    fn_PopupRegCode();
                });

                $("#btnRegList").click(function (e) {
                    fn_PopupRegList('');
                });

                $("#gridExcel1").click(function (e) {
                    ktsGrid1.download("xlsx", "조치항목분류.xlsx", { sheetName: "조치항목분류" });
                });

                $("#gridExcel2").click(function (e) {
                    ktsGrid2.download("xlsx", "점검항목.xlsx", { sheetName: "점검항목" });
                });
            }
        };

        var fn_SetControls = function () {
            fn_GetLv1Grp();
        };

        var fn_LeftGridInit = function () {
            var fn_RegButton = function (cell) {
                var rowData = cell.getRow().getData();
                var lv1grpcd = rowData.LV1GRPCD;
                var lv2grpcd = rowData.LV2GRPCD;
                var lv3grpcd = rowData.LV3GRPCD;

                var input = "<input id='btnCellSave' type='button' onclick='fn_PopupRegGroup(\"" + lv1grpcd + "\", \"" + lv2grpcd + "\", \"" + lv3grpcd + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 70},
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "LV1GRPNM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류", field: "LV2GRPNM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "소분류", field: "LV3GRPNM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "수정", field: "SAVE", width: 100, formatter: fn_RegButton },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류코드", field: "LV1GRPCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류코드", field: "LV2GRPCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소분류코드", field: "LV3GRPCD", sorter: "string", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid1', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid1 = ktsGrid;
        };

        var fn_RightGridInit = function () {
            var fn_RegButton = function (cell) {
                var rowData = cell.getRow().getData();
                var chklistcd = rowData.CHKLISTCD;

                var input = "<input id='btnCellSave' type='button' onclick='fn_PopupRegList(\"" + chklistcd + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 70 },
                { headerHozAlign: "center", hozAlign: "center", title: "점검대상", field: "LV1GRPNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "검사구분", field: "CHKTYPENM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "점검구분", field: "UPPERLISTNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "점검항목", field: "CHKLIST", sorter: "string", width: 300 },
                { headerHozAlign: "center", hozAlign: "center", title: "수정", field: "SAVE", width: 100, formatter: fn_RegButton },

                { headerHozAlign: "center", hozAlign: "center", title: "검사구분코드", field: "CHKTYPECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "점검구분코드", field: "UPPERLISTCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "점검항목코드", field: "CHKLISTCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "점검대상코드", field: "LV1GRPCD", sorter: "string", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid2', 'fitColumns', true, 'local'); ; // div의 grid id 값

            ktsGrid2 = ktsGrid;
        };

        var fn_Load = function () {
        
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                if (ktsGridId == 'ktsGrid1') {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid1.setData(json);
                    ktsGridRowCount(ktsGrid1.getDataCount());
                    fn_SearchChkList();
                }
                else {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid2.setData(json);
                    ktsGridRowCount(ktsGrid2.getDataCount());
                }
            }
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetLv1Grp') {
                    callGb = '';
                    fn_ControlBind_Lv1Grp(json);
                }
            }
        };

        var fn_GetLv1Grp = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv1GrpList');

            callGb = 'GetLv1Grp';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_Search = function () {
            var lv1grpcd = $("#ddlLv1Grp").val();

            //입력 파라미터
            var aData = [];
            aData[0] = lv1grpcd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGroupData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid1', false);

            $(".table-search").css('display', 'block');
        };

        var fn_SearchChkList = function () {
            var lv1grpcd = $("#ddlLv1Grp").val();

            //입력 파라미터
            var aData = [];
            aData[0] = lv1grpcd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetChkListData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid2', false);
        };

        var fn_ControlBind_Lv1Grp = function (json) {
            var selector = $("#ddlLv1Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "전체";

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV1GRPCD;
                options.text = item.LV1GRPNM;

                if (item.LV1GRPNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });
        };

        var fn_PopupRegGroup = function (lv1grpcd, lv2grpcd, lv3grpcd) {
            var form;

            form = "/Oper/RegInspectionGrp";
            var target = "RegInspectionGrp";

            var param = {
                pLV1GRPCD: lv1grpcd,
                pLV2GRPCD: lv2grpcd,
                pLV3GRPCD: lv3grpcd
            };

            var nWidth = 610;
            var nHeight = 290;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_PopupRegCode = function () {
            var form = "/Oper/RegInspectionCode";
            var target = "RegInspectionCode";

            var param = {};

            var nWidth = 610;
            var nHeight = 240;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_PopupRegList = function (chklistcd) {
            var form = "/Oper/RegInspectionList";
            var target = "RegInspectionList";

            var param = {
                pCHKLISTCD: chklistcd
            };

            var nWidth = 610;
            var nHeight = 440;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_Field1 = function () {
            var fieldEl = document.getElementById("sField");
            var valueEl = document.getElementById("sValue");

            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid1.setFilter('id', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Field2 = function () {
            var fieldEl = document.getElementById("sField2");
            var valueEl = document.getElementById("sValue2");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid2.setFilter('id', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }
    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
				<span class="optionbox first">
					<label>대분류</label>
                    <select id="ddlLv1Grp" style="width:200px;"></select>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->
        
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <!-- S:leftbox -->
            <div class="leftbox">    
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>항목분류</strong>
                        <label class="searchrowcount1"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <button id="btnRegGroup" type="button" class="btn-green">분류관리</button>
                                <input id="gridExcel1" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->
    
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <div class="table-search" style="display:none">
                            <span>
                                <select id="sField" style="font-weight:bold">
                                    <option value="">선택</option>
                                    <option value="LV2GRPNM">중분류</option>
                                    <option value="LV3GRPNM">소분류</option>
                                </select>
                                 :
                                <input id="sValue" type="text" disabled="disabled"/>
                            </span>
                        </div>
                        <div id="ktsGrid1"></div>
                    </div>    
                    <!-- E:scrollbox -->
                </div>
            </div>
            <!-- E:leftbox -->
            <!-- S:rightbox -->
            <div class="rightbox">
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>점검항목</strong>
                        <label class="searchrowcount2"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <button id="btnRegCode" type="button" class="btn-green">구분코드관리</button>
                                <button id="btnRegList" type="button" class="btn-green">점검항목관리</button>
                                <input id="gridExcel2" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->                    
                        
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <div class="table-search" style="display:none">
                            <span>
                                <select id="sField2" style="font-weight:bold">
                                    <option value="">선택</option>
                                    <option value="CHKTYPENM">검사구분</option>
                                    <option value="UPPERLISTNM">점검구분</option>
                                    <option value="CHKLIST">점검항목</option>
                                </select>
                                 :
                                <input id="sValue2" type="text" disabled="disabled"/>
                            </span>
                        </div>
                        <div id="ktsGrid2"></div>
                    </div>    
                    <!-- E:scrollbox -->
                </div>
                <!-- E:datalist -->
            </div>                
            <!-- E:rightbox -->
        </div>
        <!-- E:multiplebox -->

    </div>
	<!-- E: contentsarea -->
</asp:Content>
