﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.BSL.ExcelImport;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Order;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using OfficeOpenXml;

namespace KTSSolutionWeb
{
    public partial class PartnersInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PARTNERSINFO";
            string jobGb = "GET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string RegPartnersInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PARTNERSINFOUPDATE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelPartnersInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PARTNERSINFODELETE";
            string jobGb = "SET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                Stream stream = Request.Files["fu"].InputStream;

                if (stream.Length > 0)
                {
                    string strtype = Request.Files["fu"].ContentType;

                    if (strtype.Equals("application/vnd.ms-excel") || strtype.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    {
                        using (ExcelImport excel = new ExcelImport())
                        {
                            using (ExcelPackage excelPackage = new ExcelPackage(stream))
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                if (worksheet.Dimension.End.Column != 11)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "ktsGridAlert('업로드 파일 양식이 잘못되었습니다.', 2000);", true);
                                }
                                else
                                {
                                    string strEmpNo = Session["EMPNO"].ToString();

                                    excel.ExcelUpLoad("협력사정보", worksheet, "tb_partnersinfo_temp");

                                    //후처리 로직
                                    string result = string.Empty;
                                    string resultSession = string.Empty;
                                    string rptCd = "PARTNERSEXCELUPLOAD";
                                    string jobGb = "SET";

                                    List<string> aData = new List<string>();

                                    aData.Add(strEmpNo);

                                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                                    resultSession = dbTransaction.SessionCheck();

                                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                                    {
                                        result = dbTransaction.GetJsonData();
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExcel", "searchBtnClick();ktsGridAlert('협력사정보가 일괄 등록 되었습니다.', 2000);", true);
                                    }
                                    else //세션이 끊겼을 경우
                                    {
                                        result = resultSession;
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExcel", "searchBtnClick();ktsGridAlert('협력사정보 업로드중 오류가 발생했습니다.', 2000);", true);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "ktsGridAlert('엑셀파일만 업로드가 가능합니다.', 2000);", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "ktsGridAlert('업로드 할 파일을 선택해주세요.', 2000);", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "ktsGridAlert('엑셀 업로드 중 오류가 발생했습니다.', 2000);", true);
            }
        }
    }
}