﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="KTSSolutionWeb.User" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function RegUserID() {
            var nWidth = 870;
            var nHeight = 550;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Oper/RegUserID", null);

            Popupform.target = "RegUserID";
            var win = window.open("", "RegUserID", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno,
                pOPER: "Y"
            };

            var Popupform = createForm("/Common/OrgTree_Oper", param);

            Popupform.target = "OrgTree_Oper";
            var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgTree2(orgcd, empno) {

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno,
                pOPER: "Y"
            };

            var Popupform = createForm("/Common/OrgTree_Oper", param);

            Popupform.target = "OrgTree_Oper";
            var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgTree3(orgcd) {

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd
            };

            var Popupform = createForm("/Common/OrgTree_BComp", param);

            Popupform.target = "OrgTree_Oper";
            var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupPartnersOrg(empno, compcd) {

            var nWidth = 400;
            var nHeight = 425;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pCOMPCD: compcd,
                pEMPNO: empno,
                pTYPE: "C"
            };

            var Popupform = createForm("/Common/PartnersOfcPopup", param);

            Popupform.target = "OrgTree_Oper";
            var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetOrgCd(orgcd, chk) {

            this.focus();

            if (chk == "S") {
                document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;
                __doPostBack("<%=updPanelSearch.ClientID %>", "");
            }
            else {
                document.getElementById("<%=hdfEmpOrg.ClientID %>").value = orgcd;
                __doPostBack("<%=updPanel1.ClientID %>", "");
            }
        }

        function btnSaveCheck() {
            if (confirm("사용자 정보를 수정하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDelCheck() {
            if (confirm("사용자 정보를 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnUploadCheck() {
            var ktsyn = $("#<%= ddlKtsYn.ClientID %> option:selected").val();
            var msg = "";

            if (ktsyn == "Y") {
                msg = "kts 직원의 idms사번 정보를 업로드 하시겠습니까?";
            }
            else {
                msg = "협력사 사원 정보를 업로드 하시겠습니까?(권한이 없는 업체/국사의 정보는 업로드 되지 않습니다.)";
            }

            if (confirm(msg)) {
                return true;
            }
            else {
                return false;
            }
        }

        function DownloadTemplate(location) {
            if (location != undefined && location != "") {
                window.location.href = location;
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
                <div class="inpbox first plus">
                    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label id="lblTeam" runat="server">조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <button id="btnTeam" runat="server" type="button" class="btn-plus" title="검색">+</button>
				</div>
				
				<span class="inpbox">
					<label>이름</label>
                    <asp:TextBox ID="txbEmpNm" name="txbEmpNm" runat="server" MaxLength="50" Width="239px" TabIndex="1"></asp:TextBox>
                </span>                
				<span class="optionbox">
					<label>구분</label>
                    <asp:DropDownList ID="ddlKtsYn" style="width:250px;" runat="server" TabIndex="2" AutoPostBack="true" OnSelectedIndexChanged="ddlKtsYn_SelectedIndexChanged">
                        <asp:ListItem Text="kts" Value="Y" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="협력사" Value="N"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조직별 조회 결과</strong>
                <div class="pull-right">
				    <div class="btnset">
                        <asp:UpdatePanel ID="udpTemp" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <button id="btnTemp" runat="server" type="button" class="btn-green">양식다운로드</button>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlKtsYn" EventName="SelectedIndexChanged" /> 
                            </Triggers>
                        </asp:UpdatePanel>
				    </div>
                    <div class="filebox">
                        <input type="text" id="txtfu" />
                        <span class="file">
                            <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
                            <label for="fu">찾아보기</label>
                        </span>
                        <asp:Button ID="btnUpload" runat="server" OnClientClick="return btnUploadCheck();"  OnClick="btnUpload_ServerClick" class="btn-black" Text="업로드" />
                    </div>
                    <div class="btnset">
                        <button id="btnRegID" type="button" onclick="RegUserID();" class="btn-green">등록</button>
                        <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <th>No.</th>
                                <th>소속</th>
                                <th>조직코드</th>
                                <th>사번</th>
                                <th id="thktsEmpNo" runat="server">KTS사번</th>
                                <th>이름</th>
                                <th>연락처</th>
                                <th>업체명</th>
                                <th>업체코드</th>
                                <th>권한등급</th>
                                <th>조직권한</th>
                                <th>PW초기화</th>
                                <th>저장/삭제</th>
                                <th id="thJobNm" runat="server">직무</th>
                                <th id="thJobGrade" runat="server">직책</th>
                                <th id="thJobNm2" runat="server">직무2</th>
                                <th id="thEnterDt" runat="server">입사일자</th>
                                <th id="thExfireDt" runat="server">퇴사일자</th>
                                <th id="thIdmsEmpNo" runat="server">IDMS사번</th>
                                <th id="thPreIdmsEmpNo" runat="server">IDMS(구)사번</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td><%# Eval("USERORGCD") %></td>
                                            <td><asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label></td>
                                            <td id="tdKtsEmpNo" runat="server"><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td><%# Eval("MOBILE") %></td>
                                            <td><%# Eval("COMPANYNM") %></td>
                                            <td>
                                                <asp:Label ID="lblCompanyCd" runat="server" Text='<%# Eval("COMPANYCD") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAuthId" runat="server" Visible="false" Text='<%# Eval("AUTHID") %>'></asp:Label>
                                                <asp:Label ID="lblKtsYn" runat="server" Visible="false" Text='<%# Eval("KTSYN") %>'></asp:Label>
                                                <span class="optionbox">
                                                    <asp:DropDownList ID="ddlUserAuth" runat="server"></asp:DropDownList>
                                                </span>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOrgCd" runat="server" Visible="false" Text='<%# Eval("ORGCD") %>'></asp:Label>
                                                <button id="btnOrg" runat="server" type="button" class="btn-plus">+</button>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPwdReset" runat="server" Visible="false" Text='<%# Eval("PWDCHGYN") %>'></asp:Label>
                                                <asp:CheckBox ID="chkResetPwd" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Button id="btnSave" runat="server" OnClientClick="return btnSaveCheck();" OnClick="btnSave_ServerClick" class="btn-save" Text="저장" />
                                                <asp:Button id="btnDelete" runat="server" OnClientClick="return btnDelCheck();" OnClick="btnDelete_ServerClick" class="btn-del" Text="삭제" />
                                            </td>
                                            <td id="tdJobNm" runat="server"><%# Eval("JOBNM") %></td>
                                            <td id="tdJobGrade" runat="server"><%# Eval("JOBGRADE") %></td>
                                            <td id="tdJobNm2" runat="server">
                                                <div class="txtbox">
                                                    <asp:TextBox id="txbJobNm2" runat="server" Width="75px" Height="40px" MaxLength="50" Text='<%# Eval("JOBNM2") %>'></asp:TextBox>
                                                </div>
                                            </td>
                                            <td id="tdEnterDt" runat="server"><%# Eval("ENTERDT") %></td>
                                            <td id="tdExfireDt" runat="server"><%# Eval("EXFIREDT") %></td>
                                            <td id="tdIdmsEmpNo" runat="server">
                                                <div class="txtbox">
                                                    <asp:TextBox id="txbIdmsEmpNo" runat="server" Width="120px" Height="40px" MaxLength="20" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Text='<%# Eval("IDMSEMPNO") %>'></asp:TextBox>
                                                </div>
                                            </td>
                                            <td id="tdPreIdmsEmpNo" runat="server">
                                                <div class="txtbox">
                                                    <asp:TextBox id="txbPreIdmsEmpNo" runat="server" Width="120px" Height="40px" MaxLength="20" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Text='<%# Eval("PREIDMSEMPNO") %>'></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hdfEmpOrg" runat="server" OnValueChanged="hdfEmpOrg_ValueChanged" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="hdfEmpOrg" EventName="ValueChanged" />
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
