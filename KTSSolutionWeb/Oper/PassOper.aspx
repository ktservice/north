﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PassOper.aspx.cs" Inherits="KTSSolutionWeb.PassOper" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var pop1;
        var pop2;
        var pop3;

        function PopupOrgTree(orgcd) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pOPER: "Y"
            };

            var Popupform = createForm("/Common/OrgTree_Oper", param);

            Popupform.target = "OrgTree_Oper";
            pop1 = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop1.focus();
        }

        function PopupPassOper() {

            var sPassVal = $("#<%= ddlPassTypeR.ClientID %> option:selected").val();
            var bUpperYn = $("#<%= chkUpperYnR.ClientID %>").is(":checked");
            var sPtype = "";

            if (sPassVal == "") {
                alert("PASS구분을 선택해주세요");
                return;
            }
            else if (sPassVal == "BIZ") {
                sPtype = "EMPNO";
            }
            else {
                if (bUpperYn) {
                    sPtype = "EMPNO";
                }
                else {
                    sPtype = "ORGCD";
                }
            }

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pPTYPE: sPtype
            };

            var Popupform = createForm("/Common/SearchPassOper", param);

            Popupform.target = "SearchPassOper";
            pop2 = window.open("", "SearchPassOper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop2.focus();
        }

        function PopupPassOrgTree(orgcd, passtype, operator, valtype) {

            if (valtype == "S") {
                orgcd = document.getElementById("<%=hfAuthOrgCd.ClientID %>").value;
                passtype = $("#<%= ddlPassTypeR.ClientID %> option:selected").val();
            }

            if (passtype == "") {
                alert("PASS구분을 선택해주세요");
                return;
            }

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pPASSTYPE: passtype,
                pOPERATOR: operator,
                pVALTYPE: valtype
            };

            var Popupform = createForm("/Common/OrgTree_PassAuth", param);

            Popupform.target = "OrgTree_PassAuth";
            pop3 = window.open("", "OrgTree_PassAuth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop3.focus();
        }

        function PopupClosing() {
            if (pop1 != undefined) {
                pop1.close();
            }
            if (pop2 != undefined) {
                pop2.close();
            }
            if (pop3 != undefined) {
                pop3.close();
            }
        }

        function SetOrgCd(orgcd, chk) {

            this.focus();

            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelSearch.ClientID %>", "");
        }

        function SetPassOrgCd(orgcd, chk) {

            this.focus();

            if (chk == "S") {
                document.getElementById("<%=hfAuthOrgCd.ClientID %>").value = orgcd;
                __doPostBack("<%=updPanelAuthOrg.ClientID %>", "");
            }
            else {
                document.getElementById("<%=hfAuthOrg.ClientID %>").value = orgcd;
                __doPostBack("<%=updPanel1.ClientID %>", "");
            }
        }

        function SetOperCd(opercd, opernm, procid) {

            this.focus();

            document.getElementById("<%=txbOperator.ClientID %>").value = opernm;
            document.getElementById("<%=hfOperator.ClientID %>").value = opercd;
            __doPostBack("<%=updPanelOperator.ClientID %>", "");
        }

        function btnRegChk() {
            if (confirm("PASS 권한 정보를 등록하시겠습니까?")) {
                var PassVal = $("#<%= ddlPassTypeR.ClientID %> option:selected").val()
                var Operator = $("#<%= hfOperator.ClientID %>").val();
                var AuthOrg = $("#<%= hfAuthOrgCd.ClientID %>").val();

                if (PassVal == "") {
                    alert("PASS구분을 선택해주세요");
                    return false;
                }
                else if (Operator == "") {
                    alert("담당조직/담당자를 선택해주세요");
                    return false;
                }
                else if (AuthOrg == "") {
                    alert("권한조직 선택해주세요");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return false;
            }
        }

        function btnSaveChk() {
            if (confirm("PASS 권한 정보를 수정하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDelChk() {
            if (confirm("PASS 권한 정보를 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
				<span class="optionbox">
				    <label>PASS구분</label>
                    <asp:DropDownList ID="ddlPassTypeR" style="width:120px;" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPassTypeR_SelectedIndexChanged">
                    </asp:DropDownList>
				</span>
				<span class="ickbox" style="padding-right:20px">
                    <asp:UpdatePanel ID="updPanelUpperYnR" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>부서장권한</label>
                            <asp:CheckBox ID="chkUpperYnR" runat="server"/>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPassTypeR" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelOperator" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>담당조직/담당자</label>
                            <asp:TextBox ID="txbOperator" runat="server" Width="350px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" style="margin-right:10px" title="검색" onclick="javascript:PopupPassOper();">+</button>
                            <asp:HiddenField ID="hfOperator" runat="server" OnValueChanged="hfOperator_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPassTypeR" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOperator" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelAuthOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>권한조직</label>
                            <asp:TextBox ID="txtAuthOrgNm" runat="server" Width="450px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" style="margin-right:10px" title="검색" onclick="javascript:PopupPassOrgTree('', '', '', 'S');">+</button>
                            <asp:HiddenField ID="hfAuthOrgCd" runat="server" OnValueChanged="hfAuthOrgCd_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPassTypeR" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfAuthOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnPassAuthReg" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
                <asp:Button id="btnPassAuthReg" runat="server" OnClientClick="return btnRegChk();" Onclick="btnPassAuthReg_Click" class="btn-green last" style="float:right;" Text="등록" />
		    </fieldset>
        </div>
        <div class="searchbox">
		    <fieldset>       
                <div class="inpbox first plus">
                    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('');">+</button>
				</div>
				
				<span class="optionbox">
					<label>PASS구분</label>
                    <asp:DropDownList ID="ddlPassTypeS" style="width:120px;" runat="server" TabIndex="2">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="MOT" Value="MOT" ></asp:ListItem>
                        <asp:ListItem Text="신사업" Value="BIZ" ></asp:ListItem>
                        <asp:ListItem Text="공간" Value="SPACE" ></asp:ListItem>
                        <asp:ListItem Text="칭찬" Value="COMP"></asp:ListItem>
                    </asp:DropDownList>
                </span>
				<span class="inpbox">
					<label>담당조직/담당자</label>
                    <asp:TextBox ID="txbOperNm" runat="server"></asp:TextBox>
                </span>
                <asp:Button id="btnSelect" runat="server" onclick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <th>No.</th>
                                <th>PASS구분</th>
                                <th>담당조직/담당자</th>
                                <th>권한조직</th>
                                <th>부서장권한</th>
                                <th>저장/삭제</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td>
                                                <asp:Label ID="lblPassTypeNm" runat="server" Text='<%# Eval("PASSTYPENM") %>'></asp:Label>
                                                <asp:Label ID="lblPassType" runat="server" Visible="false" Text='<%# Eval("PASSTYPE") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOperNm" runat="server" Text='<%# Eval("AUTHOPERNM") %>'></asp:Label>
                                                <asp:Label ID="lblOperCd" runat="server" Visible="false" Text='<%# Eval("AUTHOPERATOR") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAuthOrgNm" runat="server" Text='<%# Eval("ORGNM") %>'></asp:Label>
                                                <asp:Label ID="lblAuthOrgCd" runat="server" visible="false" Text='<%# Eval("ORGCD") %>'></asp:Label>
                                                <button id="btnOrg" runat="server" type="button" class="btn-plus">+</button>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUpperYn" runat="server" Text='<%# Eval("UPPERYN") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Button id="btnSave" runat="server" OnClientClick="return btnSaveChk();" OnClick="btnSave_ServerClick" class="btn-save" Text="저장" />
                                                <asp:Button id="btnDelete" runat="server" OnClientClick="return btnDelChk();" OnClick="btnDelete_ServerClick" class="btn-del" Text="삭제" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hfAuthOrg" runat="server" OnValueChanged="hfAuthOrg_ValueChanged" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="btnPassAuthReg" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="hfAuthOrg" EventName="ValueChanged" />
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
