﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" CodeBehind="NoticePortalCsMng.aspx.cs" Inherits="KTSSolutionWeb.NoticePortalCsMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .line {
            border-bottom: 2px solid #00000082;
         }

        .searchbox {
            margin-top:0px !important;
            height:92px !important;
        }
        .tabcontent {
          display: inline;
        }

        .inpbox input {
            display: inline-block;
            height: 40px;
            width: 100%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 38px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }


        .inpbox textarea {
            display: inline-block;
            height: 100%;
            width: 100%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 38px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }

        .searchbox {
            height:150px;
        }

        .plusicon {
            background:#fff url('/Resource/Mobile_images/icon_plus_01.png')no-repeat 50%;
            width:11px;
            height:11px;
        }
        .btn-gray
        {
            min-width:60px;
            padding:0px;
        }

        .classReg {
            background:#ccd3cd;
            position:absolute;
        }
        
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var startDt = '';
        var endDt = '';
        var noticeUse = 'N';
        var noticeTitle = '';
        var noticeContent = '';
        var moduleCd = '';
        var moduleNm = '';
        var classCd = '';
        var classNm = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');


                    if (this.text === 'CS실적관리') {
                        fn_NoticeInfo('CSR');
                    }

                    return false;
                }).filter(':eq(0)').click();

                $("#btnSave1").click(function (e) {
                    fn_ModTran('CSR');
                });
            }
        };


        var fn_Load = function () {
            
        }

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            $("#txtStartDt1").attr('readonly', true);
            $("#txtEndDt1").attr('readonly', true);

            $(".classReg").hide();
        }

        var fn_NoticeInfo = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];
            aData[0] = gb;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetNoticeInfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ModTran = function (gubun) {
            if (fn_RegChk(gubun)) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegChk = function (gubun) {
            startDt = '';
            endDt = '';
            noticeUse = 'N';
            noticeTitle = '';
            noticeContent = '';
            moduleCd = '';
            moduleNm = '';

            if (gubun === 'CSR') {
                startDt = $("#txtStartDt1").val();
                endDt = $("#txtEndDt1").val();
                noticeTitle = $("#txtNoticeTitle1").val();
                noticeContent = $("#txtNoticeContent1").val();

                if ($('input:checkbox[id="chkNoticeUse1"]').is(":checked") == true) {
                    noticeUse = 'Y';
                }

                moduleCd = $("#hdnModuleCd1").val();
                moduleNm = $("#hdnModuleNm1").val();
            }
            

            if (startDt.length == 0) {
                ktsGridAlert("시작일을 입력하세요.");
                return false;
            }
            if (endDt.length == 0) {
                ktsGridAlert("종료일을 입력하세요.");
                return false;
            }
            if (noticeTitle.length == 0) {
                ktsGridAlert("제목을 입력하세요.");
                return false;
            }
            if (noticeContent.length == 0) {
                ktsGridAlert("내용을 입력하세요.");
                return false;
            }

            return true;
        }

        var fn_Transaction = function (gubun) {

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = moduleCd;
            aData[2] = moduleNm;
            aData[3] = noticeTitle;
            aData[4] = noticeContent;
            aData[5] = startDt;
            aData[6] = endDt;
            aData[7] = noticeUse;


            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetNoticeInfoTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {

            if (callGb == 'CSR') {
                var useYN = 'N';

                $.each(JSON.parse(json), function (idx, item) {
                    $('#txtStartDt1').val(item.STDT);
                    $('#txtEndDt1').val(item.ENDT);
                    $('#txtNoticeTitle1').val(item.TITLE);
                    $('#txtNoticeContent1').val(item.CONTENTS);
                    $('#hdnModuleCd1').val(item.MODULECD);
                    $('#hdnModuleNm1').val(item.MODULENM);
                    useYN = item.USEYN;
                });

                if (useYN === 'Y') {
                    $("input:checkbox[id='chkNoticeUse1']").prop("checked", true);
                }
                else {
                    $("input:checkbox[id='chkNoticeUse1']").prop("checked", false);
                }
            }
            else if (callGb == 'ModTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        alert('변경되었습니다');
                    }
                });

                callGb = '';
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
    <div class="contentsarea">
        <ul class="tabnav">
            <li><a href="#tab01">CS실적관리</a></li>
        </ul>
    </div>
    <div class="datalist">
        <div class="tabcontent">
            <div id="tab01">
                <div class="list-top">
                    <strong>CS실적관리</strong>
                    <label class="searchrowcount"></label>
                </div>
                <div class="scrollbox">
                    <table>
                        <tbody>
                            <tr>
                                <th style="width: 120px;">공지사항 기간</th>
                                <td class="td-left">
                                    <p class="inpbox" style="width:120px">
                                        <input id="txtStartDt1" type="text" class="date" />
                                    </p>
                                    ~
                                    <p class="inpbox" style="width:120px">
                                        <input id="txtEndDt1" type="text" class="date" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 사용</th>
                                <td class="td-left">
                                    <p class="ickbox">
                                        <label class="checkbox" for="chkNoticeUse1">
                                            사용
                                        </label>
                                        <input id="chkNoticeUse1" type="checkbox" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 제목</th>
                                <td class="td-left">
                                    <p class="inpbox">
                                        <input id="txtNoticeTitle1" type="text" maxlength="50" placeholder="공지 제목 입력" />
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 120px;">공지사항 내용</th>
                                <td class="td-left">
                                    <p class="inpbox">
                                        <textarea id="txtNoticeContent1" rows="5" maxlength="2665" placeholder="공지 내용 입력"></textarea>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" id="hdnModuleCd1" />
                                    <input type="hidden" id="hdnModuleNm1" />
                                    <input id="btnSave1" type="button" class="btn-save" value="저장" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div style="margin-bottom: 20px;"></div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>