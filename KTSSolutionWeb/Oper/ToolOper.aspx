﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ToolOper.aspx.cs" Inherits="KTSSolutionWeb.ToolOper" %>

<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var orgPopup = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });


                $("#<%= txtAuthOrgNm.ClientID %>").click(function (e) {

                    orgPopup = 1;
                    fn_PopupOrgTree();
                });

                $("#btnAuthTree").click(function (e) {
                    orgPopup = 1;
                    fn_PopupOrgTree();
                });

                $("#btnAuthOrgTree").click(function (e) {
                    fn_PopupAuthTree();
                });

                $("#<%= txbOperator.ClientID %>").click(function (e) {
                    fn_PopupAuthTree();
                });
                

                $("#<%= txbTeam.ClientID %>").click(function (e) {
                    orgPopup = 2;

                    fn_PopupOrgTree();
                });

                $("#btnOrgTree").click(function (e) {
                    orgPopup = 2;

                    fn_PopupOrgTree();
                });
                
                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_CellOrg = function (cell, formatterParams, onRendered) {
            var html = '';
            var value = cell.getValue();
            cellNo = cell.getRow().getData().id;

            html = value;
            html += "&nbsp;<input id='btnCellOrg' type='button' onclick='fn_CellOrgPopup(\"" + cellNo + "\");' value='+' class='btn-plus' style='height:23px;line-height:0px;font-size:15px;'>";
            return html;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var fn = '';
            cellNo = cell.getRow().getData().id;

            fn = "fn_CellRegTran('I','" + cellNo + "')";
            html = "<input id='btnCellReg' type='button' onclick=" + fn + " value='저장' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
            fn = "fn_CellRegTran('D','" + cellNo + "')";
            html += "&nbsp;<input id='btnCellReg' type='button' onclick=" + fn + " value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";
            return html;
        }

        var fn_GridInit = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 160, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "담당조직/담당자", field: "EMPNM", sorter: "string", width: 300 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "권한조직", field: "ORGNM", sorter: "string", formatter: fn_CellOrg },
                { headerHozAlign: "center", hozAlign: "center", title: "부서장권한", field: "UPPERYN", sorter: "string", width: 140 },
                { formatter: fn_CellReg, width: 220, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {

        }



        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("등록 하시겠습니까?")) {
                    var gubun = "I";
                    var orgcd = $("#<%= hfAuthOrgCd.ClientID %>").val();
                    var empno = $("#<%= hfOperator.ClientID %>").val();

                    callGb = 'RegTran';
                    fn_Transaction(gubun, empno, orgcd);
                }
            }
        }

        var fn_CellRegTran = function (gubun, cellNo) {
            if (gubun == "I") {
                if (confirm("저장 하시겠습니까?")) {
                    callGb = 'SaveTran';

                    var row = ktsGrid.getRow(cellNo);
                    var rowData = row.getData();
                    var empno = rowData.EMPNO;
                    var orgcd = rowData.ORGCD;

                    fn_Transaction(gubun, empno, orgcd);
                }

            }
            else if (gubun == "D") {
                if (confirm("삭제 하시겠습니까?")) {
                    callGb = 'DelTran';

                    var row = ktsGrid.getRow(cellNo);
                    var rowData = row.getData();
                    var empno = rowData.EMPNO;
                    var orgcd = '';

                    fn_Transaction(gubun, empno, orgcd);
                }
            }
        }

        var fn_CellOrgPopup = function (id) {
            orgPopup = 99;
            cellNo = id;
            fn_PopupOrgTree();
        }


        var ajaxCallBack = function (json) {
            if (callGb == 'RegTran') {
                callGb = '';
                ktsGridAlert("등록을 완료했습니다.");
                fn_Search();
            }
            else if (callGb == 'SaveTran') {
                callGb = '';
                ktsGridAlert("저장을 완료했습니다.");
                fn_Search();
            }
            else if (callGb == 'DelTran') {
                callGb = '';
                ktsGridAlert("삭제를 완료했습니다.");
                fn_Search();
            }
        }

        var fn_PopupAuthTree = function () {
            var form = "/Common/SearchUser";
            var target = "SearchUser";
            var nWidth = 840;
            var nHeight = 500;

            var param = {
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function () {
            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";
            var param = {
                pOPER: "Y"
            };
            var nWidth = 400;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=hfOperator.ClientID %>").val(empno);
            $("#<%=txbOperator.ClientID %>").val(empnm);
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();

            if (orgPopup == 1) {
                document.getElementById("<%=hfAuthOrgCd.ClientID %>").value = orgcd;
                __doPostBack("<%=updPanelAuthOrg.ClientID %>", "");
            }
            else if (orgPopup == 2) {
                document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
                __doPostBack("<%=updPanelSearch.ClientID %>", "");
            }
            else if (orgPopup == 99) {
                document.getElementById("<%=hfCellOrgCd.ClientID %>").value = orgcd;
                __doPostBack("<%=updPanelHidden.ClientID %>", "");
                var orgnm = $("#<%= hfCellOrgNm.ClientID %>").val();;
            }

            orgPopup = 0;
        }

        var fn_GridUpdate = function (orgcd, orgnm) {

            ktsGrid.updateData([{
                id: cellNo, ORGCD: orgcd, ORGNM: orgnm
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            cellNo = 0;
        }

        var fn_RegChk = function () {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();
            var EmpNo = $("#<%= hfOperator.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("권한조직을 선택해 주세요.");
                return false;
            }
            else if (EmpNo.length == 0) {
                ktsGridAlert("담당자를 선택해 주세요.");
                return false;
            }
            else {
                return true;
            }
        }

        var fn_SearchChk = function() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Transaction = function (gubun, empno, orgcd) {
            var bUpperYn = $("#<%= chkUpperYnR.ClientID %>").is(":checked");
            var upperYn = 'Z';

            if (callGb == 'RegTran') {
                if (bUpperYn) {
                    upperYn = 'Y';
                }
                else {
                    upperYn = 'N';
                }
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = empno;
            aData[2] = orgcd.replace(/,/gi, "^");
            aData[3] = upperYn;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetToolOperTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[1] = $("#<%= txbOperNm.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }


    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
				<span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelAuthOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>권한조직</label>
                            <asp:TextBox ID="txtAuthOrgNm" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <input id="btnAuthTree" type="button" class="btn-plus" title="검색" />
                            <asp:HiddenField ID="hfAuthOrgCd" runat="server" OnValueChanged="hfAuthOrgCd_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfAuthOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelOperator" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>담당자</label>
                            <asp:TextBox ID="txbOperator" runat="server" ReadOnly="true"></asp:TextBox>
                            <input id="btnAuthOrgTree" type="button" class="btn-plus"" title="검색" />
                            <asp:HiddenField ID="hfOperator" runat="server" OnValueChanged="hfOperator_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOperator" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
				<span class="ickbox" style="padding-right:20px">
                    <asp:UpdatePanel ID="updPanelUpperYnR" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>부서장권한</label>
                            <asp:CheckBox ID="chkUpperYnR" runat="server"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </span>
                <input id="btnReg" type="button" class="btn-green last" style="float:right;" value="등록" />
		    </fieldset>
        </div>
        <div class="searchbox">
		    <fieldset>       
                <span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <input id="btnOrgTree" type="button" class="btn-plus" style="width:40px" title="검색" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
				<span class="inpbox">
					<label>담당조직/담당자</label>
                    <asp:TextBox ID="txbOperNm" runat="server"></asp:TextBox>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="EMPNM">담당조직/담당자</option>
                            <option value="ORGNM">권한조직</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <asp:UpdatePanel ID="updPanelHidden" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hfCellOrgCd" runat="server" OnValueChanged="hfCellOrgCd_ValueChanged"  />
                        <asp:HiddenField ID="hfCellOrgNm" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hfCellOrgCd" EventName="ValueChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>

