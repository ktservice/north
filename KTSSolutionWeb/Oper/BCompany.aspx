﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BCompany.aspx.cs" Inherits="KTSSolutionWeb.BCompany" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function RegBCompID() {
            var nWidth = 850;
            var nHeight = 400;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Oper/RegBCompID", null);

            Popupform.target = "RegBCompID";
            var win = window.open("", "RegBCompID", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function btnDelCheck() {
            if (confirm("B협력사 정보를 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function RefreshList() {
            <%=Page.GetPostBackEventReference(btnSelect)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>          
				<span class="inpbox first">
                    <label>업체명</label>
                    <asp:TextBox ID="txbCompNm" runat="server" Width="400px"></asp:TextBox>
				</span>
                <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조직별 조회 결과</strong>
                <div class="pull-right">
                    <div class="filebox">
                        <input type="text" id="txtfu" />
                        <span class="file">
                            <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
                            <label for="fu">찾아보기</label>
                        </span>
                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_ServerClick" class="btn-black" Text="업로드" />
                    </div>
                    <div class="btnset">
                        <button id="btnRegID" onclick="RegBCompID();" type="button" class="btn-green">등록</button>
                        <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
		    <!-- S:scrollbox -->
		    <div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>업체명</th>
                                    <th>이름</th>
                                    <th>사번</th>
                                    <th>본부</th>
                                    <th>지사</th>
                                    <th>국사</th>
                                    <th>삭제</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("COMPNM") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td>
                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("BBNM") %></td>
                                            <td><%# Eval("JSNM") %></td>
                                            <td><%# Eval("OFFICENM") %></td>
                                            <td>
                                                <asp:Button id="btnDelete" runat="server" OnClientClick="return btnDelCheck();" OnClick="btnDelete_ServerClick" class="btn-del" Text="삭제" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>