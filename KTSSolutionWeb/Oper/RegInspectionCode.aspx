﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegInspectionCode.aspx.cs" Inherits="KTSSolutionWeb.RegInspectionCode"  EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title id="title" runat="server">구분코드 관리</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240624" rel="stylesheet" />

    <style>
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .th-line {width: 100px;border-right: 1px solid #dfdfdf;}
        .div-Screenlock {position: absolute;top:0px;width: 100%;height: 100%;background-color: #ffffff !important;z-index: 99;opacity: 0.7;overflow: hidden;display:none}
        .div-modal {position: absolute;width: 90%; height: 95px;top: 30%;left: 5%; background: rgb(255, 255, 255); border-radius: 10px; border: 1px solid gray;z-index:100;}
        .div-modal-inner {width:90%;text-align:center;margin-left:5%;}
        .div-modal-content {margin-top:30px;padding-bottom:5px;float:left;}
        .div-modal-content > p {width: 70px; height: 30px; padding-top: 5px;float: left;font-weight:bold;}
        .div-modal-content > input[type="text"] {width:320px;height:30px;padding-left:5px;padding-right:5px;}
        .btn-add {display: inline-block;width: 40px;height: 30px;margin-top:30px;background: #fff;border: 1px solid #2CBBB7;border-radius: 2px;color: #2CBBB7;font-size: 14px;}
        .btn-close {display: inline-block;width: 40px;height: 30px;margin-top:30px;background: #fff;border: 1px solid gray;border-radius: 2px;color: gray;font-size: 14px;}

    </style>
    <script type="text/javascript">
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_SetControls();
            },
            eventbind: function () {

                $("#ddlChkType").change(function (e) {
                    var chkType = $("#ddlChkType").val();
                    var tranGbn = '';

                    $("#btnAdd1").removeAttr("onclick");
                    $("#btnDelChkType").removeAttr("onclick");

                    if (chkType != "") {
                        $("#btnModal1Open").val("수정");
                        $("#btnDelChkType").show();
                        $("#btnDelChkType").attr("onclick", "fn_DelCode('" + chkType + "', 'C')");

                        tranGbn = "U";
                    }
                    else {
                        $("#btnModal1Open").val("추가");
                        $("#btnDelChkType").hide();

                        tranGbn = "I";
                    }

                    $("#btnAdd1").attr("onclick", "fn_Modal1Add('" + tranGbn + "')");
                });

                $("#ddlUpperList").change(function (e) {
                    var upperList = $("#ddlUpperList").val();
                    var tranGbn = '';

                    $("#btnAdd2").removeAttr("onclick");
                    $("#btnDelUpperList").removeAttr("onclick");

                    if (upperList != "") {
                        $("#btnModal2Open").val("수정");
                        $("#btnDelUpperList").show();
                        $("#btnDelUpperList").attr("onclick", "fn_DelCode('" + upperList + "', 'U')");

                        tranGbn = "U";
                    }
                    else {
                        $("#btnModal2Open").val("추가");
                        $("#btnDelUpperList").hide();

                        tranGbn = "I";
                    }

                    $("#btnAdd2").attr("onclick", "fn_Modal2Add('" + tranGbn + "')");
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });
            } 
        };

        var fn_Load = function () {
        }

        var fn_SetControls = function () {
            fn_GetChkType();
        };
        
        var fn_GetChkType = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetChkType');

            callGb = 'GetChkType';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetUpperList = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUpperList');

            callGb = 'GetUpperList';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetChkType') {
                    callGb = '';
                    fn_ControlBind_ChkType(json);
                    $("#ddlChkType").change();

                    fn_GetUpperList();
                }
                else if (callGb == 'GetUpperList') {
                    callGb = '';
                    fn_ControlBind_UpperList(json);
                    $("#ddlUpperList").change();
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {

                        fn_ModalClose();

                        if (callGb == 'InsChkTypeOk') {
                            callGb = '';
                            alert('검사구분 등록이 완료되었습니다.');
                            fn_GetChkType();
                        }
                        else if (callGb == 'UpdChkTypeOk') {
                            callGb = '';
                            alert('검사구분 수정이 완료되었습니다.');
                            fn_GetChkType();
                        }
                        else if (callGb == 'DelChkTypeOk') {
                            callGb = '';
                            alert('선택한 검사구분이 삭제 되었습니다.');
                            fn_GetChkType();
                        }
                        else if (callGb == 'InsUpperListOk') {
                            callGb = '';
                            alert('점검구분 등록이 완료되었습니다.');
                            fn_GetUpperList();
                        }
                        else if (callGb == 'UpdUpperListOk') {
                            callGb = '';
                            alert('점검구분 수정이 완료되었습니다.');
                            fn_GetUpperList();
                        }
                        else if (callGb == 'DelUpperListOk') {
                            callGb = '';
                            alert('선택한 점검구분 삭제 되었습니다.');
                            fn_GetUpperList();
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var fn_ControlBind_ChkType = function (json) {
            var chkType = $("#<%= hdfChkType.ClientID %>").val();

            var selector = $("#ddlChkType");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.CHKTYPECD;
                options.text = item.CHKTYPENM;

                if (chkType == item.CHKTYPECD) {
                    options.selected = true;
                }

                if (item.CHKTYPENM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });

            var tranGbn = '';

            $("#btnAdd1").removeAttr("onclick");

            if (selector.val() != "") {
                $("#btnModal1Open").val("수정");
                $("#btnDelChkType").show();
                tranGbn = "U";
            } else {
                $("#btnModal1Open").val("추가");
                tranGbn = "I";
            }

            $("#btnAdd1").attr("onclick", "fn_Modal1Add('" + tranGbn + "')");
        };

        var fn_ControlBind_UpperList = function (json) {
            var upperList = $("#<%= hdfUpperList.ClientID %>").val();
            var selector = $("#ddlUpperList");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.UPPERLISTCD;
                options.text = item.UPPERLISTNM;

                if (upperList == item.UPPERLISTCD) {
                    options.selected = true;
                }

                if (item.UPPERLISTNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });

            $("#btnModal2Open").show();
            $("#btnAdd2").removeAttr("onclick");

            var tranGbn = '';

            if (selector.val() != "") {
                $("#btnModal2Open").val("수정");
                $("#btnDelUpperList").show();
                tranGbn = "U";
            } else {
                $("#btnModal2Open").val("추가");
                tranGbn = "I";
            }

            $("#btnAdd2").attr("onclick", "fn_Modal2Add('" + tranGbn + "')");
        };

        var fn_ParentSearch = function () {
            window.close();
        };

        var fn_DelCode = function (code, codeType) {
            var ConfMsg = '';
            var sUrl = '';

            if (code == "" || code == undefined || codeType == "" || codeType == undefined) {
                alert('잘못된 접근입니다.');
                return false;
            }
            else {
                if (codeType == 'C') {
                    ConfMsg = '선택한 검사구분을 삭제 하시겠습니까?';
                    callGb = 'DelChkTypeOk'
                    sUrl = 'DelChkType';
                }
                else if (codeType == 'U') {
                    ConfMsg = '선택한 점검구분을 삭제 하시겠습니까?';
                    callGb = 'DelUpperListOk'
                    sUrl = 'DelUpperList';
                }
                else {
                    alert('잘못된 접근입니다.');
                    return false;
                }

                if (confirm(ConfMsg)) {
                    var aData = [];

                    aData[0] = code;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                    ajaxCallEx(jsonData, ajaxUrl, false);
                }
                else {
                    return false;
                }
            }
        };

        var fn_RegCode = function (code, codeNm, preCodeNm, codeType, type) {
            var ConfMsg = '';
            var codeTypeNm = '';

            var sUrl = '';

            var aData = [];

            if (code == "" || code == undefined || codeType == "" || codeType == undefined || type == "" || type == undefined) {
                alert('잘못된 접근입니다.');
                return false;
            }
            else {
                if (codeType == 'ChkType') {
                    codeTypeNm = '검사구분'
                }
                else if (codeType == 'UpperList') {
                    codeTypeNm = '점검구분';
                }
                else {
                    alert('잘못된 접근입니다.');
                    return false;
                }
            }
            
            if (codeNm == '') {
                alert(codeTypeNm + '명을 입력해주세요.');
                return false;
            }
            else {
                aData[0] = code;
                aData[1] = codeNm;

                if (type == 'I') {
                    ConfMsg = codeTypeNm + '에 ' + codeNm + ' 를 신규 추가 하시겠습니까?';

                    callGb = 'Ins' + codeType + 'Ok';
                    sUrl = "Ins" + codeType;
                }
                else if (type == 'U') {
                    ConfMsg = codeTypeNm + '의 ' + preCodeNm + '을 ' + codeNm + ' 으로 변경 하시겠습니까?';

                    callGb = 'Upd' + codeType + 'Ok';
                    sUrl = "Upd" + codeType;
                }

                if (confirm(ConfMsg)) {
                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
                else {
                    return false;
                }
            }
        };

        var fn_Modal1Open = function () {
            var chkType = $("#ddlChkType option:selected").val();
            var chkTypeNm = "";

            if (chkType == "") {
                grpNm = "";
            }
            else {
                chkTypeNm = $("#ddlChkType option:selected").text();
            }

            $("#txbChkTypeNm").val(chkTypeNm);

            $("#divScreenLock").show();
            $("#divModal1").show();
        };

        var fn_Modal2Open = function () {
            var upperList = $("#ddlUpperList option:selected").val();
            var upperListNm = "";

            if (upperList == "") {
                upperListNm = "";
            }
            else {
                upperList = $("#ddlUpperList option:selected").text();
            }

            $("#txbUpperListNm").val(upperListNm);

            $("#divScreenLock").show();
            $("#divModal2").show();
        };


        var fn_ModalClose = function () {
            $("#divScreenLock").hide();
            $("#divModal1").hide();
            $("#divModal2").hide();
        };

        var fn_Modal1Add = function (type) {
            var chkType = '';
            var chkTypeNm = $("#txbChkTypeNm").val();
            var preChkTypeNm = '';

            if (type == 'I') {
                chkType = $("#ddlChkType option:last").val();
            }
            else if (type == 'U') {
                chkType = $("#ddlChkType option:selected").val();
                preChkTypeNm = $("#ddlChkType option:selected").text();
            }

            fn_RegCode(chkType, chkTypeNm, preChkTypeNm, 'ChkType', type);
        };

        var fn_Modal2Add = function (type) {
            var upperList = '';
            var upperListNm = $("#txbUpperListNm").val();
            var preUpperListNm = '';

            if (type == 'I') {
                upperList = $("#ddlUpperList option:last").val();
            }
            else if (type == 'U') {
                upperList = $("#ddlUpperList option:selected").val();
                preUpperListNm = $("#ddlUpperList option:selected").text();
            }

            fn_RegCode(upperList, upperListNm, preUpperListNm, 'UpperList', type);
        };
    </script>
</head>
<body style="overflow:hidden;">
    <form id="form1" runat="server" >
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>구분코드 관리</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self);">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents" style="padding-top: 10px;">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th class="th-line">검사구분</th>
                                            <td style="text-align:left;">
                                                <p class="optionbox">
                                                    <select id="ddlChkType" style="width:200px;"></select>
                                                    <asp:HiddenField ID="hdfChkType" runat="server" Value="" />
                                                </p>
                                            </td>
                                            <td style="border-left:none;text-align:left;">
                                                <input id="btnModal1Open" type="button" class="btn-save" value="추가" onclick="fn_Modal1Open();" />
                                                <input id="btnDelChkType" type="button" class="btn-del" style="display:none;" value="삭제" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">점검구분</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <select id="ddlUpperList" style="width:200px;"></select>
                                                    <asp:HiddenField ID="hdfUpperList" runat="server" Value="" />
                                                </p>
                                            </td>
                                            <td style="border-left:none;text-align:left;">
                                                <input id="btnModal2Open" type="button" class="btn-save" value="추가" style="display:none;" onclick="fn_Modal2Open();" />
                                                <input id="btnDelUpperList" type="button" class="btn-del" style="display:none;" value="삭제" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <div class="btncenter">
                            <input id="btnClose" type="button" class="btn-gray" value="닫기" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <div id="divScreenLock" class="div-Screenlock"></div>
        <!-- S: divModal1 -->
		<div id="divModal1" class="div-modal" style="display:none;">
            <!-- S:popcontents -->
            <div class="div-modal-inner">
                <div class="div-modal-content">
                    <p>검사구분 : </p><input type="text" id="txbChkTypeNm" value="" maxlength="50"/>
                </div>
                <!-- S:btncenter -->
                <div class="btncenter">
                    <input id="btnAdd1" type="button" class="btn-add" value="저장" />
                    <input id="btnClose1" type="button" class="btn-close" onclick="fn_ModalClose();" value="닫기" />
                </div>
                <!-- //E:btncenter -->
            </div>
		</div>
        <!-- E: divModal1 -->
        <!-- S: divModal2 -->
		<div id="divModal2" class="div-modal" style="display:none;">
            <!-- S:popcontents -->
            <div class="div-modal-inner">
                <div class="div-modal-content">
                    <p>점검구분 : </p><input type="text" id="txbUpperListNm" value="" maxlength="50"/>
                </div>
                <!-- S:btncenter -->
                <div class="btncenter">
                    <input id="btnAdd2" type="button" class="btn-add" value="저장"/>
                    <input id="btnClose2" type="button" class="btn-close" onclick="fn_ModalClose();" value="닫기" />
                </div>
                <!-- //E:btncenter -->
            </div>
		</div>
        <!-- E: divModal2 -->
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
