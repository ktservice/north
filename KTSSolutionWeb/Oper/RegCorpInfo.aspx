﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegCorpInfo.aspx.cs" Inherits="KTSSolutionWeb.RegCorpInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사업자 정보 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });


        var page = {
            init: function () {
                fn_Init();
            },
            eventbind: function () {
                $("#txbCorpNo").keyup(function (e) {
                    var bChk = false;

                    bChk = fn_ChkNumber(e, "CORPNO");

                    if (!bChk) {
                        $("#txbCorpNo").val('');
                    }
                    return bChk;
                });

                $("#txbTelNo").keyup(function (e) {
                    var bChk = false;

                    bChk = fn_ChkNumber(e, "");

                    if (!bChk) {
                        $("#txbTelNo").val('');
                    }

                    return fn_ChkNumber(e, "");
                });

                $("#txbPhoneNo").keyup(function (e) {
                    var bChk = false;

                    bChk = fn_ChkNumber(e, "");

                    if (!bChk) {
                        $("#txbTelNo").val('');
                    }

                    return fn_ChkNumber(e, "");
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        };

        var fn_ChkNumber = function (e, txbtype) {
            try {
                var bChk = false;
                const regex = /^[ㄱ-ㅣ가-힣a-zA-Z]+$/;
                var keyCd = e.keyCode;
                var keyVal = e.key;

                if (txbtype == "CORPNO" && keyVal.toUpperCase() == "S") {
                    bChk = true;
                }
                else {
                    if (keyVal == '-') {
                        bChk = true;
                    }
                    else if ((keyCd >= 48 && keyCd <= 57) || keyCd == 8 || keyCd == 46) {
                        if (keyCd == 46 || keyCd == 8) {
                            bChk = true;
                        }
                        else {
                            if (regex.test(keyVal)) {
                                bChk = false;
                            }
                            else {
                                bChk = true;
                            }
                        }
                    }
                    else {
                        if (regex.test(keyVal)) {
                            bChk = false;
                        }
                        else {
                            bChk = true;
                        }
                    }
                }
            }
            catch {
                bChk = false;
            }

            return bChk;
        };


        $(document).ready(function () {
        });

        var ajaxCallBack = function (json) {
            if (callGb != "") {
                if (callGb == "InsOk") {
                    $.each(JSON.parse(json), function (idx, item) {
                        if (item.ERRCD == "00") {
                            if (callGb == "InsOk") {
                                ktsGridId = 'windowpop-wrap';
                                ktsGridAlert("사업자 정보가 등록되었습니다.", 2000);
                            }

                            fn_PageInit();
                            opener.fn_Search();
                        }
                        else {
                            ktsGridAlert(item.ERRMSG);
                        }
                    });
                }
            }
        }

        var fn_PageInit = function () {
            $("#ddlOrderCode").val("");
            $("#txbCorpNm").val("");
            $("#txbCorpNo").val("");
            $("#txbAddr").val("");
            $("#txbAddrDtl").val("");
            $("#txbTelNo").val("");
            $("#txbPhoneNo").val("");
        }

        var fn_InsCorpInfo = function () {
            var orderCd = $("#<%= ddlOrderCode.ClientID %> option:selected").val();
            var corpNm = $("#txbCorpNm").val();
            var corpNo = $("#txbCorpNo").val();

            if (orderCd.length == 0 || corpNm.length == 0 || corpNo.length == 0) {
                ktsGridAlert("오더구분, 사업자명, 사업자등록코드는 필수 입력값입니다.");
                return false;
            }
            else {
                var addr = $("#txbAddr").val();
                var addrdtl = $("#txbAddrDtl").val();
                var telno = $("#txbTelNo").val();
                var phoneno = $("#txbPhoneNo").val();

                var aData = [];

                aData[0] = orderCd;
                aData[1] = corpNm;
                aData[2] = corpNo;
                aData[3] = addr;
                aData[4] = addrdtl;
                aData[5] = telno;
                aData[6] = phoneno;

                callGb = "InsOk";

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "InsCorpInfo");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>사업자 정보 등록</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th style="width:130px">오더구분&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlOrderCode" style="width:150px;" runat="server" TabIndex="0">
                                                        <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="VAN" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="K-VIZERO" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="BIZ" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Gem" Value="4"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:130px">사업자명&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbCorpNm" style="width:190px;" maxlength="100" tabindex="1" />
                                                </p>
                                            </td>
                                            <th style="width:130px">사업자등록코드&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbCorpNo" style="width:190px;" maxlength="12" tabindex="2" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">주소</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <input type="text" id="txbAddr" style="width:500px;margin-right:10px;" maxlength="1500" tabindex="3" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">상세주소</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <input type="text" id="txbAddrDtl" style="width:500px;margin-right:10px;" maxlength="1500" tabindex="4" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:130px">연락처</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbTelNo" style="width:190px;" maxlength="30" tabindex="5" />
                                                </p>
                                            </td>
                                            <th style="width:130px">휴대폰번호</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbPhoneNo" style="width:190px;" maxlength="30" tabindex="6" />
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력값입니다.
                        <div class="btncenter">
                            <input id="btnReg" type="button" onclick="fn_InsCorpInfo();" value="저장" class="btn-green" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
