﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaterialUpload.aspx.cs" Inherits="KTSSolutionWeb.MaterialUpload" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>

    <script type="text/javascript">
		$(document).ready(function () {
            $(document).ready(function () {
                page.eventbind();
            });

            var page = {
                eventbind: function () {
                    $("#btnTemp").click(function (e) {
                        fn_DownloadTemplate();
                    });

                    $("#btnCode").click(function (e) {
                        fn_PopupCode();
                    });
                    
                }
            };

		});

        var fn_PopupCode = function () {
            var form = "/Oper/MaterialCodePopup";
            var target = "MaterialCodePopup";
            var param = {
            };

            var nWidth = 900;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_DownloadTemplate = function () {
            window.location.href = '/Template/자재 불출 Excel Upload Template.xlsx';
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-upload-management -->
        <div class="page-upload-management">
            <div class="list-top">
                <strong>자재정보</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" type="button" title="최종 등록 완료" class="submit-btn btn-green">최종 등록 완료</button>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="upload-list">
                        <ul>
                            <li>
					            <strong>자재마스터</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuMaster"/>
										<span class="file">
											<input type="file" id="fuMaster" name="fuMaster" onchange="document.getElementById('txtfuMaster').value=this.value;" /> 
											<label for="fuMaster">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
									</p>
								</div>
							</li>
                            <li>
					            <strong>자재사용 작업자별</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuUseWorker"/>
										<span class="file">
											<input type="file" id="fuUseWorker" name="fuUseWorker" onchange="document.getElementById('txtfuUseWorker').value=this.value;" /> 
											<label for="fuUseWorker">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
									</p>
								</div>
							</li>
                            <li>
					            <strong>자재불출</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
										<input type="text" id="txtfuOut"/>
										<span class="file">
											<input type="file" id="fuOut" name="fuOut" onchange="document.getElementById('txtfuOut').value=this.value;" /> 
											<label for="fuOut">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
										</span>
                                        <div class="fileBtnWrap">
                                            <input id="btnTemp" type="button" class="btn-green last" value="양식다운로드" />
                                            <input id="btnCode" type="button" class="btn-green last" value="자재코드정보" />
                                        </div>
									</p>
								</div>
							</li>
                            <li>
					            <strong>단말마스터</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							            <input type="text" id="txtfuTerminal"/>
							            <span class="file">
								            <input type="file" id="fuTerminal" name="fuTerminal" onchange="document.getElementById('txtfuTerminal').value=this.value;" /> 
								            <label for="fuTerminal">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							            </span>
                                    </p>
                                </div>
                            </li>
						</ul>
					</div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </asp:UpdatePanel>
		</div>
		<!-- E:page-upload-management -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
