﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class RegBCompID : PageBase
    {
        public string IDCheck
        {
            get
            {
                if (ViewState["IDCheck"] != null)
                {
                    return ViewState["IDCheck"].ToString();
                }
                else
                {
                    return "N";
                }
            }
            set
            {
                ViewState["IDCheck"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private bool ValidationCheck(ref string strMsg)
        {
            bool bCheck = true;

            string strCompNm = this.txbCompNm.Text;
            string strEmpNo = this.txbEmpNo.Text;
            string strEmpNm = this.txbEmpNm.Text;
            string strBBNm = this.txbTxbBBNm.Text;
            string strJSNm = this.txbTxbJSNm.Text;
            string strOfficeNm = this.txbOfficeNm.Text;

            if (strCompNm.Equals(""))
            {
                strMsg = "업체명을 입력해 주세요.";
                return false;
            }

            if (strEmpNo.Equals(""))
            {
                strMsg = "사번을 입력해주세요.";
                return false;
            }

            if (!IDCheck.Equals("Y"))
            {
                strMsg = "사번 확인을 해주세요.";
                return false;
            }

            if (strEmpNm.Equals(""))
            {
                strMsg = "이름을 입력해주세요.";
                return false;
            }

            if (strBBNm.Equals(""))
            {
                strMsg = "본부를 입력해주세요.";
                return false;
            }

            if (strJSNm.Equals(""))
            {
                strMsg = "지사를 입력해주세요.";
                return false;
            }

            if (strOfficeNm.Equals(""))
            {
                strMsg = "국사를 입력해주세요.";
                return false;
            }

            return bCheck;
        }

        protected void BtnIDCheck_Click(object sender, EventArgs e)
        {
            string strEmpNo = Request.Form[txbEmpNo.UniqueID];
            bool bExistUser = false;
            string strMsg = "";

            DataSet ds = new DataSet();

            try
            {
                if (strEmpNo.Trim().Length == 0)
                {
                    strMsg = "id를 입력해주세요.";
                }
                else
                {
                    using (UserMgmt userReg = new UserMgmt())
                    {
                        ds = userReg.GetOperBcompUserInfo(strEmpNo);

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bExistUser = true;
                            }
                        }
                    }

                    if (bExistUser)
                    {
                        strMsg = "이미 등록된 ID 입니다.";
                    }
                    else
                    {
                        IDCheck = "Y";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "IDChkOK", "alert('등록 가능한 사번 입니다.');", true);
                    }
                }

                if (strMsg.Length > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void BtnRegedit_Click(object sender, EventArgs e)
        {
            try
            {
                string strMsg = "";

                if (ValidationCheck(ref strMsg))
                {
                    string strCompNm = this.txbCompNm.Text;
                    string strEmpNo = this.txbEmpNo.Text;
                    string strEmpNm = this.txbEmpNm.Text;
                    string strBBNm = this.txbTxbBBNm.Text;
                    string strJSNm = this.txbTxbJSNm.Text;
                    string strOfficeNm = this.txbOfficeNm.Text;

                    using (UserMgmt userReg = new UserMgmt())
                    {
                        userReg.InsertBCompUserInfo(strCompNm, strEmpNo, strEmpNm, strBBNm, strJSNm, strOfficeNm);
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOK", "alert('B협력사 등록이 완료되었습니다.');RefreshList();window.close();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}