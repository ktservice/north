﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PortalSalesPolicyPopup.aspx.cs" Inherits="KTSSolutionWeb.PortalSalesPolicyPopup" enableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>판매정책</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .salePolicyFile {
          width: 90% !important;
        }

        .inpbox {
            display: inline;
        }

        
        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#selClass").change(function (e) {
                    $("#hdnClassCd").val(this.value);
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });
                
                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';

            if ($("#hdnGubun").val() === 'I') {
                $("#tblReg").show();
                $("#tblView").hide();
            }
            else {
                $("#tblReg").hide();
                $("#tblView").show()
                $("#btnMod").hide();;
                $("#selClass").attr('disabled', true);
            }

        }

        var fn_Load = function () {
            fn_SalesClass('selClass');
        }

        var fn_ModTran = function () {
            if (fn_ModChk()) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_SalesTransaction('M');
                }
            }
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("등록을 진행하시겠습니까?")) {

                    __doPostBack('<%= btnRegTran.UniqueID %>', '');

                    return true;
                }
                else {
                    return false;
                }
            }
        }

        var fn_RegChk = function () {
            var fileCheck = false;
            var fileInput = $('.salePolicyFile');
            for (var i = 0; i < fileInput.length; i++) {
                if (fileInput[i].files.length > 0) {
                    fileCheck = true;
                    break;
                }
            }

            if (!fileCheck) {
                ktsGridAlert("선택된 파일이 없습니다.");
            }

            return fileCheck;
        }


        var fn_ModChk = function () {
            var classCd = $('#hdnOClassCd').val();
            var chgClassCd = $('#selClass').val();

            if (classCd === chgClassCd) {
                alert("같은 분류명입니다.");
                return false;
            }

            return true;
        }


        var fn_SalesClass = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetSalesClass');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_SalesTransaction = function (gubun) {

            var classCd = $('#hdnOClassCd').val();
            var chgClassCd = $('#selClass').val();
            
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = classCd;
            aData[2] = chgClassCd;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetSalesTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {

            if (callGb == 'selClass') {

                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CLASSCD + "'>" + item.CLASSNM + "</option>");
                });

                if ($("#hdnGubun").val() === 'I') {
                    $("#selClass option:eq(0)").prop("selected", true);
                    $("#hdnClassCd").val($("#selClass").val());
                }
                else {
                    obj.val($("#hdnClassCd").val()).prop("selected", true);
                }

                callGb = '';
            }
            else if (callGb == 'ModTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        alert('변경되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';

                SendParentsForm();
            }
        }

        var fn_RegOk = function () { 
            alert('등록되었습니다.');
            check = 'ok';
            SendParentsForm();
        }

        function SendParentsForm() {

            opener.SetDispItem(check);
            window.close();
        }


        var fn_AddFile = function () {
            var script = '';

            var a = $(".salePolicyFile").length;
            if (a >= 10) {
                alert("더이상 추가 할 수 없습니다.");
                return false;
            }

            script += '<div>';
            script += '<span class="inpbox">';
            script += '<input class="salePolicyFile" id="fUpload" name="fUpload" title="sale-policy-file" type="file" value="" accept="image/*" />';
            script += '</span>';
            //script += '<button class="btn-del" onclick="javascript:fn_DelFile(this);" type="button">삭제</button>';
            script += '<img src="/Resource/images/icon_minus_02.png" onclick="javascript:fn_DelFile(this);" alt="삭제" />';
            script += '</div>';

            $("#fileAdd").append(script);
        }

        var fn_DelFile = function (a) {
            $(a).parent().remove();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>판매정책</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblClass">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">분류</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selClass">
                                                    </select>
                                                </span>
                                                <input type="hidden" id="hdnGubun" runat="server" />
                                                <input type="hidden" id="hdnClassCd" runat="server" />
                                                <input type="hidden" id="hdnOClassCd" runat="server" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">이미지 업로드</th>
                                            <td class="td-left">
                                                <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <span class="inpbox">
                                                            <input class="salePolicyFile" id="fUpload" name="fUpload" type="file" value="" accept="image/*"/>
                                                        </span>
                                                        <button class="btn-plus" onclick="javascript:fn_AddFile(this);" type="button" id="btnFileAdd">입력 폼 추가 </button>
                                                        <div id="fileAdd"></div>
                                                    </ContentTemplate>
                                                     <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnRegTran" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                                <asp:Button id="btnRegTran" Visible="false" runat="server" OnClick="btnRegTran_Click" ></asp:Button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="tblView">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">등록일</th>
                                            <td class="td-left">
                                                <div id="fileRegDt"><asp:Label ID="lblFileRegDt" runat="server"></asp:Label></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">첨부파일</th>
                                            <td class="td-left">
                                                <div id="fileNmList"><asp:Label ID="lblFileNmList" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="divImg" runat="server">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>