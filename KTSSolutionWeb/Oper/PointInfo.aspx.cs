﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class PointInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetYearsDropDownList();

                GetYearsPoint();
                GetProvidePoint();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetYearsDropDownList()
        {
            try
            {
                int nStartYear = DateTime.Now.Year - 4;
                int nEndYear = DateTime.Now.Year + 5;

                for (int i = nStartYear; nStartYear <= nEndYear; nStartYear++)
                {
                    ListItem item = new ListItem();

                    item.Value = nStartYear.ToString();
                    item.Text = nStartYear.ToString();

                    ddlYears.Items.Add(item);
                }

                ddlYears.SelectedValue = DateTime.Now.Year.ToString();

                updPanelDDL1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        private void SetPointYearsDropDownList()
        {
            try
            {
                ddlPointYears.Items.Clear();

                if (paging1.Dt.Rows.Count > 0)
                {
                    for (int i = 0; i < paging1.Dt.Rows.Count; i++)
                    {
                        ListItem item = new ListItem();

                        item.Value = paging1.Dt.Rows[i]["YEARS"].ToString();
                        item.Text = paging1.Dt.Rows[i]["YEARS"].ToString();

                        ddlPointYears.Items.Insert(0, item);
                    }
                }
                else
                {
                    ListItem item = new ListItem();

                    item.Value = "";
                    item.Text = "연단위 포인트를 추가하세요.";

                    ddlPointYears.Items.Add(item);
                }

                if (ddlPointYears.Items.FindByValue(DateTime.Now.Year.ToString()) == null)
                {
                    ddlPointYears.SelectedIndex = 0;
                }
                else
                {
                    ddlPointYears.SelectedValue = DateTime.Now.Year.ToString();
                }


                updPanelDDL2.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetYearsPoint()
        {
            DataSet ds = new DataSet();
            try
            {
                paging1.bPreRender = true;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                paging1.PageNumber = 0;
                paging1.PageSize = 20;

                paging1.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging1.TotalRows = ds.Tables[0].Rows.Count;
                    paging1.Dt = ds.Tables[0];
                }

                paging1.SetPagingDataList();
                SetPointYearsDropDownList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetProvidePoint()
        {
            DataSet ds = new DataSet();
            try
            {
                paging2.bPreRender = true;

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointList();
                }

                paging2.PageNumber = 0;
                paging2.PageSize = 20;

                paging2.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging2.TotalRows = ds.Tables[0].Rows.Count;
                    paging2.Dt = ds.Tables[0];
                }

                paging2.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnInsYearPoint_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string strYears = ddlYears.SelectedValue; 
                //string strPoint = txbPoint.Text;

                if (strYears.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('연도를 선택해주세요.');", true);
                }
                //else if (strPoint.Length == 0)
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('포인트를 입력해주세요.');", true);
                //}
                else
                {
                    DataRow[] dr = paging1.Dt.Select(string.Format("YEARS = {0}", strYears));

                    if (dr.Length > 0)
                    {
                        //2023.09.04 기준포인트가 지급별 사용포인트에 따라 변경됨에 따라 업데이트기능 삭제
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('등록된 연도가 존재합니다.');", true);
                        //using (PointMgmt point = new PointMgmt())
                        //{
                        //    point.UpdYearsPointList(strYears, strPoint);
                        //}
                    }
                    else
                    {
                        using (PointMgmt point = new PointMgmt())
                        {
                            point.InsYearsPointList(strYears);//, strPoint);
                        }

                        GetYearsPoint();

                        ddlYears.SelectedValue = DateTime.Now.Year.ToString();
                        //txbPoint.Text = "";

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SavePoint", "alert('기준연도를 저장완료 했습니다.');", true);
                    }
                }

                updPanel1.Update();
                updPanelDDL1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelYearPoint_ServerClick(object sender, EventArgs e)
        {
            try
            {
                List<string> lstYears = new List<string>();
                bool bDelChk = true;

                for (int i = 0; i < rptResult1.Items.Count; i++)
                {
                    HtmlInputCheckBox cbPoint = (HtmlInputCheckBox)rptResult1.Items[i].FindControl("cbPoint");
                    Label lblYears = (Label)rptResult1.Items[i].FindControl("lblYears");

                    string strYears = lblYears.Text;
                    DataRow[] dr = paging2.Dt.Select(string.Format("YEARS = {0}", strYears));

                    if (!cbPoint.Checked)
                    {
                        continue;
                    }

                    if (dr.Length > 0)
                    {
                        bDelChk = false;
                        lstYears.Clear();
                        break;
                    }

                    lstYears.Add(strYears);
                }

                if (bDelChk)
                {
                    if (lstYears.Count > 0)
                    {
                        using (PointMgmt point = new PointMgmt())
                        {
                            point.DelYearsPointList(lstYears);
                        }

                        GetYearsPoint();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "DeleteYear", "alert('연단위 포인트 삭제가 완료되었습니다.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('삭제 대상이 없습니다.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('지급별 대상목록이 있어 삭제 할 수 없습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnInsUsePoint_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string strYears = ddlPointYears.SelectedValue;
                string strTitle = txbTitle.Text;
                string strStDt = Request.Form[this.txbStDt.UniqueID].Replace("-", "");
                string strEnDt = Request.Form[this.txbEnDt.UniqueID].Replace("-", "");


                if (strYears.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('연단위 포인트를 추가후 지급별 목록 등록이 가능합니다.');", true);
                }
                if (strTitle.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('제목을 입력해주세요.');", true);
                }
                else if (strStDt.Length == 0 || strEnDt.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('일자를 입력해주세요.');", true);
                }
                else if (strStDt.Length < 8 || strEnDt.Length < 8)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('일자가 잘못되었습니다.');", true);
                }
                else if (!strYears.Equals(strStDt.Substring(0, 4)) || !strYears.Equals(strEnDt.Substring(0, 4)))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('선택한 연도와 일자가 일치해야합니다.');", true);
                } 
                else
                {
                    bool bDateChk = true;
                     
                    for (int i = 0; i < paging2.Dt.Rows.Count; i++)
                    {
                        string strSavedStDt = paging2.Dt.Rows[i]["STDT"].ToString();
                        string strSavedEnDt = paging2.Dt.Rows[i]["ENDT"].ToString();

                        if (int.Parse(strStDt) >= int.Parse(strSavedStDt) && int.Parse(strStDt) <= int.Parse(strSavedEnDt))
                        {
                            bDateChk = false;
                        }
                        else if (int.Parse(strEnDt) >= int.Parse(strSavedStDt) && int.Parse(strEnDt) <= int.Parse(strSavedEnDt))
                        {
                            bDateChk = false;
                        }
                    }

                    if (!bDateChk)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('입력한 기간과 겹치는 진행중인 지급 목록이 있습니다.');", true);
                    }
                    else
                    {
                        using (PointMgmt point = new PointMgmt())
                        {
                            point.InsProvidePointList(strYears, strTitle, strStDt, strEnDt);
                        }

                        GetProvidePoint();

                        ddlPointYears.SelectedIndex = 0;
                        txbTitle.Text = "";

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SavePoint", "alert('지급대상 목록을 저장완료 했습니다.');InitValue();", true);
                        
                    }
                }

                updPanel2.Update();
                updPanelTitle.Update();
                updPanelDDL2.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelUsePoint_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                List<string> lstSeq = new List<string>();
                bool bDel = true;

                for (int i = 0; i < rptResult2.Items.Count; i++)
                {
                    HtmlInputCheckBox cbPoint = (HtmlInputCheckBox)rptResult2.Items[i].FindControl("cbPoint");
                    Label lblYears = (Label)rptResult2.Items[i].FindControl("lblYears");
                    Label lblPointSeq = (Label)rptResult2.Items[i].FindControl("lblPointSeq");

                    if (!cbPoint.Checked)
                    {
                        continue;
                    }

                    using (PointMgmt point = new PointMgmt())
                    {
                        ds = point.GetRequestPointListCheck(lblYears.Text, lblPointSeq.Text);
                    }

                    if (ds.Tables[0].Rows[0]["REQYN"].ToString().Equals("Y"))
                    {
                        bDel = false;
                        break;
                    }
                    else
                    {
                        lstSeq.Add(lblYears.Text + "|" + lblPointSeq.Text);
                    }
                }

                if (bDel)
                {
                    if (lstSeq.Count > 0)
                    {
                        using (PointMgmt point = new PointMgmt())
                        {
                            point.DelProvidePointList(lstSeq);
                        }

                        GetProvidePoint();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "DeletePoint", "alert('지급대상 목록을 삭제완료 했습니다.');InitValue();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('선택한 삭제 목록이 없습니다.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('신청목록이 있는 지급대상이 있어 삭제할 수 없습니다.');", true);
                }

                updPanel2.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void paging1_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging1.bPreRender)
                {
                    if (paging1.Dt != null)
                    {
                        paging1.RepeaterDataBind(rptResult1);
                        updPanel1.Update();
                    }

                    paging1.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void paging2_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging2.bPreRender)
                {
                    if (paging2.Dt != null)
                    {
                        paging2.RepeaterDataBind(rptResult2);

                        for (int i = 0; i < rptResult2.Items.Count; i++)
                        {
                            Label lblTitle = (Label)rptResult2.Items[i].FindControl("lblTitle");
                            Button btnSaveUsePoint = (Button)rptResult2.Items[i].FindControl("btnSaveUsePoint");

                            lblTitle.Attributes.Add("onclick", "RegPointDetailPopup('" + paging2.Dt.Rows[i]["YEARS"].ToString() + "', '" + paging2.Dt.Rows[i]["POINTSEQ"].ToString() + "', '" + lblTitle.Text + "')");

                            AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();
                            asyncBtnSave.ControlID = btnSaveUsePoint.UniqueID;
                            asyncBtnSave.EventName = "Click";

                            updPanel2.Triggers.Add(asyncBtnSave);

                        }
                        updPanel2.Update();
                    }
                    paging2.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSaveUsePoint_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;

                Label lblYears = (Label)btnSave.Parent.FindControl("lblYears");
                Label lblPointSeq = (Label)btnSave.Parent.FindControl("lblPointSeq");
                TextBox txbPointStdt = (TextBox)btnSave.Parent.FindControl("txbPointStdt");
                TextBox txbPointEndt = (TextBox)btnSave.Parent.FindControl("txbPointEndt");

                string strYears = lblYears.Text;
                string strPointSeq = lblPointSeq.Text;
                string strStDt = txbPointStdt.Text;
                string strEnDt = txbPointEndt.Text;

                bool bDateChk = true;

                for (int i = 0; i < paging2.Dt.Rows.Count; i++)
                {
                    if (paging2.Dt.Rows[i]["YEARS"].ToString().Equals(strYears) && paging2.Dt.Rows[i]["POINTSEQ"].ToString().Equals(strPointSeq))
                    {
                        continue;
                    }

                    string strSavedStDt = paging2.Dt.Rows[i]["STDT"].ToString();
                    string strSavedEnDt = paging2.Dt.Rows[i]["ENDT"].ToString();

                    if (int.Parse(strStDt) >= int.Parse(strSavedStDt) && int.Parse(strStDt) <= int.Parse(strSavedEnDt))
                    {
                        bDateChk = false;
                    }
                    else if (int.Parse(strEnDt) >= int.Parse(strSavedStDt) && int.Parse(strEnDt) <= int.Parse(strSavedEnDt))
                    {
                        bDateChk = false;
                    }
                }

                if (!bDateChk)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('입력한 기간과 겹치는 진행중인 지급 목록이 있습니다.');", true);
                }
                else if (strStDt.Length == 0 || strEnDt.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('일자를 입력해주세요.');", true);
                }
                else if (strStDt.Length < 8 || strEnDt.Length < 8)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('일자가 잘못되었습니다.');", true);
                }
                else if (!strYears.Equals(strStDt.Substring(0, 4)) || !strYears.Equals(strEnDt.Substring(0, 4)))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('선택한 연도와 일자가 일치해야합니다.');", true);
                }
                else
                {
                    using (PointMgmt point = new PointMgmt())
                    {
                        point.UpdProvidePointList(lblYears.Text, lblPointSeq.Text, txbPointStdt.Text, txbPointEndt.Text);
                    }

                    GetProvidePoint();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SavePoint", "alert('지급대상 목록을 수정 완료 했습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}