﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegPartnersInfo.aspx.cs" Inherits="KTSSolutionWeb.RegPartnersInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>협력사 정보 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            ktsGridId = 'windowpop-wrap';

            var fn_button = function (cell) {
                var rowData = cell.getRow().getData();
                var officeNm = rowData.OFFICENM;
                var officeCd = rowData.OFFICECD;

                var input = "<input id='btnCellSave' type='button' onclick='btnOfficeChoice(\"" + officeNm + "\", \"" + officeCd + "\");' value='선택' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사명", field: "OFFICENM", width: 160, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", width: 160, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 160, formatter: fn_button }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("30vh");

            $("#btnOffice").click(function (e) {
                fn_Search();
            });

            $(document).click(function (e) {
                if ($(e.target).parents('.tabulator').length < 1 && $(e.target).parents('btn-popupgray').length < 1) {
                    $("#trOfficeinfo").hide();
                }
            });
        });

        var ajaxCallBack = function (json) {
            if (callGb != "") {
                if (callGb == "getOk") {
                    ktsGrid.setData(json);

                    if (ktsGrid.getDataCount() == 1) {
                        $("#trOfficeinfo").hide();
                        var row = ktsGrid.getRow(1);
                        var rowData = row.getData();

                        var officenm = rowData.OFFICENM;
                        var officecd = rowData.OFFICECD;

                        $("#txbOffice").val(officenm);
                        $("#hdfOfficeCd").val(officecd);

                        ktsGridId = 'windowpop-wrap';
                        ktsGridAlert(officenm + "(" + officecd + ") 선택완료.");

                    }
                    else if (ktsGrid.getDataCount() > 1) {
                        $("#trOfficeinfo").show();
                    } 
                    else {
                        $("#trOfficeinfo").hide();

                        ktsGridId = 'windowpop-wrap';
                        ktsGridAlert("조회된 국사가 없습니다. 입력한 국사를 확인해주세요");
                    }
                }
                else {
                    $.each(JSON.parse(json), function (idx, item) {
                        if (item.ERRCD == "00") {
                            if (callGb == "InsOk") {
                                ktsGridAlert("협력사 정보가 등록되었습니다.", 2000);
                            }

                            fn_PageInit();
                        }
                        else {
                            ktsGridAlert(item.ERRMSG);
                        }
                    });
                }
            }
        };

        function fn_PageInit() {
            $("#txbCompNm").val("");
            $("#txbCompCd").val("");
            $("#txbOffice").val("");
            $("#hdfOfficeCd").val("");
            $("#txbDelegate").val("");
            $("#txbEmpNo").val("");
            $("#txbTelNo").val("");
            $("#txbDelegate2").val("");
            $("#txbEmpNo2").val("");
            $("#txbTelNo2").val("");
            $("#txbNote").val("");

            ktsGrid.setData(null);
        }

        function fn_Search() {
            $("#hdfOfficeCd").val("");

            var office = $("#txbOffice").val();

            if (office == undefined || office.length == 0) {
                ktsGridAlert("담당국사를 입력해주세요.");
                return false;
            }
            else {
                callGb = "getOk";
                //입력 파라미터
                var aData = [];
                aData[0] = $("#txbOffice").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                //var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                //grid.getDataBind(jsonData, ajaxUrl);

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "GetData");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        function InsPartnersInfo(compNm, compCd, officeCd, delegate, empno, telno, delegate2, empno2, telno2, note) {

            var aData = [];

            aData[0] = compNm;
            aData[1] = compCd;
            aData[2] = officeCd;
            aData[3] = delegate;
            aData[4] = empno;
            aData[5] = telno;
            aData[6] = delegate2;
            aData[7] = empno2;
            aData[8] = telno2
            aData[9] = note;

            callGb = 'InsOk';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, "InsPartnersInfo");

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function btnOfficeChoice(officenm, officecd) {
            $("#txbOffice").val(officenm);
            $("#hdfOfficeCd").val(officecd);

            ktsGridAlert(officenm + "(" + officecd + ") 선택완료.");

            ktsGridId = 'windowpop-wrap';

            $("#trOfficeinfo").hide();
        }

        function btnRegCheck() {
            var compNm = $("#txbCompNm").val();
            var compCd = $("#txbCompCd").val();
            var officeNm = $("#txbOffice").val();
            var officeCd = $("#hdfOfficeCd").val();
            var delegate = $("#txbDelegate").val();
            var empno = $("#txbEmpNo").val();
            var telno = $("#txbTelNo").val();
            var delegate2 = $("#txbDelegate2").val();
            var empno2 = $("#txbEmpNo2").val();
            var telno2 = $("#txbTelNo2").val();
            var note = $("#txbNote").val();
            
            if (officeNm == undefined || officeNm.length == 0 || officeCd == undefined || officeCd.length == 0) {
                ktsGridAlert("담당국사를 선택해주세요.");

                return false;
            }
            else if (compNm == undefined || compNm.length == 0 || compCd == undefined || compCd.length == 0) {
                ktsGridAlert("누락된 필수 입력 값이 있습니다.");

                return false;
            }
            else {
                if (confirm("협력사 정보를 등록 하시겠습니까?")) {
                    InsPartnersInfo(compNm, compCd, officeCd, delegate, empno, telno, delegate2, empno2, telno2, note);
                }
                else {
                    return false;
                }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>협력사 정보 등록</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th style="width:100px">협력사명&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbCompNm" style="width:350px;" maxlength="50" tabindex="1" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:130px">협력사코드&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbCompCd" style="width:200px;" maxlength="6" tabindex="2" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">담당국사&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left" >
                                                <p class="inpbox" style="width:205px;" >
                                                    <input type="text" id="txbOffice" style="width:200px;" maxlength="50" tabindex="3" />
                                                    <input type="hidden" id="hdfOfficeCd" />
                                                </p>
                                                <input id="btnOffice" type="button" value="확인" class="btn-popupgray" />
                                            </td>
                                        </tr>
                                        <tr id="trOfficeinfo" style="display:none;">
                                            <td style="text-align:left;padding-top: 0;margin-top: 0;vertical-align: top;" colspan="4" >
                                                <div id="ktsGrid" style="position: absolute; z-index: 99; width:580px;"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">대표자(정)</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbDelegate" style="width:350px;" maxlength="50" tabindex="4" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">사번(정)</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbEmpNo" style="width:350px;" maxlength="50" tabindex="4" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">연락처(정)</th>
                                            <td style="text-align:left">
                                                <p class="inpbox" style="width:205px;">
                                                    <input type="text" id="txbTelNo" style="width:350px;" maxlength="50" tabindex="5" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">대표자(부)</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbDelegate2" style="width:350px;" maxlength="50" tabindex="6" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">사번(부)</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <input type="text" id="txbEmpNo2" style="width:350px;" maxlength="50" tabindex="4" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">연락처(부)</th>
                                            <td style="text-align:left">
                                                <p class="inpbox" style="width:205px;">
                                                    <input type="text" id="txbTelNo2" style="width:350px;" maxlength="50" tabindex="7" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">비고</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <input type="text" id="txbNote" style="width:500px;margin-right:10px;" maxlength="1500" tabindex="8" />
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력값입니다.
                        <div class="btncenter">
                            <input id="btnReg" type="button" onclick="btnRegCheck();" value="저장" class="btn-green" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
