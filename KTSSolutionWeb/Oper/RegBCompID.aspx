﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegBCompID.aspx.cs" Inherits="KTSSolutionWeb.RegBCompID" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>B협력사 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">
        function IdCheck() {
            if ($("#txbEmpNo").val() == "") {
                alert("ID 를 입력하세요");
                return false;
            }
        }

        function RefreshList() {
            opener.RefreshList();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>B협력사 등록</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th style="width:100px">업체명</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbCompNm" runat="server" Width="500px" TabIndex="0"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">이름</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpNm" runat="server" Width="200px" MaxLength="50" TabIndex="2"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">사번</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox" style="text-align:left;width:200px">
                                                            <asp:TextBox ID="txbEmpNo" runat="server" Width="200px" MaxLength="20" TabIndex="1"></asp:TextBox>
                                                        </p>
                                                        <asp:Button ID="btnIDChk" runat="server" OnClientClick="return IdCheck();" OnClick="BtnIDCheck_Click" class="btn-popupgray" Text="사번확인" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">본부</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox" style="text-align:left">
                                                            <asp:TextBox ID="txbTxbBBNm" runat="server" Width="200px" MaxLength="50" TabIndex="3"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">지사</th>
                                                    <td>
                                                        <p class="inpbox" style="text-align:left">
                                                            <asp:TextBox ID="txbTxbJSNm" runat="server" Width="200px" MaxLength="50" TabIndex="4"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">국사</th>
                                                    <td colspan="3">
                                                        <p class="inpbox" style="text-align:left">
                                                            <asp:TextBox ID="txbOfficeNm" runat="server" Width="200px" MaxLength="50" TabIndex="5"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click" />
                                    </Triggers>
                                </asp:updatepanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <div class="btncenter">
                            <asp:Button id="btnReg" runat="server" OnClick="BtnRegedit_Click" class="btn-green" Text="등록" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
