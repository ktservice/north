﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MeritPayUpload.aspx.cs" Inherits="KTSSolutionWeb.MeritPayUpload" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">		
		<!-- S:page-upload-management -->
        <div class="page-upload-management">
            <div class="list-top">
                <strong>실적급</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" title="최종 등록 완료" class="submit-btn btn-green">최종 등록 완료</button>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="upload-list">
                        <ul>
                            <li>
                                <strong>1.총괄</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
								        <input type="text" id="txtfu"/>
								        <span class="file">
									        <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
									        <label for="fu">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
								        </span>
							        </p>
						        </div>
					        </li>
                            <li>
                                <strong>2.개통</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuOpen"/>
							        <span class="file">
								        <input type="file" id="fuOpen" name="fuOpen" onchange="document.getElementById('txtfuOpen').value=this.value;"/> 
								        <label for="fuOpen">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>3.AS</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuAs"/>
							        <span class="file">
								        <input type="file" id="fuAs" name="fuAs" onchange="document.getElementById('txtfuAs').value=this.value;"/> 
								        <label for="fuAs">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>4.무출동</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuNoneMove"/>
							        <span class="file">
								        <input type="file" id="fuNoneMove" name="fuNoneMove" onchange="document.getElementById('txtfuNoneMove').value=this.value;"/> 
								        <label for="fuNoneMove">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>5.단말회수</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuTerminal"/>
							        <span class="file">
								        <input type="file" id="fuTerminal" name="fuTerminal" onchange="document.getElementById('txtfuTerminal').value=this.value;"/> 
								        <label for="fuTerminal">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>6.파견</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuDispatch"/>
							        <span class="file">
								        <input type="file" id="fuDispatch" name="fuDispatch" onchange="document.getElementById('txtfuDispatch').value=this.value;"/> 
								        <label for="fuDispatch">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>7.수작업</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuManual"/>
							        <span class="file">
								        <input type="file" id="fuManual" name="fuManual" onchange="document.getElementById('txtfuManual').value=this.value;"/> 
								        <label for="fuManual">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>8.이벤트1</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuEvent1"/>
							        <span class="file">
								        <input type="file" id="fuEvent1" name="fuEvent1" onchange="document.getElementById('txtfuEvent1').value=this.value;"/> 
								        <label for="fuEvent1">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>9.이벤트2</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuEvent2"/>
							        <span class="file">
								        <input type="file" id="fuEvent2" name="fuEvent2" onchange="document.getElementById('txtfuEvent2').value=this.value;"/> 
								        <label for="fuEvent2">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
                            <li>
                                <strong>10.이벤트3</strong>
                                <div class="file-field"> 
                                    <p class="filebox">
							        <input type="text" id="txtfuEvent3"/>
							        <span class="file">
								        <input type="file" id="fuEvent3" name="fuEvent3" onchange="document.getElementById('txtfuEvent3').value=this.value;"/> 
								        <label for="fuEvent3">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
							        </span>
                                    </p>
                                </div>
					        </li>
				        </ul>
			        </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
		<!-- E:page-upload-management -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>