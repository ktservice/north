﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;

namespace KTSSolutionWeb
{
    public partial class RegCorpInfo : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //세션 체크
                if (this.Page.Session.Count <= 5)
                {
                    Session.Abandon();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('잘못된 접근입니다.');window.close();", true);
                }
                else
                {
                    string strAuthId = Page.Session["AUTHID"].ToString();

                    if (!strAuthId.Equals("AUTH0001") && !strAuthId.Equals("AUTH0008"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('잘못된 접근입니다.');window.close();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCorpInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;

            //권한 및 유효성 체크
            if (aData[0].Length > 0 && aData[1].Length > 0)
            {
                string rptCd = "CORPINFOINSERT";
                string jobGb = "SET";

                string empNo = HttpContext.Current.Session["EMPNO"].ToString();

                aData.Add(empNo); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }
    }
}