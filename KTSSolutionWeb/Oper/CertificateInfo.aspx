﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CertificateInfo.aspx.cs" Inherits="KTSSolutionWeb.CertificateInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />    
    
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#btnAdd").click(function (e) {
                    fn_btnCheck('', 'S');
                });

                $("#gridExcel").click(function (e) {
                    var name = "계약서 현황조회";
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Load = function () {

            var now = new Date();
            var year = now.getFullYear() + 1;

            var selector = document.getElementById("ddlYear");

            for (var i = 2022; i <= year; i++) {
                var options = document.createElement("option");

                options.className = "options";
                options.value = i;
                options.text = i;
                selector.append(options);
            }

            fn_Search();
        };

        var fn_GridInit = function () {
            var fn_select = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var now = new Date();
                var year = now.getFullYear() + 1;

                for (var i = 2022; i <= year; i++) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = i;
                    options.text = i;
                    selector.append(options);
                }

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_Input = function (cell) {
                var cellValue = cell.getValue();
                var field = cell.getField();
                var row = cell.getRow();
                var rowData = row.getData();
                var id = rowData.id;

                var input = document.createElement("input");

                input.setAttribute("type", "text");

                input.style.padding = "4px";
                input.style.width = "100%";
                input.style.boxSizing = "border-box";
                input.style.height = "100%";
                input.style.borderStyle = "none";
                input.maxLength = 100;

                input.value = cellValue;

                var onChange = function () {
                    ktsGrid.updateData([{
                        id: id, CERTIFICATENM: input.value
                    }])
                    .then(function () {
                        //run code after row
                    })
                    .catch(function (error) {
                        //handle error
                    });
                };

                input.addEventListener("blur", onChange);

                return input;
            };

            var fn_Date = function (cell, onRendered, success, cancel, editorParams) {
                var cellValue = cell.getValue(),
                    input = document.createElement("input");

                input.setAttribute("type", "text");

                input.style.padding = "4px";
                input.style.width = "100%";
                input.style.boxSizing = "border-box";
                input.style.height = "100%";
                input.style.borderStyle = "none";
                input.setAttribute("readonly", "true");

                input.value = typeof cellValue !== "undefined" ? cellValue : "";

                onRendered(function () {
                    input.style.height = "100%";
                    $(input).datepicker({
                        dateFormat: "yy-mm-dd",
                        dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                        dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                        monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                        monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                        showOtherMonths: false,
                        selectOtherMonths: false,
                        constrainInput: false,
                        prevText: "<",
                        nextText: ">",
                        onClose: onChange
                    }); //turn input into datepicker
                    input.focus();
                });

                function onChange(e) {
                    if (((cellValue === null || typeof cellValue === "undefined") && input.value !== "") || input.value != cellValue) {
                        success(input.value);
                    } else {
                        cancel();
                    }
                }

                return input;
            };


            var fn_Check = function (cell) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var compYn = rowData.COMPANYYN;

                var element = document.createElement("input");
                element.id = "cb_" + id;
                element.name = "chkbox_Conf";
                element.setAttribute("type", "checkbox");

                if (compYn == 'Y') {
                    element.checked = true;
                }
                else {
                    element.checked = false;
                }

                function onChange() {
                    var checkedYn = "N";

                    if (element.checked) {
                        checkedYn = "Y";
                    }
                    else {
                        checkedYn = "N";
                    }

                    ktsGrid.updateData([{
                        id: id, COMPANYYN: checkedYn
                    }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                }

                //submit new value on blur or change
                element.addEventListener("click", onChange);

                return element;
            };

            var fn_regbutton = function (cell) {
                var rowData = cell.getRow().getData();
                var rowid = rowData.id;
                
                var input = "<input id='btnCellSave' type='button' onclick='fn_btnCheck(\"" + rowid + "\", \"S\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDel' type='button' onclick='fn_btnCheck(\"" + rowid + "\", \"D\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "CERTIFICATEID", field: "CERTIFICATEID", headerSort: false, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "귀속년도", field: "YEARS", sorter: "string", width: 100, editor: fn_select },
                { headerHozAlign: "center", hozAlign: "left", title: "인수증명", field: "CERTIFICATENM", sorter: "string", minWidth: 400, formatter: fn_Input  },
                { headerHozAlign: "center", hozAlign: "center", title: "계약시작일", field: "STDT", sorter: "string", width: 120, maxWidth: 200, editor: fn_Date },
                { headerHozAlign: "center", hozAlign: "center", title: "협력사전용", sorter: "string", width: 120, headerSort: false, formatter: fn_Check },
                { headerHozAlign: "center", hozAlign: "center", field: "COMPANYYN", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "수정/삭제", field: "BUTTONS", width: 200, formatter: fn_regbutton }
            ]; 

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값
            
            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("sField");
            var ValueEl = document.getElementById("sValue");

            $("#sField").change(function (e) {
                ValueEl.value = '';
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $("#sValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $(".table-search").css('display', 'none');
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'RegOk') {
                            ktsGridAlert('인수증 정보가 저장되었습니다.', 2000);
                        }
                        else if (callGb == 'DelOk') {
                            ktsGridAlert('인수증 정보가 삭제되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.SERVICE_ERROR_MSG);
                    }
                });

                callGb = '';
            }
        };

        var fn_Search = function () {

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        };

        var fn_btnCheck = function (rowNum, btnType) {
            var msg = "";

            if (btnType == "S") {
                msg = "저장";
            }
            else {
                msg = "삭제";
            }

            if (confirm("인수증 정보를 " + msg + " 하시겠습니까?")) {
                fn_SetCertificateInfo(rowNum, btnType);
            }
            else {
                return false;
            }
        };

        var fn_SetCertificateInfo = function (rowNum, btnType) {
            var Year = "";
            var CertificateId = "";
            var CertificateNm = "";
            var Stdt = "";
            var CompYn = "";
            var bChk = true;
            var msg = "";

            if (rowNum != "") {
                var rows = ktsGrid.searchRows("id", "=", rowNum);

                var rowData = rows[0].getData();
                var pagenum = ktsGrid.getPage();

                Year = rowData.YEARS;
                CertificateId = rowData.CERTIFICATEID;
                CertificateNm = rowData.CERTIFICATENM;
                Stdt = rowData.STDT;
                CompYn = rowData.COMPANYYN;
            }
            else {
                CertificateId = "";
                Year = $("#ddlYear").val();
                CertificateNm = $("#txbCertificateNm").val();
                Stdt = $("#txbDate").val();

                if ($("#chkCompYn").is(":checked")) {
                    CompYn = "Y";
                }
                else {
                    CompYn = "N";
                }
            }

            var aData = [];
            var funcNm = "";

            aData[0] = CertificateId;

            if (btnType == "S") {
                if (CertificateNm == "" || CertificateNm == undefined || Stdt == "" || Stdt == undefined) {
                    bChk = false;
                    msg = "인수증명과 계약시작일을 입력해주세요.";
                }

                aData[1] = Year;
                aData[2] = CertificateNm;
                aData[3] = Stdt;
                aData[4] = CompYn;

                funcNm = "RegCertificateInfo";
                callGb = 'RegOk';
            }
            else {
                if (CertificateId == "" || CertificateId == undefined) {
                    bChk = false;
                    msg = "잘못된 접근입니다.";
                }

                funcNm = "DelCertificateInfo";
                callGb = 'DelOk';
            }

            if (bChk) {
                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, funcNm);

                ajaxCallEx(jsonData, ajaxUrl, true);

                if (CertificateId != "") {
                    var maxPageNum = ktsGrid.getPageMax();

                    if (maxPageNum < pagenum) {
                        ktsGrid.setPage(maxPageNum);
                    }
                    else {
                        ktsGrid.setPage(pagenum);
                    }
                }
            }
            else {
                ktsGridAlert(msg);
            }
        };
    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">   
            <fieldset>
                <span class="optionbox first">
                    <label>귀속년도</label>
                    <select id="ddlYear"style="width:120px;">
                        <option value="">선택하세요</option>
                    </select>
                </span>
                <span class="inpbox">
                    <label>인수증명</label>
                    <input type="text" id="txbCertificateNm" style="width:600px;" maxlength="100" />
                </span>
                <span class="inpbox">
                    <label>계약시작일</label>
                    <input type="text" id="txbDate" class="date" style="width:120px;" maxlength="10" readonly="true"/>
                </span>
                <span class="optionbox">
                    <label>협력사전용</label>
                    <input type="checkbox" id="chkCompYn"/>
                </span>
                <input id="btnAdd" type="button" class="btn-green last" style="float:right;" value="추가" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="sField" style="font-weight:bold">
                            <option value="CERTIFICATENM">인수증명</option>
                        </select>
                        <label> : </label>
                        <input id="sValue" type="text" value="" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
