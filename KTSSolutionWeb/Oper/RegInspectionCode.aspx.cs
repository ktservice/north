﻿using System;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Collections.Generic;

namespace KTSSolutionWeb
{
    public partial class RegInspectionCode : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        private void PageInit()
        {
            try
            {
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetChkType(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONCHKTYPE";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetUpperList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONUPPERLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsChkType(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONCHKTYPEINSERT";
            string jobGb = "SET";

            //aData[0] = code;
            //aData[1] = codeNm;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string UpdChkType(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONCHKTYPEUPDATE";
            string jobGb = "SET";

            //aData[0] = code;
            //aData[1] = codeNm;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelChkType(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONCHKTYPEDELETE";
            string jobGb = "SET";

            //aData[0] = code;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsUpperList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONUPPERLISTINSERT";
            string jobGb = "SET";

            //aData[0] = code;
            //aData[1] = codeNm;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string UpdUpperList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONUPPERLISTUPDATE";
            string jobGb = "SET";

            //aData[0] = code;
            //aData[1] = codeNm;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelUpperList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONUPPERLISTDELETE";
            string jobGb = "SET";

            //aData[0] = code;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}