﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class StandardClothInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetStandardProvideInfo()
        {
            DataSet ds = new DataSet();
            try
            {
                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetStandardProvideInfo();
                }

                rptResult1.DataSource = ds.Tables[0];
                rptResult1.DataBind();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetProvideInfoByYears()
        {
            DataSet ds = new DataSet();

            try
            {
                string strYears = this.ddlYears.SelectedValue;

                if (strYears.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (PointMgmt point = new PointMgmt())
                    {
                        ds = point.GetProvideInfoByYears(strYears);
                    }

                    rptResult2.DataSource = ds.Tables[0];
                    rptResult2.DataBind();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[12];

            ArrHeader[0] = "PROVIDETYPE";
            ArrHeader[1] = "JOBNM";
            ArrHeader[2] = "CLOTHGBN";
            ArrHeader[3] = "CLOTHTYPE1";
            ArrHeader[4] = "CLOTHTYPE2";
            ArrHeader[5] = "CLOTHTYPE3";
            ArrHeader[6] = "CLOTHTYPE4";
            ArrHeader[7] = "CLOTHTYPE5";
            ArrHeader[8] = "CLOTHTYPE6";
            ArrHeader[9] = "CLOTHTYPE7";
            ArrHeader[10] = "CLOTHTYPE8";
            ArrHeader[11] = "CLOTHTYPE9";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[3];

            ArrHeader[0] = "구분";
            ArrHeader[1] = "작업복";
            ArrHeader[2] = "방한복";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[12];

            ArrHeader[0] = "";
            ArrHeader[1] = "";
            ArrHeader[2] = "";

            ArrHeader[3] = "점퍼";
            ArrHeader[4] = "조끼";
            ArrHeader[5] = "티셔츠";
            ArrHeader[6] = "바지";
            ArrHeader[7] = "모자";

            ArrHeader[8] = "외피";
            ArrHeader[9] = "내피";
            ArrHeader[10] = "방한바지";
            ArrHeader[11] = "버프";

            return ArrHeader;
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetStandardProvideInfo();
                GetProvideInfoByYears();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel1_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetStandardProvideInfo();
                }

                dt = ds.Tables[0];

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();
                    excel.MergeCell = "0/3,1/5,2/4";

                    excel.ExcelDownLoad(this.Page, dt, "연간지급기준등록-기준정보");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void btnExcel2_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                string strYears = this.ddlYears.SelectedValue;

                if (strYears.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (PointMgmt point = new PointMgmt())
                    {
                        ds = point.GetProvideInfoByYears(strYears);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();
                    excel.MergeCell = "0/3,1/5,2/4";

                    excel.ExcelDownLoad(this.Page, dt, "연간지급기준등록-등록결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptResult2.Items.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('조회된 데이터가 없습니다.');", true);
                }
                else
                {
                    List<string> lstData = new List<string>();

                    string[] strClothType = { "점퍼", "조끼", "티셔츠", "바지", "모자", "외피", "내피", "방한바지", "버프" };

                    for (int i = 0; i < rptResult2.Items.Count; i++)
                    {
                        Label lblYears = (Label)rptResult2.Items[i].FindControl("lblYears");
                        Label lblJobNm = (Label)rptResult2.Items[i].FindControl("lblJobNm");
                        Label lblProvideType = (Label)rptResult2.Items[i].FindControl("lblProvideType");
                        Label lblClothGbn = (Label)rptResult2.Items[i].FindControl("lblClothGbn");

                        string strYears = lblYears.Text.ToString();
                        string strJobNm = lblJobNm.Text.ToString();
                        string strProvideType = lblProvideType.Text.ToString();
                        string strClothGbn = lblClothGbn.Text.ToString();


                        for (int j = 1; j < 10; j++)
                        {
                            TextBox txbClothType = (TextBox)rptResult2.Items[i].FindControl("txbClothType" + j.ToString());

                            string strData = strYears + "|" + strJobNm + "|" + strProvideType + "|" + strClothGbn + "|" + strClothType[j - 1] + "|" + txbClothType.Text;

                            lstData.Add(strData);
                        }

                        if (i == 0)
                        {
                            using (PointMgmt point = new PointMgmt())
                            {
                                point.DelProvideInfoByYears(strYears);
                            }
                        }
                    }

                    using (PointMgmt point = new PointMgmt())
                    {
                        point.InsProvideInfoByYears(lstData);
                    }

                    GetStandardProvideInfo();
                    GetProvideInfoByYears();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "alert('저장을 완료했습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}