﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CorpInfo.aspx.cs" Inherits="KTSSolutionWeb.CorpInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />    
    
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {

            var fn_Input = function (cell) {
                var cellValue = cell.getValue();
                var field = cell.getField();
                var row = cell.getRow();
                var rowData = row.getData();
                var id = rowData.id;

                var input = document.createElement("input");

                input.setAttribute("type", "text");

                input.style.padding = "4px";
                input.style.width = "100%";
                input.style.boxSizing = "border-box";
                input.style.height = "100%";
                input.style.borderStyle = "none";

                if (field == "CORPNO") {
                    input.maxLength = 12;
                }
                else if (field == "CORPNM") {
                    input.maxLength = 100;
                }
                else if (field == "ADDR" || cell.field == "ADDRDTL") {
                    input.maxLength = 1500;
                }
                else {
                    input.maxLength = 30;
                }

                input.value = cellValue;

                if (field == "CORPNO" || field == "TELNO" || field == "PHONENO") {
                    //submit new value on blur or change
                    input.addEventListener("keyup", function (event) {
                        const regex = /^[ㄱ-ㅣ가-힣a-zA-Z]+$/;
                        var keyCd = event.keyCode;
                        var keyVal = event.key;

                        if (field == "CORPNO" && keyVal.toUpperCase() == "S") {
                            //fn_DataUpdate(field, input.value);
                        }
                        else {
                            if (keyVal == '-') {
                                //fn_DataUpdate(field, input.value);
                            }
                            else if ((keyCd >= 48 && keyCd <= 57) || keyCd == 8 || keyCd == 46) {
                                if (!keyCd == 46 && !keyCd == 8) {
                                    if (regex.test(keyVal)) {
                                        input.value = cellValue;
                                    }
                                    else {
                                        //fn_DataUpdate(field, input.value);
                                    }
                                }
                                else {
                                    //fn_DataUpdate(field, input.value);
                                }
                            }
                            else {
                                if (regex.test(keyVal)) {
                                    input.value = cellValue;
                                }
                                else {
                                    //fn_DataUpdate(field, input.value);
                                }
                            }
                        }
                    });
                };

                var onChange = function () {
                    if (field == "CORPNM") {
                        ktsGrid.updateData([{
                            id: id, CORPNM: input.value
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                    else if (field == "CORPNO") {
                        ktsGrid.updateData([{
                            id: id, CORPNO: input.value
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                    else if (field == "ADDR") {
                        ktsGrid.updateData([{
                            id: id, ADDR: input.value
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                    else if (field == "ADDRDTL") {
                        ktsGrid.updateData([{
                            id: id, ADDRDTL: input.value
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                    else if (field == "TELNO") {
                        ktsGrid.updateData([{
                            id: id, TELNO: input.value
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                    else if (field == "PHONENO") {
                        ktsGrid.updateData([{
                            id: id, PHONENO: input.value
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                };

                input.addEventListener("blur", onChange);

                return input;
            };

            var fn_regbutton = function (cell) {
                var rowData = cell.getRow().getData();
                var rowid = rowData.id;
                
                var input = "<input id='btnCellSave' type='button' onclick='btnCheck(\"" + rowid + "\", \"S\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDel' type='button' onclick='btnCheck(\"" + rowid + "\", \"D\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDX", field: "IDX", headerSort: false, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "오더구분", field: "ORDERCODE", sorter: "string", width: 100  },
                { headerHozAlign: "center", hozAlign: "left", title: "사업자명", field: "CORPNM", sorter: "string", minWidth: 100, maxWidth: 400, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "center", title: "사업자등록번호", field: "CORPNO", sorter: "string", minWidth: 100, maxWidth: 400, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "left", title: "주소", field: "ADDR", sorter: "string", minWidth: 100, maxWidth: 1500, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "left", title: "주소상세", field: "ADDRDTL", sorter: "string", minWidth: 100, maxWidth: 1500, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "left", title: "연락처", field: "TELNO", sorter: "string", minWidth: 100, maxWidth: 200, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "left", title: "휴대폰번호", field: "PHONENO", sorter: "string", minWidth: 100, maxWidth: 200, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "center", title: "수정/삭제", field: "BUTTONS", width: 160, formatter: fn_regbutton }
            ]; 

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값
            
            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("sField");
            var ValueEl = document.getElementById("sValue");

            $("#sField").change(function (e) {
                ValueEl.value = '';
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $("#sValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.hideColumn("BUTTONS");
                ktsGrid.download("xlsx", "사업자 정보.xlsx", { sheetName: "사업자 정보" });
                ktsGrid.showColumn("BUTTONS");
            }); 

            $(".table-search").css('display', 'none');
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'RegOk') {
                            ktsGridAlert('사업자 정보가 수정되었습니다.', 2000);
                        }
                        else if (callGb == 'DelOk') {
                            ktsGridAlert('사업자 정보가 삭제되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });

                callGb = '';
            }
        };

        function fn_Search() {
            var orderCd = $("#<%= ddlOrderCode.ClientID %> option:selected").val();

            //입력 파라미터
            var aData = [];
            aData[0] = orderCd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function btnCheck(rowNum, btnType) {
            var msg = "";

            if (btnType == "S") {
                msg = "수정";
            }
            else {
                msg = "삭제";
            }

            if (confirm("사업자 정보를 " + msg + " 하시겠습니까?")) {
                SetPartnersInfo(rowNum, btnType);
            }
            else {
                return false;
            }
        }

        function SetPartnersInfo(rowNum, btnType) {
            var rows = ktsGrid.searchRows("id", "=", rowNum);

            var rowData = rows[0].getData();
            var pagenum = ktsGrid.getPage();

            var aData = [];
            var funcNm = "";

            aData[0] = rowData.IDX;

            if (btnType == "S") {
                aData[1] = rowData.CORPNM;
                aData[2] = rowData.CORPNO;
                aData[3] = rowData.ADDR;
                aData[4] = rowData.ADDRDTL;
                aData[5] = rowData.TELNO;
                aData[6] = rowData.PHONENO;

                funcNm = "RegCorpInfo";
                callGb = 'RegOk';
            }
            else {

                funcNm = "DelCorpInfo";
                callGb = 'DelOk';
            }

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, funcNm);

            ajaxCallEx(jsonData, ajaxUrl, true);

            var maxPageNum = ktsGrid.getPageMax();

            if (maxPageNum < pagenum) {
                ktsGrid.setPage(maxPageNum);
            }
            else {
                ktsGrid.setPage(pagenum);
            }
        }

        function PopupRegCorp() {
            var form = "/Oper/RegCorpInfo";
            var target = "CorpInfo";

            var param = {};

            var nWidth = 800;
            var nHeight = 500;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

        function DownloadTemplate() {
            window.location.href = '/Template/사업자 정보 Excel Upload Template.xlsx';
        }

        function btnUploadCheck() {
            if (confirm("사업자 정보를 일괄 등록 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset> 
				<span class="optionbox" style="margin-right:20px">
					<label>오더구분</label> 
                    <asp:DropDownList ID="ddlOrderCode" style="width:100px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="VAN" Value="1"></asp:ListItem>
                        <asp:ListItem Text="K-VIZERO" Value="2"></asp:ListItem>
                        <asp:ListItem Text="BIZ" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Gem" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <button id="btnTemp" type="button" onclick="DownloadTemplate();" class="btn-green">양식다운로드</button>
				    </div>
                    <div class="filebox">
                        <input type="text" id="txtfu" />
                        <span class="file">
                            <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
                            <label for="fu">찾아보기</label>
                        </span>
                        <asp:Button ID="btnUpload" runat="server" OnClientClick="return btnUploadCheck();" OnClick="btnUpload_Click" class="btn-black" Text="일괄등록" />
                    </div>
				    <div class="btnset">
                        <button id="btnRegID" type="button" onclick="PopupRegCorp();" class="btn-green">등록</button>
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="sField" style="font-weight:bold">
                            <option value="CORPNM">사업자명</option>
                            <option value="CORPNO">사업자등록번호</option>
                        </select>
                        <label> : </label>
                        <input id="sValue" type="text" value="" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
