﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyInfoPopup.aspx.cs" Inherits="KTSSolutionWeb.SafetyInfoPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>품목코드</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnDel").click(function (e) {
                    fn_DelTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';


            
            if ($("#hdnCode").val().length != 0) {
                $("#txtItemCd").attr('readonly', true);

                $("#btnReg").hide();
                $("#btnMod").show();
                $("#btnDel").show();
            }
            else {
                $("#btnReg").show();
                $("#btnMod").hide();
                $("#btnDel").hide();
            }

            var obj1 = $('#selGive');
            obj1.empty();

            var obj2 = $('#selUse');
            obj2.empty();

            obj1.append("<option value='Y'>Y</option>");
            obj1.append("<option value='N'>N</option>");
            obj2.append("<option value='Y'>Y</option>");
            obj2.append("<option value='N'>N</option>");
        }

        var fn_Load = function () {
            fn_Unit();
        }

        var fn_DelTran = function () {
            if (confirm("품목코드정보가 삭제 됩니다.\n계속 진행하시겠습니까?")) {
                callGb = 'DelTran';

                fn_Transaction('D');
            }
        }

        var fn_ModTran = function () {
            if (fn_RegChk()) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_RegChk = function () {
            var itemCd = $("#txtItemCd").val();
            var itemNm = $("#txtItemNm").val();
            var serviceLife = $("#txtServiceLife").val();
            var standard = $("#txtStandard").val();
            var selUnit = $("#selUnit").val();
            var selGive = $("#selGive").val();
            var selUse = $("#selUse").val();
            var giveCnt = $("#txtGiveCnt").val();
            

            if (itemCd.length == 0) {
                ktsGridAlert("품목코드를 입력하세요.");
                return false;
            }
            if (itemNm.length == 0) {
                ktsGridAlert("품목명을 입력하세요.");
                return false;
            }
            if (serviceLife.length == 0) {
                ktsGridAlert("내용연수를 입력하세요.");
                return false;
            }
            if (standard.length == 0) {
                ktsGridAlert("규격을 입력하세요.");
                return false;
            }
            if (giveCnt.length == 0) {
                ktsGridAlert("신규지급 수량을 입력하세요.");
                return false;
            }

            if (selUnit.length == 0) {
                ktsGridAlert("변경하려는 단위를 선택하세요.");
                return false;
            }
            if (selGive.length == 0) {
                ktsGridAlert("변경하려는 신규지급을 선택하세요.");
                return false;
            }
            if (selUse.length == 0) {
                ktsGridAlert("변경하려는 사용을 선택하세요.");
                return false;
            }

            return true;
        }

        var fn_Unit = function () {
            callGb = 'selUnit';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T06';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function (code) {
            callGb = 'Data';

            //입력 파라미터
            var aData = [];
            aData[0] = '11';
            aData[1] = '';
            aData[2] = code;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun, productCd) {
            var itemCd = $("#txtItemCd").val();
            var itemNm = $("#txtItemNm").val();
            var serviceLife = $("#txtServiceLife").val();
            var standard = $("#txtStandard").val();
            var selUnit = $("#selUnit").val();
            var selGive = $("#selGive").val();
            var selUse = $("#selUse").val();
            var giveCnt = $("#txtGiveCnt").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = itemCd;
            aData[2] = itemNm;
            aData[3] = standard;
            aData[4] = selUnit;
            aData[5] = serviceLife;
            aData[6] = selGive;
            aData[7] = giveCnt;
            aData[8] = selUse;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;

            if (callGb == 'selUnit') {
                obj1 = $('#selUnit');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });
                
                callGb = '';

                if ($("#hdnCode").val().length != 0) {
                    fn_Data($("#hdnCode").val());
                }
            }
            else if (callGb == 'Data') {

                var itemCd = '';
                var itemNm = '';
                var serviceLife = '';
                var standard = '';
                var giveCnt = '';
                var selUnit = '';
                var selGive = '';
                var selUse = '';

                $.each(JSON.parse(json), function (idx, item) {
                    itemCd = item.id;
                    itemNm = item.ITEMNM;
                    serviceLife = item.SERVICELIFE;
                    standard = item.STANDARD;
                    giveCnt = item.GIVECNT;
                    selUnit = item.UNIT;
                    selGive = item.GIVEYN;
                    selUse = item.USEYN;
                });

                $("#txtItemCd").val(itemCd);
                $("#txtItemNm").val(itemNm);
                $("#txtServiceLife").val(serviceLife);
                $("#txtStandard").val(standard);
                $("#txtGiveCnt").val(giveCnt);

                $("#selUnit").val(selUnit).prop("selected", true);
                $("#selGive").val(selGive).prop("selected", true);
                $("#selUse").val(selUse).prop("selected", true);

                callGb = '';
            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
            else if (callGb == 'ModTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('변경되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
            else if (callGb == 'DelTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('삭제되었습니다.');
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {
            
            opener.SetDispItem(check);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>품목코드 등록</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">품목코드</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemCd" maxlength="10" tabindex="1" style="width:472px;" placeholder="품목코드 입력" />
                                                    <input type="hidden" id="hdnCode" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" maxlength="200" tabindex="1" style="width:472px;" placeholder="품목명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">단위</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUnit">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">내용연수</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtServiceLife" maxlength="11" tabindex="1" style="width:150px;" placeholder="내용연수 입력" class="text-number" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">신규지급</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selGive">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">신규지급수량</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveCnt" maxlength="11" tabindex="1" style="width:100px;" placeholder="수량 입력" class="text-number" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">사용</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUse">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">규격</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtStandard" maxlength="400" tabindex="1" style="width:472px;" placeholder="규격 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                                <input id="btnDel" type="button" class="btn-del" value="삭제" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
