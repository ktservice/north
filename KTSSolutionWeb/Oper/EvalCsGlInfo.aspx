﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EvalCsGlInfo.aspx.cs" Inherits="KTSSolutionWeb.EvalCsGlInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
  
                $("#selField").change(function (e) {
                    fn_Field('selField', 'inputValue');
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    $('.tabnav a').filter(':eq(0)').click();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "평가CS GL 기준정보.xlsx", { sheetName: "평가CS GL 기준정보" });
                });

                $("#gridExcel2").click(function (e) {
                    ktsGrid.download("xlsx", "평가CS GL 기준정보 이력.xlsx", { sheetName: "평가CS GL 기준정보 이력" });
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnHist").click(function (e) {
                    fn_PopupHist();
                });

                $("#btnHolidayInfo").click(function (e) {
                    fn_PopupHolidayInfo();
                });

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    if (this.text === '업로드 이력 조회') {
                        fn_UploadHist();
                    }
                    else {
                        fn_Search('SEARCH');
                    }

                    return false;
                }).filter(':eq(0)').click();
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            fn_Month();
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen:true },
                { headerHozAlign: "center", hozAlign: "center", title: "기준월", field: "MDATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "대상코드", field: "MODULECD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "대상명", field: "MODULENM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "right", title: "GL", field: "GL", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_UploadHistGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "명칭", field: "UPLOADNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "UPLOADDT", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "결과", field: "RESULT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MESSAGE", sorter: "string" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsUploadHistGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
        }

        var fn_PopupUpload = function () {
            var form = "/Oper/EvalCsGlInfoUploadPopup";
            var target = "EvalCsGlInfoUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupHist = function () {
            var form = "/Oper/EvalCsGlInfoHistPopup";
            var target = "EvalCsGlInfoHistPopup";
            var param = {
            };
            var nWidth = 1060;
            var nHeight = 840;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupHolidayInfo = function () {
            var form = "/Oper/HolidayInfoPopup";
            var target = "HolidayInfoPopup";
            var param = {
            };
            var nWidth = 620;
            var nHeight = 800;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_Field = function (id1, id2) {
            var fieldEl = document.getElementById(id1);
            var valueEl = document.getElementById(id2);
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function (gubun) {
            var pDate = $("#selMon").val();
            var pGubun = gubun;

            //if (gubun == 'SEARCH') {
            //    $('.tabnav a').filter(':eq(0)').click();
            //}

            //입력 파라미터
            var aData = [];
            aData[0] = pGubun;
            aData[1] = pDate;
            aData[2] = '';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            if (gubun == 'SEARCH') {
                fn_GridInit();
                ktsRowCountClass = 'searchrowcount';
            }

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        var fn_UploadHist = function () {
            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetUploadHistGridData');

            fn_UploadHistGridInit();

            ktsRowCountClass = 'uploadhistrowcount';
            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
            <fieldset>
                <span class="inpbox">
                    <label>일자</label>
                    <span class="optionbox">
                        <select id="selMon" style="width: 110px"></select>
                    </span>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnHolidayInfo" type="button" class="btn-green last" style="float: right;" value="휴일정보" />
                    <input id="btnHist" type="button" class="btn-green last" style="float: right;" value="GL이력" />
                    <input id="btnUpload" type="button" class="btn-green last" style="float: right;" value="GL일괄등록" />
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <ul class="tabnav">
                <li><a href="#tab01"><span id="lblTabTitle1">기준정보 조회</span></a></li>
                <li><a href="#tab02">업로드 이력 조회</a></li>
            </ul>
            <div class="tabcontent">
                <div id="tab01">
                    <div class="list-top">
                        <strong><span id="lblGridTitle1">기준정보 조회 결과</span></strong>
                        <label class="searchrowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField">
                                    <option value="">선택</option>
                                    <option value="ORGNM">조직명</option>
                                    <option value="MODULENM">대상명</option>
                                </select>
                                : 
                        <input id="inputValue" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid"></div>
                    </div>
                </div>
                <div id="tab02">
                    <div class="list-top">
                        <strong>업로드 이력 조회 결과</strong>
                        <label class="uploadhistrowcount"></label>
                    </div>
                    <div class="scrollbox">
                        <div id="ktsUploadHistGrid"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>