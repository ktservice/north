﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class ChangePassword : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void BtnChgPwd_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            try
            {
                string strMsg = "";

                string strEmpNo = Request.Form[txbEmpNo.UniqueID];
                string strEmpPwd = Request.Form[txbEmpPwd.UniqueID];
                string strEmpPwdChg = Request.Form[txbEmpPwdChg.UniqueID];
                string strEmpPwdChk = Request.Form[txbEmpPwdChk.UniqueID];

                if (!ValidationCheck(ref strMsg))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationError", "alert('" + strMsg + "');window.close();", true);
                }
                else if (ExfireYn(strEmpNo))
                {
                    this.txbEmpNo.Text = "";
                    this.txbEmpPwd.Text = "";
                                                                                                                                                                                           
                    this.txbEmpPwdChg.Text = "";
                    this.txbEmpPwdChk.Text = "";
                    this.hdfChk1.Value = "";
                    this.hdfChk2.Value = "";
                    this.hdfChk3.Value = "";
                    this.hdfChk4.Value = "";

                    this.txbEmpNo.Focus();

                    updPanel1.Update();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ExfireChk", "alert('퇴사자의 패스워드는 변경할 수 없습니다.');", true);
                }
                else if (!PasswordRegex(strEmpPwdChg, ref strMsg))
                {
                    this.txbEmpPwdChg.Text = "";
                    this.txbEmpPwdChk.Text = "";
                    this.hdfChk1.Value = "";
                    this.hdfChk2.Value = "";
                    this.hdfChk3.Value = "";
                    this.hdfChk4.Value = "";

                    this.txbEmpPwd.Focus();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PasswordCheck", "alert('" + strMsg + "');", true);
                }
                else
                {
                    string strSHAPwd = Utility.SHA256Hash(strEmpPwd);
                    string strSHAPwdChg = Utility.SHA256Hash(strEmpPwdChg);

                    bool bResult = false;

                    using (KTSUser ktsUser = new KTSUser())
                    {
                        ds = ktsUser.GetUserLogOn(strEmpNo, strSHAPwd);

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bResult = ChangePWD(strEmpNo, strSHAPwd, strSHAPwdChg, ref strMsg);
                            }
                            else
                            {
                                strMsg = "사번과 패스워드를 확인해주세요.";
                            }
                        }
                        else
                        {
                            strMsg = "사번과 패스워드를 확인해주세요.";
                        }
                    }
                    string strScript = "alert('" + strMsg + "');";

                    if (bResult)
                    {
                        strScript += "window.close()";
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "confirm", strScript, true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strMsg">ref string</param>
        /// <returns></returns>
        private bool ValidationCheck(ref string strMsg)
        {
            string strEmpNo = Request.Form[txbEmpNo.UniqueID];
            string strEmpPWD = Request.Form[txbEmpPwd.UniqueID];
            string strEmpPWDChg = Request.Form[txbEmpPwdChg.UniqueID];
            string strEmpPWDChk = Request.Form[txbEmpPwdChk.UniqueID];
            string strhdfChk1 = Request.Form[hdfChk1.UniqueID];
            string strhdfChk2 = Request.Form[hdfChk2.UniqueID];
            string strhdfChk3 = Request.Form[hdfChk3.UniqueID];
            string strhdfChk4 = Request.Form[hdfChk4.UniqueID];

            bool bChk = true;

            if (strEmpNo.Length == 0 || strEmpPWD.Length == 0 || strEmpPWDChg.Length == 0 || strEmpPWDChk.Length == 0 ||
                strEmpNo != strhdfChk1 || strEmpPWD != strhdfChk2 || strEmpPWDChg != strhdfChk3 || strEmpPWDChk != strhdfChk4 ||
                strEmpPWD == strEmpPWDChg || strEmpPWDChg != strEmpPWDChk)
            {
                this.txbEmpNo.Text = "";
                this.txbEmpPwd.Text = "";
                this.txbEmpPwdChg.Text = "";
                this.txbEmpPwdChk.Text = "";
                this.hdfChk1.Value = "";
                this.hdfChk2.Value = "";
                this.hdfChk3.Value = "";
                this.hdfChk4.Value = "";

                updPanel1.Update();

                strMsg = "비정상적인 접근이 감지되었습니다. 패스워드 변경 창을 닫습니다.";
                bChk = false;
            }

            return bChk;
        }
        #endregion

        private bool ExfireYn(string strEmpNo)
        {
            bool bExfire = false;
            DataSet ds = new DataSet();

            try
            {
                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetUserExfireYn(strEmpNo);
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    bExfire = ds.Tables[0].Rows[0]["EXFIREYN"].ToString().Equals("Y");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bExfire;
        }

        private bool PasswordRegex(string strEmpPwd, ref string strMsg)
        {
            bool bPwdChk = true;
            Regex rxPassword = new Regex(@"^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", RegexOptions.IgnorePatternWhitespace);
            
            if (strEmpPwd.Length < 8 || !rxPassword.IsMatch(strEmpPwd))
            {
                bPwdChk = false;
                strMsg = "비밀번호는 숫자/영문/특수문자 조합 8자이상으로 설정해주세요.";
            }
            
            return bPwdChk;
        }

        private bool ChangePWD(string strEmpNo, string strEmpPwd, string strEmpPwdChg, ref string strResult)
        {
            bool bResult = false;

            try
            {
                using (UserMgmt userReg = new UserMgmt())
                {
                    userReg.SetUserPassWord(strEmpNo, strEmpPwdChg);
                }

                strResult = "비밀번호 변경을 완료했습니다. 다시 로그인 해주세요.";
                bResult = true;
            }
            catch
            {
                strResult = "비밀번호 변경에 실패했습니다.";
                bResult = false;
            }

            return bResult;
        }
    }
}