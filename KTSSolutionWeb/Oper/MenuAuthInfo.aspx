﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MenuAuthInfo.aspx.cs" Inherits="KTSSolutionWeb.MenuAuthInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        th {
            background-color: #f4f4f4;
        }

        /* 테이블에 스크롤 추가 */
        .table-container {
            max-height: 300px; /* 원하는 높이 설정 */
            overflow-y: auto;
        }

        /* thead 고정 */
        .table-container table thead th {
            position: sticky;
            top: 0;
            z-index: 2; /* 필요 시 우선순위 설정 */
        }
    </style>

    <script type="text/javascript">
        function btnSaveCheck() {
            if (confirm("권한 정보를 저장하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDelCheck() {
            if (confirm("선택한 권한에 사용자가 존재 시 삭제가 불가합니다. 선택한 권한을 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function fn_onScroll() {
            $("#divMenu1").scrollTop($("#divMenu2").scrollTop());
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->

    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S: page-menu-management -->
        <div class="page-menu-management">
            <div class="list-top">
                <strong>메뉴 관리</strong>
                <div class="pull-right">
                    <div class="btnset">
                        <asp:Button ID="btnSelect" runat="server" class="btn-green" OnClick="btnSelect_ServerClick" Text="초기화" />
                        <asp:Button ID="btnAdd" runat="server" class="btn-green" OnClick="btnAdd_ServerClick" Text="추가" />
                        <asp:Button ID="btnSave" runat="server" class="btn-green" OnClientClick="return btnSaveCheck();" OnClick="btnSave_ServerClick" Text="저장" />
                        <asp:Button ID="btnDel" runat="server" class="btn-green" OnClientClick="return btnDelCheck();" OnClick="btnDel_Click" Text="권한삭제" />
                    </div>
                </div>
            </div>
            <div class="datalist" style="overflow-x:auto;">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divMenu1" class="table-container" style="width:230px;max-height:575px;float:left;overflow-x:hidden;overflow-y:hidden;position:relative">
                            <table>
                                <thead>
                                    <tr style="height:53px;">
                                        <th style="width:230px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="tdMenu" runat="server" style="width:230px"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divMenu2" class="table-container" style="margin-left:230px;max-height:592px;" onscroll="fn_onScroll();">
                            <table>
                                <thead>
                                    <tr>
                                        <asp:Repeater ID="rptHead" runat="server">
                                            <ItemTemplate>
                                                <th style="width:160px;text-align:center;padding-left:3px;padding-right:3px;">
                                                    <asp:Label ID="lblAuthId" runat="server" Visible="false" Text='<%# Eval("AUTHID") %>'></asp:Label>
                                                    <div style="width:160px">
                                                        <asp:TextBox ID="txbAuthNm" runat="server" Height="30px" Font-Size="15px" Font-Bold="true" style="width:135px;text-align:center;float:left;" Text='<%# Eval("AUTHNM") %>'></asp:TextBox>
                                                        <input id="chkDel" runat="server" type="checkbox" style="float:left;margin-top:5px;margin-left:5px;width:20px;height:20px;" tooltip="권한그룹삭제여부" />   
                                                    </div>
                                                </th>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <asp:Repeater ID="rptBody" runat="server">
                                            <ItemTemplate>
                                                <td style="width:160px;text-align:center;">
                                                    <asp:Label ID="lblAuthId" runat="server" Visible="false" Text='<%# Eval("AUTHID") %>'></asp:Label>
                                                    <table style="border:0;">
                                                        <asp:Repeater ID="rptInTable" runat="server">
                                                            <ItemTemplate>
                                                                <tr style="height:25px">
                                                                    <td style="width:160px;text-align:center">
                                                                        <asp:Label ID="lblMenuId" runat="server" Visible="false"></asp:Label>
                                                                        <asp:RadioButton ID="rdY" runat="server" Text="Y" GroupName="rbYN" style="padding-right:10px;" />
                                                                        <asp:RadioButton ID="rdN" runat="server" Text="N" GroupName="rbYN"/>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </tbody>
                            </table>
                            <div style="width:17px"></div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" />
                        <asp:AsyncPostBackTrigger ControlID="btnAdd" />
                        <asp:AsyncPostBackTrigger ControlID="btnSave" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <!-- E: page-menu-management -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>