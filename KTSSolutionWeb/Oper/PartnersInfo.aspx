﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PartnersInfo.aspx.cs" Inherits="KTSSolutionWeb.PartnersInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />    
    
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {

            var inputEditor = function (cell, onRendered, success, cancel) {
                var editor = document.createElement("input");

                editor.setAttribute("type", "text");

                editor.style.padding = "4px";
                editor.style.width = "100%";
                editor.style.boxSizing = "border-box";
                editor.style.height = "100%";

                if (cell.field == "NOTE") {
                    editor.maxLength = 1500;
                }
                else {
                    editor.maxLength = 50;
                }

                var cellValue = cell.getValue();

                if (cellValue != undefined) {
                    editor.value = cellValue;
                }

                onRendered(function () {
                    editor.focus();
                    editor.style.height = "100%";
                });

                function onChange() {
                    if (editor.value != cellValue) {
                        success(editor.value);
                    } else {
                        cancel();
                    }
                }

                //submit new value on blur or change
                editor.addEventListener("blur", onChange);

                //submit new value on enter
                editor.addEventListener("keydown", function (e) {
                    if (e.keyCode == 13) {
                        onChange();
                    }

                    if (e.keyCode == 27) {
                        cancel();
                    }
                });

                return editor;
            };

            var fn_CellInput = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                cell.getElement().style.backgroundColor = "#f1f1f1";
                cell.getElement().style.fontWeight = "500";
                
                return value;
            };

            var fn_regbutton = function (cell) {
                var rowData = cell.getRow().getData();
                var rowNum = rowData.NUM;
                
                var input = "<input id='btnCellSave' type='button' onclick='btnCheck(\"" + rowNum + "\", \"S\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDel' type='button' onclick='btnCheck(\"" + rowNum + "\", \"D\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "협력사명", field: "COMPANYNM", sorter: "string", width: 180 },
                { headerHozAlign: "center", hozAlign: "center", title: "협력사코드", field: "COMPANYCD", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "국사명", field: "OFFICENM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대표자(정)", field: "DELEGATE", sorter: "string", width: 100, editor: inputEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "사번(정)", field: "EMPNO", sorter: "string", width: 100, editor: inputEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처(정)", field: "TELNO", sorter: "string", width: 140, editor: inputEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "대표자(부)", field: "DELEGATE2", sorter: "string", width: 100, editor: inputEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "사번(부)", field: "EMPNO2", sorter: "string", width: 100, editor: inputEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처(부)", field: "TELNO2", sorter: "string", width: 140, editor: inputEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "left", title: "비고", field: "NOTE", sorter: "string", width: 200, editor: inputEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "수정/삭제", field: "BUTTONS", width: 160, formatter: fn_regbutton }
            ]; 

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값
            
            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("sField");
            var ValueEl = document.getElementById("sValue");

            $("#sField").change(function (e) {
                ValueEl.value = '';
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $("#sValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            /*----검색 조건 ------- */
            $("#txbCompNm").keydown(function (e) {
                if (e.keyCode == 13) {
                    fn_Search();
                    return false;
                }
            });;

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.hideColumn("BUTTONS");
                ktsGrid.download("xlsx", "협력사 정보.xlsx", { sheetName: "협력사 정보" });
                ktsGrid.showColumn("BUTTONS");
            }); 

            $(".table-search").css('display', 'none');
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'RegOk') {
                            ktsGridAlert('협력사 정보가 수정되었습니다.', 2000);
                        }
                        else if (callGb == 'DelOk') {
                            ktsGridAlert('협력사 정보가 삭제되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
            }
        };

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#txbCompNm").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function btnCheck(rowNum, btnType) {
            var msg = "";

            if (btnType == "S") {
                msg = "수정";
            }
            else {
                msg = "삭제";
            }

            if (confirm("협력사 정보를 " + msg + " 하시겠습니까?")) {
                SetPartnersInfo(rowNum, btnType);
            }
            else {
                return false;
            }
        }

        function SetPartnersInfo(rowNum, btnType) {
            var rows = ktsGrid.searchRows("NUM", "=", rowNum);

            var rowData = rows[0].getData();
            var pagenum = ktsGrid.getPage();

            var aData = [];
            var funcNm = "";

            aData[0] = rowData.COMPANYCD;
            aData[1] = rowData.OFFICECD;

            if (btnType == "S") {
                aData[2] = rowData.DELEGATE;
                aData[3] = rowData.EMPNO;
                aData[4] = rowData.TELNO;
                aData[5] = rowData.DELEGATE2;
                aData[6] = rowData.EMPNO2;
                aData[7] = rowData.TELNO2;
                aData[8] = rowData.NOTE;

                funcNm = "RegPartnersInfo";
                callGb = 'RegOk';
            }
            else {

                funcNm = "DelPartnersInfo";
                callGb = 'DelOk';
            }

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, funcNm);

            ajaxCallEx(jsonData, ajaxUrl, true);

            var maxPageNum = ktsGrid.getPageMax();

            if (maxPageNum < pagenum) {
                ktsGrid.setPage(maxPageNum);
            }
            else {
                ktsGrid.setPage(pagenum);
            }
        }

        function PopupRegPartners() {
            var form = "/Oper/RegPartnersInfo";
            var target = "PartnersInfo";

            var param = {};

            var nWidth = 800;
            var nHeight = 600;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

        function DownloadTemplate() {
            window.location.href = '/Template/협력사정보 Excel Upload Template.xlsx';
        }

        function btnUploadCheck() {
            if (confirm("협력사정보를 일괄 등록 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
				<span class="inpbox first">
                    <label>협력사명</label>
                    <input type="text" id="txbCompNm" style="margin-right:10px;width:400px;" />
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <button id="btnTemp" type="button" onclick="DownloadTemplate();" class="btn-green">양식다운로드</button>
				    </div>
                    <div class="filebox">
                        <input type="text" id="txtfu" />
                        <span class="file">
                            <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
                            <label for="fu">찾아보기</label>
                        </span>
                        <asp:Button ID="btnUpload" runat="server" OnClientClick="return btnUploadCheck();" OnClick="btnUpload_Click" class="btn-black" Text="일괄등록" />
                    </div>
				    <div class="btnset">
                        <button id="btnRegID" type="button" onclick="PopupRegPartners();" class="btn-green">등록</button>
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="sField" style="font-weight:bold">
                            <option value="COMPANYCD">협력사코드</option>
                            <option value="OFFICENM">국사명</option>
                        </select>
                        <label for="Value">: </label>
                        <input id="sValue" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
