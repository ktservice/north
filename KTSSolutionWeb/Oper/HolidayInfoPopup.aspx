﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HolidayInfoPopup.aspx.cs" Inherits="KTSSolutionWeb.HolidayInfoPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>휴일정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
        .ui-datepicker {
            line-height: 1;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var chkOrgCd = '';
        var chkOrgNm = '';
        var gubun;
        var rtn;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $("#txtDate").datepicker({ dateFormat: 'yy-mm-dd' });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    $("#hdnStMonth").val($(".stMonth").val());
                    fn_Search();

                    return false;
                });

                $(".etMonth").change(function (e) {
                    $("#hdnEtMonth").val($(".etMonth").val());
                    fn_Search();

                    return false;
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });
                
                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Init = function () {
            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            $(".table-search").css('display', 'none');
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_CellDel = function (cell, formatterParams, onRendered) {
            var html = '';
            var hDate = cell.getRow().getData().HDATE;

            html = "<input id='btnDel' type='button' onclick='fn_DelTran(\"" + hDate + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            return html;
        }

        var fn_GridInit = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "휴일", field: "HDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "내용", field: "HDATENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "INPUTDATE", sorter: "string", width: 200 },
                { formatter: fn_CellDel, width: 80, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            fn_Search();
        }

        var ajaxCallBack = function (json) {
            if (callGb == 'RegTran') {
                ktsGridAlert("등록을 완료했습니다.");
                fn_Search();

                callGb = '';
            }
            else if (callGb == 'DelTran') {
                ktsGridAlert("삭제를 완료했습니다.");
                fn_Search();

                callGb = '';
            }
        }

        var fn_DelTran = function (hDate) {
            if (confirm("삭제를 하시겠습니까?")) {
                callGb = 'DelTran';
                fn_Transaction('D', hDate);
            }
        }

        var fn_RegTran = function () {
            if (confirm("등록 하시겠습니까?")) {
                var hDate = $(".date").val();

                callGb = 'RegTran';
                fn_Transaction('I', hDate);
            }
        }

        var fn_Transaction = function (gubun, hDate) {
            var pStatus = gubun;
            var pDate = '';
            var pContent = '';

            pDate = hDate;
            pContent = $("#txtContent").val();

            //입력 파라미터
            var aData = [];
            aData[0] = pStatus;
            aData[1] = pDate;
            aData[2] = pContent;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetHolidayInfoTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function () {
            var pDate1 = '';
            var pDate2 = '';

            pDate1 = $(".stMonth").val();
            pDate2 = $(".etMonth").val();

            //입력 파라미터
            var aData = [];
            aData[0] = pDate1;
            aData[1] = pDate2;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">휴일정보</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <div class="list-top">
                <fieldset>
                    <span class="inpbox">
                        <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" Style="width: 30% !important;"></asp:TextBox>
                        <label id="lblDt" style="width: 10px; margin-left: 7px">~</label>
                        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" Style="width: 30% !important;"></asp:TextBox>
                    </span>
                </fieldset>
            </div>
            <br />
            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
                            <div class="pull-right">
                                <div class="btnset">
                                    <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                                </div>
                            </div>
                        </div>
                        <!-- //E:list-top -->
                        <div>
                            <table id="tblReg">
                                <tbody>
                                    <tr>
                                        <th>휴일</th>
                                        <td class="td-left">
                                            <p class="inpbox">
                                                <asp:TextBox ID="txtDate" runat="server" class="date" ReadOnly="true" Width="120px"></asp:TextBox>
                                            </p>
                                        </td>
                                        <th>내용</th>
                                        <td class="td-left">
                                            <p class="inpbox">
                                                <input type="text" id="txtContent" maxlength="16" tabindex="1" placeholder="내용 입력" />
                                            </p>
                                        </td>
                                        <td>
                                            <input id="btnReg" type="button" class="btn-save" value="등록" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display: none;">
                                <span>
                                    <select id="selField">
                                        <option value="">선택</option>
                                        <option value="HDATE">휴일</option>
                                        <option value="HDATENM">내용</option>
                                        <option value="INPUTDATE">등록일</option>
                                    </select>
                                    : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin: 20px 30px 0 30px; display: grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                        <input id="hdnStMonth" type="hidden" />
                        <input id="hdnEtMonth" type="hidden" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>