﻿using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace KTSSolutionWeb
{
    public partial class BoardCategoryInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetDataList();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            try
            {
                DataSet ds = new DataSet();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "BOARDCATEGORYINFO";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];

                        rptResult.DataSource = dt;
                        rptResult.DataBind();

                        int nCnt = dt.Rows.Count;

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            TextBox txbCategory = (TextBox)rptResult.Items[i].FindControl("txbCategory");

                            bool bSaved = false;

                            if (txbCategory.Text.Length > 0)
                            {
                                bSaved = true;
                            }

                            Label lblOrder = (Label)rptResult.Items[i].FindControl("lblOrder");
                            DropDownList ddlOrder = (DropDownList)rptResult.Items[i].FindControl("ddlOrder");

                            for (int n = 1; n <= nCnt; n++)
                            {
                                ddlOrder.Items.Add(new ListItem(n.ToString(), n.ToString()));
                            }

                            ddlOrder.SelectedValue = lblOrder.Text;

                            Label lblUseYn = (Label)rptResult.Items[i].FindControl("lblUseYn");
                            CheckBox chkUseYn = (CheckBox)rptResult.Items[i].FindControl("chkUseYn");

                            chkUseYn.Checked = lblUseYn.Text.Equals("Y") ? true : false;

                            Button btnSave = (Button)rptResult.Items[i].FindControl("btnSave");
                            Button btnDelete = (Button)rptResult.Items[i].FindControl("btnDelete");

                            AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();
                            asyncBtnSave.ControlID = btnSave.UniqueID;
                            asyncBtnSave.EventName = "Click";

                            updPanel1.Triggers.Add(asyncBtnSave);

                            AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                            asyncBtnDelete.ControlID = btnDelete.UniqueID;
                            asyncBtnDelete.EventName = "Click";

                            updPanel1.Triggers.Add(asyncBtnDelete);

                            if (bSaved)
                            {
                                btnDelete.Visible = true;
                            }
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            GetDataList();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;                
                Label lblBoardType = (Label)btnSave.Parent.FindControl("lblBoardType");
                TextBox txbCategory = (TextBox)btnSave.Parent.FindControl("txbCategory");
                DropDownList ddlOrder = (DropDownList)btnSave.Parent.FindControl("ddlOrder");
                CheckBox chkUseYn = (CheckBox)btnSave.Parent.FindControl("chkUseYn");

                string strBoardType = lblBoardType.Text;
                string strCategory = txbCategory.Text;

                if (strBoardType.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.')", true);
                }
                else if (strCategory.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('항목명을 입력해주세요.')", true);
                }
                else
                {
                    string strOrder = ddlOrder.SelectedValue;
                    string strUseYn = chkUseYn.Checked ? "Y" : "N";
                    string strEmpNo = Session["EMPNO"].ToString();

                    string result = string.Empty;
                    string resultSession = string.Empty;
                    string rptCd = "BOARDCATEGORYTRAN";
                    string jobGb = "SET";

                    List<string> aData = new List<string>();

                    aData.Add(strBoardType);
                    aData.Add(strCategory);
                    aData.Add(strOrder);
                    aData.Add(strUseYn);
                    aData.Add(strEmpNo);

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        result = dbTransaction.GetJsonData();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionChk", "alert('세션이 만료되었습니다.');", true);
                        Session.Abandon();
                        Response.Redirect("/Login");
                    }

                    JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                    string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                    GetDataList();

                    if (errcd.Equals("00"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('항목 정보가 저장되었습니다.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('저장 중 오류가 발생했습니다.');", true);
                    }

                    this.updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnDelete = (Button)sender;
                Label lblBoardType = (Label)btnDelete.Parent.FindControl("lblBoardType");
                TextBox txbCategory = (TextBox)btnDelete.Parent.FindControl("txbCategory");

                string strBoardType = lblBoardType.Text;
                string strCategory = txbCategory.Text;

                if (strBoardType.Length == 0 || strCategory.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.')", true);
                }
                else
                {

                    string result = string.Empty;
                    string resultSession = string.Empty;
                    string rptCd = "BOARDCATEGORYDELETE";
                    string jobGb = "SET";

                    List<string> aData = new List<string>();

                    aData.Add(strBoardType);

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        result = dbTransaction.GetJsonData();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionChk", "alert('세션이 만료되었습니다.');", true);
                        Session.Abandon();
                        Response.Redirect("/Login");
                    }

                    JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                    string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                    GetDataList();

                    if (errcd.Equals("00"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('항목 정보가 삭제되었습니다.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('삭제 중 오류가 발생했습니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('삭제 중 오류가 발생했습니다.');", true);
            }
        }
    }
}