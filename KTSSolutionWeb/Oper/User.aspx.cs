﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.ExcelImport;
using OfficeOpenXml;
using System.Linq;

namespace KTSSolutionWeb
{
    public partial class User : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private DataTable DtAuthList
        {
            get
            {
                if (ViewState["DtAuthList"] != null)
                    return (DataTable)ViewState["DtAuthList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtAuthList"] = value;
            }
        }

        private string KTSYN
        {
            get
            {
                if (ViewState["KTSYN"] != null)
                    return ViewState["KTSYN"].ToString();
                else
                    return "N";
            }
            set
            {
                ViewState["KTSYN"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                PageInit();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PageInit()
        {
            try
            {
                MenuTree_Load();

                string strlocation = "";

                if (this.Session["KTSYN"].ToString().Equals("Y"))
                {
                    this.ddlKtsYn.Enabled = true;
                    this.thktsEmpNo.Visible = true;
                    this.thJobNm.Visible = true;
                    this.thJobGrade.Visible = true;
                    this.thJobNm2.Visible = true;
                    this.thEnterDt.Visible = true;
                    this.thExfireDt.Visible = true;
                    this.thIdmsEmpNo.Visible = true;
                    this.thPreIdmsEmpNo.Visible = true;

                    this.hdfOrgCd.Value = "000001";
                    this.txbTeam.Attributes.Remove("onclick");
                    this.btnTeam.Attributes.Remove("onclick");
                    this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");
                    this.btnTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");
                    this.lblTeam.Visible = true;
                    this.txbTeam.Visible = true;
                    this.btnTeam.Visible = true;

                    strlocation = "/Template/IDMS정보 Excel Upload Template.xlsx";
                }
                else
                {
                    this.ddlKtsYn.SelectedValue = "N";
                    this.ddlKtsYn.Enabled = false;
                    this.ddlKtsYn.Items.Remove(ddlKtsYn.Items.FindByValue("Y"));
                    this.thktsEmpNo.Visible = false;
                    this.thJobNm.Visible = false;
                    this.thJobGrade.Visible = false;
                    this.thJobNm2.Visible = false;
                    this.thEnterDt.Visible = false;
                    this.thExfireDt.Visible = false;
                    this.thIdmsEmpNo.Visible = false;
                    this.thPreIdmsEmpNo.Visible = false;

                    this.hdfOrgCd.Value = this.Session["ORGCD"].ToString();
                    this.txbTeam.Attributes.Remove("onclick");
                    this.btnTeam.Attributes.Remove("onclick");
                    this.lblTeam.Visible = false;
                    this.txbTeam.Visible = false;
                    this.btnTeam.Visible = false;

                    strlocation = "/Template/협력사사용자정보 Excel Upload Template.xlsx";

                }

                this.btnTemp.Attributes.Remove("onclick");
                this.btnTemp.Attributes.Add("onclick", "DownloadTemplate('" + strlocation + "');");

                SetAuthGroupList();

                SetOrgList();

                this.updPanelSearch.Update();

                this.udpTemp.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetAuthGroupList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (UserMgmt user = new UserMgmt())
                {
                    ds = user.GetAuthGroup();
                }

                if (ds.Tables.Count > 0)
                {
                    DtAuthList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hdfOrgCd.Value.ToString();
                string strEmpNm = txbEmpNm.Text.ToString().Trim();
                KTSYN = ddlKtsYn.SelectedValue.ToString();
                string strCompCd = this.Session["COMPANYCD"].ToString();

                using (UserMgmt user = new UserMgmt())
                {
                    ds = user.GetOperUserList(strOrgCd, strEmpNm, KTSYN, strCompCd);
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (KTSYN.Equals("Y"))
                {
                    thktsEmpNo.Visible = true;
                    thJobNm.Visible = true;
                    thJobGrade.Visible = true;
                    thJobNm2.Visible = true;
                    thEnterDt.Visible = true;
                    thExfireDt.Visible = true;
                    thIdmsEmpNo.Visible = true;
                    thPreIdmsEmpNo.Visible = true;
                }
                else
                {
                    thktsEmpNo.Visible = false;
                    thJobNm.Visible = false;
                    thJobGrade.Visible = false;
                    thJobNm2.Visible = false;
                    thEnterDt.Visible = false;
                    thExfireDt.Visible = false;
                    thIdmsEmpNo.Visible = false;
                    thPreIdmsEmpNo.Visible = false;
                }

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        private bool ValidationCheck(string strEmpNo, string strUserAuth, string strOrgCd, ref string strMsg)
        {
            bool bCheck = true;

            if (strEmpNo.Length == 0 || strOrgCd.Length == 0)
            {
                strMsg = "잘못된 접근입니다.";
                bCheck = false;            
            }
            else if (strUserAuth.Length == 0)
            {
                strMsg = "권한등급을 선택해 주세요.";
                bCheck = false;
            }

            return bCheck;
        }


        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = null;

            if (KTSYN.Equals("Y"))
            {
                ArrHeader = new string[19];

                ArrHeader[0] = "NUM";
                ArrHeader[1] = "ORGFULLNM";
                ArrHeader[2] = "USERORGCD";
                ArrHeader[3] = "EMPNO";
                ArrHeader[4] = "KTSEMPNO";
                ArrHeader[5] = "EMPNM";
                ArrHeader[6] = "MOBILE";
                ArrHeader[7] = "COMPANYNM";
                ArrHeader[8] = "COMPANYCD";
                ArrHeader[9] = "AUTHNM";
                ArrHeader[10] = "ORGCD";
                ArrHeader[11] = "PWDCHGYN";
                ArrHeader[12] = "JOBNM";
                ArrHeader[13] = "JOBGRADE";
                ArrHeader[14] = "JOBNM2";
                ArrHeader[15] = "ENTERDT";
                ArrHeader[16] = "EXFIREDT";
                ArrHeader[17] = "IDMSEMPNO";
                ArrHeader[18] = "PREIDMSEMPNO";
            }
            else
            {
                ArrHeader = new string[11];

                ArrHeader[0] = "NUM";
                ArrHeader[1] = "ORGFULLNM";
                ArrHeader[2] = "USERORGCD";
                ArrHeader[3] = "EMPNO";
                ArrHeader[4] = "EMPNM";
                ArrHeader[5] = "MOBILE";
                ArrHeader[6] = "COMPANYNM";
                ArrHeader[7] = "COMPANYCD";
                ArrHeader[8] = "AUTHNM";
                ArrHeader[9] = "ORGCD";
                ArrHeader[10] = "PWDCHGYN";
            }

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = null;

            if (KTSYN.Equals("Y"))
            {
                ArrHeader = new string[19];

                ArrHeader[0] = "No.";
                ArrHeader[1] = "소속";
                ArrHeader[2] = "조직코드";
                ArrHeader[3] = "사번";
                ArrHeader[4] = "KTS사번";
                ArrHeader[5] = "이름";
                ArrHeader[6] = "연락처";
                ArrHeader[7] = "업체명";
                ArrHeader[8] = "업체코드";
                ArrHeader[9] = "권한등급";
                ArrHeader[10] = "조직권한";
                ArrHeader[11] = "PWD초기화";
                ArrHeader[12] = "직무";
                ArrHeader[13] = "직책";
                ArrHeader[14] = "직무2";
                ArrHeader[15] = "입사일자";
                ArrHeader[16] = "퇴사일자";
                ArrHeader[17] = "IDMS사번";
                ArrHeader[18] = "IDMS(구)사번";
            }
            else
            {
                ArrHeader = new string[11];

                ArrHeader[0] = "No.";
                ArrHeader[1] = "소속";
                ArrHeader[2] = "조직코드";
                ArrHeader[3] = "사번";
                ArrHeader[4] = "이름";
                ArrHeader[5] = "연락처";
                ArrHeader[6] = "업체명";
                ArrHeader[7] = "업체코드";
                ArrHeader[8] = "권한등급";
                ArrHeader[9] = "조직권한";
                ArrHeader[10] = "PWD초기화";
            }

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    string strOrgCd = this.hdfOrgCd.Value.ToString();
                    string strEmpNm = this.txbEmpNm.Text.ToString().Trim();
                    KTSYN = this.ddlKtsYn.SelectedValue.ToString();
                    string strCompCd = this.Session["COMPANYCD"].ToString();

                    using (UserMgmt user = new UserMgmt())
                    {
                        ds = user.GetOperUserList(strOrgCd, strEmpNm, KTSYN, strCompCd);
                    }

                    dt = ds.Tables[0];
                }

                dt.Columns.Remove(dt.Columns["KTSYN"]);
                dt.Columns.Remove(dt.Columns["AUTHID"]);

                if (KTSYN.Equals("N"))
                {
                    dt.Columns.Remove(dt.Columns["KTSEMPNO"]);
                    dt.Columns.Remove(dt.Columns["JOBNM"]);
                    dt.Columns.Remove(dt.Columns["JOBGRADE"]);
                    dt.Columns.Remove(dt.Columns["JOBNM2"]);
                    dt.Columns.Remove(dt.Columns["ENTERDT"]);
                    dt.Columns.Remove(dt.Columns["EXFIREDT"]);
                    dt.Columns.Remove(dt.Columns["IDMSEMPNO"]);
                    dt.Columns.Remove(dt.Columns["PREIDMSEMPNO"]);
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "사용자관리");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            DropDownList ddlAuthGroup = (DropDownList)rptResult.Items[i].FindControl("ddlUserAuth");
                            Label lblAuthId = (Label)rptResult.Items[i].FindControl("lblAuthId");
                            Label lblKtsYn = (Label)rptResult.Items[i].FindControl("lblKtsYn");
                            Label lblCompanyCd = (Label)rptResult.Items[i].FindControl("lblCompanyCd");

                            HtmlTableCell tdktsEmpNo = (HtmlTableCell)rptResult.Items[i].FindControl("tdktsEmpNo");
                            HtmlTableCell tdJobNm = (HtmlTableCell)rptResult.Items[i].FindControl("tdJobNm");
                            HtmlTableCell tdJobGrade = (HtmlTableCell)rptResult.Items[i].FindControl("tdJobGrade");
                            HtmlTableCell tdJobNm2 = (HtmlTableCell)rptResult.Items[i].FindControl("tdJobNm2");
                            HtmlTableCell tdEnterDt = (HtmlTableCell)rptResult.Items[i].FindControl("tdEnterDt");
                            HtmlTableCell tdExfireDt = (HtmlTableCell)rptResult.Items[i].FindControl("tdExfireDt");
                            HtmlTableCell tdIdmsEmpNo = (HtmlTableCell)rptResult.Items[i].FindControl("tdIdmsEmpNo");
                            HtmlTableCell tdPreIdmsEmpNo = (HtmlTableCell)rptResult.Items[i].FindControl("tdPreIdmsEmpNo");

                            DataRow[] drAuth = DtAuthList.Select(string.Format("KTSYN = '{0}'", lblKtsYn.Text));

                            ddlAuthGroup.Items.Add(new ListItem("선택하세요", ""));

                            foreach (DataRow dr in drAuth)
                            {
                                ddlAuthGroup.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                            }

                            if (lblAuthId.Text.Length > 0 && ddlAuthGroup.Items.Count > 0)
                            {
                                ddlAuthGroup.SelectedValue = lblAuthId.Text;
                            }
                            else
                            {
                                ddlAuthGroup.SelectedIndex = 0;
                            }

                            Label lblOrgCd = (Label)rptResult.Items[i].FindControl("lblOrgCd");
                            Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");


                            HtmlButton btnOrg = (HtmlButton)rptResult.Items[i].FindControl("btnOrg");

                            if (lblKtsYn.Text.Equals("Y"))
                            {
                                //btnOrg.Attributes.Add("onclick", "javascript: PopupOrgTree2('" + lblOrgCd.Text + "', '" + lblEmpNo.Text + "');");
                                btnOrg.Attributes.Add("onclick", "javascript: PopupOrgTree('" + lblOrgCd.Text + "', '" + lblEmpNo.Text + "');");
                            }
                            else
                            {
                                //btnOrg.Attributes.Add("onclick", "javascript: PopupOrgTree3('" + lblOrgCd.Text + "');");
                                btnOrg.Attributes.Add("onclick", "javascript: PopupPartnersOrg('" + lblEmpNo.Text + "', '" + lblCompanyCd.Text + "');");
                            }

                            Label lblPwdReset = (Label)rptResult.Items[i].FindControl("lblPwdReset");
                            CheckBox chkResetPwd = (CheckBox)rptResult.Items[i].FindControl("chkResetPwd");

                            if (lblPwdReset.Text.ToString().Equals("Y"))
                            {
                                chkResetPwd.Checked = true;
                            }
                            else
                            {
                                chkResetPwd.Checked = false;
                            }

                            if (KTSYN.Equals("Y"))
                            {
                                tdktsEmpNo.Visible = true;
                                tdJobNm.Visible = true;
                                tdJobGrade.Visible = true;
                                tdJobNm2.Visible = true;
                                tdEnterDt.Visible = true;
                                tdExfireDt.Visible = true;
                                tdIdmsEmpNo.Visible = true;
                                tdPreIdmsEmpNo.Visible = true;
                            }
                            else
                            {
                                tdktsEmpNo.Visible = false;
                                tdJobNm.Visible = false;
                                tdJobGrade.Visible = false;
                                tdJobNm2.Visible = false;
                                tdEnterDt.Visible = false;
                                tdExfireDt.Visible = false;
                                tdIdmsEmpNo.Visible = false;
                                tdPreIdmsEmpNo.Visible = false;
                            }

                            Button btnSave = (Button)rptResult.Items[i].FindControl("btnSave");
                            Button btnDelete = (Button)rptResult.Items[i].FindControl("btnDelete");                             

                            AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();
                            asyncBtnSave.ControlID = btnSave.UniqueID;
                            asyncBtnSave.EventName = "Click";

                            AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                            asyncBtnDelete.ControlID = btnDelete.UniqueID;
                            asyncBtnDelete.EventName = "Click";

                            updPanel1.Triggers.Add(asyncBtnSave);
                            updPanel1.Triggers.Add(asyncBtnDelete);

                            updPanel1.Update();
                        }
                    }
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strOrgCd = this.hdfOrgCd.Value.Split(',');
                string strOrgNm = "";
                string strWhere = "";

                for (int i = 0; i < strOrgCd.Length; i++)
                {
                    if (strWhere.Length > 0)
                        strWhere += " OR ";

                    strWhere += string.Format("ORGCD = '{0}'", strOrgCd[i]);
                }

                DataRow[] dr = DtOrgList.Select(strWhere);
                DataTable dt = DtOrgList.Clone();

                if (dr.Length > 0)
                {
                    dt = dr.CopyToDataTable();
                }

                if(dt.Rows.Count == 1)
                {
                    strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                }
                else
                { 
                    foreach (DataRow drOrg in dt.Rows)
                    {
                        if (strOrgNm.Length > 0)
                            strOrgNm += ", ";

                        strOrgNm += drOrg["ORGNM"].ToString();
                    }
                }

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hdfEmpOrg_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] strEmpOrg = hdfEmpOrg.Value.Split('|');
                string strEmpNo = "";
                string strOrgCd = "";

                if (strEmpOrg.Length > 1)
                {
                    strEmpNo = strEmpOrg[0];
                    strOrgCd = strEmpOrg[1];
                }

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");
                    Label lblKtsYn = (Label)rptResult.Items[i].FindControl("lblKtsYn");
                    Label lblCompanyCd = (Label)rptResult.Items[i].FindControl("lblCompanyCd");

                    if (lblEmpNo.Text.Equals(strEmpNo))
                    {
                        Label lblOrgCd = (Label)rptResult.Items[i].FindControl("lblOrgCd");
                        HtmlButton btnOrg = (HtmlButton)rptResult.Items[i].FindControl("btnOrg");
                        lblOrgCd.Text = strOrgCd;
                        hdfEmpOrg.Value = "";
                        btnOrg.Attributes.Remove("onclick");
                        //btnOrg.Attributes.Add("onclick", "javascript: PopupOrgTree('" + strOrgCd + "', '" + strEmpNo + "');");

                        if (lblKtsYn.Text.Equals("Y"))
                        {
                            btnOrg.Attributes.Add("onclick", "javascript: PopupOrgTree('" + strOrgCd + "', '" + strEmpNo + "');");
                        }
                        else
                        {
                            btnOrg.Attributes.Add("onclick", "javascript: PopupPartnersOrg('" + strEmpNo + "', '" + lblCompanyCd.Text + "');");
                        }

                        break;
                    }
                }
                
                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblEmpNo = (Label)btnSave.Parent.FindControl("lblEmpNo");
                DropDownList ddlUserAuth = (DropDownList)btnSave.Parent.FindControl("ddlUserAuth");
                Label lblOrgCd = (Label)btnSave.Parent.FindControl("lblOrgCd");
                TextBox txbJobNm2 = (TextBox)btnSave.Parent.FindControl("txbJobNm2");
                CheckBox chkResetPwd = (CheckBox)btnSave.Parent.FindControl("chkResetPwd");
                TextBox txbIdmsEmpNo = (TextBox)btnSave.Parent.FindControl("txbIdmsEmpNo");
                TextBox txbPreIdmsEmpNo = (TextBox)btnSave.Parent.FindControl("txbPreIdmsEmpNo");

                string strPwdChgYn = "N";

                if (chkResetPwd.Checked)
                {
                    strPwdChgYn = "Y";
                }

                string strMsg = "";

                if (!ValidationCheck(lblEmpNo.Text, ddlUserAuth.SelectedValue, lblOrgCd.Text, ref strMsg))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strMsg + "')", true);
                }
                else
                {
                    using (UserMgmt user = new UserMgmt())
                    {
                        user.SetOperUserInfo(lblEmpNo.Text, ddlUserAuth.SelectedValue, lblOrgCd.Text, txbJobNm2.Text, strPwdChgYn, txbIdmsEmpNo.Text, txbPreIdmsEmpNo.Text);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('사용자 정보가 수정되었습니다.');", true);
                }

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 정보 저장 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblEmpNo = (Label)btnSave.Parent.FindControl("lblEmpNo");


                if (lblEmpNo.Text.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (UserMgmt user = new UserMgmt())
                    {
                        user.DelOperUserInfo(lblEmpNo.Text);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('사용자 정보가 삭제되었습니다.');", true);
                }

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 정보 삭제 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnUpload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                int nBaseCol = 0;
                string strUploadNm = "";
                string strTargetTable = "";
                string strKtsYn = this.ddlKtsYn.SelectedValue;
                string strMsg = "";
                string strEmpNo = this.Session["EMPNO"].ToString();

                if (strKtsYn.Equals("Y"))
                {
                    nBaseCol = 4;
                    strUploadNm = "사용자정보";
                    strTargetTable = "tb_userinfo_temp";
                    strMsg = "kts 직원의 idms사번 정보를 변경하였습니다.";
                }
                else
                {
                    nBaseCol = 5;
                    strUploadNm = "협력사사용자정보";
                    strTargetTable = "tb_userinfobypartners_temp";
                    strMsg = "협력사 사원 정보를 업로드 완료 하였습니다.";
                }

                Stream stream = Request.Files["fu"].InputStream;

                if (stream.Length > 0)
                {
                    string strtype = Request.Files["fu"].ContentType;

                    if (strtype.Equals("application/vnd.ms-excel") || strtype.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    {
                        using (ExcelImport excel = new ExcelImport())
                        {
                            using (ExcelPackage excelPackage = new ExcelPackage(stream))
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                if (worksheet.Dimension.End.Column != nBaseCol)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidatioChk", "alert('업로드 파일 양식이 잘못되었습니다.');", true);
                                }
                                else
                                {
                                    excel.ExcelUpLoad(strUploadNm, worksheet, strTargetTable);

                                    using (UserMgmt user = new UserMgmt())
                                    {
                                        if (strKtsYn.Equals("Y"))
                                        {
                                            user.SetOperUserInfoExcel();
                                        }
                                        else
                                        {
                                            user.SetOperPartnersUserInfoExcel(strEmpNo);
                                        }
                                    }

                                    GetDataList();
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('" + strMsg + "');", true);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('엑셀파일만 업로드가 가능합니다.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidatioChk", "alert('업로드 할 파일을 선택해주세요.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('엑셀 업로드 중 오류가 발생했습니다.');", true);
            }
        }

        protected void ddlKtsYn_SelectedIndexChanged(object sender, EventArgs e)
        {

            string strKtsYn = this.ddlKtsYn.SelectedValue;
            string strlocation = "";

            if (strKtsYn.Equals("Y"))
            {
                strlocation = "/Template/IDMS정보 Excel Upload Template.xlsx";
            }
            else
            {
                strlocation = "/Template/협력사사용자정보 Excel Upload Template.xlsx";
            }
            
            this.btnTemp.Attributes.Remove("onclick");
            this.btnTemp.Attributes.Add("onclick", "DownloadTemplate('" + strlocation + "');");

            this.udpTemp.Update();
        }
    }
}