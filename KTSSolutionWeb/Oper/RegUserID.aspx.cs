﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class RegUserID : PageBase
    {
        public string bIDCheck
        {
            get
            {
                if (ViewState["IDCheck"] != null)
                {
                    return ViewState["IDCheck"].ToString();
                }
                else
                {
                    return "N";
                }
            }
            set
            {
                ViewState["IDCheck"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void PageInit()
        {
            string strKtsYn = this.Session["KTSYN"].ToString();

            SetDDLAuthGroup(strKtsYn);
            SetDDLCompany(strKtsYn);

            if (strKtsYn.Equals("Y"))
            {
                SetPageInfo(strKtsYn);
                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree();");
                this.btnTeam.Attributes.Add("onclick", "PopupOrgTree();");
            }
            else
            {
                SetPageInfo(strKtsYn);
                this.ddlKtsYn.SelectedValue = "N";
                this.ddlKtsYn.Enabled = false;
                this.ddlKtsYn.Items.Remove(ddlKtsYn.Items.FindByValue("Y"));

                string strCompanyCd = this.Session["COMPANYCD"].ToString();

                this.txbTeam.Attributes.Add("onclick", "PopupPartnersOrg('" + strCompanyCd + "');");
                this.btnTeam.Attributes.Add("onclick", "PopupPartnersOrg('" + strCompanyCd + "');");
            }

            updPanel1.Update();
        }

        private void SetDDLAuthGroup(string strKtsYn)
        {
            DataSet ds = new DataSet();

            try
            {
                using (UserMgmt userReg = new UserMgmt())
                {                   
                    ds = userReg.GetUserAuthGroup(strKtsYn);

                    this.ddlAuth.Items.Clear();

                    this.ddlAuth.Items.Add(new ListItem("선택하세요", ""));

                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            this.ddlAuth.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                        }

                        this.ddlAuth.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetDDLCompany(string strKtsYn)
        {
            DataSet ds = new DataSet();

            try
            {
                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetPartnersList();

                    this.ddlCompany.Items.Clear();

                    if (strKtsYn.Equals("Y"))
                    {
                        this.ddlCompany.Items.Add(new ListItem("선택하세요", ""));

                        if (ds.Tables.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                this.ddlCompany.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                            }
                        }
                    }
                    else
                    {
                        string strCompanyCd = this.Session["COMPANYCD"].ToString();
                        string strCompanyNm = this.Session["COMPANYNM"].ToString();

                        this.ddlCompany.Items.Add(new ListItem(strCompanyNm, strCompanyCd));
                    }

                    this.ddlCompany.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetPageInfo(string strKtsYn)
        {
            try
            {
                SetDDLAuthGroup(strKtsYn);

                bIDCheck = "N";
                this.txbTeam.Text = "";

                this.txbEmpNo.Text = "";
                this.txbKtsEmpNo.Text = "";
                this.txbTelNo.Text = "";
                this.txbEmpNm.Text = "";
                this.txbIdmsEmpNo.Text = "";
                this.txbPreIdmsEmpNo.Text = "";

                this.ddlCompany.SelectedValue = "";

                this.ddlAuth.SelectedIndex = 0;

                this.hdfOrgCd.Value = "";

                if (strKtsYn.Equals("Y"))
                {
                    this.tdEmpNo.ColSpan = 1;
                    this.thKtsEmpNo.Visible = true;
                    this.tdKtsEmpNo.Visible = true;

                    this.trCompany.Visible = false;

                    this.trIdms.Visible = true;
                    this.trIdms.Visible = true;

                    this.txbTeam.Attributes.Remove("onclick");
                    this.btnTeam.Attributes.Remove("onclick");

                    this.txbTeam.Attributes.Add("onclick", "PopupOrgTree();");
                    this.btnTeam.Attributes.Add("onclick", "PopupOrgTree();");
                }
                else
                {
                    this.tdEmpNo.ColSpan = 3;
                    this.thKtsEmpNo.Visible = false;
                    this.tdKtsEmpNo.Visible = false;

                    this.trCompany.Visible = true;

                    this.trIdms.Visible = false;
                    this.trIdms.Visible = false;

                    this.txbTeam.Attributes.Remove("onclick");
                    this.btnTeam.Attributes.Remove("onclick");

                    if (this.Session["KTSYN"].ToString().Equals("N"))
                    {
                        string strCompanyCd = this.Session["COMPANYCD"].ToString();

                        this.txbTeam.Attributes.Add("onclick", "PopupPartnersOrg('" + strCompanyCd + "');");
                        this.btnTeam.Attributes.Add("onclick", "PopupPartnersOrg('" + strCompanyCd + "');");
                    }
                    else
                    {
                        this.txbTeam.Attributes.Add("onclick", "PopupPartnersOrg('');");
                        this.btnTeam.Attributes.Add("onclick", "PopupPartnersOrg('');");
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ValidationCheck(ref string strMsg)
        {
            bool bCheck = true;

            string strEmpNo = Request.Form[txbEmpNo.UniqueID];
            string strTeamFullNm = Request.Form[txbTeam.UniqueID];
            string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
            string strEmpNm = Request.Form[txbEmpNm.UniqueID];
            string strTelNo = Request.Form[txbTelNo.UniqueID];

            if (!bIDCheck.Equals("Y"))
            {
                strMsg = "사번 확인을 해주세요.";
                return false;
            }

            if (strEmpNo.Equals(""))
            {
                strMsg = "로그인 사번을 입력해주세요.";
                return false;
            }

            if (this.ddlKtsYn.SelectedValue.Equals("N"))
            {
                string strCompCd = this.ddlKtsYn.SelectedValue;

                if (strCompCd.Equals(""))
                {
                    strMsg = "업체를 선택해주세요.";
                    return false;
                }
            }

            if (strTeamFullNm.Equals("") || strOrgCd.Equals(""))
            {
                strMsg = "조직을 선택해주세요.";
                return false;
            }

            if (strEmpNm.Equals(""))
            {
                strMsg = "이름을 입력해주세요.";
                return false;
            }

            if (strTelNo.Equals(""))
            {
                strMsg = "연락처를 입력해주세요.";
                return false;
            }

            if (ddlAuth.SelectedValue.Equals(""))
            {
                strMsg = "권한을 선택해주세요.";
                return false;
            }

            return bCheck;
        }

        protected void BtnIDCheck_Click(object sender, EventArgs e)
        {
            string strEmpNo = Request.Form[txbEmpNo.UniqueID];
            bool bExistUser = false;
            string strMsg = "";

            DataSet ds = new DataSet();

            try
            {
                if (strEmpNo.Trim().Length == 0)
                {
                    strMsg = "잘못된 접근입니다.";
                }
                else
                {
                    using (KTSUser ktsUser = new KTSUser())
                    {
                        ds = ktsUser.GetUserLogInfo(strEmpNo);

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bExistUser = true;
                            }
                        }
                    }

                    if (bExistUser)
                    {
                        strMsg = "이미 등록된 사번 입니다.";
                    }
                    else
                    {
                        bIDCheck = "Y";

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "IDChkOK", "alert('등록 가능한 사번 입니다.');", true);
                    }
                }

                if (strMsg.Length > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DDLKtsYn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strKtsYn = this.ddlKtsYn.SelectedValue;

                SetPageInfo(strKtsYn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void DDLCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strCompanyCd = ddlCompany.SelectedValue;

                this.txbTeam.Attributes.Remove("onclick");
                this.btnTeam.Attributes.Remove("onclick");

                this.txbTeam.Attributes.Add("onclick", "PopupPartnersOrg('" + strCompanyCd + "');");
                this.btnTeam.Attributes.Add("onclick", "PopupPartnersOrg('" + strCompanyCd + "');");

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
                bool bValCheck = true;

                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetOrgInfo(strOrgCd);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.txbTeam.Text = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString();

                            this.updPanel1.Update();
                        }
                        else
                        {
                            bValCheck = false;
                        }
                    }
                    else
                    {
                        bValCheck = false;
                    }

                    if (!bValCheck)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void BtnRegedit_Click(object sender, EventArgs e)
        {
            try
            {
                string strMsg = "";

                if (ValidationCheck(ref strMsg))
                {
                    string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
                    string strEmpNo = Request.Form[txbEmpNo.UniqueID];
                    string strKtsEmpNo = "";
                    string strCompCd = "";
                    string strCompNm = "";
                    string strEmpNm = Request.Form[txbEmpNm.UniqueID];
                    string strTelNo = Request.Form[txbTelNo.UniqueID];
                    string strAuth = this.ddlAuth.SelectedValue;
                    string strIdmsEmpNo = "";
                    string strPreIdmsEmpNo = "";

                    string strKTSYN = this.ddlKtsYn.SelectedValue;

                    if (strKTSYN.Equals("Y"))
                    {
                        strKtsEmpNo = Request.Form[txbKtsEmpNo.UniqueID];
                        strIdmsEmpNo = Request.Form[txbIdmsEmpNo.UniqueID];
                        strPreIdmsEmpNo = Request.Form[txbPreIdmsEmpNo.UniqueID];
                    }
                    else
                    {
                        strCompCd = ddlCompany.SelectedValue;
                        strCompNm = ddlCompany.Items[ddlCompany.SelectedIndex].Text;
                    }

                    using (UserMgmt userReg = new UserMgmt())
                    {
                        userReg.InsertUserInfo(strEmpNo, strKtsEmpNo, strEmpNm, strTelNo, strIdmsEmpNo, strPreIdmsEmpNo, strOrgCd, strKTSYN, strCompCd, strCompNm, strAuth);
                        
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOK", "alert('사용자 계정등록이 완료되었습니다.'); window.close();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}