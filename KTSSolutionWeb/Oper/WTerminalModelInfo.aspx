﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WTerminalModelInfo.aspx.cs" Inherits="KTSSolutionWeb.WTerminalModelInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/lengthCheck.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
            text-align: left !important;
        }

        .td-width1 {
            width: 300px !important;
        }

        .selUse {
            width: 100%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: transparent!important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        .txtProduct {
            width: 200px;
            height: 100%;
            text-align:center;
            border: 1px solid #999 !important;
            border-radius: 5px !important;
            box-sizing: content-box;
            background: transparent!important;
            font-family: kt,Dotum,Arial,sans-serif;
            font-weight: 200;
            letter-spacing: 0.2px;
            cursor:pointer;
        }


    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = '';
        var callGb;
        var cellUseYnNm = '';

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {


                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "무선단말모델정보.xlsx", { sheetName: "무선단말모델정보" });
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnExReg").click(function (e) {
                    fn_ExReg();
                });

                $("#btnExClose").click(function (e) {
                    $("#divExReg").hide();
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#divExReg").hide();
        }

        var fn_ExBind = function (id) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var pModelCd = rowData.MODELCD;
            var pModelNm = rowData.MODELNM;
            var pProductNm = rowData.PRODUCTNM;


            $('#txtModelCd').val(pModelCd);
            $('#txtModelNm').val(pModelNm);
            $('#txtProductNm').val(pProductNm);
            $('#hdnId').val(id);

            $("#txtModelCd").attr('readonly', true);
            $("#txtModelCd").attr('style', 'background: #d1d1d1 !important');
            $("#txtModelNm").attr('readonly', true);
            $("#txtModelNm").attr('style', 'background: #d1d1d1 !important');


            var targetTop = $("#ktsGrid").offset().top;
            var targetHeight = $("#ktsGrid").height();
            var targetLeft = $("#ktsGrid").offset().left;
            var targetWidth = $("#ktsGrid").width();
            var vMiddle = targetTop + 120;
            var vCenter = targetLeft + (targetWidth / 2) - ($("#divExReg").width() / 2);

            $("#divExReg").css('background', '#fff');
            $("#divExReg").css('position', 'absolute');


            $("#divExReg").css('top', vMiddle);
            $("#divExReg").css('left', vCenter);

            $("#divExReg").show();

        }

        var fn_ExReg = function () {
            var modelCd = $("#txtModelCd").val();
            var productNm = $("#txtProductNm").val();

            if (!getStringLengthCheck(productNm, 100)) {
                alert('품명값이 초과됐습니다.');
                return false;
            }
            else {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'TranPM';
                    fn_Transaction('PM', modelCd, productNm);
                }
            }
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellInput = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == 'Y') value = '사용';
            else if(value == 'N') value = '미사용';

            cell.getElement().style.backgroundColor = "#f1f1f1";
            cell.getElement().style.fontWeight = "500";

            return value;
        };

        var fn_CellProductReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;

            html = "<input id='txtProduct' type='text' value=\"" + cellValue + "\" class='txtProduct' onclick='fn_ExBind(\"" + id + "\");' readonly>";

            return html;
        }


        var fn_CellUseReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;
            

            html = "<select id='selReg' class='selUse' onchange='fn_UseReg(this, \"" + id + "\");'>";
            html += "<option value='사용'>사용</opiton>";
            html += "<option value='미사용'>미사용</opiton>";
            html += "</select>";

            if (cellValue == '사용') {
                html = html.replace("<option value='사용'>", "<option value='사용' selected='selected'>");
            }
            else if (cellValue == '미사용') {
                html = html.replace("<option value='미사용'>", "<option value='미사용' selected='selected'>");
            }


            return html;
        }

        var fn_UseReg = function (obj, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            if (confirm("변경을 진행하시겠습니까?")) {
                var modelCd = rowData.MODELCD;
                var useYnNm = obj.value;
                var useYn = '';

                if (useYnNm == '사용') useYn = 'Y';
                else useYn = 'N';

                cellUseYnNm = useYnNm;
                cellNo = id;

                callGb = 'TranUM';
                fn_Transaction('UM', modelCd, useYn);
            }
            else {
                var originValue = rowData.USEYNNM;

                //원본으로 복귀
                obj.value = originValue;
            }
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "모델ID", field: "MODELCD", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "품명", field: "PRODUCTNM", sorter: "string", width: 210, formatter: fn_CellProductReg },
                { headerHozAlign: "center", hozAlign: "center", title: "사용유무", field: "USEYNNM", sorter: "string", width: 120, formatter: fn_CellUseReg },
                { headerHozAlign: "center", hozAlign: "center", title: "사용유무코드", field: "USEYN", sorter: "string", visible:false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'TranUM') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('변경되었습니다.', 1000);

                        var row = ktsGrid.getRow(cellNo);
                        row.select();

                        ktsGrid.updateData([{
                            id: cellNo, USEYNNM: cellUseYnNm
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });

                        cellNo = '';
                        cellUseYnNm = '';
                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });

                callGb = '';
            }
            else if (callGb == 'TranPM') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        $('#divExReg').hide();
                        ktsGridAlert('변경되었습니다.', 1000);

                        cellNo = $('#hdnId').val();

                        var row = ktsGrid.getRow(cellNo);
                        var productNm = $("#txtProductNm").val();
                        row.select();

                        ktsGrid.updateData([{
                            id: cellNo, PRODUCTNM: productNm
                        }])
                            .then(function () {
                                //run code after row
                            })
                            .catch(function (error) {
                                //handle error
                            });

                        cellNo = '';

                        $("#hdnId").val('');
                        $("#txtProductNm").val('');

                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });

                callGb = '';
            }

        }

        var fn_PopupUpload = function () {
            var form = "/Oper/WTerminalModelInfoUploadPopup";
            var target = "WTerminalModelInfoUploadPopup";
            var param = {
            };
            var nWidth = 840;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_Transaction = function (gubun, modelCd, value) {
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = modelCd;
            aData[2] = value;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function () {
            //입력 파라미터
            var aData = [];
            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>
                <div class="btnSearchWrap">
                    <input id="btnUpload" type="button" class="btn-green last" value="일괄등록" />
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>무선단말 모델정보 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="MAKERNM">제조사</option>
                            <option value="PRODUCTNM">품명</option>
                            <option value="MODELCD">모델ID</option>
                            <option value="MODELNM">모델명</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <div id="divExReg">
                <table>
                    <tr>
                        <th style="width: 120px;">모델ID</th>
                        <td class="td-left">
                            <p class="inpbox">
                                <input type="text" id="txtModelCd" class="td-width1" />
                                <input type="hidden" id="hdnId" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 120px;">모델명</th>
                        <td class="td-left">
                            <p class="inpbox">
                                <input type="text" id="txtModelNm" class="td-width1" placeholder="모델명 입력" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 120px;">품명</th>
                        <td class="td-left">
                            <p class="inpbox">
                                <input type="text" id="txtProductNm" maxlength="40" class="td-width1" placeholder="품명 입력" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input id="btnExReg" type="button" class="btn-gray" value="변경" />
                            <button id="btnExClose" type="button" class="btn-green">닫기</button>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>