﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaterialCodeMapping.aspx.cs" Inherits="KTSSolutionWeb.MaterialCodeMapping" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .searchbox {
            height:140px;
        }

        .plusicon {
            background:#fff url('/Resource/Mobile_images/icon_plus_01.png')no-repeat 50%;
            width:11px;
            height:11px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var ktsGrid1;
        var ktsGrid2;
        var ktsGrid2RowCnt = 0;
        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var cellUnit;
        var cellUnitCnt;

        var page = {
            init: function () {
                fn_Init();
                fn_LeftGridInit();
                fn_RightGridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selClass1").change(function (e) {
                    fn_Class(this.value, 2, 'selClass2');
                });

                $("#selClass2").change(function (e) {
                    fn_Class(this.value, 3, 'selClass3');
                });

                $("#selClass3").change(function (e) {
                    fn_Search('2', '', this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field1();
                });

                $("#selField2").change(function (e) {
                    fn_Field2();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#inputValue2").keyup(function (e) {
                    var fieldEl = document.getElementById("selField2");
                    var valueEl = document.getElementById("inputValue2");

                    ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    var nm = $("#txtProductNm").val();
                    fn_Search('1', nm, '');
                });

                $("#btnNoSelect").click(function (e) {
                    fn_Search('11', '', '');
                });

                $("#btnClass").click(function (e) {
                    fn_PopupClass();
                });

                $("#btnCode").click(function (e) {
                    fn_PopupCode();
                });
                
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnMappingReg' type='button' onclick='fn_RegTran(\"" + cellNo + "\");' value='매핑' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
            return html;
        }

        var fn_CellDelBo = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnMappingReg' type='button' onclick='fn_DelTran(\"" + cellNo + "\");' value='매핑제거' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnMappingMo' type='button' onclick='fn_MoTran(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            //html += "<input id='btnMappingBo' type='button' onclick='fn_BoTran(\"" + cellNo + "\");' value='보정' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
            return html;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);

            return value;
        }

        var fn_CellColor = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            cell.getElement().style.backgroundColor = "#f1f1f1";
            cell.getElement().style.fontWeight = "500";
            value = priceToString(value);
            return value;
        }

        var fn_CellUnit = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            cellUnit = cell.getRow().getData().UNIT;
            cellUnitCnt = cell.getRow().getData().UNITCNT;

            return value;
        }

        var inputEditor = function (cell, onRendered, success, cancel) {
            var value = cell.getValue();
            var input = document.createElement("input");

            if (value == '0') value = '';

            input.setAttribute("type", "text");
            input.placeholder = "변경 수량 입력";
            input.style.padding = "3px";
            input.style.width = "100%";
            input.style.boxSizing = "border-box";
            input.style.fontWeight = "500";
            input.maxLength = 11;
            input.value = value;

            onRendered(function () {
                input.focus();
                input.style.height = "100%";
            });


            function onSuccess() {
                if (input.value == '') input.value = '0';
                success(input.value);
            }

            //submit new value on blur or change
            input.addEventListener("blur", onSuccess);

            //submit new value on enter
            input.addEventListener("keydown", function (e) {
                if (e.keyCode >= 48 && e.keyCode <= 57 || (event.keyCode >= 96 && event.keyCode <= 105)
                    || (event.keyCode == 8) //백 스페이스
                ) { //숫자키만 입력
                    return input.value;
                } else {
                    cancel();
                }
            });

            return input;
        };

        var fn_LeftGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "id", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "PRODUCTNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "left", title: "규격", field: "STANDARD", sorter: "string" },
                { formatter: fn_CellReg, width: 110, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid1 = ktsGrid;
        }

        var fn_RightGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "id", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "PRODUCTNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "자재단위", field: "UNIT", sorter: "string", width: 90, formatter: fn_CellUnit },
                { headerHozAlign: "center", hozAlign: "right", title: "자재수량", field: "UNITCNT", sorter: "number", width: 100, editor: inputEditor, formatter: fn_CellColor },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스코드4명", field: "SERVICECTGLVCD4", sorter: "string", visible: false },
                { formatter: fn_CellDelBo, width: 220, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid2', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid2 = ktsGrid;
        }

        var fn_Load = function () {
            fn_Class('', '1', 'selClass1');
        }

        var fn_RegTran = function (id) {
            if (confirm("매핑을 하시겠습니까?")) {
                callGb = 'RegTran';
                fn_Transaction('I', id);
            }
        }

        var fn_DelTran = function (id) {
            if (confirm("매핑제거를 하시겠습니까?")) {
                callGb = 'DelTran';

                ktsGrid2RowCnt = ktsGrid2.getDataCount();
                fn_Transaction('D', id);
            }
        }

        var fn_MoTran = function (id) {
            if (confirm("변경 하시겠습니까?")) {
                callGb = 'MoTran';

                ktsGrid2RowCnt = ktsGrid2.getDataCount();
                fn_Transaction('M', id);
            }
        }

        var fn_BoTran = function (id) {
            if (confirm("관련 DATA를 보정 하시겠습니까?")) {
                callGb = 'BoTran';

                fn_TransactionBo(id);
            }
        }

        var fn_Search = function (gubun, nm, lv4cd) {
            if (gubun == '1' || gubun == '11') {
                fn_LeftGridInit();
            }
            else {
                fn_RightGridInit();

                var resultTxt = '';
                resultTxt = $("#selClass1 option:selected").text();
                resultTxt += ' > ' + $("#selClass2 option:selected").text();
                resultTxt += ' > ' + $("#selClass3 option:selected").text();
                $("#lblServiceRlt").text(resultTxt);
            }
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = nm; //$("#txtProductNm").val();
            aData[2] = lv4cd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            if (gubun == '1' || gubun == '11') {
                ktsRowCountClass = 'searchrowcount';
                grid.getDataBind(jsonData, ajaxUrl);
            }
            else {
                ktsRowCountClass = 'searchrowcount2';
                grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid2');
            }

            $(".table-option").css('display', 'block');
        }

        var fn_TransactionBo = function (productCd) {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#selClass3").val();
            aData[1] = productCd;
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetServiceCfgBo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun, productCd) {
            var param = '';
            if (gubun == "M") {
                var row = ktsGrid2.getRow(productCd);
                var rowData = row.getData();
                param = rowData.UNITCNT;
            }
            else {
                param = $("#selClass3").val();
            }

            //입력 파라미터
            var aData = [];
            aData[0] = param;
            aData[1] = productCd;
            aData[2] = gubun;
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetServiceCfgTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selClass1' || callGb == 'selClass2' || callGb == 'selClass3') {
                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });

                obj.val(obj.val()).trigger('change');
            }
            else if (callGb == 'RegTran') {
                var value = $("#selClass3").val();
                ktsGridId = 'ktsGrid';
                ktsGridAlert("매핑을 완료했습니다.");
                fn_Search('2', '', value);

                callGb = '';
            }
            else if (callGb == 'DelTran') {
                var value = $("#selClass3").val();
                ktsGridId = 'ktsGrid2';
                fn_Search('2', '', value);

                ktsGridAlert("매핑제거를 완료했습니다.");
                callGb = '';

                if (ktsGrid2RowCnt == 1) {
                    location.reload();
                }
            }
            else if (callGb == 'MoTran') {
                var value = $("#selClass3").val();
                ktsGridId = 'ktsGrid2';
                fn_Search('2', '', value);

                ktsGridAlert("변경을 완료했습니다.");
                callGb = '';
            }
            else if (callGb == 'BoTran') {
                var value = $("#selClass3").val();
                ktsGridId = 'ktsGrid2';
                ktsGridAlert("DATA 보정을 완료했습니다.");
                callGb = '';
            }
        }

        var fn_PopupCode = function () {
            var form = "/Oper/MaterialCodePopup";
            var target = "MaterialCodePopup";
            var param = {
            };

            var nWidth = 900;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupClass = function () {
            var form = "/Oper/MaterialCodeMappingPopup";
            var target = "MaterialCodeMappingPopup";
            var param = {
                pClassNm1: $("#selClass1 option:selected").text(),
                pClassNm2: $("#selClass2 option:selected").text(),
                pClassNm3: $("#selClass3 option:selected").text(),
                pClassCd3: $("#selClass3 option:selected").val(),
                pUnit: cellUnit,
                pUnitCnt: cellUnitCnt
            };

            var nWidth = 720;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetDispClass = function (check) {

            if (check == 'ok') {
                fn_Class('', '1', 'selClass1');
            }
        }

        var fn_Field1 = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid1.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Field2 = function () {
            var fieldEl = document.getElementById("selField2");
            var valueEl = document.getElementById("inputValue2");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid2.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Class = function (val, lv, gb) {
            callGb = gb;

            var sVal1 = '';
            var sVal2 = '';
            var sVal3 = '';

            if ($("#selClass1 option:selected").val() != undefined) {
                sVal1 = $("#selClass1").val();
            }
            if ($("#selClass2 option:selected").val() != undefined) {
                sVal2 = $("#selClass2").val();
            }
            if ($("#selClass3 option:selected").val() != undefined) {
                sVal3 = $("#selClass3").val();
            }

            //입력 파라미터
            var aData = [];
            aData[0] = lv;
            aData[1] = '';
            aData[2] = sVal1;
            aData[3] = sVal2;
            aData[4] = sVal3;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <ul>
                <li>
                    <!-- S:leftbox -->
                    <div class="leftbox">
                        <!-- S:searchbox -->
                        <div class="searchbox">
                            <fieldset>
                                <span class="inpbox first">
                                    <label>상품코드명</label>
                                    <input type="text" id="txtProductNm" maxlength="50" tabindex="1" style="width: 300px;" placeholder="값 입력" />
                                </span>
                                <div class="btnSearchWrap">
                                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                                </div>
                                <hr />
                                <div class="btnSearchWrap">
                                    <input id="btnNoSelect" type="button" class="btn-green last" value="미매핑 상품코드 조회" />
                                </div>
                            </fieldset>
                        </div>
                        <!-- E:searchbox -->
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <!-- S:datalist -->
                                <div class="datalist">
                                    <!-- S:list-top -->
                                    <div class="list-top">
                                        <strong>상품코드 조회 결과</strong>
                                        <label class="searchrowcount"></label>
                                    </div>
                                    <!-- //E:list-top -->
                                    <!-- S:scrollbox -->
                                    <div class="scrollbox">
                                        <div class="table-search" style="display: none;">
                                            <span>
                                                <select id="selField">
                                                    <option value="">선택</option>
                                                    <option value="id">상품코드</option>
                                                    <option value="PRODUCTNM">상품명</option>
                                                </select>
                                                : 
                                        <input id="inputValue" disabled="disabled" type="text" />
                                            </span>
                                        </div>
                                        <div id="ktsGrid"></div>
                                    </div>
                                    <!-- E:scrollbox -->
                                </div>
                                <!-- E:datalist -->
                            </div>
                        </div>
                    </div>
                    <!-- E:leftbox -->
                </li>
                <li>
                    <!-- S:rightbox -->
                    <div class="rightbox">
                        <!-- S:searchbox -->
                        <div class="searchbox">
                            <fieldset>
                                <span class="optionbox">
                                    <label>대분류</label>
                                    <select id="selClass1">
                                        <option selected="selected" value=""></option>
                                    </select>
                                </span>
                                <span class="optionbox">
                                    <label>중분류</label>
                                    <select id="selClass2">
                                        <option selected="selected" value=""></option>
                                    </select>
                                </span>
                                <span class="optionbox">
                                    <label>소분류</label>
                                    <select id="selClass3">
                                        <option selected="selected" value=""></option>
                                    </select>
                                </span>
                                <div class="btnSearchWrap">
                                    <input id="btnClass" type="button" class="btn-green last" value="서비스코드 등록" />
                                </div>
                                <hr />
                                <div class="btnSearchWrap">
                                    <input id="btnCode" type="button" class="btn-green last" value="자재코드정보" />
                                </div>
                            </fieldset>
                        </div>
                        <!-- E:searchbox -->
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:list-top -->
                            <div class="list-top">
                                <strong>
                                <span id="lblServiceRlt"></span></strong>
                                <label class="searchrowcount2"></label>
                            </div>
                            <!-- //E:list-top -->
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <div class="table-search" style="display: none;">
                                    <span>
                                        <select id="selField2">
                                            <option value="">선택</option>
                                            <option value="id">상품코드</option>
                                            <option value="PRODUCTNM">상품명</option>
                                        </select>
                                        : 
                                <input id="inputValue2" disabled="disabled" type="text" />
                                    </span>
                                </div>
                                <div id="ktsGrid2"></div>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:rightbox -->
                </li>
            </ul>
        </div>
        <!-- E:multiplebox -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>
