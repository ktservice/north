﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PortalMotCodeMapping.aspx.cs" Inherits="KTSSolutionWeb.PortalMotCodeMapping" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        /* .searchbox {
            height:140px;
        } */

        .plusicon {
            background:#fff url('/Resource/Mobile_images/icon_plus_01.png')no-repeat 50%;
            width:11px;
            height:11px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var ktsGridRowCnt = 0;
        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var cellUnit;
        var cellUnitCnt;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selClass").change(function (e) {
                    fn_Search(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });


                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnCodeReg").click(function (e) {
                    fn_PopupCodeReg('I', '');
                });

                $("#btnCode").click(function (e) {
                    fn_PopupCode();
                });

                $("#btnNoMapping").click(function (e) {
                    fn_NoMapping();
                });
                
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#divProductListData").hide();
        }

        var fn_excelDown = function (id, sheetNm, fileNm, type, fn,) {
            var elt = document.getElementById(id);
            var wb = XLSX.utils.table_to_book(elt, { sheet: sheetNm });
            XLSX.writeFile(wb, fileNm + '.' + type);
        }

        var fn_CellFunction = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnMappingDel' type='button' onclick='fn_DelTran(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnMappingMo' type='button' onclick='fn_PopupCodeReg(\"M\",\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            return html;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "PRODUCTCD", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "PRODUCTNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스코드", field: "SERVICECD", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "서비스명", field: "SERVICECNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "left", title: "상품설명", field: "PRODUCTDESC", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "right", title: "판매실적급포인트", field: "MOT_POINT", sorter: "number", width: 134, formatter: fn_CellNumber, visible:false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용유무", field: "USEYN", sorter: "string", width: 100 },
                { formatter: fn_CellFunction, width: 220, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            fn_Class('selClass');
        }

        var fn_DelTran = function (id) {
            if (confirm("삭제를 하시겠습니까?")) {
                callGb = 'DelTran';

                ktsGridRowCnt = ktsGrid.getDataCount();

                fn_Transaction('D', id);
            }
        }

        var fn_Search = function (serviceCd) {
            var resultTxt = '';
            resultTxt = $("#selClass option:selected").text();
            $("#lblServiceRlt").text(resultTxt);

            //입력 파라미터
            var aData = [];
            aData[0] = serviceCd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            ktsRowCountClass = 'searchrowcount';
            grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid');

            $(".table-option").css('display', 'block');
        }

        var fn_NoMapping = function () {
            callGb = 'noMapping';

            //입력 파라미터
            var aData = [];
            aData[0] = 'nomapping';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');
            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var productCd = rowData.PRODUCTCD;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = '';
            aData[2] = '';
            aData[3] = productCd;
            aData[4] = '';
            aData[5] = '';
            aData[6] = '';
            aData[7] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selClass') {
                obj = $('#' + callGb);
                obj.empty();

                obj.append("<option value='all'>전체</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.SERVICECD + "'>" + item.SERVICECNM + "</option>");
                });

                obj.val(obj.val()).trigger('change');
            }
            if (callGb == 'noMapping') {
                fn_ListBind(json);
                fn_excelDown('tblProductListData', '미매핑상품', '미매핑상품', 'xlsx');
                callGb = '';
            }
            else if (callGb == 'DelTran') {
                var value = $("#selClass").val();
                ktsGridId = 'ktsGrid';
                fn_Search(value);

                ktsGridAlert("삭제를 완료했습니다.");
                callGb = '';

                if (ktsGridRowCnt == 1) {
                    location.reload();
                }
            }
        }

        var fn_PopupCode = function () {
            var form = "/Oper/PortalMotCodePopup";
            var target = "PortalMotCodePopup";
            var param = {
            };

            var nWidth = 900;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupCodeReg = function (gubun, id) {
            var serviceCd = '';
            var servicecNm = '';
            var productCd = '';
            var productNm = '';
            var productDesc = '';
            var motPoint = '';
            var useYn = '';


            if (gubun === 'M') {
                var row = ktsGrid.getRow(id);
                var rowData = row.getData();
                
                serviceCd = rowData.SERVICECD;
                servicecNm = rowData.SERVICECNM;
                productCd = rowData.PRODUCTCD;
                productNm = rowData.PRODUCTNM;
                productDesc = rowData.PRODUCTDESC;
                motPoint = rowData.MOT_POINT;
                useYn = rowData.USEYN;

                if (serviceCd === null) serviceCd = '';
                if (servicecNm === null) servicecNm = '';
                if (productCd === null) productCd = '';
                if (productNm === null) productNm = '';
                if (productDesc === null) productDesc = '';
                if (motPoint === null) motPoint = '';
                if (useYn === null) useYn = '';
            }


            var form = "/Oper/PortalMotCodeMappingPopup";
            var target = "PortalMotCodeMappingPopup";

            var param = {
                pCellNo: id,
                pStatus: gubun,
                pClassNm: servicecNm,
                pClassCd: serviceCd,
                pProductNm: productNm,
                pProductCd: productCd,
                pProductDesc: productDesc,
                pMotPoint: motPoint,
                pUse: useYn
            };

            var nWidth = 720;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetDispClass = function (check, cellNo, serviceCd, servicecNm, productCd, productNm, productDesc, motPoint, UseYn) {

            if (check == 'ok' && cellNo != '') {
                var row = ktsGrid.getRow(cellNo);
                row.select();

                ktsGrid.updateData([{
                    id: cellNo, PRODUCTNM: productNm, SERVICECD: serviceCd, SERVICECNM: servicecNm, PRODUCTDESC: productDesc, MOT_POINT: motPoint, USEYN: UseYn
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

            }
            else if (check == 'ok' && cellNo == '') {
                fn_Class('selClass');
            }
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }


        var fn_ListHtml = function (json) {
            var html = '';

            if (json != undefined) {
                if (json.length > 0) {
                    for (var i = 0; i < json.length; i++) {
                        var colData = json[i]["PRODUCTNM"];

                        html += '<tr>';
                        html += '<td>' + colData + '</td>';
                        html += '</tr>';
                    }
                }
            }

            return html;
        }

        var fn_ListBind = function (getJson) {
            var html = '';

            var json = JSON.parse(getJson);
            html = fn_ListHtml(json);
            $('#productListData').html(html);
        }

        var fn_Class = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S:searchbox -->
        <div class="searchbox">
            <fieldset>
                <span class="optionbox">
                    <label>분류</label>
                    <select id="selClass">
                        <option selected="selected" value="all">전체</option>
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnNoMapping" type="button" class="btn-green last" value="미매핑상품정보 다운로드" />
                    <input id="btnCode" type="button" class="btn-green last" value="코드정보" />
                    <input id="btnCodeReg" type="button" class="btn-green last" value="코드등록" />
                </div>
            </fieldset>
        </div>
        <div id="divProductListData">
            <table id="tblProductListData">
                <thead>
                    <tr>
                        <th>상품명</th>
                    </tr>
                </thead>
                <tbody id="productListData">
                </tbody>
            </table>
        </div>
        <!-- E:searchbox -->
        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>
                    <span id="lblServiceRlt"></span></strong>
                <label class="searchrowcount"></label>
            </div>
            <!-- //E:list-top -->
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display: none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="PRODUCTCD">상품코드</option>
                            <option value="PRODUCTNM">상품명</option>
                        </select>
                        :  <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>
