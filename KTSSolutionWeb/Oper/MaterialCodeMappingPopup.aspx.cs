﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class MaterialCodeMappingPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    string pClassNm1 = Request.Form["pClassNm1"] == null ? "" : Request.Form["pClassNm1"].ToString();
                    string pClassNm2 = Request.Form["pClassNm2"] == null ? "" : Request.Form["pClassNm2"].ToString();
                    string pClassNm3 = Request.Form["pClassNm3"] == null ? "" : Request.Form["pClassNm3"].ToString();
                    string pClassCd3 = Request.Form["pClassCd3"] == null ? "" : Request.Form["pClassCd3"].ToString();
                    string pUnit = Request.Form["pUnit"] == null ? "EA" : Request.Form["pUnit"].ToString();
                    string pUnitCnt = Request.Form["pUnitCnt"] == null ? "1" : Request.Form["pUnitCnt"].ToString();

                    lblClass.Text = pClassNm1 + " > " + pClassNm2 + " > " + pClassNm3 + "(" + pClassCd3 + ")";
                    hdnCode.Value = pClassCd3;
                    hdnUnit.Value = pUnit;
                    hdnUnitCnt.Value = pUnitCnt;
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetTransaction(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "MATERIALSERVICECTGTRAN";
            string jobGb = "SET";
            string empNo = string.Empty;
            string empNm = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();
                empNm = HttpContext.Current.Session["EMPNM"].ToString();

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            aData.Add(empNo); //파라미터 추가 시
            aData.Add(empNm); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCode(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CODECOLLECTION";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetGridData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = HttpContext.Current.CurrentHandler.GetType().BaseType.Name;
            string jobGb = "GET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}