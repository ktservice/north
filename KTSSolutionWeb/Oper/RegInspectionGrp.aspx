﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegInspectionGrp.aspx.cs" Inherits="KTSSolutionWeb.RegInspectionGrp"  EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title id="title" runat="server">조치요구분류 관리</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240624" rel="stylesheet" />

    <style>
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .th-line {width: 100px;border-right: 1px solid #dfdfdf;}
        .div-Screenlock {position: absolute;top:0px;width: 100%;height: 100%;background-color: #ffffff !important;z-index: 99;opacity: 0.7;overflow: hidden;display:none}
        .div-modal {position: absolute;width: 90%; height: 95px;top: 30%;left: 5%; background: rgb(255, 255, 255); border-radius: 10px; border: 1px solid gray;z-index:100;}
        .div-modal-inner {width:90%;text-align:center;margin-left:5%;}
        .div-modal-content {margin-top:30px;padding-bottom:5px;float:left;}
        .div-modal-content > p {width: 70px; height: 30px; padding-top: 5px;float: left;font-weight:bold;}
        .div-modal-content > input[type="text"] {width:320px;height:30px;padding-left:5px;padding-right:5px;}
        .btn-add {display: inline-block;width: 40px;height: 30px;margin-top:30px;background: #fff;border: 1px solid #2CBBB7;border-radius: 2px;color: #2CBBB7;font-size: 14px;}
        .btn-close {display: inline-block;width: 40px;height: 30px;margin-top:30px;background: #fff;border: 1px solid gray;border-radius: 2px;color: gray;font-size: 14px;}

    </style>
    <script type="text/javascript">
        var callGb;
        var lvGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        $(window).bind("beforeunload", function (e) {
            opener.fn_Search();
        });

        var page = {
            init: function () {
                fn_SetControls();
            },
            eventbind: function () {

                $("#ddlLv1Grp").change(function (e) {
                    var lv1GrpCd = $("#ddlLv1Grp").val();
                    var tranGbn = '';

                    $("#btnAdd1").removeAttr("onclick");
                    $("#btnDelLv1").removeAttr("onclick");

                    if (lv1GrpCd != "") {
                        $("#ddlLv2Grp").removeAttr("disabled");
                        fn_GetLv2Grp(lv1GrpCd);
                        $("#btnModal1Open").val("수정");
                        $("#btnDelLv1").show();
                        $("#btnDelLv1").attr("onclick", "fn_DelGrpCd('" + lv1GrpCd + "', '1')");
                        tranGbn = "U";
                    }
                    else {
                        $("#btnModal1Open").val("추가");
                        $("#btnDelLv1").hide();

                        $("#ddlLv2Grp").empty();
                        $("#ddlLv2Grp").attr("disabled", "disabled");
                        $("#ddlLv3Grp").empty();
                        $("#ddlLv3Grp").attr("disabled", "disabled");

                        $("#btnModal2Open").hide();
                        $("#btnDelLv2").hide();
                        $("#btnModal3Open").hide();
                        $("#btnDelLv3").hide();
                        tranGbn = "I";
                    }

                    $("#btnAdd1").attr("onclick", "fn_Modal1Add('" + tranGbn + "')");
                });

                $("#ddlLv2Grp").change(function (e) {
                    var lv2GrpCd = $("#ddlLv2Grp").val();
                    var tranGbn = '';

                    $("#btnAdd2").removeAttr("onclick");
                    $("#btnDelLv2").removeAttr("onclick");

                    if (lv2GrpCd != "") {
                        $("#ddlLv3Grp").removeAttr("disabled");
                        fn_GetLv3Grp(lv2GrpCd);
                        $("#btnModal2Open").val("수정");
                        $("#btnDelLv2").show();
                        $("#btnDelLv2").attr("onclick", "fn_DelGrpCd('" + lv2GrpCd + "', '2')");
                        tranGbn = "U";
                    }
                    else {
                        $("#btnModal2Open").val("추가");
                        $("#btnDelLv2").hide();

                        $("#ddlLv3Grp").empty();
                        $("#ddlLv3Grp").attr("disabled", "disabled");

                        $("#btnModal3Open").hide();
                        $("#btnDelLv3").hide();
                        tranGbn = "I";
                    }

                    $("#btnAdd2").attr("onclick", "fn_Modal2Add('" + tranGbn + "')");
                });

                $("#ddlLv3Grp").change(function (e) {
                    var lv3GrpCd = $("#ddlLv3Grp").val();
                    var tranGbn = '';

                    $("#btnAdd3").removeAttr("onclick");
                    $("#btnDelLv3").removeAttr("onclick");

                    if (lv3GrpCd != "") {
                        $("#btnModal3Open").val("수정");
                        $("#btnDelLv3").show();
                        $("#btnDelLv3").attr("onclick", "fn_DelGrpCd('" + lv3GrpCd + "', '3')");
                        tranGbn = "U";
                    }
                    else {
                        $("#btnModal3Open").val("추가");
                        $("#btnDelLv3").hide();
                        tranGbn = "I";
                    }

                    $("#btnAdd3").attr("onclick", "fn_Modal3Add('" + tranGbn + "')");
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });
            } 
        };

        var fn_Load = function () {
        }

        var fn_SetControls = function () {
            fn_GetLv1Grp();
        };

        var fn_GetLv1Grp = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv1GrpList');

            callGb = 'GetLv1Grp';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetLv2Grp = function (lv1GrpCd) {
            if (lv1GrpCd != "") {

                var aData = [];

                aData[0] = lv1GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv2GrpList');

                callGb = 'GetLv2Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetLv3Grp = function (lv2GrpCd) {

            if (lv2GrpCd != "") {

                var aData = [];

                aData[0] = lv2GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv3GrpList');

                callGb = 'GetLv3Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetLv1Grp') {
                    callGb = '';
                    fn_ControlBind_Lv1Grp(json);

                    $("#ddlLv1Grp").change();

                }
                else if (callGb == 'GetLv2Grp') {
                    callGb = '';
                    fn_ControlBind_Lv2Grp(json);

                    $("#ddlLv2Grp").change();

                }
                else if (callGb == 'GetLv3Grp') {
                    callGb = '';
                    fn_ControlBind_Lv3Grp(json);

                    $("#ddlLv3Grp").change();
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'InsOk') {
                            callGb = '';
                            alert('분류 등록이 완료되었습니다.');

                            fn_ModalClose();
                        }
                        else if (callGb == 'UpdOk') {
                            callGb = '';
                            alert('분류 수정이 완료되었습니다.');

                            fn_ModalClose();
                        }
                        else if (callGb == 'DelOk') {
                            callGb = '';
                            alert('선택한 분류가 삭제 되었습니다.');

                            fn_ModalClose();
                        }

                        if (lvGb == '1') {
                            fn_GetLv1Grp();
                        }
                        else if (lvGb == '2') {
                            var lv1GrpCd = $("#ddlLv1Grp").val();

                            fn_GetLv2Grp(lv1GrpCd);
                        }
                        else if (lvGb == '3') {
                            var lv2GrpCd = $("#ddlLv2Grp").val();

                            fn_GetLv3Grp(lv2GrpCd);
                        }

                        lvGb = '';
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }

        };

        var fn_ControlBind_Lv1Grp = function (json) {
            var lv1GrpCd = $("#<%= hdfLv1Grp.ClientID %>").val();

            var selector = $("#ddlLv1Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV1GRPCD;
                options.text = item.LV1GRPNM;

                if (lv1GrpCd == item.LV1GRPCD) {
                    options.selected = true;
                }

                if (item.LV1GRPNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });

            var tranGbn = '';

            $("#btnAdd1").removeAttr("onclick");

            if (selector.val() != "") {
                $("#btnModal1Open").val("수정");
                $("#btnDelLv1").show();
                tranGbn = "U";
            } else {
                $("#btnModal1Open").val("추가");
                tranGbn = "I";
            }

            $("#btnAdd1").attr("onclick", "fn_Modal1Add('" + tranGbn + "')");
        };

        var fn_ControlBind_Lv2Grp = function (json) {
            var lv2GrpCd = $("#<%= hdfLv2Grp.ClientID %>").val();
            var selector = $("#ddlLv2Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV2GRPCD;
                options.text = item.LV2GRPNM;

                if (lv2GrpCd == item.LV2GRPCD) {
                    options.selected = true;
                }

                if (item.LV2GRPNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });

            $("#btnModal2Open").show();
            $("#btnAdd2").removeAttr("onclick");
            var tranGbn = '';

            if (selector.val() != "") {
                $("#btnModal2Open").val("수정");
                $("#btnDelLv2").show();
                tranGbn = "U";
            } else {
                $("#btnModal2Open").val("추가");
                tranGbn = "I";
            }

            $("#btnAdd2").attr("onclick", "fn_Modal2Add('" + tranGbn + "')");
        };

        var fn_ControlBind_Lv3Grp = function (json) {
            var lv3GrpCd = $("#<%= hdfLv3Grp.ClientID %>").val();
            var selector = $("#ddlLv3Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV3GRPCD;
                options.text = item.LV3GRPNM;

                if (lv3GrpCd == item.LV3GRPCD) {
                    options.selected = true;
                }

                if (item.LV3GRPNM == "") {
                    options.style.display = "none";
                }

                selector.append(options);
            });

            $("#btnModal3Open").show();
            $("#btnAdd3").removeAttr("onclick");
            var tranGbn = '';

            if (selector.val() != "") {
                $("#btnModal3Open").val("수정");
                $("#btnDelLv3").show();
                tranGbn = "U";
            } else {
                $("#btnModal3Open").val("추가");
                tranGbn = "I";
            }

            $("#btnAdd3").attr("onclick", "fn_Modal3Add('" + tranGbn + "')");
        };

        var fn_ParentSearch = function () {
            //opener.fn_GetLv1Grp();
            window.close();
        };

        var fn_DelGrpCd = function (GrpCd, Lv) {
            var ConfMsg = '';
            var LvNm = '';
            lvGb = Lv;

            if (Lv == '1') {
                ConfMsg = '선택한 대분류에 포함된 중/소분류도 모두 삭제 됩니다. 삭제 하시겠습니까?';
            } else if (Lv == '2') {
                ConfMsg = '선택한 중분류에 포함된 소분류도 모두 삭제 됩니다. 삭제 하시겠습니까?';
            } else if (Lv == '3') {
                ConfMsg = '선택한 소분류 항목을 삭제 하시겠습니까?';
            }

            if (confirm(ConfMsg)) {

                if ((GrpCd == "" || Lv == "")) {
                    alert('잘못된 접근입니다.');
                }
                else {
                    callGb = 'DelOk'

                    var aData = [];

                    aData[0] = GrpCd;
                    aData[1] = Lv;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "DelGrpCdInfo");

                    ajaxCallEx(jsonData, ajaxUrl, false);
                }
            }
            else {
                return false;
            }
        };

        var fn_RegGrpList = function (type, Lv, GrpCd, UpperGrpCd, GrpNm, preGrpNm) {
            var ConfMsg = '';
            var LvNm = '';
            lvGb = Lv;

            var sUrl = '';
            var aData = [];

            if (GrpNm == '') {
                alert('분류명을 입력해주세요.');
            }
            else {
                if (Lv == '1') {
                    LvNm = '대분류';
                    $("#<%= hdfLv1Grp.ClientID %>").val(GrpCd);
                } else if (Lv == '2') {
                    LvNm = '중분류';
                    $("#<%= hdfLv2Grp.ClientID %>").val(GrpCd);
                } else if (Lv == '3') {
                    LvNm = '소분류';
                    $("#<%= hdfLv3Grp.ClientID %>").val(GrpCd);
                }

                if (type == 'I') {
                    ConfMsg = LvNm + '에 ' + GrpNm + ' 를 신규 추가 하시겠습니까?';

                    callGb = 'InsOk';
                    sUrl = "InsGrpCdInfo";

                    aData[0] = GrpCd;
                    aData[1] = GrpNm;
                    aData[2] = UpperGrpCd;
                    aData[3] = Lv;
                } else if (type == 'U') {
                    ConfMsg = LvNm + '의 ' + preGrpNm + '을 ' + GrpNm + ' 으로 변경 하시겠습니까?';

                    callGb = 'UpdOk';
                    sUrl = "UpdGrpCdInfo";

                    aData[0] = GrpCd;
                    aData[1] = GrpNm;
                }

                if (confirm(ConfMsg)) {
                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        };

        var fn_Modal1Open = function () {
            var grpCd = $("#ddlLv1Grp option:selected").val();
            var grpNm = "";

            if (grpCd == "") {
                grpNm = "";
            }
            else {
                grpNm = $("#ddlLv1Grp option:selected").text();
            }

            $("#txbLv1GrpNm").val(grpNm);

            $("#divScreenLock").show();
            $("#divModal1").show();
        };

        var fn_Modal2Open = function () {
            var grpCd = $("#ddlLv2Grp option:selected").val();
            var grpNm = "";

            if (grpCd == "") {
                grpNm = "";
            }
            else {
                grpNm = $("#ddlLv2Grp option:selected").text();
            }

            $("#txbLv2GrpNm").val(grpNm);

            $("#divScreenLock").show();
            $("#divModal2").show();
        };

        var fn_Modal3Open = function () {
            var grpCd = $("#ddlLv3Grp option:selected").val();
            var grpNm = "";

            if (grpCd == "") {
                grpNm = "";
            }
            else {
                grpNm = $("#ddlLv3Grp option:selected").text();
            }

            $("#txbLv3GrpNm").val(grpNm);

            $("#divScreenLock").show();
            $("#divModal3").show();
        };

        var fn_ModalClose = function () {
            $("#divScreenLock").hide();
            $("#divModal1").hide();
            $("#divModal2").hide();
            $("#divModal3").hide();
        };

        var fn_Modal1Add = function (type) {
            var lv1GrpCd = '';
            var lv1GrpNm = $("#txbLv1GrpNm").val();
            var preGrpNm = '';

            if (type == 'I') {
                lv1GrpCd = $("#ddlLv1Grp option:last").val();
            }
            else if (type == 'U') {
                lv1GrpCd = $("#ddlLv1Grp option:selected").val();
                preGrpNm = $("#ddlLv1Grp option:selected").text();
            }

            fn_RegGrpList(type, '1', lv1GrpCd, '000000', lv1GrpNm, preGrpNm);
        };

        var fn_Modal2Add = function (type) {
            var lv2GrpCd = '';
            var lv2GrpNm = $("#txbLv2GrpNm").val();
            var preGrpNm = '';
            var UpperGrpCd = $("#ddlLv1Grp option:selected").val();

            if (type == 'I') {
                lv2GrpCd = $("#ddlLv2Grp option:last").val();
            }
            else if (type == 'U') {
                lv2GrpCd = $("#ddlLv2Grp option:selected").val();
                preGrpNm = $("#ddlLv2Grp option:selected").text();

                //$("#ddlLv2Grp option:selected").text(lv2GrpNm);
            }

            fn_RegGrpList(type, '2', lv2GrpCd, UpperGrpCd, lv2GrpNm, preGrpNm);
        };

        var fn_Modal3Add = function (type) {
            var lv3GrpCd = '';
            var lv3GrpNm = $("#txbLv3GrpNm").val();
            var preGrpNm = '';
            var UpperGrpCd = $("#ddlLv2Grp option:selected").val();

            if (type == 'I') {
                lv3GrpCd = $("#ddlLv3Grp option:last").val();
            }
            else if (type == 'U') {
                lv3GrpCd = $("#ddlLv3Grp option:selected").val();
                preGrpNm = $("#ddlLv3Grp option:selected").text();
            }

            fn_RegGrpList(type, '3', lv3GrpCd, UpperGrpCd, lv3GrpNm, preGrpNm);
        };
    </script>
</head>
<body style="overflow:hidden;">
    <form id="form1" runat="server" >
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>조치분류</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self);">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents" style="padding-top: 10px;">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th class="th-line">대분류</th>
                                            <td style="text-align:left;">
                                                <p class="optionbox">
                                                    <select id="ddlLv1Grp" style="width:200px;"></select>
                                                    <asp:HiddenField ID="hdfLv1Grp" runat="server" Value="" />
                                                </p>
                                            </td>
                                            <td style="border-left:none;text-align:left;">
                                                <input id="btnModal1Open" type="button" class="btn-save" value="추가" onclick="fn_Modal1Open();" />
                                                <input id="btnDelLv1" type="button" class="btn-del" style="display:none;" value="삭제" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">중분류</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <select id="ddlLv2Grp" style="width:200px;" disabled="disabled"></select>
                                                    <asp:HiddenField ID="hdfLv2Grp" runat="server" Value="" />
                                                </p>
                                            </td>
                                            <td style="border-left:none;text-align:left;">
                                                <input id="btnModal2Open" type="button" class="btn-save" value="추가" style="display:none;" onclick="fn_Modal2Open();" />
                                                <input id="btnDelLv2" type="button" class="btn-del" style="display:none;" value="삭제" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">소분류</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <select id="ddlLv3Grp" style="width:200px;" disabled="disabled"></select>
                                                    <asp:HiddenField ID="hdfLv3Grp" runat="server" Value="" />
                                                </p>
                                            </td>
                                            <td style="border-left:none;text-align:left;">
                                                <input id="btnModal3Open" type="button" class="btn-save" style="display:none;" value="추가" onclick="fn_Modal3Open();" />
                                                <input id="btnDelLv3" type="button" class="btn-del" style="display:none;" value="삭제" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <div class="btncenter">
                            <input id="btnClose" type="button" class="btn-gray" value="닫기" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <div id="divScreenLock" class="div-Screenlock"></div>
        <!-- S: divModal1 -->
		<div id="divModal1" class="div-modal" style="display:none;">
            <!-- S:popcontents -->
            <div class="div-modal-inner">
                <div class="div-modal-content">
                    <p>대분류명 : </p><input type="text" id="txbLv1GrpNm" value="" maxlength="50"/>
                </div>
                <!-- S:btncenter -->
                <div class="btncenter">
                    <input id="btnAdd1" type="button" class="btn-add" value="저장" />
                    <input id="btnClose1" type="button" class="btn-close" onclick="fn_ModalClose();" value="닫기" />
                </div>
                <!-- //E:btncenter -->
            </div>
		</div>
        <!-- E: divModal1 -->
        <!-- S: divModal2 -->
		<div id="divModal2" class="div-modal" style="display:none;">
            <!-- S:popcontents -->
            <div class="div-modal-inner">
                <div class="div-modal-content">
                    <p>중분류명 : </p><input type="text" id="txbLv2GrpNm" value="" maxlength="50"/>
                </div>
                <!-- S:btncenter -->
                <div class="btncenter">
                    <input id="btnAdd2" type="button" class="btn-add" value="저장"/>
                    <input id="btnClose2" type="button" class="btn-close" onclick="fn_ModalClose();" value="닫기" />
                </div>
                <!-- //E:btncenter -->
            </div>
		</div>
        <!-- E: divModal2 -->
        <!-- S: divModal3 -->
		<div id="divModal3" class="div-modal" style="display:none;">
            <!-- S:popcontents -->
            <div class="div-modal-inner">
                <div class="div-modal-content">
                    <p>소분류명 : </p><input type="text" id="txbLv3GrpNm" value="" maxlength="50"/>
                </div>
                <!-- S:btncenter -->
                <div class="btncenter">
                    <input id="btnAdd3" type="button" class="btn-add" value="저장" />
                    <input id="btnClose3" type="button" class="btn-close" onclick="fn_ModalClose();" value="닫기" />
                </div>
                <!-- //E:btncenter -->
            </div>
		</div>
        <!-- E: divModal3 -->
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
