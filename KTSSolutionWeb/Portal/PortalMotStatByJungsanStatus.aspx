﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PortalMotStatByJungsanStatus.aspx.cs" Inherits="KTSSolutionWeb.PortalMotStatByJungsanStatus" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        
        .tabnav li a{position:relative; display:block; color: #9a9fb2; font-size: 18px; line-height: 40px; text-decoration:none;}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_HistGridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {
                    $('.tabnav a').filter(':eq(0)').click();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "MOT정산현황.xlsx", { sheetName: "MOT정산현황" });
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    if (this.text === '이력 조회') {
                        fn_Hist();
                    }
                    else {
                        fn_Search();
                    }

                    return false;
                }).filter(':eq(0)').click();
            }
        };

        var fn_Init = function () {
            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            $(".table-search").css('display', 'none');
            $(".stMonth").show();
            $(".etMonth").show();
            $("#lblDt").show();

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen:true },
                { headerHozAlign: "center", hozAlign: "center", title: "작업월", field: "WORKING_MDATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "ORGCD4", sorter: "string", visible: false, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "ORGNM4", sorter: "string", width: 140, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자명", field: "EMPNM", sorter: "string", width: 100, frozen: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사내사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "지급총액(세전금액)", field: "PAYMENT_AMT", sorter: "number", width: 144, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT실적급(재게)", field: "MOT_AMT", sorter: "number", width: 138, formatter: fn_CellNumber },
                {
                    title: '인터넷', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "BASE_INTERNET_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "BASE_INTERNET_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: 'TV', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "BASE_TV_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "BASE_TV_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '음성', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "BASE_VOICE_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "BASE_VOICE_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '기가전환', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "VAS_GIGA_CHANGE_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "VAS_GIGA_CHANGE_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '기가WIFI', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "VAS_GIGA_WIFI_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "VAS_GIGA_WIFI_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '경품재약정', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "BONUS_POINT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "BONUS_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '기타상품', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "VAS_ETC_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "VAS_ETC_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '무선상품', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "AIR_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "AIR_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '유지수수료', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "COMMISSION_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "COMMISSION_AMT", sorter: "number", width: 100, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: 'MOT상품 외', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수", field: "MOT_OTHER_CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "MOT_OTHER_AMT", sorter: "number", width: 100, formatter: fn_CellNumber }
                    ]
                }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_HistGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "명칭", field: "UPLOADNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "UPLOADDT", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "결과", field: "RESULT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MESSAGE", sorter: "string" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsHistGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
        }

        var fn_PopupUpload = function () {
            var form = "/Portal/PortalMotJungsanUploadPopup";
            var target = "PortalMotJungsanUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: BaseMonth,
                    pType: "portal"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function () {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val();
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {
                var pDate1 = '';
                var pDate2 = '';

                //$('.tabnav a').filter(':eq(0)').click();

                if ($("#selDt").val() == 'm') {
                    pDate1 = $("#selMon").val();
                    pDate2 = $("#selMon").val();
                }
                else {
                    pDate1 = $(".stMonth").val();
                    pDate2 = $(".etMonth").val();
                }
          
                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[2] = pDate1;
                aData[3] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                fn_GridInit();

                ktsRowCountClass = 'searchrowcount';
                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_Hist = function () {
            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetHistGridData');

            fn_HistGridInit();

            ktsRowCountClass = 'histrowcount';
            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>구분</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="m">월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnUpload" type="button" class="btn-green last" value="정산일괄등록" />
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <ul class="tabnav">
                <li><a href="#tab01">MOT정산 현황</a></li>
                <li><a href="#tab02">이력 조회</a></li>
            </ul>
            <div class="tabcontent">
                <div id="tab01">
                    <div class="list-top">
                        <strong>MOT정산 현황 조회 결과</strong>
                        <label class="searchrowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField">
                                    <option value="">선택</option>
                                    <option value="WORKING_MDATE">작업월</option>
                                    <option value="ORGNM1">본부</option>
                                    <option value="ORGNM2">지사</option>
                                    <option value="ORGNM3">지점</option>
                                    <option value="ORGNM4">국사</option>
                                    <option value="EMPNM">작업자명</option>
                                </select>
                                : 
                        <input id="inputValue" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid"></div>
                    </div>
                </div>
                <div id="tab02">
                    <div class="list-top">
                        <strong>이력 조회 결과</strong>
                        <label class="histrowcount"></label>
                    </div>
                    <div class="scrollbox">
                        <div id="ktsHistGrid"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>