﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PortalCsStatByResultDetailPopup.aspx.cs" Inherits="KTSSolutionWeb.PortalCsStatByResultDetailPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>상세리스트</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var chkOrgCd = '';
        var chkOrgNm = '';
        var gubun;
        var rtn;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            fn_SetSelField();
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "작업월", field: "WORKING_MDATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자명", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사내사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                {
                    title: '전월 매출', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "개통", field: "SALES_OPEN", sorter: "number", width: 100, formatter: fn_CellNumber, visible:false },
                        { headerHozAlign: "center", hozAlign: "right", title: "AS", field: "SALES_AS", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "수작업", field: "SALES_MAN", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "4P-AGW", field: "SALES_4PAGW", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "GIGAEyes", field: "SALES_GIGAEYES", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "2인1조", field: "SALES_2PEOPLE1", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "무효화", field: "SALES_CANCEL", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "버킷", field: "SALES_BUCKET", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "신호수", field: "SALES_SINHOSU", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "파견", field: "SALES_DISPATCH", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "VAN", field: "SALES_VAN", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "공공WiFi", field: "SALES_WIFI", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "단자이설", field: "SALES_DANJA", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "단말", field: "SALES_TERMINAL", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "페어링차감", field: "SALES_PAIRING", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "CS실적급", field: "SALES_CSRESULT", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트1", field: "SALES_EVENT1", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트2", field: "SALES_EVENT2", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "계", field: "SALES_SUM", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false }
                    ]
                },
                {
                    title: '당월 건수', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "개통", field: "CNT_OPEN", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "AS", field: "CNT_AS", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "계", field: "CNT_SUM", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false }
                    ]
                },
                {
                    title: '전월 포인트', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "개통", field: "POINT_OPEN", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "AS", field: "POINT_AS", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "수작업", field: "POINT_MAN", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "4P-AGW", field: "POINT_4PAGW", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "GIGAEyes", field: "POINT_GIGAEYES", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "2인1조", field: "POINT_2PEOPLE1", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "무효화", field: "POINT_CANCEL", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "버킷", field: "POINT_BUCKET", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "신호수", field: "POINT_SINHOSU", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "파견", field: "POINT_DISPATCH", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "VAN", field: "POINT_VAN", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "공공WiFi", field: "POINT_WIFI", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "단자이설", field: "POINT_DANJA", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "페어링차감", field: "POINT_PAIRING", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트1", field: "POINT_EVENT1", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트2", field: "POINT_EVENT2", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "계", field: "POINT_SUM", sorter: "number", width: 100, formatter: fn_CellNumber, visible: false }
                    ]
                }

            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("62vh");

            fn_showColumn();
        }

        var fn_showColumn = function () {
            var gubun = $("#<%= hdnGubun.ClientID %>").val();

            if (gubun === '1') {
                ktsGrid.showColumn("SALES_OPEN");
            }
            else if (gubun === '2') {
                ktsGrid.showColumn("SALES_AS");
            }
            else if (gubun === '3') {
                ktsGrid.showColumn("SALES_MAN");
            }
            else if (gubun === '4') {
                ktsGrid.showColumn("SALES_4PAGW");
            }
            else if (gubun === '5') {
                ktsGrid.showColumn("SALES_GIGAEYES");
            }
            else if (gubun === '6') {
                ktsGrid.showColumn("SALES_2PEOPLE1");
            }
            else if (gubun === '7') {
                ktsGrid.showColumn("SALES_CANCEL");
            }
            else if (gubun === '8') {
                ktsGrid.showColumn("SALES_BUCKET");
            }
            else if (gubun === '9') {
                ktsGrid.showColumn("SALES_SINHOSU");
            }
            else if (gubun === '10') {
                ktsGrid.showColumn("SALES_DISPATCH");
            }
            else if (gubun === '11') {
                ktsGrid.showColumn("SALES_VAN");
            }
            else if (gubun === '12') {
                ktsGrid.showColumn("SALES_WIFI");
            }
            else if (gubun === '13') {
                ktsGrid.showColumn("SALES_DANJA");
            }
            else if (gubun === '14') {
                ktsGrid.showColumn("SALES_TERMINAL");
            }
            else if (gubun === '15') {
                ktsGrid.showColumn("SALES_PAIRING");
            }
            else if (gubun === '16') {
                ktsGrid.showColumn("SALES_CSRESULT");
            }
            else if (gubun === '17') {
                ktsGrid.showColumn("SALES_EVENT1");
            }
            else if (gubun === '18') {
                ktsGrid.showColumn("SALES_EVENT2");
            }
            else if (gubun === '19') {
                ktsGrid.showColumn("SALES_OPEN");
                ktsGrid.showColumn("SALES_AS");
                ktsGrid.showColumn("SALES_MAN");
                ktsGrid.showColumn("SALES_4PAGW");
                ktsGrid.showColumn("SALES_GIGAEYES");
                ktsGrid.showColumn("SALES_2PEOPLE1");
                ktsGrid.showColumn("SALES_CANCEL");
                ktsGrid.showColumn("SALES_BUCKET");
                ktsGrid.showColumn("SALES_SINHOSU");
                ktsGrid.showColumn("SALES_DISPATCH");
                ktsGrid.showColumn("SALES_VAN");
                ktsGrid.showColumn("SALES_WIFI");
                ktsGrid.showColumn("SALES_DANJA");
                ktsGrid.showColumn("SALES_TERMINAL");
                ktsGrid.showColumn("SALES_PAIRING");
                ktsGrid.showColumn("SALES_CSRESULT");
                ktsGrid.showColumn("SALES_EVENT1");
                ktsGrid.showColumn("SALES_EVENT2");
                ktsGrid.showColumn("SALES_SUM");
            }
            else if (gubun === '20') {
                ktsGrid.showColumn("CNT_OPEN");
            }
            else if (gubun === '21') {
                ktsGrid.showColumn("CNT_AS");
            }
            else if (gubun === '22') {
                ktsGrid.showColumn("CNT_OPEN");
                ktsGrid.showColumn("CNT_AS");
                ktsGrid.showColumn("CNT_SUM");
            }
            else if (gubun === '23') {
                ktsGrid.showColumn("POINT_OPEN");
            }
            else if (gubun === '24') {
                ktsGrid.showColumn("POINT_AS");
            }
            else if (gubun === '25') {
                ktsGrid.showColumn("POINT_MAN");
            }
            else if (gubun === '26') {
                ktsGrid.showColumn("POINT_4PAGW");
            }
            else if (gubun === '27') {
                ktsGrid.showColumn("POINT_GIGAEYES");
            }
            else if (gubun === '28') {
                ktsGrid.showColumn("POINT_2PEOPLE1");
            }
            else if (gubun === '29') {
                ktsGrid.showColumn("POINT_CANCEL");
            }
            else if (gubun === '30') {
                ktsGrid.showColumn("POINT_BUCKET");
            }
            else if (gubun === '31') {
                ktsGrid.showColumn("POINT_SINHOSU");
            }
            else if (gubun === '32') {
                ktsGrid.showColumn("POINT_DISPATCH");
            }
            else if (gubun === '33') {
                ktsGrid.showColumn("POINT_VAN");
            }
            else if (gubun === '34') {
                ktsGrid.showColumn("POINT_WIFI");
            }
            else if (gubun === '35') {
                ktsGrid.showColumn("POINT_DANJA");
            }
            else if (gubun === '36') {
                ktsGrid.showColumn("POINT_PAIRING");
            }
            else if (gubun === '37') {
                ktsGrid.showColumn("POINT_EVENT1");
            }
            else if (gubun === '38') {
                ktsGrid.showColumn("POINT_EVENT2");
            }
            else if (gubun === '39') {
                ktsGrid.showColumn("POINT_OPEN");
                ktsGrid.showColumn("POINT_AS");
                ktsGrid.showColumn("POINT_MAN");
                ktsGrid.showColumn("POINT_4PAGW");
                ktsGrid.showColumn("POINT_GIGAEYES");
                ktsGrid.showColumn("POINT_2PEOPLE1");
                ktsGrid.showColumn("POINT_CANCEL");
                ktsGrid.showColumn("POINT_BUCKET");
                ktsGrid.showColumn("POINT_SINHOSU");
                ktsGrid.showColumn("POINT_DISPATCH");
                ktsGrid.showColumn("POINT_VAN");
                ktsGrid.showColumn("POINT_WIFI");
                ktsGrid.showColumn("POINT_DANJA");
                ktsGrid.showColumn("POINT_PAIRING");
                ktsGrid.showColumn("POINT_EVENT1");
                ktsGrid.showColumn("POINT_EVENT2");
                ktsGrid.showColumn("POINT_SUM");
            }
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_SetSelField = function () {
            var obj1 = $('#selField');

            obj1.empty();
            obj1.append("<option value=''>선택</option>");
            obj1.append("<option value='WORKING_MDATE'>작업월</option>");
            obj1.append("<option value='ORGNM'>소속</option>");
            obj1.append("<option value='EMPNM'>이름</option>");
            obj1.append("<option value='EMPNO'>사내사번</option>");
        }

        var fn_Search = function () {
            var workingMdate = $("#hdnWorkingMdate").val();


            //입력 파라미터
            var aData = [];
            aData[0] = workingMdate;
            
            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">상세리스트</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <input type="hidden" id="hdnWorkingMdate" runat="server" />
                    <input type="hidden" id="hdnGubun" runat="server" />
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>조회 결과</strong>
                        <label class="searchrowcount"></label>
			            <div class="pull-right">
				            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				            </div>
			            </div>
                    </div>
                    <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none;">
                                <span>
                                    <select id="selField">
                                        <option value="">선택</option>
                                    </select> : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
