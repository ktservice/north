﻿using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;
using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class PortalMotResultUploadPopup : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    string pGubun = Request.Form["pGubun"] == null ? "R" : Request.Form["pGubun"].ToString();
                    hdnGubun.Value = pGubun;
                }
            }
        }

        protected void btnUpload_ServerClick(object sender, EventArgs e)
        {
            string strMsg = "";

            try
            {
                HttpFileCollection uploadFiles = Request.Files;

                string strUploadPath = ConfigurationSettings.AppSettings["PortalPath"].ToString();
                string strFailFileName = "";

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];

                    if (postedFIle.ContentLength == 0)
                        continue;

                    string strUploadType = Regex.Replace(uploadFiles.AllKeys[i].ToString(), "[^a-zA-Zㄱ-힗]+", "");
                    string strFilePath = "";
                    string strDir = "";

                    if (strUploadType.Equals("fuReg"))
                    {
                        if(hdnGubun.Value.Equals("I"))
                        {
                            strDir = "MotResultIncentive";
                        }
                        else if (hdnGubun.Value.Equals("L"))
                        {
                            strDir = "MotResultSellerLv";
                        }
                        else
                        {
                            strDir = "MotResult";
                        }
                    }

                    strFilePath = strUploadPath + "\\" + strDir;

                    DirectoryInfo di = new DirectoryInfo(strFilePath);

                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    FileInfo[] xlFiles = di.GetFiles("*.xlsx", SearchOption.TopDirectoryOnly);

                    foreach (FileInfo fi in xlFiles)
                    {
                        if (fi.Exists)
                        {
                            bool bFileRun = false;

                            using (FileUtil fileUtil = new FileUtil())
                            {
                                bFileRun = fileUtil.IsFileRun(fi.FullName);
                            }

                            if (bFileRun)
                            {
                                if (strFailFileName.Length > 0)
                                    strFailFileName += ", ";

                                strFailFileName += fi.Name;

                                continue;
                            }
                            else
                            {
                                fi.Delete();
                            }
                        }
                    }

                    if (strFailFileName.Length > 0)
                    {
                        continue;
                    }

                    postedFIle.SaveAs(strFilePath + "\\" + Path.GetFileName(postedFIle.FileName));
                }

                if (strFailFileName.Length > 0)
                {
                    strMsg = strFailFileName + " 파일은 이미 업로드 진행중이므로 업로드가 불가합니다. 잠시후 다시 시도해주세요.";
                }
                else
                {
                    strMsg = "파일 업로드가 완료되었습니다. 업로드된 데이터가 반영되기까지는 시간이 걸릴 수 있습니다.";
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "UploadOk", "alert('" + strMsg + "')", true);
            }
            catch (Exception ex)
            {
                strMsg = "파일 업로드 중 오류가 발생했습니다.";

                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "')", true);
            }

        }
    }
}