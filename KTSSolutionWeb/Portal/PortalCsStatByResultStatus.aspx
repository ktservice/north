﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PortalCsStatByResultStatus.aspx.cs" Inherits="KTSSolutionWeb.PortalCsStatByResultStatus" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();
                    }
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "CS실적현황.xlsx", { sheetName: "CS실적현황" });
                });

            }
        };

        var fn_Init = function () {
            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            $(".table-search").css('display', 'none');
            $(".stMonth").show();
            $(".etMonth").show();
            $("#lblDt").show();

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
        }

        var fn_CellPopup = function (e, cell, rptCd, gubun) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }
        

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen:true },
                { headerHozAlign: "center", hozAlign: "center", title: "작업월", field: "WORKING_MDATE", sorter: "string", width: 140, frozen: true },
                {
                    title: '전월 매출', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "개통", field: "SALES_OPEN", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '1');} },
                        { headerHozAlign: "center", hozAlign: "right", title: "AS", field: "SALES_AS", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '2'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "수작업", field: "SALES_MAN", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '3'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "4P-AGW", field: "SALES_4PAGW", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '4'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "GIGAEyes", field: "SALES_GIGAEYES", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '5'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "2인1조", field: "SALES_2PEOPLE1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '6'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "무효화", field: "SALES_CANCEL", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '7'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "버킷", field: "SALES_BUCKET", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '8'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "신호수", field: "SALES_SINHOSU", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '9'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "파견", field: "SALES_DISPATCH", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '10'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "VAN", field: "SALES_VAN", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '11'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "공공WiFi", field: "SALES_WIFI", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '12'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "단자이설", field: "SALES_DANJA", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '13'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "단말", field: "SALES_TERMINAL", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '14'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "페어링차감", field: "SALES_PAIRING", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '15'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "CS실적급", field: "SALES_CSRESULT", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '16'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트1", field: "SALES_EVENT1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '17'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트2", field: "SALES_EVENT2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '18'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "계", field: "SALES_SUM", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '19'); } }
                    ]
                },
                {
                    title: '당월 건수', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "개통", field: "CNT_OPEN", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '20'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "AS", field: "CNT_AS", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '21'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "계", field: "CNT_SUM", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '22'); } }
                    ]
                },
                {
                    title: '전월 포인트', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "개통", field: "POINT_OPEN", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '23'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "AS", field: "POINT_AS", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '24'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "수작업", field: "POINT_MAN", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '25'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "4P-AGW", field: "POINT_4PAGW", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '26'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "GIGAEyes", field: "POINT_GIGAEYES", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '27'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "2인1조", field: "POINT_2PEOPLE1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '28'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "무효화", field: "POINT_CANCEL", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '29'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "버킷", field: "POINT_BUCKET", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '30'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "신호수", field: "POINT_SINHOSU", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '31'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "파견", field: "POINT_DISPATCH", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '32'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "VAN", field: "POINT_VAN", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '33'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "공공WiFi", field: "POINT_WIFI", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '34'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "단자이설", field: "POINT_DANJA", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '35'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "페어링차감", field: "POINT_PAIRING", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '36'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트1", field: "POINT_EVENT1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '37'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "이벤트2", field: "POINT_EVENT2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '38'); } },
                        { headerHozAlign: "center", hozAlign: "right", title: "계", field: "POINT_SUM", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'CSSTATBYRESULT', '39'); } }
                    ]
                }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
        }


        var fn_PopupDetail = function (id, rptCd, gubun) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var workingMdate = rowData.WORKING_MDATE;

            var form = "/Portal/PortalCsStatByResultDetailPopup";
            var target = "PortalCsStatByResultDetailPopup";
            var param = {
                pWORKINGMDATE: workingMdate,
                pGUBUN: gubun
            };

            var nWidth = 808;
            var nHeight = 780;

            if (gubun === '4' || gubun === '8' || gubun === '12') {
                nWidth = 980;
            }

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var fn_SearchChk = function() {

            return true;
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {
                var pDate1 = '';
                var pDate2 = '';

                if ($("#selDt").val() == 'm') {
                    pDate1 = $("#selMon").val();
                    pDate2 = $("#selMon").val();
                }
                else {
                    pDate1 = $(".stMonth").val();
                    pDate2 = $(".etMonth").val();
                }
          
                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = pDate1;
                aData[2] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }




    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>구분</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="m">월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                <input id="hdnStMonth" type="hidden" />
                <input id="hdnEtMonth" type="hidden" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>CS실적현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="WORKING_MDATE">작업월</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>