﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="MobileMain.aspx.cs" Inherits="KTSSolutionWeb.MobileMain" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">      
        .btn_today_close {
            width: 100%;
            height: 45px;
            margin-top:10px;
            background-color: #333;
            text-align: center;
            vertical-align:middle;
            color: #fff;
            font-size: 14px;
            display: block;
            border:0 none;
        }
    </style>
    <script type="text/javascript"> 
        var ModalOpen = function () {
			$("#divModal").show();
        }
        var ModalClose = function () {
            $("#divModal").hide();
        }

    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<div class="page-main">
			<ul>
				<asp:PlaceHolder ID="phMenu" runat="server"></asp:PlaceHolder>
			</ul>
			<%--<button type="button" onclick="window.location.href='/Default'" title="PC버전보기" class="btn-gray">PC버전보기</button>--%>
            <!-- S: divModal -->
			<div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:340px;top:15%;background:rgb(255, 255, 255);border-radius:10px;border:1px solid gray;">
                <!-- S:popcontents -->
                <div class="popcontents" style="text-align:center;vertical-align:middle;font-size:14px;padding-top:10px;">
                   금일은 귀하의 휴일 또는 휴가일로<br/>
                    근로제공의무가 없는 날이 오니<br/>
                    업무처리를 삼가하시고 귀가하시기 바랍니다.<br/><br/>

                    만약 부서장의 근무지시가 있었다면<br/>
                    지금 즉시 복무시스템에 복무를<br/>
                    처리하여 주시기 바랍니다.<br/><br/>

                    휴일근무한 경우 휴일근무를 신청하고<br/>
                    휴가일인 경우 휴가 취소를 신청하시어<br/>
                    부서장 승인을 받으시기 바랍니다.<br/>                
                    <!-- S:btncenter -->
                    <button type="button" id="btnModalClose"  onclick="ModalClose();" title="닫기" class="btn-gray" style="margin-top:10px;">닫기</button>
                    <!-- //E:btncenter -->
                </div>
                <asp:UpdatePanel ID="UdpToday" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:button ID="btnToday" runat="server" class="btn_today_close" OnClick="btnToday_Click" Text="오늘 하루 자동 팝업을 띄우지 않습니다." />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnToday" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
                <%--<button type="button" id="btnToday" class="btn_today_close"><span>오늘 하루 자동 팝업을 띄우지 않습니다.</span></button>--%>
			</div>
			<!--E: divModal -->
		</div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>