﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ToolNoteBookMngPartner.aspx.cs" Inherits="KTSSolutionWeb.ToolNoteBookMngPartner" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabnav li a{position:relative; display:block; color: #9a9fb2; font-size: 18px; line-height: 40px; text-decoration:none;}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_EmpGridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbEnDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbEnDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#spanDt2").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#spanDt2").hide();
                        $("#selMon").show();
                    }
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#selTeam").change(function (e) {
                    $("#<%= hfOrgCd.ClientID %>").val(this.value);
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    $('.tabnav a').filter(':eq(0)').click();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "부서 노트북 관리.xlsx", { sheetName: "노트북 관리" });
                });

                $("#gridExcel2").click(function (e) {
                    ktsGrid.download("xlsx", "나의 노트북 관리.xlsx", { sheetName: "노트북 관리" });
                });

                $("#btnRegNew").click(function (e) {
                    fn_PopupReg('');
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#selRequest").change(function (e) {
                    $("#selDtType").hide();

                    if (this.value == '01') {
                        $("#divDt").hide();
                    }
                    else {
                        $("#divDt").show();

                        if ($("#selDt").val() == 'd') {
                            $("#selMon").hide();
                        }
                        else {
                            fn_Month();
                            $("#selMon").show();
                        }
                    }
                });

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    if (this.text === '나의 노트북 관리') {
                        fn_EmpSearch();
                    }
                    else {
                        fn_Search();
                    }

                    return false;
                }).filter(':eq(0)').click();

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#divDt").hide();

            $("#btnRegNew").hide();
            $("#btnUpload").hide();
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";

            return html;
        }

        var fn_GridInit = function () {

            var Columns = [
                { field: "REQUEST_STATUSCD", formatter: fn_CellReg, width: 80, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이력일", field: "HIST_DATE", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "팀코드", field: "ORGCD4", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "팀", field: "ORGNM4", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", width: 100 },
                {
                    title: '모델정보', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "S/N", field: "SN", sorter: "string", width: 200 },
                        { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 60 },
                        { headerHozAlign: "center", hozAlign: "center", title: "속도", field: "SPEED", sorter: "string", width: 80 },
                        {
                            title: 'MAC주소', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "유선", field: "MAC_CABLE", sorter: "string", width: 160 },
                                { headerHozAlign: "center", hozAlign: "center", title: "무선", field: "MAC_WIRELESS", sorter: "string", width: 160 },
                            ]
                        },
                    ]
                },
                {
                    title: '체크', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "직영구분코드", field: "COMPGBCD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "직영구분", field: "COMPGB", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "사용여부코드", field: "USECD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "사용여부", field: "USENM", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "상태코드", field: "STATUSCD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "STATUSNM", sorter: "string", width: 100 },
                        {
                            title: '보유여부', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "전원어댑터", field: "HOLD_ADAPTER", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "노트북가방", field: "HOLD_BAG", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "5G젠더", field: "HOLD_GENDER5G", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "USB젠더", field: "HOLD_GENDERUSB", sorter: "string", width: 100 },
                            ]
                        }
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string", width: 300 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "REG_DATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "CHANGE_DATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경자사번", field: "CHG_EMPNO", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경자", field: "CHG_EMPNM", sorter: "string", width: 100, visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_EmpGridInit = function () {

            var Columns = [
                { formatter: fn_CellReg, width: 80, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "팀코드", field: "ORGCD4", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "팀", field: "ORGNM4", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", width: 100 },
                {
                    title: '모델정보', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "S/N", field: "SN", sorter: "string", width: 200 },
                        { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 60 },
                        { headerHozAlign: "center", hozAlign: "center", title: "속도", field: "SPEED", sorter: "string", width: 80 },
                        {
                            title: 'MAC주소', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "유선", field: "MAC_CABLE", sorter: "string", width: 160 },
                                { headerHozAlign: "center", hozAlign: "center", title: "무선", field: "MAC_WIRELESS", sorter: "string", width: 160 },
                            ]
                        },
                    ]
                },
                {
                    title: '체크', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "직영구분코드", field: "COMPGBCD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "직영구분", field: "COMPGB", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "사용여부코드", field: "USECD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "사용여부", field: "USENM", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "상태코드", field: "STATUSCD", sorter: "string", visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "STATUSNM", sorter: "string", width: 100 },
                        {
                            title: '보유여부', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "전원어댑터", field: "HOLD_ADAPTER", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "노트북가방", field: "HOLD_BAG", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "5G젠더", field: "HOLD_GENDER5G", sorter: "string", width: 100 },
                                { headerHozAlign: "center", hozAlign: "center", title: "USB젠더", field: "HOLD_GENDERUSB", sorter: "string", width: 100 },
                            ]
                        }
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string", width: 300 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "REG_DATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "CHANGE_DATE", sorter: "string", width: 120 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsEmpGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_Team = function () {
            callGb = 'selTeam';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUserOrg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            aData[5] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Reg = function (id) {
            fn_PopupReg(id);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });

                if ($("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'y' || $("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'Y') {
                    $('#selTeam').hide();
                }
                else {
                    fn_Team();
                }
            }
            else if (callGb == 'selTeam') {
                var dataCnt = 0;
                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.OFFICECD + "'>" + item.OFFICENM + "</option>");
                    dataCnt++;
                });

                if (dataCnt > 1) {
                    $("#" + callGb + " option:eq(0)").prop("selected", true);
                    $("#<%= hfOrgCd.ClientID %>").val(obj.val());
                    $("#<%= hfOrgCdFix.ClientID %>").val(obj.val());

                    obj.show();
                    $("#<%=txbTeam.ClientID %>").hide();
                }
                else {
                    obj.hide();
                }

                callGb = '';
            }
        }

        var fn_PopupReg = function (id) {
            var row = '';
            var rowData = '';
            var sn = '';

            if (id != '') {
                row = ktsGrid.getRow(id);
                rowData = row.getData();
                sn = rowData.SN;
            }

            var form = "/Tool/ToolNoteBookMngPartnerPopup";
            var target = "ToolNoteBookMngPartnerPopup";
            var param = {
                pCompGb:'',
                pSn: sn,
                pId: id
            };

            var nWidth = 920;
            var nHeight = 944;

            if (id == '') nHeight = 904;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupUpload = function () {
            var form = "/Tool/ToolNoteBookUploadPopup";
            var target = "ToolNoteBookUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_OrgPartner = function (orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCdFix.ClientID %>").value;
            }

            var form = "/Common/OrgPartners";
            var target = "OrgPartners";
            var param = {
                pCheckType: 'R',
                pType: 'OFFICE',
                pUpperOrgCd: orgcd,
                pOfficeCd: '',
                pTeamCd: '',
                pKtsYn: ''
            };
            var nWidth = 580;
            var nHeight = 580;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }
 
        var fn_PopupOrgTree = function (orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var form = "/Common/OrgTree_Eval";
            var target = "OrgTree_Eval";
            var param = {
                pMULTICHK: 'Y',
                pCHKNODELV: '0',
                pSHOWORGLV: '3',
                pOPER: 'N'
            };
            var nWidth = 400;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetOrgPartner = function (officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(officeCd);
            $("#<%=txbTeam.ClientID %>").val(officeNm);
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var fn_SearchChk = function() {
            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val();
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            /*
            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            */

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pGubun = 'ALL';
                var pDateType = '';
                var pDate1 = '';
                var pDate2 = '';
                var pOrgCd = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");


                if ($("#selRequest").val() == '02') {
                    if ($("#selDt").val() == 'd') {
                        pDate1 = $("#<%= txbStDt.ClientID %>").val();
                        pDate2 = $("#<%= txbEnDt.ClientID %>").val();
                    }
                    else {
                        pDate1 = $("#selMon").val();
                    }
                    pGubun = 'HIST';
                    pDateType = 'ALL';
                }

                //입력 파라미터
                var aData = [];
                aData[0] = pGubun;
                aData[1] = pDateType;
                aData[2] = pOrgCd;
                aData[3] = pDate1;
                aData[4] = pDate2;
                aData[5] = $("#selRequest").val();

                /*
                aData[0] = '';
                aData[1] = $("#selDtType").val();
                aData[2] = pOrgCd;
                aData[3] = pDate1;
                aData[4] = pDate2;
                aData[5] = '01';
                */

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                fn_GridInit();

                if ($("#selRequest").val() == '01') {
                    ktsGrid.hideColumn("GUBUN");
                    ktsGrid.hideColumn("HIST_DATE");
                    ktsGrid.hideColumn("CHG_EMPNO");
                    ktsGrid.hideColumn("CHG_EMPNM");

                    ktsGrid.showColumn("REQUEST_STATUSCD");
                    ktsGrid.showColumn("REG_DATE");
                    ktsGrid.showColumn("CHANGE_DATE");
                }
                else {
                    ktsGrid.showColumn("GUBUN");
                    ktsGrid.showColumn("HIST_DATE");
                    ktsGrid.showColumn("CHG_EMPNO");
                    ktsGrid.showColumn("CHG_EMPNM");

                    ktsGrid.hideColumn("REQUEST_STATUSCD");
                    ktsGrid.hideColumn("REG_DATE");
                    ktsGrid.hideColumn("CHANGE_DATE");
                }

                ktsRowCountClass = 'searchrowcount';

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_EmpSearch = function () {
            //입력 파라미터
            var aData = [];
            aData[0] = 'EMP';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            aData[5] = '01';


            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            fn_EmpGridInit();

            ktsRowCountClass = 'emprowcount';

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function findActiveTab() {
            var tabs = document.querySelectorAll('.tabnav a'); // 탭 요소들을 선택
            for (var i = 0; i < tabs.length; i++) {
                if (tabs[i].classList.contains('active')) {
                    return tabs[i].text;
                }
            }
            return ''; // 활성 탭을 찾지 못한 경우 null 반환
        }

        var SetDispMng = function (sn, check) {

            if (check == 'ok') {
                var activeTab = findActiveTab();

                if (activeTab === '나의 노트북 관리') {
                    fn_EmpSearch();
                }
                else {
                    fn_Search();
                }
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <span class="optionbox">
                                <select id="selTeam" style="width:160px;display:none"></select>
                            </span>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCdFix" runat="server"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="optionbox">
                    <label>구분</label>
                    <select id="selRequest">
                        <option selected="selected" value="01">관리대상</option>
                        <option value="02">이력</option>
                    </select>
                </span>
                <div id="divDt" style="display: inline-block;vertical-align: bottom;">
                    <span class="optionbox">
                    <label>일자</label>
                    <select id="selDt" style="width:70px">
                        <option selected="selected" value="d">일</option>
                        <option value="m">월</option>
                    </select>
                    <select id="selDtType" style="width:120px">
                        <option selected="selected" value="ALL">전체</option>
                        <option value="REG">등록일</option>
                        <option value="GIVE">지급일</option>
                        <option value="CHANGE">변경일</option>
                    </select>
                    </span>
                    <span class="inpbox">
                        <span id="spanDt2">
                            <asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:112px !important;"></asp:TextBox>
						        <label id="lblDt" style="width:10px">~</label>
			                <asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:112px !important;"></asp:TextBox>
                        </span>
                        <span class="optionbox">
                            <select id="selMon" style="width:110px"></select>
                        </span>
                    </span> 
                </div>
                <div class="btnSearchWrap">
                    <input id="btnUpload" type="button" class="btn-green last" value="일괄등록" />
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                    <input id="btnRegNew" type="button" class="btn-green last" value="등록" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <ul class="tabnav">
                <li><a href="#tab01">부서 노트북 관리</a></li>
                <li><a href="#tab02">나의 노트북 관리</a></li>
            </ul>
            <div class="tabcontent">
                <div id="tab01">
                    <div class="list-top">
                        <strong>부서 노트북 관리</strong>
                        <label class="searchrowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField">
                                    <option value="">선택</option>
                                    <option value="GIVE_DATE">지급일자</option>
                                    <option value="ORGNM">소속</option>
                                    <option value="EMPNM">이름</option>
                                    <option value="EMPNO">사번</option>
                                    <option value="MODELNM">모델명</option>
                                    <option value="SN">S/N</option>
                                    <option value="MAKERNM">제조사</option>
                                </select>
                                : 
                        <input id="inputValue" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid"></div>
                    </div>
                </div>
                <div id="tab02">
                    <div class="list-top">
                        <strong>나의 노트북 관리</strong>
                        <label class="emprowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel2" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div id="ktsEmpGrid"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
