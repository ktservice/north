﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ToolUseMng.aspx.cs" Inherits="KTSSolutionWeb.ToolUseMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsExcelFileNm = "공기구 사용현황 관리"; //20221102 엑셀SIZE 관련 추가
                    ktsGrid.download("json", ktsExcelFileNm + ".xlsx", { sheetName: ktsExcelFileNm }); //20221102 엑셀SIZE 관련 추가
                    //ktsGrid.download("xlsx", "공기구 사용현황 관리.xlsx", { sheetName: "공기구 사용현황 관리" });
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";

            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목순번", field: "ITEMSEQ", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구매일자", field: "BUY_DATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "right", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도코드", field: "USECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도", field: "USENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태코드", field: "ITEMCD_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태", field: "ITEMNM_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태코드", field: "OPER_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태", field: "OPER_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태코드", field: "HOLD_TYPECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태", field: "HOLD_TYPENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string" },
                { formatter: fn_CellReg, width: 150, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //20221102 엑셀SIZE 관련 추가
            ktsExcelSizeYN = "Y";
            ktsExcelHeaders = { id: "No.", GIVE_DATE: "지급일자", ORGNM3: "지점", ORGNM: "소속", EMPNM: "이름", EMPNO: "사번", KTSEMPNO: "KTS사번", JOBNM: "직무", ITEMNM: "품목", ITEMSEQ: "품목순번", BUY_DATE: "구매일자", SERVICELIFE: "내용연수", USENM: "사용용도", ITEMNM_STATUSNM: "제품상태", OPER_STATUSNM: "운용상태", HOLD_TYPENM: "보유형태", MEMO: "비고" };

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값

            
        }

        var fn_Load = function () {
        }

        var fn_Reg = function (id) {
            fn_PopupReg(id);
        }

        var fn_Del = function (id) {
            if (confirm("정말로 삭제 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('D', id);
            }
        }

        var fn_Transaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var giveDate = rowData.GIVE_DATE;
            var itemCd = rowData.ITEMCD;
            var itemSeq = rowData.ITEMSEQ;
            var empno = rowData.EMPNO;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = giveDate;
            aData[2] = itemCd;
            aData[3] = itemSeq;
            aData[4] = '';
            aData[5] = '';
            aData[6] = '';
            aData[7] = '';
            aData[8] = '';
            aData[9] = '';
            aData[10] = '';
            aData[11] = '';
            aData[12] = '';
            aData[13] = '';
            aData[14] = empno;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'delTran') {
                ktsGridAlert("삭제를 완료했습니다.");
                fn_Search();

                callGb = '';
            }
        }

        var SetDispItem = function (check, cellNo, buyDate, use, useNm, status, statusNm, oper, operNm, type, typeNm, memo) {

            if (check == 'ok') {

                var row = ktsGrid.getRow(cellNo);
                row.select();

                ktsGrid.updateData([{
                    id: cellNo, BUY_DATE: buyDate, USECD: use, USENM: useNm, ITEMCD_STATUSCD: status, ITEMNM_STATUSNM: statusNm, OPER_STATUSCD: oper, OPER_STATUSNM: operNm, HOLD_TYPECD: type, HOLD_TYPENM: typeNm, MEMO: memo
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

                callGb = '';
            }
        }

        var fn_PopupReg = function (id) {
            
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vGiveDate = rowData.GIVE_DATE;
            var vBuyDate = rowData.BUY_DATE;
            var vOrgNm = rowData.ORGNM;
            var vEmpNm = rowData.EMPNM;
            var vEmpNO = rowData.EMPNO;
            var vEmpJobNm = rowData.JOBNM;
            var vItemNm = rowData.ITEMNM;
            var vItemCd = rowData.ITEMCD;
            var vItemSeq = rowData.ITEMSEQ;
            var vUse = rowData.USECD;
            var vStatus = rowData.ITEMCD_STATUSCD;
            var vOper = rowData.OPER_STATUSCD;
            var vType = rowData.HOLD_TYPECD;
            var vMemo = rowData.MEMO;

            var form = "/Tool/ToolUseMngPopup";
            var target = "ToolUseMngPopup";
            var param = {
                pIId: id,
                pOrgNm: vOrgNm,
                pEmpNm: vEmpNm,
                pEmpNo: vEmpNO,
                pEmpJobNm: vEmpJobNm,
                pGiveDate: vGiveDate,
                pBuyDate: vBuyDate,
                pItemNm: vItemNm,
                pItemCd: vItemCd,
                pItemSeq: vItemSeq,
                pUse: vUse,
                pStatus: vStatus,
                pOper: vOper,
                pType: vType,
                pMemo: vMemo
            };

            var nWidth = 720;
            var nHeight = 680;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Tool";
                var target = "OrgTree_Tool";
                var param = {
                    pORGCD: orgcd,
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {
                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>공기구 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="GIVE_DATE">지급일자</option>
                            <option value="ORGNM">소속</option>
                            <option value="EMPNM">이름</option>
                            <option value="EMPNO">사번</option>
                            <option value="KTSEMPNO">KTS사번</option>
                            <option value="ITEMNM">품목</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>

