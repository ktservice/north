﻿using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Framework.Web;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class ToolApprovalMngPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    string pGubun = Request.Form["pGubun"] == null ? "" : Request.Form["pGubun"].ToString();
                    hdnGubun.Value = pGubun;

                    string pIId = Request.Form["pIId"] == null ? "" : Request.Form["pIId"].ToString();
                    hdnId.Value = pIId;

                    string pRequestStatus = Request.Form["pRequestStatus"] == null ? "" : Request.Form["pRequestStatus"].ToString();
                    txtReq2.Value = pRequestStatus;

                    string pRequestStatusCd = Request.Form["pRequestStatusCd"] == null ? "" : Request.Form["pRequestStatusCd"].ToString();
                    hdnRequestStatusCd.Value = pRequestStatusCd;

                    string pOrgNm = Request.Form["pOrgNm"] == null ? "" : Request.Form["pOrgNm"].ToString();
                    txtOrgNm.Value = pOrgNm;

                    string pEmpNm = Request.Form["pEmpNm"] == null ? "" : Request.Form["pEmpNm"].ToString();
                    txtEmpNm.Value = pEmpNm;

                    string pEmpNo = Request.Form["pEmpNo"] == null ? "" : Request.Form["pEmpNo"].ToString();
                    hdnEmpNo.Value = pEmpNo;

                    string oEmpJobNm = Request.Form["oEmpJobNm"] == null ? "" : Request.Form["oEmpJobNm"].ToString();
                    txtEmpJobNm.Value = oEmpJobNm;

                    string pGiveDate = Request.Form["pGiveDate"] == null ? "" : Request.Form["pGiveDate"].ToString();
                    txtGiveDate.Value = pGiveDate;

                    string pItemNm = Request.Form["pItemNm"] == null ? "" : Request.Form["pItemNm"].ToString();
                    txtItemNm.Value = pItemNm;

                    string pItemCd = Request.Form["pItemCd"] == null ? "" : Request.Form["pItemCd"].ToString();
                    hdnItemCd.Value = pItemCd;

                    string pItemSeq = Request.Form["pItemSeq"] == null ? "" : Request.Form["pItemSeq"].ToString();
                    txtItemSeq.Value = pItemSeq;

                    string pChgReasonNm = Request.Form["pChgReasonNm"] == null ? "" : Request.Form["pChgReasonNm"].ToString();
                    txtChgReason.Value = pChgReasonNm;

                    string pReqGb = Request.Form["pReqGb"] == null ? "" : Request.Form["pReqGb"].ToString();
                    hdnReq.Value = pReqGb;

                    string pReqGbNm = Request.Form["pReqGbNm"] == null ? "" : Request.Form["pReqGbNm"].ToString();
                    txtReq.Value = pReqGbNm;

                    string pMemo = Request.Form["pMemo"] == null ? "" : Request.Form["pMemo"].ToString();
                    txtMemo3.Text = pMemo;

                    string pMemo1 = Request.Form["pMemo1"] == null ? "" : Request.Form["pMemo1"].ToString();
                    txtMemo1.Text = pMemo1;

                    string pMemo2 = Request.Form["pMemo2"] == null ? "" : Request.Form["pMemo2"].ToString();
                    txtMemo2.Text = pMemo2;

                    string pFileNm1 = Request.Form["pFileNm1"] == null ? "" : Request.Form["pFileNm1"].ToString();
                    aFile1.InnerText = pFileNm1.Replace(@"C:\fakepath\", "");

                    string pFileNm2 = Request.Form["pFileNm2"] == null ? "" : Request.Form["pFileNm2"].ToString();
                    aFile2.InnerText = pFileNm2.Replace(@"C:\fakepath\", "");

                    string pFileNm3 = Request.Form["pFileNm3"] == null ? "" : Request.Form["pFileNm3"].ToString();
                    aFile3.InnerText = pFileNm3.Replace(@"C:\fakepath\", "");

                    string pFileSeq1 = Request.Form["pFileSeq1"] == null ? "" : Request.Form["pFileSeq1"].ToString();
                    hdnFileSeq1.Value = pFileSeq1;

                    string pFileSeq2 = Request.Form["pFileSeq2"] == null ? "" : Request.Form["pFileSeq2"].ToString();
                    hdnFileSeq2.Value = pFileSeq2;

                    string pFileSeq3 = Request.Form["pFileSeq3"] == null ? "" : Request.Form["pFileSeq3"].ToString();
                    hdnFileSeq3.Value = pFileSeq3;

                    if (pGubun.Equals("1"))
                    {
                        btnReg.Visible = true;
                        selReq.Visible = true;
                        txtReq2.Visible = false;
                    }
                    else
                    {
                        btnReg.Visible = false;
                        selReq.Visible = false;
                        txtReq2.Visible = true;
                        txtMemo3.ReadOnly = true;
                    }
                }
            }
        }


        private void imageFileUpload(string fileId)
        {
            string result = string.Empty;

            DataSet ds = new DataSet();

            Stream stream = null;
            try
            {
                HttpPostedFile postedFIle = HttpContext.Current.Request.Files[fileId];

                stream = postedFIle.InputStream;
                string strFileNm = Path.GetFileName(postedFIle.FileName);
                string strFileType = postedFIle.ContentType;

                //type체크 필요 
                if (strFileType.Contains("image"))
                {
                    int nFileLen = Convert.ToInt32(stream.Length);

                    byte[] byteFile = new byte[nFileLen];

                    stream.Read(byteFile, 0, nFileLen);

                    ds = SetDbFile("I", -1, strFileNm, byteFile, strFileType, nFileLen);

                }

                stream.Close();
                stream.Dispose();

                if(fileId.Equals("fu1"))
                {
                    hdnFileSeq1.Value = ds.Tables[0].Rows[0]["SEQ"].ToString();
                    aFile1.InnerText = ds.Tables[0].Rows[0]["FILENM"].ToString();
                }
                else if (fileId.Equals("fu2"))
                {
                    hdnFileSeq2.Value = ds.Tables[0].Rows[0]["SEQ"].ToString();
                    aFile2.InnerText = ds.Tables[0].Rows[0]["FILENM"].ToString();
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }

            if (fileId.Equals("fu1"))
            {
                this.updPanel1.Update();
            }
            else if (fileId.Equals("fu2"))
            {
                this.updPanel2.Update();
            }
            
        }


        DataSet SetDbFile(string strGubun, int strSeq, string strFileNm, byte[] bfile, string strFileType, int nFileSize)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];
                parameters[0] = new MySqlParameter("@pGUBUN", MySqlDbType.VarChar, 1);
                parameters[0].Value = strGubun;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strSeq;
                parameters[2] = new MySqlParameter("@pFILENM", MySqlDbType.VarChar, 150);
                parameters[2].Value = strFileNm;
                parameters[3] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[3].Value = bfile;
                parameters[4] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[4].Value = strFileType;
                parameters[5] = new MySqlParameter("@pFILESIZE", MySqlDbType.VarChar, 20);
                parameters[5].Value = nFileSize;

                CommonData data = new CommonData("UP_TOOLIMAGEFILES_TRANSACTION", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }



        protected void btnFileAdd1_Click(object sender, EventArgs e)
        {
            try
            {
                imageFileUpload("fu1");
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnFileAdd2_Click(object sender, EventArgs e)
        {
            try
            {
                imageFileUpload("fu2");
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }


        protected void btnFileDownload_ServerClick(object sender, EventArgs e)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOOLIMAGEFILE";
            string jobGb = "GET";
            string errCd = string.Empty;
            string errMsg = string.Empty;
            DataSet ds = new DataSet();

            List<string> aData = new List<string>();

            aData.Add(hdnFileSeq.Value);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                (ds, errCd, errMsg) = dbTransaction.GetTableRow();
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //SELECT FILENM, FILEDATA, FILETYPE, FILESIZE 
                    int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                    byte[] bFile = new byte[nFileLen];
                    bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                    string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                    using (FileUtil fileUtil = new FileUtil())
                    {
                        fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                    }
                }

            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetTransaction(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOOLAPPROVALMNGTRAN";
            string jobGb = "SET";
            string empNo = string.Empty;
            string empNm = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCode(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CODECOLLECTION";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

    }
}