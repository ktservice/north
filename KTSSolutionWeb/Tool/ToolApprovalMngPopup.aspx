﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolApprovalMngPopup.aspx.cs" Inherits="KTSSolutionWeb.ToolApprovalMngPopup" EnableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>공기구승인</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type="text"]:read-only, textarea:read-only {
          background : #e3e3e3;
          border: solid 1px #9e9e9e;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var init;
        var req;
        var requestStatusCd;
        var callGb;
        var giveDate;
        var itemCd;
        var itemSeq;
        var reqGb;
        var reqGbNm;
        var fileNm1;
        var fileNm2;
        var memo;
        var empno;

        var reqNm;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $("#selReq").change(function (e) {
                    $('#hdnSelReq').val(this.value);

                    if (req == '02') {
                        if (this.value != 'FF') {
                            $("#selReq").val('FF').prop("selected", true);

                            ktsGridAlert("요청구분이 불용은 승인구분을 승인완료만 선택가능합니다.");
                            return false;
                        }
                    }
                    else {
                        if (this.value == 'FF' && (requestStatusCd == '01' || requestStatusCd == '03')) {
                            $("#trFile1").show();
                            $("#trFile2").show();
                        }
                        else {
                            $("#trFile1").hide();
                            $("#trFile2").show();
                        }
                    }
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });


                $("#aFile1").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq1.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile2").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq2.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile3").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq3.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#imgFile1").click(function (e) {
                    $("#<%= hdnFileSeq1.ClientID %>").val('');
                    $('#aFile1').text('');
                    $("#pFilebox1").show();
                    $("#pFileA1").hide();
                    $('#txtfu1').val('');
                });

                $("#imgFile2").click(function (e) {
                    $("#<%= hdnFileSeq2.ClientID %>").val('');
                    $('#aFile2').text('');
                    $("#pFilebox2").show();
                    $("#pFileA2").hide();
                    $('#txtfu2').val('');
                });
                
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            requestStatusCd = $("#hdnRequestStatusCd").val();
            req = $("#hdnReq").val();

            if ($('#hdnGubun').val() == '2') {
                $("#trFile1").show();
                $("#trFile2").show();
                $("#pFilebox1").hide();
                $("#pFilebox2").hide();
                $("#imgFile1").hide();
                $("#imgFile2").hide();
            }
            else {
                var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
                var vFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();

                if (requestStatusCd == '03') {
                    $("#trFile1").show();
                    $("#trFile2").show();
                    $("#pFilebox1").hide();
                    $("#pFilebox2").hide();
                    $("#imgFile1").hide();
                    $("#imgFile2").hide();
                }
                else if (req == '02') {
                    $("#trFile1").hide();
                    $("#trFile2").show();

                    if (vFileSeq1 != '') {
                        $("#pFilebox1").hide();
                        $("#imgFile1").show();
                    }
                    else {
                        $("#pFilebox1").show();
                        $("#imgFile1").hide();
                    }

                    if (vFileSeq2 != '') {
                        $("#pFilebox2").hide();
                        $("#imgFile2").hide();
                    }
                    else {
                        $("#pFilebox2").hide();
                        $("#imgFile2").hide();
                    }
                }
                else {
                    $("#trFile1").hide();
                    $("#trFile2").show();
                    $("#pFilebox1").show();
                    $("#pFilebox2").show();
                    $("#imgFile1").hide();
                    $("#imgFile2").hide();
                }
            }

            fn_Req();
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('U');
                }
            }
        }

        var fn_RegChk = function () {
            if ($("#selReq option:selected").val() == 'FF') {

                if (req != '02') {
                    if ($('#aFile1').text() == '') {

                        ktsGridAlert('인수증파일을 첨부해야 합니다.');
                        return false;
                    }
                }
            }

            return true;
        }

        var fn_Req = function () {
            callGb = 'selReq';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T10';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_FileDownload = function () {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }


        var fn_Transaction = function (gubun) {
            giveDate = $("#txtGiveDate").val();
            itemCd = $("#hdnItemCd").val();
            itemSeq = $("#txtItemSeq").val();
            reqGb = $("#selReq").val();
            reqGbNm = $("#selReq option:selected").text();
            fileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
            fileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
            memo = $("#txtMemo3").val();
            empno = $("#hdnEmpNo").val();

            reqNm = $("#txtReq").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = giveDate;
            aData[2] = itemCd;
            aData[3] = itemSeq;
            aData[4] = reqGb;
            aData[5] = reqGbNm;
            aData[6] = fileSeq1;
            aData[7] = fileSeq2;
            aData[8] = memo;
            aData[9] = empno;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;

            if (callGb == 'selReq') {
                obj1 = $('#selReq');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                var vSelReq = $('#hdnSelReq').val();
                var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
                var vFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();

                if ($('#hdnGubun').val() != '2') {

                    if (requestStatusCd == '03' || req == '02') {
                        obj1.val('FF').prop("selected", true);
                    }

                    if (vSelReq != '') {
                        obj1.val($('#hdnSelReq').val()).prop("selected", true);

                        if (vSelReq == 'FF' && (requestStatusCd == '01')) {
                            $("#trFile1").show();
                            $("#trFile2").show();
                        }
                        else {
                            
                            $("#trFile1").hide();
                            $("#trFile2").show();
                        }
                    }

                    if ($('#hdnLoaded').val() == undefined || $('#hdnLoaded').val() == '') {
                        if (vFileSeq1 != '') {
                            $("#pFilebox1").hide();
                            $("#imgFile1").show();
                        }
                        if (vFileSeq2 != '') {
                            $("#pFilebox2").hide();
                            $("#imgFile2").hide();
                        }

                        $('#hdnLoaded').val('Y');
                    }
                    else {
                        if (vFileSeq1 != '') {
                            var val = $('#txtfu1').val();

                            if (val != undefined) {
                                $("#pFilebox1").hide();
                                $("#imgFile1").show();
                            }
                        }
                        if (vFileSeq2 != '') {
                            var val = $('#txtfu2').val();

                            if (val != undefined) {
                                $("#pFilebox2").hide();
                                $("#imgFile2").hide();
                            }
                        }
                    }
                    
                }
            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');

                        setTimeout(function () {
                            SendParentsForm();
                        }, 1000);
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {

            var cellNo = $("#hdnId").val();
            var file1 = $('#aFile1').text();
            var file2 = $('#aFile2').text();
            var fileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
            var fileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();

            opener.SetDispItem(check, cellNo, reqGb, reqGbNm, memo, file1, fileSeq1, file2, fileSeq2, reqNm);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>공기구</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">소속</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                    <input type="hidden" id="hdnGubun" runat="server" />
                                                    <input type="hidden" id="hdnRequestStatusCd" runat="server" />
                                                    <input type="hidden" id="hdnReq" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">직무</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpJobNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">품목순번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemSeq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">요청구분</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtReq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">교체사유</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgReason" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(신청)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo1" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(인수)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo2" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo3" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">승인구분</th>
                                            <td colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selReq" style="width:112px;" runat="server">
                                                    </select>
                                                    <input type="hidden" id="hdnSelReq" runat="server"/>
                                                    <input type="text" id="txtReq2" tabindex="1" style="width:112px;" readonly="true" runat="server" visible="false" />
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="trFile1">
                                            <th style="width: 120px;">인수증파일</th>
                                            <td colspan="3" class="td-left">
                                                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <p id="pFilebox1" class="filebox" runat="server">
                                                        <input type="text" id="txtfu1" style="width: 236px;margin-left:0px" runat="server" />
                                                        <span class="file">
                                                            <input type="file" accept="image/*" id="fu1" name="fu1" onchange="document.getElementById('txtfu1').value=this.value;" />
                                                            <label for="fu1">찾아보기</label>
                                                        </span>
                                                        <asp:Button ID="btnFileAdd1" runat="server" class="btn-black" OnClick="btnFileAdd1_Click" Text="첨부" />
                                                    </p>
                                                </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnFileAdd1" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <p id="pFileA1" class="filebox">
                                                    <a id="aFile1" href="#" runat="server"></a>
                                                    <img id="imgFile1" src="/Resource/images/icon_minus_02.png" alt="삭제" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile2">
                                            <th style="width: 120px;">품목파일</th>
                                            <td colspan="3" class="td-left">
                                                <asp:UpdatePanel ID="updPanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <p id="pFilebox2" class="filebox" runat="server">
                                                        <input type="text" id="txtfu2" style="width: 236px;margin-left:0px" runat="server" />
                                                        <span class="file">
                                                            <input type="file" accept="image/*" id="fu2" name="fu2" onchange="document.getElementById('txtfu2').value=this.value;" />
                                                            <label for="fu2">찾아보기</label>
                                                        </span>
                                                        <asp:Button ID="btnFileAdd2" runat="server" class="btn-black" OnClick="btnFileAdd2_Click" Text="첨부" />
                                                    </p>
                                                </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnFileAdd2" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <p id="pFileA2" class="filebox">
                                                    <a id="aFile2" href="#" runat="server"></a>
                                                    <img id="imgFile2" src="/Resource/images/icon_minus_02.png" alt="삭제" />
                                                </p>
                                            </td>
                                        </tr>
                                         <tr id="trFile3">
                                            <th style="width: 120px;">첨부파일</th>
                                            <td colspan="3" class="td-left">
                                                <p id="pFileA3" class="filebox">
                                                    <a id="aFile3" href="#" runat="server"></a>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" runat="server" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                            <asp:Button id="btnFileDownload" OnClick="btnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                            <input type="hidden" id="hdnLoaded" runat="server" />
                            <input type="hidden" id="hdnFileSeq1" runat="server" />
                            <input type="hidden" id="hdnFileSeq2" runat="server" />
                            <input type="hidden" id="hdnFileSeq3" runat="server" />
                            <input type="hidden" id="hdnFileSeq" runat="server" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>