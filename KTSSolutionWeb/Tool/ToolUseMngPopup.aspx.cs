﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class ToolUseMngPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    string pIId = Request.Form["pIId"] == null ? "" : Request.Form["pIId"].ToString();
                    hdnId.Value = pIId;

                    string pOrgNm = Request.Form["pOrgNm"] == null ? "" : Request.Form["pOrgNm"].ToString();
                    txtOrgNm.Value = pOrgNm;

                    string pEmpNm = Request.Form["pEmpNm"] == null ? "" : Request.Form["pEmpNm"].ToString();
                    txtEmpNm.Value = pEmpNm;

                    string pEmpNo = Request.Form["pEmpNo"] == null ? "" : Request.Form["pEmpNo"].ToString();
                    hdnEmpNo.Value = pEmpNo;

                    string pEmpJobNm = Request.Form["pEmpJobNm"] == null ? "" : Request.Form["pEmpJobNm"].ToString();
                    txtEmpJobNm.Value = pEmpJobNm;

                    string pGiveDate = Request.Form["pGiveDate"] == null ? "" : Request.Form["pGiveDate"].ToString();
                    txtGiveDate.Value = pGiveDate;

                    string pBuyDate = Request.Form["pBuyDate"] == null ? "" : Request.Form["pBuyDate"].ToString();
                    txtBuyDate.Value = pBuyDate;

                    string pItemNm = Request.Form["pItemNm"] == null ? "" : Request.Form["pItemNm"].ToString();
                    txtItemNm.Value = pItemNm;

                    string pItemCd = Request.Form["pItemCd"] == null ? "" : Request.Form["pItemCd"].ToString();
                    hdnItemCd.Value = pItemCd;

                    string pItemSeq = Request.Form["pItemSeq"] == null ? "" : Request.Form["pItemSeq"].ToString();
                    txtItemSeq.Value = pItemSeq;

                    string pUse = Request.Form["pUse"] == null ? "" : Request.Form["pUse"].ToString();
                    hdnUse.Value = pUse;

                    string pStatus = Request.Form["pStatus"] == null ? "" : Request.Form["pStatus"].ToString();
                    hdnStatus.Value = pStatus;

                    string pOper = Request.Form["pOper"] == null ? "" : Request.Form["pOper"].ToString();
                    hdnOper.Value = pOper;

                    string pType = Request.Form["pType"] == null ? "" : Request.Form["pType"].ToString();
                    hdnType.Value = pType;

                    string pMemo = Request.Form["pMemo"] == null ? "" : Request.Form["pMemo"].ToString();
                    txtMemo.Text = pMemo;

                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetTransaction(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOOLUSEMNGTRAN";
            string jobGb = "SET";
            string empNo = string.Empty;
            string empNm = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCode(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CODECOLLECTION";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

    }
}