﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ToolStatByHolding.aspx.cs" Inherits="KTSSolutionWeb.ToolStatByHolding" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "공기구 보유 현황.xlsx", { sheetName: "공기구 보유 현황" });
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            obj = $('#selOutGb');
            obj.empty();
            obj.append("<option value='3'>지점별</option>");
            obj.append("<option value='2'>지사별</option>");
            obj.append("<option value='1'>본부별</option>");
        }

        var fn_CellPopup = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            var value = cell.getValue();
            value = priceToString(value);

            var cellReqStatusCd = cell.getRow().getData().REQUEST_STATUSCD;

            if ($("#selRequest").val() == '01') {
                if (cellReqStatusCd == '00' || cellReqStatusCd == 'FF') { //00 미신청 FF	승인완료
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='신청' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
                }
                else if (cellReqStatusCd == '01') { //01	신청
                    html = "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='취소' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
                }
            }
            else {
                html = "<input id='btnReg' type='button' onclick='fn_Hist(\"" + cellNo + "\");' value='보기' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            }

            return html;
        }

        var fn_CellStock = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '4';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellMaster = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '1';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellExfire = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '3';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellUseless = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '2';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "right", title: "보유수량", field: "ITEMCNT", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기준수량", field: "STOCK_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellStock },
                { headerHozAlign: "center", hozAlign: "right", title: "지급수량", field: "MASTER_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellMaster },
                { headerHozAlign: "center", hozAlign: "right", title: "회수수량", field: "EXFIRE_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellExfire },
                { headerHozAlign: "center", hozAlign: "right", title: "불용수량", field: "USELESS_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellUseless }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var ajaxCallBack = function (json) {
        }

        var fn_PopupDetail = function (id, rptCd, gubun) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var orgCd1 = rowData.ORGCD1;
            var orgCd2 = rowData.ORGCD2;
            var orgCd3 = rowData.ORGCD3;
            var itemCd = rowData.ITEMCD;

            if (orgCd2 == undefined) {
                orgCd2 = '';
            }
            if (orgCd3 == undefined) {
                orgCd3 = '';
            }

            var form = "/Tool/ToolStatByDetailPopup";
            var target = "ToolStatByDetailPopup";
            var param = {
                pRPTCD: rptCd,
                pGUBUN: gubun,
                pORGCD1: orgCd1,
                pORGCD2: orgCd2,
                pORGCD3: orgCd3,
                pITEMCD: itemCd
            };

            var nWidth = 780;
            var nHeight = 780;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Tool";
                var target = "OrgTree_Tool";
                var param = {
                    pORGCD: orgcd,
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var outGb = $('#selOutGb').val();

                if (outGb == '1') {
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                }
                else if (outGb == '2') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                }
                else if (outGb == '3') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                }
                
                //입력 파라미터
                var aData = [];
                aData[0] = outGb;
                aData[1] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
                
                $(".table-search").css('display', 'block');
            }
        }




    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox" style="margin-right:20px">
                    <label>출력구분</label>
                    <select id="selOutGb">
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>공기구 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                            <option value="ITEMNM">품목</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>