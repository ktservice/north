﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolNoteBookStatByDetailPopup.aspx.cs" Inherits="KTSSolutionWeb.ToolNoteBookStatByDetailPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>상세리스트</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var chkOrgCd = '';
        var chkOrgNm = '';
        var gubun;
        var rtn;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            fn_SetSelField();
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Cell = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            cell.getElement().style.backgroundColor = "#e6e6e6";

            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", width: 140, visible: true, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "직영구분코드", field: "COMPGBCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직영구분", field: "COMPGB", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", width: 100 },
                {
                    title: '모델정보', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "S/N", field: "SN", sorter: "string", width: 200 },
                        { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 60 },
                        { headerHozAlign: "center", hozAlign: "center", title: "속도", field: "SPEED", sorter: "string", width: 80 },
                        {
                            title: 'MAC주소', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "유선", field: "MAC_CABLE", sorter: "string", width: 160 },
                                { headerHozAlign: "center", hozAlign: "center", title: "무선", field: "MAC_WIRELESS", sorter: "string", width: 160 },
                            ]
                        },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "사용여부코드", field: "USECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용여부", field: "USENM", sorter: "string", width: 100, visible: false, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "상태코드", field: "STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "STATUSNM", sorter: "string", width: 100, visible: false, formatter: fn_Cell },
                {
                    title: '보유여부', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "전원어댑터", field: "HOLD_ADAPTER", sorter: "string", width: 100, visible: false, formatter: fn_Cell },
                        { headerHozAlign: "center", hozAlign: "center", title: "노트북가방", field: "HOLD_BAG", sorter: "string", width: 100, visible: false, formatter: fn_Cell },
                        { headerHozAlign: "center", hozAlign: "center", title: "5G젠더", field: "HOLD_GENDER5G", sorter: "string", width: 100, visible: false, formatter: fn_Cell },
                        { headerHozAlign: "center", hozAlign: "center", title: "USB젠더", field: "HOLD_GENDERUSB", sorter: "string", width: 100, visible: false, formatter: fn_Cell },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string", width: 300 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "REG_DATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "CHANGE_DATE", sorter: "string", width: 120 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("62vh");

            fn_showColumn();
        }

        var fn_showColumn = function () {
            var gubun = $("#<%= hdnGubun.ClientID %>").val();

            if (gubun === '1' || gubun === '2' || gubun === '3') {
                ktsGrid.showColumn("USENM");
            }
            else if (gubun === '4' || gubun === '5') {
                ktsGrid.showColumn("STATUSNM");
            }
            else if (gubun === '6' || gubun === '7') {
                ktsGrid.showColumn("HOLD_ADAPTER");
            }
            else if (gubun === '8' || gubun === '9') {
                ktsGrid.showColumn("HOLD_BAG");
            }
            else if (gubun === '10' || gubun === '11') {
                ktsGrid.showColumn("HOLD_GENDER5G");
            }
            else if (gubun === '12' || gubun === '13') {
                ktsGrid.showColumn("HOLD_GENDERUSB");
            }
          
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_SetSelField = function () {
            var obj1 = $('#selField');

            obj1.empty();
            obj1.append("<option value=''>선택</option>");
            obj1.append("<option value='WORKING_MDATE'>작업월</option>");
            obj1.append("<option value='ORGNM'>소속</option>");
            obj1.append("<option value='EMPNM'>이름</option>");
            obj1.append("<option value='EMPNO'>사내사번</option>");
        }

        var fn_Search = function () {
            var gubun = $("#hdnGubun").val();
            var orgCd = $("#hdnOrgCd").val();
            var compGbCd = $("#hdnCompGbCd").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = orgCd;
            aData[2] = compGbCd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">상세리스트</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <input type="hidden" id="hdnGubun" runat="server" />
                    <input type="hidden" id="hdnOrgCd" runat="server" />
                    <input type="hidden" id="hdnCompGbCd" runat="server" />
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>조회 결과</strong>
                        <label class="searchrowcount"></label>
			            <div class="pull-right">
				            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				            </div>
			            </div>
                    </div>
                    <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none;">
                                <span>
                                    <select id="selField">
                                        <option value="">선택</option>
                                    </select> : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
