﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ToolNoteBookStatByStatus.aspx.cs" Inherits="KTSSolutionWeb.ToolNoteBookStatByStatus" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "노트북 현황.xlsx", { sheetName: "노트북 현황" });
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            obj = $('#selOutGb');
            obj.empty();
            obj.append("<option value='3'>지점별</option>");
            obj.append("<option value='2'>지사별</option>");
            obj.append("<option value='1'>본부별</option>");
        }

        var fn_CellUse1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '1';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }
        var fn_CellUse2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '1';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }
        var fn_CellUse3 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '1';
            var code = '03';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellUse4 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '1';
            var code = '99';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellItemStatus1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '2';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellItemStatus2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '2';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellOperStatus1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '3';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellOperStatus2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '3';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellHoldType1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '4';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellHoldType2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '4';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellRepair1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '5';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellRepair2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYGIVE';
            var gubun = '5';
            var code = 'FF';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellPopup = function (e, cell, rptCd, gubun) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen:true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "팀코드", field: "ORGCD4", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "팀", field: "ORGNM4", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "직영구분코드", field: "COMPGBCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직영구분", field: "COMPGB", sorter: "string", width: 140 },
                {
                    title: '사용여부', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "사용중", field: "USECD1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '1'); } },
                        { headerHozAlign: "center", hozAlign: "center", title: "예비품", field: "USECD2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '2'); } },
                        { headerHozAlign: "center", hozAlign: "center", title: "분실", field: "USECD3", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '3'); } }
                    ]
                },
                {
                    title: '노트북상태', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "양호", field: "STATUSCD1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '4'); } },
                        { headerHozAlign: "center", hozAlign: "center", title: "불량", field: "STATUSCD2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '5'); } }
                    ]
                },
                {
                    title: '보유여부', headerHozAlign: "center",
                    columns: [
                        {
                            title: '전원어댑터', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "보유", field: "HOLD_ADAPTER1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '6'); } },
                                { headerHozAlign: "center", hozAlign: "center", title: "미보유", field: "HOLD_ADAPTER2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '7'); } }
                            ]
                        },
                        {
                            title: '노트북가방', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "보유", field: "HOLD_BAG1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '8'); } },
                                { headerHozAlign: "center", hozAlign: "center", title: "미보유", field: "HOLD_BAG2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '9'); } }
                            ]
                        },
                        {
                            title: '5G젠더', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "보유", field: "HOLD_GENDER5G1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '10'); } },
                                { headerHozAlign: "center", hozAlign: "center", title: "미보유", field: "HOLD_GENDER5G2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '11'); } }
                            ]
                        },
                        {
                            title: 'USB젠더', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "보유", field: "HOLD_GENDERUSB1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '12'); } },
                                { headerHozAlign: "center", hozAlign: "center", title: "미보유", field: "HOLD_GENDERUSB2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: (e, cell) => { fn_CellPopup(e, cell, 'NOTEBOOK', '13'); } }
                            ]
                        }
                    ]
                }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var ajaxCallBack = function (json) {
            var obj;
        }

        var fn_PopupDetail = function (id, rptCd, gubun) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var compGbCd = rowData.COMPGBCD;
            var orgCd1 = rowData.ORGCD1;
            var orgCd2 = rowData.ORGCD2;
            var orgCd3 = rowData.ORGCD3;
            var orgCd4 = rowData.ORGCD4;
            var orgCd = '';

            if (orgCd1 != null) orgCd = orgCd1;
            if (orgCd2 != null) orgCd = orgCd2;
            if (orgCd3 != null) orgCd = orgCd3;
            if (orgCd4 != null) orgCd = orgCd4;

            var form = "/Tool/ToolNoteBookStatByDetailPopup";
            var target = "ToolNoteBookStatByDetailPopup";
            var param = {
                pGUBUN: gubun,
                pORGCD: orgCd,
                pCOMPGBCD: compGbCd,
            };

            var nWidth = 780;
            var nHeight = 780;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var fn_PopupOrgTree = function (orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var form = "/Common/OrgTree_Eval";
            var target = "OrgTree_Eval";
            var param = {
                pMULTICHK: 'Y',
                pCHKNODELV: '0',
                pSHOWORGLV: '3',
                pOPER: 'N'
            };
            var nWidth = 400;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {
                var outGb = $('#selOutGb').val();

                if (outGb == '1') {
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                    ktsGrid.hideColumn("ORGNM4");
                }
                else if (outGb == '2') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                    ktsGrid.hideColumn("ORGNM4");
                }
                else if (outGb == '3') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                    ktsGrid.hideColumn("ORGNM4");
                }
                else if (outGb == '4') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                    ktsGrid.showColumn("ORGNM4");
                }

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = outGb;
                aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");


                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="optionbox" style="margin-right:20px">
                    <label>출력구분</label>
                    <select id="selOutGb">
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>노트북 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                            <option value="COMPGB">직영구분</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
