﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolRequestUserHistPopup.aspx.cs" Inherits="KTSSolutionWeb.ToolRequestUserHistPopup" EnableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>공기구이력</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type="text"]:read-only, textarea:read-only {
          background : #e3e3e3;
          border: solid 1px #9e9e9e;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var init;
        var callGb;

        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });


                $("#aFile1").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq1.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile2").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq2.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile3").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq3.ClientID %>").val());
                    fn_FileDownload();
                });
                
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            fn_Data();
        }


        var fn_Req = function () {
            callGb = 'selReq';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T10';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function () {
            callGb = 'Data';

            var gubun = $("#<%= hdnGubun.ClientID %>").val();
            var seq = $("#<%= hdnId.ClientID %>").val();
            var giveDate = $("#<%= hdnGiveDate.ClientID %>").val();
            var empNo = $("#<%= hdnEmpNo.ClientID %>").val();
            var itemCd = $("#<%= hdnItemCd.ClientID %>").val();
            var itemSeq = $("#<%= hdnItemSeq.ClientID %>").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = seq;
            aData[2] = giveDate;
            aData[3] = empNo;
            aData[4] = itemCd;
            aData[5] = itemSeq;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_FileDownload = function () {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }


        var ajaxCallBack = function (json) {

            if (callGb == 'Data') {

                $.each(JSON.parse(json), function (idx, item) {
                    $("#txtGiveDate").val(item.GIVE_DATE);
                    $("#txtOrgNm").val(item.ORGNM);
                    $("#txtEmpNm").val(item.EMPNM);
                    $("#txtEmpJobNm").val(item.JOBNM);
                    $("#txtItemNm").val(item.ITEMNM);
                    $("#txtItemSeq").val(item.ITEMSEQ);
                    $("#txtReq").val(item.REQ_GBNM);
                    $("#txtChgReason").val(item.CHG_REASONNM);
                    $("#txtChgDt").val(item.CHG_DATE);
                    $("#txtChgEmp").val(item.CHG_EMPNM);
                    $("#txtReqDt").val(item.REG_DATE);
                    $("#txtReqEmp").val(item.REG_EMPNM);
                    $("#txtAccDt").val(item.ACCEPT_DATE);
                    $("#txtAccEmp").val(item.ACCEPT_EMPNM);
                    $("#txtAppDt").val(item.APPROVAL_DATE);
                    $("#txtAppEmp").val(item.APPROVAL_EMPNM);
                    $("#txtReq2").val(item.REQ_STATUSNM);
                    $("#hdnFileSeq1").val(item.FILE_SEQ1);
                    $("#hdnFileSeq2").val(item.FILE_SEQ2);
                    $("#hdnFileSeq3").val(item.FILE_SEQ3);
                    $("#aFile1").text(item.FILENM1);
                    $("#aFile2").text(item.FILENM2);
                    $("#aFile3").text(item.FILENM3);
                    $("#txtMemo11").val(item.MEMO11);
                    $("#txtMemo1").val(item.MEMO1);
                    $("#txtMemo2").val(item.MEMO2);
                    $("#txtMemo3").val(item.MEMO3);
                });

                callGb = '';
            }
        }

        function SendParentsForm() {
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>공기구이력</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">소속</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnGubun" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                    <input type="hidden" id="hdnGiveDate" runat="server" />
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                    <input type="hidden" id="hdnItemSeq" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">직무</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpJobNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">품목순번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemSeq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">요청구분</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtReq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">교체사유</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgReason" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">교체신청일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">교체신청자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">신청일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtReqDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">신청자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtReqEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">인수일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAccDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">인수자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAccEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">승인일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAppDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">승인자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAppEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(교체신청)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo11" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(신청)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo1" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(인수)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo2" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(승인)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo3" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">승인구분</th>
                                            <td colspan="3" class="td-left">
                                                <span class="inpbox">
                                                    <input type="text" id="txtReq2" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="trFile1">
                                            <th style="width: 120px;">인수증파일</th>
                                            <td colspan="3" class="td-left">
                                                <p id="pFileA1" class="filebox">
                                                    <a id="aFile1" href="#" runat="server"></a>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile2">
                                            <th style="width: 120px;">품목파일</th>
                                            <td colspan="3" class="td-left">
                                                <p id="pFileA2" class="filebox">
                                                    <a id="aFile2" href="#" runat="server"></a>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile3">
                                            <th style="width: 120px;">첨부파일</th>
                                            <td colspan="3" class="td-left">
                                                <p id="pFileA3" class="filebox">
                                                    <a id="aFile3" href="#" runat="server"></a>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                            <asp:Button id="btnFileDownload" OnClick="btnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                            <input type="hidden" id="hdnFileSeq1" runat="server" />
                            <input type="hidden" id="hdnFileSeq2" runat="server" />
                            <input type="hidden" id="hdnFileSeq3" runat="server" />
                            <input type="hidden" id="hdnFileSeq" runat="server" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>