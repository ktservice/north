﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolNoteBookMngPopup.aspx.cs" Inherits="KTSSolutionWeb.ToolNoteBookMngPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>노트북정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/lengthCheck.js"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    

    <style type="text/css">
        input[type="button"], input[type="submit"] {
            cursor: pointer;
        }

        .ui-datepicker {
            height: 260px;
        }

        label{
            color: #f00;
        }

        .inpbox textarea {
            display: inline-block;
            height: 100%;
            width: 94%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 18px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }

        .td-button {
            width: 20px !important;
            padding:0px !important;
        }

        .td-left {
            text-align: left !important;
        }

        .td-width1 {
            width: 180px !important;
        }

        .td-width2 {
            width: 240px !important;
        }

        .td-width3 {
            width: 150px !important;
        }

        .td-width4 {
            width: 220px !important;
        }

        .td-width5 {
            width: 180px !important;
            text-align: center;
            letter-spacing: 0.5px;
        }

        .td-widthAI {
            width: 18.4px !important;
            padding: 0 !important;
            border-left: 0px solid !important;
            border-right: 0px solid !important;
            text-align: center;
        }

        .td-widthAS {
            width: 34px !important;
            padding: 0px 0px 0px 14px !important;
            border-right: 0px solid !important;
            text-align: center;
        }

        .td-widthAE {
            width: 41px !important;
            padding: 0px 21px 0px 0px !important;
            border-left: 0px solid !important;
            text-align: center;
        }
        
        .td-widthA {
            width: 6px !important;
            padding: 0px !important;
            border-left: 0px solid !important;
            border-right: 0px solid !important;
            text-align: center;
        }

        .text-number {
            text-align: right !important;
        }

        .tabulator-alert {
            position: absolute;
            display: flex;
            align-items: center;
            top: 0;
            left: 0;
            z-index: 100;
            height: 100%;
            width: 100%;
            background: rgba(0, 0, 0, 0.4);
            text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
            display: inline-block;
            margin: 0 auto;
            padding: 10px 20px;
            border-radius: 10px;
            background: #fff;
            font-weight: bold;
            font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
            border: 4px solid #333;
            color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#selCompGb").change(function (e) {

                    $("#txtOrgNm").val('');
                    $("#hdnOrgCd").val('');
                    

                    if (this.value == '01') {
                        $("#btnOrgNm").attr("onclick", "return fn_PopupOrg()");
                    }
                    else {
                        $("#btnOrgNm").attr("onclick", "return fn_PopupPartner()");
                    }

                });

                $("#txtGiveDate").datepicker({ dateFormat: 'yy-mm-dd' });

                $("#txtSn").keypress(function (e) {
                    return fn_Keypress2(e);
                });
                
                $("#txtMacCable1").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacCable1").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacCable1").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacCable2").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacCable2").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacCable2").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacCable3").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacCable3").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacCable3").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacCable4").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacCable4").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacCable4").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacCable5").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacCable5").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacCable5").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacCable6").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacCable6").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacCable6").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacWireless1").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacWireless1").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacWireless1").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacWireless2").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacWireless2").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacWireless2").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacWireless3").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacWireless3").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacWireless3").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacWireless4").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacWireless4").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacWireless4").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacWireless5").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacWireless5").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacWireless5").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#txtMacWireless6").keypress(function (e) {
                    return fn_Keypress(e);
                });

                $("#txtMacWireless6").blur(function (e) {
                    fn_Blur(this, e);
                });

                $("#txtMacWireless6").keyup(function (e) {
                    return fn_Keyup(this, e);
                });

                $("#btnEmpNm").click(function (e) {
                    fn_PopupUser();
                });


                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnDel").click(function (e) {
                    fn_DelTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm('', '');
                });
            }
        };

        function fn_Blur(obj, e) {
            var id = obj.id;
            var newVal = obj.value.toUpperCase().replace(/[^a-zA-Z-_0-9]/g, '');
            obj.value = newVal;
            $("#" + id.replace('txt', 'hdn') + "").val($("#" + id + "").val());
        }

        function fn_Keypress(e) {
            //영어문자, 0부터9, 숫자모음(0~9), 백스페이스
            if ((e.keyCode >= 65 && e.keyCode <= 70) || (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 97 && e.keyCode <= 102) || (e.keyCode == 8)) {
                return true;
            }
            else {
                return false;
            }
        }
        function fn_Keypress2(e) {
            //영어문자, 0부터9, 숫자모음(0~9), 백스페이스, 기호(-) 키
            if ((e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 97 && e.keyCode <= 122) || (e.keyCode == 8) || (e.keyCode == 45)) {
                return true;
            }
            else {
                return false;
            }
        }

        function fn_Keyup(obj, e) {
            var mac1 = 'txtMacCable';
            var mac2 = 'txtMacWireless';
            var mac = '';
            var id = obj.id;
            var idx = 0;
            var newVal = obj.value.toUpperCase().replace(/[^a-zA-Z-_0-9]/g,'');
            var oldVal = $("#" + id.replace('txt', 'hdn') + "").val();

            if (!((e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode == 8))) {
                return false;
            }

            obj.value = newVal;

            if (obj.value.length == 2) {

                if (newVal == oldVal) {
                    return false;
                }

                $("#" + id.replace('txt', 'hdn') + "").val($("#" + id + "").val());

                if (id.indexOf(mac1) >= 0) {
                    mac = mac1;
                }

                if (id.indexOf(mac2) >= 0) {
                    mac = mac2;
                }

                if (mac != '') {
                    idx = id.replace(mac, '');
                    idx++;
                    id = mac + idx;

                    if (idx > 0 && idx < 7) {
                        $("#" + id + "").focus();
                    }
                }
            }
        }

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';

            fn_CtrlInit();
            fn_SelInit();
        }

        var fn_CtrlInit = function () {
            /*
            if ($("#hdnCompGb").val().length != 0) {
                if ($("#hdnCompGb").val() == 'PARTNER') {

                }
                else {

                }
            }
            else {
                
            }
            */

            $("#txtGiveDate").attr('readonly', true);

            $("#txtOrgNm").attr('readonly', true);
            $("#txtOrgNm").attr('style', 'background: #d1d1d1 !important');
            
            if ($("#hdnSn").val().length != 0) {
                $("#txtMacCable").attr('readonly', true);
                $("#txtMacWireless").attr('readonly', true);

                $("#txtMacCable").attr('style', 'background: #d1d1d1 !important');
                $("#txtMacWireless").attr('style', 'background: #d1d1d1 !important');

                $("#txtSn").attr('readonly', true);
                $("#txtSn").attr('style', 'background: #d1d1d1 !important');

                $("#divMacCableDisp").show();
                $("#divWirelessDisp").show();

                $("#btnReg").hide();
                $("#btnMod").show();
                $("#btnDel").show();
            }
            else {
                $("#divMacCableDisp").hide();
                $("#divWirelessDisp").hide();

                $("#btnReg").show();
                $("#btnMod").hide();
                $("#btnDel").hide();
            }

        }

        var fn_SelInit = function () {
            var obj1 = $('#selHoldAdapter');
            obj1.empty();

            var obj2 = $('#selHoldBag');
            obj2.empty();

            var obj3 = $('#selHoldGender5g');
            obj3.empty();

            var obj4 = $('#selHoldGenderusb');
            obj4.empty();

            obj1.append("<option value='O'>O</option>");
            obj1.append("<option value='X'>X</option>");
            obj2.append("<option value='O'>O</option>");
            obj2.append("<option value='X'>X</option>");
            obj3.append("<option value='O'>O</option>");
            obj3.append("<option value='X'>X</option>");
            obj4.append("<option value='O'>O</option>");
            obj4.append("<option value='X'>X</option>");
        }

        var fn_Load = function () {
            fn_CompGb();
        }

        var fn_PopupPartner = function () {
            var form = "/Common/OrgPartners";
            var target = "OrgPartners";
            var param = {
                pCheckType: 'R',
                pType: 'OFFICE',
                pOfficeCd: '',
                pTeamCd: '',
                pKtsYn: ''
            };
            var nWidth = 580;
            var nHeight = 580;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrg = function () {
            var form = "/Common/SearchOrgCd";
            var target = "SearchOrgCd";
            var param = {
                pORGLV: "F"
            };
            var nWidth = 600;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupUser = function () {
            var form = "/Common/SearchUser";
            var target = "SearchUser";
            var param = {
            };
            var nWidth = 700;
            var nHeight = 500;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }
        

        var fn_DelTran = function () {
            if (confirm("정보가 삭제 됩니다.\n계속 진행하시겠습니까?")) {
                callGb = 'DelTran';

                fn_Transaction('D');
            }
        }

        var fn_ModTran = function () {
            if (fn_RegChk()) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_AddressText = function (str1, str2, str3, str4, str5, str6) {
            var val = '';

            if (str1 != '') val = str1;
            if (str2 != '') val += '-' + str2;
            if (str3 != '') val += '-' + str3;
            if (str4 != '') val += '-' + str4;
            if (str5 != '') val += '-' + str5;
            if (str6 != '') val += '-' + str6;

            return val.toUpperCase();
        }

        function fn_MacAddressCheck(value) {
            if (value == null) {
                value = '';
            }

            if (value.length == 17) {
                var macformat = /^([0-9A-F]{2}[:-]?){5}([0-9A-F]{2})$/;
                if (value.match(macformat)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        var fn_RegChk = function () {
            var pGiveDate = $("#txtGiveDate").val();
            var pOrgCd = $("#hdnOrgCd").val();
            var pCompGb = $("#selCompGb").val();
            var pEmpno = $("#txtEmpno").val();
            var pKtsEmpno = $("#txtKtsEmpno").val();
            var pIdmsEmpno = $("#txtIdmsEmpno").val();
            var pEmpNm = $("#txtEmpNm").val();
            var pUse = $("#selUse").val();
            var pSn = $("#txtSn").val();
            var pMacCable = '';
            var pMacWireless = '';
            var pMakerNm = $("#selMaker").val();
            var pModelNm = $("#txtModelNm").val();
            var pColor = $("#txtColor").val();
            var pSpeed = $("#txtSpeed").val();
            var pHoldAdapter = $("#selHoldAdapter").val();
            var pHoldBag = $("#selHoldBag").val();
            var pHoldGender5g = $("#selHoldGender5g").val();
            var pHoldGenderusb = $("#selHoldGenderusb").val();
            var pStatus = $("#selStatus").val();
            var pMemo = $("#txtMemo").val();

            pMacCable = fn_AddressText($("#txtMacCable1").val(), $("#txtMacCable2").val(), $("#txtMacCable3").val(), $("#txtMacCable4").val(), $("#txtMacCable5").val(), $("#txtMacCable6").val());
            pMacWireless = fn_AddressText($("#txtMacWireless1").val(), $("#txtMacWireless2").val(), $("#txtMacWireless3").val(), $("#txtMacWireless4").val(), $("#txtMacWireless5").val(), $("#txtMacWireless6").val());

            if (pSn.length == 0) {
                ktsGridAlert('S/N 값을 입력하세요.');
                return false;
            }

            if (pGiveDate.length == 0) {
                ktsGridAlert('지급일을 입력하세요.');
                return false;
            }

            if (pOrgCd.length == 0) {
                ktsGridAlert('조직값을 입력하세요.');
                return false;
            }

            if (pMacCable.length > 0) {
                if (!fn_MacAddressCheck(pMacCable)) {
                    ktsGridAlert('MAC주소(유선)값이 잘못되어있습니다.');
                    return false;
                }
            }

            if (pMacWireless.length > 0) {
                if (!fn_MacAddressCheck(pMacWireless)) {
                    ktsGridAlert('MAC주소(무선)값이 잘못되어있습니다.');
                    return false;
                }
            }

            if (!getStringLengthCheck(pEmpno, 20)) {
                ktsGridAlert('사번값이 초과됐습니다.');
                return false;
            }
            if (!getStringLengthCheck(pKtsEmpno, 20)) {
                ktsGridAlert('KTS사번값이 초과됐습니다.');
                return false;
            }
            if (!getStringLengthCheck(pIdmsEmpno, 20)) {
                ktsGridAlert('IDMS사번값이 초과됐습니다.');
                return false;
            }
            if (!getStringLengthCheck(pEmpNm, 20)) {
                ktsGridAlert('성명값이 초과됐습니다.');
                return false;
            }

            if (!getStringLengthCheck(pSn, 50)) {
                ktsGridAlert('S/N값이 초과됐습니다.');
                return false;
            }

            if (!getStringLengthCheck(pMacCable, 30)) {
                ktsGridAlert('MAC주소(유선) 입력값이 초과됐습니다.');
                return false;
            }
            if (!getStringLengthCheck(pMacWireless, 30)) {
                ktsGridAlert('MAC주소(무선) 입력값이 초과됐습니다.');
                return false;
            }

            if (!getStringLengthCheck(pModelNm, 20)) {
                ktsGridAlert('모델명 입력값이 초과됐습니다.');
                return false;
            }

            if (!getStringLengthCheck(pColor, 10)) {
                ktsGridAlert('색상 입력값이 초과됐습니다.');
                return false;
            }

            if (!getStringLengthCheck(pMemo, 200)) {
                ktsGridAlert('비고 입력값이 초과됐습니다.');
                return false;
            }

            return true;
        }

        var fn_CompGb = function () {
            callGb = 'selCompGb';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T21';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Use = function () {
            callGb = 'selUse';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T22';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Maker = function () {
            callGb = 'selMaker';

            //입력 파라미터
            var aData = [];
            aData[0] = 'MAKER';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetMaker');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }
        
        var fn_Status = function () {
            callGb = 'selStatus';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T23';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }
        

        var fn_Emp = function (gubun, emp) {
            callGb = 'Emp';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = emp;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetEmp');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function (sn) {
            callGb = 'Data';

            //입력 파라미터
            var aData = [];
            aData[0] = sn;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun) {
            var pGiveDate = $("#txtGiveDate").val();
            var pOrgCd = $("#hdnOrgCd").val();
            var pCompGb = $("#selCompGb").val();
            var pEmpno = $("#txtEmpno").val();
            var pKtsEmpno = $("#txtKtsEmpno").val();
            var pIdmsEmpno = $("#txtIdmsEmpno").val();
            var pEmpNm = $("#txtEmpNm").val();
            var pUse = $("#selUse").val();
            var pSn = $("#txtSn").val();
            var pMacCable = '';
            var pMacWireless = '';
            var pMakerNm = $("#selMaker").val();
            var pModelNm = $("#txtModelNm").val();
            var pColor = $("#txtColor").val();
            var pSpeed = $("#txtSpeed").val();
            var pHoldAdapter = $("#selHoldAdapter").val();
            var pHoldBag = $("#selHoldBag").val();
            var pHoldGender5g = $("#selHoldGender5g").val();
            var pHoldGenderusb = $("#selHoldGenderusb").val();
            var pStatus = $("#selStatus").val();
            var pMemo = $("#txtMemo").val();

            pMacCable = fn_AddressText($("#txtMacCable1").val(), $("#txtMacCable2").val(), $("#txtMacCable3").val(), $("#txtMacCable4").val(), $("#txtMacCable5").val(), $("#txtMacCable6").val());
            pMacWireless = fn_AddressText($("#txtMacWireless1").val(), $("#txtMacWireless2").val(), $("#txtMacWireless3").val(), $("#txtMacWireless4").val(), $("#txtMacWireless5").val(), $("#txtMacWireless6").val());

            if (pMacCable == '') pMacCable = $("#txtMacCable").val();
            if (pMacWireless == '') pMacWireless = $("#txtMacWireless").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pGiveDate;
            aData[2] = pOrgCd;
            aData[3] = pCompGb;
            aData[4] = pEmpno;
            aData[5] = pKtsEmpno;
            aData[6] = pIdmsEmpno;
            aData[7] = pEmpNm;
            aData[8] = pUse;
            aData[9] = pSn;
            aData[10] = pMacCable;
            aData[11] = pMacWireless;
            aData[12] = pMakerNm;
            aData[13] = pModelNm;
            aData[14] = pColor;
            aData[15] = pSpeed;
            aData[16] = pHoldAdapter;
            aData[17] = pHoldBag;
            aData[18] = pHoldGender5g;
            aData[19] = pHoldGenderusb;
            aData[20] = pStatus;
            aData[21] = pMemo;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;

            if (callGb == 'selCompGb') {
                obj1 = $('#selCompGb');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });
                
                callGb = '';

                fn_Use();
            }
            else if (callGb == 'selUse') {
                obj1 = $('#selUse');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                callGb = '';

                fn_Maker();
            }
            else if (callGb == 'selMaker') {
                obj1 = $('#selMaker');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });

                callGb = '';

                fn_Status();
            }
            else if (callGb == 'selStatus') {
                obj1 = $('#selStatus');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                callGb = '';
                
                if ($("#hdnSn").val().length != 0) {
                    $("#txtSn").val($("#hdnSn").val());
                    fn_Data($("#hdnSn").val());
                }
            }
            else if (callGb == 'Emp') {
                $.each(JSON.parse(json), function (idx, item) {
                    $("#txtKtsEmpno").val(item.KTSEMPNO);
                    $("#txtIdmsEmpno").val(item.IDMSEMPNO);

                });

                callGb = '';
            }
            else if (callGb == 'Data') {

                var vModelNm = '';
                var vMakerNm = '';
                var vColor = '';
                var vSpeed = '';
                var vGiveDate = '';
                var vMacCable = '';
                var vMacWireless = '';
                var vOrgNm = '';
                var vOrgCd = '';
                var vEmpno = '';
                var vEmpNm = '';
                var vKtsEmpno = '';
                var vIdmsEmpno = '';
                var vMemo = '';
                var vCompGb = '';
                var vUse = '';
                var vStatus = '';
                var vHoldAdapter = '';
                var vHoldBag = '';
                var vHoldGender5g = '';
                var vHoldGenderusb = '';
                
                $.each(JSON.parse(json), function (idx, item) {
                    vModelNm = item.MODELNM;
                    vMakerNm = item.MAKERNM;
                    vColor = item.COLOR;
                    vSpeed = item.SPEED;
                    vGiveDate = item.GIVE_DATE;
                    vMacCable = item.MAC_CABLE;
                    vMacWireless = item.MAC_WIRELESS;
                    vOrgNm = item.ORGNM;
                    vOrgCd = item.ORGCD;
                    vEmpno = item.EMPNO;
                    vEmpNm = item.EMPNM;
                    vKtsEmpno = item.KTSEMPNO;
                    vIdmsEmpno = item.IDMSEMPNO;
                    vMemo = item.MEMO;
                    vCompGb = item.COMPGBCD;
                    vUse = item.USECD;
                    vStatus = item.STATUSCD;
                    vHoldAdapter = item.HOLD_ADAPTER;
                    vHoldBag = item.HOLD_BAG;
                    vHoldGender5g = item.HOLD_GENDER5G;
                    vHoldGenderusb = item.HOLD_GENDERUSB;
                });

                $("#txtModelNm").val(vModelNm);
                $("#txtColor").val(vColor);
                $("#txtSpeed").val(vSpeed);
                $("#txtGiveDate").val(vGiveDate);
                $("#txtMacCable").val(vMacCable);
                $("#txtMacWireless").val(vMacWireless);

                if (fn_MacAddressCheck(vMacCable)) {
                    var arrMacCable = vMacCable.split('-');

                    $("#txtMacCable1").val(arrMacCable[0]);
                    $("#txtMacCable2").val(arrMacCable[1]);
                    $("#txtMacCable3").val(arrMacCable[2]);
                    $("#txtMacCable4").val(arrMacCable[3]);
                    $("#txtMacCable5").val(arrMacCable[4]);
                    $("#txtMacCable6").val(arrMacCable[5]);

                    $("#hdnMacCable1").val(arrMacCable[0]);
                    $("#hdnMacCable2").val(arrMacCable[1]);
                    $("#hdnMacCable3").val(arrMacCable[2]);
                    $("#hdnMacCable4").val(arrMacCable[3]);
                    $("#hdnMacCable5").val(arrMacCable[4]);
                    $("#hdnMacCable6").val(arrMacCable[5]);

                    $("#divMacCableDisp").hide();
                }
                else {
                    if (vMacCable == '') {
                        $("#divMacCableDisp").hide();
                    }
                    else {
                        $("#divMacCableDisp").show();
                    }
                }

                if (fn_MacAddressCheck(vMacWireless)) {
                    var arrMacWireless = vMacWireless.split('-');

                    $("#txtMacWireless1").val(arrMacWireless[0]);
                    $("#txtMacWireless2").val(arrMacWireless[1]);
                    $("#txtMacWireless3").val(arrMacWireless[2]);
                    $("#txtMacWireless4").val(arrMacWireless[3]);
                    $("#txtMacWireless5").val(arrMacWireless[4]);
                    $("#txtMacWireless6").val(arrMacWireless[5]);

                    $("#hdnMacWireless1").val(arrMacWireless[0]);
                    $("#hdnMacWireless2").val(arrMacWireless[1]);
                    $("#hdnMacWireless3").val(arrMacWireless[2]);
                    $("#hdnMacWireless4").val(arrMacWireless[3]);
                    $("#hdnMacWireless5").val(arrMacWireless[4]);
                    $("#hdnMacWireless6").val(arrMacWireless[5]);

                    $("#divWirelessDisp").hide();
                }
                else {
                    if (vMacWireless == '') {
                        $("#divWirelessDisp").hide();
                    }
                    else {
                        $("#divWirelessDisp").show();
                    }
                }
                

                $("#txtOrgNm").val(vOrgNm);
                $("#hdnOrgCd").val(vOrgCd);
                $("#txtEmpno").val(vEmpno);
                $("#txtEmpNm").val(vEmpNm);
                $("#txtKtsEmpno").val(vKtsEmpno);
                $("#txtIdmsEmpno").val(vIdmsEmpno);
                $("#txtMemo").val(vMemo);

                if (vMakerNm == null) vMakerNm = '';
                if (vCompGb == null) vCompGb = '';
                if (vUse == null) vUse = '';
                if (vStatus == null) vStatus = '';
                if (vHoldAdapter == null) vHoldAdapter = '';
                if (vHoldBag == null) vHoldBag = '';
                if (vHoldGender5g == null) vHoldGender5g = '';
                if (vHoldGenderusb == null) vHoldGenderusb = '';

                $("#selMaker").val(vMakerNm).prop("selected", true);
                $("#selCompGb").val(vCompGb).prop("selected", true);
                $("#selUse").val(vUse).prop("selected", true);
                $("#selStatus").val(vStatus).prop("selected", true);
                $("#selHoldAdapter").val(vHoldAdapter).prop("selected", true);
                $("#selHoldBag").val(vHoldBag).prop("selected", true);
                $("#selHoldGender5g").val(vHoldGender5g).prop("selected", true);
                $("#selHoldGenderusb").val(vHoldGenderusb).prop("selected", true);

                if ($("#selCompGb").val() == '01') {
                    $("#btnOrgNm").attr("onclick", "return fn_PopupOrg()");
                }
                else {
                    $("#btnOrgNm").attr("onclick", "return fn_PopupPartner()");
                }

                callGb = '';
            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        check = 'ok';
                        ktsGridAlert('등록되었습니다.');
                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
                
                callGb = '';
            }
            else if (callGb == 'ModTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        check = 'ok';
                        ktsGridAlert('변경되었습니다.');
                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });

                callGb = '';
            }
            else if (callGb == 'DelTran') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        check = 'ok';
                        ktsGridAlert('삭제되었습니다.');
                    }
                    else if (item.ERRCD == '01') {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
                
                callGb = '';
            }
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            if (empno != '') {
                $("#txtEmpno").val(empno);
                $("#txtEmpNm").val(empnm);

                fn_Emp('NO', empno);
            }
            else {
                $("#txtEmpno").val('');
                $("#txtEmpNm").val('');
                $("#txtKtsEmpno").val('');
                $("#txtIdmsEmpno").val('');
            }
        }

        function SetDispOrgCd(orgcd, orgnm) {
            $("#txtOrgNm").val(orgnm);
            $("#hdnOrgCd").val(orgcd);
        }

        function SetOrgPartner(officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm) {
            $("#txtOrgNm").val(officeNm);
            $("#hdnOrgCd").val(officeCd);
        }

        var ktsGridAlert = function (msg, timeout) {
            var time = 1300;
            if (timeout != undefined) time = timeout;
            $("#" + ktsGridId).append('<div class="tabulator-alert"><div class="tabulator-alert-msg tabulator-alert-state-msg" role="alert">' + msg + '</div></div>');

            setTimeout(function () {
                $(".tabulator-alert").remove();
                if (check == 'ok') {
                    var id = $("#hdnId").val();
                    SendParentsForm(id, check);
                }
                
            }, time);
        }

        function SendParentsForm(id, check) {
            opener.SetDispMng(id, check);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>노트북 등록</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">S/N<label>*</label></th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtSn" maxlength="50" tabindex="1" class="td-width2" placeholder="S/N 입력" />
                                                    <input type="hidden" id="hdnSn" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                    <input type="hidden" id="hdnCompGb" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">지급일자<label>*</label></th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" maxlength="10" class="td-width1" tabindex="1" placeholder="지급일자 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">모델명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtModelNm" maxlength="20" tabindex="1" class="td-width1" placeholder="모델명 입력" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">제조사</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selMaker" class="td-width1">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">색상</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtColor" maxlength="10" tabindex="1" class="td-width1" placeholder="색상 입력" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">속도</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtSpeed" maxlength="10" tabindex="1" class="td-width1" placeholder="속도 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">MAC주소(유선)</th>
                                            <td class="td-left">
                                                <div id="divMacCableDisp" >
                                                    <p class="inpbox">
                                                        <input type="text" id="txtMacCable" maxlength="30" tabindex="1" class="td-width5"/>
                                                    </p>
                                                    <br />
                                                </div>
                                                <p class="inpbox">
                                                    <input type="text" id="txtMacCable1" maxlength="2" tabindex="1" class="td-widthAS"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacCable2" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacCable3" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacCable4" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacCable5" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacCable6" maxlength="2" tabindex="1" class="td-widthAE"/>
                                                    <input type="hidden" id="hdnMacCable1" />
                                                    <input type="hidden" id="hdnMacCable2" />
                                                    <input type="hidden" id="hdnMacCable3" />
                                                    <input type="hidden" id="hdnMacCable4" />
                                                    <input type="hidden" id="hdnMacCable5" />
                                                    <input type="hidden" id="hdnMacCable6" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">MAC주소(무선)</th>
                                            <td class="td-left">
                                                <div id="divWirelessDisp" >
                                                    <p class="inpbox">
                                                        <input type="text" id="txtMacWireless" maxlength="30" tabindex="1" class="td-width5"/>
                                                    </p>
                                                    <br />
                                                </div>
                                                <p class="inpbox">
                                                    <input type="text" id="txtMacWireless1" maxlength="2" tabindex="1" class="td-widthAS"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacWireless2" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacWireless3" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacWireless4" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacWireless5" maxlength="2" tabindex="1" class="td-widthAI"/>
                                                    <input type="text" value="-" class="td-widthA" readonly="readonly"/>
                                                    <input type="text" id="txtMacWireless6" maxlength="2" tabindex="1" class="td-widthAE"/>
                                                    <input type="hidden" id="hdnMacWireless1" />
                                                    <input type="hidden" id="hdnMacWireless2" />
                                                    <input type="hidden" id="hdnMacWireless3" />
                                                    <input type="hidden" id="hdnMacWireless4" />
                                                    <input type="hidden" id="hdnMacWireless5" />
                                                    <input type="hidden" id="hdnMacWireless6" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">직영구분</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selCompGb" class="td-width1">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">조직<label>*</label></th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" maxlength="50" tabindex="1" class="td-width1" placeholder="조직명 입력" />
                                                    <input type="hidden" id="hdnOrgCd" runat="server" />
                                                    <input type="button" id="btnOrgNm" class="td-button" value=".." onclick="return fn_PopupOrg()"/>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">성명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" maxlength="50" tabindex="1" class="td-width3" placeholder="성명 입력" />
                                                    <input type="button" id="btnEmpNm" class="td-button" value=".."/>
                                                </p>
                                            </td>
                                            <th style="width: 120px;">사번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpno" maxlength="20" tabindex="1" class="td-width3" placeholder="사번 입력" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">KTS사번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtKtsEmpno" maxlength="20" tabindex="1" class="td-width3" placeholder="KTS사번 입력" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">IDMS사번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtIdmsEmpno" maxlength="20" tabindex="1" class="td-width3" placeholder="IDMS사번 입력" onkeypress="return OnlyNumber();" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">사용여부</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUse">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">상태</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selStatus">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                         <tr>
                                            <th style="width: 120px;">전원어댑터보유</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selHoldAdapter">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">노트북가방보유</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selHoldBag">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">5G젠더보유</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selHoldGender5g">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">USB젠더보유</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selHoldGenderusb">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <textarea id="txtMemo" rows="7" maxlength="200" placeholder="비고 입력" ></textarea>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                                <input id="btnDel" type="button" class="btn-del" value="삭제" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>