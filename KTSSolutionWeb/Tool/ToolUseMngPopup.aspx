﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolUseMngPopup.aspx.cs" Inherits="KTSSolutionWeb.ToolUseMngPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>공기구관리</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type="text"]:read-only, textarea:read-only {
          background : #e3e3e3;
          border: solid 1px #9e9e9e;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var buyDate;
        var use;
        var useNm;
        var status;
        var statusNm;
        var oper;
        var operNm;
        var type;
        var typeNm;
        var memo;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#txtBuyDate").datepicker({ dateFormat: 'yy-mm-dd' });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            fn_Use();
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("변경하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegChk = function () {
            return true;
        }

        var fn_Use = function () {
            callGb = 'selUse';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T01';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Status = function () {
            callGb = 'selStatus';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T02';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Oper = function () {
            callGb = 'selOper';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T03';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Type = function () {
            callGb = 'selType';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'T04';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun, productCd) {
            var giveDate = $("#txtGiveDate").val();
            var itemCd = $("#hdnItemCd").val();
            var itemSeq = $("#txtItemSeq").val();
            var empno = $("#hdnEmpNo").val();

            buyDate = $("#txtBuyDate").val();
            use = $("#selUse").val();
            useNm = $("#selUse option:selected").text();
            status = $("#selStatus").val();
            statusNm = $("#selStatus option:selected").text();
            oper = $("#selOper").val();
            operNm = $("#selOper option:selected").text();
            type = $("#selType").val();
            typeNm = $("#selType option:selected").text();
            memo = $("#txtMemo").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = giveDate;
            aData[2] = itemCd;
            aData[3] = itemSeq;
            aData[4] = buyDate;
            aData[5] = use;
            aData[6] = useNm;
            aData[7] = status;
            aData[8] = statusNm;
            aData[9] = oper;
            aData[10] = operNm;
            aData[11] = type;
            aData[12] = typeNm;
            aData[13] = memo;
            aData[14] = empno;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;
            var obj3;
            var obj4;

            if (callGb == 'selUse') {
                obj1 = $('#selUse');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                obj1.val($('#hdnUse').val()).prop("selected", true);

                fn_Status();
            }
            else if (callGb == 'selStatus') {
                obj2 = $('#selStatus');
                obj2.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj2.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                obj2.val($('#hdnStatus').val()).prop("selected", true);

                fn_Oper();
            }
            else if (callGb == 'selOper') {
                obj3 = $('#selOper');
                obj3.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj3.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                obj3.val($('#hdnOper').val()).prop("selected", true);

                fn_Type();
            }
            else if (callGb == 'selType') {
                obj4 = $('#selType');
                obj4.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj4.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                obj4.val($('#hdnType').val()).prop("selected", true);

                callGb = '';
            }

            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('변경되었습니다.');

                        setTimeout(function () {
                            SendParentsForm();
                        }, 1000);
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {

            var cellNo = $("#hdnId").val();

            opener.SetDispItem(check, cellNo, buyDate, use, useNm, status, statusNm, oper, operNm, type, typeNm, memo);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>공기구</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">소속</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                    <input type="hidden" id="hdnUse" runat="server" />
                                                    <input type="hidden" id="hdnStatus" runat="server" />
                                                    <input type="hidden" id="hdnOper" runat="server" />
                                                    <input type="hidden" id="hdnType" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">직무</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpJobNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">구매일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtBuyDate" tabindex="1" style="width:112px;background-color:#ffffff !important" runat="server" class="date" readonly="true" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">품목순번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemSeq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">사용용도</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUse" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">제품상태</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selStatus" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">운용상태</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selOper" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">보유상태</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selType" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:476px" TextMode="MultiLine"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="변경" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>