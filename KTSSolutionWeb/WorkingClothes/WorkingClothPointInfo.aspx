﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkingClothPointInfo.aspx.cs" Inherits="KTSSolutionWeb.WorkingClothPointInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%--<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>--%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", sorter: "string", width: 450 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "연도", field: "YEARS", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "총포인트", field: "TOTALPOINT", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "사용포인트", field: "USEPOINT", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "잔여포인트", field: "RESTPOINT", sorter: "string", width: 150 }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                ValueEl.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');
                    sField = 'id';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $("#<%= ddlYears.ClientID %>").change(function (e) {
                fn_SetTeam();
            });

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "피복포인트관리.xlsx", { sheetName: "피복포인트관리" });
            });

            $(".table-search").css('display', 'none');
        });

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                var org = json.split('|');

                if (org.length > 1) {
                    $("#<%= hdfOrgCd.ClientID %>").val(org[0]);
                    $("#<%=txbTeam.ClientID %>").val(org[1]);
                }
            }
            callGb = '';
        }

        function fn_SetTeam() {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();

            if (Years.length == 0) {
                $("#<%= txbTeam.ClientID %>").val('');
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= hdfMonth.ClientID %>").val('');                
            } else {
                var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
                var sumupmonth;
                var nowDate = new Date();

                var nowYear = nowDate.getFullYear();
                var nowMonth = ("0" + (nowDate.getMonth() + 1)).slice(-2);

                if (nowYear < Years) {
                    sumupmonth = Years + "12";
                }
                else {
                    sumupmonth = nowYear + nowMonth;
                }

                $("#<%= hdfMonth.ClientID %>").val(sumupmonth);

                var aData = [];
                aData[0] = OrgCd;
                aData[1] = sumupmonth;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
                callGb = 'GetTeam';

                ajaxCall(jsonData, ajaxUrl);
            }
        }

        function fn_Search() {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();

            if (Years.length == 0) {
                alert("연도를 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                //입력 파라미터
                var aData = [];
                aData[0] = OrgCd.replace(/,/gi, "^");
                aData[1] = Years;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        function PopupOrgTree(orgcd, empno) {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            if (years.length == 0) {
                alert("연도를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = $("#<%= hdfOrgCd.ClientID %>").val();
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: sumupmonth,
                    pType: "cloth"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SetOrgCd(orgcd, valtype) {
            this.focus();

            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            var aData = [];
            aData[0] = orgcd;
            aData[1] = sumupmonth;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
                <span class="optionbox" style="margin-right:20px">
                    <label>연도</label>
                    <asp:DropDownList ID="ddlYears" runat="server" Width="150px" AutoPostBack="false"></asp:DropDownList>
                    <asp:HiddenField ID="hdfOrgCd" runat="server" />
                    <asp:HiddenField ID="hdfMonth" runat="server" />
			    </span>
				<span class="inpbox">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
					<button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>피복포인트 관리조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="ORGFULLNM">소속</option>
                            <option value="EMPNO">사번</option>
                            <option value="KTSEMPNO">KTS사번</option>
                            <option value="EMPNM">성명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->            
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
