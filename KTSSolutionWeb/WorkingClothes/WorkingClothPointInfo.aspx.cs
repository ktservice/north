﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.WorkingClothes;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using KTS.KTSSolution.BSL.Oper;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class WorkingClothPointInfo : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();

                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");

                //updPanelSearch.Update();

                //GetDataList();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }

                //updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();
            DataSet dsOrg = new DataSet();

            string result = string.Empty;

            string strOrgCd = "";
            string strMonth = aData[1];

            using (OrgCodeTree org = new OrgCodeTree())
            {
                DateTime chkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime month = new DateTime(int.Parse(strMonth.Substring(0, 4)), int.Parse(strMonth.Substring(4, 2)), 1);

                if (chkDate > month)
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }
                else
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (aData[0].Length == 0)
                {
                    using (KTSUser user = new KTSUser())
                    {
                        dsOrg = user.GetUserViewOrgMonth(HttpContext.Current.Session["EMPNO"].ToString(), strMonth);
                    }

                    for (int i = 0; i < dsOrg.Tables[0].Rows.Count; i++)
                    {
                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += dsOrg.Tables[0].Rows[i]["ORGCD"].ToString();
                    }
                }
                else
                {
                    strOrgCd = aData[0];
                }
            }

            result = Utility.GetOrgNm(strOrgCd, ds.Tables[0]);

            return strOrgCd + "|" +result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;

            DataSet ds = new DataSet();

            string strOrgCd = aData[0];
            string strYears = aData[1];

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, "", "", aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetWorkingClothPointInfo(strOrgCd, strYears);
                }

                result = Utility.DataTableToJson(ds.Tables[0]);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}