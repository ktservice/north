﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserRequestClothInfo.aspx.cs" Inherits="KTSSolutionWeb.UserRequestClothInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlYears.ClientID %>").change(function (e) {
                    fn_GetPointSeq();
                });

                $("#btnSelect").click(function (e) {

                    var years = $("#<%= ddlYears.ClientID %> option:selected").val();
                    var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

                    if (years.length == 0 || pointseq.length == 0) {
                        alert("연도와 지급기준을 선택해주세요.");
                    } else {
                        fn_Search();
                    }
                });
            }
        };

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", width: 60, headerSort: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", width: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", width: 80, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지급기준", field: "TITLE", width: 200, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청구분", field: "PROVIDETYPE", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청상태", field: "REQSTATUS", width: 140, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "작업복구분", field: "CLOTHGBN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "종류", field: "CLOTHTYPE", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사이즈", field: "CLOTHSIZE", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "수량", field: "REQCNT", width: 80, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청일자", field: "REGDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사용포인트", field: "USEPOINT", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "YEARS", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "POINTSEQ", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "REQTYPE", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "REQYN", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            var rowDoubleClick = function (e, row) {
                var rowData = row.getData();
                var strReqYn = rowData.REQYN;

                if (strReqYn == "Y") {
                    var strYears = rowData.YEARS;
                    var strPointSeq = rowData.POINTSEQ;
                    var strEmpNo = rowData.EMPNO;
                    var strReqType = rowData.REQTYPE;

                    RegRequestClothPopup(strYears, strPointSeq, strEmpNo, strReqType);
                }
            };

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값
            //grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            //ktsGrid.setHeight("30vh");

        }

        var fn_Load = function () {
            //fn_Search();
        }

        var successCallBack = function (json) {
            if (callGb == 'getPointSeq') {
                var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");
                $.each(JSON.parse(json), function (idx, item) {
                    ddlPointSeq.append("<option value='" + item.POINTSEQ + "'>" + item.TITLE + "</option>");
                });
            }
            else if (callGb == 'setPassAll') {
                alert("일괄미신청 요청이 완료되었습니다.");
                fn_Search();
            }
            callGb = '';
        }

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                $.each(JSON.parse(json), function (key, value) {
                    if (key == 'Table') {
                        ktsGrid.setData(value);
                        ktsGridRowCount(ktsGrid.getDataCount());
                        ktsGridSearch();
                    }
                    else {                        
                        var poptype = value[0].POPUPTYPE;
                        var ReqType = value[0].REQTYPE;
                        var JobNm = value[0].JOBNM;
                        var PassYn = value[0].PASSYN;

                        if (poptype == "Y") {
                            var Years = value[0].YEARS;
                            var PointSeq = value[0].POINTSEQ;
                            var EmpNo = value[0].EMPNO;

                            $("#btnRequest").attr('onclick', '').unbind('click');

                            $("#btnRequest").click(function (e) {
                                RegRequestClothPopup(Years, PointSeq, EmpNo, ReqType);
                            });

                            $("#btnRequest").show();
                        }
                        else if (poptype == "A") {

                            $("#btnRequest").click(function (e) {
                                alert('피복 신청대상자가 아닙니다.');
                            });
                            $("#btnRequest").show();
                        }
                        else {
                            $("#btnRequest").hide();
                        }

                        if (PassYn == "Y") {
                            if (poptype != "A") {
                                $("#btnPassAll").show();

                                $("#btnPassAll").attr('onclick', '').unbind('click');

                                $("#btnPassAll").click(function (e) {
                                    fn_SetPassAll(Years, PointSeq, EmpNo, ReqType);
                                });
                            }
                        }
                        else {
                            $("#btnPassAll").hide();
                        }

                        if (ReqType != "N" && JobNm == "kte") {
                            ktsGrid.showColumn("USEPOINT");
                        }
                        else {
                            ktsGrid.hideColumn("USEPOINT");
                        }
                    }
                });

            }
        }

        var fn_GetPointSeq = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");

            ddlPointSeq.html("");
            ddlPointSeq.append("<option value=''>선택하세요</option>");

            if (Years != "") {
                //입력 파라미터
                var aData = [];
                aData[0] = Years;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetPointSeq');

                callGb = 'getPointSeq';

                ajaxCall(jsonData, ajaxUrl);
            }
        }

        var fn_Search = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length == 0 || PointSeq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {
                //입력 파라미터
                var aData = [];
                aData[0] = Years;
                aData[1] = PointSeq;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
        }

        function RegRequestClothPopup(strYears, strPointSeq, strEmpNo, strReqType) {
            var nWidth = 1000;
            var nHeight = 900;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                YEARS: strYears,
                POINTSEQ: strPointSeq,
                EMPNO: strEmpNo,
                REQTYPE: strReqType,
                SUBREQ: "N"
            };

            var Popupform = createForm("/WorkingClothes/RegRequestCloth", param);

            Popupform.target = "RegRequestCloth";
            var win = window.open("", "RegRequestCloth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SearchChk() {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length > 0 && PointSeq.length) {
                return true;
            } else {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            }
        }

        var fn_SetPassAll = function (Years, PointSeq, EmpNo, ReqType) {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length == 0 || PointSeq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {
                var PointSeqTxt = $("#<%= ddlPointSeq.ClientID %> option:selected").text();
                var confMsg = Years + "년도 " + PointSeqTxt + "을 일괄미신청 하시겠습니까?";

                if (confirm(confMsg)) {
                    //입력 파라미터
                    var aData = [];
                    aData[0] = Years;
                    aData[1] = PointSeq;
                    aData[2] = EmpNo;
                    aData[3] = ReqType;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'SetPassAll');

                    callGb = 'setPassAll';

                    //grid Bind
                    ajaxCall(jsonData, ajaxUrl);
                }
                else {
                    return false;
                }
            }
        }

        function ReSearch() {
            fn_Search();
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
                <span class="optionbox">
                    <label>연도</label>
                    <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="false" Width="150px"></asp:DropDownList>
			    </span> 
                <span class="optionbox" style="margin-left:10px;">
                    <label>지급기준</label>
                    <asp:DropDownList ID="ddlPointSeq" runat="server" AutoPostBack="false" Width="250px"></asp:DropDownList>
			    </span> 
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>조직별 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="btnPassAll" type="button" class="btn-gray" style="display:none;" value="일괄미신청" />
                        <input id="btnRequest" type="button" class="btn-green" style="display:none;" value="신청" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
		    <!-- S:scrollbox -->
		    <div class="scrollbox">  
                <div id="ktsGrid"></div>
            </div>      
		    <!-- E:scrollbox -->
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
