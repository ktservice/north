﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Oper;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class OperStandardClothStats : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetStandardInfo(string strYears, string strPointSeq)
        {
            DataSet ds = new DataSet();
            try
            {

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetStandardInfo(strYears, strPointSeq);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetRequestStatsList(string strYears, string strPointSeq)
        {
            DataSet ds = new DataSet();
            try
            {
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                /*
                string strYears = this.ddlYears.SelectedValue;
                string strPointSeq = this.ddlPointSeq.SelectedValue;

                if (strYears.Length == 0 || strPointSeq.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    GetStandardInfo(strYears, strPointSeq);
                    GetRequestStatsList(strYears, strPointSeq);
                }

                */
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPointSeq(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;

            using (PointMgmt point = new PointMgmt())
            {
                ds = point.GetProvidePointInfo(aData[0]);
            }

            result = Utility.DataTableToJson(ds.Tables[0]);

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string fn_GetStandard(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;

            DataSet ds = new DataSet();

            string strYears = aData[0];
            string strPointSeq = aData[1];

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, "", "", aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetStandardInfo(strYears, strPointSeq);
                }

                result = Utility.DataTableToJson(ds.Tables[0]);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetRequsetStats(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;

            DataSet ds = new DataSet();

            string strYears = aData[0];
            string strPointSeq = aData[1];

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, "", "", aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetRequestStatsList(strYears, strPointSeq);
                }

                result = Utility.DataTableToJson(ds.Tables[0]);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}