﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OperRequestConfList.aspx.cs" Inherits="KTSSolutionWeb.OperRequestConfList" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit(null);
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlYears.ClientID %>").change(function (e) {
                    fn_GetPointSeq();
                });

                $("#<%= ddlPointSeq.ClientID %>").change(function (e) {
                    fn_GetTeam();
                });

                $("#btnSelect").click(function (e) {
                    var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
                    var years = $("#<%= ddlYears.ClientID %> option:selected").val();
                    var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

                    if (years.length == 0 || pointseq.length == 0) {
                        alert("연도와 지급기준을 선택해주세요.");
                    } else if (OrgCd.length == 0) {
                        alert("조직을 선택해주세요.");
                    } else {
                        fn_GridSetting();
                        //fn_Search();
                    }
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "피복지급 결제용.xlsx", { sheetName: "피복지급상세" });
                });
            }
        };
        
        var fn_GridSetting = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length > 0 && PointSeq.length > 0) {
                //입력 파라미터
                var aData = [];
                aData[0] = Years;
                aData[1] = PointSeq.split("||")[0];

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'getHeaderInfo');

                callGb = 'getHeaderInfo';

                ajaxCall(jsonData, ajaxUrl);
            }
        };

        var fn_GridInit = function (json) {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "순번", field: "id", width: 60, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV1NM", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV2NM", width: 150, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV3NM", width: 150, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "팀", field: "ORGLV4NM", width: 150, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "KTSEMPNO", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "성별", field: "SEX", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "미신청", field: "NOREQ", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "직책", field: "LEVELNM", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지급기준", field: "JOBNM2", width: 120, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "신규여부", field: "PROVIDETYPE", width: 100, sorter: "string" }
            ];

            if (json != null) {
                var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
                var pointdata = pointseq.split("||");

                if (pointdata.length == 3) {
                    var clothgbn = pointdata[2];

                    var col1 = [];
                    var col2 = [];

                    $.each(JSON.parse(json), function (idx, item) {
                        if (clothgbn == '동계') {
                            if (item.TITLENM == '외피' || item.TITLENM == '내피') {
                                col1.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "사이즈", field: item.FIELDNM + "_SIZE", width: 120, sorter: "number" });
                                col1.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "수량", field: item.FIELDNM + "_CNT", width: 100, sorter: "number" });
                            }
                            else if (item.TITLENM == '방한바지') {
                                col1.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "사이즈", field: item.FIELDNM + "_SIZE", width: 120, sorter: "number" });
                                col1.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "기장", field: item.FIELDNM + "_LEN", width: 120, sorter: "string" });
                                col1.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "수량", field: item.FIELDNM + "_CNT", width: 120, sorter: "number" });
                            }
                            else if (item.TITLENM == '버프') {
                                col1.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "수량", field: item.FIELDNM + "_CNT", width: 100, sorter: "number" });
                            }
                        }

                        if (item.TITLENM == '점퍼' || item.TITLENM == '조끼' || item.TITLENM == '긴팔T' || item.TITLENM == '반팔T' || item.TITLENM == '티셔츠') {
                            col2.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "사이즈", field: item.FIELDNM + "_SIZE", width: 120, sorter: "number" });
                            col2.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "수량", field: item.FIELDNM + "_CNT", width: 100, sorter: "number" });
                        }
                        else if (item.TITLENM == '바지') {
                            col2.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "사이즈", field: item.FIELDNM + "_SIZE", width: 120, sorter: "number" });
                            col2.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "기장", field: item.FIELDNM + "_LEN", width: 100, sorter: "string" });
                            col2.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "수량", field: item.FIELDNM + "_CNT", width: 100, sorter: "number" });
                        }
                        else if (item.TITLENM == '모자') {
                            col2.push({ headerHozAlign: "center", hozAlign: "center", title: item.TITLENM + "수량", field: item.FIELDNM + "_CNT", width: 100, sorter: "number" });
                        }
                    });

                    if (clothgbn == '동계') {
                        Columns.push({ headerHozAlign: "center", hozAlign: "center", title: '방한복', columns: col1 });
                    }

                    Columns.push({ headerHozAlign: "center", hozAlign: "center", title: '작업복', columns: col2 });
                }
            }

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            //fn_Search();
        }

        var ajaxCallBack = function (json) {
            if (callGb != "") {
                if (callGb == "getOk") {
                    ktsGrid.setData(json);
                }
                else {
                    $.each(JSON.parse(json), function (idx, item) {                       
                        if (item.SERVICE_ERROR_CD == '00') {
                            if (callGb == 'RegOk') {
                                ktsGridAlert('추가신청 승인을 완료했습니다.', 2000);

                                fn_Search();
                            }
                        }
                        else {
                            ktsGridAlert(item.ERRMSG);
                        }
                    });
                }

                callGb = '';
            }
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                var org = json.split('|');

                if (org.length > 1) {
                    $("#<%= hdfOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);
                }
            }
            else if (callGb == 'getPointSeq') {
                var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");
                $.each(JSON.parse(json), function (idx, item) {
                    ddlPointSeq.append("<option value='" + item.POINTSEQ + "||" + item.STDT + "||" + item.CLOTHGBN + "'>" + item.TITLE + "</option>");
                });
            }
            else if (callGb == 'getHeaderInfo') {
                fn_GridInit(json);
                fn_Search();
            }
            callGb = '';
        }

        var fn_GetPointSeq = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");

            ddlPointSeq.html("");
            ddlPointSeq.append("<option value=''>선택하세요</option>");

            if (Years != "") {
                //입력 파라미터
                var aData = [];
                aData[0] = Years;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetPointSeq');

                callGb = 'getPointSeq';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
                $("#<%= hdfMonth.ClientID %>").val('');
            }
        }

        var fn_GetTeam = function () {
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (pointseq != '') {
                var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
                var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
                var pointdata = pointseq.split("||");

                if (pointdata.length == 3) {
                    var sumupmonth = pointdata[1].substring(0, 6);

                    $("#<%= hdfMonth.ClientID %>").val(sumupmonth);

                    var aData = [];
                    aData[0] = OrgCd;
                    aData[1] = sumupmonth;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                    callGb = 'GetTeam';

                    ajaxCall(jsonData, ajaxUrl);
                }
                else {
                    alert("잘못된 접근입니다.");
                }
            }
            else {
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
                $("#<%= hdfMonth.ClientID %>").val('');
            }
        };

        var fn_Search = function () {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length == 0 || PointSeq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {
                //입력 파라미터
                var aData = [];
                aData[0] = OrgCd.replace(/,/gi, "^");
                aData[1] = Years;
                aData[2] = PointSeq.split("||")[0];

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
        };

        function PopupOrgTree(orgcd, empno) {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            if (years.length == 0 || pointseq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: sumupmonth,
                    pType: "cloth"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SetOrgCd(orgcd, valtype) {
            this.focus();

            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            var aData = [];
            aData[0] = orgcd;
            aData[1] = sumupmonth;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul id="ulPage" runat="server">
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
                <span class="optionbox first">
                    <label>연도</label>
                    <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="false" Width="150px"></asp:DropDownList>
			    </span> 
                <span class="optionbox">
                    <label>지급기준</label>
                    <asp:DropDownList ID="ddlPointSeq" runat="server" Width="250px" AutoPostBack="false">
                        <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdfMonth" runat="server" />
                    <asp:HiddenField ID="hdfOrgCd" runat="server" />
			    </span> 
			    <span class="inpbox">
				    <label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
				    <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
			    </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div id="ktsGrid"></div>
            </div>
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
