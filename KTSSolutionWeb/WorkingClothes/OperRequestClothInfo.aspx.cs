﻿using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.WorkingClothes;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class OperRequestClothInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();

                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPointSeq(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = null;

                string strYears = aData[0];

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointInfo(strYears);
                }

                if (ds != null)
                {
                    result = Utility.DataTableToJson(ds.Tables[0]);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();
            DataSet dsOrg = new DataSet();

            string result = string.Empty;

            string strOrgCd = "";
            string strMonth = aData[1];

            using (OrgCodeTree org = new OrgCodeTree())
            {
                DateTime chkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime month = new DateTime(int.Parse(strMonth.Substring(0, 4)), int.Parse(strMonth.Substring(4, 2)), 1);

                if (chkDate > month)
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }
                else
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (aData[0].Length == 0)
                {
                    using (KTSUser user = new KTSUser())
                    {
                        dsOrg = user.GetUserViewOrgMonth(HttpContext.Current.Session["EMPNO"].ToString(), strMonth);
                    }

                    for (int i = 0; i < dsOrg.Tables[0].Rows.Count; i++)
                    {
                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += dsOrg.Tables[0].Rows[i]["ORGCD"].ToString();
                    }
                }
                else
                {
                    strOrgCd = aData[0];
                }
            }

            result = Utility.GetOrgNm(strOrgCd, ds.Tables[0]);

            return strOrgCd + "|" + result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = null;

                string strOrgCd = aData[0];
                string strEmpNo = aData[1];
                string strEmpNm = aData[2];
                string strYears = aData[3];
                string strPointSeq = aData[4];
                string strReqType = aData[5];

                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetRequestOperClothesList(strOrgCd, strEmpNo, strEmpNm, strYears, strPointSeq, strReqType);
                }

                if (ds != null)
                {
                    result = Utility.DataTableToJson(ds.Tables[0]);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string ReqPermissionAll(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                List<string> lstData = new List<string>();

                foreach (string strData in aData)
                {
                    lstData.Add(strData);
                }
                DataTable dt = new DataTable();

                try
                {
                    using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                    {
                        mgmt.UpdRequestClothPermission(lstData);
                        dt = Utility.DataTableError("00", "", "");
                    }
                }
                catch
                {
                    dt = Utility.DataTableError("01", "추가승인 중 오류가 발생했습니다.", "");
                }

                result = Utility.DataTableToJson(dt);
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetWorkingClothSize(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataTable dt = new DataTable();

                try
                {
                    string strRequestId = aData[0];
                    string strYears = aData[1];
                    string strPointSeq = aData[2];
                    string strDetailSeq = aData[3];
                    string strEmpNo = aData[4];
                    string strSize = aData[5];

                    using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                    {
                        mgmt.UdpWorkingClothSize(strRequestId, strYears, strPointSeq, strDetailSeq, strEmpNo, strSize);
                        dt = Utility.DataTableError("00", "", "");
                    }
                }
                catch
                {
                    dt = Utility.DataTableError("01", "추가승인 중 오류가 발생했습니다.", "");
                }

                result = Utility.DataTableToJson(dt);
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        protected void btnModify_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnModify = (Button)sender;
                Label lblRequestID = (Label)btnModify.Parent.FindControl("lblRequestID");
                Label lblYears = (Label)btnModify.Parent.FindControl("lblYears");
                Label lblPointSeq = (Label)btnModify.Parent.FindControl("lblPointSeq");
                Label lblDetailSeq = (Label)btnModify.Parent.FindControl("lblDetailSeq");
                Label lblEmpNo = (Label)btnModify.Parent.FindControl("lblEmpNo");
                Label lblClothType = (Label)btnModify.Parent.FindControl("lblClothType");

                TextBox txbSize = (TextBox)btnModify.Parent.FindControl("txbSize");
                DropDownList ddlSize1 = (DropDownList)btnModify.Parent.FindControl("ddlSize1");
                DropDownList ddlSize2 = (DropDownList)btnModify.Parent.FindControl("ddlSize2");
                Label lblBtn = (Label)btnModify.Parent.FindControl("lblBtn");

                string strRequestId = lblRequestID.Text;
                string strYears = lblYears.Text;
                string strPointSeq = lblPointSeq.Text;
                string strDetailSeq = lblDetailSeq.Text;
                string strEmpNo = lblEmpNo.Text;

                if (strRequestId.Length == 0 || strYears.Length == 0 || strPointSeq.Length == 0 || strDetailSeq.Length == 0 || strEmpNo.Length == 0 || !lblBtn.Text.Equals("M"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    string strClothType = lblClothType.Text;
                    string strSize = "";
                    string strMsg = "";

                    if (strClothType.Equals("상의") || strClothType.Equals("점퍼") || strClothType.Equals("조끼")
                        || strClothType.Equals("티셔츠") || strClothType.ToUpper().Equals("긴팔T") || strClothType.ToUpper().Equals("반팔T")
                        || strClothType.Equals("외피") || strClothType.Equals("내피")
                        || strClothType.Equals("신발") || strClothType.Equals("모자") || strClothType.Equals("버프"))
                    {
                        if (ddlSize1.SelectedValue.Length == 0)
                        {
                            strMsg = "사이즈를 선택해야합니다.";
                        }
                        else
                        {
                            strSize = ddlSize1.SelectedValue;
                        }
                    }
                    else if (strClothType.Equals("하의") || strClothType.Contains("바지") || strClothType.Contains("방한바지"))
                    {
                        if (ddlSize1.SelectedValue.Length == 0 || ddlSize2.SelectedValue.Length == 0)
                        {
                            strMsg = "사이즈 두가지 모두 선택해야합니다.";
                        }
                        else
                        {
                            strSize = ddlSize1.SelectedValue + "/" + ddlSize2.SelectedValue;
                        }
                    }
                    else
                    {
                        if (txbSize.Text.Length == 0)
                        {
                            strMsg = "사이즈를 입력해야합니다.";
                        }
                        else
                        {
                            strSize = txbSize.Text;
                        }
                    }

                    if (strMsg.Length > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('" + strMsg + "');", true);
                    }
                    else
                    {
                        using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                        {
                            mgmt.UdpWorkingClothSize(strRequestId, strYears, strPointSeq, strDetailSeq, strEmpNo, strSize);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Modify", "alert('수정되었습니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetPassAll(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "WORKINGCLOTHPASSALLFOROPER";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataTable dt = new DataTable();

                try
                {
                    result = dbTransaction.GetJsonData();
                }
                catch
                {
                    dt = Utility.DataTableError("01", "추가승인 중 오류가 발생했습니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
    }
}