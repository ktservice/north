﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OperStandardClothStats.aspx.cs" Inherits="KTSSolutionWeb.OperStandardClothStats" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var ktsGrid1;
        var ktsGrid2;
        var grid1;
        var grid2;
        var callGb;

        var page = {
            init: function () {
                fn_LeftGridInit();
                fn_RightGridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlYears.ClientID %>").change(function (e) {
                    fn_GetPointSeq();
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel1").click(function (e) {
                    ktsGrid1.download("xlsx", "피복지급내역통계-지급기준.xlsx", { sheetName: "지급기준" });
                });

                $("#gridExcel2").click(function (e) {
                    ktsGrid2.download("xlsx", "피복지급내역통계-연도지급기준결과.xlsx", { sheetName: "연도지급기준결과" });
                });
            }
        };

        var fn_LeftGridInit = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "CLOTHGBN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "종류", field: "CLOTHTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "수량", field: "PROVIDECNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단가", field: "COST", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "인원", field: "EMPUSERCNT", sorter: "string"},
                { headerHozAlign: "center", hozAlign: "center", title: "구매수량", field: "BUYCNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "금액", field: "TOTALCOST", sorter: "string" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid1 = ktsTabulator;
            grid1.gridDefault('ktsGrid1', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid1 = ktsGrid;
        }

        var fn_RightGridInit = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "CLOTHGBN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "종류", field: "CLOTHTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "수량", field: "PROVIDECNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단가", field: "COST", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "인원", field: "EMPUSERCNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "구매수량", field: "BUYCNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "금액", field: "TOTALCOST", sorter: "string" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid2 = ktsTabulator;
            grid2.gridDefault('ktsGrid2', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid2 = ktsGrid;
        }

        var successCallBack = function (json) {
            if (callGb == 'PointSeq') {
                var obj = $("#ddlPointSeq");

                obj.empty();

                obj.append("<option value=''>선택하세요</option>");
                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.POINTSEQ + "'>" + item.TITLE + "</option>");
                });
            }
            callGb = '';
        }

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                if (ktsGridId == 'ktsGrid1') {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid1.setData(json);
                    ktsGridRowCount(ktsGrid1.getDataCount());
                    fn_GetRequsetStats();
                }
                else {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid2.setData(json);
                    ktsGridRowCount(ktsGrid2.getDataCount());
                }
            }
        }

        var fn_Load = function () {
        }

        var fn_GetPointSeq = function () {

            var obj = $("#ddlPointSeq");

            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();

            if (Years.length == 0) {
                obj.empty();

                obj.append("<option value=''>선택하세요</option>");

            } else {

                var aData = [];
                aData[0] = Years;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPointSeq');
                callGb = 'PointSeq';

                ajaxCall(jsonData, ajaxUrl);
            }
        }

        var fn_Search = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#ddlPointSeq option:selected").val();

            if (Years.length == 0 || PointSeq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
            } else {
                fn_GetStandard();
            }
        }

        var fn_GetStandard = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#ddlPointSeq option:selected").val();

            //입력 파라미터
            var aData = [];
            aData[0] = Years;
            aData[1] = PointSeq;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid1.getUrl(location.href, 'fn_GetStandard');
            //grid Bind
            grid1.getDataBind(jsonData, ajaxUrl, 'ktsGrid1');
        }

        var fn_GetRequsetStats = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#ddlPointSeq option:selected").val();

            //입력 파라미터
            var aData = [];
            aData[0] = Years;
            aData[1] = PointSeq;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid2.getUrl(location.href, 'GetRequsetStats');

            //grid Bind
            grid2.getDataBind(jsonData, ajaxUrl, 'ktsGrid2');
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;margin-bottom:20px;">
		    <fieldset>
                <span class="optionbox">
                    <label>연도</label>
                    <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="false" Width="150px"></asp:DropDownList>
			    </span> 
                <span class="optionbox" style="margin-left:10px;">
                    <label>지급기준</label>
                    <select id="ddlPointSeq" style="width:250px;">
                        <option value="">선택하세요</option>
                    </select>
			    </span> 
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <!-- S:leftbox -->
            <div class="leftbox">    
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>기준지급</strong>
                        <label class="searchrowcount1"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel1" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->
    
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <div id="ktsGrid1"></div>
                    </div>    
                    <!-- E:scrollbox -->
                </div>
            </div>
            <!-- E:leftbox -->
            <!-- S:rightbox -->
            <div class="rightbox">
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>연도/지급기준 결과</strong>
                        <label class="searchrowcount2"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel2" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <!-- E:list-top -->                    
                        
                    <!-- S:scrollbox -->
                    <div class="scrollbox">
                        <div id="ktsGrid2"></div>
                    </div>    
                    <!-- E:scrollbox -->
                </div>
                <!-- E:datalist -->
            </div>                
            <!-- E:rightbox -->
        </div>
        <!-- E:multiplebox -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>