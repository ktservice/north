﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.WorkingClothes;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class UserRequestClothInfo : PageBase
    {
        private string ReqType
        {
            get
            {
                if (ViewState["ReqType"] != null)
                {
                    return ViewState["ReqType"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ReqType"] = value;
            }
        }
        private string REQYN
        {
            get
            {
                if (ViewState["REQYN"] != null)
                {
                    return ViewState["REQYN"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["REQYN"] = value;
            }
        }

        private string NEWREQYN
        {
            get
            {
                if (ViewState["NEWREQYN"] != null)
                {
                    return ViewState["NEWREQYN"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["NEWREQYN"] = value;
            }
        }

        private string REQLISTYN
        {
            get
            {
                if (ViewState["REQLISTYN"] != null)
                {
                    return ViewState["REQLISTYN"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["REQLISTYN"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLYears();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                ddlYears.Items.Clear();

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetYearsPointList();
                }

                ddlYears.Items.Add(new ListItem("선택", ""));

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlYears.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPointSeq(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = null;

                string strYears = aData[0];

                using (PointMgmt point = new PointMgmt())
                {
                    ds = point.GetProvidePointInfo(strYears);
                }

                if (ds != null)
                {
                    result = Utility.DataTableToJson(ds.Tables[0]);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = null;

                string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();
                string strYears = aData[0];
                string strPointSeq = aData[1];

                string strJobNm = HttpContext.Current.Session["JOBNM2"].ToString().ToLower();

                string strReqType = "N";
                string strAddReqChk = "N";
                string strReqYn = "N";
                string strNewReqYn = "N";
                string strReqListYn = "N";

                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetRequestUserClothesList(strEmpNo, strYears, strPointSeq);
                }

                if (ds != null && ds.Tables.Count >= 4)
                {
                    DataSet dsNew = new DataSet();
                    DataTable dt1 = ds.Tables[0].Copy();
                    dt1.Columns.Add("REQYN");

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        strReqType = ds.Tables[1].Rows[0]["REQTYPE"].ToString();
                        strAddReqChk = ds.Tables[1].Rows[0]["ADDREQCHK"].ToString();
                    }

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        strReqYn = ds.Tables[2].Rows[0]["REQYN"].ToString();
                        strNewReqYn = ds.Tables[2].Rows[0]["NEWREQYN"].ToString();
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        strReqListYn = ds.Tables[3].Rows[0]["REQLISTYN"].ToString();
                    }

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        dt1.Rows[i]["REQYN"] = strReqYn;
                    }

                    dsNew.Tables.Add(dt1);

                    bool bReq = false;
                    bool bPass = false;

                    if (!strYears.Equals("") && !strPointSeq.Equals("") && strReqYn.Equals("Y") && strNewReqYn.Equals("Y"))
                    {
                        if (strReqType.Equals("A"))
                        {
                            if (strJobNm.Equals("kte") && strAddReqChk.Equals("Y"))
                            {
                                bReq = true;
                            }
                        }
                        else
                        {
                            bReq = true;

                            if (!strReqType.Equals("N"))
                            {
                                bPass = true;
                            }
                        }
                    }

                    DataTable dt2 = new DataTable();

                    dt2.Columns.Add("POPUPTYPE");
                    dt2.Columns.Add("YEARS");
                    dt2.Columns.Add("POINTSEQ");
                    dt2.Columns.Add("EMPNO");
                    dt2.Columns.Add("REQTYPE");
                    dt2.Columns.Add("JOBNM");
                    dt2.Columns.Add("PASSYN");

                    DataRow dr = dt2.NewRow();

                    if (bReq)
                    {
                        if (strReqListYn.Equals("Y"))
                        {
                            dr["POPUPTYPE"] = "Y";
                            dr["YEARS"] = strYears;
                            dr["POINTSEQ"] = strPointSeq;
                            dr["EMPNO"] = strEmpNo;
                            dr["REQTYPE"] = strReqType;
                            dr["JOBNM"] = strJobNm;
                        }
                        else
                        {
                            dr["POPUPTYPE"] = "A";
                        }
                        if(bPass)
                        {
                            dr["PASSYN"] = "Y";
                        }
                        else
                        {
                            dr["PASSYN"] = "N";
                        }
                    }
                    else
                    {
                        dr["POPUPTYPE"] = "N";
                        dr["PASSYN"] = "N";
                    }

                    dt2.Rows.Add(dr);
                    dsNew.Tables.Add(dt2);

                    result = Utility.DataSetToJson(dsNew);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetPassAll(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "WORKINGCLOTHPASSALL";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }        
    }
}