﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OperRequestClothInfo.aspx.cs" Inherits="KTSSolutionWeb.OperRequestClothInfo"%>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlYears.ClientID %>").change(function (e) {
                    fn_GetPointSeq();
                });

                $("#<%= ddlPointSeq.ClientID %>").change(function (e) {
                    fn_GetTeam();
                });

                $("#btnSelect").click(function (e) {

                    var years = $("#<%= ddlYears.ClientID %> option:selected").val();
                    var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
                    var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();

                    if (years.length == 0 || pointseq.length == 0) {
                        alert("연도와 지급기준을 선택해주세요.");
                    } else if (OrgCd.length == 0) {
                        alert("조직을 선택해주세요.");
                    } else {
                        fn_Search();
                    }
                });
                
                $("#btnPerm").click(function (e) {
                    fn_Permission();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.hideColumn("SAVE");
                    ktsGrid.download("xlsx", "피복신청승인관리.xlsx", { sheetName: "피복신청승인관리" });
                    ktsGrid.showColumn("SAVE");
                });
            }
        };

        var fn_GridInit = function () {

            var inputCheck = function (cell) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var reqType = rowData.REQTYPE;
                var reqStat = rowData.REQSTATUS;

                var element;

                //if (reqType == "A" && reqStat == "W") {
                if (reqStat == "W") {
                    element = document.createElement("input");
                    element.id = "cb_" + id;
                    element.name = "chkbox_Conf";
                    element.setAttribute("type", "checkbox");

                    element.checked = false;

                    function onChange() {
                        var confYn = "N";

                        if (element.checked) {
                            confYn = "Y";
                        }
                        else {
                            confYn = "N";
                        }

                        ktsGrid.updateData([{
                            id: id, CONFRIMYN: confYn
                        }])
                            .then(function () {
                                //run code after row
                            })
                            .catch(function (error) {
                                //handle error
                            });
                    }

                    //submit new value on blur or change
                    element.addEventListener("click", onChange);

                    return element;
                }
                else {
                    return id;
                }
            };

            var inputSize = function (cell) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var btnChk = rowData.BTNCHK;
                var clothType = rowData.CLOTHTYPE;
                var clothSize = rowData.CLOTHSIZE;
                var reqStat = rowData.REQSTATUS;

                if (reqStat != "W" && reqStat != "PF") {
                    if (btnChk == "M") {
                        var selType = "";

                        if (clothType == "상의" || clothType == "점퍼" || clothType == "조끼" || clothType == "티셔츠" || clothType.toUpperCase() == "긴팔T" || clothType.toUpperCase() == "반팔T"
                            || clothType == "외피" || clothType == "내피" || clothType == "신발" || clothType == "모자" || clothType == "버프") {
                            selType = "1";
                        }
                        else if (clothType == "하의" || clothType == "바지" || clothType == "방한바지") {
                            selType = "2";
                        }
                        else {
                            selType = "0";
                        }

                        if (selType == "1") {
                            var selector = document.createElement("select");
                            selector.id = "sel_" + id;

                            selector.style.padding = "1px";
                            selector.style.width = "80px";
                            selector.style.height = "100%";
                            selector.style.boxSizing = "border-box";
                            selector.style.textAlign = "center";

                            if (clothType == "모자" || clothType == "버프") {
                                var option = document.createElement("option");

                                option.className = "options";
                                option.value = "FREE";
                                option.text = "FREE";

                                selector.append(option);
                            }
                            else if (clothType == "신발") {
                                var option = document.createElement("option");

                                option.className = "options";
                                option.value = "";
                                option.text = "선택";

                                selector.append(option);

                                for (var i = 230; i <= 300; i += 5) {
                                    var options = document.createElement("option");

                                    options.className = "options";
                                    options.value = i;
                                    options.text = i;
                                    selector.append(options);
                                }
                            }
                            else {
                                var option = document.createElement("option");

                                option.className = "options";
                                option.value = "";
                                option.text = "선택";

                                selector.append(option);

                                for (var i = 44; i <= 77; i += 11) {
                                    var options = document.createElement("option");

                                    options.className = "options";
                                    options.value = i;
                                    options.text = i;
                                    selector.append(options);
                                }

                                for (var i = 90; i <= 125; i += 5) {
                                    var options = document.createElement("option");

                                    options.className = "options";
                                    options.value = i;
                                    options.text = i;
                                    selector.append(options);
                                }
                            }

                            selector.value = clothSize;

                            function onChange(e) {
                                var size = this.value;

                                ktsGrid.updateData([{
                                    id: id, CLOTHSIZE: size
                                }])
                                    .then(function () {
                                        //run code after row
                                    })
                                    .catch(function (error) {
                                        //handle error
                                    });
                            }

                            //submit new value on blur or change
                            selector.addEventListener("change", onChange);

                            return selector;
                        }
                        else if (selType == "2") {

                            var selector1 = document.createElement("select");
                            selector1.id = "sel1_" + id;

                            selector1.style.padding = "1px";
                            selector1.style.width = "60px";
                            selector1.style.height = "100%";
                            selector1.style.boxSizing = "border-box";
                            selector1.style.textAlign = "center";

                            var option = document.createElement("option");

                            option.className = "options";
                            option.value = "";
                            option.text = "선택";

                            selector1.append(option);

                            var selector2 = document.createElement("select");
                            selector2.id = "sel2_" + id;

                            selector2.style.marginLeft = "3px";
                            selector2.style.padding = "1px";
                            selector2.style.width = "100px";
                            selector2.style.height = "100%";
                            selector2.style.boxSizing = "border-box";
                            selector2.style.textAlign = "center";

                            for (var i = 24; i <= 46; i++) {
                                var options = document.createElement("option");

                                options.className = "options";
                                options.value = i;
                                options.text = i;
                                selector1.append(options);
                            }

                            var sizelist = ["선택|", "160cm이하|SSS", "160~165cm|SS", "165~170cm|S", "170~175cm|M", "175~180cm|L", "180~185cm|LL", "185~190cm|LLL", "FREE|FREE"];

                            sizelist.forEach(function (str) {
                                var options = document.createElement("option");
                                var size = str.split('|');

                                options.className = "options";
                                options.value = size[1];
                                options.text = size[0];
                                selector2.append(options);
                            });

                            var cSize = clothSize.split('/');

                            selector1.value = cSize[0];
                            selector2.value = cSize[1];

                            function onChange(e) {
                                var rowid = this.id.split('_')[1];
                                var size1 = document.getElementById("sel1_" + rowid).value;
                                var size2 = document.getElementById("sel2_" + rowid).value;

                                var size = size1 + "/" + size2;

                                ktsGrid.updateData([{
                                    id: id, CLOTHSIZE: size
                                }])
                                    .then(function () {
                                        //run code after row
                                    })
                                    .catch(function (error) {
                                        //handle error
                                    });
                            }

                            //submit new value on blur or change
                            selector1.addEventListener("change", onChange);
                            selector2.addEventListener("change", onChange);

                            var div = document.createElement("div");
                            div.style.width = "100%";
                            div.style.height = "100%";

                            div.append(selector1);
                            div.append(selector2);

                            return div;
                        }
                        else {
                            var input = document.createElement("input");

                            input.setAttribute("type", "Text");

                            input.style.padding = "4px";
                            input.style.width = "100%";
                            input.style.boxSizing = "border-box";
                            input.setAttribute("maxlength", "10");

                            input.value = clothSize;

                            function onChange(e) {
                                var size = this.value;

                                ktsGrid.updateData([{
                                    id: id, CLOTHSIZE: size
                                }])
                                    .then(function () {
                                        //run code after row
                                    })
                                    .catch(function (error) {
                                        //handle error
                                    });
                            }

                            input.addEventListener("blur", onChange);

                            return input;
                        }
                    }
                    else {
                        return clothSize;
                    }
                }
                else {
                    return clothSize;
                }
            };

            var fn_regbutton = function (cell) {
                var rowData = cell.getRow().getData();
                var id = rowData.id;
                var reqStat = rowData.REQSTATUS;
                var btnChk = rowData.BTNCHK;
                var years = rowData.YEARS;
                var pointseq = rowData.POINTSEQ;
                var empno = rowData.EMPNO;
                var reqtype = rowData.REQTYPE;

                var input = "";

                if (reqStat != "W" && reqStat != "PF") {
                    if (reqStat == "D" && btnChk == "D") {
                        input = "<input id='btnSave' type='button' onclick='fn_ReqClothPopup(\"" + id + "\");' value='대리신청' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                        input += "&nbsp;<input id='btnSave' type='button' onclick='fn_SetPassAll(\"" + years + "\", \"" + pointseq + "\", \"" + empno + "\", \"" + reqtype + "\");' value='미신청' class='btn-del' style='height:23px;line-height:0px;font-size:15px'>";
                    }
                    else if (btnChk == "M") {
                        input = "<input id='btnSave' type='button' onclick='fn_ModifySize(\"" + id + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
                    }
                }

                return input;
            };
            

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", field: "id", sorter: "number", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", formatter: inputCheck, width: 60, headerSort: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", width: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", width: 80, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM2", width: 60, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지급기준", field: "TITLE", width: 200, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청구분", field: "PROVIDETYPE", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청상태", field: "REQSTATUSNM", width: 120, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "작업복구분", field: "CLOTHGBN", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "종류", field: "CLOTHTYPE", width: 80, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사이즈", field: "SIZE", width: 210, formatter: inputSize },
                { headerHozAlign: "center", hozAlign: "center", title: "수량", field: "REQCNT", width: 80, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청일자", field: "REGDT", width: 120, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사용포인트", field: "USEPOINT", width: 120, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "대리신청", field: "SAVE", width: 160, formatter: fn_regbutton },
                { headerHozAlign: "center", hozAlign: "center", field: "REQUESTID", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "YEARS", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "POINTSEQ", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "DETAILSEQ", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "REQSTATUS", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "REQTYPE", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "BTNCHK", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "REQUESTID", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "CONFRIMYN", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "CLOTHSIZE", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            //ktsGrid.setHeight("30vh");

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("sValue");

            $("#selField").change(function (e) {
                ValueEl.value = '';

                if (FieldEl.value == "") {
                    valueEl.setAttribute('disabled', 'disabled');
                    ktsGrid.setFilter('id', 'like', '');
                }
                else {
                    ValueEl.removeAttribute('disabled');
                    ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
                }
            });

            $("#sValue").keyup(function (e) {
                var sField = FieldEl.value;

                ktsGrid.setFilter(sField, 'like', ValueEl.value);
            });
        }

        var fn_Load = function () {
            //fn_Search();
        }

        var ajaxCallBack = function (json) {
            if (callGb != "") {
                if (callGb == "getOk") {
                    ktsGrid.setData(json);
                }
                else {
                    $.each(JSON.parse(json), function (idx, item) {
                        if (item.SERVICE_ERROR_CD == '00') {
                            if (callGb == 'PermissionOk') {
                                //ktsGridAlert('체크한 추가신청 승인을 완료했습니다.', 2000);
                                ktsGridAlert('체크한 미신청 승인을 완료했습니다.', 2000);

                                fn_Search();
                            }
                            else if (callGb == 'RegOk') {
                                ktsGridAlert('사이즈 수정을 완료했습니다.', 2000);

                                fn_Search();
                            }
                            else if (callGb == 'setPassAll') {
                                ktsGridAlert('해당 직원의 피복 미신청 처리를 완료 했습니다.', 2000);

                                fn_Search();
                            }
                        }
                        else {
                            ktsGridAlert(item.ERRMSG);
                        }
                    });
                }

                callGb = '';
            }
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                var org = json.split('|');

                if (org.length > 1) {
                    $("#<%= hdfOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);
                }
            }
            else if (callGb == 'getPointSeq') {
                var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");
                $.each(JSON.parse(json), function (idx, item) {
                    ddlPointSeq.append("<option value='" + item.POINTSEQ + "||" + item.STDT + "'>" + item.TITLE + "</option>");
                });
            }
            callGb = '';
        }

        var fn_GetPointSeq = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");

            ddlPointSeq.html("");
            ddlPointSeq.append("<option value=''>선택하세요</option>");

            if (Years != "") {
                //입력 파라미터
                var aData = [];
                aData[0] = Years;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetPointSeq');

                callGb = 'getPointSeq';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
                $("#<%= hdfMonth.ClientID %>").val('');
            }
        }

        var fn_GetTeam = function () {
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (pointseq != '') {
                var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
                var pointdata = pointseq.split("||");

                if (pointdata.length == 2) {
                    var sumupmonth = pointdata[1].substring(0, 6);

                    $("#<%= hdfMonth.ClientID %>").val(sumupmonth);

                    var aData = [];
                    aData[0] = OrgCd;
                    aData[1] = sumupmonth;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                    callGb = 'GetTeam';

                    ajaxCall(jsonData, ajaxUrl);
                }
                else {
                    alert("잘못된 접근입니다.");
                }
            }
            else {
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
                $("#<%= hdfMonth.ClientID %>").val('');
            }

        }

        var fn_Search = function () {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var EmpNo = $("#<%= txbEmpNo.ClientID %>").val();
            var EmpNm = $("#<%= txbEmpNm.ClientID %>").val();
            var ReqType = $("#<%= ddlStatus.ClientID %> option:selected").val();

            if (Years.length == 0 || PointSeq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {
                //입력 파라미터
                var aData = [];
                aData[0] = OrgCd.replace(/,/gi, "^");
                aData[1] = EmpNo;
                aData[2] = EmpNm;
                aData[3] = Years;
                aData[4] = PointSeq.split("||")[0];
                aData[5] = ReqType;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_Permission = function () {
            //if (confirm("체크한 추가신청을 승인하시겠습니까?")) {
            if (confirm("체크한 미신청을 승인하시겠습니까?")) {
                var dataCnt = ktsGrid.getDataCount();
                var aData = [];
                var n = 0;

                callGb = 'PermissionOk';

                for (var i = 1; i <= dataCnt; i++) {
                    var row = ktsGrid.getRow(i);
                    var rowData = row.getData();

                    var id = rowData.id;
                    var chkbox = document.getElementById("cb_" + id);

                    if (chkbox == undefined || chkbox == "undefined" || chkbox == null) {
                        continue;
                    }

                    if (chkbox.checked) {
                        var requestId = rowData.REQUESTID;
                        var years = rowData.YEARS;
                        var pointSeq = rowData.POINTSEQ;
                        var detailSeq = rowData.DETAILSEQ;
                        var empNo = rowData.EMPNO;

                        aData[n] = requestId + "|" + years + "|" + pointSeq + "|" + detailSeq + "|" + empNo;
                        n++;
                    }
                    else {
                        continue;
                    }
                }

                if (aData.length > 0) {
                    var pagenum = ktsGrid.getPage();

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "ReqPermissionAll");

                    ajaxCallEx(jsonData, ajaxUrl, true);

                    var maxPageNum = ktsGrid.getPageMax();

                    if (maxPageNum < pagenum) {
                        ktsGrid.setPage(maxPageNum);
                    }
                    else {
                        ktsGrid.setPage(pagenum);
                    }
                }
            }
        }

        var fn_ReqClothPopup = function (id) {
            if (id == undefined || id == "0" || id == "" || id == null) {
                alert("잘못된 접근입니다.");
                return false;
            }
            else {
                var row = ktsGrid.getRow(id);
                var rowData = row.getData();

                var years = rowData.YEARS;
                var pointSeq = rowData.POINTSEQ;
                var empNo = rowData.EMPNO;
                var reqType = rowData.REQTYPE;

                var nWidth = 1000;
                var nHeight = 900;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    YEARS: years,
                    POINTSEQ: pointSeq,
                    EMPNO: empNo,
                    REQTYPE: reqType,
                    SUBREQ: "Y"
                };

                var Popupform = createForm("/WorkingClothes/RegRequestCloth", param);

                Popupform.target = "RegRequestCloth";
                var win = window.open("", "RegRequestCloth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        var fn_SetPassAll = function (Years, PointSeq, EmpNo, ReqType) {
            if (confirm("해당직원의 피복요청을 일괄 미신청 처리 하시겠습니까?")) {
                //입력 파라미터
                var aData = [];
                aData[0] = Years;
                aData[1] = PointSeq;
                aData[2] = EmpNo;
                aData[3] = ReqType;

                var pagenum = ktsGrid.getPage();

                var jsonData = JSON.stringify({ aData: aData });

                callGb = 'setPassAll';

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "SetPassAll");

                ajaxCallEx(jsonData, ajaxUrl, true);

                var maxPageNum = ktsGrid.getPageMax();

                if (maxPageNum < pagenum) {
                    ktsGrid.setPage(maxPageNum);
                }
                else {
                    ktsGrid.setPage(pagenum);
                }
            }
            else {
                return false;
            }
        }

        var fn_ModifySize = function (id) {
            if (id == undefined || id == "0" || id == "" || id == null) {
                alert("잘못된 접근입니다.");
                return false;
            }
            else {
                if (confirm("수정하시겠습니까?")) {
                    var row = ktsGrid.getRow(id);
                    var rowData = row.getData();

                    var years = rowData.YEARS;
                    var pointSeq = rowData.POINTSEQ;
                    var detailSeq = rowData.DETAILSEQ;
                    var requestId = rowData.REQUESTID;
                    var empNo = rowData.EMPNO;
                    var clothType = rowData.CLOTHTYPE;
                    var clothSize = rowData.CLOTHSIZE;

                    var bChk = true;
                    var msg = "";

                    if (clothType == "상의" || clothType == "점퍼" || clothType == "조끼" || clothType == "티셔츠" || clothType.toUpperCase() == "긴팔T" || clothType.toUpperCase() == "반팔T"
                        || clothType == "외피" || clothType == "내피" || clothType == "신발" || clothType == "모자" || clothType == "버프") {
                        if (clothSize.length == "") {
                            msg = "사이즈를 선택해주세요.";
                            bChk = false;
                        }
                    }
                    else if (clothType == "하의" || clothType == "바지" || clothType == "방한바지") {
                        var size = clothSize.split('/');

                        if (size[0].length == 0 || size[1].length == 0) {
                            msg = "사이즈 두가지 모두 선택해주세요.";
                            bChk = false;
                        }
                    }
                    else {
                        if (clothSize.length == "") {
                            msg = "사이즈를 입력해주세요.";
                            bChk = false;
                        }
                    }

                    if (bChk) {
                        callGb = 'RegOk';

                        var aData = [];
                        aData[0] = requestId;
                        aData[1] = years;
                        aData[2] = pointSeq;
                        aData[3] = detailSeq;
                        aData[4] = empNo;
                        aData[5] = clothSize;

                        var pagenum = ktsGrid.getPage();

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = ajaxCallGetUrl(location.href, "SetWorkingClothSize");

                        ajaxCallEx(jsonData, ajaxUrl, true);

                        var maxPageNum = ktsGrid.getPageMax();

                        if (maxPageNum < pagenum) {
                            ktsGrid.setPage(maxPageNum);
                        }
                        else {
                            ktsGrid.setPage(pagenum);
                        }
                    }
                    else {
                        alert(msg);
                    }
                }
                else {
                    return false;
                }
            }
        };

        function PopupOrgTree(orgcd, empno) {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            if (years.length == 0 || pointseq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: sumupmonth,
                    pType: "cloth"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SetOrgCd(orgcd, valtype) {
            this.focus();

            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            var aData = [];
            aData[0] = orgcd;
            aData[1] = sumupmonth;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" name="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" name="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>     
                <span class="optionbox first">
                    <label>연도</label>
                    <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="false" Width="150px"></asp:DropDownList>
			    </span> 
                <span class="optionbox">
                    <label>지급기준</label>
                    <asp:DropDownList ID="ddlPointSeq" runat="server" Width="250px" AutoPostBack="false">
                        <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdfMonth" runat="server" />
                    <asp:HiddenField ID="hdfOrgCd" runat="server" />
			    </span> 
			    <span class="inpbox">
				    <label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
				    <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
			    </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
                <hr class="blank" />
                <span class="inpbox">
                    <label>이름</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" Width="150px" MaxLength="20" TabIndex="1"></asp:TextBox>
			    </span>
                <span class="inpbox">
                    <label>사번</label>
                    <asp:TextBox ID="txbEmpNo" runat="server" Width="150px" MaxLength="20" TabIndex="2" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" ></asp:TextBox>
			    </span>
                <span class="optionbox">
                    <label>상태</label>
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="150px">
                        <asp:ListItem Text="전체" Value=""></asp:ListItem>
                        <asp:ListItem Text="신규" Value="N"></asp:ListItem>
                        <asp:ListItem Text="기본" Value="B"></asp:ListItem>
                        <asp:ListItem Text="추가" Value="A"></asp:ListItem>
                        <asp:ListItem Text="미신청" Value="P"></asp:ListItem>
                        <asp:ListItem Text="신청대기" Value="D"></asp:ListItem>
                    </asp:DropDownList>
			    </span>
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>조직별 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="btnPerm" type="button" class="btn-green" value="미신청승인" />
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField">
                            <option value="" selected="selected" >선택</option>
                            <option value="EMPNO">사번</option>
                            <option value="EMPNM">성명</option>
                            <option value="JOBNM2">직무</option>
                            <option value="CLOTHTYPE">종류</option>
                        </select>
                        <label> : </label>
                        <input id="sValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
