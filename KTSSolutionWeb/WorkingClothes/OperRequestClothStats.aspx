﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OperRequestClothStats.aspx.cs" Inherits="KTSSolutionWeb.OperRequestClothStats" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlYears.ClientID %>").change(function (e) {
                    fn_GetPointSeq();
                });

                $("#<%= ddlPointSeq.ClientID %>").change(function (e) {
                    fn_GetTeam();
                });

                $("#btnSelect").click(function (e) {
                    
                    var years = $("#<%= ddlYears.ClientID %> option:selected").val();
                    var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
                    var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();

                    if (years.length == 0 || pointseq.length == 0) {
                        alert("연도와 지급기준을 선택해주세요.");
                    } else if (OrgCd.length == 0) {
                        alert("조직을 선택해주세요.");
                    } else {
                        fn_Search();
                    }
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "피복현황통계.xlsx", { sheetName: "피복현황통계" });
                });
            }
        };

        var fn_GridInit = function () {

            var inputCheck = function (cell, formatterPatterns) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var reqWaitCnt = rowData.NOREQWAITCNT;

                var element;
                if (reqWaitCnt > 0) {
                    element = document.createElement("input");
                    element.id = "cb_" + id;
                    element.name = "chkbox_Conf";
                    element.setAttribute("type", "checkbox");

                    element.checked = false;

                    function onChange() {
                        var confYn = "N";

                        if (element.checked) {
                            confYn = "Y";
                        }
                        else {
                            confYn = "N";
                        }

                        ktsGrid.updateData([{
                            id: id, CONFRIMYN: confYn
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }

                    //submit new value on blur or change
                    element.addEventListener("click", onChange);

                    return element;
                }
                else {
                    return id;
                }
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", field: "id", sorter: "number", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", formatter: inputCheck, width: 60, headerSort: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", width: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지급기준", field: "TITLE", width: 200, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대상자(명)", field: "TGTCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청자(명)", field: "REQCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "신청율(%)", field: "REQRATE", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "추가신청자(명)", field: "ADDREQCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", field: "ORGCD", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "YEARS", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "POINTSEQ", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "NOREQWAITCNT", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "CONFRIMYN", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            //ktsGrid.setHeight("30vh");

            var ValueEl = document.getElementById("sValue");

            $("#sValue").change(function (e) {
                ktsGrid.setFilter('NOREQWAITCNT', ValueEl.value, 0);
            });
        }

        var fn_Load = function () {
            //fn_Search();
        }

        var ajaxCallBack = function (json) {
            if (callGb != "") {
                if (callGb == "getOk") {
                    ktsGrid.setData(json);
                }
                else {
                    $.each(JSON.parse(json), function (idx, item) {                       
                        if (item.SERVICE_ERROR_CD == '00') {
                            if (callGb == 'RegOk') {
                                ktsGridAlert('미신청 승인을 완료했습니다.', 2000);

                                fn_Search();
                            }
                        }
                        else {
                            ktsGridAlert(item.ERRMSG);
                        }
                    });
                }

                callGb = '';
            }
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                var org = json.split('|');

                if (org.length > 1) {
                    $("#<%= hdfOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);
                }
            }
            else if (callGb == 'getPointSeq') {
                var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");
                $.each(JSON.parse(json), function (idx, item) {
                    ddlPointSeq.append("<option value='" + item.POINTSEQ + "||" + item.STDT + "'>" + item.TITLE + "</option>");
                });
            }
            callGb = '';
        }

        var fn_GetPointSeq = function () {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var ddlPointSeq = $("#<%= ddlPointSeq.ClientID %>");

            ddlPointSeq.html("");
            ddlPointSeq.append("<option value=''>선택하세요</option>");

            if (Years != "") {
                //입력 파라미터
                var aData = [];
                aData[0] = Years;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetPointSeq');

                callGb = 'getPointSeq';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
                $("#<%= hdfMonth.ClientID %>").val('');
            }
        }

        var fn_GetTeam = function () {
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (pointseq != '') {
                var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
                var pointdata = pointseq.split("||");

                if (pointdata.length == 2) {
                    var sumupmonth = pointdata[1].substring(0, 6);

                    $("#<%= hdfMonth.ClientID %>").val(sumupmonth);

                    var aData = [];
                    aData[0] = OrgCd;
                    aData[1] = sumupmonth;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                    callGb = 'GetTeam';

                    ajaxCall(jsonData, ajaxUrl);
                }
                else {
                    alert("잘못된 접근입니다.");
                }
            }
            else {
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
                $("#<%= hdfMonth.ClientID %>").val('');
            }
            
        }

        var fn_Search = function () {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length == 0 || PointSeq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {
                //입력 파라미터
                var aData = [];
                aData[0] = OrgCd.replace(/,/gi, "^");
                aData[1] = Years;
                aData[2] = PointSeq.split("||")[0];

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_Permission = function () {
            if (confirm("미신청자 일괄승인을 하시겠습니까?")) {
                var dataCnt = ktsGrid.getDataCount();
                var aData = [];
                var n = 0;

                callGb = 'RegOk';

                for (var i = 1; i <= dataCnt; i++) {
                    var row = ktsGrid.getRow(i);
                    var rowData = row.getData();

                    var id = rowData.id;
                    var chkbox = document.getElementById("cb_" + id);

                    if (chkbox == undefined || chkbox == "undefined" || chkbox == null) {
                        continue;
                    }

                    if (chkbox.checked) {
                        var orgCd = rowData.ORGCD;
                        var Years = rowData.YEARS;
                        var PointSeq = rowData.POINTSEQ;

                        aData[n] = orgCd + "|" + Years + "|" + PointSeq;
                        n++;
                    }
                    else {
                        continue;
                    }
                }

                if (aData.length > 0) {
                    var pagenum = ktsGrid.getPage();

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "ReqPermissionAll");

                    ajaxCallEx(jsonData, ajaxUrl, true);

                    var maxPageNum = ktsGrid.getPageMax();

                    if (maxPageNum < pagenum) {
                        ktsGrid.setPage(maxPageNum);
                    }
                    else {
                        ktsGrid.setPage(pagenum);
                    }
                }
                else {
                    alert("미신청 승인 대상을 선택해 주세요.");
                }
            }
        }

        function PopupOrgTree(orgcd, empno) {

            var years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var pointseq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            if (years.length == 0 || pointseq.length == 0) {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: sumupmonth,
                    pType: "cloth"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SetOrgCd(orgcd, valtype) {
            this.focus();

            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);
            var sumupmonth = $("#<%= hdfMonth.ClientID %>").val();

            var aData = [];
            aData[0] = orgcd;
            aData[1] = sumupmonth;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul id="ulPage" runat="server">
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
                <span class="optionbox first">
                    <label>연도</label>
                    <asp:DropDownList ID="ddlYears" runat="server" AutoPostBack="false" Width="150px"></asp:DropDownList>
			    </span> 
                <span class="optionbox">
                    <label>지급기준</label>
                    <asp:DropDownList ID="ddlPointSeq" runat="server" Width="250px" AutoPostBack="false">
                        <asp:ListItem Text="선택하세요" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdfMonth" runat="server" />
                    <asp:HiddenField ID="hdfOrgCd" runat="server" />
			    </span> 
			    <span class="inpbox">
				    <label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
				    <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
			    </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>조직별 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="btnPerm" type="button" class="btn-green" runat="server" visible="false" value="미신청자일괄승인" />
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <label>조회대상</label>
                        <label for="Value">: </label>
                        <select id="sValue" style="font-weight:bold">
                            <option value=">=">전체</option>
                            <option value=">">승인대기</option>
                        </select>
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
