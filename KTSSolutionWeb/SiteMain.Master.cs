﻿using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class SiteMainMaster : MasterPage
    {
        List<KtsMenuItem> menuitems { get; set; }

        string aesPassword = "ktsTestMode";

        protected void Page_Load(object sender, EventArgs e)
        {
            //SSL 적용시 처리
            if (!Request.Url.Port.Equals(443))
            {
                //Response.Redirect(Request.Url.ToString().Replace("hhttp:", "https:"));
            }

            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!GetPageAuthYn())
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/Default");
            }
            else
            {
                UserInfo userInfo = new UserInfo();

                userInfo.SetUserInfo(this.Page.Session["EMPNO"].ToString(), "", this.Page);

                if (!IsPostBack)
                {
                    this.Page.Session.Remove("TESTMODE");
                    this.Page.Session["TESTMODE"] = "";

                    SetMenuData();
                }
            }
        }

        #region GetPageAuthYn
        /// <summary>
        /// GetPageAuthYn
        /// </summary>
        /// <returns>bool</returns>
        public bool GetPageAuthYn()
        {
            DataSet ds = new DataSet();

            bool bPageAuth = false;

            try
            {
                string strEmpNo = Page.Session["EMPNO"].ToString();
                string strDefaultPage = "/Default";
                string strUrl = Request.Url.AbsolutePath.ToString();

                if (strDefaultPage.Equals(strUrl))
                {
                    bPageAuth = true;
                }
                else
                {
                    using (KTSUser biz = new KTSUser())
                    {
                        ds = biz.GetUserPageAuth(strEmpNo, strUrl);

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                bPageAuth = ds.Tables[0].Rows[0]["PAGEAUTHYN"].ToString() == "Y" ? true : false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bPageAuth;
        }
        #endregion


        #region SetMenuData
        /// <summary>
        /// SetMenuData
        /// </summary>
        private void SetMenuData()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = Page.Session["EMPNO"].ToString();
                string strEmpNm = Page.Session["EMPNM"].ToString();

                strUserNm.InnerText = strEmpNm;

                using (KTSUser biz = new KTSUser())
                {
                    ds = biz.GetUserMenu(strEmpNo);

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        menuitems = new List<KtsMenuItem>();

                        foreach (DataRow row in dt.Rows)
                        {
                            KtsMenuItem item = new KtsMenuItem(row);

                            if (!item.MENULEVEL.Equals("1"))
                            {
                                int idx = menuitems.FindIndex(t => t.MENUID == item.UPPERMENUID);
                                int order = menuitems.FindLastIndex(t => t.UPPERMENUID == item.UPPERMENUID);

                                if (order > -1)
                                {
                                    menuitems.Insert(order + 1, item);
                                }
                                else
                                {
                                    menuitems.Insert(idx + 1, item);
                                }
                            }
                            else
                            {
                                menuitems.Add(item);
                            }
                        }

                        StringBuilder strMenu1 = new StringBuilder();
                        StringBuilder strMenu2 = new StringBuilder();
                        StringBuilder strSubMenu1 = new StringBuilder();
                        StringBuilder strSubMenu1Low = new StringBuilder();
                        StringBuilder strMenu2High = new StringBuilder();
                        StringBuilder strMenu2Low = new StringBuilder();

                        strMenu1.Append("<div class=\"menuAll\"><a>전체메뉴</a></div>");
                        strMenu1.Append("<ul id=\"ulMenuList\">");
                        strMenu2.Append("<div class=\"menuAllLayerIn\">");
                        strMenu2.Append("<a id=\"closeMenuAll\" href = \"javascript:void(0);\" class=\"close\">닫기</a>");
                        strMenu2High.Append("<ul class=\"highDepth\">");
                        strSubMenu1.Append("<div class=\"menuSubLayerIn\">");
                        strSubMenu1.Append("<a id=\"closeMenuSub\" href = \"javascript:void(0);\" class=\"close\">닫기</a>");

                        int i = 0;
                        foreach (var item in menuitems)
                        {
                            if (item.MENULEVEL.Equals("1"))
                            {
                                strMenu1.AppendFormat("<li><a href=\"javascript:void(0);\" onclick=\"showSubMenu(this, '{0}')\">{1}</a></li>", item.MENUID, item.MENUNM);
                                strMenu2High.AppendFormat("<li><a id=\"{0}\" onmouseover=\"showAllSubMenu('{0}')\">{1}</a>", item.MENUID, item.MENUNM);
                                strMenu2Low.AppendFormat("<div id=\"{0}\" class=\"lowDepthWrap\" style=\"display:none\">", "lowMenu" + item.MENUID);
                                strSubMenu1Low.AppendFormat("<div id=\"{0}\" class=\"lowDepthWrap\" style=\"display:none\">", "lowSubMenu" + item.MENUID);

                                foreach (var subItem in menuitems)
                                {
                                    if (subItem.MENULEVEL.Equals("2") && subItem.UPPERMENUID == item.MENUID)
                                    {
                                        strMenu2Low.Append("<ul class=\"lowDepth\">");
                                        strMenu2Low.AppendFormat("<li class=\"title\"><a href=\"{0}\">{1}</a></li>", (subItem.URL.Length == 0 ? "#" : subItem.URL), subItem.MENUNM);
                                        strSubMenu1Low.Append("<ul class=\"lowDepth\">");
                                        strSubMenu1Low.AppendFormat("<li class=\"title\"><a href=\"{0}\">{1}</a></li>", (subItem.URL.Length == 0 ? "#" : subItem.URL), subItem.MENUNM);

                                        foreach (var subSubItem in menuitems)
                                        {
                                            if (subSubItem.UPPERMENUID == subItem.MENUID && subSubItem.MENULEVEL.Equals("3"))
                                            {
                                                strMenu2Low.AppendFormat("<li><a href=\"{0}\">{1}</a></li>", (subSubItem.URL.Length == 0 ? "#" : subSubItem.URL), subSubItem.MENUNM);
                                                strSubMenu1Low.AppendFormat("<li><a href=\"{0}\">{1}</a></li>", (subSubItem.URL.Length == 0 ? "#" : subSubItem.URL), subSubItem.MENUNM);
                                            }
                                        }

                                        strMenu2Low.Append("</ul>");
                                        strSubMenu1Low.Append("</ul>");
                                    }
                                }

                                strMenu2Low.Append("</div>");
                                strSubMenu1Low.Append("</div>");

                                i++;
                            }
                        }
                        strMenu2High.Append("</ul>");


                        strMenu1.Append("</ul>");
                        strMenu2.Append(strMenu2High);
                        strMenu2.Append(strMenu2Low);
                        strMenu2.Append("</div>");

                        strSubMenu1.Append(strSubMenu1Low);
                        strSubMenu1.Append("</div>");

                        topMenuIn.InnerHtml = strMenu1.ToString();
                        allMenuLayer.InnerHtml = strMenu2.ToString();
                        subMenuLayer.InnerHtml = strSubMenu1.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        internal string GetMenuTree(string url)
        {
            KtsMenuItem menuItem = menuitems.Where(t => t.URL == url).First();

            string menuTree = GetMenuName(menuItem, menuItem.MENUNM);

            return menuTree;
        }

        private string GetMenuName(KtsMenuItem menuItem, string childName)
        {

            if (menuitems.Where(t => t.MENUID == menuItem.UPPERMENUID).Any())
            {
                KtsMenuItem parentMenu = menuitems.Where(t => t.MENUID == menuItem.UPPERMENUID).First();

                return GetMenuName(parentMenu, parentMenu.MENUNM + ">" + childName);
            }
            else
            {
                return childName;
            }
        }



        string EncryptAES(string plainText)
        {
            UnicodeEncoding UE = new UnicodeEncoding();

            byte[] encrypted;
            byte[] passwordBytes = UE.GetBytes(aesPassword);
            byte[] aesKey = SHA256Managed.Create().ComputeHash(passwordBytes);
            byte[] aesIV = MD5.Create().ComputeHash(passwordBytes);


            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.KeySize = 256;
                aes.BlockSize = 128;
                aes.Key = aesKey;
                aes.IV = aesIV;
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                ICryptoTransform enc = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(cs))
                        {
                            sw.Write(plainText);
                        }

                        encrypted = ms.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(encrypted);
        }


        protected void aLogout_ServerClick(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("/Login");
        }

        protected void hfTCheck_ValueChanged(object sender, EventArgs e)
        {
            string strTCheck = EncryptAES(hfTCheck.Value);

            if (strTCheck.Equals("BX9sUHeJvhRfmR76Oh/qKw=="))
            {
                this.Page.Session["TESTMODE"] = 'Y';
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Ok", "alert('테스트모드가 되었습니다.');TKeyRemove();", true);
            }

        }
    }
}