﻿using KTS.KTSSolution.BSL.ExcelImport;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class WTerminalModelUploadPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                }
            }
        }

        protected void btnUpload_ServerClick(object sender, EventArgs e)
        {
            string strMsg = "";

            try
            {
                Stream stream = Request.Files["fu"].InputStream;

                if (stream.Length > 0)
                {
                    string strtype = Request.Files["fu"].ContentType;

                    if (strtype.Equals("application/vnd.ms-excel") || strtype.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    {
                        using (ExcelImport excel = new ExcelImport())
                        {
                            using (ExcelPackage excelPackage = new ExcelPackage(stream))
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                if (worksheet.Dimension.End.Column != 6)
                                {
                                    strMsg = "업로드 파일 양식이 잘못되었습니다.";
                                }
                                else
                                {
                                    string strEmpNo = Session["EMPNO"].ToString();

                                    excel.ExcelUpLoad("무선단말모델별현황", worksheet, "tb_wterminal_model_master_temp");

                                    string result = string.Empty;
                                    string resultSession = string.Empty;
                                    string rptCd = "WTERMINALSTATBYMODELINSERT";
                                    string jobGb = "SET";

                                    List<string> aData = new List<string>();

                                    aData.Add(strEmpNo);

                                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                                    resultSession = dbTransaction.SessionCheck();

                                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                                    {
                                        result = dbTransaction.GetJsonData();
                                        strMsg = "파일 업로드가 완료되었습니다.";
                                    }
                                    else //세션이 끊겼을 경우
                                    {
                                        result = resultSession;
                                        strMsg = "업로드중 오류가 발생했습니다.";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        strMsg = "엑셀파일만 업로드가 가능합니다.";
                    }
                }
                else
                {
                    strMsg = "업로드 할 파일을 선택해주세요.";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "UploadOk", "alert('" + strMsg + "')", true);
            }
            catch (Exception ex)
            {
                strMsg = "파일 업로드 중 오류가 발생했습니다.";

                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "')", true);
            }

        }
    }
}