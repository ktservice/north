﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WTerminalStatByOpen.aspx.cs" Inherits="KTSSolutionWeb.WTerminalStatByOpen" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#<%= txbToDt.ClientID %>").hide();
                        $("#lblDt").show();
                        $("#<%= txbStDt.ClientID %>").show();
                        $("#<%= txbEnDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else if (this.value == 't') {
                        $("#<%= txbToDt.ClientID %>").show();
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#<%= txbToDt.ClientID %>").hide();
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "무선개통현황.xlsx", { sheetName: "무선개통현황" });
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnSnUpload").click(function (e) {
                    fn_PopupSnUpload();
                });

                $("#btnSn").click(function (e) {
                    fn_PopupReg('S', '');;
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbToDt.ClientID %>").show();
            $("#<%= txbStDt.ClientID %>").hide();
            $("#<%= txbEnDt.ClientID %>").hide();
            $("#lblDt").hide();
            $("#selMon").hide();
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var value = cell.getValue();
            var inputSnGb = cell.getRow().getData().INPUT_SNGB;

            var mngYn = $("#<%= hfMngYn.ClientID %>").val();

            cellNo = cell.getRow().getData().id;

            if (mngYn == 'Y') {
                if (value == '') {
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"I\", \"" + cellNo + "\");' value='매핑' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
                }
                else {
                    if (inputSnGb == 'Y') {
                        html = "<label>";
                        html += value + "</label>";
                    }
                    else {
                        html = "<label style='cursor:pointer;border-bottom:1px solid black;' onclick='fn_Reg(\"U\", \"" + cellNo + "\");'>";
                        html += value + "</label>";
                    }
                }
            }
            else {
                html = "<label>";
                html += value + "</label>";
            }

            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수번호", field: "RECEIPT_NUM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수월", field: "RECEIPT_MDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일자", field: "RECEIPT_DATE", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수시간", field: "RECEIPT_TIME", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "일련번호", field: "SN", sorter: "string", width: 180, formatter: fn_CellReg },
                { headerHozAlign: "center", hozAlign: "center", title: "시리얼번호", field: "INPUT_SN", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "일련번호구분", field: "INPUT_SNGB", sorter: "string", width: 10, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "모델ID", field: "MODELCD", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "연락가능휴대폰번호", field: "MOBILE", sorter: "string", width: 180 },
                { headerHozAlign: "center", hozAlign: "center", title: "가입유형", field: "JOIN_TYPE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "주문자", field: "ORDERNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "우편번호", field: "ZIPCODE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "배송주소", field: "ADDR", sorter: "string", width: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "수령인", field: "RECEIPT_EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수령인연락처", field: "RECEIPT_PHONENO", sorter: "string", width: 180 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "PROGRESS_STATUS", sorter: "string", width: 300 },
                { headerHozAlign: "center", hozAlign: "center", title: "배송중유무", field: "SHIPPINGYN", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "요금제명", field: "PHONEPLAN", sorter: "string", width: 210 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자명", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자소속", field: "SALEORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", visible: false, width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "번들상품유형", field: "BUNDLE_TYPE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "번들상품일련번호", field: "BUNDLE_SEQ", sorter: "string", width: 140 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            fn_Progress();
        }

        var fn_Reg = function (gubun, id) {
            fn_PopupReg(gubun, id);
        }

        var fn_PopupReg = function (gubun, id) {
            var row;
            var rowData;
            var pReceiptNum = '';
            var pModelNm = '';

            if (gubun != 'S') {
                row = ktsGrid.getRow(id);
                rowData = row.getData();
                pReceiptNum = rowData.RECEIPT_NUM;
                pModelNm = rowData.MODELNM;
            }

            var form = "/WTerminal/WTerminalSnPopup";
            var target = "WTerminalSnPopup";
            var param = {
                pId: id,
                pGubun: gubun,
                pReceiptNum: pReceiptNum,
                pModelNm: pModelNm
            };

            var nWidth = 1000;
            var nHeight = 820;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupUpload = function () {
            var form = "/WTerminal/WTerminalOpenUploadPopup";
            var target = "WTerminalOpenUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupSnUpload = function () {
            var form = "/WTerminal/WTerminalSnUploadPopup";
            var target = "WTerminalSnUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function (orgcd, empno) {
            var BaseMonth = '';

            if ($("#selDt").val() == 'd') {
                BaseMonth = $("#<%= txbEnDt.ClientID %>").val().substring(0, 7);
            }
            else if ($("#selDt").val() == 't') {
                BaseMonth = $("#<%= txbToDt.ClientID %>").val().substring(0, 7);
            }
            else {
                BaseMonth = $("#selMon").val();
            }

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Eval";
                var target = "OrgTree_Eval";
                var param = {
                    pMdate: BaseMonth.replace(/-/gi, ""),
                    pRTNCDNM: 'Y',
                    pMULTICHK: 'Y',
                    pCHKNODELV: '0',
                    pSHOWORGLV: '4',
                    pOPER: 'N'
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCodeName = function (orgcd, orgnm) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            document.getElementById("<%=txbTeam.ClientID %>").value = orgnm;
        }

        var fn_SearchChk = function() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'selProgress') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                obj.append("<option value='ALL'>전체</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });


                $("#selProgress option:eq(0)").prop("selected", true);
            }
        }

        var fn_Progress = function () {
            callGb = 'selProgress';

            //입력 파라미터
            var aData = [];
            aData[0] = 'STATUS';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlList');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate1 = '';
                var pDate2 = '';

                if ($("#selDt").val() == 'd') {
                    pDate1 = $("#<%= txbStDt.ClientID %>").val();
                    pDate2 = $("#<%= txbEnDt.ClientID %>").val();
                }
                else if ($("#selDt").val() == 't') {
                    pDate1 = $("#<%= txbToDt.ClientID %>").val();
                    pDate2 = pDate1;
                }
                else {
                    pDate1 = $("#selMon").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = $("#selProgress").val();;
                aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[3] = pDate1;
                aData[4] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var SetDispItem2 = function (cellNo, pSn, pReceiptNum) {
            var row = ktsGrid.getRow(cellNo);
            row.select();

            callGb = '';

            ktsGrid.updateData([{
                id: cellNo, SN: pSn
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            if(pReceiptNum != '')
            {
                // 데이터 가져오기
                var data = ktsGrid.getData();

                // 특정 컬럼 값으로 행 찾기
                var searchValue = pReceiptNum; // 찾고자 하는 값
                var columnField = "RECEIPT_NUM"; // 컬럼 이름 또는 필드
                var columnFieldId = "id"; // id 컬럼

                var foundRow = data.find(function (rows) {

                    if (rows[columnField] == searchValue) {
                        row = ktsGrid.getRow(rows[columnFieldId]);
                        row.select();

                        ktsGrid.updateData([{
                            id: rows[columnFieldId], SN: ''
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });


                        return true;
                    }

                });

            }
            
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>
                <span class="optionbox">
                <label>일자</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="t">오늘</option>
                    <option value="d">일</option>
                    <option value="m">월</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbToDt" runat="server" ReadOnly="true" style="width:120px !important;"></asp:TextBox>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:120px !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:120px !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" />
                            <asp:HiddenField ID="hfMngYn" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox" style="margin-right:20px;">
                    <label>진행상태</label>
                    <select id="selProgress" style="width:320px;">
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSnUpload" type="button" class="btn-green last" value="일련번호일괄등록" />
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                    <input id="btnSn" type="button" class="btn-green last" value="일련번호정보" />
                    <input id="btnUpload" type="button" class="btn-green last" value="일괄등록" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>무선개통 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="RECEIPT_DATE">접수일자</option>
                            <option value="RECEIPT_NUM">접수번호</option>
                            <option value="ORGNM">소속명</option>
                            <option value="CUSTOMERNM">고객명</option>
                            <option value="EMPNM">판매자명</option>
                            <option value="SN">일련번호</option>
                            <option value="MODELNM">모델명</option>
                            <option value="PHONEPLAN">요금제명</option>
                            <option value="JOIN_TYPE">가입유형</option>
                            <option value="ORDERNO">주문자</option>
                            <option value="PROGRESS_STATUS">진행상태</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
