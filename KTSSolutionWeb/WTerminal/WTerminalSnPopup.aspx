﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WTerminalSnPopup.aspx.cs" Inherits="KTSSolutionWeb.WTerminalSnPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>일련번호매핑</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }


    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var gubun;
        var rtn;


        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#lblDt").show();
                        $("#<%= txbStDt.ClientID %>").show();
                        $("#<%= txbEnDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnClose").click(function (e) {
                    fn_Close();
                });

                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });

                $(".date").datepicker({ dateFormat: 'yy-mm-dd' });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#lblDt").show();
            $("#selMon").hide();
            fn_SetSelField();
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            var mappingYn = cell.getRow().getData().MAPPINGYN;

            if (mappingYn == 'Y') {
                html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='매핑완료' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";
            }
            else {
                html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='선택' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
            }
            


            return html;
        }


        var fn_CellCnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellMapping = function (cell, formatterParams, onRendered) {
            var html = '';
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var row = ktsGrid.getRow(id);

            if (value == 'Y') {
                html = "O";
                row.select();
            }
            else {
                html = "X";
            }

            return html;
        }

        var fn_GridInit = function () {

            var Columns = [
                { formatter: fn_CellReg, field: "REG", width: 80, minWidth: 40, hozAlign: "center", resizable: false, headerSort: false, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "최종등록월", field: "REG_MDATE", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "최종등록일", field: "REG_DATE", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "일련번호", field: "SN", sorter: "string", width: 180 },
                { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "단가", field: "OUT_AMT", sorter: "number", width: 100, formatter: fn_CellCnt },
                { headerHozAlign: "center", hozAlign: "center", title: "매핑여부", field: "MAPPINGYN", sorter: "string", width: 100, formatter: fn_CellMapping, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일시", field: "DBCREATEDT", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일시", field: "DBLASTUPDATEDT", sorter: "string", width: 140, visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("62vh");

        }

        var fn_Load = function () {
            if ($("#hdnGubun").val() == 'S') {
                $('#divListTop').show();

                ktsGrid.hideColumn("REG");
                ktsGrid.hideColumn("MAPPINGYN");
                ktsGrid.showColumn("REG_MDATE");
                ktsGrid.showColumn("REG_DATE");
                ktsGrid.showColumn("DBCREATEDT");
                ktsGrid.showColumn("DBLASTUPDATEDT");
            }
            else {
                $('#divListTop').hide();

                ktsGrid.showColumn("REG");
                ktsGrid.showColumn("MAPPINGYN");
                ktsGrid.hideColumn("REG_MDATE");
                ktsGrid.hideColumn("REG_DATE");
                ktsGrid.hideColumn("DBCREATEDT");
                ktsGrid.hideColumn("DBLASTUPDATEDT");

                fn_Search();
            }
        }

        var fn_AlertDialog = function (message, pReceiptNum) {
            $("#alertDialog").html("<p>" + message + "</p>");

            $("#alertDialog").dialog({
                modal: true,
                buttons: {
                    Ok: function () {

                        SendParentsForm($("#hdnId").val(), pReceiptNum);

                        $(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                    SendParentsForm($("#hdnId").val(), pReceiptNum);
                }
            });
        }

        var fn_ConfirmDialog = function (message, id) {
            $("#confirmDialog").html("<p>" + message + "</p>");

            $("#confirmDialog").dialog({
                modal: true,
                buttons: {
                    "Yes": function () {
                        $("#updateProgress").show();
                        fn_Transaction(id);

                        $(this).dialog("close");
                    },
                    "No": function () {
                        $(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                }
            });
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                })
            }
            else if (callGb == 'regTran') {

                var vReceiptNum = '';

                callGb = '';

                $("#updateProgress").hide();
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        vReceiptNum = item.RECEIPT_NUM;
                        
                        ktsGridAlert('매핑되었습니다.');
                    }
                });

                fn_AlertDialog('매핑을 완료했습니다.', vReceiptNum);
            }

        }

        var fn_Reg = function (id) {
            var msg = '';

            msg += '<label style="color:#ed1c24">현재 선택한 일련번호가<br/>';
            msg += '기존 접수번호에 매핑되어 있는 경우<br/>';
            msg += '기존 접수번호의 일련번호는<br/>';
            msg += '매핑이 삭제됩니다.</label><br/>';
            msg += '매핑을 진행하시겠습니까?<br/>';

            callGb = 'regTran';
            fn_ConfirmDialog(msg, id);

        }

        var vSn = '';
        var fn_Transaction = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var pSn = rowData.SN;
            var pGubun = $("#hdnGubun").val();
            var pReceiptNum = $("#hdnReceiptNum").val();

            vSn = pSn;

            //입력 파라미터
            var aData = [];
            aData[0] = pGubun;
            aData[1] = pReceiptNum;
            aData[2] = pSn;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_SetSelField = function () {
            var obj1 = $('#selField');

            obj1.empty();
            obj1.append("<option value=''>선택</option>");
            obj1.append("<option value='SN'>일련번호</option>");
            obj1.append("<option value='MAKERNM'>제조사</option>");
            obj1.append("<option value='MODELNM'>모델명</option>");
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function () {
            var pGubun = $("#hdnGubun").val();
            var pModelNm = $("#hdnModelNm").val();
            var pDate1 = '';
            var pDate2 = '';

            if (pGubun == 'S') {
                pGubun = 'ALL';

                if ($("#selDt").val() == 'd') {
                    pDate1 = $("#<%= txbStDt.ClientID %>").val();
                    pDate2 = $("#<%= txbEnDt.ClientID %>").val();
                }
                else {
                    pDate1 = $("#selMon").val();
                }
            }

            //입력 파라미터
            var aData = [];
            aData[0] = pGubun;
            aData[1] = pModelNm;
            aData[2] = pDate1;
            aData[3] = pDate2;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function fn_Close() {
            window.close();
        }

        function SendParentsForm(id, pReceiptNum) {
            if ($("#hdnGubun").val() != 'S') {
                opener.SetDispItem2(id, vSn, pReceiptNum);
            }
            window.close();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <div id="alertDialog" class="alert" title="Alert"></div>
            <div id="confirmDialog" class="confirm" title="Confirm"></div>
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">일련번호매핑</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <input type="hidden" id="hdnId" runat="server" />
                    <input type="hidden" id="hdnGubun" runat="server" />
                    <input type="hidden" id="hdnReceiptNum" runat="server" />
                    <input type="hidden" id="hdnModelNm" runat="server" />
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <div id="divListTop" class="list-top" style="display:none">
                <fieldset>
                    <span class="optionbox">
                        <label>일자</label>
                        <select id="selDt" style="width: 70px">
                            <option selected="selected" value="d">일</option>
                            <option value="m">월</option>
                        </select>
                    </span>
                    <span class="inpbox">
                        <asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" Style="width: 30% !important;"></asp:TextBox>
                        <label id="lblDt" style="width: 1px">~</label>
                        <asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" Style="width: 30% !important;"></asp:TextBox>
                        <span class="optionbox">
                            <select id="selMon" style="width: 110px"></select>
                        </span>
                    </span>
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </fieldset>
            </div>
            <br />
            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                        <strong>조회 결과</strong>
                        <label class="searchrowcount"></label>
			            <div class="pull-right">
				            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				            </div>
			            </div>
                    </div>
                    <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none;">
                                <span>
                                    <select id="selField">
                                        <option value="">선택</option>
                                    </select> : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
