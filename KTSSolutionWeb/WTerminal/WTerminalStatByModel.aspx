﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WTerminalStatByModel.aspx.cs" Inherits="KTSSolutionWeb.WTerminalStatByModel" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {


                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "무선단말 모델별 현황.xlsx", { sheetName: "무선단말 모델별 현황" });
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELCD", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "제품명", field: "PRODUCTNM", sorter: "string", width: 250 },
                { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "모델수량", field: "MODEL_CNT", sorter: "number", width: 100, formatter: fn_CellICnt }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_PopupUpload = function () {
            var form = "/WTerminal/WTerminalModelUploadPopup";
            var target = "WTerminalModelUploadPopup";
            var param = {
            };
            var nWidth = 840;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var ajaxCallBack = function (json) {
            var obj;

        }

        var fn_Search = function () {
            //입력 파라미터
            var aData = [];
            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                    <input id="btnUpload" type="button" class="btn-green last" value="일괄등록" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>무선단말 모델별 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="MAKERNM">제조사</option>
                            <option value="MODELCD">모델ID</option>
                            <option value="MODELNM">모델명</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>