﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="KTSSolutionWeb.Login" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>kt Service 북부 업무지원시스템</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    
    <webopt:bundlereference runat="server" path="~/Content/css" />

    <script type="text/javascript">
        function EnterKeyPress(event) {
            event = event || window.event;
            var keyid = (event.which) ? event.which : event.keyCode;
            if (keyid == 13) {
                if (LoginCheck()) {
                    __doPostBack("<%=btnLogin.ClientID%>", "");
                }
            }
            return;
        }

        function LoginCheck() {
            $("#hdfChk1").val($("#txbEmpNo").val());
            $("#hdfChk2").val($("#txbEmpPwd").val());

            if ($("#txbEmpNo").val() == "") {
                alert("사원번호를 입력하세요");
                $("#txbID").focus();
                return false;
            }

            if ($("#txbEmpPwd").val() == "") {
                alert("패스워드를 입력하세요");
                $("#txbEmpNo").focus();
                return false;
            }

            return true;
        }

        function PopupChangPWD() {
            var nWidth = 520;
            var nHeight = 480;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Oper/ChangePassword", null);

            Popupform.target = "ChangePassword";
            var win = window.open("", "ChangePassword", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }
        function NoticePopup(noticeid) {
            var nWidth = 600;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pNOTICEID: noticeid
            };

            var Popupform = createForm("/Common/NoticeView", param);

            Popupform.target = "NoticeView_" + noticeid;
            var win = window.open("", "NoticeView_" + noticeid, "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function ssoTokenAuth() {
            __doPostBack("<%= btnSsoAuth.UniqueID %>", "OnClick");
        }

        function fn_DiligAlert() {
            var nWidth = 400;
            var nHeight = 600;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {};

            var Popupform = createForm("/DiligAlert", param);

            Popupform.target = "DiligAlert";
            var win = window.open("", "DiligAlert", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="hdfChk1" runat="server" />
                <asp:HiddenField ID="hdfChk2" runat="server" />
                <asp:HiddenField ID="hdfSSOToken" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="wrap">
            <div class="page-login">
                <div class="login-table">
                    <div class="login-cell">
                        <div class="login-contents">
                            <h2 class="login-logo">
                                <img src="/Resource/images/icon_login_logo.png" alt="KT서비스 북부 오피스">
                            </h2>
                            <div class="loginbox">
                                <div class="inpbox">
                                    <span class="iconId" id="iconId"></span>
                                    <asp:TextBox id="txbEmpNo" runat="server" MaxLength="20" TabIndex="1" placeholder="아이디"></asp:TextBox>
                                </div>
                                <div class="inpbox">
                                    <span class="iconPw" id="iconPw"></span>
                                    <asp:TextBox id="txbEmpPwd" runat="server" MaxLength="50" TabIndex="2"  placeholder="비밀번호" AutoCompleteType="None" TextMode="Password" onkeypress="return EnterKeyPress();"></asp:TextBox>
                                </div>
                            </div>
                            <div class="button-wrapper">
                                <asp:Button id="btnLogin" runat="server" OnClientClick="return LoginCheck();" OnClick="btnLogin_Click" title="로그인" tabindex="3" class="login-btn" Text="로그인"></asp:Button>
                                <asp:Button id="btnChgPwd" runat="server" title="암호변경" OnClientClick="PopupChangPWD();return false;" tabindex="4" class="change-btn" Text="암호변경"></asp:Button>
                                <asp:Button id="btnSsoAuth" runat="server" OnClick="btnSsoAuth_Click" Visible="false"></asp:Button>
                            </div>
                        </div>
                            <!-- // E : login-contents -->
                    </div>
                    <!-- // E : login-cell -->
                </div>
                <!-- // E : login-table -->
                <p class="copyright">Copyright© 2020 kt service 북부 corp. all rights reserved</p>
            </div>
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>