﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.Framework.ExceptionManager;

namespace KTSSolutionWeb
{
    public partial class MobileMain : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                SessionExpire();
            }
            else
            {
                if (!IsPostBack)
                {
                    int loginChk = LoginChk();

                    if (loginChk.Equals(0) || loginChk.Equals(1))
                    {
                        MenuBind(loginChk);

                        PageUtility pageUtil = new PageUtility(this.Page);

                        string strEmpNo = Page.Session["EMPNO"].ToString();
                        pageUtil.DiligAlertMobile(strEmpNo, "MobileMain_Dilig");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('다시 로그인하세요.');", true);
                        SessionExpire();
                    }
                }
            }
        }

        void SessionExpire()
        {
            PageUtility pageUtil = new PageUtility(this.Page);
            pageUtil.PageMove("/MobileSessionExpire");
        }

        void MenuBind(int key)
        {
            string html = string.Empty;

            if (key.Equals(0))
            {
                if (Session["AUTHID"] != null)
                {
                    if (!Session["AUTHID"].Equals("AUTH0007"))
                    {
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PortalMyPage\"' style='background-image:url(\"/Resource/Mobile_images/icon_mypage.png?20240531\")'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/UserCarLogInfoM\"' style='background-image:url(\"/Resource/Mobile_images/icon_operation.jpg?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/UserCarMoveInfo\"' style='background-image:url(\"/Resource/Mobile_images/icon_operation.jpg\")'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/TowerWagonM\"' style='background-image:url(\"/Resource/Mobile_images/icon_towerwagon.png?20240531\")'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/UserCarReservationM\"' style='background-image:url(\"/Resource/Mobile_images/icon_CarReservation.png?20240531\")'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/ResvCarLogInfoM\"' style='background-image:url(\"/Resource/Mobile_images/icon_resvOperation.png?20240531\")'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/WorkingClothInfo\"' style='background-image:url(\"/Resource/Mobile_images/icon_cloth.jpg?20240531\")'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassNShootingM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassRegM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassInfoM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passSearch.jpg?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassNewBizInfoM\"'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/OrderInfoByEmp\"' style='background-image:url(\"/Resource/Mobile_images/icon_order.jpg?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassCompInfoM\"' style='background-image:url(\"/Resource/Mobile_images/icon-passComp.jpg\")'></a></li>";
                        

                    }
                    else
                    {
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/MaterialOut\"' style='background-image:url(\"/Resource/Mobile_images/icon_materialOut.png?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/MaterialStatByHoldingEmpM\"' style='background-image:url(\"/Resource/Mobile_images/icon_materialStat.png?20240531\")'></a></li>";
                        html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassNShootingM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassRegM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                        //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassInfoM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passSearch.jpg?20240531\")'></a></li>";
                    }
                }
                else
                {
                    html += "<li><a href='#' onclick='window.location.href=\"/Mobile/MaterialOut\"' style='background-image:url(\"/Resource/Mobile_images/icon_materialOut.png?20240531\")'></a></li>";
                    //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/MaterialStatByHoldingEmpM\"' style='background-image:url(\"/Resource/Mobile_images/icon_materialStat.png?20240531\")'></a></li>";
                    html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassNShootingM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                    //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassRegM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                    //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassInfoM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passSearch.jpg?20240531\")'></a></li>";
                }
                html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PortalSalesPolicyM\"' style='background-image:url(\"/Resource/Mobile_images/icon_SalesPolicy.jpg?20240531\")'></a></li>";
                html += "<li><a href='#' onclick='window.location.href=\"/Mobile/WTerminalStatbyInvenM\"' style='background-image:url(\"/Resource/Mobile_images/icon_wirelessInven.jpg?20240531\")'></a></li>";
                html += "<li><a href='#' onclick='window.location.href=\"/Mobile/WTerminalStatByOpenM\"' style='background-image:url(\"/Resource/Mobile_images/icon_wirelessopen.jpg?20240531\")'></a></li>";
                html += "<li><a href='#' onclick='window.location.href=\"/Mobile/InspectionListM\"' style='background-image:url(\"/Resource/Mobile_images/icon_inspection.png?20240531\")'></a></li>";
                html += "<li><a href='https://contract.ktservice.co.kr/mot_wjdcorrPtksrl_dnpqqjwjs_ahqkdlf/ktservice_dnpqrPtksrl_page/index.php'  style='background-image:url(\"/Resource/Mobile_images/icon_motCalculator.png?20240903\")'></a></li>";
            }
            else if (key.Equals(1))
            {
                html += "<li><a href='#' onclick='window.location.href=\"/Mobile/MaterialOut\"' style='background-image:url(\"/Resource/Mobile_images/icon_materialOut.png?20240531\")'></a></li>";
                //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/MaterialStatByHoldingEmpM\"' style='background-image:url(\"/Resource/Mobile_images/icon_materialStat.png?20240531\")'></a></li>";
                html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassNShootingM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassRegM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passReg.jpg?20240531\")'></a></li>";
                //html += "<li><a href='#' onclick='window.location.href=\"/Mobile/PassInfoM\"' style='background-image:url(\"/Resource/Mobile_images/icon_passSearch.jpg?20240531\")'></a></li>";
                html += "<li><a href='#' onclick='window.location.href=\"/Mobile/InspectionListByCompM\"' style='background-image:url(\"/Resource/Mobile_images/icon_inspection.png?20240531\")'></a></li>";
               
            }

            if (!html.Equals("")) phMenu.Controls.Add(new Literal() { Text = html });
        }

        int LoginChk()
        {
            int iResult = 0; //모바일kt로 로그인

            if(Session["MLogin"] != null)
            {
                string login = Session["MLogin"].ToString();
                DateTime today = DateTime.Today;

                if(login.Equals(today.ToString("yyyyMMdd"))) iResult = 1; //MLogin으로 로그인
                else iResult = 2; //키값 불일치
            }

            return iResult;

        }

        protected void btnToday_Click(object sender, EventArgs e)
        {
            try
            {
                PageUtility pageUtil = new PageUtility(this.Page);

                string strEmpNo = Page.Session["EMPNO"].ToString();

                pageUtil.SetDiligCookieM(strEmpNo, "MobileMain_Dilig");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalClose", "ModalClose();", true);

            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}