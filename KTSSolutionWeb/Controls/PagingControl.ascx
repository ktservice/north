﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingControl.ascx.cs" Inherits="KTSSolutionWeb.Controls.PagingControl" %>
<div class="paging">
    <asp:UpdatePanel ID="updPagingPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:LinkButton ID="btnFirstPage" runat="server" CssClass="btn-paging-first" OnClick="btnFirstPage_Click" Visible="false"><<</asp:LinkButton>
            <asp:LinkButton ID="btnPrevPage" runat="server" CssClass="btn-paging-prev" OnClick="btnPrevPage_Click" Visible="false"><</asp:LinkButton>
            <asp:Repeater ID="rptPages" runat="server">
                <ItemTemplate>
                    <%--<asp:LinkButton id="btnPage" runat="server" CommandName="Page" CommandArgument="<%# Container.DataItem %>"><%# Container.DataItem %></asp:LinkButton>--%>
                    <span>
                        <a id="btnPage" runat="server" onserverclick="btnPage_ServerClick"><%# Container.DataItem %></a>
                    </span>
                </ItemTemplate>
            </asp:Repeater>
            <asp:LinkButton ID="btnNextPage" runat="server" CssClass="btn-paging-next" OnClick="btnNextPage_Click" Visible="false">></asp:LinkButton>
            <asp:LinkButton ID="btnEndPage" runat="server" CssClass="btn-paging-end" OnClick="btnEndPage_Click" Visible="false">>></asp:LinkButton>
                        
            <asp:Label ID="lblPageNum" runat="server" Text="현재페이지 : 1"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>