﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuControlM.ascx.cs" Inherits="KTSSolutionWeb.Controls.MenuControlM" %>
    <style type="text/css">   
        .mobile-menu-open{
            position:absolute;
            top:0;
            right:0;
            width:50px;
            height:50px;
            bottom:30px;
            cursor:pointer;
        }
        .mobile-menu-list{
            right: 15px;
            top: 15px;
            position: absolute;
            width: 20px;
            height: 2px;
            background: #000000;
        }
        .mobile-menu-list .top-line{
            width: 20px;
            height: 2px;
            background-color: #000000;
            bottom: -5px;
            content: '';
            position: absolute;
            right: 0;
            -webkit-transition:  -webkit-transform .3s;
            transition: transform .3s;

        }
        .mobile-menu-list .bot-line {
            width: 20px;
            height: 2px;
            background-color: #000000;
            top: -5px;
            content: '';
            position: absolute;
            right: 0;
            -webkit-transition:  -webkit-transform .3s;
            transition: transform .3s;
        }
        .mobile-menu-list.on {
            width: 0;
        }
        .mobile-menu-list.on .bot-line {
            -webkit-transform: translateY(5px) rotate(135deg);
            transform: translateY(5px) rotate(135deg);
        }
        .mobile-menu-list.on .top-line{
            -webkit-transform: translateY(-5px) rotate(-135deg);
            transform: translateY(-5px) rotate(-135deg);
        }
        .mobile-menu-title{
            width:100%;
            padding:10px;
            color:white;
            font-weight:500;
            font-size:18px;
            display:block;
            justify-content:center;
            align-items:center;
            border-bottom:1px solid #e1e1e1;
            /*background-color:#c1c1c1;*/
            background-color:#0eaba7;
        }
        .mobile-menu{
            opacity: 0;
            width:100%;
            position:absolute;
            top:40px;
            bottom:0;
            z-index:-1;
        }
        .mobile-menu.on{        
            z-index:99;
            animation: fadein 1s forwards;
            -moz-animation: fadein 1s forwards; /* Firefox */
            -webkit-animation: fadein 1s forwards; /* Safari and Chrome */
            -o-animation: fadein 1s forwards; /* Opera */
            animation-fill-mode: forwards;
        }
        @keyframes fadein {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
        @-moz-keyframes fadein { /* Firefox */
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
        @-webkit-keyframes fadein { /* Safari and Chrome */
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
        @-o-keyframes fadein { /* Opera */
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
        .mobile-menu ul li{
            list-style: disc;
            display:block;
            width:100%;
            padding-top:15px;
            padding-left:15px;
            padding-right:15px;
            border-bottom:1px solid #e1e1e1;
            background-color:white;
        }
        .child-menu {
            width:100%;
            padding:0;
            display:none;
            /*background-color:#f1f1f1;*/
            background-color:#ddfafa;
        }
        .child-menu.on {
            width:100%;
            padding:0;
            display:block;
            background-color:#f1f1f1;
        }
        .parent-li {
            font-size:15px;
            font-weight:bold;
        }
        .submenu {
          background: #808080;
          height: 10px;
          position: relative;
          width: 2px;
          right:0px;
          float:right;
          top:6px;
          right:5px;
        }
        .submenu:after {
          background: #808080;
          content: "";
          height: 2px;
          top:4px;
          left:-4px;
          position: absolute;
          width: 10px;
        }
        .submenu.on {
          height: 0px;
          position: relative;
          width: 0px;
          right:0px;
          float:right;
          top:6px;
          right:7px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#mobile-menu-open").click(function () {  //버튼 클릭 시

                if ($("#mobile-menu-list").hasClass('on')) { //메뉴가 X 상태일때

                    $("#mobile-menu-list").removeClass('on'); //메뉴 원복
                    $("#<%= mobile_menu.ClientID %>").removeClass('on');  //슬라이드 메뉴 원복

                } else {

                    $("#mobile-menu-list").addClass('on');    //메뉴 3줄
                    $("#<%= mobile_menu.ClientID %>").addClass('on');     //슬라이드 메뉴 감춤

                }
            });
        });

        function fn_childMenu_open(id) {
            if ($("#ul_" + id).hasClass('on')) {
                $("#ul_" + id).removeClass('on');
                $("#ul_" + id).parent().removeClass('on');
                $("#submenu_" + id).removeClass('on');
            }
            else {
                $("#ul_" + id).addClass('on');
                $("#ul_" + id).parent().addClass('on');
                $("#submenu_" + id).addClass('on');
            }
        }
    </script>
    <div>
        <div style="display:inline-block"><h2><span id="lblContentTitle" runat="server"></span></h2></div>
        <%--<div style="display:inline-block;float:right;margin-top: 2px;"><a id="aMenuList"><img src="../Resource/Mobile_images/icon_menu.png" /></a></div>--%>
        <div class="mobile-menu-open" id="mobile-menu-open">
            <span class="mobile-menu-list" id="mobile-menu-list">
                <span class="top-line"></span>
                <span class="bot-line"></span>
            </span>
        </div>
        <hr style="width:100%;height:1px;background-color:#dfdfdf;margin-bottom:10px;" />
    </div>
    <!-- S: divMenu -->
    <div class="mobile-menu" id="mobile_menu" runat="server">
        <div class="mobile-menu-title">MENU</div>
    </div>