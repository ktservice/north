﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuControl.ascx.cs" Inherits="KTSSolutionWeb.Controls.MenuControl" %>
<!-- S: header -->
<div id="header">
	<!-- S: header-inner -->
	<div class="header-inner">
		<h1><a href="/Default">kt service 북부 업무지원시스템</a></h1>
		<div class="utilmenu">
            <div class="profile">
                <div class="pic"><img src="/Resource/images/icon_profile_default.png?20240618" alt="" /></div>
                <p><strong id="strUserNm" runat="server">케이티</strong>님 안녕하세요.</p>
            </div>
			<ul>
				<li><a onclick="UserInfoPopup();">사용자정보</a></li>
				<li><a id="aLogout" runat="server" onclick="return LogoutChk();" onserverclick="aLogout_ServerClick">로그아웃</a></li>
				<li><a onclick="NoticeList();">공지사항</a></li>
				<li><a onclick="BoardList();">기술공유</a></li>
				<li><a onclick="MMSSend();">MMS전송</a></li>
                <li><a onclick="MobileVersion();">모바일버전</a></li>
			</ul>
		</div>
	</div>
	<!-- //E: header-inner -->

	<!-- S:topmenu -->
	<div class="topmenu">
		<div id="topMenuIn" class="topmenuIn" runat="server"></div>
        <div id="allMenuLayer" class="menuAllLayer" style="display:none" runat="server"></div>
        <div id="subMenuLayer" class="menuSubLayer" style="display:none" runat="server"></div>
	</div>
	<!-- E:topmenu -->


	<script type="text/javascript">

        $(document).ready(function () {
            $("#closeMenuAll").click(function (e) {
                $(".menuAllLayer").css('display', 'none');
                $(".menuSubLayer").css('display', 'none');
            });

            $("#closeMenuSub").click(function (e) {
                $(".menuAllLayer").css('display', 'none');
                $(".menuSubLayer").css('display', 'none');
            });

            $(".menuAll").click(function (e) {
                var allLayer = $(".menuAllLayer");

                if (allLayer.is(':visible')) {
                    allLayer.hide();
                }
                else {
                    allLayer.show();
                }

                var allMenuItems = document.querySelectorAll('.menuAll');
                allMenuItems.forEach(function (item) {
                    var link = item.querySelector('a');

                    if (allLayer.is(':visible')) {
                        link.classList.add('on');
                    }
                    else {
                        link.classList.remove('on');
                    }
                });

                var items = document.querySelectorAll('[id^="ulMenuList"]');
                items.forEach(function (item) {
                    var link = item.querySelectorAll('a');

                    link.forEach(function (item) {
                        item.classList.remove('on');
                    });
                });

                //TOP서브메뉴 on 제거
                var subitems = document.querySelectorAll('[id^="ulMenuList"]');
                subitems.forEach(function (item) {
                    var link = item.querySelectorAll('a');

                    link.forEach(function (item) {
                        item.classList.remove('on');
                    });
                });

                $(".menuSubLayer").css('display', 'none');
            });

        });

        function showSubMenu(obj, menuId) {
            var items = document.querySelectorAll('[id^="lowSubMenu"]');

            menuId = 'lowSubMenu' + menuId;

            items.forEach(function (item) {
                if (item.id === menuId) {
                    item.style.display = 'flex';
                    item.style.flexWrap = 'wrap';
                } else {
                    item.style.display = 'none';
                }
            });

            //전체메뉴 on 제거
            var allLayer = $(".menuAllLayer");
            allLayer.hide();

            var allMenuItems = document.querySelectorAll('.menuAll');
            allMenuItems.forEach(function (item) {
                var link = item.querySelector('a');
                link.classList.remove('on');
            });

            var items = document.querySelectorAll('[id^="ulMenuList"]');

            items.forEach(function (item) {
                var link = item.querySelectorAll('a');

                link.forEach(function (item) {
                    item.classList.remove('on');
                });
            });

            //TOP서브메뉴 on 제거
            var subitems = document.querySelectorAll('[id^="ulMenuList"]');
            subitems.forEach(function (item) {
                var link = item.querySelectorAll('a');

                link.forEach(function (item) {
                    item.classList.remove('on');
                });
            });

            obj.classList.add('on');

            $(".menuAllLayer").css('display', 'none');
            $(".menuSubLayer").css('display', 'flex');

        }

        function showAllSubMenu(menuId) {
            var topMenuItems = document.querySelectorAll('.highDepth');
            var items = document.querySelectorAll('[id^="lowMenu"]');

            topMenuItems.forEach(function (item) {
                var link = item.querySelectorAll('a');

                link.forEach(function (item) {
                    if (item.id === menuId) {
                        item.classList.add('on');
                    } else {
                        item.classList.remove('on');
                    }
                });
            });

            menuId = 'lowMenu' + menuId;

            items.forEach(function (item) {
                if (item.id === menuId) {
                    item.style.display = 'flex';
                    item.style.flexWrap = 'wrap';
                } else {
                    item.style.display = 'none';
                }
            });
        }
    </script>
</div>
<!-- E: header -->