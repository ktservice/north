﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Linq;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb.Controls
{
    public partial class PagingControl : ControlBase
    {
        public int PageSize
        {
            get
            {
                if (ViewState["PageSize"] != null)
                    return Convert.ToInt32(ViewState["PageSize"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageSize"] = value;
            }
        }
        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public int TotalRows
        {
            get
            {
                if (ViewState["TotalRows"] != null)
                    return Convert.ToInt32(ViewState["TotalRows"]);
                else
                    return 0;
            }
            set
            {
                ViewState["TotalRows"] = value;
            }
        }

        public DataTable Dt
        {
            get
            {
                if (Session["DT" + this.ClientID] != null)
                    return (DataTable)Session["DT" + this.ClientID];
                else
                    return null;
            }
            set
            {
                Session["DT" + this.ClientID] = value;
            }
        }

        public bool bPreRender
        {
            get
            {
                if (ViewState["bPreRender"] != null)
                    return (bool)ViewState["bPreRender"];
                else
                    return false;
            }
            set
            {
                ViewState["bPreRender"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        public void Page_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        public void SetPagingDataList()
        {
            try
            {
                int PageCnt = (int)Math.Ceiling((double)TotalRows / PageSize);

                if (PageNumber == -1) PageNumber = PageCnt - 1;

                int nStartNum = PageSize * PageNumber + 1;
                int nEndNum = PageSize * PageNumber + PageSize;

                ArrayList pages = new ArrayList();

                if(PageNumber < 10)
                {
                    int total = PageCnt;
                    if (total > 10) total = 10;
                    for (int i = 0; i < total; i++)
                    {
                        pages.Add(i + 1).ToString();
                    }
                }
                else if(PageNumber >= (PageCnt - (PageCnt % 10)))
                {
                    int total = PageCnt;
                    int num = total - (PageCnt % 10);

                    if (num < 0) num = 0;

                    for (int i = num; i < total; i++)
                    {
                        pages.Add(i + 1).ToString();
                    }
                }
                else
                {
                    int sNum = PageNumber - (PageNumber % 10);
                    int eNum = sNum + 10;

                    for (int i = sNum; i < eNum; i++)
                    {
                        pages.Add(i + 1).ToString();
                    }
                }

                rptPages.DataSource = pages;
                rptPages.DataBind();

                if (PageNumber < 10)
                {
                    btnFirstPage.Visible = false;
                    btnPrevPage.Visible = false;

                    if (PageCnt < 10)
                    {
                        btnNextPage.Visible = false;
                        btnEndPage.Visible = false;
                    }
                    else
                    {
                        btnNextPage.Visible = true;
                        btnEndPage.Visible = true;
                    }
                }
                else if (PageNumber >= 10)
                {
                    btnFirstPage.Visible = true;
                    btnPrevPage.Visible = true;

                    if (PageCnt - 10 > (PageNumber/10) * 10)
                    {
                        btnNextPage.Visible = true;
                        btnEndPage.Visible = true;
                    }
                    else
                    {
                        btnNextPage.Visible = false;
                        btnEndPage.Visible = false;
                    }
                }

                for (int i = 0; i < rptPages.Items.Count; i++)
                {
                    HtmlAnchor btn = (HtmlAnchor)rptPages.Items[i].FindControl("btnPage");
                    
                    if (btn.InnerText.ToString().Equals((PageNumber + 1).ToString()))
                    {
                        btn.Attributes.Remove("class");
                        btn.Attributes.Add("class", "on");                        
                    }
                    else
                    {
                        btn.Attributes.Remove("class");
                    }
                }

                lblPageNum.Text = "현재페이지 : " + (PageNumber + 1).ToString();

                updPagingPanel.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DtSessionClear()
        {
        }

        public void RepeaterDataBind(Repeater repeater)
        {
            PagedDataSource pgitems = new PagedDataSource();

            pgitems.AllowPaging = true;
            pgitems.PageSize = PageSize;

            int PageCnt = (int)Math.Ceiling((double)TotalRows / PageSize);

            if (PageNumber == -1) PageNumber = PageCnt - 1;

            int nStartNum = PageSize * PageNumber + 1;
            int nEndNum = PageSize * PageNumber + PageSize;

            string dtFilter = string.Format("NUM >= '{0}' AND NUM <= '{1}'", nStartNum, nEndNum);
            string dtSort = "NUM ASC";

            DataTable pageDt = Dt.Clone();

            if (Dt.Rows.Count > 0)
            {
                pageDt = Dt.Select(dtFilter, dtSort).CopyToDataTable();
            }

            DataView dv = new DataView(pageDt);
            pgitems.DataSource = dv;

            repeater.DataSource = pgitems;
            repeater.DataBind();

            //updatePanel.Update();
        }

        protected void btnFirstPage_Click(object sender, EventArgs e)
        {
            bPreRender = true;
            PageNumber = 0;
            SetPagingDataList();
        }

        protected void btnPrevPage_Click(object sender, EventArgs e)
        {
            bPreRender = true;
            PageNumber = (Convert.ToInt32(PageNumber) - (Convert.ToInt32(PageNumber) % 10 + 1));
            SetPagingDataList();
        }

        protected void btnNextPage_Click(object sender, EventArgs e)
        {
            bPreRender = true;
            PageNumber = ((Convert.ToInt32(PageNumber) / 10) + 1) * 10;
            SetPagingDataList();
        }

        protected void btnEndPage_Click(object sender, EventArgs e)
        {
            bPreRender = true;
            PageNumber = -1;
            SetPagingDataList();
        }

        protected void btnPage_ServerClick(object sender, EventArgs e)
        {
            bPreRender = true;
            HtmlAnchor btnNum = (HtmlAnchor)sender;
            PageNumber = Convert.ToInt32(btnNum.InnerText) - 1;

            SetPagingDataList();
        }
    }
}