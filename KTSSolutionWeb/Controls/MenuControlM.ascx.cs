﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using System.Web;
using System.Text;

namespace KTSSolutionWeb.Controls
{
    public partial class MenuControlM : ControlBase
    {
        List<KtsMobileMenuItem> menuitems { get; set; }

        public string getTitleId()
        {
            return lblContentTitle.ClientID;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        
        public void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    SetMenuData();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        #region SetMenuData
        /// <summary>
        /// SetMenuData
        /// </summary>
        public void SetMenuData()
        {
            DataSet ds = null;

            try
            {
                string resultSession = string.Empty;
                string rptCd = "MOBILEMENULIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables.Count > 0)
                    {
                        menuitems = new List<KtsMobileMenuItem>();

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            int idx = 0;
                            int order = 0;

                            KtsMobileMenuItem item = new KtsMobileMenuItem(row);

                            if (!item.MENULEVEL.Equals("1"))
                            {
                                idx = menuitems.FindIndex(t => t.MENUID == item.UPPERMENUID);
                                order = menuitems.FindLastIndex(t => t.UPPERMENUID == item.UPPERMENUID);

                                if (order > -1)
                                {
                                    menuitems.Insert(order + 1, item);
                                }
                                else
                                {
                                    menuitems.Insert(idx + 1, item);
                                }
                            }
                            else
                            {
                                menuitems.Add(item);
                            }
                        }

                        StringBuilder strMenu = new StringBuilder();

                        int i = 0;

                        string strUrl = Request.Url.PathAndQuery;
                        string preMenuLevel = "";
                        string preUpperMenuId = "";

                        strMenu.AppendLine("<ul>");

                        strMenu.AppendLine("<li class='parent-li' style='padding-bottom:15px;'><div style='width:100%' onclick='window.location.href=\"/MobileMain\"'>HOME</div></li>");

                        foreach (KtsMobileMenuItem item in menuitems)
                        {
                            string strColor = "";
                            string strBgColor = "background-color:#ddfafa;";

                            if (i != 0 && !item.UPPERMENUID.Equals(preUpperMenuId) && preMenuLevel.Equals("2"))
                            {
                                strMenu.AppendLine("</ul>");
                                strMenu.AppendLine("</li>");
                            }

                            if (strUrl.ToUpper().Equals(item.URL.ToUpper()))
                            {
                                strColor = "color:red;";
                                strBgColor = "background-color:#2CBBB7;";

                                lblContentTitle.InnerText = item.TITLE;
                            }

                            if (item.MENULEVEL.Equals("1"))
                            {
                                if (item.LEAFMENUYN.Equals("Y"))
                                {
                                    string strPageUpperMenu = menuitems.Find(t => t.URL.ToUpper() == strUrl.ToUpper()).UPPERMENUID;
                                    string strCssOn = "";

                                    if (strPageUpperMenu.Equals(item.MENUID))
                                    {
                                        strCssOn = " on";
                                    }

                                    strMenu.AppendLine("<li class='parent-li'><div style='width:100%' onclick='fn_childMenu_open(\"" + item.MENUID + "\");'>" + item.MENUNM + "<div id='submenu_" + item.MENUID + "' class='submenu" + strCssOn + "'></div></div>");
                                    strMenu.AppendLine("<div style='height:15px'></div>");
                                    strMenu.AppendLine("<ul id='ul_" + item.MENUID + "' class='child-menu" + strCssOn + "'>");
                                }
                                else
                                {
                                    strMenu.AppendLine("<li class='parent-li' style='" + strColor + " padding-bottom:15px;'><div style='width:100%' onclick='window.location.href=\"" + item.URL + "\"'>" + item.MENUNM + "</div></li>");
                                }

                                preMenuLevel = item.MENULEVEL;
                                preUpperMenuId = item.MENUID;
                            }
                            else
                            {
                                strMenu.AppendLine("<li style='font-size:14px; font-weight:bold; " + strColor + " border:0; " + strBgColor + " padding:6px;'><div style='width:100%' onclick='window.location.href=\"" + item.URL + "\"'>" + item.MENUNM + "</div></li>");

                                preMenuLevel = item.MENULEVEL;
                                preUpperMenuId = item.UPPERMENUID;
                            }

                            i++;
                        }

                        strMenu.AppendLine("</ul>");

                        mobile_menu.InnerHtml += strMenu;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PageInit", "alert('오류가 발생했습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion
    }
}