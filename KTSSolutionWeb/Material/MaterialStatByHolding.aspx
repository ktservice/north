﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaterialStatByHolding.aspx.cs" Inherits="KTSSolutionWeb.MaterialStatByHold" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selClass1").change(function (e) {
                    fn_Class(this, 2, 'selClass2');
                });

                $("#selClass2").change(function (e) {
                    fn_Class(this, 3, 'selClass3');
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "자재 보유 현황.xlsx", { sheetName: "자재 보유 현황" });
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            fn_BCnt();
        }

        var fn_button = function (cell, formatterParams, onRendered) { //plain text value
            var html = '';

            cellNo = cell.getRow().getData().id;

            if (cell.getRow().getData().GUBUN == "Y") {
                html = "<input id='btnCellOrgCd' type='button' onclick='fn_PopupOrgCd(\"" + cellNo + "\");' value='창고선택' class='btn-gray' style='height:23px;line-height:0px;font-size:15px'>";
            }
            else if (cell.getRow().getData().GUBUN == "T") {
                html = "<input id='btnCellOrgCd' type='button' onclick='fn_PopupOrgCd(\"" + cellNo + "\");' value='창고변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                html += "&nbsp;<input id='btnCellOk' type='button' onclick='fn_CellOk(\"" + cellNo + "\");' value='이관' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
            }
            else if (cell.getRow().getData().GUBUN == "N") {
                html = "";
            }

            return html;
        };


        var fn_CellOk = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            if (confirm("이관을 하시겠습니까?")) {

                
                //같은 부서
                if (row.getData().CD == row.getData().TOCD) {
                    ktsGridAlert("같은 창고로 이관 할 수 없습니다");
                    return false;
                }
                else if (rowData.TOCNT == '' || rowData.TOCNT == '0') {
                    ktsGridAlert("이관대상수량을 입력하세요.");
                    return false;
                }
                else {
                    var toCnt = rowData.TOCNT;
                    var Cnt = rowData.CNT;

                    if (Cnt < 1) {
                        ktsGridAlert("이관할 수 없는 보유수량입니다.");
                        return false;
                    }
                    else if (Cnt < toCnt) {
                        ktsGridAlert("보유수량보다 이관대상수량이 커서 이관 할 수 없습니다.");
                        return false;
                    }
                    else {
                        //입력 파라미터
                        var aData = [];
                        aData[0] = rowData.CD;
                        aData[1] = rowData.NM;
                        aData[2] = rowData.TOCD;
                        aData[3] = rowData.TONM;
                        aData[4] = rowData.ITEMCD;
                        aData[5] = rowData.ITEMNM;
                        aData[6] = rowData.TOCNT;

                        var jsonData = JSON.stringify({ aData: aData });
                        var ajaxUrl = ajaxCallGetUrl(location.href, 'SetInTransaction');

                        cellNo = id;
                        callGb = 'inTranOk';
                        ajaxCallEx(jsonData, ajaxUrl, true);
                    }
                }
            }
        }

        var fn_CellTransInfoPopup1 = function (e, cell) {
            var value = cell.getValue();
            var cd = cell.getRow().getData().CD;
            var itemCd = cell.getRow().getData().ITEMCD;

            if (value != "0") {
                fn_TransInfoPopup('2', cd, itemCd);
            }

            return '';
        }

        var fn_CellTransInfoPopup2 = function (e, cell) {
            var value = cell.getValue();
            var cd = cell.getRow().getData().CD;
            var itemCd = cell.getRow().getData().ITEMCD;

            if (value != "0") {
                fn_TransInfoPopup('1', cd, itemCd);
            }

            return '';
        }

        var fn_CellPopup = function (e, cell) {

            callGb = 'userOrgLv';
            cellLeft = cell.getElement().getBoundingClientRect().left;
            cellTop = window.pageYOffset + cell.getElement().getBoundingClientRect().top - $("#ktsGrid").position().top;

            var data = cell.getRow().getData().HOLDING_WORKER;

            if (data != '') {
                //입력 파라미터
                var aData = [];
                aData[0] = data;
                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUserOrgLv');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

            return '';
        }

        var fn_CellColor = function (cell, formatterParams, onRendered) {
            var value = cell.getRow().getData().TONM;

            if (value == null) value = '';

            if (value != '') {
                cell.getElement().style.backgroundColor = "#077078";
                cell.getElement().style.color = "#fff";
            }
            else {
                cell.getElement().style.backgroundColor = "#fff";
                cell.getElement().style.color = "#333";
            }

            return value;
        }

        var fn_CellColor2 = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            cell.getElement().style.backgroundColor = "#f1f1f1";
            cell.getElement().style.fontWeight = "500";
            value = priceToString(value);
            return value;
        }

        var fn_CellBoCnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            cell.getElement().style.fontWeight = "500";
            value = priceToString(value);
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
                cell.getElement().style.fontWeight = "bold";
            }
            return value;
        }

        var inputEditor = function (cell, onRendered, success, cancel) {
            var value = cell.getValue();
            var input = document.createElement("input");

            if (value == '0') value = '';

            input.setAttribute("type", "text");
            input.placeholder = "이관 수량 입력";
            input.style.padding = "3px";
            input.style.width = "100%";
            input.style.boxSizing = "border-box";
            input.style.fontWeight = "500";
            input.maxLength = 11;
            input.value = value;

            onRendered(function () {
                input.focus();
                input.style.height = "100%";
            });


            function onSuccess() {
                if (input.value == '') input.value = '0';
                success(input.value);
            }

            //submit new value on blur or change
            input.addEventListener("blur", onSuccess);

            //submit new value on enter
            input.addEventListener("keydown", function (e) {
                if (e.keyCode >= 48 && e.keyCode <= 57 || (event.keyCode >= 96 && event.keyCode <= 105)
                    || (event.keyCode == 8) //백 스페이스
                ) { //숫자키만 입력
                    return input.value;
                } else {
                    cancel();
                }
            });

            return input;
        };

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen:true },
                { headerHozAlign: "center", hozAlign: "center", title: "창고", field: "NM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 200, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "SERVICECTGLVNM2", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류", field: "SERVICECTGLVNM3", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "right", title: "보유수량", field: "CNT", sorter: "number", width: 100, formatter: fn_CellBoCnt },
                { headerHozAlign: "center", hozAlign: "left", title: "창고코드", field: "CD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "품목", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "이관받은건수", field: "FROMICNT", sorter: "number", width: 110, formatter: fn_CellICnt, cellClick: fn_CellTransInfoPopup1 },
                { headerHozAlign: "center", hozAlign: "right", title: "이관받은수량", field: "FROMOKCNT", sorter: "number", width: 110, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "이관보낸건수", field: "TOICNT", sorter: "number", width: 110, formatter: fn_CellICnt, cellClick: fn_CellTransInfoPopup2 },
                { headerHozAlign: "center", hozAlign: "right", title: "이관보낸수량", field: "TOOKCNT", sorter: "number", width: 110, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "이관대상수량", field: "TOCNT", sorter: "number", width: 110, editor: inputEditor, formatter: fn_CellColor2 },
                { headerHozAlign: "center", hozAlign: "right", title: "원본보유수량", field: "OCNT", sorter: "number", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이관창고", field: "TOCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이관창고", field: "TONM", width: 200, sorter: "string", formatter: fn_CellColor},
                { headerHozAlign: "center", hozAlign: "center", title: "이관신청", field: "GUBUN", width: 170, sorter: "string", formatter: fn_button, tooltip: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            callGb = 'selClass1';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

            ajaxCallEx(jsonData, ajaxUrl, true);

            
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'transCheck') {
                var chk = 'N';

                $.each(JSON.parse(json), function (idx, item) {
                    chk = item.CHK;
                });

                if (chk == 'Y') {
                    fn_TransInfoPopup('2', '', '');
                }

                callGb = '';
            }
            else if (callGb == 'selClass1' || callGb == 'selClass2' || callGb == 'selClass3') {
                obj = $('#' + callGb);

                obj.empty();
                obj.append("<option value='전체'>전체</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });

                if (callGb == 'selClass1') {
                    callGb = '';
                    fn_TransCheck();
                }
                else {
                    callGb = '';
                }
            }
            else if (callGb == 'inTranOk') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    //이관 완료
                    if (item.SERVICE_ERROR_CD == null) {
                        var cnt = item.CNT;
                        var nm = item.NM;

                        fn_InTranOk(cnt, nm);
                    }
                });
            }
            else if (callGb == 'userOrgLv') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ORGNM != null) {
                        $(".tabulator-popup-container").text(item.ORGNM);
                    }
                });
            }
            else if (callGb == 'transResult') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.CD != null) {
                        var pCD = item.CD;
                        var pNM = item.NM;
                        var pTOCD = item.TOCD;
                        var pTONM = item.TONM;
                        var pITEMCD = item.ITEMCD;
                        var pITEMNM = item.ITEMNM;
                        var pUNIT = item.UNIT;
                        var pCNT = item.CNT;
                        var pOCNT = item.OCNT;
                        var pFROMOKCNT = item.FROMOKCNT;
                        var pFROMICNT = item.FROMICNT;
                        var pTOOKCNT = item.TOOKCNT;
                        var pTOCNT = item.TOCNT;
                        var pTOICNT = item.TOICNT;
                        fn_InGridChange(pCD, pNM, pTOCD, pTONM, pITEMCD, pITEMNM, pUNIT, pCNT, pOCNT, pFROMOKCNT, pFROMICNT, pTOOKCNT, pTOCNT, pTOICNT);
                    }
                });
            }
        }

        var fn_InGridChange = function (pCD, pNM, pTOCD, pTONM, pITEMCD, pITEMNM, pUNIT, pCNT, pOCNT, pFROMOKCNT, pFROMICNT, pTOOKCNT, pTOCNT, pTOICNT) {
            //이관보낸쪽
            ktsGrid.updateData([{
                id: cellNo, CNT: pCNT, FROMICNT: pFROMICNT, FROMOKCNT: pFROMOKCNT, TOICNT: pTOICNT, TOOKCNT: pTOOKCNT, TOCNT: '0', OCNT: pOCNT, TOCD: '', TONM: '', GUBUN: "Y"
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            var row = ktsGrid.getRow(cellNo);
            row.select();

            var rowData;
            var gridCnt = ktsGrid.getDataCount();
            var position;
            var tempId = '';
            var tempFromICnt = '0';
            var tempFromCnt = '0';
            var tempCnt = '0';

            for (var i = 0; i < gridCnt; i++) {
                position = ktsGrid.getRowPosition(i, true);

                if (position != false) {
                    row = ktsGrid.getRow(position);
                    rowData = row.getData();

                    if (rowData.CD == pTOCD && rowData.ITEMCD == pITEMCD) {
                        tempId = rowData.id;
                        tempFromICnt = rowData.FROMICNT;
                        tempFromCnt = rowData.FROMOKCNT;
                        tempCnt = rowData.CNT;
                        break;
                    }
                }
            }

            if (tempId != '') {
                tempFromICnt = parseInt(tempFromICnt) + 1;
                tempFromCnt = parseInt(tempFromCnt) + parseInt(pFROMOKCNT);
                tempCnt = parseInt(tempCnt) + parseInt(pFROMOKCNT);

                //이관받은쪽
                ktsGrid.updateData([{
                    id: tempId, CNT: tempCnt, FROMICNT: tempFromICnt, FROMOKCNT: tempFromCnt, OCNT: tempCnt
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

                row = ktsGrid.getRow(tempId);
                row.select();
            }

            ktsGridRowCount(ktsGrid.getDataCount());

        }

        var fn_InTranOk = function (cnt, nm) {
            ktsGridAlert(nm + "으로 이관이 완료됐습니다.");

            var row = ktsGrid.getRow(cellNo);
            var rowData = row.getData();
            var pCD = rowData.CD;
            var pITEMCD = rowData.ITEMCD;
            var pTOCD = rowData.TOCD;
            var pTONM = rowData.TONM;
            
            fn_TransResult(pCD, pITEMCD, pTOCD, pTONM);
        }

        var fn_TransInfoPopup = function (gubun, cd, itemCd) {
            var form = "/Material/MaterialStatByHoldingPopup";
            var target = "MaterialStatByHoldingPopup";
            var param = {
                pGUBUN: gubun,
                pCD: cd,
                pITEMCD: itemCd
            };
            var nWidth = 720;
            var nHeight = 700;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        //조직 찾기 함수
        var fn_PopupOrgCd = function (id) {
            cellNo = id;

            var form = "/Common/SearchOrgInventoryCd";
            var target = "SearchOrgInventoryCd";
            var param = {
                pGUBUN: "1",
                pRTN: "2"
            };
            var nWidth = 720;
            var nHeight = 600;

            /*
             * pGUBUN: "2"
            var nWidth = 400;
            var nHeight = 600;
            */

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        //조직 찾기 리턴 함수명 정해져 있음
        var SetDispOrgCd = function (orgcd, orgnm) {

            ktsGrid.updateData([{
                id: cellNo, TOCD: orgcd, TONM: orgnm, GUBUN: "T"
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);
            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";
            var param = {
                pORGCD: orgcd,
                pEMPNO: empno,
                pMdate: BaseMonth,
                pType: "material"
            };
            var nWidth = 400;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);
            var form = "/Common/OrgTree_BComp";
            var target = "OrgTree_BComp";
            var param = {
                pCHKNODELV: '9',
                pOPER: empno,
                pMdate: BaseMonth,
                pType: "material"
            };
            var nWidth = 400;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var fn_SearchChk = function() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();
            
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[1] = $("#selClass1").val().replace('전체', '');
                aData[2] = $("#selClass2").val().replace('전체', '');
                aData[3] = $("#selClass3").val().replace('전체', '');
                aData[4] = '';
                aData[5] = '';
                aData[6] = '';
                aData[7] = '';
                aData[8] = $('#selBCnt').val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_TransResult = function (pCD, pITEMCD,pTOCD, pTONM) {
            callGb = 'transResult';

            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = pCD;
            aData[5] = pITEMCD;
            aData[6] = pTOCD;
            aData[7] = pTONM;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_TransCheck = function () {
            var gubun = 'C';

            callGb = 'transCheck';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = '';
            aData[2] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var url = location.href;
            url = url.replace(/#/gi, "") + 'Popup';
            var ajaxUrl = ajaxCallGetUrl(url, 'GetTransCheck');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_BCnt = function () {
            obj = $('#selBCnt');

            obj.empty();
            obj.append("<option value='A'>전체</option>");
            obj.append("<option value='Y'>유</option>");
            obj.append("<option value='N'>무</option>");
        }

        var fn_Class = function (obj, lv, gb) {

            callGb = gb;

            if (obj.value == '전체') {
                if (lv == '2') {
                    $("#selClass2").empty();
                    $("#selClass2").append("<option value='전체'>전체</option>");
                }
                else if (lv == '3') {
                    $("#selClass3").empty();
                    $("#selClass3").append("<option value='전체'>전체</option>");
                }
            }
            else {

                if (lv == '2') {
                    $("#selClass3").empty();
                    $("#selClass3").append("<option value='전체'>전체</option>");
                }

                var sVal1 = '';
                var sVal2 = '';
                var sVal3 = '';

                if ($("#selClass1 option:selected").val() != undefined) {
                    sVal1 = $("#selClass1").val();
                }
                if ($("#selClass2 option:selected").val() != undefined) {
                    sVal2 = $("#selClass2").val();
                }
                if ($("#selClass3 option:selected").val() != undefined) {
                    sVal3 = $("#selClass3").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = lv;
                aData[1] = '';
                aData[2] = sVal1;
                aData[3] = sVal2;
                aData[4] = sVal3;
                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

        }


    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox">
                    <label>대분류</label>
                    <select id="selClass1">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>중분류</label>
                    <select id="selClass2">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>소분류</label>
                    <select id="selClass3">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>보유수량</label>
                    <select id="selBCnt" style="width:80px">
                        <option selected="selected" value="A">전체</option>
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>자재 보유 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="NM">창고</option>
                            <option value="ITEMNM">품목</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
