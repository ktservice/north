﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TerminalInfoDetailPopup.aspx.cs" Inherits="KTSSolutionWeb.TerminalInfoDetailPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>보유단말 상세현황</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENAMESCODE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GROUPLV1NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류", field: "GROUPLV2NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "소분류", field: "GROUPLV3NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "K9", field: "K9", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "바코드", field: "BARCODE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "유형", field: "CATEGORY", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사용횟수", field: "USECNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "제조일", field: "PRODDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단말상태", field: "TERMINALSTATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단말 작업사유", field: "WORKCAUSE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "MAC주소", field: "MACADDR", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "자산번호", field: "ASSETNUM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "최초지사입고일", field: "FIRSTSTOREDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "STORECLASS", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사입고날짜", field: "STOREDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사보유기간(일)", field: "POSSESSIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "Engineer보유기간(일)", field: "ENGINEER_POSSESSIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사창고보유기간(일)", field: "STORE_POSSESSIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "현위치", field: "LOCATION", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "현위치입고날짜", field: "LOCATIONDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "현위치보유기간(일)", field: "LOCATIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사입고전상태", field: "BEFORESTATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "Engineer보유기간분출횟수", field: "CARRYOUTCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "최종보유작업자소속", field: "LASTORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "최종보유작업자ID", field: "LASTWORKERID", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "보유작업자명", field: "LASTWORKERNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "소속구분", field: "KTSTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단말구매여부", field: "PURCHASEYN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "회수여부", field: "RETRIEVEYN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "재활용여부", field: "RECYCLEYN", sorter: "string" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("76.5vh");
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_Search = function () {

            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hdnBaseDate.ClientID %>").val();
            aData[1] = $("#<%= hdnGroupLv1Nm.ClientID %>").val();
            aData[2] = $("#<%= hdnIdmsEmpNo.ClientID %>").val();
            aData[3] = $("#<%= hdnHoldingYn.ClientID %>").val();

            if (aData[3] == "Y") {
                $("#lblTitle").text("30일이상 상세 보유현황");
            }
            else {
                $("#lblTitle").text("전체단말 상세 보유현황");
            }

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">30일이상 상세 보유현황</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
				                <div class="btnset">
                                    <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <input type="hidden" id="hdnBaseDate" runat="server" />
        <input type="hidden" id="hdnGroupLv1Nm" runat="server" />
        <input type="hidden" id="hdnIdmsEmpNo" runat="server" />
        <input type="hidden" id="hdnHoldingYn" runat="server" />
    </form>
</body>
</html>
