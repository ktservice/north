﻿using System;
using System.Data;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Matrials;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class TerminalSendMMSResult : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPageInit();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }


        private void SetOrgList(string strEmpNo, string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList(strEmpNo, strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetPageInit()
        {
            try
            {
                this.txbBaseDt.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                string strMonth = this.txbBaseDt.Text.Replace("-", "").Substring(0, 6);

                this.hfOrgCd.Value = Session["ORGCD"].ToString();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");

                    SetOrgList("", strMonth);
                }
                else
                {
                    SetOrgList(this.Session["EMPNO"].ToString(), strMonth);

                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;
                }

                this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbEmpUserNo.Text = "";
                this.txbEmpUserNm.Text = "";

                this.ddlSendResult.SelectedValue = "";

                updPanelOrgCd.Update();
                updPanelEmp.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;


                string strOrgCd = this.hfOrgCd.Value.ToString();
                string strBaseDate = Request.Form[this.txbBaseDt.UniqueID].ToString();
                string strEmpNo = Request.Form[this.txbEmpUserNo.UniqueID].ToString();
                string strResult = this.ddlSendResult.SelectedValue;

                string strTargetDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");

                using (TerminalMgmt mgmt = new TerminalMgmt())
                {
                    ds = mgmt.GetTerminalSendMMSResult(strOrgCd, strBaseDate, strEmpNo, strResult);
                }

                string strDisplay = "N";

                if (strBaseDate.Equals(strTargetDate))
                {
                    strDisplay = "Y";
                }
                else
                {
                    strDisplay = "N";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "btnVisible", "btnDisplay('" + strDisplay + "');", true);

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        private bool ValidationCheck(string strBaseDate, string strIdmsEmpno, string strEmpNo, string strEmpNm, string strMobile, ref string strMsg)
        {
            bool bCheck = true;

            if (strBaseDate.Length == 0 || strIdmsEmpno.Length == 0)
            {
                strMsg = "잘못된 접근입니다.";
                bCheck = false;
            }
            else if (strEmpNo.Length == 0 || strEmpNm.Length == 0 || strMobile.Length == 0)
            {
                strMsg = "단말 보유자 정보를 모두 입력해주세요.";
                bCheck = false;
            }            

            return bCheck;
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[10];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "BASEDATE";
            ArrHeader[2] = "ORGNM";
            ArrHeader[3] = "IDMSEMPNO";
            ArrHeader[4] = "EMPNO";
            ArrHeader[5] = "EMPNM";
            ArrHeader[6] = "MOBILE";
            ArrHeader[7] = "TOTALCNT";
            ArrHeader[8] = "DAYOVER30CNT";
            ArrHeader[9] = "SMSSENDRESULT";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[10];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "기준일자";
            ArrHeader[2] = "조직";
            ArrHeader[3] = "IDMS사번";
            ArrHeader[4] = "사번";
            ArrHeader[5] = "이름";
            ArrHeader[6] = "휴대폰번호";
            ArrHeader[7] = "보유현황";
            ArrHeader[8] = "30일이상보유현황";
            ArrHeader[9] = "SMS발송결과";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    string strOrgCd = this.hfOrgCd.Value.ToString(); ;
                    string strBaseDate = Request.Form[this.txbBaseDt.UniqueID].ToString();
                    string strEmpNo = Request.Form[this.txbEmpUserNo.UniqueID].ToString();
                    string strResult = this.ddlSendResult.SelectedValue;

                    using (TerminalMgmt mgmt = new TerminalMgmt())
                    {
                        ds = mgmt.GetTerminalSendMMSResult(strOrgCd, strBaseDate, strEmpNo, strResult);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "장기 미사용단말 MMS발송 결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                        string strBaseDate = Request.Form[this.txbBaseDt.UniqueID].ToString();
                        string strTargetDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                        HtmlTableCell thSave = (HtmlTableCell)rptResult.Parent.FindControl("thSave");

                        if (strBaseDate.Equals(strTargetDate))
                        {
                            thSave.Visible = true;
                        }
                        else
                        {
                            thSave.Visible = false;
                        }

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblBaseDate = (Label)rptResult.Items[i].FindControl("lblBaseDate");
                            Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");
                            TextBox txbEmpNo = (TextBox)rptResult.Items[i].FindControl("txbEmpNo");
                            Label lblEmpNm = (Label)rptResult.Items[i].FindControl("lblEmpNm");
                            TextBox txbEmpNm = (TextBox)rptResult.Items[i].FindControl("txbEmpNm");
                            Label lblMobile = (Label)rptResult.Items[i].FindControl("lblMobile");
                            TextBox txbMobile = (TextBox)rptResult.Items[i].FindControl("txbMobile");
                            Label lblSmsSendResult = (Label)rptResult.Items[i].FindControl("lblSmsSendResult");

                            HtmlTableCell tdSave = (HtmlTableCell)rptResult.Items[i].FindControl("tdSave");
                            Button btnSave = (Button)rptResult.Items[i].FindControl("btnSave");
                            Button btnSend = (Button)rptResult.Items[i].FindControl("btnSend");

                            if (lblSmsSendResult.Text.Equals("SUCC"))
                            {
                                lblEmpNo.Visible = true;
                                txbEmpNo.Visible = false;
                                lblEmpNm.Visible = true;
                                txbEmpNm.Visible = false;
                                lblMobile.Visible = true;
                                txbMobile.Visible = false;
                                btnSave.Visible = false;
                                btnSend.Visible = false;
                            }
                            else
                            {

                                if (strTargetDate.Equals(lblBaseDate.Text.ToString()))
                                {
                                    tdSave.Visible = true;

                                    if (lblSmsSendResult.Text.Equals("NOEMPINFO"))
                                    {
                                        lblEmpNo.Visible = false;
                                        txbEmpNo.Visible = true;
                                        lblEmpNm.Visible = false;
                                        txbEmpNm.Visible = true;
                                        lblMobile.Visible = false;
                                        txbMobile.Visible = true;

                                        btnSave.Visible = true;
                                        btnSend.Visible = false;
                                    }
                                    else if (lblSmsSendResult.Text.Equals("FAIL"))
                                    {
                                        lblEmpNo.Visible = false;
                                        txbEmpNo.Visible = true;
                                        lblEmpNm.Visible = false;
                                        txbEmpNm.Visible = true;
                                        lblMobile.Visible = false;
                                        txbMobile.Visible = true;

                                        btnSave.Visible = true;
                                        btnSend.Visible = true;
                                    }
                                    else
                                    {
                                        lblEmpNo.Visible = true;
                                        txbEmpNo.Visible = false;
                                        lblEmpNm.Visible = true;
                                        txbEmpNm.Visible = false;
                                        lblMobile.Visible = true;
                                        txbMobile.Visible = false;

                                        btnSave.Visible = false;
                                        btnSend.Visible = true;
                                    }
                                }
                                else
                                {
                                    tdSave.Visible = false;
                                    btnSave.Visible = false;
                                    btnSend.Visible = false;
                                }
                            }
                        }
                    }

                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfBaseDt_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = Request.Form[this.txbBaseDt.UniqueID].ToString().Replace("-", "").Substring(0, 6);

                if (!strMonth.Equals(""))
                {
                    if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                    {
                        SetOrgList("", strMonth);
                    }
                    else
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    using (KTSUser user = new KTSUser())
                    {
                        ds = user.GetUserViewOrgMonth2(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strOrgCd = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";

                            strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        }

                        this.hfOrgCd.Value = strOrgCd;
                        this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                    }
                    else
                    {
                        this.hfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;
                Label lblBaseDate = (Label)btnSave.Parent.FindControl("lblBaseDate");
                Label lblIdmsEmpNo = (Label)btnSave.Parent.FindControl("lblIdmsEmpNo");
                TextBox txbEmpNo = (TextBox)btnSave.Parent.FindControl("txbEmpNo");
                TextBox txbEmpNm = (TextBox)btnSave.Parent.FindControl("txbEmpNm");
                TextBox txbMobile = (TextBox)btnSave.Parent.FindControl("txbMobile");

                string strMsg = "";

                if (!ValidationCheck(lblBaseDate.Text, lblIdmsEmpNo.Text, txbEmpNo.Text, txbEmpNm.Text, txbMobile.Text, ref strMsg))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strMsg + "')", true);
                }
                else
                {
                    using (TerminalMgmt mgmt = new TerminalMgmt())
                    {
                        mgmt.SetTerminalIdmsUserInfo(lblBaseDate.Text, lblIdmsEmpNo.Text, txbEmpNo.Text, txbEmpNm.Text, txbMobile.Text);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('단말보유자 정보가 수정되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 정보 저장 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnSend_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnSend = (Button)sender;
                Label lblBaseDate = (Label)btnSend.Parent.FindControl("lblBaseDate");
                Label lblIdmsEmpNo = (Label)btnSend.Parent.FindControl("lblIdmsEmpNo");
                Label lblEmpNo = (Label)btnSend.Parent.FindControl("lblEmpNo");
                TextBox txbEmpNo = (TextBox)btnSend.Parent.FindControl("txbEmpNo");
                Label lblEmpNm = (Label)btnSend.Parent.FindControl("lblEmpNm");
                TextBox txbEmpNm = (TextBox)btnSend.Parent.FindControl("txbEmpNm");
                Label lblMobile = (Label)btnSend.Parent.FindControl("lblMobile");
                TextBox txbMobile = (TextBox)btnSend.Parent.FindControl("txbMobile");
                Label lblGroupLv1Nm = (Label)btnSend.Parent.FindControl("lblGroupLv1Nm");
                Label lblDayOver30Cnt = (Label)btnSend.Parent.FindControl("lblDayOver30Cnt");
                Label lblSmsSendResult = (Label)btnSend.Parent.FindControl("lblSmsSendResult");
                Label lblSmsMsg = (Label)btnSend.Parent.FindControl("lblSmsMsg");

                string strEmpNo = "";
                string strEmpNm = "";
                string strMobile = "";

                if (lblSmsSendResult.Text.Equals("NOEMPINFO") || lblSmsSendResult.Text.Equals("FAIL"))
                {
                    strEmpNo = txbEmpNo.Text;
                    strEmpNm = txbEmpNm.Text;
                    strMobile = txbMobile.Text;
                }
                else
                {
                    strEmpNo = lblEmpNo.Text;
                    strEmpNm = lblEmpNm.Text;
                    strMobile = lblMobile.Text;
                }

                string strMsg = "";

                if (!ValidationCheck(lblBaseDate.Text, lblIdmsEmpNo.Text, strEmpNo, strEmpNm, strMobile, ref strMsg))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('" + strMsg + "')", true);
                }
                else
                {
                    using (TerminalMgmt mgmt = new TerminalMgmt())
                    {
                        mgmt.SetTerminalInfoSendMMS(lblBaseDate.Text, lblIdmsEmpNo.Text, strEmpNo, strEmpNm, strMobile, lblSmsMsg.Text);
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('장기 미사용 단말 반납 안내 메세지를 발송했습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 정보 저장 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnMMSReSend_Click(object sender, EventArgs e)
        {
            try
            {
                string strBaseDate = Request.Form[this.txbBaseDt.UniqueID].ToString();
                string strTargetDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");

                if (!strBaseDate.Equals(strTargetDate))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "btnDisplay('N');alert('잘못된 접근입니다.')", true);
                }
                else
                {
                    using (TerminalMgmt mgmt = new TerminalMgmt())
                    {
                        mgmt.SetTerminalInfoReSendMMS();
                    }

                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('인사정보에 등록된 휴대번호로 장기 미사용 단말 반납 안내 메세지를 전체 재발송했습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 정보 저장 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                SetPageInit();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}