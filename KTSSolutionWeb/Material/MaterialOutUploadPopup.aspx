﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MaterialOutUploadPopup.aspx.cs" Inherits="KTSSolutionWeb.MaterialOutUploadPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>자재불출일괄등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var page = {
                init: function () {
                    fn_Init();
                },
                eventbind: function () {
                    $("#btnTemp").click(function (e) {
                        fn_DownloadTemplate();
                    });

                    $("#btnCode").click(function (e) {
                        fn_PopupCode();
                    });

                    $("#btnClose").click(function (e) {
                        SendParentsForm();
                    });
                }
            };

        });

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_PopupCode = function () {
            var form = "/Oper/MaterialCodePopup";
            var target = "MaterialCodePopup";
            var param = {
            };

            var nWidth = 900;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_DownloadTemplate = function () {
            window.location.href = '/Template/자재 불출 Excel Upload Template.xlsx';
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">자재불출일괄등록</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <label class="searchrowcount"></label>
                            <div class="pull-right">
                                <div class="btnset">
                                    <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" type="button" style="float: right;margin: 0px 10px" title="자료등록" class="btn-green last">자료등록</button>
                                    <input id="btnTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="양식다운로드" />
                                    <input id="btnCode" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="자재코드정보" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div class="upload-list" style="vertical-align:text-bottom;margin: 50px 0 0 0;">
                            <ul>
                                <li>
                                    <strong style="padding:10px 0px;float:left;margin-left:32px">파일</strong>
                                    <div class="file-field">
                                        <p class="filebox">
                                            <input type="text" id="txtfuOut" style="width:600px" />
                                            <span class="file">
                                                <input type="file" id="fuOut" name="fuOut" onchange="document.getElementById('txtfuOut').value=this.value;" />
                                                <label for="fuOut">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
                                            </span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                         </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUpload" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin: 120px 30px 0 30px; display: grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
