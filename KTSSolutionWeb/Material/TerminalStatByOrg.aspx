﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TerminalStatByOrg.aspx.cs" Inherits="KTSSolutionWeb.TerminalStatByOrg" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">

        function PopupOrgTree(orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 720;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: BaseMonth
                };

                var Popupform = createForm("/Common/OrgTree_Terminal", param);

                Popupform.target = "OrgTree_Terminal";
                var win = window.open("", "OrgTree_Terminal", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SetOrgCode(orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        function SearchChk() {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0 ) {
                alert("조회할 조직을 선택해 주세요.");
                return false;
            }else {
                return true;
            }
        }

        $(document).ready(function () {
            $("#<%= txbBaseDt.ClientID %>").change(function () {
                $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbBaseDt.ClientID %>").val());
                __doPostBack("<%=hfBaseDt.ClientID %>", "");
            });

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GROUPLV1NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "보유 현황", field: "TOTALCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "30일이상 보유현황", field: "DAYOVER30CNT", sorter: "number" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            var grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

            /*----검색 조건 ------- */

            $("#inputValue").keyup(function (e) {
                var valueEl = document.getElementById("inputValue");
                ktsGrid.setFilter('GROUPLV1NM', 'like', valueEl.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {

                if (SearchChk()) {

                    //입력 파라미터
                    var aData = [];
                    aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                    aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetData');

                    //grid Bind
                    grid.getDataBind(jsonData, ajaxUrl);

                    $(".table-search").css('display', 'block');
                }
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "조직별 단말 현황.xlsx", { sheetName: "조직별 단말 현황" });
            });

            $(".table-search").css('display', 'none');
        });
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox">
					<label>기준일자</label>
                    <asp:TextBox ID="txbBaseDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조직별 단말 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <label for="inputValue">대분류 : </label>
                        <input id="inputValue" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
