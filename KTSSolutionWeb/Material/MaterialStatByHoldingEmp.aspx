﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaterialStatByHoldingEmp.aspx.cs" Inherits="KTSSolutionWeb.MaterialStatByHoldingEmp" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "개인 보유 현황.xlsx", { sheetName: "개인 보유 현황" });
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGNM", sorter: "string", frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                //{ headerHozAlign: "center", hozAlign: "right", title: "UTP 옥외 1P", field: "UTPO1P", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "UTP 옥외 2P", field: "UTPO2P", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "UTP 옥외 4P", field: "UTPO4P", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "UTP 옥외 기타", field: "UTPONP", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "UTP 옥내 2P", field: "UTPI2P", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "UTP 옥내 4P", field: "UTPI4P", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "UTP 옥내 6P", field: "UTPI6P", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "UTP 옥내 기타", field: "UTPINP", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "광옥외선", field: "FIBN", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "광케이블 편단", field: "FIBS", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "광케이블 양단", field: "FIBD", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "광 커넥터", field: "FIBCON", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "광 커넥터 접속함", field: "FIBBOX", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "동축케이블", field: "COXCBL", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "동축케이블(창틀형)", field: "COXWIN", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "분배기", field: "LINDIV", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "라인증폭기", field: "LINAMP", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "동축커넥터", field: "COXCON", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "Sky 안테나", field: "SKYANT", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "인입클램프", field: "INNCLP", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "HDMI 케이블", field: "HDMCBL", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "컴포넌트 케이블", field: "CMPCBL", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "UTP 패치케이블", field: "UTPPAT", sorter: "number", formatter: fn_CellNumber },
                //{ headerHozAlign: "center", hozAlign: "right", title: "평면UTP 케이블", field: "UTPFLT", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "몰딩", field: "MOL", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "배선정리함", field: "LINBOX", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "랜카드", field: "LANCRD", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "공유기(유/무선)", field: "LANSHA", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "스위치(허브)", field: "LANSWI", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "PoE", field: "LANPOE", sorter: "number", formatter: fn_CellNumber }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);
            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";
            var param = {
                pORGCD: orgcd,
                pEMPNO: empno,
                pMdate: BaseMonth,
                pType: "material"
            };
            var nWidth = 400;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);
            var form = "/Common/OrgTree_BComp";
            var target = "OrgTree_BComp";
            var param = {
                pCHKNODELV: '9',
                pOPER: empno,
                pMdate: BaseMonth,
                pType: "material"
            };
            var nWidth = 400;
            var nHeight = 720;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var fn_SearchChk = function() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {
                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                 <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>개인 보유 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM">부서</option>
                            <option value="EMPNO">사번</option>
                            <option value="EMPNM">이름</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
