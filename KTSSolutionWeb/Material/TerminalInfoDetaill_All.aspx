﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TerminalInfoDetaill_All.aspx.cs" Inherits="KTSSolutionWeb.TerminalInfoDetaill_All" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">

        function PopupOrgTree(orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 720;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: BaseMonth
                };

                var Popupform = createForm("/Common/OrgTree_Terminal", param);

                Popupform.target = "OrgTree_Terminal";
                var win = window.open("", "OrgTree_Terminal", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SetOrgCode(orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        function SearchChk() {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                alert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        $(document).ready(function () {
            $("#<%= txbBaseDt.ClientID %>").change(function () {
                $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbBaseDt.ClientID %>").val());
                __doPostBack("<%=hfBaseDt.ClientID %>", "");
            });

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENAMESCODE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GROUPLV1NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류", field: "GROUPLV2NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "소분류", field: "GROUPLV3NM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "제조사", field: "MAKERNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "K9", field: "K9", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "바코드", field: "BARCODE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "유형", field: "CATEGORY", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사용횟수", field: "USECNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "제조일", field: "PRODDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단말상태", field: "TERMINALSTATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단말 작업사유", field: "WORKCAUSE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "MAC주소", field: "MACADDR", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "자산번호", field: "ASSETNUM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "최초지사입고일", field: "FIRSTSTOREDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "STORECLASS", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사입고날짜", field: "STOREDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사보유기간(일)", field: "POSSESSIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "Engineer보유기간(일)", field: "ENGINEER_POSSESSIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사창고보유기간(일)", field: "STORE_POSSESSIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "현위치", field: "LOCATION", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "현위치입고날짜", field: "LOCATIONDATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "현위치보유기간(일)", field: "LOCATIONDAY", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사입고전상태", field: "BEFORESTATE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "Engineer보유기간분출횟수", field: "CARRYOUTCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "최종보유작업자소속", field: "LASTORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "최종보유작업자ID", field: "LASTWORKERID", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "보유작업자명", field: "LASTWORKERNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "소속구분", field: "KTSTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "단말구매여부", field: "PURCHASEYN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "회수여부", field: "RETRIEVEYN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "재활용여부", field: "RECYCLEYN", sorter: "string" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //20221102 엑셀SIZE 관련 추가
            ktsExcelSizeYN = "Y";
            ktsExcelHeaders = {
                NUM: "No.", BASEDATE: "기준일자", ORGLV2NM: "본부", ORGLV3NM: "지사", ORGLV4NM: "지점", OFFICENAMESCODE: "국사", GROUPLV1NM: "대분류", GROUPLV2NM: "중분류", GROUPLV3NM: "소분류", MAKERNM: "제조사", MODELNM: "모델명", K9: "K9", BARCODE: "바코드", CATEGORY: "유형", USECNT: "사용횟수", PRODDATE: "제조일", TERMINALSTATE: "단말상태", WORKCAUSE: "단말 작업사유", MACADDR: "MAC주소"
                , ASSETNUM: "자산번호", FIRSTSTOREDATE: "최초지사입고일", STORECLASS: "구분", STOREDATE: "지사입고날짜", POSSESSIONDAY: "지사보유기간(일)", ENGINEER_POSSESSIONDAY: "Engineer보유기간(일)", STORE_POSSESSIONDAY: "지사창고보유기간(일)", LOCATION: "현위치", LOCATIONDATE: "현위치입고날짜", LOCATIONDAY: "현위치보유기간(일)", BEFORESTATE: "지사입고전상태", CARRYOUTCNT: "Engineer보유기간분출횟수", LASTORGNM: "최종보유작업자소속"
                , LASTWORKERID: "최종보유작업자ID", LASTWORKERNM: "보유작업자명", KTSTYPE: "소속구분", PURCHASEYN: "단말구매여부", RETRIEVEYN: "회수여부", RECYCLEYN: "재활용여부"
            };


            //그리드 초기 세팅
            var grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값

            /*----검색 조건 ------- */

            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                valueEl.value = '';

                if (fieldEl.value == "") {
                    valueEl.setAttribute('disabled', 'disabled');
                    ktsGrid.setFilter('NUM', 'like', '');
                }
                else {
                    valueEl.removeAttribute('disabled');
                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                }
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {

                if (SearchChk()) {

                    //입력 파라미터
                    var aData = [];
                    aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                    aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetData');

                    //grid Bind
                    grid.getDataBind(jsonData, ajaxUrl);

                    $(".table-search").css('display', 'block');
                }
            });

            $("#gridExcel").click(function (e) {

                ktsExcelFileNm = "전체 단말 상세"; //20221102 엑셀SIZE 관련 추가
                ktsGrid.download("json", ktsExcelFileNm + ".xlsx", { sheetName: ktsExcelFileNm }); //20221102 엑셀SIZE 관련 추가

               // ktsGrid.download("xlsx", "전체 단말 상세.xlsx", { sheetName: "전체 단말 상세" });
            });

            $(".table-search").css('display', 'none');
        });
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox">
					<label>기준일자</label>
                    <asp:TextBox ID="txbBaseDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>전체 단말 상세 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGLV4NM">지점</option>
                            <option value="OFFICENAMESCODE">국사</option>
                            <option value="GROUPLV1NM">대분류</option>
                            <option value="LASTWORKERID">사번</option>
                            <option value="LASTWORKERNM">작업자명</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
