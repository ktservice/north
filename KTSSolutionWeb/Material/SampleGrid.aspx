﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleGrid.aspx.cs" Inherits="KTSSolutionWeb.SampleGrid" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <link href="../Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
        <script type="text/javascript">

        function PopupOrgTree(orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 720;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: BaseMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_BComp", param);

                Popupform.target = "OrgTree_BComp";
                var win = window.open("", "OrgTree_BComp", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function SetOrgCode(orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        function SearchChk() {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0 ) {
                alert("조회할 조직을 선택해 주세요.");
                return false;
            }else {
                return true;
            }
        }
        </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox">
					<label>기준일자</label>
                    <asp:TextBox ID="txbBaseDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div id="datalist" class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>

                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridDefault" type="button" class="btn-green last" value="fit" />
                        <input id="gridDefault2" type="button" class="btn-green last" value="fit2" />
                        <input id="gridNoPaging" type="button" class="btn-green last" value="NoPaging" />
                        <input id="gridNoPaging2" type="button" class="btn-green last" value="NoPaging2" />
                        <input id="gridMultiHeader" type="button" class="btn-green last" value="2중헤더" />
                        <input id="gridFrozen" type="button" class="btn-green last" value="Frozen컬럼" />
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                        <input id="gridCsv" type="button" class="btn-green last" value="Csv" />
                        <input id="gridJson" type="button" class="btn-green last" value="Json" />
                        <input id="gridMapView" type="button" class="btn-green last" value="DB프레임워크" />
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <label for="selField">구분: </label>
                        <select id="selField">
                            <option></option>
                            <option value="NUM">No.</option>
                            <option value="BASEDATE">기준일자</option>
                            <option value="ORGNM">조직</option>
                            <option value="GROUPLV1NM">대분류</option>
                            <option value="TOTALCNT">보유 현황</option>
                            <option value="DAYOVER30CNT">30일이상 보유현황</option>
                        </select>
                    </span>
                    <span>
                        <label for="selType">유형: </label>
                        <select id="selType">
                            <option value="like">like</option>
                            <option value="=">=</option>
                            <option value="<"><</option>
                            <option value="<="><=</option>
                            <option value=">">></option>
                            <option value=">=">>=</option>
                            <option value="!=">!=</option>
                        </select>
                    </span>
                    <span>
                        <label for="inputValue">값: </label>
                        <input id="inputValue" type="text" placeholder="찾을 내용 입력" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->

    <script>

        $(document).ready(function () {

            var grid;

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GROUPLV1NM" },
                { headerHozAlign: "center", hozAlign: "center", title: "보유 현황", field: "TOTALCNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "30일이상 보유현황", field: "DAYOVER30CNT", sorter: "string" }
            ];

            //2중헤더 
            var GroupColumns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                {
                    title: '그룹1', hozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                        { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GROUPLV1NM" },
                    ],
                },
                {
                    title: '그룹2', hozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "보유 현황", field: "TOTALCNT", sorter: "string" },
                        { headerHozAlign: "center", hozAlign: "center", title: "30일이상 보유현황", field: "DAYOVER30CNT", sorter: "string" }
                    ],
                },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" }
            ];

            //Frozen
            var FrozenColumns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GROUPLV1NM" },
                { headerHozAlign: "center", hozAlign: "center", title: "보유 현황", field: "TOTALCNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "30일이상 보유현황", field: "DAYOVER30CNT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "기준일자", field: "BASEDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string" }
            ];

            /*----start 검색 조건 ------- */
            var fieldEl = document.getElementById("selField");
            var typeEl = document.getElementById("selType");
            var valueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                updateFilter(fieldEl, typeEl, valueEl);
            });

            $("#selType").change(function (e) {
                updateFilter(fieldEl, typeEl, valueEl);
            });

            $("#inputValue").keyup(function (e) {
                updateFilter(fieldEl, typeEl, valueEl);
            });
            /*----end   검색 조건 ------- */

            $("#btnSelect").click(function (e) {

                if (SearchChk()) {

                    //컬럼 세팅
                    ktsColumns = Columns;

                    //그리드 초기 세팅
                    grid = ktsTabulator;
                    grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

                    //입력 파라미터
                    var aData = [];
                    aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                    aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetData');

                    //grid Bind
                    grid.getDataBind(jsonData, ajaxUrl);

                }
            });

            $("#gridMultiHeader").click(function (e) {

                //컬럼 세팅
                ktsColumns = GroupColumns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitData', false, 'local'); // div의 grid id 값

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            });

            $("#gridFrozen").click(function (e) {

                //컬럼 세팅
                ktsColumns = FrozenColumns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitData', false, 'local'); // div의 grid id 값

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
             });

            $("#gridDefault").click(function (e) {

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            });

            $("#gridDefault2").click(function (e) {

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetTestData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            });

            $("#gridNoPaging").click(function (e) {

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitColumns', true, null); // div의 grid id 값

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            });

            $("#gridNoPaging2").click(function (e) {

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitColumns', true, null); // div의 grid id 값

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetTestData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            });

            $("#gridExcel").click(function (e) {

                ktsGrid.download("xlsx", "data.xlsx", { sheetName: "My Data" });
            });

            $("#gridCsv").click(function (e) {

                ktsGrid.download("csv", "data.csv");
            });

            $("#gridJson").click(function (e) {

                ktsGrid.download("json", "data.json");
            });

            $("#gridMapView").click(function (e) {

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                /*
                DB Injection으로 콤마(,)를 내부적으로 없애기 때문에 조직 멀티 선택 후 조회 값은
                111,333,444 ... 이런식이 아니라 111^333^444로 변환 한다.
                DB에서는 콤마(,) 대신에 ^로 처리해야 멀티 조직에 대한 조회값이 나온다.
                 	SET @CHARLEN = INSTR(pORGCD, '^') - 1;
		            SET @DELIMETERCNT = (CHAR_LENGTH(pORGCD) - CHAR_LENGTH(REPLACE(pORGCD,'^','')) + 1);
                */
                aData[1] = $("#<%= txbBaseDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetDataMapView');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            });

            

        });

    </script>
</asp:Content>
