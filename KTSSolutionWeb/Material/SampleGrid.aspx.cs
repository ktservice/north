﻿using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Matrials;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class SampleGrid : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetOrgList();

                SetPageInit();

                Session["TESTSAMPLE"] = "Test";
            }
        }

        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }


        /// <summary>  
        /// This method will return the prepare html table body  
        /// </summary>  
        /// <returns>string html table body</returns>  
        public string GetTableRow()
        {
            string result = string.Empty;
            string htmlTr = string.Empty;
            string htmlTd = string.Empty;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            try
            {
                string strOrgCd = this.hfOrgCd.Value.ToString();
                //string strBaseDate = this.txbBaseDt.Text;
                string strBaseDate = Request.Form[this.txbBaseDt.UniqueID].ToString();

                using (TerminalMgmt mgmt = new TerminalMgmt())
                {
                    ds = mgmt.GetTerminalStatByOrg(strOrgCd, strBaseDate);
                }

                dt = ds.Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (var item in row.ItemArray)
                        {
                            htmlTd += "<td>" + item + "</td>";
                        }

                        htmlTr += "<tr>" + htmlTd + "</tr>";
                        htmlTd = string.Empty;
                    }
                }

                result = htmlTr;
            }
            catch (Exception)
            {
                result = string.Empty;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                dt = null;
            }
            return result;
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetPageInit()
        {
            try
            {
                //this.txbBaseDt.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                this.txbBaseDt.Text = "2022-03-21";

                this.hfOrgCd.Value = Session["ORGCD"].ToString();
                this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;
                }

                updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }


        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;

            Transaction transaction = new Transaction(aData);

            result = transaction.GetJsonData();

            return result;
        }

        /// <summary>
        /// 2000건 Test용도로 차후에는 사용하지 않는 메서드
        /// </summary>
        /// <param name="aData"></param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTestData(List<string> aData)
        {
            string result = string.Empty;

            Transaction transaction = new Transaction(aData);

            result = transaction.GetJsonTestData();

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetDataMapView(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = HttpContext.Current.CurrentHandler.GetType().BaseType.Name;
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }

    internal class Transaction
    {
        List<string> param = new List<string>();

        public Transaction(List<string> aData)
        {
            param = aData;
        }

        /// <summary>
        /// 2000건 Test용도로 차후에는 사용하지 않는 메서드
        /// </summary>
        /// <returns></returns>
        public string GetJsonTestData()
        {
            string result = string.Empty;

            DataSet ds = new DataSet();
            ds = GetTableRow();
            
            if (ds != null)
            {
                DataTable dt = new DataTable();
                dt = ds.Tables[0];

                if(dt.Rows.Count > 0)
                {
                    for (int i = 0; i < 20000; i++)
                    {
                        dt.ImportRow(ds.Tables[0].Rows[0]);
                    }
                }

                result = Utility.DataTableToJson(dt);

                ds.Dispose();
                dt = null;
            }
                
            return result;
        }

        public string GetJsonData()
        {
            string result = string.Empty;

            DataSet ds = new DataSet();
            ds = GetTableRow();

            if (ds != null)
            {
                result = Utility.DataTableToJson(ds.Tables[0]);

                ds.Dispose();
            }

            return result;
        }

        DataSet GetTableRow()
        {
            string orgCd = string.Empty;
            string baseDate = string.Empty;

            DataSet ds = new DataSet();

            try
            {
                orgCd = param[0];
                baseDate = param[1];

                using (TerminalMgmt mgmt = new TerminalMgmt())
                {
                    ds = mgmt.GetTerminalStatByOrg(orgCd, baseDate);
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
            }

            return ds;
        }

    }
}