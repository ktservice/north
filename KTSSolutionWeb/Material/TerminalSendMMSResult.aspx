﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TerminalSendMMSResult.aspx.cs" Inherits="KTSSolutionWeb.TerminalSendMMSResult" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            $("#<%= txbBaseDt.ClientID %>").change(function () {
                $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbBaseDt.ClientID %>").val());
                __doPostBack("<%=hfBaseDt.ClientID %>", "");
            });
        });

        function PopupOrgTree(orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 720;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: BaseMonth
                };

                var Popupform = createForm("/Common/OrgTree_Terminal", param);

                Popupform.target = "OrgTree_Terminal";
                var win = window.open("", "OrgTree_Terminal", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetOrgCode(orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=txbEmpUserNo.ClientID %>").val(empno);
            $("#<%=txbEmpUserNm.ClientID %>").val(empnm);
        }

        function btnDisplay(displayYn) {
            if (displayYn == "Y") {
                $("#<%=btnMMSReSend.ClientID %>").show();
            }
            else {
                $("#<%=btnMMSReSend.ClientID %>").hide();
            }
        }


        function SearchChk() {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                alert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        function btnSaveCheck() {
            if (confirm("단말보유자 정보를 수정하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnSendCheck() {
            if (confirm("장기 미사용 단말 반납 안내 메세지를 발송하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnReSendCheck() {
            if (confirm("전체 재발송 시 인사정보에 등록된 휴대폰번호로 발송됩니다. 장기 미사용 단말 반납 안내 메세지를 재발송하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox">
					<label>기준일자</label>
                    <asp:TextBox ID="txbBaseDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
				<span class="inpbox" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelEmp" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>사번</label>
                            <asp:TextBox ID="txbEmpUserNo" runat="server" Width="100px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
					        <label>이름</label>
                            <asp:TextBox ID="txbEmpUserNm" runat="server" Width="100px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
				<span class="optionbox" style="margin-right:20px">
					<label>발송결과</label> 
                    <asp:DropDownList ID="ddlSendResult" style="width:150px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="성공" Value="SUCC"></asp:ListItem>
                        <asp:ListItem Text="실패" Value="FAIL"></asp:ListItem>
                        <asp:ListItem Text="보유자정보없음" Value="NOEMPINFO"></asp:ListItem>
                        <asp:ListItem Text="미발송" Value="NOSEND"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
                <asp:Button id="btnReset" runat="server" OnClick="btnReset_Click" class="btn-green" style="float:right;margin-right:10px;" Text="조건초기화" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>장기 미사용단말 MMS발송 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnMMSReSend" runat="server" class="btn-green" style="display:none;" OnClientClick="return btnReSendCheck();" onClick="btnMMSReSend_Click" Text="전체 재발송" />
			            <asp:Button id="btnExcel" runat="server" class="btn-green" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <th>No.</th>
                                <th>기준일자</th>
                                <th>조직</th>
                                <th>IDMS사번</th>
                                <th>사번</th>
                                <th>이름</th>
                                <th>휴대폰번호</th>
                                <th>보유 현황</th>
                                <th>30일이상 보유현황</th>
                                <th>SMS발송결과</th>
                                <th id="thSave" runat="server" visible="false">수정/재발송</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><asp:Label ID="lblBaseDate" runat="server" Text='<%# Eval("BASEDATE") %>'></asp:Label></td>
                                            <td><%# Eval("ORGNM") %></td>
                                            <td><asp:Label ID="lblIdmsEmpNo" runat="server" Text='<%# Eval("IDMSEMPNO") %>'></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                                <div class="txtbox">
                                                    <asp:TextBox id="txbEmpNo" runat="server" Width="120px" Height="40px" MaxLength="20" Visible="false" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Text='<%# Eval("EMPNO") %>'></asp:TextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmpNm" runat="server" Text='<%# Eval("EMPNM") %>'></asp:Label>
                                                <div class="txtbox">
                                                    <asp:TextBox id="txbEmpNm" runat="server" Width="120px" Height="40px" MaxLength="50" Visible="false" Text='<%# Eval("EMPNM") %>'></asp:TextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("MOBILE") %>'></asp:Label>
                                                <div class="txtbox">
                                                    <asp:TextBox id="txbMobile" runat="server" Width="180px" Height="40px" MaxLength="20" Visible="false" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" Text='<%# Eval("MOBILE") %>'></asp:TextBox>
                                                </div>
                                            </td>
                                            <td><%# Eval("TOTALCNT") %></td>
                                            <td><asp:Label ID="lblDayOver30Cnt" runat="server" Text='<%# Eval("DAYOVER30CNT") %>'></asp:Label></td>
                                            <td><%# Eval("SMSSENDRESULT") %></td>
                                            <td id="tdSave" runat="server" visible="false">
                                                <asp:Label ID="lblSmsSendResult" runat="server" Visible="false" Text='<%# Eval("SMSSENDRESULTCODE") %>'></asp:Label>
                                                <asp:Label ID="lblSmsMsg" runat="server" Visible="false" Text='<%# Eval("MMS_MSG") %>'></asp:Label>
                                                <asp:Button id="btnSave" runat="server" OnClientClick="return btnSaveCheck();" OnClick="btnSave_ServerClick" class="btn-save" Visible="false" Text="수정" />
                                                <asp:Button id="btnSend" runat="server" OnClientClick="return btnSendCheck();" OnClick="btnSend_ServerClick" class="btn-save" Visible="false" Text="재발송" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
