﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MaterialStatByHoldingPopup.aspx.cs" Inherits="KTSSolutionWeb.MaterialStatByHoldingPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>조직검색</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var chkOrgCd = '';
        var chkOrgNm = '';
        var gubun;
        var rtn;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#txbOrgNm").keydown(function (e) {
                    if (e.keyCode == 13) {
                        fn_Search();
                        return false;
                    }
                });

                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {

            var Columns;

            gubun = $("#<%= hdnGubun.ClientID %>").val();

            if (gubun == "1") {
                $("#lblTitle").text('창고이관 정보');

                $("#selField").empty();
                $("#selField").append("<option value=''>선택</option>");
                $("#selField").append("<option value='NM'>보낸창고명</option>");
                $("#selField").append("<option value='FROMNM'>창고명</option>");
                $("#selField").append("<option value='ITEMNM'>품목</option>");

                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                    { headerHozAlign: "center", hozAlign: "center", title: "이관일", field: "WORKDT", sorter: "string", width: 140 },
                    { headerHozAlign: "center", hozAlign: "center", title: "보낸창고코드", field: "CD", sorter: "string", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "보낸창고명", field: "NM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "창고코드", field: "FROMCD", sorter: "string", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "창고명", field: "FROMNM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 100 },
                    { headerHozAlign: "center", hozAlign: "right", title: "이관수량", field: "CNT", sorter: "number", width: 100, formatter: fn_CellNumber }
                ];
            }
            else if (gubun == "2") {
                if ($("#hdnCd").val() == '') {
                    $("#lblTitle").text('받은 창고이관 알림');
                }
                else {
                    $("#lblTitle").text('창고이관 정보');
                }

                $("#selField").empty();
                $("#selField").append("<option value=''>선택</option>");
                $("#selField").append("<option value='NM'>창고명</option>");
                $("#selField").append("<option value='FROMNM'>보낸창고명</option>");
                $("#selField").append("<option value='ITEMNM'>품목</option>");

                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                    { headerHozAlign: "center", hozAlign: "center", title: "이관일", field: "WORKDT", sorter: "string", width: 140 },
                    { headerHozAlign: "center", hozAlign: "center", title: "창고코드", field: "CD", sorter: "string", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "창고명", field: "NM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "보낸창고코드", field: "FROMCD", sorter: "string", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "보낸창고명", field: "FROMNM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 100 },
                    { headerHozAlign: "center", hozAlign: "right", title: "이관수량", field: "CNT", sorter: "number", width: 100, formatter: fn_CellNumber }
                ];
            }

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("62vh");

        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_Search = function () {
            var gubun = $("#hdnGubun").val();
            var cd = $("#hdnCd").val();
            var itemCd = $("#hdnItemCd").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = cd;
            aData[2] = itemCd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">창고이관</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>조회 결과</strong>
                        <label class="searchrowcount"></label>
			            <div class="pull-right">
				            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				            </div>
			            </div>
                    </div>
                    <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none;">
                                <span>
                                    <select id="selField">
                                        <option value="">선택</option>
                                    </select> : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                                 <input type="hidden" id="hdnGubun" runat="server" />
                                <input type="hidden" id="hdnCd" runat="server" />
                                <input type="hidden" id="hdnItemCd" runat="server" />
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
