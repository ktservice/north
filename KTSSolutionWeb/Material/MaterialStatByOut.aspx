﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaterialStatByOut.aspx.cs" Inherits="KTSSolutionWeb.MaterialStatByOut" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbBaseDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbBaseDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#<%= txbBaseDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#<%= txbBaseDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });

                $("#selClass1").change(function (e) {
                    fn_Class(this, 2, 'selClass2');
                });

                $("#selClass2").change(function (e) {
                    fn_Class(this, 3, 'selClass3');
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "자재 불출 현황.xlsx", { sheetName: "자재 불출 현황" });
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbBaseDt.ClientID %>").show();
            $("#selMon").hide();
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellMemo = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = value.replace(/\n/gi, "<br/>");
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "WORKDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 120},
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "수량", field: "CNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "left", title: "비고", field: "MEMO", sorter: "string", width: 200, formatter: fn_CellMemo }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            callGb = 'selClass1';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'selClass1' || callGb == 'selClass2' || callGb == 'selClass3') {
                obj = $('#' + callGb);
                callGb = '';

                obj.empty();
                obj.append("<option value='전체'>전체</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });
            }
            else if (callGb == 'userOrgLv') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ORGNM != null) {
                        ktsGridPopup(item.ORGNM, cellTop, cellLeft, 5000);
                    }
                });
            }
        }

        var fn_PopupUpload = function () {
            var form = "/Material/MaterialOutUploadPopup";
            var target = "MaterialOutUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        //조직 찾기 함수
        var fn_PopupOrgCd = function (id) {
            cellNo = id;

            var form = "/Common/SearchOrgInventoryCd";
            var target = "SearchOrgInventoryCd";
            var param = {
                pGUBUN: "1",
                pRTN: "1"
            };
            var nWidth = 800;
            var nHeight = 600;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function (orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var fn_SearchChk = function() {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate = '';

                if ($("#selDt").val() == 'd') {
                    pDate = $("#<%= txbBaseDt.ClientID %>").val();
                }
                else {
                    pDate = $("#selMon").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[1] = pDate;
                aData[2] = $("#selClass1").val().replace('전체', '');
                aData[3] = $("#selClass2").val().replace('전체', '');
                aData[4] = $("#selClass3").val().replace('전체', '');

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_Class = function (obj, lv, gb) {

            callGb = gb;

            if (obj.value == '전체') {
                if (lv == '2') {
                    $("#selClass2").empty();
                    $("#selClass2").append("<option value='전체'>전체</option>");
                }
                else if (lv == '3') {
                    $("#selClass3").empty();
                    $("#selClass3").append("<option value='전체'>전체</option>");
                }
            }
            else {

                if (lv == '2') {
                    $("#selClass3").empty();
                    $("#selClass3").append("<option value='전체'>전체</option>");
                }

                var sVal1 = '';
                var sVal2 = '';
                var sVal3 = '';

                if ($("#selClass1 option:selected").val() != undefined) {
                    sVal1 = $("#selClass1").val();
                }
                if ($("#selClass2 option:selected").val() != undefined) {
                    sVal2 = $("#selClass2").val();
                }
                if ($("#selClass3 option:selected").val() != undefined) {
                    sVal3 = $("#selClass3").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = lv;
                aData[1] = '';
                aData[2] = sVal1;
                aData[3] = sVal2;
                aData[4] = sVal3;
                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="optionbox">
                <label>일자</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="d">일</option>
                    <option value="m">월</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbBaseDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox">
                    <label>대분류</label>
                    <select id="selClass1">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>중분류</label>
                    <select id="selClass2">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>소분류</label>
                    <select id="selClass3">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnUpload" type="button" class="btn-green last" value="일괄등록" />
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>자재 불출 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM">부서</option>
                            <option value="EMPNO">사번</option>
                            <option value="EMPNM">이름</option>
                            <option value="ITEMNM">품목</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
