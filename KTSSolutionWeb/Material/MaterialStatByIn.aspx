﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaterialStatByIn.aspx.cs" Inherits="KTSSolutionWeb.MaterialStatByIn" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbBaseDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbBaseDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#<%= txbBaseDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#<%= txbBaseDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });
                
                $("#selClass1").change(function (e) {
                    fn_Class(this, 2, 'selClass2');
                });

                $("#selClass2").change(function (e) {
                    fn_Class(this, 3, 'selClass3');
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "자재 입고 현황.xlsx", { sheetName: "자재 입고 현황" });
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbBaseDt.ClientID %>").show();
            $("#selMon").hide();
        }

        var fn_button = function (cell, formatterParams, onRendered) { //plain text value
            var html = '';

            cellNo = cell.getRow().getData().id;

            if (cell.getRow().getData().GUBUN == "Y") {
                html = "<input id='btnCellOrgCd' type='button' onclick='fn_PopupOrgCd(\"" + cellNo + "\");' value='부서선택' class='btn-gray' style='height:23px;line-height:0px;font-size:15px'>";
            }
            else if (cell.getRow().getData().GUBUN == "T") {
                html = "<input id='btnCellOrgCd' type='button' onclick='fn_PopupOrgCd(\"" + cellNo + "\");' value='부서변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                html += "&nbsp;<input id='btnCellOk' type='button' onclick='fn_CellOk(\"" + cellNo + "\");' value='이관' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
            }
            else if (cell.getRow().getData().GUBUN == "N") {
                html = "";
            }
            else {
                html = "<input id='btnCellCancel' type='button' onclick='fn_CellCancel(\"" + cellNo + "\");' value='이관취소' class='btn-del' style='height:23px;line-height:0px;font-size:15px'>";
            }

            return html;
        };


        var fn_CellOk = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var odrNum = rowData.ODRNUM;
            var itemNum = rowData.ITEMNUM;
            var orgCd = rowData.HOLDINGORGCD;
            var orgNm = rowData.HOLDINGORGNM;
            var statusCd = '02';

            if (confirm("이관을 하시겠습니까?")) {

                //같은 부서
                if (row.getData().ORGCD == orgCd) {
                    ktsGridAlert("같은 부서로 이관 할 수 없습니다");
                    return false;
                }
                else {
                    //입력 파라미터
                    var aData = [];
                    aData[0] = odrNum;
                    aData[1] = itemNum;
                    aData[2] = orgCd;
                    aData[3] = orgNm;
                    aData[4] = statusCd;

                    var jsonData = JSON.stringify({ aData: aData });
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'SetInTransaction');

                    cellNo = id;
                    callGb = 'inTranOk';
                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        }

        var fn_CellCancel = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var odrNum = rowData.ODRNUM;
            var itemNum = rowData.ITEMNUM;
            var orgCd = rowData.HOLDINGORGCD;
            var orgNm = rowData.HOLDINGORGNM;
            var statusCd = 'CC';

            if (confirm("이관취소를 하시겠습니까?")) {
                //입력 파라미터
                var aData = [];
                aData[0] = odrNum;
                aData[1] = itemNum;
                aData[2] = orgCd;
                aData[3] = orgNm;
                aData[4] = statusCd;

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetInTransaction');

                cellNo = id;
                callGb = 'inTranCancel';
                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        //삭제 시
        /*
        var fn_CellDelete = function (id) {
            //삭제 시
            ktsGrid.deleteRow(id);
            //RowCount Update
            ktsGridRowCount(ktsGrid.getDataCount());
        }
        */

        var fn_CellPopup = function (e, cell) {
            
            callGb = 'userOrgLv';
            cellLeft = cell.getElement().getBoundingClientRect().left;
            cellTop = window.pageYOffset + cell.getElement().getBoundingClientRect().top - $("#ktsGrid").position().top;

            var data = cell.getRow().getData().HOLDING_WORKER;

            if (data != '') {
                //입력 파라미터
                var aData = [];
                aData[0] = data;
                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUserOrgLv');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

            return '';
        }
        
         //CellClick 이벤트 시
        /*
        var fn_CellClick = function (e, cell) {
            alert(cell.getRow().getData().id);
        }
        */


        var fn_CellColor1 = function (cell, formatterParams, onRendered) {
            var value = cell.getRow().getData().ODRSTATUSNM;

            if (value == null) value = '';

            if (value != '') {

                if (value == '배송완료' || value == '입고') {
                    cell.getElement().style.color = "#077078";
                }
            }

            return value;
        }

        var fn_CellColor2 = function (cell, formatterParams, onRendered) {
            var value = cell.getRow().getData().HOLDINGORGNM;

            if (value == null) value = '';

            if (value != '') {
                cell.getElement().style.backgroundColor = "#077078";
                cell.getElement().style.color = "#fff";
            }
            else {
                cell.getElement().style.backgroundColor = "#fff";
                cell.getElement().style.color = "#333";
            }

            return value;
        }

        var fn_CellWorkerNm = function (cell, formatterParams, onRendered) {
            var value = cell.getRow().getData().HOLDING_WORKERNM;

            if (value == null) value = '';

            if (value != '') {
                cell.getElement().style.color = "#077078";
                cell.getElement().style.fontWeight = "bold";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGNM", sorter: "string", frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "주문일자", field: "ODRDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "주문번호", field: "ODRNUM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "품목번호", field: "ITEMNUM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "주문상태", field: "ODRSTATUSNM", sorter: "string", formatter: fn_CellColor1 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "right", title: "수량", field: "PRODUCTCNT", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "left", title: "배송지", field: "ADDR", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "left", title: "배송지상세", field: "ADDR2", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "이관일자", field: "HOLDINGDATE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "이관자사번", field: "HOLDING_WORKER", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "이관자이름", field: "HOLDING_WORKERNM", sorter: "string", width: 120, formatter: fn_CellWorkerNm, cellClick: fn_CellPopup},
                { headerHozAlign: "center", hozAlign: "left", title: "이관부서코드", field: "HOLDINGORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이관부서", field: "HOLDINGORGNM", width: 200, sorter: "string", formatter: fn_CellColor2},
                { headerHozAlign: "center", hozAlign: "center", title: "이관신청", field: "GUBUN", width: 170, sorter: "string", formatter: fn_button, tooltip:false } //셀클릭 이벤트 시, cellClick: fn_CellClick
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            callGb = 'selClass1';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();
                //obj.append("<option value='a'>전체누적</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'selClass1' || callGb == 'selClass2' || callGb == 'selClass3') {
                obj = $('#' + callGb);
                callGb = '';

                obj.empty();
                obj.append("<option value='전체'>전체</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });
            }
            else if (callGb == 'inTranOk') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    //이관 완료
                    if (item.SERVICE_ERROR_CD == null) {

                        var date = item.HOLDINGDATE;
                        var worker = item.HOLDING_WORKER;
                        var workerNm = item.HOLDING_WORKERNM;
                        var orgCd = item.HOLDINGORGCD;
                        var orgNm = item.HOLDINGORGNM;

                        fn_InTranOk(date, worker, workerNm, orgCd, orgNm);
                    }
                });
            }
            else if (callGb == 'inTranCancel') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    //이관 취소
                    if (item.SERVICE_ERROR_CD == null) {
                        fn_InTranCancel();
                    }
                });
            }
            else if (callGb == 'userOrgLv') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ORGNM != null) {
                        ktsGridPopup(item.ORGNM, cellTop, cellLeft, 5000);
                    }
                });
            }
        }

        var fn_InTranOk = function (date, worker, workerNm, orgCd, orgNm) {
            var row = ktsGrid.getRow(cellNo);
            var rowData = row.getData();
            var orgNm = rowData.HOLDINGORGNM;

            ktsGridAlert(orgNm + "으로 이관이 완료됐습니다.");

            ktsGrid.updateData([{
                id: cellNo, HOLDINGDATE: date, HOLDING_WORKER: worker, HOLDING_WORKERNM: workerNm, HOLDINGORGCD: orgCd, HOLDINGORGNM: orgNm, GUBUN: "C"
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });
        }

        var fn_InTranCancel = function () {
            ktsGridAlert("이관이 취소됐습니다.");

            ktsGrid.updateData([{
                id: cellNo, HOLDINGDATE: "", HOLDING_WORKER: "", HOLDING_WORKERNM: "", HOLDINGORGCD: "", HOLDINGORGNM: "", GUBUN: "Y"
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });
        }

        //조직 찾기 함수
        var fn_PopupOrgCd = function (id) {
            cellNo = id;

            var form = "/Common/SearchOrgInventoryCd";
            var target = "SearchOrgInventoryCd";
            var param = {
                pGUBUN: "1",
                pRTN: "1"
            };
            var nWidth = 720;
            var nHeight = 600;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        //조직 찾기 리턴 함수명 정해져 있음
        var SetDispOrgCd = function (orgcd, orgnm) {

            ktsGrid.updateData([{
                id: cellNo, HOLDINGORGCD: orgcd, HOLDINGORGNM: orgnm, GUBUN: "T"
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate = '';

                if ($("#selDt").val() == 'd') {
                    pDate = $("#<%= txbBaseDt.ClientID %>").val();
                }
                else {
                    pDate = $("#selMon").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[1] = pDate;
                aData[2] = $("#selHolding").val();
                aData[3] = $("#selClass1").val().replace('전체', '');
                aData[4] = $("#selClass2").val().replace('전체', '');
                aData[5] = $("#selClass3").val().replace('전체', '');

                if ($("#selHolding").val() == '02') {
                    ktsGrid.hideColumn("HOLDINGORGNM");
                    ktsGrid.hideColumn("GUBUN");
                }
                else {
                    ktsGrid.showColumn("HOLDINGORGNM");
                    ktsGrid.showColumn("GUBUN");
                }

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_Class = function (obj, lv, gb) {

            callGb = gb;

            if (obj.value == '전체') {
                if (lv == '2') {
                    $("#selClass2").empty();
                    $("#selClass2").append("<option value='전체'>전체</option>");
                }
                else if (lv == '3') {
                    $("#selClass3").empty();
                    $("#selClass3").append("<option value='전체'>전체</option>");
                }
            }
            else {

                if (lv == '2') {
                    $("#selClass3").empty();
                    $("#selClass3").append("<option value='전체'>전체</option>");
                }

                var sVal1 = '';
                var sVal2 = '';
                var sVal3 = '';

                if ($("#selClass1 option:selected").val() != undefined) {
                    sVal1 = $("#selClass1").val();
                }
                if ($("#selClass2 option:selected").val() != undefined) {
                    sVal2 = $("#selClass2").val();
                }
                if ($("#selClass3 option:selected").val() != undefined) {
                    sVal3 = $("#selClass3").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = lv;
                aData[1] = '';
                aData[2] = sVal1;
                aData[3] = sVal2;
                aData[4] = sVal3;
                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetServiceCfg');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            
        }



    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>일자</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="d">일</option>
                    <option value="m">월</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbBaseDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox">
                    <label>이관</label>
                    <select id="selHolding">
                        <option selected="selected" value="01">이관신청</option>
                        <option value="02">이관완료</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>대분류</label>
                    <select id="selClass1">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>중분류</label>
                    <select id="selClass2">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <span class="optionbox">
                    <label>소분류</label>
                    <select id="selClass3">
                        <option selected="selected" value="전체">전체</option>
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>자재 입고 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM">부서</option>
                            <option value="ODRSTATUSNM">주문상태</option>
                            <option value="ITEMNM">품목</option>
                            <option value="ADDR">배송지</option>
                            <option value="HOLDINGORGNM">이관부서</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
