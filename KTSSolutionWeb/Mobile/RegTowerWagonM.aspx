﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="RegTowerWagonM.aspx.cs" Inherits="KTSSolutionWeb.RegTowerWagonM" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var callGb;

        $(document).ready(function () {

            var carId = $("#<%= ddlCarNo.ClientID %> option:selected").val();

            if (carId == "" || carId == undefined) {
                $("#btnInsLog").show();
                $("#btnRegLog").hide();
            } else {
                $("#btnInsLog").hide();
                $("#btnRegLog").show();
            }

            $("#<%= ddlCarNo.ClientID %>").change(function (e) {
                fn_CarNoChange();
            });

            var fn_Choicebutton = function (cell) {
                var rowData = cell.getRow().getData();

                var orgnm = rowData.ORGNM;
                var empno = rowData.EMPNO;
                var empnm = rowData.EMPNM;

                var input = "<input id='btnCell' type='button' onclick='fn_SetReqEmpNm(\"" + orgnm + "\", \"" + empno + "\", \"" + empnm + "\");' value='선택' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;border-style:none;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 80, formatter: fn_Choicebutton, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 400, resizable: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            var grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, null); // div의 grid id 값
            ktsGrid.setHeight("300px");

            $("#btnSelect").click(function (e) {
                var EmpNm = $("#<%= txbSearchEmpNm.ClientID %>").val();

                if (EmpNm.length == 0) {
                    alert("이름을 입력해주세요.");
                    return false;
                } else {
                    //입력 파라미터
                    var aData = [];
                    aData[0] = EmpNm;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetData');

                    //grid Bind
                    grid.getDataBind(jsonData, ajaxUrl);
                }
            });
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (callGb == 'GetCarInfo') {
                        fn_SetCarInfo(json);
                    }
                    else {
                        if (item.ERRCD == '00') {
                            if (callGb == 'InsOk') {
                                alert('작업일보가 등록되었습니다.');
                            }
                            if (callGb == 'RegOk') {
                                alert('작업일보가 수정되었습니다.');
                            }

                            fn_OpenerSearch();
                        }
                        else {
                            alert(item.ERRMSG);
                        }
                    }
                });
            }

            callGb = '';
        };

        function fn_CarNoChange() {
            var carId = $("#<%= ddlCarNo.ClientID %> option:selected").val();

            if (carId == '') {
                fn_ResetCarInfo('');
            }
            else {
                var aData = [];
                aData[0] = carId;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarInfo');
                callGb = 'GetCarInfo';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        function fn_SetCarInfo(json) {
            if (JSON.parse(json).length > 0) {
                $.each(JSON.parse(json)[0], function (key, value) {
                    switch (key) {
                        case 'COMPANY':
                            $("#<%= txbCompany.ClientID %>").val(value);
                            break
                        case 'CARORGCD':
                            $("#<%= hdfCarOrgCd.ClientID %>").val(value);
                            break
                        case 'ORGLV2NM':
                            $("#<%= txbOrgLv2Nm.ClientID %>").val(value);
                            break
                        case 'ORGLV3NM':
                            $("#<%= txbOrgLv3Nm.ClientID %>").val(value);
                            break
                        case 'ORGLV4NM':
                            $("#<%= txbOrgLv4Nm.ClientID %>").val(value);
                            break
                        case 'EMPORGNM':
                            $("#<%= txbEmpNm.ClientID %>").val(value);
                            break
                        case 'EMPNO':
                            $("#<%= hdfEmpNo.ClientID %>").val(value);
                            break
                        default:
                            break
                    }
                });
            }
        }

        function fn_ResetCarInfo(clearChk) {
            $("#<%= hdfCarOrgCd.ClientID %>").val('');
            $("#<%= txbCompany.ClientID %>").val('');
            $("#<%= txbOrgLv2Nm.ClientID %>").val('');
            $("#<%= txbOrgLv3Nm.ClientID %>").val('');
            $("#<%= txbOrgLv4Nm.ClientID %>").val('');
            $("#<%= hdfEmpNo.ClientID %>").val('');
            $("#<%= txbEmpNm.ClientID %>").val('');

            if (clearChk != '' || clearChk != undefined) {
                $("#<%= hdfReqEmpNo.ClientID %>").val('');
                $("#<%= txbReqEmpNm.ClientID %>").val('');
                $("#<%= txbOdrNum.ClientID %>").val('');
                $("#<%= txbWorkDt.ClientID %>").val('');
                $("#<%= ddlWorkType.ClientID %>").val('');
                $("#<%= ddlSvcType.ClientID %>").val('');
                $("#<%= ddlCableType.ClientID %>").val('');
                $("#<%= ddlBldType.ClientID %>").val('');
                $("#<%= ddlPositionType.ClientID %>").val('');
                $("#<%= txbNotice.ClientID %>").val('');
            }
        }

        function fn_ModalEmpUser(emptype) {
            $("#divModal").show();
            $("#hdfEmpType").val(emptype);
        }
        
        function fn_SetReqEmpNm(orgnm, empno, empnm) {
            var emptype = $("#hdfEmpType").val();

            if (emptype == "REQ") {
                $("#<%=hdfReqEmpNo.ClientID %>").val(empno);
                $("#<%= txbReqEmpNm.ClientID %>").val(orgnm + ' ' + empnm);
            }
            else {
                $("#<%=hdfEmpNo.ClientID %>").val(empno);
                $("#<%= txbEmpNm.ClientID %>").val(orgnm + ' ' + empnm);
            }

            $("#divModal").hide();
            $("#hdfEmpType").val();
        }

        function fn_ModalClose() {
            $("#divModal").hide();
            $("#hdfEmpType").val();
        }

        function fn_InsCheck() {
            if (confirm("작업일보를 등록 하시겠습니까?")) {
                InsWagonLoginfo();
            }
            else {
                return false;
            }
        }

        function fn_RegCheck() {
            if (confirm("작업일보를 수정 하시겠습니까?")) {
                SetWagonLoginfo();
            }
            else {
                return false;
            }
        }

        function InsWagonLoginfo() {

            var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();
            var company = $("#<%= txbCompany.ClientID %>").val();
            var reqempno = $("#<%= hdfReqEmpNo.ClientID %>").val();
            var empno = $("#<%= hdfEmpNo.ClientID %>").val();
            var traning = "";
            var odrnum = $("#<%= txbOdrNum.ClientID %>").val();
            var workdate = $("#<%= txbWorkDt.ClientID %>").val();
            var worktype = $("#<%= ddlWorkType.ClientID %> option:selected").val();
            var svctype = $("#<%= ddlSvcType.ClientID %> option:selected").val();
            var cabletype = $("#<%= ddlCableType.ClientID %> option:selected").val();
            var bldtype = $("#<%= ddlBldType.ClientID %> option:selected").val();
            var positiontype = $("#<%= ddlPositionType.ClientID %> option:selected").val();
            var noice = $("#<%= txbNotice.ClientID %>").val();

            var message = '';
            var bChk = true;

            if (carid == "" || carid == undefined) {
                message = "등록할 차량을 선택해주세요.";
                bChk = false;
            }
            else if (company == "" || company == undefined) {
                message = "사업장을 입력해주세요.";
                bChk = false;
            }
            else if (reqempno == "" || reqempno == undefined) {
                message = "요청자를 입력해주세요";
                bChk = false;
            }
            else if (empno == "" || empno == undefined) {
                message = "작업자가 할당되지 않은 고소차량은 일보를 등록 할 수 없습니다.";
                bChk = false;
            }
            else if (odrnum == "" || odrnum == undefined) {
                message = "명령번호를 입력해주세요.";
                bChk = false;
            }
            else if (workdate == "" || workdate == undefined) {
                message = "작업일자를 입력해주세요";
                bChk = false;
            }


            if (bChk) {
                var aData = [];

                aData[0] = carid;
                aData[1] = company;
                aData[2] = reqempno;
                aData[3] = empno;
                aData[4] = traning;
                aData[5] = odrnum;
                aData[6] = workdate;
                aData[7] = worktype;
                aData[8] = svctype;
                aData[9] = cabletype;
                aData[10] = bldtype;
                aData[11] = positiontype;
                aData[12] = noice;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'InsTowerWagonLog');
                callGb = 'InsOk';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            else {
                alert(message);
            }
        }

        function SetWagonLoginfo() {

            var workdate = $("#<%= txbWorkDt.ClientID %>").val();
            var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();
            var logno = $("#<%= hdfLogNo.ClientID %>").val();
            var traning = "";
            var worktype = $("#<%= ddlWorkType.ClientID %> option:selected").val();
            var svctype = $("#<%= ddlSvcType.ClientID %> option:selected").val();
            var odrnum = $("#<%= txbOdrNum.ClientID %>").val();
            var cabletype = $("#<%= ddlCableType.ClientID %> option:selected").val();
            var bldtype = $("#<%= ddlBldType.ClientID %> option:selected").val();
            var positiontype = $("#<%= ddlPositionType.ClientID %> option:selected").val();
            var noice = $("#<%= txbNotice.ClientID %>").val();

            var aData = [];

            aData[0] = workdate;
            aData[1] = carid;
            aData[2] = logno;
            aData[3] = traning;
            aData[4] = worktype;
            aData[5] = svctype;
            aData[6] = odrnum;
            aData[7] = cabletype;
            aData[8] = bldtype;
            aData[9] = positiontype;
            aData[10] = noice;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTowerWagonLog');

            callGb = 'RegOk';
            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function fn_OpenerSearch() {
            var stdate = $("#<%= hdfStDate.ClientID %>").val();
            var endate = $("#<%= hdfEnDate.ClientID %>").val();
            var empnm = $("#<%= hdfEmpNm.ClientID %>").val();
            var carno = $("#<%= hdfCarNo.ClientID %>").val();

            var param = {
                pSTDATE: stdate,
                pENDATE: endate,
                pEMPNM: empnm,
                pCARNO: carno,
                pREGYN: "Y"
            };

            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/TowerWagonM");

            if (param != null) {
                for (var name in param) {
                    var valObj = param[name];
                    addInput(form, name, valObj);
                }
            }

            form.submit();
        }
    </script>        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-clothes-request -->
        <div class="page-clothes-request">
			<h2>고소차 작업일보 등록/수정</h2> 
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>작업내용</legend>
					<span class="optionbox" id="spanCarNo" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="width:84% !important;margin:0 !important;margin-left:0;">차량번호&nbsp;<span style="color:red;">*</span></label><br />
                        <asp:DropDownList ID="ddlCarNo" runat="server" AutoPostBack="false" style="width:84% !important"></asp:DropDownList>
                        <asp:HiddenField ID="hdfLogNo" runat="server" />
                        <asp:HiddenField ID="hdfCarOrgCd" runat="server" />
                    </span>
					<span class="inpbox" id="spanCompany" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">사업자&nbsp;<span style="color:red;">*</span></label><br />
						<asp:TextBox ID="txbCompany" runat="server" MaxLength="50" style="width:84% !important"></asp:TextBox>
                    </span>
					<span class="inpbox" id="spanOrgLv2Nm" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">본부</label><br />
						<asp:TextBox ID="txbOrgLv2Nm" runat="server" ReadOnly="true" style="width:84% !important"></asp:TextBox>
                    </span>
					<span class="inpbox" id="spanOrgLv3Nm" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">지사</label><br />
						<asp:TextBox ID="txbOrgLv3Nm" runat="server" ReadOnly="true" style="width:84% !important"></asp:TextBox>
                    </span>
					<span class="inpbox" id="spanOrgLv4Nm" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">지점</label><br />
						<asp:TextBox ID="txbOrgLv4Nm" runat="server" ReadOnly="true" style="width:42% !important"></asp:TextBox>
                    </span>
					<span class="inpbox" id="spanReqEmpNm" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">요청자명&nbsp;<span style="color:red;">*</span></label><br />
						<asp:TextBox ID="txbReqEmpNm" runat="server" style="width:90% !important" onclick="fn_ModalEmpUser('REQ');" ReadOnly="true"></asp:TextBox>
						<asp:HiddenField ID="hdfReqEmpNo" runat="server" />
                    </span>
					<span class="inpbox" id="spanWorkEmpNm" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">고소차작업자&nbsp;<span style="color:red;">*</span></label><br />
						<asp:TextBox ID="txbEmpNm" runat="server" style="width:90% !important" onclick="fn_ModalEmpUser('WORKER');"  ReadOnly="true"></asp:TextBox>
						<asp:HiddenField ID="hdfEmpNo" runat="server" />
                    </span>
					<span class="inpbox" id="spanOdrNum" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">명령번호/접속번호/오더번호&nbsp;<span style="color:red;">*</span></label><br />
						<asp:TextBox ID="txbOdrNum" runat="server" style="width:60% !important" MaxLength="20"></asp:TextBox>
                    </span>
					<span class="inpbox" id="spanOdrEx" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="width:90% !important;margin:0 !important;margin-left:0;padding-left:10px;font-weight:normal">개통:명령번호(OA-0000형식, 접속번호X)</label><br />
						<label style="width:90% !important;margin:0 !important;margin-left:0;padding-left:10px;font-weight:normal">수리:서비스계약번호</label><br />
						<label style="width:90% !important;margin:0 !important;margin-left:0;padding-left:10px;font-weight:normal">GigaEyes:서비스계약ID(SAID)</label><br />
                    </span>
					<span class="inpbox" id="spanDate" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="width:84% !important;margin:0 !important;margin-left:0;">작업일자&nbsp;<span style="color:red;">*</span></label><br />
						<asp:TextBox ID="txbWorkDt" runat="server" class="date" ReadOnly="true" style="width:84% !important"></asp:TextBox>
					</span>
					<span class="optionbox" id="spanWorkType" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">작업구분</label><br />
                        <asp:DropDownList ID="ddlWorkType" runat="server" AutoPostBack="false" style="width:84% !important"></asp:DropDownList>
                    </span>
					<span class="optionbox" id="spanSvcType" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">상품</label><br />
                        <asp:DropDownList ID="ddlSvcType" runat="server" AutoPostBack="false" style="width:84% !important"></asp:DropDownList>
                    </span>
					<span class="optionbox" id="spanCableType" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="width:80%;margin:0 !important;margin-left:0;">인입선구분</label><br />
                        <asp:DropDownList ID="ddlCableType" runat="server" AutoPostBack="false" style="width:84% !important"></asp:DropDownList>
                    </span>
					<span class="optionbox" id="spanBldType" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">건물유형</label><br />
                        <asp:DropDownList ID="ddlBldType" runat="server" AutoPostBack="false" style="width:84% !important"></asp:DropDownList>
                    </span>
					<span class="optionbox" id="spanPositionType" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">작업위치</label><br />
                        <asp:DropDownList ID="ddlPositionType" runat="server" AutoPostBack="false" style="width:84% !important"></asp:DropDownList>
                    </span>
					<span class="inpbox" id="spanNotice" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">비고</label><br />
						<asp:TextBox ID="txbNotice" runat="server" style="width:90% !important" MaxLength="1500"></asp:TextBox>
                    </span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
                        <label style="width:90% !important;margin:0 !important;margin-left:0;"><span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력 항목입니다.</label>                        
                    </span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnInsLog" type="button" class="btn-green" style="border-style:none;" onclick="fn_InsCheck()" value="등록" />
                        <input id="btnRegLog" type="button" class="btn-green" style="border-style:none;display:none;" onclick="fn_RegCheck()" value="수정" />
                        <input id="btnClose" type="button" class="btn-black" style="border-style:none;" onclick="fn_OpenerSearch()" value="닫기" />
					</span>
				</fieldset>
            </div>
			
            <div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:450px;top:10%;left:0;background:rgb(255, 255, 255); border-radius:10px;overflow-y:scroll;">
				<div class="requestlist" style="width:90%;height:90%;text-align:center;margin-left:5%">
					<strong style="margin-top:2px;margin-right:10px;margin-bottom:10px;float:left";">사원 조회</strong>

					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">사원명</label>
						<asp:TextBox ID="txbSearchEmpNm" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
                        <input id="btnSelect" class="btn-green" style="width:33px;margin:0 !important;color:white;border-style:none;" value="조회" />
                        <input type="hidden" id="hdfEmpType" />
					</span>

                    <div class="datalist" style="margin-top:10px;padding-bottom:10px;">
			            <!-- S:scrollbox -->
			            <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
			            <!-- E:scrollbox -->
					</div>

                    <input id="btnModalClose" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ModalClose()" value="닫기" />
				</div>
			</div>

            <asp:HiddenField ID="hdfStDate" runat="server" />
            <asp:HiddenField ID="hdfEnDate" runat="server" />
            <asp:HiddenField ID="hdfEmpNm" runat="server" />
            <asp:HiddenField ID="hdfCarNo" runat="server" />
        </div>
		<!-- E:page-clothes-request -->

    </div>
	<!-- E: contentsarea -->
</asp:Content>
