﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PassNewBizInfoM.aspx.cs" Inherits="KTSSolutionWeb.PassNewBizInfoM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">  
		
        function SaveChk() {
            if (confirm("성공여부는 Shooting 완료상태에서만 저장됩니다. PASS 상태를 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

		function ModalEmpInfo(empno) {
            document.getElementById("<%=hfEmpNo.ClientID %>").value = empno;
			$("#divModal").show();
			__doPostBack("<%=updPanelModal.ClientID %>", "");            
		}

		function ModalClose() {
			$("#divModal").hide();
			return false;
		}
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div class="page-clothes-request">
		    <h2>PASS 정보 조회</h2>
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>조회 조건</legend>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">조회일자</label><br />
						<input type="text" id="txbStDate" runat="server" class="date" readonly="true" style="width:46% !important;" />
						<label style="width:10px">~</label>
						<input type="text" id="txbEnDate" runat="server" class="date" readonly="true" style="width:46% !important;" />
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
						<asp:Button id="btnSelect" runat="server" OnClick="btnSelect_Click" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;" Text="조회" />
					</span>
				</fieldset>
			</div>
            <!-- S: scrollBox -->
			<strong style="margin-top:2px;margin-right:10px;float:left">신사업 Pass 조회</strong>
            <div class="scrollbox">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<table>
									<thead>
										<th>No,</th>
										<th>일자</th>
										<th>등록자</th>
										<th>관계</th>
										<th>성명</th>
										<th>전화번호</th>
										<th>희망상품</th>
										<th>비고</th>
										<th>상태</th>
										<th>성공여부</th>
										<th>수정</th>
									</thead>
									<tbody>
                                        <asp:Repeater ID="rptResult1" runat="server">
                                            <ItemTemplate>
												<tr>
													<td><%# Eval("NUM") %></td>
													<td><%# Eval("PASSDT") %></td>
													<td>
														<asp:Label ID="lblEmpNm" runat="server" Text='<%# Eval("EMPNM") %>'></asp:Label>
														<asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>' Visible="false"></asp:Label>
													</td>
													<td><%# Eval("RELATIONTYPE") %></td>
													<td><%# Eval("CUSTNM") %></td>
													<td><a href='tel:<%# Eval("CUSTTELNO") %>'><%# Eval("CUSTTELNO") %></a></td>
													<td><%# Eval("SVCTYPE") %></td>
													<td><%# Eval("NOTI1") %></td>
													<td>
														<%# Eval("PASSSTATNM") %>
														<asp:Label ID="lblPassStat" runat="server" Text='<%# Eval("PASSSTAT") %>' visible="false"></asp:Label>
													</td>
													<td>
														<span class="optionbox" style="width:100%; margin:3px 0 0 0; font-size:0;">
															<asp:DropDownList ID="ddlSuccYn" runat="server" Width="90px">
																<asp:ListItem Value="" Text="-"></asp:ListItem>
																<asp:ListItem Value="Y" Text="성공"></asp:ListItem>
																<asp:ListItem Value="N" Text="실패"></asp:ListItem>
															</asp:DropDownList>
															<asp:Label ID="lblSuccYn" runat="server" Text='<%# Eval("SUCCYN") %>' visible="false"></asp:Label>
														</span>
													</td>
													</td>
													<td>
														<asp:Label ID="lblProcId" runat="server" Text='<%# Eval("PROCID") %>' visible="false"></asp:Label>
														<asp:Button id="btnSave" runat="server" class="btn-save" Text="저장" OnClientClick="return SaveChk()" OnClick="btnSave_Click"/>
													</td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
								<asp:AsyncPostBackTrigger ControlID="paging" />
							</Triggers>
						</asp:updatepanel>
					</div>
				</div>
			</div>
			<uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
			<!--E: scrollBox -->
			
            <div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:50%;top:30%;left:0;background:rgb(255, 255, 255); border-radius:10px;">
				<div class="requestlist" style="width:90%;text-align:center;margin-left:5%">
					<strong style="margin-top:2px;margin-right:10px;float:left;">Pass 등록자 정보</strong>
                    <div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<asp:updatepanel id="updPanelModal" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<asp:HiddenField ID="hfEmpNo" runat="server" OnValueChanged="hfEmpNo_ValueChanged" />
								<table>
									<thead>
										<th>부서</th>
										<th>성명</th>
										<th>직책</th>
										<th>전화번호</th>
									</thead>
									<tbody>
										<asp:Repeater ID="rptResult3" runat="server">
											<ItemTemplate>
												<tr>
													<td><%# Eval("ORGFULLNM") %></td>
													<td><%# Eval("EMPNM") %></td>
													<td><%# Eval("TITLENM") %></td>
													<td><a href='tel:<%# Eval("MOBILE") %>'><%# Eval("MOBILE") %></a></td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="hfEmpNo" EventName="ValueChanged" />
							</Triggers>
						</asp:updatepanel>
					</div>
					<asp:Button id="btnModalClose" runat="server" class="btn-black" BorderStyle="None" style="width:33px;margin:0 !important;" OnClientClick="return ModalClose();" Text="닫기" />
				</div>
			</div>
        </div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>