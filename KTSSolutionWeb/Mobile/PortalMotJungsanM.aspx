﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PortalMotJungsanM.aspx.cs" Inherits="KTSSolutionWeb.PortalMotJungsanM" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="../Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="../Resource/css/notice_M.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };
                $("#txtMdate").monthpicker(options);

                $("#txtMdate").change(function () {
                    fn_Data('DATA');
                });
            }
        };

        var fn_Init = function () {
            //$("#lblContentTitle").text('MOT 정산 조회');

            $(".table-option").css('display', 'none');
            $(".mdate").val($("#<%=hdnMdate.ClientID %>").val());
            $("#divDetail").hide();
        }

        var fn_Load = function () {
            fn_NoticeInfo('MOTJ');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }
 
        var ajaxCallBack = function (json) {
            if (callGb == 'MOTJ') {
                $.each(JSON.parse(json), function (idx, item) {
                    $('.noticetitletext').html(replaceBrTag(item.TITLE));
                    $('.noticecontenttext').html(replaceBrTag(item.CONTENTS));
                });

                callGb = '';

                fn_Data('DATA');
            }
            else if (callGb == 'DATA') {
                var vTemp = '';
                var html1 = '';
                var html2 = '';
                var html3 = '';
                var html4 = '';
                var html5 = '';
                var html6 = '';

                $.each(JSON.parse(json), function (idx, item) {
                    var htmlData = '';
                    var productNm = item.PRODUCT_NM;
                    var productNms = productNm.split(',');

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[0] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.MOT_AMT) + '</td>';
                    htmlData += '</tr>';

                    html1 = htmlData;

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[1] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.BASE_INTERNET_AMT) + '(' + priceToString(item.BASE_INTERNET_CNT)+ '건)</td>';
                    htmlData += '</tr>';
                    htmlData += '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[2] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.BASE_TV_AMT) + '(' + priceToString(item.BASE_TV_CNT) + '건)</td>';
                    htmlData += '</tr>';
                    htmlData += '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[3] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.BASE_VOICE_AMT) + '(' + priceToString(item.BASE_VOICE_CNT) + '건)</td>';
                    htmlData += '</tr>';

                    html2 = htmlData;

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[4] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.VAS_GIGA_CHANGE_AMT) + '(' + priceToString(item.VAS_GIGA_CHANGE_CNT) + '건)</td>';
                    htmlData += '</tr>';
                    htmlData += '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[5] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.VAS_GIGA_WIFI_AMT) + '(' + priceToString(item.VAS_GIGA_WIFI_CNT) + '건)</td>';
                    htmlData += '</tr>';
                    htmlData += '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[6] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.BONUS_AMT) + '(' + priceToString(item.BONUS_POINT) + '건)</td>';
                    htmlData += '</tr>';
                    htmlData += '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[7] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.VAS_ETC_AMT) + '(' + priceToString(item.VAS_ETC_CNT) + '건)</td>';
                    htmlData += '</tr>';

                    html3 = htmlData;

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[8] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.AIR_AMT) + '(' + priceToString(item.AIR_CNT) + '건)</td>';
                    htmlData += '</tr>';

                    html4 = htmlData;

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[9] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.COMMISSION_AMT) + '(' + priceToString(item.COMMISSION_CNT) + '건)</td>';
                    htmlData += '</tr>';

                    html5 = htmlData;

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right" style="font-weight:bold;">' + productNms[10] + '</td>';
                    htmlData += '    <td class="table-cell last">' + priceToString(item.MOT_OTHER_AMT) + '(' + priceToString(item.MOT_OTHER_CNT) + '건)</td>';
                    htmlData += '</tr>';

                    html6 = htmlData;

                    $('#lblPaymentAmt').text(priceToString(item.PAYMENT_AMT));
                    $('#lblTotAmt').text(priceToString(item.TOT_AMT));

                    $('#lblRegDt').text(item.REGDT);

                    vTemp = item.REGDT;
                });

                if (vTemp != '') {

                    $('#bodyMotResultData').html(html1);
                    $('#bodyHomeProductData').html(html2);
                    $('#bodyHomeProductAddData').html(html3);
                    $('#bodyAirProductData').html(html4);
                    $('#bodyCommissionData').html(html5);
                    $('#bodyEtcData').html(html6);

                    $('#divData').show();
                    $('#divData').css('display', 'block');
                    $('#divAlert').hide();
                    $('#divAlert').css('display', 'none');

                }
                else {
                    $('#divData').hide();
                    $('#divData').css('display', 'none');
                    $('#divAlert').show();
                    $('#divAlert').css('display', 'block');
                    $('#lblRegDt').text('');
                    
                }

                callGb = '';
            }

        }

        function replaceBrTag(str) {
            if (str == undefined || str == null) {
                return "";
            }

            str = str.replace(/\r\n/ig, '<br>');
            str = str.replace(/\\n/ig, '<br>');
            str = str.replace(/\n/ig, '<br>');
            return str;
        }

        var fn_NoticeInfo = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];
            aData[0] = gb;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetNoticeInfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function (gubun) {
            callGb = gubun;

            //입력 파라미터
            var aData = [];
            aData[0] = $(".mdate").val();

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


    </script>

	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div>
            <!-- S:UserControlTitle -->
            <uc:menu ID="menu" runat="server" />
            <!-- E:UserControlTitle -->
            <div class="noticebox">
                <div class="noticetitle">
                    <span class="noticetitletext"></span>
                </div>
                <div class="noticecontent">
                    <span class="noticecontenttext"></span>
                </div>
            </div>
            <h2></h2>
            <div class="topContent">
                <!-- S:fieldset -->
                <fieldset>
                    <legend>조회조건</legend>
                    <span class="inpbox fieldsetSpan">
                        <label class="fieldsetLabel" for="txtMdate">월</label>
                        <input type="text" id="txtMdate" class="mdate" />
                        <input type="hidden" id="hdnMdate" runat="server" />
                    </span>
                    <span>
                        <label id="lblRegDt" class="fieldsetLabel2"></label></span>
                </fieldset>
                <!-- //E:fieldset -->
            </div>
            <h2></h2>
            <!-- S: scrollBox -->
            <div class="scrollbox">
                <fieldset class="view-data-list-body">
                    <div class="data-group">
                        <div id="divData" style="display: none;">
                            <fieldset class="fieldset mobile-search">
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header td_width50 table-header border_right">지급총액(세전)</th>
                                                <th class="table-header">정산총액</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="table-cell td_backwhite border_right"><span id="lblPaymentAmt"></span></td>
                                                <td class="table-cell td_backwhite"><span id="lblTotAmt"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">MOT 실적(제게)</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                        </thead>
                                        <tbody id="bodyMotResultData">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">홈기본상품</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                        </thead>
                                        <tbody id="bodyHomeProductData">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">홈부가상품</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                        </thead>
                                        <tbody id="bodyHomeProductAddData">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">무선상품</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                        </thead>
                                        <tbody id="bodyAirProductData">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">유지수수료</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                        </thead>
                                        <tbody id="bodyCommissionData">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">기타</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                        </thead>
                                        <tbody id="bodyEtcData">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                        <div id="divAlert" style="display: none;">
                            <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <!-- E: scrollBox -->
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>