﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="TowerWagonM.aspx.cs" Inherits="KTSSolutionWeb.TowerWagonM" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        function PageReady() {
            var fn_buttons = function (cell) {
                var value = cell.getValue();
                var rowData = cell.getRow().getData();

                var rowNo = rowData.id;

                var input = "<input id='btnCellSave' type='button' onclick='fn_RegCheck(\"" + rowNo + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDel' type='button' onclick='fn_DelCheck(\"" + rowNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>"

                return input;
            };

            var Columns = [
                { formatter: "responsiveCollapse", width: 25, minWidth: 25, hozAlign: "center", resizable: false, headerSort: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", resizable: false, width: 60, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업일", field: "WORKDATE", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 0, resizable: false, visible: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "저장", field: "SAVE", resizable: false, width: 90, minWidth: 90, formatter: fn_buttons, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "사업자", field: "COMPANY", sorter: "string", width: 400, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "요청자사번", field: "REQEMPNO", sorter: "string", width: 120, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "요청자명", field: "REQEMPNM", sorter: "string", width: 140, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "고소차작업자사번", field: "EMPNO", sorter: "string", width: 140, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "교육이수현황", field: "TRANING", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "고소차작업자명", field: "EMPNM", sorter: "string", width: 140, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업번호", field: "LOGNO", sorter: "string", width: 120, visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업구분", field: "WORKTYPE", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품", field: "SVCTYPE", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "명령번호/접속번호/오더번호", field: "ODRNUM", sorter: "string", width: 180, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "인입선구분", field: "CABLETYPE", sorter: "string", width: 120, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "건물유형", field: "BLDTYPE", sorter: "string", width: 150, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업위치", field: "POSITIONTYPE", sorter: "string", width: 220, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTICE", sorter: "string", responsive: 2 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridCollapseM('ktsGrid', 'fitDataFill', true, null); // div의 grid id 값

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#btnRegLog").click(function (e) {
                fn_PopupSetLog("", "", "");
            });
        }
        $(document).ready(PageReady);

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'DelOk') {
                            ktsGridAlert('작업일보가 삭제되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
            }

            callGb = '';
        };

        function fn_Search() {
            var sdt = new Date($("#<%= txbStDt.ClientID %>").val());
            var edt = new Date($("#<%= txbEnDt.ClientID %>").val());

            var dateDiff = Math.ceil((edt.getTime() - sdt.getTime()) / (1000 * 3600 * 24));

            if (dateDiff > 7) {
                alert(" 조회기간은 7일을 초과 할 수 없습니다.");
            }
            else {
                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= txbStDt.ClientID %>").val();
                aData[1] = $("#<%= txbEnDt.ClientID %>").val();
                aData[2] = $("#<%= txbEmpNm.ClientID %>").val();
                aData[3] = $("#<%= txbCarNo.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
        }

        function searchBtnClick() {
            //PageReady();
            $("#btnSelect").click();
        }

        function fn_RegCheck(rowNo) {
            SetWagonLoginfo(rowNo);
        }

        function fn_DelCheck(rowNo) {
            if (confirm("작업일보를 삭제 하시겠습니까?")) {
                DelWagonLogInfo(rowNo);
            }
            else {
                return false;
            }
        }

        function SetWagonLoginfo(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var workdate = rowData.WORKDATE;
            var carid = rowData.CARID;
            var logno = rowData.LOGNO;

            fn_PopupSetLog(workdate, carid, logno);
        }

        function DelWagonLogInfo(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var aData = [];

            aData[0] = rowData.WORKDATE;
            aData[1] = rowData.CARID;
            aData[2] = rowData.LOGNO;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'DelTowerWagonLog');

            callGb = 'DelOk';
            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function fn_PopupSetLog(workdate, carid, logno) {
            var stdate = $("#<%= txbStDt.ClientID %>").val();
            var endate = $("#<%= txbEnDt.ClientID %>").val();
            var empnm = $("#<%= txbEmpNm.ClientID %>").val();
            var carno = $("#<%= txbCarNo.ClientID %>").val();

            var param = {
                pWORKDATE: workdate,
                pCARID: carid,
                pLOGNO: logno,
                pSTDATE: stdate,
                pENDATE: endate,
                pEMPNM: empnm,
                pCARNO: carno
            };

            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/RegTowerWagonM");

            if (param != null) {
                for (var name in param) {
                    var valObj = param[name];
                    addInput(form, name, valObj);
                }
            }

            form.submit();
        }
    </script>        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-clothes-request -->
        <div class="page-clothes-request">
			<h2>고소차 작업일보</h2> 
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>조회조건</legend>
					<span class="inpbox" id="spanDate" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">작업일자</label><br />
						<asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
						<label style="width:10px">~</label>
						<asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanEmpNm" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">작업자명</label><br />
						<asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:84% !important"></asp:TextBox>
						<asp:HiddenField ID="hfEmpNo" runat="server" />
                        <asp:HiddenField ID="hfOrgCd" runat="server" />
                    </span>
					<span class="inpbox" id="spanCarNo" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">차량번호</label><br />
						<asp:TextBox ID="txbCarNo" runat="server" MaxLength="20" style="width:84% !important"></asp:TextBox>
                    </span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnSelect" type="button" class="btn-green" style="border-style:none;margin-right:10px;" value="조회" />
                        <input id="btnRegLog" type="button" class="btn-green last" style="border-style:none;" value="작업일보등록" />
					</span>
				</fieldset>
            </div>
            <!-- S: scrollBox -->
            <div class="scrollbox">
			    <div class="requestlist">
					<strong style="margin-top:2px;margin-right:10px;float:left">작업일보</strong>
                    <!-- S:datalist -->
                    <div class="datalist">
			            <!-- S:scrollbox -->
			            <div class="scrollbox">
                            <div id="ktsGrid"></div>
                            <asp:HiddenField ID="hdfWorkType" runat="server" />
                            <asp:HiddenField ID="hdfSvcType" runat="server" />
                            <asp:HiddenField ID="hdfCableType" runat="server" />
                            <asp:HiddenField ID="hdfBldType" runat="server" />
                            <asp:HiddenField ID="hdfPositionType" runat="server" />
                        </div>
			            <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
				</div>
		        <!-- E:requestlist -->
			</div>
		    <!-- Escrollbox -->
        </div>
		<!-- E:page-clothes-request -->

    </div>
	<!-- E: contentsarea -->
</asp:Content>
