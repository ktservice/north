﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Board.Master" AutoEventWireup="true" CodeBehind="BoardWriteM.aspx.cs" Inherits="KTSSolutionWeb.BoardWriteM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">        
        function RegCheck() {
            if (confirm("게시글을 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function DelCheck() {
            if (confirm("현재 게시글을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function DelFileCheck() {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function FileDownload() {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }

        function AddFileUpload() {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:60%;");

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "document.getElementById('fu" + AddFileUpload.lastAssignedId + "-value').value=this.value;");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.innerText = "찾아보기";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        }

        function BoardList() {
            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/BoardListM");

            form.submit();
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<h2>글 등록</h2>
        <div class="boardwrite">
            <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <colgroup>
                            <col style="width:20%" />
                            <col style="width:80%" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>제목</th>
                                <td>
                                    <p class="inpbox">
                                        <asp:TextBox ID="txbTitle" runat="server" MaxLength="50" ></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th>구분</th>
                                <td>
                                    <p class="optionbox">
                                        <asp:DropDownList ID="ddlBoardType" runat="server"></asp:DropDownList>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th>내용</th>
                                <td>
                                    <textarea id="txaContent" runat="server" maxlength="8000" style="padding-top:5px;padding-bottom:5px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>파일추가</th>
                                <td>
                                    <div id="filebox">
                                        <p class="filebox">
                                            <input type="text" id="fu1-value" style="width:60%" />
                                            <span class="file">
                                                <input type="file" id="fu1" name="fu1" onchange="document.getElementById('fu1-value').value=this.value;" /> 
                                                <label for="fu1">찾아보기</label>
                                            </span>
                                            <button type="button" onclick="AddFileUpload();" class="btn-replysave" style="float:right;">추가</button>
                                        </p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th style="vertical-align:middle;">첨부파일</th>
                                <td>
                                    <div class="scrollbox" id="divFileList" runat="server" style="max-height:90px;overflow-y:auto;">
                                        <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptFile" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                                        <p><a id="btnDownload" runat="server" onserverclick="BtnDownload_ServerClick" ><%# Eval("FILENM") %></a>&nbsp;<button type="button" id="btnDelete" runat="server" onclick="return DelFileCheck();" onserverclick="BtnDelete_ServerClick" style="color:black;">삭제</button></p>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnRegBoard" />
                    <asp:PostBackTrigger ControlID="btnDelBoard" />
                </Triggers>
            </asp:UpdatePanel>
                
            <!-- S: button -->
            <div style="margin-top:10px;text-align:center">
                <asp:UpdatePanel ID="updPanelBtn" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button id="btnRegBoard" runat="server" OnClientClick="return RegCheck();" style="margin-right:5px" class="btn-green" BorderStyle="None" Text="등록" />
                        <asp:Button id="btnDelBoard" runat="server" OnClientClick="return DelCheck();" style="margin-right:5px" class="btn-black" BorderStyle="None" Text="삭제" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button id="btnFileDownload" OnClick="BtnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
            </div>
            <!-- //E: button -->
        </div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>