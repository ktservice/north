﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="OrderInfoByEmp.aspx.cs" Inherits="KTSSolutionWeb.OrderInfoByEmp" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">  
        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'AcceptOk') {
                            alert('선택한 오더를 수령했습니다.');
                        }
						else if (callGb == 'NonAcceptOk') {
                            alert('선택한 오더를 수령 거부했습니다.');
                            $("#<%=txbReason.ClientID %>").val('');
                        }

                        $("#divModal").hide();
                        $("#MainContent_btnSelect").click();
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }

            callGb = '';
        };

        var fn_ChkAccept = function (status) {

            var msg = "";

            if (status == "Accept") {
                msg = "현재 오더를 수령하시겠습니까?";
            }
            else if (status == "NonAccept") {
                msg = "현재 오더를 수령 거부하시겠습니까?";
            }
            else {
                alert("잘못된 접근입니다.");
                window.close();
            }

			if (confirm(msg)) {
                if (status == "NonAccept") {
					var reason = $("#<%=txbReason.ClientID %>").val();

                    if (reason.length == 0) {
						alert("수령거부 사유를 입력해주세요.");
					}
					else {
						fn_NonAccect();
					}
                }
                else {
                    fn_Accect();
                }
            }
        }

        var fn_Accect = function () {
            var odrno = $("#<%=hfOrderNo.ClientID %>").val();

            if (odrno.length == 0) {
                alert("잘못된 접근입니다.");
                window.close();
            }
            else {
                var aData = [];

                aData[0] = odrno;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetAccect');
                callGb = 'AcceptOk';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        var fn_NonAccect = function () {
            var odrno = $("#<%=hfOrderNo.ClientID %>").val();
            var reason = $("#<%=txbReason.ClientID %>").val();

            if (odrno.length == 0 || reason.length == 0) {
                alert("잘못된 접근입니다.");
                window.close();
            }
            else {
                var aData = [];

                aData[0] = odrno;
                aData[1] = reason;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetNonAccect');
                callGb = 'NonAcceptOk';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        var fn_PageMoveDetail = function (OdrNo, RcvType) {

            var form;

            if (RcvType == "K") {
                var param = {
                    pORDERNO: OdrNo
                };

                form = createForm("/Mobile/VanOrderInfoByOrderNo", param);
			}
            else {
                var param = {
                    pORDERNUM: OdrNo
                };

                form = createForm("/Mobile/OrderInfoByOrderNum", param);
            }


            form.submit();
        }

        var fn_ModalOdrInfo = function(OdrNo) {
			$("#<%=hfOrderNo.ClientID %>").val(OdrNo);

            $("#btnReg").attr("onclick", "fn_ChkAccept('NonAccept')");
            $("#<%=txbReason.ClientID %>").attr("placeholder", "수령 거부 시 사유를 입력해주세요.");

			$("#divModal").show();
        }

        var fn_ModalClose = function () {
			$("#divModal").hide();
		}
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div class="page-clothes-request">
		    <h2>오더 정보 조회</h2>
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>조회 조건</legend>
					<span class="inpbox" style="width:100%; margin:11px 0 11px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:80px;">등록일자</label>
						<input type="text" id="txbStDate" runat="server" class="date" readonly="true" style="width:23% !important;min-width:90px;padding: 0; text-align:center;" />
						<label style="width:16px;padding-left:3px;padding-right:3px;">~</label>
						<input type="text" id="txbEnDate" runat="server" class="date" readonly="true" style="width:23% !important;min-width:90px;padding: 0; text-align:center;" />
					</span>
					<span class="optionbox" style="width:70%; margin:11px 0 11px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:80px;">오더구분</label>
						<asp:DropDownList ID="ddlOrderCode" runat="server" style="width:50% !important">
							<asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
							<asp:ListItem Text="VAN" Value="1"></asp:ListItem>
							<asp:ListItem Text="K-VIZERO" Value="2"></asp:ListItem>
							<asp:ListItem Text="BIZ" Value="3"></asp:ListItem>
							<asp:ListItem Text="Gem" Value="4"></asp:ListItem>
						</asp:DropDownList>
					</span>
					<span style="width:30%; margin:11px 0 11px 0; font-size:0; text-align:center;">
						<asp:Button id="btnSelect" runat="server" OnClick="btnSelect_Click" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;" Text="조회" />
					</span>
				</fieldset>
			</div>
            <!-- S: scrollBox -->
            <div class="scrollbox">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<table>
									<thead>
										<th>No.</th>
										<th>등록일자</th>
										<th>오더구분</th>
										<th>업무구분</th>
										<th>이름/업체명</th>
										<th>연락처/장소명</th>
										<th>방문요청일</th>
										<th>오더현황</th>
										<th>수령일시</th>
										<th>완료일시</th>
									</thead>
									<tbody>
                                        <asp:Repeater ID="rptResult1" runat="server">
                                            <ItemTemplate>
												<tr id="tr" runat="server">
													<td><%# Eval("NUM") %></td>
													<td><%# Eval("SUMUPDATE") %></td>
													<td>
														<%# Eval("ORDERNAME") %>
														<asp:Label ID="lblOrderNum" runat="server" Text='<%# Eval("ORDERNUM") %>' Visible="false"></asp:Label>
														<asp:Label ID="lblWorkState" runat="server" Text='<%# Eval("WORKSTATE") %>' Visible="false"></asp:Label>
														<asp:Label ID="lblRcvType" runat="server" Text='<%# Eval("RCVTYPE") %>' Visible="false"></asp:Label>
													</td>
													<td><%# Eval("WORKTYPE") %></td>
													<td><%# Eval("FRANCHISENM") %></td>
													<td><%# Eval("PHONENO") %></td>
													<td><%# Eval("HOPEDT") %></td>
													<td><%# Eval("WORKSTATENM") %></td>
													<td><%# Eval("ACCEPTDT") %></td>
													<td><%# Eval("SUCCDT") %></td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
								<asp:AsyncPostBackTrigger ControlID="paging" />
							</Triggers>
						</asp:updatepanel>
					</div>
				</div>
			</div>
			<uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
			<!--E: scrollBox -->
			
            <!-- S: divModal -->
			<div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:260px;top:20%;left:0;background:rgb(255, 255, 255); border-radius:10px;">
				<div class="requestlist" style="width:90%;text-align:center;margin-left:5%">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom: 15px">
						<asp:HiddenField ID="hfOrderNo" runat="server" />
						<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0; text-align:left;">
							<label style="margin:0 !important;padding-top:7px;margin-left:0;">사유</label>
							<asp:TextBox ID="txbReason" runat="server" MaxLength="8000" TextMode="MultiLine"></asp:TextBox> 
						</span>
					</div>
					<input id="btnAccept" type="button" class="btn-green" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ChkAccept('Accept');" value="수령" />
					<input id="btnReg" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" value="거부" />
					<input id="btnModalClose" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ModalClose();" value="닫기" />
				</div>	
			</div>		
			<!--E: divModal -->
        </div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>