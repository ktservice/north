﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;

namespace KTSSolutionWeb
{
    public partial class InspectionListByCompM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetDDLLv1Grp();

                    SetPageInit();
                }
            }
        }
        private void SetDDLLv1Grp()
        {
            DataSet ds = null;

            try
            {
                ddlLv1Grp.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "INSPECTIONLV1GRPLIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    ddlLv1Grp.Items.Add(new ListItem("전체", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["LV1GRPNM"].ToString().Equals(""))
                        {
                            continue;
                        }

                        ddlLv1Grp.Items.Add(new ListItem(ds.Tables[0].Rows[i]["LV1GRPNM"].ToString(), ds.Tables[0].Rows[i]["LV1GRPCD"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("대분류를 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetPageInit()
        {
            try
            {

                string strStDt = Request.Form["pSTDT"] == null ? "" : Request.Form["pSTDT"].ToString();
                string strEnDt = Request.Form["pENDT"] == null ? "" : Request.Form["pENDT"].ToString();
                string strActCode = Request.Form["pACTCODE"] == null ? "" : Request.Form["pACTCODE"].ToString();
                string strLv1Grp = Request.Form["pLV1CD"] == null ? "" : Request.Form["pLV1CD"].ToString();

                if (strStDt != "" && strEnDt != "")
                {
                    this.txbStDt.Text = strStDt;
                    this.txbEnDt.Text = strEnDt;
                }
                else
                {
                    this.txbStDt.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                    this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }

                this.hdfActCode.Value = strActCode;

                if (strLv1Grp != "")
                {
                    this.ddlLv1Grp.SelectedValue = strLv1Grp;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONINFOLISTBYBCOMP";
            string jobGb = "GET";

            string orgcd = HttpContext.Current.Session["ORGCD"].ToString();
            string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();;

            List<string> aList = new List<string>();

            //aData[0] = $("#<%= txbStDt.ClientID %>").val();
            //aData[1] = $("#<%= txbEnDt.ClientID %>").val();
            //aData[2] = $("#ddlActCode option:selected").val();
            //aData[3] = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();

            foreach (string strData in aData)
            {
                aList.Add(strData);
            }

            aList.Add("");
            aList.Add("");
            aList.Add("CHK_DATE");
            aList.Add(strEmpNo);
            aList.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            DataSet dataSet = new DataSet();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                dataSet = dbTransaction.GetDataSet();

                result = Utility.DataSetToJson(dataSet);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}