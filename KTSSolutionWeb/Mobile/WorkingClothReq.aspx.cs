﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Mobile;
using KTS.KTSSolution.BSL.WorkingClothes;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class WorkingClothReq : PageBase
    {
        #region Properties

        #region Years
        private string Years
        {
            get
            {
                if (ViewState["Years"] != null)
                {
                    return ViewState["Years"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["Years"] = value;
            }
        }
        #endregion

        #region PointSeq
        private string PointSeq
        {
            get
            {
                if (ViewState["PointSeq"] != null)
                {
                    return ViewState["PointSeq"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["PointSeq"] = value;
            }
        }
        #endregion

        #region EmpNo
        private string EmpNo
        {
            get
            {
                if (ViewState["EmpNo"] != null)
                {
                    return ViewState["EmpNo"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["EmpNo"] = value;
            }
        }
        #endregion

        #region ReqType
        private string ReqType
        {
            get
            {
                if (ViewState["ReqType"] != null)
                {
                    return ViewState["ReqType"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ReqType"] = value;
            }
        }
        #endregion

        #region nReqCnt
        private int nReqCnt
        {
            get
            {
                if (ViewState["nReqCnt"] != null)
                {
                    return int.Parse(ViewState["nReqCnt"].ToString());
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["nReqCnt"] = value;
            }
        }
        #endregion

        #region ds
        private DataSet ds
        {
            get
            {
                if (ViewState["ds"] != null)
                {
                    return (DataSet)ViewState["ds"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["ds"] = value;
            }
        }
        #endregion

        #region dtList
        private DataTable dtList
        {
            get
            {
                if (ViewState["dtList"] != null)
                {
                    return (DataTable)ViewState["dtList"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["dtList"] = value;
            }
        }
        #endregion

        #region dtAdd
        private DataTable dtAdd
        {
            get
            {
                if (ViewState["dtAdd"] != null)
                {
                    return (DataTable)ViewState["dtAdd"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["dtAdd"] = value;
            }
        }
        #endregion

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    GetDataList();
                }
            }
        }

        private void GetDataList()
        {
            DataSet dsList = new DataSet();

            try
            {
                nReqCnt = 0;
                string ImageGBN = "";

                Years = Request.Form["YEARS"] == null ? "" : Request.Form["YEARS"].ToString();
                PointSeq = Request.Form["POINTSEQ"] == null ? "" : Request.Form["POINTSEQ"].ToString();
                EmpNo = Request.Form["EMPNO"] == null ? "" : Request.Form["EMPNO"].ToString();
                ReqType = Request.Form["REQTYPE"] == null ? "N" : Request.Form["REQTYPE"].ToString();

                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    ds = mgmt.GetRequestCnt(Years, PointSeq, EmpNo, ReqType);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (!ds.Tables[0].Rows[0]["REQCNT"].ToString().Equals("0"))
                            {
                                nReqCnt = int.Parse(ds.Tables[0].Rows[0]["REQCNT"].ToString());
                            }
                        }
                    }

                    if (nReqCnt > 0)
                    {
                        btnReg.Text = "수정";

                        if (ReqType.Equals("B") && ds.Tables[1].Rows[0]["DELYN"].ToString().Equals("N"))
                        {
                            btnDel.Visible = false;
                        }
                        else
                        {
                            btnDel.Visible = true;
                        }
                    }
                    else
                    {
                        btnReg.Text = "신청";
                        btnDel.Visible = false;
                    }

                    if (ReqType.Equals("A"))
                    {
                        //추가 신청
                        ds = mgmt.GetAddClothList(Years, PointSeq, EmpNo);
                        dsList = mgmt.GetRequestAddClothDetailList(Years, PointSeq, EmpNo);
                        btnAddList.Style.Remove("display");
                        btnAddList.Style.Add("display", "block");

                        ReqTitle.InnerText = "추가 신청";

                        ImageGBN = dsList.Tables[0].Rows[0]["CLOTHGBN"].ToString();
                    }
                    else
                    {
                        dsList = mgmt.GetRequestClothDetailList(Years, PointSeq, EmpNo, ReqType);
                        btnAddList.Style.Remove("display");
                        btnAddList.Style.Add("display", "none");

                        ReqTitle.InnerText = "기본 신청";

                        ImageGBN = dsList.Tables[0].Rows[0]["CLOTHGBN"].ToString();
                    }

                    if (ImageGBN == "하계")
                    {
                        winterClothimg.Visible = false;
                    }
                    else
                    {
                        summerClothImg.Visible = false;
                    };
                        
                    if (dsList.Tables.Count > 0)
                    {
                        if (dsList.Tables[0].Rows.Count > 0)
                        {
                            dtAdd = dsList.Tables[0].Clone();

                            DataRow drAdd = dtAdd.NewRow();

                            drAdd["REQUESTID"] = dsList.Tables[0].Rows[0]["REQUESTID"].ToString();
                            drAdd["ORGFULLNM"] = dsList.Tables[0].Rows[0]["ORGFULLNM"].ToString();
                            drAdd["EMPNO"] = dsList.Tables[0].Rows[0]["EMPNO"].ToString();
                            drAdd["EMPNM"] = dsList.Tables[0].Rows[0]["EMPNM"].ToString();
                            drAdd["YEARS"] = dsList.Tables[0].Rows[0]["YEARS"].ToString();
                            drAdd["POINTSEQ"] = dsList.Tables[0].Rows[0]["POINTSEQ"].ToString();
                            drAdd["TITLE"] = dsList.Tables[0].Rows[0]["TITLE"].ToString();
                            drAdd["CLOTHGBN"] = dsList.Tables[0].Rows[0]["CLOTHGBN"].ToString();

                            dtAdd.Rows.Add(drAdd);

                            txbOrgFullNm.Text = Session["ORGFULLNM"].ToString();
                            txbTitle.Text = dsList.Tables[0].Rows[0]["TITLE"].ToString();

                            if (Session["JOBNM2"].ToString().ToLower().Equals("kte") && !ReqType.Equals("N"))
                            {
                                fieldset.Visible = true;

                                txbTotPoint.Text = dsList.Tables[1].Rows[0]["TOTALPOINT"].ToString();
                                txbUsePoint.Text = dsList.Tables[1].Rows[0]["USEPOINT"].ToString();
                                txbRestPoint.Text = dsList.Tables[1].Rows[0]["RESTPOINT"].ToString();
                            }
                            else
                            {
                                fieldset.Visible = false;
                            }

                            if (ReqType.Equals("A"))
                            {
                                dtList = dsList.Tables[0].Clone();
                                DataRow[] drList = dsList.Tables[0].Select("DETAILSEQ > 0");

                                if (drList.Length > 0)
                                {
                                    for (int i = 0; i < drList.Length; i++)
                                    {
                                        DataRow dr = dtList.NewRow();

                                        dr["REQUESTID"] = drList[i]["REQUESTID"].ToString();
                                        dr["ORGFULLNM"] = drList[i]["ORGFULLNM"].ToString();
                                        dr["EMPNO"] = drList[i]["EMPNO"].ToString();
                                        dr["EMPNM"] = drList[i]["EMPNM"].ToString();
                                        dr["YEARS"] = drList[i]["YEARS"].ToString();
                                        dr["POINTSEQ"] = drList[i]["POINTSEQ"].ToString();
                                        dr["DETAILSEQ"] = drList[i]["DETAILSEQ"].ToString();
                                        dr["TITLE"] = drList[i]["TITLE"].ToString();
                                        dr["CLOTHGBN"] = drList[i]["CLOTHGBN"].ToString();
                                        dr["CLOTHTYPE"] = drList[i]["CLOTHTYPE"].ToString();
                                        dr["CLOTHSIZE"] = drList[i]["CLOTHSIZE"].ToString();
                                        dr["REQCNT"] = drList[i]["REQCNT"].ToString();
                                        dr["USEPOINT"] = drList[i]["USEPOINT"].ToString();
                                        dr["REQSTATUS"] = drList[i]["REQSTATUS"].ToString();

                                        dtList.Rows.Add(dr);
                                    }
                                }
                            }
                            else
                            {
                                dtList = dsList.Tables[0].Copy();
                            }
                        }

                        rptResult.DataSource = dtList;
                        rptResult.DataBind();

                        SetRptList();

                        updPanelInfo.Update();
                        updPanel1.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }

                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (dsList != null)
                    dsList.Dispose();
            }
        }
        private void SetRptList()
        {
            try
            {
                bool bViewPoint = false;
                bool bChkEnable = false;
                bool bNoReqChecked = false;

                if (this.Session["JOBNM2"].ToString().ToLower().Equals("kte") && !ReqType.Equals("N"))
                {
                    bViewPoint = true;
                }
                else
                {
                    bViewPoint = false;
                }

                if (btnReg.Text.Equals("수정"))
                {
                    bChkEnable = true;
                }

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    Label lblDetailSeq = (Label)rptResult.Items[i].FindControl("lblDetailSeq");
                    TextBox txbClothType = (TextBox)rptResult.Items[i].FindControl("txbClothType");
                    DropDownList ddlAddClothType = (DropDownList)rptResult.Items[i].FindControl("ddlAddClothType");

                    TextBox txbSize = (TextBox)rptResult.Items[i].FindControl("txbSize");
                    DropDownList ddlSize1 = (DropDownList)rptResult.Items[i].FindControl("ddlSize1");
                    DropDownList ddlSize2 = (DropDownList)rptResult.Items[i].FindControl("ddlSize2");
                    TextBox txbReqCnt = (TextBox)rptResult.Items[i].FindControl("txbReqCnt");
                    HtmlTableRow tr = (HtmlTableRow)rptResult.Items[i].FindControl("tr");
                    HtmlTableCell thPass = (HtmlTableCell)rptResult.Items[i].FindControl("thPass");
                    HtmlTableCell tdPass = (HtmlTableCell)rptResult.Items[i].FindControl("tdPass");
                    HtmlTableCell thPoint = (HtmlTableCell)rptResult.Items[i].FindControl("thPoint");
                    HtmlTableCell tdPoint = (HtmlTableCell)rptResult.Items[i].FindControl("tdPoint");
                    TextBox txbPoint = (TextBox)rptResult.Items[i].FindControl("txbPoint");
                    CheckBox chkNoReq = (CheckBox)rptResult.Items[i].FindControl("chkNoReq");
                    TextBox txbReqStatus = (TextBox)rptResult.Items[i].FindControl("txbReqStatus");


                    string strClothType = "";

                    if (ReqType.Equals("A"))
                    {
                        txbClothType.Visible = false;
                        ddlAddClothType.Visible = true;
                        txbReqCnt.ReadOnly = false;
                        chkNoReq.Enabled = false;
                        chkNoReq.Checked = false;

                        ddlAddClothType.Items.Add(new ListItem("선택", ""));

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            ddlAddClothType.Items.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                        }

                        if (lblDetailSeq.Text.Length > 0)
                        {
                            ddlAddClothType.SelectedValue = lblDetailSeq.Text;
                        }
                        else
                        {
                            ddlAddClothType.SelectedIndex = 0;
                        }

                        if (!ddlAddClothType.SelectedValue.Equals(""))
                        {
                            DataRow[] drList = ds.Tables[0].Select(string.Format("DETAILSEQ = {0}", ddlAddClothType.SelectedValue));

                            if (drList.Length > 0 && txbReqCnt.Text.Length > 0)
                            {
                                int nPoint = 0;
                                int nCnt = 0;

                                int.TryParse(drList[0]["USEPOINT"].ToString(), out nPoint);
                                int.TryParse(txbReqCnt.Text, out nCnt);

                                txbPoint.Text = String.Format("{0:#,###}", (nPoint * nCnt));
                            }
                            else
                            {
                                txbPoint.Text = "0";
                            }
                        }
                        else
                        {
                            txbPoint.Text = "0";
                        }

                        AsyncPostBackTrigger asyncPost = new AsyncPostBackTrigger();
                        asyncPost.ControlID = txbReqCnt.UniqueID;
                        asyncPost.EventName = "TextChanged";

                        updPanel1.Triggers.Add(asyncPost);

                        AsyncPostBackTrigger asyncPost2 = new AsyncPostBackTrigger();
                        asyncPost2.ControlID = ddlAddClothType.UniqueID;
                        asyncPost2.EventName = "SelectedIndexChanged";

                        updPanel1.Triggers.Add(asyncPost2);

                        strClothType = ddlAddClothType.SelectedItem.Text;
                    }
                    else if (ReqType.Equals("N"))
                    {
                        txbClothType.Visible = true;
                        ddlAddClothType.Visible = false;
                        txbReqCnt.ReadOnly = true;
                        chkNoReq.Enabled = false;
                        thPass.Visible = false;
                        tdPass.Visible = false;

                        strClothType = txbClothType.Text;
                    }
                    else
                    {
                        txbClothType.Visible = true;
                        ddlAddClothType.Visible = false;
                        txbReqCnt.ReadOnly = true;
                        chkNoReq.Enabled = true;
                        chkNoReq.Checked = (txbReqStatus.Text == "W" || txbReqStatus.Text == "PF") ? true : false;
                        thPass.Visible = true;
                        tdPass.Visible = true;

                        strClothType = txbClothType.Text;
                    }

                    if (chkNoReq.Enabled)
                    {
                        AsyncPostBackTrigger asyncPost = new AsyncPostBackTrigger();
                        asyncPost.ControlID = chkNoReq.UniqueID;
                        asyncPost.EventName = "CheckedChanged";

                        updPanel1.Triggers.Add(asyncPost);

                        if (bChkEnable)
                        {
                            if (chkNoReq.Checked)
                            {
                                txbSize.Text = "";
                                txbSize.Enabled = false;
                                ddlSize1.SelectedValue = "";
                                ddlSize1.Enabled = false;
                                ddlSize2.SelectedValue = "";
                                ddlSize2.Enabled = false;
                                bNoReqChecked = true;
                            }
                            else
                            {
                                if (txbSize.Visible) txbSize.Enabled = true;
                                if (ddlSize1.Visible) ddlSize1.Enabled = true;
                                if (ddlSize2.Visible) ddlSize2.Enabled = true;
                            }
                        }
                    }

                    if (!bViewPoint && !chkNoReq.Enabled)
                    {
                        tr.Visible = false;
                        tdPass.ColSpan = 1;
                        tdPoint.ColSpan = 1;
                    }
                    else
                    {
                        tr.Visible = true;
                        thPass.Visible = chkNoReq.Enabled;
                        thPass.Visible = chkNoReq.Enabled;
                        thPoint.Visible = bViewPoint;
                        tdPoint.Visible = bViewPoint;

                        if (bViewPoint && !chkNoReq.Enabled)
                        {
                            tdPass.ColSpan = 1;
                            tdPoint.ColSpan = 3;
                        }
                        else if (!bViewPoint && chkNoReq.Enabled)
                        {
                            tdPass.ColSpan = 3;
                            tdPoint.ColSpan = 1;
                        }
                        else
                        {
                            tdPass.ColSpan = 1;
                            tdPoint.ColSpan = 1;
                        }
                    }

                    string[] strSize = txbSize.Text.Split('/');

                    SetDDLSize(strClothType, txbSize, ddlSize1, ddlSize2);

                    if (strClothType.Equals("상의") || strClothType.Equals("점퍼") || strClothType.Equals("조끼")
                        || strClothType.Equals("티셔츠") || strClothType.ToUpper().Equals("긴팔T") || strClothType.ToUpper().Equals("반팔T")
                        || strClothType.Equals("외피") || strClothType.Equals("내피"))
                    {
                        ddlSize1.SelectedValue = strSize[0];
                    }
                    else if (strClothType.Equals("하의") || strClothType.Contains("바지") || strClothType.Contains("방한바지"))
                    {
                        if (strSize.Length > 1)
                        {
                            ddlSize1.SelectedValue = strSize[0];
                            ddlSize2.SelectedValue = strSize[1];
                        }
                        else
                        {
                            ddlSize1.SelectedIndex = 0;
                            ddlSize2.SelectedIndex = 0;
                        }

                    }
                    else if (strClothType.Equals("신발"))
                    {
                        ddlSize1.SelectedValue = strSize[0];
                    }
                    else if (strClothType.Equals("모자") || strClothType.Equals("버프"))
                    {
                        ddlSize1.SelectedValue = "FREE";
                    }
                }

                if (bNoReqChecked)
                {
                    hdfNoReqCheckedYn.Value = "Y";
                }
                else
                {
                    hdfNoReqCheckedYn.Value = "N";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetDDLSize(string strClothType, TextBox txbSize, DropDownList ddlSize1, DropDownList ddlSize2)
        {
            try
            {
                if (strClothType.Equals("상의") || strClothType.Equals("점퍼") || strClothType.Equals("조끼")
                    || strClothType.Equals("티셔츠") || strClothType.ToUpper().Equals("긴팔T") || strClothType.ToUpper().Equals("반팔T")
                    || strClothType.Equals("외피") || strClothType.Equals("내피"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("선택", ""));

                    ddlSize1.Items.Add(new ListItem("44", "44"));
                    ddlSize1.Items.Add(new ListItem("55", "55"));
                    ddlSize1.Items.Add(new ListItem("66", "66"));
                    ddlSize1.Items.Add(new ListItem("77", "77"));

                    for (int i = 90; i <= 125; i += 5)
                    {
                        ddlSize1.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }

                    ddlSize1.Width = Unit.Parse("80px");
                }
                else if (strClothType.Equals("하의") || strClothType.Contains("바지") || strClothType.Contains("방한바지"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = true;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("선택", ""));

                    for (int i = 24; i <= 46; i++)
                    {
                        ddlSize1.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }

                    ddlSize2.Items.Add(new ListItem("선택", ""));
                    ddlSize2.Items.Add(new ListItem("160cm이하", "SSS"));
                    ddlSize2.Items.Add(new ListItem("160~165cm", "SS"));
                    ddlSize2.Items.Add(new ListItem("165~170cm", "S"));
                    ddlSize2.Items.Add(new ListItem("170~175cm", "M"));
                    ddlSize2.Items.Add(new ListItem("175~180cm", "L"));
                    ddlSize2.Items.Add(new ListItem("180~185cm", "LL"));
                    ddlSize2.Items.Add(new ListItem("185~190cm", "LLL"));
                    ddlSize2.Items.Add(new ListItem("FREE", "FREE"));
                }
                else if (strClothType.Equals("신발"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("선택", ""));

                    for (int i = 230; i <= 300; i += 5)
                    {
                        ddlSize1.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    }

                    ddlSize1.Width = Unit.Parse("80px");
                }
                else if (strClothType.Equals("모자") || strClothType.Equals("버프"))
                {
                    txbSize.Visible = false;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = true;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();

                    ddlSize1.Items.Add(new ListItem("FREE", "FREE"));

                    ddlSize1.Width = Unit.Parse("80px");
                }
                else if (strClothType.Equals(""))
                {
                    txbSize.Visible = true;
                    txbSize.ReadOnly = true;

                    ddlSize1.Visible = false;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();
                }
                else
                {
                    txbSize.Visible = true;
                    txbSize.ReadOnly = false;

                    ddlSize1.Visible = false;
                    ddlSize2.Visible = false;

                    ddlSize1.Items.Clear();
                    ddlSize2.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnAddList_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                DataTable dtAddList = dtList.Clone();

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    dr = dtAddList.NewRow();

                    Label lblRequestID = (Label)rptResult.Items[i].FindControl("lblRequestID");
                    TextBox txbSize = (TextBox)rptResult.Items[i].FindControl("txbSize");
                    DropDownList ddlSize1 = (DropDownList)rptResult.Items[i].FindControl("ddlSize1");
                    DropDownList ddlSize2 = (DropDownList)rptResult.Items[i].FindControl("ddlSize2");
                    DropDownList ddlAddClothType = (DropDownList)rptResult.Items[i].FindControl("ddlAddClothType");
                    TextBox txbReqCnt = (TextBox)rptResult.Items[i].FindControl("txbReqCnt");

                    string strSize = "";

                    if (txbSize.Visible)
                    {
                        strSize = txbSize.Text;
                    }
                    else
                    {
                        strSize = ddlSize1.SelectedValue;

                        if (ddlSize2.Visible)
                        {
                            strSize += "/" + ddlSize2.SelectedValue;
                        }
                    }

                    dr["REQUESTID"] = lblRequestID.Text;
                    dr["ORGFULLNM"] = dtAdd.Rows[0]["ORGFULLNM"].ToString();
                    dr["EMPNO"] = dtAdd.Rows[0]["EMPNO"].ToString();
                    dr["EMPNM"] = dtAdd.Rows[0]["EMPNM"].ToString();
                    dr["YEARS"] = dtAdd.Rows[0]["YEARS"].ToString();
                    dr["POINTSEQ"] = dtAdd.Rows[0]["POINTSEQ"].ToString();
                    dr["TITLE"] = dtAdd.Rows[0]["TITLE"].ToString();
                    dr["CLOTHGBN"] = dtAdd.Rows[0]["CLOTHGBN"].ToString();
                    dr["DETAILSEQ"] = ddlAddClothType.SelectedValue;
                    dr["CLOTHSIZE"] = strSize;
                    dr["REQCNT"] = txbReqCnt.Text;

                    dtAddList.Rows.Add(dr);
                }

                dtList = dtAddList;

                dr = dtList.NewRow();

                dr["REQUESTID"] = dtAdd.Rows[0]["REQUESTID"].ToString();
                dr["ORGFULLNM"] = dtAdd.Rows[0]["ORGFULLNM"].ToString();
                dr["EMPNO"] = dtAdd.Rows[0]["EMPNO"].ToString();
                dr["EMPNM"] = dtAdd.Rows[0]["EMPNM"].ToString();
                dr["YEARS"] = dtAdd.Rows[0]["YEARS"].ToString();
                dr["POINTSEQ"] = dtAdd.Rows[0]["POINTSEQ"].ToString();
                dr["TITLE"] = dtAdd.Rows[0]["TITLE"].ToString();
                dr["CLOTHGBN"] = dtAdd.Rows[0]["CLOTHGBN"].ToString();

                dtList.Rows.Add(dr);

                rptResult.DataSource = dtList;
                rptResult.DataBind();

                SetRptList();

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }

        protected void ddlAddClothType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlAddClothType = (DropDownList)sender;
                TextBox txbReqCnt = (TextBox)ddlAddClothType.Parent.FindControl("txbReqCnt");
                TextBox txbPoint = (TextBox)ddlAddClothType.Parent.FindControl("txbPoint");
                TextBox txbSize = (TextBox)ddlAddClothType.Parent.FindControl("txbSize");
                DropDownList ddlSize1 = (DropDownList)ddlAddClothType.Parent.FindControl("ddlSize1");
                DropDownList ddlSize2 = (DropDownList)ddlAddClothType.Parent.FindControl("ddlSize2");


                string strClothType = ddlAddClothType.SelectedItem.Text;

                if (Session["JOBNM2"].ToString().ToLower().Equals("kte") && !ReqType.Equals("N"))
                {
                    if (ddlAddClothType.SelectedValue.Equals("") && txbReqCnt.Text.Length > 0)
                    {
                        txbPoint.Text = "0";
                    }
                    else
                    {
                        DataRow[] dr = ds.Tables[0].Select(string.Format("DETAILSEQ = '{0}'", ddlAddClothType.SelectedValue));

                        if (dr.Length > 0)
                        {
                            int nPoint = 0;
                            int nCnt = 0;
                            int.TryParse(dr[0]["USEPOINT"].ToString(), out nPoint);
                            int.TryParse(txbReqCnt.Text, out nCnt);

                            txbPoint.Text = String.Format("{0:#,###}", (nPoint * nCnt));
                        }
                        else
                        {
                            txbPoint.Text = "0";
                        }
                    }
                }

                SetDDLSize(strClothType, txbSize, ddlSize1, ddlSize2);

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    DropDownList ClothType = (DropDownList)rptResult.Items[i].FindControl("ddlAddClothType");

                    AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
                    trigger1.ControlID = ClothType.UniqueID;
                    trigger1.EventName = "SelectedIndexChanged";
                    updPanel1.Triggers.Add(trigger1);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void txbReqCnt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["JOBNM2"].ToString().ToLower().Equals("kte") && !ReqType.Equals("N"))
                {
                    TextBox txbReqCnt = (TextBox)sender;
                    DropDownList ddlAddClothType = (DropDownList)txbReqCnt.Parent.FindControl("ddlAddClothType");
                    TextBox txbPoint = (TextBox)txbReqCnt.Parent.FindControl("txbPoint");

                    if (ddlAddClothType.SelectedValue.Equals("") && txbReqCnt.Text.Length > 0)
                    {
                        txbPoint.Text = "0";
                    }
                    else
                    {
                        DataRow[] dr = ds.Tables[0].Select(string.Format("DETAILSEQ = '{0}'", ddlAddClothType.SelectedValue));

                        if (dr.Length > 0)
                        {
                            int nPoint = 0;
                            int nCnt = 0;
                            int.TryParse(dr[0]["USEPOINT"].ToString(), out nPoint);
                            int.TryParse(txbReqCnt.Text, out nCnt);

                            txbPoint.Text = String.Format("{0:#,###}", (nPoint * nCnt));
                        }
                        else
                        {
                            txbPoint.Text = "0";
                        }
                    }

                    for (int i = 0; i < rptResult.Items.Count; i++)
                    {
                        DropDownList ReqCnt = (DropDownList)rptResult.Items[i].FindControl("ddlAddClothType");

                        AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
                        trigger1.ControlID = txbReqCnt.UniqueID;
                        trigger1.EventName = "TextChanged";
                        updPanel1.Triggers.Add(trigger1);
                    }

                    updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnReg_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> lstData = new List<string>();

                int nPoint = 0;

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    Label lblRequestID = (Label)rptResult.Items[i].FindControl("lblRequestID");
                    Label lblDetailSeq = (Label)rptResult.Items[i].FindControl("lblDetailSeq");
                    TextBox txbSize = (TextBox)rptResult.Items[i].FindControl("txbSize");
                    DropDownList ddlSize1 = (DropDownList)rptResult.Items[i].FindControl("ddlSize1");
                    DropDownList ddlSize2 = (DropDownList)rptResult.Items[i].FindControl("ddlSize2");
                    DropDownList ddlAddClothType = (DropDownList)rptResult.Items[i].FindControl("ddlAddClothType");
                    TextBox txbReqCnt = (TextBox)rptResult.Items[i].FindControl("txbReqCnt");
                    TextBox txbPoint = (TextBox)rptResult.Items[i].FindControl("txbPoint");
                    CheckBox chkNoReq = (CheckBox)rptResult.Items[i].FindControl("chkNoReq");
                    TextBox txbReqStatus = (TextBox)rptResult.Items[i].FindControl("txbReqStatus");

                    if (lblDetailSeq.Text.Equals("") && ddlAddClothType.SelectedValue.Equals(""))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('잘못된 접근입니다..');", true);
                        lstData.Clear();
                        break;
                    }
                    else
                    {
                        if (ReqType.Equals("A"))
                        {
                            //추가

                            int nCnt = 0;
                            int.TryParse(txbReqCnt.Text, out nCnt);

                            if (ddlAddClothType.SelectedValue.Equals(""))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('종류를 선택해주세요.');", true);
                                lstData.Clear();
                                break;
                            }
                            if (txbReqCnt.Text.Equals("") || nCnt == 0)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('수량을 입력해주세요.');", true);
                                lstData.Clear();
                                break;
                            }
                            if (txbSize.Visible && txbSize.Text.Equals(""))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('사이즈를 입력해주세요.');", true);
                                lstData.Clear();
                                break;
                            }
                            if (ddlSize1.Visible && ddlSize1.SelectedValue.Equals(""))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('사이즈를 선택해주세요.');", true);
                                lstData.Clear();
                                break;
                            }
                            if (ddlSize2.Visible && ddlSize2.SelectedValue.Equals(""))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('기장을 선택해주세요.');", true);
                                lstData.Clear();
                                break;
                            }

                            string strSize = "";

                            if (txbSize.Visible)
                            {
                                strSize = txbSize.Text;
                            }
                            else
                            {
                                strSize = ddlSize1.SelectedValue;

                                if (ddlSize2.Visible)
                                {
                                    strSize += "/" + ddlSize2.SelectedValue;
                                }
                            }

                            string strReqStatus = "F";
                            string strData = Years + "|" + PointSeq + "|" + ddlAddClothType.SelectedValue + "|" + txbReqCnt.Text + "|" + strSize + "|" + ReqType + "|" + strReqStatus + "|" + lblRequestID.Text;
                            lstData.Add(strData);
                        }
                        else
                        {
                            //기본, 신규
                            TextBox txbClothType = (TextBox)rptResult.Items[i].FindControl("txbClothType");

                            if (!chkNoReq.Checked)
                            {
                                if (txbSize.Visible && txbSize.Text.Equals(""))
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('사이즈를 입력해주세요.');", true);
                                    lstData.Clear();
                                    break;
                                }
                                if (ddlSize1.Visible && ddlSize1.SelectedValue.Equals(""))
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('사이즈를 선택해주세요.');", true);
                                    lstData.Clear();
                                    break;
                                }
                                if (ddlSize2.Visible && ddlSize2.SelectedValue.Equals(""))
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('기장을 선택해주세요.');", true);
                                    lstData.Clear();
                                    break;
                                }
                            }

                            string strSize = "";

                            string strReqStatus = "";

                            if (!chkNoReq.Checked)
                            {
                                if (txbSize.Visible)
                                {
                                    strSize = txbSize.Text;
                                }
                                else
                                {
                                    strSize = ddlSize1.SelectedValue;

                                    if (ddlSize2.Visible)
                                    {
                                        strSize += "/" + ddlSize2.SelectedValue;
                                    }
                                }

                                strReqStatus = "F";
                            }
                            else
                            {
                                if (ddlSize2.Visible)
                                {
                                    strSize = "/";
                                }

                                strReqStatus = txbReqStatus.Text.Equals("PF") ? "PF" : "W";
                            }

                            string strData = Years + "|" + PointSeq + "|" + lblDetailSeq.Text + "|" + txbReqCnt.Text + "|" + strSize + "|" + ReqType + "|" + strReqStatus + "|" + lblRequestID.Text;
                            lstData.Add(strData);
                        }

                        string strPoint = txbPoint.Text.Length == 0 ? "0" : txbPoint.Text.Replace(",", "");

                        if (Session["JOBNM2"].ToString().ToLower().Equals("kte") && !ReqType.Equals("N"))
                        {
                            nPoint += int.Parse(strPoint);
                        }
                    }
                }

                if (lstData.Count > 0)
                {
                    using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                    {
                        int nRestPoint = txbRestPoint.Text.Replace(",", "").Length == 0 ? 0 : int.Parse(txbRestPoint.Text.Replace(",", ""));

                        if (nPoint > nRestPoint)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "alert('사용가능한 포인트가 부족합니다.');", true);
                        }
                        else
                        {
                            if (nReqCnt > 0)
                            {
                                //수정
                                mgmt.UdpRequestUserCloth(lstData, EmpNo);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "$(\"#updateProgress\").css('display', 'none');alert('수정을 완료했습니다.');window.location.href='/Mobile/WorkingClothInfo';", true);
                            }
                            else
                            {
                                //신청
                                mgmt.InsRequestUserCloth(lstData, EmpNo);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "$(\"#updateProgress\").css('display', 'none');alert('신청을 완료했습니다.');window.location.href='/Mobile/WorkingClothInfo';", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                using (WorkingClothesMgmt mgmt = new WorkingClothesMgmt())
                {
                    if (nReqCnt > 0)
                    {
                        mgmt.DelRequestUserCloth(EmpNo, Years, PointSeq, ReqType);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "alert('삭제를 완료했습니다.');window.location.href='/Mobile/WorkingClothInfo';", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValdationCheck", "alert('잘못된 접근입니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void chkNoReq_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkBox = (CheckBox)sender;
            TextBox txbSize = (TextBox)chkBox.Parent.FindControl("txbSize");
            DropDownList ddlSize1 = (DropDownList)chkBox.Parent.FindControl("ddlSize1");
            DropDownList ddlSize2 = (DropDownList)chkBox.Parent.FindControl("ddlSize2");

            if (chkBox.Checked)
            {
                txbSize.Text = "";
                txbSize.Enabled = false;
                ddlSize1.SelectedValue = "";
                ddlSize1.Enabled = false;
                ddlSize2.SelectedValue = "";
                ddlSize2.Enabled = false;
            }
            else
            {
                if (txbSize.Visible) txbSize.Enabled = true;
                if (ddlSize1.Visible) ddlSize1.Enabled = true;
                if (ddlSize2.Visible) ddlSize2.Enabled = true;
            }

            bool bNoReqChecked = false;

            for (int i = 0; i < rptResult.Items.Count; i++)
            {
                CheckBox chkNoReq = (CheckBox)rptResult.Items[i].FindControl("chkNoReq");

                if (chkNoReq.Checked)
                {
                    bNoReqChecked = true;
                    break;
                }
            }

            if (bNoReqChecked)
            {
                hdfNoReqCheckedYn.Value = "Y";
            }
            else
            {
                hdfNoReqCheckedYn.Value = "N";
            }

            updPanel1.Update();
        }
    }
}