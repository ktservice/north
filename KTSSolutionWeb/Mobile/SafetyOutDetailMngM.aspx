﻿<%@ Page Language="C#" EnableViewState="true" AutoEventWireup="true" CodeBehind="SafetyOutDetailMngM.aspx.cs" Inherits="KTSSolutionWeb.SafetyOutDetailMngM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>kt service 북부 office</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=yes, viewport-fit=cover, initial-scale=1.0, maximum-scale=10, minimum-scale=1.0" />
    <meta name="description" content="kt service 북부 office" />
    <meta name="keywords" content="kt service 북부 office" />

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>

    <link href="../Resource/css/ktservice_M.css" rel="stylesheet" />
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">

        .td-left {
            text-align: left !important;
        }

        .filebox {
            width: auto !important;
        }

        .filebox .file input[type="file"] + label {
            background: #fff0;
        }

        .filebox .file {
            display: flex;
            float: left;
        }

        .custom-file-upload {
            display: inline-block !important;
            cursor: pointer !important;
        }

        .custom-file-upload img {
            vertical-align: middle;
        }
        .btn-save, .btn-del {
            width: auto;
            font-size: 14px;
            margin: 3px;
            padding: 5px;
        }

    </style>

        <script type="text/javascript">
            //var form = document.getElementById("form1");
            //form.setAttribute("enctype", "multipart/form-data");

            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var grid;
            var callGb;
            var regType;
            var dt;
            var selReqJson;
            var selChgReasonJson;

            var page = {
                init: function () {
                    fn_Init();
                    fn_Load();
                },
                eventbind: function () {

                    $("#imgFile1").click(function (e) {
                        fn_ConfirmDialog('파일을 삭제 하시겠습니까?', this, '', '');
                    });

                    $("#imgFile2").click(function (e) {
                        fn_ConfirmDialog('파일을 삭제 하시겠습니까?', this, '', '');
                    });


                    $("#aFile1").click(function (e) {
                        $("#hdnFileSeq").val($("#hdnFileSeq1").val());
                        fn_FileDown();
                        return false;
                    });

                    $("#aFile2").click(function (e) {
                        $("#hdnFileSeq").val($("#hdnFileSeq2").val());
                        fn_FileDown();
                        return false;
                    })

                    $("#aFile11").click(function (e) {
                        $("#hdnFileSeq").val($("#hdnFileSeq11").val());
                        fn_FileDown();
                        return false;
                    })

                    $("#aFile12").click(function (e) {
                        $("#hdnFileSeq").val($("#hdnFileSeq12").val());
                        fn_FileDown();
                        return false;
                    })

                    $("#aFile13").click(function (e) {
                        $("#hdnFileSeq").val($("#hdnFileSeq13").val());
                        fn_FileDown();
                        return false;
                    })

                }
            };

            var fn_Init = function () {
                ktsGridId = 'divPage';
                grid = ktsTabulator;
            }


            var fn_Load = function () {
                fn_Req();
            }

            var fn_AlertDialog = function (message, gubun) {
                $("#alertDialog").html("<p>" + message + "</p>");

                $("#alertDialog").dialog({
                    modal: true,
                    buttons: {
                        Ok: function () {
                            if (gubun == 'REG' || gubun == 'MOD' || gubun == 'DEL' || gubun == 'SAVE' || gubun == 'CANCEL' ) {
                                fn_Close('detail', 'Y');
                            }

                            $(this).dialog("close");
                        }
                    }
                });
            }

            var fn_ConfirmDialog = function (message, obj, tranType, saveYn) {
                $("#confirmDialog").html("<p>" + message + "</p>");

                $("#confirmDialog").dialog({
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            if (obj.id == 'imgFile1') {
                                $("#hdnFileSeq1").val('');
                                $('#aFile1').text('');
                                $('#txtfu1').val('');
                                $('#fu1').val('');
                                $('#fuC1').val('');
                                $("#pFileA1").hide();
                                $("#pFilebox1").css("display", "block");
                            }
                            else if (obj.id == 'imgFile2') {
                                $("#hdnFileSeq2").val('');
                                $('#aFile2').text('');
                                $('#txtfu2').val('');
                                $('#fu2').val('');
                                $('#fuC2').val('');
                                $("#pFilebox2").show();
                                $("#pFileA2").hide();
                            }
                            else {
                                if (tranType == 'REG') {
                                    callGb = 'RegTran';
                                    fn_Transaction(tranType, saveYn);
                                }
                                else if (tranType == 'MOD') {
                                    callGb = 'ModTran';

                                    fn_Transaction(tranType, saveYn);
                                }
                                else if (tranType == 'DEL') {
                                    callGb = 'DelTran';

                                    fn_Transaction(tranType, saveYn);
                                }
                                else if (tranType == 'SAVE') {
                                    callGb = 'DelTran';

                                    fn_Transaction(tranType, saveYn);
                                }
                                else if (tranType == 'CANCEL') {
                                    callGb = 'CancelTran';
                                    fn_Transaction(tranType, saveYn);
                                }
                            }


                            $(this).dialog("close");
                        },
                        "No": function () {
                            // 사용자가 "No" 버튼을 클릭했을 때 실행할 코드
                            $(this).dialog("close");
                        }
                    }
                });
            }
 
            var fn_RegTran = function (tranType, saveYn) {
                if (tranType == 'REG') {
                    if (fn_RegChk()) {
                        fn_ConfirmDialog('등록하시겠습니까?', this, tranType, saveYn);
                    }
                }
                else if (tranType == 'MOD') {
                    if (fn_RegChk()) {
                        fn_ConfirmDialog('변경하시겠습니까?', this, tranType, saveYn);
                    }
                }
                else if (tranType == 'DEL') {
                    fn_ConfirmDialog('삭제하시겠습니까?', this, tranType, saveYn);
                }
                else if (tranType == 'SAVE') {
                    if (fn_RegChk()) {
                        fn_ConfirmDialog('임시저장하시겠습니까?', this, tranType, saveYn);
                    }
                }
                else if (tranType == 'CANCEL') {
                    fn_ConfirmDialog('취소하시겠습니까?', this, tranType, saveYn);
                }
                
            }

            var fn_RegChk = function () {
                if ($('#aFile1').text() == '') {

                    fn_AlertDialog('품목파일을 첨부해야 합니다.', '');
                    return false;
                }

                return true;
            }

            var ajaxCallBack = function (json) {
                var obj1;
                var obj2;

                if (callGb == 'selReq') {
                    selReqJson = json;

                    fn_ChgReason();
                }
                else if (callGb == 'selChgReason') {
                    selChgReasonJson = json;
                    callGb = '';

                    if ($("#loaded").val() == '') {
                        $("#loaded").val('Y');
                        var params = parent.detailParams;

                        var saveYn = params[5];

                        if (saveYn != "Y") { //초기화
                            $("#lblItemNm").text('');
                            $("#txtOrgNm").val('');
                            $("#txtEmpNm").val('');
                            $("#hdnEmp").val('');
                            $("#txtEmpJobNm").val('');
                            $("#txtRegEmpNm").val('');
                            $("#txtRegDate").val('');
                            $("#txtGiveDate").val('');
                            $("#txtBuyDate").val('');
                            $("#txtItemNm").val('');
                            $("#hdnItemCd").val('');
                            $("#txtItemSeq").val('');
                            $("#txtMemo1").val('');
                            $("#hdnFileSeq1").val('');
                            $("#hdnFileSeq2").val('');
                            $('#aFile1').text('');
                            $('#aFile2').text('');
                        }

                        fn_Search("DETAIL", params[0], params[1], params[2], params[3], params[4], params[6]);
                    }
                    else {
                        $("#lblItemNm").text($("#hdnItemNm").val());
                        fn_SelBind($("#hdnReq").val(), $("#hdnChgReason").val());
                        fn_CtlBind();
                    }

                }
                else if (callGb == 'DETAIL') {
                    var regEmpNm;

                    $.each(JSON.parse(json), function (idx, item) {
                        $("#lblItemNm").text(item.ITEMNM);
                        $("#txtOrgNm").val(item.ORGNM);
                        $("#txtEmpNm").val(item.EMPNM);
                        $("#hdnEmp").val(item.EMPNO);
                        $("#txtEmpJobNm").val(item.JOBNM);
                        $("#txtRegEmpNm").val(item.REG_EMPNM);
                        $("#txtRegDate").val(item.REG_DATE);
                        $("#txtGiveDate").val(item.GIVE_DATE);
                        $("#txtBuyDate").val(item.BUY_DATE);
                        $("#txtItemNm").val(item.ITEMNM);
                        $("#hdnItemCd").val(item.ITEMCD);
                        $("#txtItemSeq").val(item.ITEMSEQ);
                        $("#txtMemo1").val(item.MEMO1);
                        $("#hdnFileSeq1").val(item.FILE_SEQ1); //교체 품목파일
                        $("#hdnFileSeq2").val(item.FILE_SEQ2); //교체 첨부파일
                        $('#aFile1').text(item.FILENM1);
                        $('#aFile2').text(item.FILENM2);
                        $("#hdnFileSeqNm1").val(item.FILENM1);
                        $("#hdnFileSeqNm2").val(item.FILENM2);

                        if (item.FILE_SEQ11 != null) $("#hdnFileSeq11").val(item.FILE_SEQ11); //인수증파일
                        if (item.FILE_SEQ12 != null) $("#hdnFileSeq12").val(item.FILE_SEQ12); //품목파일
                        if (item.FILE_SEQ13 != null) $("#hdnFileSeq13").val(item.FILE_SEQ13); //첨부파일
                        if (item.FILENM11 != null) $('#aFile11').text(item.FILENM11);//인수증파일
                        if (item.FILENM12 != null) $('#aFile12').text(item.FILENM12);//품목파일
                        if (item.FILENM13 != null) $('#aFile13').text(item.FILENM13);//첨부파일
                        if (item.MEMOA11 != null) $("#txtMemoA11").val(item.MEMOA11); //비고(교체신청)
                        if (item.MEMOA1 != null) $("#txtMemoA1").val(item.MEMOA1); //비고(신청)
                        if (item.MEMOA2 != null) $("#txtMemoA2").val(item.MEMOA2); //비고(인수)
                        if (item.OPER_STATUSNM != null) $("#txtOperStatus").val(item.OPER_STATUSNM);
                        if (item.REQ_STATUSNM != null) $("#txtReqStatus").val(item.REQ_STATUSNM);

                        regEmpNm = item.REG_EMPNM;

                        fn_SelBind(item.REQ_GB, item.CHG_REASONCD);

                    });

                    fn_CtlBind();

                    if (regEmpNm == null) $('#trRegEmp').hide();

                    callGb = '';
                }
                else if (callGb == 'REG') {
                    fn_AlertDialog('등록되었습니다.', callGb);
                }
                else if (callGb == 'MOD') {
                    fn_AlertDialog('변경되었습니다.', callGb);
                }
                else if (callGb == 'DEL') {
                    fn_AlertDialog('삭제되었습니다.', callGb);
                }
                else if (callGb == 'SAVE') {
                    fn_AlertDialog('저장되었습니다.', callGb);
                }
                else if (callGb == 'CANCEL') {
                    fn_AlertDialog('취소되었습니다.', callGb);
                }
                else if (callGb == 'FILEDOWN') {

                    $.each(JSON.parse(json), function (idx, item) {
                        var link = document.createElement('a');
                        link.href = 'data:application/octet-stream;base64,' + item.FILEDATA;
                        link.download = item.FILENM;
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    });

                    callGb = '';
                    
                }

            }

            var fn_CtlBind = function () {
                var params = parent.detailParams;

                regType = params[0];

                if (regType == 'A') {
                    $('#trRegEmp').hide();
                    $("#trStatus").hide();
                    $("#trFile11").hide();
                    $("#trFile12").hide();
                    $("#trFile13").hide();
                    $("#trMemoA11").hide();
                    $("#trMemoA1").hide();
                    $("#trMemoA2").hide();

                    $('#btnSave').show();
                    $('#btnReg').show();
                    $('#btnMod').hide();
                    $('#btnCancel').hide();

                    if ($('#hdnFileSeq1').val() == '') {
                        $("#pFilebox1").show();
                        $("#pFileA1").hide();
                        $("#imgFile1").hide();
                        $("#aFile1").text('');
                    }
                    else {
                        $("#pFilebox1").hide();
                        $("#pFileA1").show();
                        $("#aFile1").text($("#hdnFileSeqNm1").val());
                    }

                    if ($('#hdnFileSeq2').val() == '') {
                        $("#pFilebox2").show();
                        $("#pFileA2").hide();
                        $("#imgFile2").hide();
                        $("#aFile2").text('');
                    }
                    else {
                        $("#pFilebox2").hide();
                        $("#pFileA2").show();
                        $("#aFile2").text($("#hdnFileSeqNm2").val());
                    }
                }
                else if (regType == 'B') {
                    $('#trRegEmp').show();
                    $("#trStatus").show();
                    $("#trFile11").show();
                    $("#trFile12").show();
                    $("#trFile13").show();
                    $("#trMemoA11").show();
                    $("#trMemoA1").show();
                    $("#trMemoA2").show();

                    $('#btnSave').hide();
                    $('#btnReg').hide();
                    $('#btnMod').hide();
                    $('#btnCancel').hide();
                    $("#pFilebox1").hide();
                    $("#pFilebox2").hide();

                    $("#pFileA1").show();
                    $("#pFileA2").show();
                    $("#pFileA11").show();
                    $("#pFileA12").show();
                    $("#pFileA13").show();

                    $("#imgFile1").hide();
                    $("#imgFile2").hide();

                    

                    $('#selReq').attr('disabled', 'disabled');
                    $('#selChgReason').attr('disabled', 'readonly');
                    $('#txtMemo1').attr('readonly', 'readonly');

                }
                else if (regType == 'C') {
                    $('#trRegEmp').show();
                    $("#trStatus").hide();
                    $("#trFile11").hide();
                    $("#trFile12").hide();
                    $("#trFile13").hide();
                    $("#trMemoA11").hide();
                    $("#trMemoA1").hide();
                    $("#trMemoA2").hide();

                    $('#btnSave').hide();
                    $('#btnReg').hide();
                    $('#btnMod').hide();
                    $('#btnCancel').hide();
                    $("#pFilebox1").hide();
                    $("#pFilebox2").hide();

                    $("#pFileA1").show();
                    $("#pFileA2").show();

                    $("#imgFile1").hide();
                    $("#imgFile2").hide();

                    $('#selReq').attr('disabled', 'disabled');
                    $('#selChgReason').attr('disabled', 'readonly');
                    $('#txtMemo1').attr('readonly', 'readonly');

                }
                else if (regType == 'D') {
                    $('#trRegEmp').show();
                    $("#trStatus").hide();
                    $("#trFile11").hide();
                    $("#trFile12").hide();
                    $("#trFile13").hide();
                    $("#trMemoA11").hide();
                    $("#trMemoA1").hide();
                    $("#trMemoA2").hide();

                    $('#btnSave').show();
                    $('#btnReg').show();
                    $('#btnMod').hide();
                    $('#btnCancel').show();
                    
                    if ($('#hdnFileSeq1').val() == '') {
                        $("#pFilebox1").show();
                        $("#pFileA1").hide();
                        $("#imgFile1").hide();
                        $("#aFile1").text('');
                    }
                    else {
                        $("#pFilebox1").hide();
                        $("#pFileA1").show();
                        $("#aFile1").text($("#hdnFileSeqNm1").val());
                    }

                    if ($('#hdnFileSeq2').val() == '') {
                        $("#pFilebox2").show();
                        $("#pFileA2").hide();
                        $("#imgFile2").hide();
                        $("#aFile2").text('');
                    }
                    else {
                        $("#pFilebox2").hide();
                        $("#pFileA2").show();
                        $("#aFile2").text($("#hdnFileSeqNm2").val());
                    }
                }
                else if (regType == 'H') {
                    $('#trRegEmp').show();
                    $("#trStatus").hide();
                    $("#trFile11").hide();
                    $("#trFile12").hide();
                    $("#trFile13").hide();
                    $("#trMemoA11").hide();
                    $("#trMemoA1").hide();
                    $("#trMemoA2").hide();

                    $('#btnSave').hide();
                    $('#btnReg').hide();
                    $('#btnMod').hide();
                    $('#btnCancel').hide();

                    $("#pFilebox1").hide();
                    $("#pFilebox2").hide();

                    $("#pFileA1").show();
                    $("#pFileA2").show();

                    $("#imgFile1").hide();
                    $("#imgFile2").hide();

                    $('#selReq').attr('disabled', 'disabled');
                    $('#selChgReason').attr('disabled', 'readonly');
                    $('#txtMemo1').attr('readonly', 'readonly');
                }
            }

            var fn_SelBind = function (reqGb, chgReasonCd) {
                var obj1;
                var obj2;
                obj1 = $('#selReq');
                obj1.empty();

                $.each(JSON.parse(selReqJson), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                obj2 = $('#selChgReason');
                obj2.empty();

                $.each(JSON.parse(selChgReasonJson), function (idx, item) {
                    obj2.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });


                if (reqGb != undefined) {
                    obj1.val(reqGb).prop("selected", true);
                }
                else {
                    obj1.val($('#selReq option:first').val());
                }

                if (chgReasonCd != undefined) {
                    obj2.val(chgReasonCd).prop("selected", true);
                }
                else {
                    obj2.val($('#selChgReason option:first').val());
                }
            }

            var fn_Transaction = function (gubun, saveYn) {
                callGb = gubun;

                if (gubun == 'REG') {
                    gubun = 'I';
                }
                else if (gubun == 'MOD') {
                    gubun = 'I';
                }
                else if (gubun == 'DEL') {
                    gubun = 'D';
                }
                else if (gubun == 'CANCEL') {
                    gubun = 'D';
                }
                else if (gubun == 'SAVE') {
                    gubun = 'I';
                }
                

                var pGubun = gubun;
                var pSaveYn = saveYn;
                var pGiveDate = $("#txtGiveDate").val();
                var pItemCd = $("#hdnItemCd").val();
                var pItemSeq = $("#txtItemSeq").val();
                var pReqGb = $('#selReq').val();
                var pReqGbNm = $("#selReq option:selected").text();
                var pChgReasonCd = $('#selChgReason').val();
                var pChgReasonNm = $("#selChgReason option:selected").text();
                var pMemo1 = $("#txtMemo1").val();
                var pFileSeq1 = $("#hdnFileSeq1").val();
                var pFileSeq2 = $("#hdnFileSeq2").val();
                var pEmpno = $("#hdnEmp").val();

                //입력 파라미터
                var aData = [];
                aData[0] = pGubun;
                aData[1] = pSaveYn;
                aData[2] = pGiveDate;
                aData[3] = pItemCd;
                aData[4] = pItemSeq;
                aData[5] = pReqGb;
                aData[6] = pReqGbNm;
                aData[7] = pChgReasonCd;
                aData[8] = pChgReasonNm;
                aData[9] = pMemo1;
                aData[10] = pFileSeq1;
                aData[11] = pFileSeq2;
                aData[12] = pEmpno;


                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

            var fn_Req = function () {
                callGb = 'selReq';

                //입력 파라미터
                var aData = [];
                aData[0] = '1';
                aData[1] = '';
                aData[2] = 'S09';
                aData[3] = 'A01';
                aData[4] = 'Y';

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

            var fn_ChgReason = function () {
                callGb = 'selChgReason';

                //입력 파라미터
                var aData = [];
                aData[0] = '1';
                aData[1] = '';
                aData[2] = 'S08';
                aData[3] = 'A01';
                aData[4] = 'Y';

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

            var fn_Search = function (gubun, out, giveDate, itemCd, itemSeq, empNo, histSeq) {
                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = out;
                aData[2] = empNo;
                aData[3] = giveDate;
                aData[4] = itemCd;
                aData[5] = itemSeq;
                aData[6] = histSeq;

                callGb = gubun;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                if (gubun == 'DETAIL') {
                    ajaxCallEx(jsonData, ajaxUrl, true);
                }

            }

            var fn_FileAddStatus = function (gubun) {

                if (gubun == '1') {
                    if ($('#txtfu1').val() == '') {
                        fn_AlertDialog('품목파일을 선택하세요.', '');
                        return false;
                    }
                }
                if (gubun == '2') {
                    if ($('#txtfu2').val() == '') {
                        fn_AlertDialog('첨부파일을 선택하세요.', '');
                        return false;
                    }
                }
                

                $("#hdnChgReason").val($("#selChgReason").val());
                $("#hdnReq").val($("#selReq").val());
                $("#hdnItemNm").val($("#lblItemNm").val());

                return true;
            }
            

            var fn_Close = function (gubun, searchYn) {
                if (gubun == 'imgView') {
                    $(".page").show();
                    $("#divImgView").hide();
                }
                else if (gubun == 'detail') {
                    parent.fn_DetailViewClose(searchYn);
                }
            }

            var fn_fileChg = function (obj, gubun) {
                var file = obj.files[0];
                var fileName = file ? file.name : '';

                if (gubun == '1') {
                    if (obj.id == 'fuC1') $("#fu1").prop("files", $("#fuC1").prop("files"));
                    $("#txtfu1").val(fileName);
                }
                else if (gubun == '2') {
                    if (obj.id == 'fuC2') $("#fu2").prop("files", $("#fuC2").prop("files"));
                    $("#txtfu2").val(fileName);
                }
            }

            var fn_FileDown = function () {
                callGb = 'FILEDOWN';

                //입력 파라미터
                var aData = [];
                aData[0] = $("#hdnFileSeq").val();

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'FileDownload');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div id="alertDialog" class="alert" title="Alert"></div>
        <div id="confirmDialog" class="confirm" title="Confirm"></div>
        <div id="divPage" class="page">
            <h2><span id="lblItemNm" runat="server"></span>
                <input type="hidden" id="hdnItemNm" runat="server" /></h2>
            <table>
                <colgroup>
                    <col style="width: 20%" />
                    <col style="width: 30%" />
                    <col style="width: 20%" />
                    <col style="width: 30%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th style="text-align: left;">소속</th>
                            <td colspan="3" style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtOrgNm" readonly="readonly" runat="server" />
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;">이름</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtEmpNm" readonly="readonly" runat="server" />
                                    <input type="hidden" id="hdnEmp"  runat="server" />
                                </p>
                            </td>
                            <th style="text-align: left;">직무</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtEmpJobNm" readonly="readonly" runat="server" />
                                </p>
                            </td>
                        </tr>
                        <tr id="trRegEmp" runat="server">
                            <th style="text-align: left;">등록자명</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtRegEmpNm" readonly="readonly" runat="server" />
                                </p>
                            </td>
                            <th style="text-align: left;">등록일자</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtRegDate" readonly="readonly" runat="server" />
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;">지급일자</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtGiveDate" readonly="readonly" runat="server" />
                                </p>
                            </td>
                            <th style="text-align: left;">구매일자</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtBuyDate" readonly="readonly" runat="server" />
                                </p>
                            </td>
                        </tr>
                        <tr id="trStatus" style="display:none;">
                            <th style="text-align: left;">운용상태</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtOperStatus" readonly="readonly" runat="server" />
                                </p>
                            </td>
                            <th style="text-align: left;">처리상태</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtReqStatus" readonly="readonly" runat="server" />
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;">품목</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtItemNm" readonly="readonly" runat="server" />
                                    <input type="hidden" id="hdnItemCd"  runat="server" />
                                </p>
                            </td>
                            <th style="text-align: left;">품목순번</th>
                            <td style="padding-right: 2px;">
                                <p class="inpbox">
                                    <input type="text" id="txtItemSeq" readonly="readonly" runat="server" />
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;">교체사유</th>
                            <td style="padding-right: 2px;">
                                <p class="optionbox">
                                    <select id="selChgReason" runat="server"></select>
                                    <input type="hidden" id="hdnChgReason" runat="server" />
                                </p>
                            </td>
                            <th style="text-align: left;">요청구분</th>
                            <td style="padding-right: 2px;">
                                <p class="optionbox">
                                    <select id="selReq" runat="server"></select>
                                    <input type="hidden" id="hdnReq" runat="server" />
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;">비고(교환)</th>
                            <td colspan="3" style="padding-right: 2px;">
                                <p class="inpbox">
                                    <textarea id="txtMemo1" style="min-height: 100px; height: 100px;" runat="server"></textarea>
                                </p>
                            </td>
                        </tr>
                        <tr id="trMemoA11" style="display:none">
                            <th style="text-align: left;">비고(교체신청)</th>
                            <td colspan="3" style="padding-right: 2px;">
                                <p class="inpbox">
                                    <textarea id="txtMemoA11" style="min-height: 100px; height: 100px;" readonly="readonly" runat="server"></textarea>
                                </p>
                            </td>
                        </tr>
                        <tr id="trMemoA1" style="display:none">
                            <th style="text-align: left;">비고(신청)</th>
                            <td colspan="3" style="padding-right: 2px;">
                                <p class="inpbox">
                                    <textarea id="txtMemoA1" style="min-height: 100px; height: 100px;" readonly="readonly" runat="server"></textarea>
                                </p>
                            </td>
                        </tr>
                        <tr id="trMemoA2" style="display:none">
                            <th style="text-align: left;">비고(인수)</th>
                            <td colspan="3" style="padding-right: 2px;">
                                <p class="inpbox">
                                    <textarea id="txtMemoA2" style="min-height: 100px; height: 100px;" readonly="readonly" runat="server"></textarea>
                                </p>
                            </td>
                        </tr>
                        <tr id="trFile1">
                            <th style="text-align: left;">품목파일(교환)</th>
                            <td colspan="3" class="td-left" style="padding-right: 2px;">
                                <div id="divFile1">
                                    <span id="pFilebox1" class="filebox" runat="server">
                                        <input type="text" id="txtfu1" runat="server" />
                                        <span class="file">
                                            <label for="fu1" class="custom-file-upload">
                                                <img src="/Resource/Mobile_images/icon_folder.png" alt="Upload File" />
                                            </label>
                                            <input type="file" accept="image/*" id="fu1" name="fu1" onchange="fn_fileChg(this, 1)" style="display: block;" />
                                            <label for="fuC1" class="custom-file-upload">
                                                <img src="/Resource/Mobile_images/icon_camera.png" alt="Upload Image" />
                                            </label>
                                            <input id="fuC1" type="file" accept="image/*" capture="camera" onchange="fn_fileChg(this, 1)" style="display: none;" />
                                            <asp:Button ID="btnFileAdd1" runat="server" Style="min-width: auto; padding: 1px" class="btn-black" OnClientClick="return fn_FileAddStatus('1');" OnClick="btnFileAdd1_Click" Text="첨부" />
                                        </span>
                                    </span>
                                    <span id="pFileA1" class="filebox" runat="server" style="display:none;align-items: center;"><a id="aFile1" href="#" runat="server"></a>
                                        <img id="imgFile1" src="/Resource/images/icon_minus_02.png" alt="삭제" runat="server" />
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr id="trFile2">
                            <th style="text-align: left;">첨부파일(교환)</th>
                            <td colspan="3" class="td-left" style="padding-right: 2px;">
                                <div id="divFile2">
                                    <span id="pFilebox2" class="filebox" runat="server">
                                        <input type="text" id="txtfu2" runat="server" />
                                        <span class="file">
                                            <label for="fu2" class="custom-file-upload">
                                                <img src="/Resource/Mobile_images/icon_folder.png" alt="Upload File" />
                                            </label>
                                            <input type="file" accept="image/*" id="fu2" name="fu2" onchange="fn_fileChg(this, 2)" style="display: none;" />
                                            <label for="fuC2" class="custom-file-upload">
                                                <img src="/Resource/Mobile_images/icon_camera.png" alt="Upload Image" />
                                            </label>
                                            <input id="fuC2" type="file" accept="image/*" capture="camera" onchange="fn_fileChg(this, 2)" style="display: none;" />
                                            <asp:Button ID="btnFileAdd2" runat="server" Style="min-width: auto; padding: 1px" class="btn-black" OnClientClick="return fn_FileAddStatus('2');" OnClick="btnFileAdd2_Click" Text="첨부" />
                                        </span>
                                    </span>
                                    <span id="pFileA2" class="filebox" runat="server" style="display: none;align-items: center;">
                                        <a id="aFile2" href="#" runat="server"></a>
                                        <img id="imgFile2" src="/Resource/images/icon_minus_02.png" alt="삭제" runat="server" />
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr id="trFile11" style="display:none">
                            <th style="text-align: left;">인수증파일</th>
                            <td colspan="3" class="td-left" style="padding-right: 2px;">
                                <div id="divFile11">
                                    <span id="pFileA11" class="filebox" runat="server" style="display: none">
                                        <a id="aFile11" href="#" runat="server"></a>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr id="trFile12" style="display:none">
                            <th style="text-align: left;">품목파일</th>
                            <td colspan="3" class="td-left" style="padding-right: 2px;">
                                <div id="divFile12">
                                    <span id="pFileA12" class="filebox" runat="server" style="display: none">
                                        <a id="aFile12" href="#" runat="server"></a>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr id="trFile13" style="display:none">
                            <th style="text-align: left;">첨부파일</th>
                            <td colspan="3" class="td-left" style="padding-right: 2px;">
                                <div id="divFile13">
                                    <span id="pFileA13" class="filebox" runat="server" style="display: none">
                                        <a id="aFile13" href="#" runat="server"></a>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <h2></h2>
                                <span style="display: flex; justify-content: center;">
                                    <input id="btnSave" type="button" class="btn-save" onclick="fn_RegTran('SAVE', 'Y')" value="임시저장" style="display:none" />
                                    <input id="btnReg" type="button" class="btn-save" onclick="fn_RegTran('REG', 'N')" value="등록" style="display:none" />
                                    <input id="btnMod" type="button" class="btn-save" onclick="fn_RegTran('MOD', 'N')" value="변경" style="display:none" />
                                    <input id="btnCancel" type="button" class="btn-del" onclick="fn_RegTran('CANCEL', 'N')" value="저장취소" style="display:none" />
                                    <input type="hidden" id="loaded" runat="server" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <span style="display: flex; justify-content: center;">
                                    <button id="btnDetailClose" type="button" onclick="fn_Close('detail', '')" class="btn-black">닫기</button>
                                    <input type="hidden" id="hdnFileSeq1" runat="server" />
                                    <input type="hidden" id="hdnFileSeq2" runat="server" />
                                    <input type="hidden" id="hdnFileSeq11" runat="server" />
                                    <input type="hidden" id="hdnFileSeq12" runat="server" />
                                    <input type="hidden" id="hdnFileSeq13" runat="server" />
                                    <input type="hidden" id="hdnFileSeq" runat="server" />
                                    <input type="hidden" id="hdnFileSeqNm1" runat="server" />
                                    <input type="hidden" id="hdnFileSeqNm2" runat="server" />
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="divImgView" style="display: none">
                <button id="btnImgClose" type="button" onclick="fn_Close('imgView', '')" class="btn-black">닫기</button>
                <span class="spanImgView">
                    <img id="imgImgView" src="#" style="max-width: 100%" runat="server" />
                </span>
            </div>
    </form>
</body>
</html>
