﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_HTC.Master" AutoEventWireup="true" CodeBehind="HTC.aspx.cs" Inherits="KTSSolutionWeb.HTC" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">      
        .btn_today_close {
            width: 100%;
            height: 45px;
            margin-top:10px;
            background-color: #333;
            text-align: center;
            vertical-align:middle;
            color: #fff;
            font-size: 14px;
            display: block;
            border:0 none;
        }
    </style>
    <script type="text/javascript">
        function ChkSendMsg() {
            if (confirm("선택한 업체정보로 메세지를 전송 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
		}

        var ModalOpen = function () {
            $("#divModal").show();
		}

        var ModalClose = function () {
            $("#divModal").hide();
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">

		<div class="page">
			<h2>고객편의서비스</h2>

			<asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
				<ContentTemplate>					
					<!-- S:fieldset -->
					<fieldset>
						<legend>HTC</legend>
						<span class="inpbox" style="width:49%">
							<label>사번</label>
							<asp:TextBox ID="txbEmpNo" runat="server" ReadOnly="true"></asp:TextBox>
						</span>
						<span class="inpbox" style="width:49%">
							<label>이름</label>
							<asp:TextBox ID="txbEmpNm" runat="server" ReadOnly="true"></asp:TextBox>
						</span>
					</fieldset>
					<fieldset>
						<span class="inpbox" style="width:100%">
							<label style="width:15%">소속</label>
							<asp:TextBox ID="txbOrgNm" runat="server" Width="84.5%" ReadOnly="true"></asp:TextBox>
						</span>
					</fieldset>
					<!-- E:fieldset -->

					<h2></h2>

					<!-- S:fieldset -->
					<fieldset>
						<span class="optionbox" style="width:100%">
							<label style="width:15%">업체</label>
							<asp:DropDownList ID="ddlCompanyUrl" runat="server" Width="83%"></asp:DropDownList>
						</span>
					</fieldset>
					<!-- E:feileset -->
					<!-- S:fieldset -->
					<fieldset>
						<span class="inpbox" style="width:100%">
							<label style="width:15%">내용</label>
							<asp:TextBox ID="txbContent" runat="server" Width="83%"></asp:TextBox>
						</span>
					</fieldset>
					<!-- E:feileset -->
					<div>
						<span class="inpbox" style="width:100%">
							<label style="width:15%;margin-left:-8px;">전화번호</label>
							<asp:TextBox ID="txbPhone1" MaxLength="3" runat="server" Width="23%"></asp:TextBox>
							<label style="margin-left:8px;">-</label>
							<asp:TextBox ID="txbPhone2" MaxLength="4" runat="server" Width="23%"></asp:TextBox>
							<label style="margin-left:8px;">-</label>
							<asp:TextBox ID="txbPhone3" MaxLength="4" runat="server" Width="23%"></asp:TextBox>
						</span>
					</div>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnSendMsg" EventName="click" />
				</Triggers>
			</asp:UpdatePanel>

			<div style="text-align:center;margin-top:40px;">
				<asp:Button ID="btnSendMsg" runat="server"  class="btn-green" style="width:150px" OnClientClick="return ChkSendMsg();" OnClick="btnSendMsg_Click" BorderStyle="None" Text="전송" />
			</div>

            <!-- S: divModal -->
			<div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:340px;top:15%;background:rgb(255, 255, 255);border-radius:10px;border:1px solid gray;">
                <!-- S:popcontents -->
                <div class="popcontents" style="text-align:center;vertical-align:middle;font-size:14px;padding-top:10px;">
                   금일은 귀하의 휴일 또는 휴가일로<br/>
                    근로제공의무가 없는 날이 오니<br/>
                    업무처리를 삼가하시고 귀가하시기 바랍니다.<br/><br/>

                    만약 부서장의 근무지시가 있었다면<br/>
                    지금 즉시 복무시스템에 복무를<br/>
                    처리하여 주시기 바랍니다.<br/><br/>

                    휴일근무한 경우 휴일근무를 신청하고<br/>
                    휴가일인 경우 휴가 취소를 신청하시어<br/>
                    부서장 승인을 받으시기 바랍니다.<br/>                
                    <!-- S:btncenter -->
                    <button type="button" id="btnModalClose"  onclick="ModalClose();" title="닫기" class="btn-gray" style="margin-top:10px;">닫기</button>
                    <!-- //E:btncenter -->
                </div>
                <asp:UpdatePanel ID="UdpToday" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:button ID="btnToday" runat="server" class="btn_today_close" OnClick="btnToday_Click" Text="오늘 하루 자동 팝업을 띄우지 않습니다." />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnToday" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
                <%--<button type="button" id="btnToday" class="btn_today_close"><span>오늘 하루 자동 팝업을 띄우지 않습니다.</span></button>--%>
			</div>
			<!--E: divModal -->
		</div>		
	</div>
	<!--//E: contentsarea -->
</asp:Content>