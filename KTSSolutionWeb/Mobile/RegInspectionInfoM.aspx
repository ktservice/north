﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Order.Master" AutoEventWireup="true" CodeBehind="RegInspectionInfoM.aspx.cs" Inherits="KTSSolutionWeb.RegInspectionInfoM" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>        
        .scrollbox2 {overflow-x:hidden; overflow-y:auto; width:100%; height:auto; min-height:30px; margin-top:2px; margin-bottom:2px}
        .inpbox span {display:inline-block; width:100%; vertical-align:middle; color:#000; font-size:13px;}
        .inpbox input {display:inline-block; width:100%; height:30px; padding:0 14px; border:1px solid #dfdfdf; border-radius:2px; line-height:30px; vertical-align:middle; color:#000; font-size:13px;}
        .hTitle {height:35px; padding-top:13px}
        .tabulator {position:relative;background-color:#fff;overflow:hidden;font-size:11px;text-align:left;-ms-transform:translateZ(0);transform:translateZ(0);}

        .div-chktitle {width:100%;height:25px;font-weight:bold;font-size:15px;text-align:left;}
        .div-list {overflow:auto;}
        .div-chkgroup {width: 100%;border: 1px solid black;border-radius: 10px;padding: 5px 5px;margin-bottom:5px}
        .div-chkupper {font-weight: bold;font-size: 14px;line-height: 20px;text-align: left;margin-bottom:5px}
        .div-chklist {vertical-align: middle;font-weight: normal;font-size: 14px;line-height: 14px;text-align: left;padding: 2px 10px;}
        .div-radio-box {width: 100%;height: 24px;padding: 0 10px;margin-bottom:5px;text-align: right;display: flex;justify-content: flex-end;}
        .div-radio {cursor: pointer;padding-top: 1px;padding-left: 4px;padding-right: 4px;margin-right: 4px;text-align: left;border-radius: 3px;border: 1px solid gray;float: left;background-color:white}

        .pageView {table-layout: fixed; border-collapse: collapse; border-spacing: 0; width: 100%; background: #ffffff;}
        .pageViewTh {font-size: 13px; text-align: left; border-bottom: 1px solid #dfdfdf; padding: 6px 10px; background: #f9f9f9;}
        .pageViewTd {font-size: 13px; text-align: left; border-bottom: 1px solid #dfdfdf; padding: 6px 10px;}
        /*p.inpbox {display: flex;}*/
        p.inpbox input {flex: 1; padding:0 10px;}
        .inpbox label {margin-right: 0;}
        .tableInTitle {display:block !important; justify-content:center; text-align: center; background-color: #eaf7f6; padding: 6px 0; margin-bottom: 10px;}
        #container h2#htitle {margin-bottom: 0;}
        .inpbox textarea {padding: 10px 14px;}
        .inpbox textarea:read-only {background: #f9f9f9; border: 1px solid #dfdfdf;}
        .inpbox input:read-only {background: #f9f9f9; border: 1px solid #dfdfdf;}
        .btnWrap {display: flex; justify-content: center; width: 100%; margin-top: 30px;}
        .btnWrap input {min-width: 55px; padding: 0 10px; margin-right: 5px; border: none;}
        .btnWrap input:last-child {margin-right: 0;}
        .filebox .file {display: flex; float: left; width: 30px !important;}

    </style>
    <script type="text/javascript">
        var ktsGrid1;
        var ktsGrid2;
        var ktsGrid3;
        var grid1;
        var grid2;
        var grid3;
        var callGb;
        var reloadGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
                fn_GridInit1();
                fn_GridInit2();
                fn_GridInit3();
            },
            eventbind: function () {
                $("#<%= ddlCompany.ClientID %>").change(function (e) {
                    $("#<%= txbPartnersEmpNm.ClientID %>").val("");
                    $("#<%= hdfPartnersEmpNo.ClientID %>").val("");

                    var CompCd = $("#<%= ddlCompany.ClientID %> option:selected").val();
                    var delegate = "";

                    if (CompCd != "") {
                        delegate = CompCd.split('_')[2];
                    }

                    if (delegate == "Y") {
                        $("#<%= txbPartnersEmpNm.ClientID %>").val("협력사 대표자");
                    }
                });

                 $("#<%= ddlLv1Grp.ClientID %>").change(function (e) {
                     var lv1GrpCd = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();

                     if (lv1GrpCd == "") {
                         $("#ddlLv2Grp").attr("disabled", "disabled");
                         $("#ddlLv3Grp").attr("disabled", "disabled");
                         fn_ControlBind_Lv2Grp(null);
                         fn_ControlBind_Lv3Grp(null);
                     }
                     else {
                         $("#ddlLv2Grp").removeAttr("disabled");
                         fn_ControlBind_Lv3Grp(null);

                         fn_GetLv2Grp(lv1GrpCd);
                     }
                 });

                $("#ddlLv2Grp").change(function (e) {
                    var lv2GrpCd = $("#ddlLv2Grp").val();
                    $("#ddlLv3Grp").removeAttr("disabled");

                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = "";
                    options.text = "전체";
                    options.selected = true;

                    if (lv2GrpCd == "") {
                        $("#ddlLv3Grp").attr("disabled", "disabled");
                        fn_ControlBind_Lv3Grp(null);;
                    }
                    else {
                        fn_GetLv3Grp(lv2GrpCd);
                    }
                });

                $("#btnOrgSearch").click(function (e) {
                    var OrgNm = $("#<%= txbOrgNm.ClientID %>").val();

                    if (OrgNm.length == 0) {
                        alert("조직명을 입력해주세요.");
                        return false;
                    } else {
                        //입력 파라미터
                        var aData = [];
                        aData[0] = OrgNm;

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = grid1.getUrl(location.href, 'GetOrgData');

                        //grid Bind
                        grid1.getDataBind(jsonData, ajaxUrl, 'ktsGrid1');
                    }
                });

                $("#btnKtsUserSearch").click(function (e) {
                    var EmpNm = $("#<%= txbSearchKtsUser.ClientID %>").val();
                    var OrgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();

                    if (EmpNm.length == 0) {
                        alert("사원명을 입력해주세요.");
                        return false;
                    } else {
                        //입력 파라미터
                        var aData = [];
                        aData[0] = EmpNm;
                        aData[1] = OrgCd;

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = grid2.getUrl(location.href, 'GetKtsUser');

                        //grid Bind
                        grid2.getDataBind(jsonData, ajaxUrl, 'ktsGrid2');
                    }
                });

                $("#btnPUserSearch").click(function (e) {
                    var EmpNm = $("#<%= txbSearchPUser.ClientID %>").val();
                    var CompCd = $("#<%= ddlCompany.ClientID %> option:selected").val();

                    //입력 파라미터
                    var aData = [];
                    aData[0] = EmpNm;
                    aData[1] = CompCd;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = grid3.getUrl(location.href, 'GetPartnersUser');

                    //grid Bind
                    grid3.getDataBind(jsonData, ajaxUrl, 'ktsGrid3');
                });
            }
        };

        var fn_Load = function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $("input.date").datepicker({
                    dateFormat: "yy-mm-dd",
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    showOtherMonths: false,
                    selectOtherMonths: false,
                    constrainInput: false,
                    prevText: "<",
                    nextText: ">",
                });
            }

            var chkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var actStatus = $("#<%= hdfActStatus.ClientID %>").val();

            if (chkStatus != "") {
                if (chkStatus == 'F' && actStatus == 'U') {
                    $("#hReq").hide();
                    $("#tableReq").hide();
                }
                else {
                    $("#hReq").show();
                    $("#tableReq").show();
                }

                if (actStatus == 'F') {
                    $("#hAct").show();
                    $("#tableAct").show();
                }
                else {
                    $("#hAct").hide();
                    $("#tableAct").hide();
                }

                if (chkStatus == 'F') {
                    $("#stitle").text('품질검사 조치요청서 [조치완료]');
                }
                else if (chkStatus == 'T') {
                    $("#stitle").text('품질검사 조치요청서 [등록중]');
                }
                else if (chkStatus == 'R' || chkStatus == 'B') {
                    $("#stitle").text('품질검사 조치요청서 [조치요청중]');
                }
                else {
                    $("#stitle").text('품질검사 조치요청서');
                }
            }
        };

        var fn_GridInit1 = function () {

            var fn_Choicebutton = function (cell) {
                var rowData = cell.getRow().getData();

                var orgcd = rowData.ORGCD;
                var orglv2nm = rowData.ORGLV2NM;
                var orglv3nm = rowData.ORGLV3NM;
                var orglv4nm = rowData.ORGLV4NM;
                var orgnm = rowData.ORGNM;

                var input = "<input id='btnCell' type='button' onclick='fn_SetOrg(\"" + orgcd + "\", \"" + orglv2nm + "\", \"" + orglv3nm + "\", \"" + orglv4nm + "\", \"" + orgnm + "\");' value='선택' class='btn-gray' style='min-width:40px;height:23px;padding:0 5px;line-height:0px;font-size:15px;border-style:none;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80, resizable: false, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 55, formatter: fn_Choicebutton, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조직명", field: "ORGNM", sorter: "string", minWidth: 30, width:90, maxWidth: 400, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점명", field: "ORGLV4NM", sorter: "string", minWidth: 30, width: 90, maxWidth: 150, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사명", field: "ORGLV3NM", sorter: "string", minWidth: 30, width: 90, maxWidth: 150, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부명", field: "ORGLV2NM", sorter: "string", minWidth: 30, width: 80, maxWidth: 100, resizable: false },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid1 = ktsTabulator;
            grid1.gridDefault('ktsGrid1', 'fitColumns', true, null); // div의 grid id 값
            ktsGrid1 = ktsGrid;

            ktsGrid1.setHeight("200px");
        };

        var fn_GridInit2 = function () {

            var fn_Choicebutton = function (cell) {
                var rowData = cell.getRow().getData();

                var empno = rowData.EMPNO;
                var empnm = rowData.EMPNM;

                var input = "<input id='btnCell' type='button' onclick='fn_SetKtsUser(\"" + empno + "\", \"" + empnm + "\");' value='선택' class='btn-gray' style='min-width:40px;height:23px;padding:0 5px;line-height:0px;font-size:15px;border-style:none;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80, resizable: false, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 55, formatter: fn_Choicebutton, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", minWidth: 30, maxWidth: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", minWidth: 30, maxWidth: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", minWidth: 30, maxWidth: 400, resizable: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid2 = ktsTabulator;
            grid2.gridDefault('ktsGrid2', 'fitColumns', true, null); // div의 grid id 값
            ktsGrid2 = ktsGrid;

            ktsGrid2.setHeight("200px");
        };

        var fn_GridInit3 = function () {

            var fn_Choicebutton = function (cell) {
                var rowData = cell.getRow().getData();

                var empno = rowData.EMPNO;
                var empnm = rowData.EMPNM;

                var input = "<input id='btnCell' type='button' onclick='fn_SetPartnersUser(\"" + empno + "\", \"" + empnm + "\");' value='선택' class='btn-gray' style='min-width:40px;height:23px;padding:0 5px;line-height:0px;font-size:15px;border-style:none;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80, resizable: false, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 55, formatter: fn_Choicebutton, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", minWidth: 60, maxWidth: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "업체명", field: "COMPANYNM", sorter: "string", minWidth: 70, maxWidth: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", minWidth: 30, maxWidth: 400, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", minWidth: 30, maxWidth: 400, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처", field: "MOBILE", sorter: "string", minWidth: 88, maxWidth: 400, resizable: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid3 = ktsTabulator;
            grid3.gridDefault('ktsGrid3', 'fitData', false, null); // div의 grid id 값
            ktsGrid3 = ktsGrid;

            ktsGrid3.setHeight("200px");
        }

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                if (ktsGridId == 'ktsGrid1') {
                    ktsGrid1.setData(json);
                }
                else if (ktsGridId == 'ktsGrid2') {
                    ktsGrid2.setData(json);
                }
                else if (ktsGridId == 'ktsGrid3') {
                    ktsGrid3.setData(json);
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'InsOk') {
                            alert('신규 조치요청서 등록이 완료되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'UpdOk') {
                            alert('조치요청서 수정이 완료되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'DelOk') {
                            alert('조치요청서가 삭제 되었습니다.');

                            fn_PageMoveMain();

                            callGb = '';
                        }
                        else if (callGb == 'FileInsOk') {
                            alert('요청사항 이미지가 등록 되었습니다.');
                            $("#filebox").empty();
                            $("#divReqImg").empty();
                            callGb = '';

                            fn_GegImgData("R");
                        }
                        else if (callGb == 'FileDelOk') {
                            alert('요청사항 이미지가 삭제 되었습니다.');
                            $("#filebox").empty();
                            $("#divReqImg").empty();
                            callGb = '';

                            fn_GegImgData("R");
                        }
                        else if (callGb == 'SetStatusOK_R') {
                            alert('점검결과 추가 조치사항을 요청하였습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'SetStatusOK_B') {
                            alert('조치사항을 반려하고 재요청 하였습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'SetStatusOK_F') {
                            alert('조치완료 되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }

                        if (reloadGb == "Y") {
                            var inspection_No = "";

                            if (item.INSPECTION_NO != undefined && item.INSPECTION_NO != '') {
                                inspection_No = item.INSPECTION_NO;

                            }
                            else {
                                inspection_No = $("#<%= hdfInspection_No.ClientID %>").val();
                            }

                            if (inspection_No != '' && inspection_No != undefined) {
                                fn_Reload(inspection_No);
                            }

                            reloadGb = '';
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetLv2Grp') {
                    callGb = '';

                    fn_ControlBind_Lv2Grp(json);
                }
                else if (callGb == 'GetLv3Grp') {
                    callGb = '';

                    fn_ControlBind_Lv3Grp(json);
                }
                else if (callGb == 'GetChkList') {
                    callGb = '';

                    fn_ControlBind_ChkList(json);
                }
                else if (callGb == 'GetImgData') {
                    callGb = '';
                    var status = $("#<%= hdfChkStatus.ClientID %>").val();
                    var bEnable = false;

                    if (status == 'T') {
                        bEnable = true;
                    }

                    fn_ControlBind_Img(json, bEnable, 'R');
                }
            }
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://bss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var fn_ArrayToBase64 = function (buffer) {

            var binary = '';

            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;

            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }

            return window.btoa(binary);
        };

        var fn_GetLv2Grp = function (lv1GrpCd) {
            if (lv1GrpCd != "") {
                var aData = [];

                aData[0] = lv1GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv2GrpList');

                callGb = 'GetLv2Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetLv3Grp = function (lv2GrpCd) {

            if (lv2GrpCd != "") {

                var aData = [];

                aData[0] = lv2GrpCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLv3GrpList');

                callGb = 'GetLv3Grp';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetChkList = function () {
            var inspection_No = $("#<%= hdfInspection_No.ClientID %>").val();
            var chkDt = $("#<%= txbChkDt.ClientID %>").val();
            var chkOrgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();
            var chkEmpNo = $("#<%= hdfChkEmpNo.ClientID %>").val();
            var compCd = $("#<%= ddlCompany.ClientID %> option:selected").val().replace("_Y", "").replace("_N", "");
            var actEmpNo = $("#<%= hdfPartnersEmpNo.ClientID %>").val();

            var DelegateChk = $("#<%= ddlCompany.ClientID %>").val().split('_')[2];

            var lv1GrpCd = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();
            var lv2GrpCd = $("#ddlLv2Grp").val();
            var lv3GrpCd = $("#ddlLv3Grp").val();
            var preLv1GrpCd = $("#<%= hdfLv1Grp.ClientID %>").val();

            if (chkDt == "" || chkDt == undefined || chkOrgCd == "" || chkOrgCd == undefined || chkEmpNo == "" || chkEmpNo == undefined) {
                alert("검사원 정보를 입력해주세요.");
            }
            else if (compCd == "" || compCd == undefined || compCd == null) {
                message = "업체 정보를 선택해주세요.";
            }
            else if (DelegateChk == "N" && actEmpNo == "") {
                alert("등록된 협력사 대표자가 없습니다. 현장대리인 정보를 입력해주세요.");
            }
            else if (lv1GrpCd == "" || lv1GrpCd == undefined || lv2GrpCd == "" || lv2GrpCd == undefined || lv3GrpCd == "" || lv3GrpCd == undefined) {
                alert("조치항목 분류를 선택해주세요.");
            }
            else {
                var sUrl = '';
                //입력 파라미터
                var aData = [];
                
                if (preLv1GrpCd == lv1GrpCd) {
                    aData[0] = inspection_No;
                    sUrl = 'GetChkResultData';

                }
                else {
                    aData[0] = lv1GrpCd;
                    sUrl = 'GetChkListData';
                }

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                callGb = 'GetChkList';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_radioBoxChk = function (val) {
            $("#" + val).prop('checked', true);
            $("#" + val).change();
        };

        var fn_DelInfoChk = function (inspection_no) {
            if (confirm("조회된 조치요청서를 삭제 하시겠습니까?")) {
                fn_DelInspectionInfo(inspection_no);
            }
        };

        var fn_SetControls = function (bRegInfo, bRegImg) {
            var chkList = $("#<%= hdfResult.ClientID %>").val();
            var lv2 = $("#<%= hdfLv2.ClientID %>").val();
            var lv3 = $("#<%= hdfLv3.ClientID %>").val();
            var imgReqData = $("#<%= hdfReqImg.ClientID %>").val();
            var imgActData = $("#<%= hdfActImg.ClientID %>").val();

            fn_ControlBind_ResultList(chkList);
            fn_ControlBind_Lv2Grp(lv2);
            fn_ControlBind_Lv3Grp(lv3);

            if (imgReqData != '') {
                fn_ControlBind_Img(imgReqData, bRegImg, 'R');
            }

            if (imgActData != '') {
                fn_ControlBind_Img(imgActData, false, 'A');
            }

            if (!bRegInfo) {
                $("#ddlLv2Grp").attr("disabled", "disabled");
                $("#ddlLv3Grp").attr("disabled", "disabled");
            }

            $("#lblList").show();
            $("#tableList").show();
        };

        var fn_ControlBind_Lv2Grp = function (json) {
            var ChkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var Lv2GrpCd = $("#<%= hdfLv2Grp.ClientID %>").val();

            var selector = $("#ddlLv2Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV2GRPCD;
                options.text = item.LV2GRPNM;

                if (item.LV2GRPNM == "") {
                    options.style.display = "none";
                }

                if (item.LV2GRPCD == Lv2GrpCd) {
                    options.selected = true;
                }

                selector.append(options);
            });
        };

        var fn_ControlBind_Lv3Grp = function (json) {
            var ChkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var Lv3GrpCd = $("#<%= hdfLv3Grp.ClientID %>").val();

            var selector = $("#ddlLv3Grp");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";
            options.selected = true;

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.LV3GRPCD;
                options.text = item.LV3GRPNM;

                if (item.LV3GRPNM == "") {
                    options.style.display = "none";
                }

                if (item.LV3GRPCD == Lv3GrpCd) {
                    options.selected = true;
                }

                selector.append(options);
            });
        };

        var fn_ControlBind_ResultList = function (json) {
            //감리항목 바인드
            var tList = $("#tList");

            tList.empty();

            var i = 0;
            var preUpperCd = "";

            $.each(JSON.parse(json), function (idx, item) {
                i++;

                var tr = document.createElement("tr");
                var topLine = false;

                if (preUpperCd != item.UPPERLISTCD) {

                    var tdTitle = document.createElement("td");

                    tdTitle.className = "pageViewTd";
                    tdTitle.style.border = "none";
                    tdTitle.style.borderRight = "1px solid #dfdfdf";
                    tdTitle.style.borderBottom = "1px solid #dfdfdf";
                    tdTitle.style.whiteSpace = "break-spaces";
                    tdTitle.rowSpan = item.ROWCNT;

                    tdTitle.innerText = item.UPPERLISTNM;

                    tr.append(tdTitle);
                    tList.append(tr);

                    topLine = true;
                }

                preUpperCd = item.UPPERLISTCD;


                var td1 = document.createElement("td");

                td1.className = "pageViewTd";
                td1.style.textAlign = "left";
                td1.style.border = "none";
                td1.style.borderRight = "1px solid #dfdfdf";
                td1.style.whiteSpace = "break-spaces";

                if (topLine) {
                    td1.style.borderTop = "1px solid #dfdfdf";
                }

                td1.innerText = "- " + item.CHKLIST;

                tr.append(td1);

                var td2 = document.createElement("td");

                td2.className = "pageViewTd";
                td2.style.border = "none";
                td2.style.textAlign = "left";

                if (topLine) {
                    td2.style.borderTop = "1px solid #dfdfdf";
                }

                var resultText = "";

                if (item.RESULTCD == "S") {
                    resultText = "정상";
                }
                else if (item.RESULTCD == "F") {
                    resultText = "불량";
                }
                else {
                    resultText = "미대상";
                }

                td2.innerText = resultText;

                tr.append(td2);

                tList.append(tr);
            });

            if (i == 0) {
                var tr = document.createElement("tr");

                var td = document.createElement("td");

                td.className = "pageViewTd";
                td.style.textAlign = "left";
                td.colSpan = 2;
                td.innerText = "등록된 점검항목이 없습니다.";

                tr.append(td);

                tList.append(tr);
            }

            $("#hList").show();
            $("#tableList").show();
        };

        var fn_ControlBind_ChkList = function (json) {
            var div = $("#divResult");

            div.empty();
            
            var divTitle = document.createElement("div");
            divTitle.className = "div-chktitle";
            divTitle.innerText = "검사목록";

            div.append(divTitle);

            var divList = document.createElement("div");
            divList.className = "div-list";
            divList.setAttribute("id", "divList");

            var i = 0;
            var preUpperCd = "";
            var divGroup;

            $.each(JSON.parse(json), function (idx, item) {
                i++;

                if (preUpperCd != item.UPPERLISTCD) {
                    if (divGroup != undefined || divGroup != null) {
                        divList.append(divGroup);
                    }

                    divGroup = document.createElement("div");
                    divGroup.className = "div-chkgroup";

                    divUpper = document.createElement("div");
                    divUpper.className = "div-chkupper";
                    divUpper.innerText = item.UPPERLISTNM;

                    divGroup.append(divUpper);
                }

                var divChkList = document.createElement("div");
                divChkList.className = "div-chklist";
                divChkList.innerText = "- " + item.CHKLIST;
                
                divGroup.append(divChkList);

                divRb = document.createElement("div");
                divRb.className = "div-radio-box";

                var divS = document.createElement("div");
                divS.className = "div-radio";
                divS.setAttribute("onclick", "fn_radioBoxChk('rbS" + i + "')");

                var lblChkS = document.createElement("label");
                lblChkS.style.fontsize = "14px";

                var rbChkS = document.createElement("input");

                rbChkS.type = "radio";
                rbChkS.name = "rbChkList" + i;
                rbChkS.id = "rbS" + i;
                rbChkS.value = "S";
                rbChkS.style.marginRight = "3px";

                if (item.RESULTCD == "S") {
                    rbChkS.checked = true;
                }

                lblChkS.append(rbChkS);
                lblChkS.append("정상");

                divS.append(lblChkS);

                divRb.append(divS);

                var divF = document.createElement("div");
                divF.className = "div-radio";
                divF.setAttribute("onclick", "fn_radioBoxChk('rbF" + i + "')");

                var lblChkF = document.createElement("label");
                lblChkF.style.fontsize = "14px";

                var rbChkF = document.createElement("input");

                rbChkF.type = "radio";
                rbChkF.name = "rbChkList" + i;
                rbChkF.id = "rbF" + i;
                rbChkF.value = "F";
                rbChkF.style.marginRight = "3px";

                if (item.RESULTCD == "F") {
                    rbChkF.checked = true;
                }

                lblChkF.append(rbChkF);
                lblChkF.append("불량");

                divF.append(lblChkF);

                divRb.append(divF);

                var divN = document.createElement("div");
                divN.className = "div-radio";
                divN.setAttribute("onclick", "fn_radioBoxChk('rbN" + i + "')");

                var lblChkN = document.createElement("label");
                lblChkN.style.fontsize = "14px";

                var rbChkN = document.createElement("input");

                rbChkN.type = "radio";
                rbChkN.name = "rbChkList" + i;
                rbChkN.id = "rbN" + i;
                rbChkN.value = "N";
                rbChkN.style.marginRight = "3px";

                if (item.RESULTCD == "N") {
                    rbChkN.checked = true;
                }

                lblChkN.append(rbChkN);
                lblChkN.append("미대상");

                divN.append(lblChkN);

                divRb.append(divN);

                divGroup.append(divRb);

                preUpperCd = item.UPPERLISTCD;
            });

            if (divGroup != undefined || divGroup != null) {
                divList.append(divGroup);
            }

            divList.append(divGroup);

            var button1 = document.createElement("input");

            button1.type = "button";
            button1.setAttribute("onclick", "javascript:fn_RegInspectionInfo();");
            button1.setAttribute("style", "min-width:55px;width:55px;padding:0 2px;border-style:none;margin-right:3px");
            button1.className = "btn-green";
            button1.value = "저장";

            var button2 = document.createElement("input");

            button2.type = "button";
            button2.setAttribute("onclick", "javascript:fn_ChkListClose();");
            button2.setAttribute("style", "min-width:55px;width:55px;padding:0 2px;border-style:none;");
            button2.className = "btn-black";
            button2.value = "닫기";

            divList.append(button1);
            divList.append(button2);

            div.append(divList);

            $("#divModal4").show();
            $('html, body').scrollTop = 0;
            $('html, body').css('overflow', 'hidden');

            var divHeight = $("#divResult").height() - 100;

            $("#divList").height(divHeight);
        };

        var fn_ControlBind_Img = function (json, bEnabled, typeCd) {
            var tdImg;
            var divImgList;

            if (typeCd == 'R') {
                tdImg = $("#tdReqImg");
                divImgList = $("#divReqImg");
            }
            else {
                tdImg = $("#tdActImg");
            }

            tdImg.empty();

            $.each(JSON.parse(json), function (idx, item) {
                var img = document.createElement("img");
                var imgData = fn_ArrayToBase64(item.IMGDATA);

                img.setAttribute("id", "img" + item.IMG_NO);
                img.src = "data:" + item.IMGTYPE + "; base64," + imgData;
                img.style.maxWidth = "100%";

                tdImg.append(img);

                if (typeCd == 'R') {
                    var p = document.createElement("p");
                    p.className = "inpbox";
                    p.style.display = "block";
                    p.style.textAlign = "left";

                    var label = document.createElement("label");
                    label.setAttribute("id", "lblImg" + item.IMG_NO);
                    label.setAttribute("name", "lblReqFileNm");
                    label.style.height = "20px";
                    label.style.marginBottom = "3px";
                    label.style.maxWidth = "80%";
                    label.innerText = item.IMGNAME;

                    p.append(label);

                    if (bEnabled) {
                        var button = document.createElement("button");
                        button.type = "button";
                        button.setAttribute("id", "btnImgDel" + item.IMG_NO);
                        button.setAttribute("onclick", "javascript:fn_DelFileCheck(" + item.IMG_NO + ", 'R');");
                        button.style.width = "20px";
                        button.style.height = "20px";
                        button.style.lineHeight = "0";
                        button.style.color = "black"; 
                        button.style.padding = "0";
                        button.style.margin = "0";
                        button.style.border = "none";
                        button.innerText = "X";

                        p.append(button);
                    }

                    divImgList.append(p);
                }
            });
        };

        var fn_ModalOrgCd = function () {
            $("#divModal").show();
            $('html, body').css('overflow', 'hidden');
        };

        var fn_ModalKtsUser = function () {
            var orgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();

            if (orgCd != '' && orgCd != undefined) {
                $("#divModal2").show();
                $('html, body').css('overflow', 'hidden');
            }
            else {
                alert('소속 조직을 먼저 선택해주세요');
            }
        };

        var fn_ModalPartnersUser = function () {
            var CompCd = $("#<%= ddlCompany.ClientID %> option:selected").val();

            if (CompCd != '' && CompCd != undefined) {
                $("#divModal3").show();
                $('html, body').css('overflow', 'hidden');
            }
            else {
                alert('업체를 선택해주세요');
            }
        };

        var fn_ModalClose = function () {
            $("#<%= txbOrgNm.ClientID %>").val('');
            $("#<%= txbSearchKtsUser.ClientID %>").val('');
            $("#divModal").hide();
            $("#divModal2").hide();
            $("#divModal3").hide();

            $('html, body').css('overflow', 'auto');
        };

        var fn_ChkListClose = function () {
            if (confirm('현재 창을 닫으면 저장되지 않은 점검내용이 사라집니다.')) {
                $('html, body').css('overflow', 'auto');
                var div = $("#divResult");

                div.empty();
                $("#divModal4").hide();
            }
            else {
                return false;
            }
        };

        var fn_SetOrg = function (orgcd, orglv2nm, orglv3nm, orglv4nm, orgnm) {
            if (confirm("조직이 변경 될 경우 선택된 직원도 초기화됩니다.")) {
                var preOrgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();

                if (orgcd != preOrgCd) {
                    var orgNm = orglv2nm;

                    if (orglv3nm != '') {
                        orgNm += " " + orglv3nm;
                    }

                    if (orglv4nm != '') {
                        orgNm += " " + orglv4nm;
                    }

                    if (orgnm != '' && orglv2nm != orgnm && orglv3nm != orgnm && orglv4nm != orgnm) {
                        orgNm += " " + orgnm;
                    }

                    $("#<%= txbChkOrgNm.ClientID %>").val(orgNm);
                    $("#<%= txbChkEmpNm.ClientID %>").val('');
                    $("#<%= hdfChkOrgCd.ClientID %>").val(orgcd);
                    $("#<%= hdfChkEmpNo.ClientID %>").val('');
                }

                fn_ModalClose();
            }
            else {
                return false;
            }
        };

        var fn_SetKtsUser = function (empno, empnm) {
            var preKtsUser = $("#<%= hdfChkEmpNo.ClientID %>").val();

            if (empno != preKtsUser) {
                $("#<%= hdfChkEmpNo.ClientID %>").val(empno);
                $("#<%= txbChkEmpNm.ClientID %>").val(empnm);
            }

            fn_ModalClose();
        };

        var fn_SetPartnersUser = function (empno, empnm) {
            var CompCd = $("#<%= ddlCompany.ClientID %> option:selected").val();
            var delegate = "";

            if (CompCd != "") {
                delegate = CompCd.split('_')[2];
            }

            if (empno != '') {
                $("#<%= txbPartnersEmpNm.ClientID %>").val(empnm);
            } else {
                if (delegate == "Y") {
                    $("#<%= txbPartnersEmpNm.ClientID %>").val("협력사 대표자");
                }
                else {
                    $("#<%= txbPartnersEmpNm.ClientID %>").val("");
                }
            }

            $("#<%= hdfPartnersEmpNo.ClientID %>").val(empno);

            fn_ModalClose();
        };

        var fn_RegInspectionInfo = function () {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var status = $("#<%= hdfChkStatus.ClientID %>").val();

            var confMsg = "";

            var bChk = false;
            var sUrl = "";
            var aData = [];

            var ChkDt = $("#<%= txbChkDt.ClientID %>").val();
            var ChkOrgCd = $("#<%= hdfChkOrgCd.ClientID %>").val();
            var ChkUser = $("#<%= hdfChkEmpNo.ClientID %>").val();
            var CompanyCd = $("#<%= ddlCompany.ClientID %>").val().replace("_Y", "").replace("_N", "");;
            var ActUser = $("#<%= hdfPartnersEmpNo.ClientID %>").val();
            var Noti = $("#<%= txbNoti.ClientID %>").val();
            var LineNo = $("#<%= txbLineNo.ClientID %>").val();
            var CustNm = $("#<%= txbCustNm.ClientID %>").val();
            var Lv1Grp = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();
            var Lv2Grp = $("#ddlLv2Grp option:selected").val();
            var Lv3Grp = $("#ddlLv3Grp option:selected").val();

            var div = $("#divResult");

            if (inspectionNo == "") {
                confMsg = "조치요청서를 등록 하시겠습니까?"
                callGb = "InsOk";
                sUrl = "InsInspectionInfo";
            }
            else {
                confMsg = "등록된 조치요청서를 수정 하시겠습니까?"
                callGb = "UpdOk";
                sUrl = "UpdInspectionInfo";
            }

            if (confirm(confMsg)) {
                var message = "";
                var ChkResult = "";

                var rbNm = "";

                div.find("input[type='radio']").each(function (index, item) {
                    if (rbNm != $(item).attr("name")) {
                        rbNm = $(item).attr("name");

                        if (ChkResult != "") {
                            ChkResult += "^";
                        }

                        var val = $("input[name='" + rbNm + "']:checked").val();

                        if (val == undefined) {
                            ChkResult += " ";
                        } else {
                            ChkResult += val;
                        }
                    }
                });

                ChkResult = ChkResult.replaceAll(' ', '') + "^";

                if (ChkDt == "" || ChkOrgCd == "" || ChkUser == "" || CompanyCd == "" || Lv1Grp == "" || Lv2Grp == "" || Lv3Grp == "") {
                    message = "잘못된 접근입니다.";
                }
                else {
                    bChk = true;
                }

                aData[0] = ChkDt;
                aData[1] = ChkOrgCd;
                aData[2] = ChkUser;
                aData[3] = CompanyCd;
                aData[4] = ActUser;
                aData[5] = Noti;
                aData[6] = LineNo;
                aData[7] = CustNm;
                aData[8] = Lv1Grp;
                aData[9] = Lv2Grp;
                aData[10] = Lv3Grp;
                aData[11] = ChkResult;

                if (callGb == "UpdOk") {
                    aData[12] = inspectionNo;
                }

                if (bChk) {
                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
                else {
                    alert(message);
                }
            }
        };

        var fn_RegFileCheck = function (typeCd) {
            if (confirm("참조 파일을 저장 하시겠습니까?")) {
                fn_RegImgData(typeCd);
            }
        };

        var fn_DelFileCheck = function (imgNo, typeCd) {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                fn_DelImgData(imgNo, typeCd);
            }
        };

        var fn_RegImgData = function (typeCd) {
            var imgFiles = $("#filebox").find("input[type='file']");
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo == "") {
                alert("잘못된 접근입니다.");
            }
            else if (imgFiles.length == 0) {
                alert("선택된 파일이 없습니다.");
            }
            else {
                var formData = new FormData();

                for (var i = 0; i < imgFiles.length; i++) {
                    var keyNm = inspectionNo + "_" + typeCd + "_" + i;

                    formData.append(keyNm, imgFiles[i].files[0]);
                }

                var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/QC/FileUploadHandler.ashx");

                callGb = "FileInsOk";

                ajaxCallFileUpload(formData, ajaxUrl);
            }
        };

        var fn_DelImgData = function (imgNo, typeCd) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var status = $("#<%= hdfChkStatus.ClientID %>").val();
            var message = "";

            var bChk = false;
            var aData = [];

            if (inspectionNo == "" || imgNo.length == 0 || imgNo == undefined || typeCd == "" || typeCd == undefined) {
                message = "잘못된 접근입니다.";
            }
            else {
                bChk = true;

                callGb = 'FileDelOk';

                aData[0] = inspectionNo;
                aData[1] = typeCd;
                aData[2] = imgNo;
            }

            if (bChk) {
                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "DelImgData");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            else {
                alert(message);
            }
        };

        var fn_GegImgData = function (typeCd) {

            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo != "" && inspectionNo != undefined) {
                //입력 파라미터
                var aData = [];
                aData[0] = inspectionNo;
                aData[1] = typeCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetImgData');

                callGb = 'GetImgData';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_SetStatus = function (status) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var ChkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var ActStatus = $("#<%= hdfActStatus.ClientID %>").val();
            var reqNoti = $("#<%= txbReqNoti.ClientID %>").val();
            var rejectNoti = $("#<%= txbReject.ClientID %>").val();

            var message = '';

            var bChk = false;
            var aData = [];

            if (inspectionNo != "") {

                if (status == 'R') {
                    message = "요청사항에 대한 조치요청을 하시겠습니까?";
                    callGb = 'SetStatusOK_R';

                    if (reqNoti != '') {
                        bChk = true;
                    }
                    else {
                        message = "요청사항을 입력해주세요.";
                    }
                }
                else if (status == 'B') {

                    message = "조치완료 사항을 반려하고 재요청하시겠습니까?";
                    callGb = 'SetStatusOK_B';

                    if (rejectNoti == '') {
                        message = "반려사유을 입력해주세요.";
                    }
                    else if (reqNoti == '') {
                        message = "요청사항을 입력해주세요.";
                    }
                    else {
                        bChk = true;
                    }
                }
                else if (status == 'F') {
                    message = "조치완료 처리 하시겠습니까?";
                    callGb = 'SetStatusOK_F';
                    bChk = true;
                }
                else {
                    message = "잘못된 접근입니다.";
                }
            }
            else {
                message = "잘못된 접근입니다.";
            }

            if (bChk) {
                if (confirm(message)) {
                    bChk = true;

                    aData[0] = inspectionNo;
                    aData[1] = status;
                    aData[2] = reqNoti;
                    aData[3] = rejectNoti;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetInspectionStatus");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
            else {
                alert(message);
            }
        };

        var fn_DelInspectionInfo = function (inspection_no) {
            if (inspection_no == '' || inspection_no == undefined) {
                alert("잘못된 접근입니다.");
            }
            else {
                var aData = [];

                callGb = "DelOk";

                aData[0] = inspection_no;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "DelInspectionInfo");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        };

        var AddFileUpload = function () {
            var cnt1 = $("#filebox").find("input[type='file']").length;
            var cnt2 = $("#divReqImg").find("label[name='lblReqFileNm']").length;

            if (cnt1 + cnt2 >= 4) {
                return;
            }

            if (!document.getElementById && !document.createElement)
                return;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-right:0;");
            newFileBox.setAttribute("name", "reqfile");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:90%;");
            newFileText.style.marginLeft = "0";

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");
            newFileSpan.style.width = "30px";

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "fileTypeChk('fu" + AddFileUpload.lastAssignedId + "', this.value);");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("accept", "image/*");

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.style.width = "30px";
            newFileLabel.innerText = "...";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fileTypeChk = function (id, val) {
            var filechk = false;

            var extIndex = val.lastIndexOf('.');

            var extList = ["tiff", "pjp", "jfif", "bmp", "gif", "svg", "png", "xbm", "dib", "jxl", "jpeg", "svgz", "jpg", "webp", "ico", "tif", "pjpeg", "avif"];

            if (extIndex > 1) {

                var fileExt = val.substring(extIndex + 1, val.length).toLowerCase();

                if (extList.indexOf(fileExt) > 0) {
                    filechk = true;
                }
            }
            if (filechk) {
                document.getElementById(id + "-value").value = val;
            }
            else {
                alert("이미지파일만 업로드 가능합니다.");

                document.getElementById(id).value = "";
                document.getElementById(id + "-value").value = "";
            }
        };

        var fn_PageMoveMain = function () {
            var stdt = $("#<%= hdfStdt.ClientID %>").val();
            var endt = $("#<%= hdfEndt.ClientID %>").val();
            var chkCode = $("#<%= hdfChkCode.ClientID %>").val();
            var lv1GrpCd = $("#<%= hdfLv1.ClientID %>").val();

            var param = {
                pSTDT: stdt,
                pENDT: endt,
                pCHKCODE: chkCode,
                pLV1CD: lv1GrpCd
            };

            var form = createForm("/Mobile/InspectionListM", param);

            form.submit();
        };

        var fn_Reload = function (inspection_no) {
            var stdt = $("#<%= hdfStdt.ClientID %>").val();
            var endt = $("#<%= hdfEndt.ClientID %>").val();
            var chkCode = $("#<%= hdfChkCode.ClientID %>").val();
            var lv1GrpCd = $("#<%= hdfLv1.ClientID %>").val();

            var param = {
                pSTDT: stdt,
                pENDT: endt,
                pCHKCODE: chkCode,
                pLV1CD: lv1GrpCd,
                pINSPECTION_NO: inspection_no
            };

            var form = createForm("/Mobile/RegInspectionInfoM", param);

            form.submit();
        };
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<div class="page">
            <h2 id="htitle" style="margin-bottom:5px;padding-bottom:5px;">
                <strong id="stitle">품질검사 조치요청서</strong>
			    <input id="btnDelete" runat="server" type="button" class="btn-black" style="width:40px;min-width:40px;height:24px;line-height:24px;padding:0px;margin-top:1px;border-style:none;float:right;" value="삭제" />
            </h2>
            <h4>kt service 북부 검사원&nbsp;<span style="color:red;text-align:center;">*</span></h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">검사일</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:Label ID="lblChkDt" runat="server" Visible="false"></asp:Label>
                                <asp:TextBox ID="txbChkDt" class="date" runat="server" Width="100px" ReadOnly="true" BackColor="white" placeholder="YYYY-MM-DD"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">소속</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbChkOrgNm" runat="server" BackColor="white" ReadOnly="true"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">검사원</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbChkEmpNm" runat="server" Width="180px" BackColor="white" ReadOnly="true"></asp:TextBox>
                                <asp:HiddenField ID="hdfChkOrgCd" runat="server" />
                                <asp:HiddenField ID="hdfChkEmpNo" runat="server" />
							</p>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">검사대상&nbsp;<span style="color:red;text-align:center;">*</span></h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">업체명</th>
                        <td class="pageViewTd">
                            <span class="optionbox">
                                <asp:DropDownList ID="ddlCompany" style="width: 180px;height: 30px;line-height: 30px;" runat="server">
                                </asp:DropDownList>
                            </span>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">현장대리인</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbPartnersEmpNm" runat="server" Width="180px" BackColor="white" ReadOnly="true"></asp:TextBox>
                                <asp:HiddenField ID="hdfPartnersEmpNo" runat="server" />
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">주요내용</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbNoti" runat="server" BackColor="white"></asp:TextBox>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">고객정보</h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">회선번호</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbLineNo" runat="server" Width="180px" BackColor="white"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">고객명</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbCustNm" Width="180px" runat="server" BackColor="white"></asp:TextBox>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">조치항목 분류&nbsp;<span style="color:red;text-align:center;">*</span></h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">대분류</th>
                        <td class="pageViewTd">
                            <span class="optionbox">
                                <asp:DropDownList ID ="ddlLv1Grp" runat="server" style="width:180px;height: 30px;line-height: 30px;" AutoPostBack="false"></asp:DropDownList>
                                <asp:HiddenField ID="hdfLv1Grp" runat="server" />
                            </span>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">중분류</th>
                        <td class="pageViewTd">
                            <span class="optionbox">
                                <select id="ddlLv2Grp" style="width:180px;height: 30px;line-height: 30px;" disabled="disabled">
                                    <option value="">선택하세요</option>
                                </select>
                                <asp:HiddenField ID="hdfLv2Grp" runat="server" />
                            </span>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">소분류</th>
                        <td class="pageViewTd">
                            <span class="optionbox">
                                <select id="ddlLv3Grp" style="width:180px;height: 30px;line-height: 30px;" disabled="disabled">
                                    <option value="">선택하세요</option>
                                </select>
                                <asp:HiddenField ID="hdfLv3Grp" runat="server" />
                            </span>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">
                <label id="lblList" style="display:none;">검사목록</label>
                <input id="btnChkList" runat="server" type="button" class="btn-green" style="display:none;min-width:55px;width:55px;padding:0 2px;margin-top:-35px;border-style:none;position:absolute;right:0;" value="점검등록"/>                
            </h4>
			<table id="tableList" class="pageView" style="display:none;">
				<colgroup>
                    <col style="width:20%;min-width:20%;max-width:20%;" />
                    <col style="width:65%;min-width:65%;max-width:65%;" />
                    <col />
				</colgroup>
                <thead>
                    <tr>
                        <th class="pageViewTh" colspan="2" style="border-right:1px solid #dfdfdf">항목</th>
                        <th class="pageViewTh">결과</th>
                    </tr>
                </thead>
                <tbody id="tList">
				</tbody>
			</table>
            <h4 id="hReq" style="display:none;" class="hTitle">조치요청</h4>
			<table id="tableReq" class="pageView" style="display:none;">
                <thead>
                    <tr>
                        <th class="pageViewTh">요청사항</th>
                    </tr>
                </thead>
                <tbody >
					<tr>
                        <td id="tdReqImg" class="pageViewTd" style="height:200px;vertical-align:top;white-space:normal">
                        </td>
					</tr>
					<tr id="trReqImgNm" runat="server" style="height:80px;vertical-align:top;">
                        <td class="pageViewTd">
			                <div style="text-align:right;">
                                <button type="button" id="btnReqAdd" runat="server" class="btn-replysave" style="width:30px;height:33px;">추가</button>
                                <button type="button" id="btnReqImg" runat="server" class="btn-green">사진저장</button>
			                </div>
                            <div>
                                <div id="filebox">
                                </div>                                                    
                            </div>
                            <div id="divReqImg" style="width:100%;overflow-x:hidden;overflow-y:auto; margin-top:2px; margin-bottom:2px">
                            </div>
                        </td>
					</tr>
					<tr>
                        <td class="pageViewTd">
                            <p class="inpbox">                                                    
                                <asp:TextBox ID="txbReqNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px;min-height:100px"></asp:TextBox>
                            </p>   
                            <button type="button" id="btnReqAct" runat="server" class="btn-green" style="margin-top:6px;float:right">조치요청</button>
                        </td>
					</tr>
				</tbody>
			</table>
            <h4 id="hAct" style="display:none;" class="hTitle">요청사항 조치결과</h4>
			<table id="tableAct" class="pageView" style="display:none;">
                <thead>
                    <tr>
                        <th class="pageViewTh">조치결과</th>
                    </tr>
                </thead>
                <tbody >
					<tr>
                        <td id="tdActImg" class="pageViewTd" style="height:200px;vertical-align:top;white-space:normal">
                        </td>
					</tr>
					<tr>
                        <td class="pageViewTd">
                            <p class="inpbox">                                                    
                                <asp:TextBox ID="txbActNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px;min-height:100px" ReadOnly="true"></asp:TextBox>
                            </p>
                        </td>
					</tr>
				</tbody>
			</table>
            <h4 id="hReject" class="hTitle" runat="server" visible="false">반려사유</h4>
			<table id="tableReject" class="pageView" runat="server" visible="false">
                <tbody>
					<tr>
                        <td class="pageViewTd">
                            <p class="inpbox">                                                    
                                <asp:TextBox ID="txbReject" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px;min-height:100px" ReadOnly="true"></asp:TextBox>
                            </p>
                        </td>
					</tr>
				</tbody>
			</table>
			<div style="text-align:center;margin-top:20px;">
                <input id="btnChkFin" runat="server" type="button" class="btn-green" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="완료"/>
                <input id="btnReject" runat="server" type="button" class="btn-gray" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="반려"/>
                <input id="btnBack" type="button" class="btn-black" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" onclick="fn_PageMoveMain();" value="이전화면"/>
			</div>
            <asp:HiddenField ID="hdfInspection_No" runat="server"/>
            <asp:HiddenField ID="hdfChkStatus" runat="server"/>
            <asp:HiddenField ID="hdfActStatus" runat="server"/>
            <asp:HiddenField ID="hdfResult" runat="server"/>
            <asp:HiddenField ID="hdfLv2" runat="server"/>
            <asp:HiddenField ID="hdfLv3" runat="server"/>
            <asp:HiddenField ID="hdfReqImg" runat="server"/>
            <asp:HiddenField ID="hdfActImg" runat="server"/>
            <asp:HiddenField ID="hdfStdt" runat="server"/>
            <asp:HiddenField ID="hdfEndt" runat="server"/>
            <asp:HiddenField ID="hdfChkCode" runat="server"/>
            <asp:HiddenField ID="hdfLv1" runat="server"/>

            <!-- S: divModal -->
			<div id="divModal" class="scrollbox" style="display:none;position:fixed;width:100%;height:330px;top:20%;left:0;background:rgb(255, 255, 255); border: 1px solid black; border-radius:10px;">  
                <div style="z-index: 97;position: fixed;top: 0;width: 100%;height: 100%;background-color: #ffffff !important;opacity: 0.7;"></div>
				<div class="requestlist" style="z-index: 99;position: fixed;width:90%;text-align:center;margin-left:5%">
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; padding-top:20px; font-size:0;">
						<label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">조직명</label>
						<asp:TextBox ID="txbOrgNm" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
						<input id="btnOrgSearch" class="btn-green" style="width:33px;margin:0 !important;color:white;border-style:none;" value="조회" />
					</span>

					<div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<!-- S:scrollbox -->
						<div class="scrollbox">
							<div id="ktsGrid1"></div>
						</div>
						<!-- E:scrollbox -->
					</div>

					<input type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ModalClose()" value="닫기" />
				</div>
			</div>			
			<div id="divModal2" class="scrollbox" style="display:none;position:fixed;width:100%;height:330px;top:20%;left:0;background:rgb(255, 255, 255); border: 1px solid black; border-radius:10px;">  
                <div style="z-index: 97;position: fixed;top: 0;width: 100%;height: 100%;background-color: #ffffff !important;opacity: 0.7;"></div>           
				<div class="requestlist" style="z-index: 99;position: fixed;width:90%;text-align:center;margin-left:5%">
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; padding-top:20px; font-size:0;">
						<label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">사원명</label>
						<asp:TextBox ID="txbSearchKtsUser" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
						<input id="btnKtsUserSearch" class="btn-green" style="width:33px;margin:0 !important;color:white;border-style:none;" value="조회" />
					</span>

					<div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<!-- S:scrollbox -->
						<div class="scrollbox">
							<div id="ktsGrid2"></div>
						</div>
						<!-- E:scrollbox -->
					</div>

					<input type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ModalClose()" value="닫기" />
				</div>
			</div>
			<div id="divModal3" class="scrollbox" style="display:none;position:fixed;width:100%;height:330px;top:20%;left:0;background:rgb(255, 255, 255); border: 1px solid black; border-radius:10px;">
                <div style="z-index: 97;position: fixed;top: 0;width: 100%;height: 100%;background-color: #ffffff !important;opacity: 0.7;"></div>    
				<div class="requestlist" style="z-index: 99;position: fixed;width:90%;text-align:center;margin-left:5%">
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; padding-top:20px; font-size:0;">
						<label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">이름</label>
						<asp:TextBox ID="txbSearchPUser" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
						<input id="btnPUserSearch" class="btn-green" style="width:33px;margin:0 !important;color:white;border-style:none;" value="조회" />
					</span>

					<div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<!-- S:scrollbox -->
						<div class="scrollbox">
							<div id="ktsGrid3"></div>
						</div>
						<!-- E:scrollbox -->
					</div>
                    
					    <input type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ModalClose()" value="닫기" />
			    </div>
			    <!--E: divModal -->
		    </div>		        
	    </div>
		<div id="divModal4" style="display:none;position:fixed;width:100%;height:100%;top:66.5px;left:0;background:#edf1f1">
            <div id="divResult" style="width:95%;height:100%;text-align:center;margin-left:2.5%;padding-top:5px">
            </div>
	    </div>
	    <!--//E: contentsarea -->
    </div>
</asp:Content>