﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Order;

namespace KTSSolutionWeb
{
    public partial class OrderInfoByOrderNum : PageBase
    {
        private string ORDERNUM
        {
            get
            {
                if (ViewState["ORDERNUM"] != null)
                    return ViewState["ORDERNUM"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORDERNUM"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    initializeControls();
                    GetDataList();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                ORDERNUM = Request.Form["pORDERNUM"] == null ? "" : Request.Form["pORDERNUM"].ToString();

                if (ORDERNUM.Equals(""))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');window.close();", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();
            DataSet dsFile = new DataSet();

            try
            {
                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    ds = mgmt.GetOrderNumInfo(ORDERNUM);
                    dsFile = mgmt.GetOrderFile(ORDERNUM);
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.lblWorkState.Text = ds.Tables[0].Rows[0]["WORKSTATENM"].ToString();

                    this.txbSumupDate.Text = ds.Tables[0].Rows[0]["SUMUPDATE"].ToString();
                    this.txbRegOrgNm.Text = ds.Tables[0].Rows[0]["REGORGNM"].ToString();
                    this.txbEmpNo.Text = ds.Tables[0].Rows[0]["REGEMPNO"].ToString();
                    this.txbEmpNm.Text = ds.Tables[0].Rows[0]["REGEMPNM"].ToString();
                    this.txbOrderName.Text = ds.Tables[0].Rows[0]["ORDERNAME"].ToString();
                    this.txbWorkType.Text = ds.Tables[0].Rows[0]["WORKTYPE"].ToString();
                    this.txbFranchiseNm.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                    this.txbCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();
                    this.txbAddr.Text = ds.Tables[0].Rows[0]["ADDR"].ToString();
                    this.txbPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();
                    this.txbHopDt.Text = ds.Tables[0].Rows[0]["HOPEDT"].ToString();
                    this.txbEquipment.Text = ds.Tables[0].Rows[0]["EQUIPMENT"].ToString();
                    this.txbOrderState.Text = ds.Tables[0].Rows[0]["ORDERSTATE"].ToString();
                    this.txbNote.Text = ds.Tables[0].Rows[0]["NOTE"].ToString();
                    this.txbReason.Text = ds.Tables[0].Rows[0]["REASON"].ToString();

                    string strOrderCode = ds.Tables[0].Rows[0]["ORDERCODE"].ToString();
                    string strWorkState = ds.Tables[0].Rows[0]["WORKSTATE"].ToString();

                    if (dsFile.Tables.Count > 0)
                    {
                        if (dsFile.Tables[0].Rows.Count > 0)
                        {

                            rptFile.DataSource = dsFile.Tables[0];
                            rptFile.DataBind();

                            for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                            {
                                if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                {
                                    HtmlImage ImgBoard = new HtmlImage();

                                    string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                    byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                    ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                    ImgBoard.Attributes.Add("style", "max-width:100%;height:auto;");

                                    spanImg.Controls.Add(ImgBoard);
                                }
                            }

                            if (this.Session.Count > 0)
                            {
                                divFileList.Visible = true;
                            }
                            else
                            {
                                divFileList.Visible = false;
                            }
                        }
                        else
                        {
                            divFileList.Visible = false;
                            trImage.Visible = false;
                        }
                    }

                    if (strOrderCode.Equals("3") || strOrderCode.Equals("4"))
                    {
                        this.thName.InnerHtml = "관리번호";
                        this.thNum.InnerHtml = "접속번호";
                    }
                    else
                    {
                        this.thName.InnerHtml = "업체명";
                        this.thNum.InnerHtml = "사업자등록번호";
                        this.thNum.Style.Add("font-size", "11px");
                    }

                    if (strOrderCode.Equals("3"))
                    {
                        thPhoneNo.InnerHtml = "장소명";
                        thEquipment.InnerHtml = "모뎀위치";
                    }
                    else
                    {
                        thPhoneNo.InnerHtml = "연락처";
                        thEquipment.InnerHtml = "사용장비";
                    }

                    if (strWorkState.Equals("2"))
                    {
                        btnAccept.Visible = true;
                        btnAccept.Enabled = true;

                        btnSucc.Visible = false;
                        btnSucc.Enabled = false;

                        btnImpossible.Visible = false;
                        btnImpossible.Enabled = false;

                        btnHold.Visible = false;
                        btnHold.Enabled = false;
                    }
                    else if (strWorkState.Equals("3"))
                    {
                        btnAccept.Visible = false;
                        btnAccept.Enabled = false;

                        btnSucc.Visible = true;
                        btnSucc.Enabled = true;

                        btnImpossible.Visible = true;
                        btnImpossible.Enabled = true;

                        btnHold.Visible = true;
                        btnHold.Enabled = true;

                        txbReason.ReadOnly = false;
                        divFile.Visible = true;
                    }
                    else if (strWorkState.Equals("4") || strWorkState.Equals("5"))
                    {
                        btnAccept.Visible = false;
                        btnAccept.Enabled = false;

                        btnSucc.Visible = false;
                        btnSucc.Enabled = false;

                        btnImpossible.Visible = false;
                        btnImpossible.Enabled = false;

                        btnHold.Visible = false;
                        btnHold.Enabled = false;

                        txbReason.ReadOnly = true;
                        divFile.Visible = false;

                        for (int i = 0; i < rptFile.Items.Count; i++)
                        {
                            HtmlButton btnDelete = (HtmlButton)rptFile.Items[i].FindControl("btnDelete");

                            btnDelete.Visible = false;
                            btnDelete.Attributes.Remove("onclick");
                            btnDelete.Attributes.Remove("onserverclick");
                        }
                    }
                    else if (strWorkState.Equals("6"))
                    {
                        btnAccept.Visible = false;
                        btnAccept.Enabled = false;

                        btnSucc.Visible = true;
                        btnSucc.Enabled = true;

                        btnImpossible.Visible = true;
                        btnImpossible.Enabled = true;

                        btnHold.Visible = false;
                        btnHold.Enabled = false;

                        txbReason.ReadOnly = false;
                        divFile.Visible = true;
                    }
                    else if (strWorkState.Equals("7"))
                    {
                        btnAccept.Visible = true;
                        btnAccept.Enabled = true;

                        btnSucc.Visible = false;
                        btnSucc.Enabled = false;

                        btnImpossible.Visible = false;
                        btnImpossible.Enabled = false;

                        btnHold.Visible = false;
                        btnHold.Enabled = false;

                        txbReason.ReadOnly = true;
                        divFile.Visible = false;

                        for (int i = 0; i < rptFile.Items.Count; i++)
                        {
                            HtmlButton btnDelete = (HtmlButton)rptFile.Items[i].FindControl("btnDelete");

                            btnDelete.Visible = false;
                            btnDelete.Attributes.Remove("onclick");
                            btnDelete.Attributes.Remove("onserverclick");
                        }
                    }
                    else
                    {
                        btnAccept.Visible = false;
                        btnAccept.Enabled = false;

                        btnSucc.Visible = false;
                        btnSucc.Enabled = false;

                        btnImpossible.Visible = false;
                        btnImpossible.Enabled = false;

                        btnHold.Visible = false;
                        btnHold.Enabled = false;

                        txbReason.ReadOnly = true;
                        divFile.Visible = false;

                        for (int i = 0; i < rptFile.Items.Count; i++)
                        {
                            HtmlButton btnDelete = (HtmlButton)rptFile.Items[i].FindControl("btnDelete");

                            btnDelete.Visible = false;
                            btnDelete.Attributes.Remove("onclick");
                            btnDelete.Attributes.Remove("onserverclick");
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void InsOrderImageFiles()
        {
            Stream stream = null;

            try
            {
                HttpFileCollection uploadFiles = Request.Files;

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];

                    stream = postedFIle.InputStream;

                    if (stream.Length == 0)
                        continue;

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    //type체크 필요 
                    if (strFileType.Contains("image"))
                    {
                        int nFileLen = Convert.ToInt32(stream.Length);

                        byte[] byteFile = new byte[nFileLen];

                        stream.Read(byteFile, 0, nFileLen);

                        using (OrderMgmt mgmt = new OrderMgmt())
                        {
                            mgmt.InsOrderImageFile(ORDERNUM, strFileNm, byteFile, strFileType, nFileLen);
                        }
                    }

                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    mgmt.UpdateOrderNumWorkState(ORDERNUM, "3", "", "");
                }

                GetDataList();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetWorkState", "alert('해당 오더를 수령했습니다.');", true);
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSucc_Click(object sender, EventArgs e)
        {
            try
            {
                string strReason = this.txbReason.Text;
                string strDate = Request.Form[this.txbHopDt.UniqueID];

                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    mgmt.UpdateOrderNumWorkState(ORDERNUM, "4", strReason, strDate);
                }

                InsOrderImageFiles();

                string strSumupDate = this.txbSumupDate.Text;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetWorkState", "alert('해당 오더가 작업완료 처리 되었습니다.');PageMoveMain('" + strSumupDate + "');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnImpossible_Click(object sender, EventArgs e)
        {
            try
            {
                string strReason = this.txbReason.Text;
                string strDate = Request.Form[this.txbHopDt.UniqueID];

                if (strReason.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (OrderMgmt mgmt = new OrderMgmt())
                    {
                        mgmt.UpdateOrderNumWorkState(ORDERNUM, "5", strReason, strDate);
                    }

                    InsOrderImageFiles();

                    string strSumupDate = this.txbSumupDate.Text;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetWorkState", "alert('해당 오더가 작업불가 처리 되었습니다.');PageMoveMain('" + strSumupDate + "');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnHold_Click(object sender, EventArgs e)
        {
            try
            {
                string strReason = this.txbReason.Text;
                string strDate = Request.Form[this.txbHopDt.UniqueID];

                if (strReason.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    using (OrderMgmt mgmt = new OrderMgmt())
                    {
                        mgmt.UpdateOrderNumWorkState(ORDERNUM, "6", strReason, strDate);
                    }

                    InsOrderImageFiles();

                    GetDataList();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetWorkState", "alert('해당 오더가 작업보류 처리 되었습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlButton btnDownload = (HtmlButton)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    mgmt.DelOrderImageFile(ORDERNUM, lblSeq.Text);
                }

                GetDataList();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "alert('파일 삭제를 완료 했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}