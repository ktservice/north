﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Order;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class UserCarReservationM : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetPageInit();
                }
            }
        }
        #endregion

        #endregion

        #region Method

        #region SetPageInit
        /// <summary>
        /// 페이지 초기화
        /// </summary>
        private void SetPageInit()
        {
            try
            {
                SetDDLCarNo();

                this.txbDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.hdfEmpNo.Value = Session["EMPNO"].ToString();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region SetDDLCarNo
        /// <summary>
        /// 차량번호 ddl 세팅
        /// </summary>
        private void SetDDLCarNo()
        {
            DataSet ds = null;

            try
            {
                string resultSession = string.Empty;
                string rptCd = "USERTEAMCARLIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                string empNo = HttpContext.Current.Session["EMPNO"].ToString();

                aData.Add(empNo); //파라미터 추가 시
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PageInit", "alert('오류가 발생했습니다.');", true);
                }

                ddlCarNo.Items.Clear();
                ddlCarNo.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlCarNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CARNO"].ToString(), ds.Tables[0].Rows[i]["CARID"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region GetCarInfo
        /// <summary>
        /// 차량정보조회
        /// </summary>
        /// <param name="aData">
        /// aData[0] = carId;
        /// </param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARINFOBYCARID";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
        #endregion

        #region GetResvInfo
        /// <summary>
        /// 예약정보조회
        /// </summary>
        /// <param name="aData"></param>
        /// aData[0] = carId;
        /// aData[1] = resvDt;
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetResvInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARRESVINFO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
        #endregion

        #region InsResvinfo
        /// <summary>
        /// 예약등록
        /// </summary>
        /// <param name="aData">
        /// aData[0] = carId;
        /// aData[1] = resvReason;
        /// aData[2] = resvDt;
        /// aData[3] = StTime;
        /// aData[4] = EnTime;
        /// </param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsResvinfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARRESVINFOINSERT";
            string jobGb = "SET";

            //aData[0] = carId;
            //aData[1] = resvReason;
            //aData[2] = resvDt;
            //aData[3] = StTime;
            //aData[4] = EnTime;

            string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(strEmpNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
        #endregion

        #region DelResvinfo
        /// <summary>
        /// 예약취소
        /// </summary>
        /// <param name="aData">
        /// aData[0] = carId;
        /// aData[1] = resvDt;
        /// aData[2] = resvTime;
        /// aData[3] = resvEmpNo;
        /// </param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelResvinfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARRESVINFODELETE";
            string jobGb = "SET";

            //aData[0] = carId;
            //aData[1] = resvDt;
            //aData[2] = resvTime;
            //aData[3] = resvEmpNo;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
        #endregion

#endregion
    }
}