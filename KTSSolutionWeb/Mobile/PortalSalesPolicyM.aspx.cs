﻿using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Framework.Web;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class PortalSalesPolicyM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetPageInit();
                }
            }
        }

        string authCheck()
        {
            string rtn = string.Empty;
            string authId = string.Empty;
            authId = this.Session["AUTHID"].ToString();

            if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004"))
            {
                rtn = "A";
            }
            else
            {
                rtn = "B";
            }

            return rtn;
        }

        private void SetPageInit()
        {
            SetTabs();
        }

        void SetTabs()
        {
            string ulScript = string.Empty;

            DataSet dsList = new DataSet();

            dsList = GetDbList("all");

            if(dsList.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsList.Tables[0].Rows.Count; i++)
                {
                    string classCd = dsList.Tables[0].Rows[i]["CLASSCD"].ToString();
                    string classNm = dsList.Tables[0].Rows[i]["CLASSNM"].ToString();
                    string fileNm = dsList.Tables[0].Rows[i]["FILENM"].ToString();
                    string tabId = "tab" + i.ToString();
                    string tabTitle = classNm;
                    string imgId = "img" + i.ToString();

                    HtmlGenericControl divImg = new HtmlGenericControl("div");

                    if (fileNm.Equals(""))
                    {
                        HtmlGenericControl divAlert = new HtmlGenericControl("div");
                        divAlert.Attributes.Add("class", "noticeAlert");
                        divAlert.InnerHtml = "입력된 판매정책이 없습니다.";

                        divImg.Controls.Add(divAlert);
                        divImg.Attributes.Add("class", "noticeListA");
                    }
                    else
                    {
                        HtmlImage Img = new HtmlImage();
                        divImg = GetImageFile(classCd);
                        divImg.Attributes.Add("class", "noticeList");
                    }

                    HtmlGenericControl divTab = new HtmlGenericControl("div");
                    divTab.ID = tabId;



                    
                    divImg.Controls.Add(new LiteralControl("<br/>"));
                    divTab.Controls.Add(divImg);
                    divTabcontent.Controls.Add(divTab);

                    ulScript += string.Format("<li><a href='#{0}'>{1}</a></li>", divTab.ClientID, tabTitle);

                }

                ulTabnav.InnerHtml = ulScript;
            }
        }

        private HtmlGenericControl GetImageFile(string classCd)
        {
            HtmlGenericControl divImg = new HtmlGenericControl();
            DataSet dsFile = new DataSet();
            DataSet dsSeq = new DataSet();

            dsFile = GetDbFile(classCd);

            if (dsFile.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                {
                    if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                    {
                        HtmlImage ImgBoard = new HtmlImage();
                        string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                        byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                        ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);
                        ImgBoard.Attributes.Add("style", "max-width:100%");

                        divImg.Controls.Add(ImgBoard);
                    }
                }

            }

            return divImg;
        }

        DataSet GetDbList(string strClassCd)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pCLASSCD", MySqlDbType.VarChar, 10);
                parameters[0].Value = strClassCd;

                CommonData data = new CommonData("UP_PORTALSALESPOLICYM_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        DataSet GetDbFile(string strClassCd)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pCLASSCD", MySqlDbType.VarChar, 20);
                parameters[0].Value = strClassCd;

                CommonData data = new CommonData("UP_PORTALSALESPOLICY_FILE_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
    }
}
