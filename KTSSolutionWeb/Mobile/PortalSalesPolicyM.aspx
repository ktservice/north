﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PortalSalesPolicyM.aspx.cs" Inherits="KTSSolutionWeb.PortalSalesPolicyM" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="../Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/notice_M.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/tabs.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
        .noticeList {
            padding:10px;
            background-color: #ffffff;
            border-radius: 0px 0px 5px 5px;
            position:absolute;
        }

        .noticeListA {
            padding:10px;
            background-color: #ffffff;
            border-radius: 0px 0px 5px 5px;
            position:relative;
        }

        .noticeAlert {
            background-color: #fee5e2;
            border-color: #fcaca5;
            color: #b50303;
            padding: 10px;
            border-radius: 3px;
            box-shadow: 0px 0px 0px 1px;
            font-size: 13px;
        }

        .contentsarea ul{
             white-space: nowrap;
            overflow-x: auto; 
            overflow-y: hidden;
            flex-wrap: inherit !important;
            scrollbar-width: none; 
        }
 
        .contentsarea ul::-webkit-scrollbar { 
          display: none; }

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    return false;
                }).filter(':eq(0)').click();
            }
        };

        var fn_Init = function () {
            //$("#lblContentTitle").text('판매정책');

            $(".table-option").css('display', 'none');
        }

        var fn_Load = function () {
        }

        var ajaxCallBack = function (json) {
        }

    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div>
            <!-- S:UserControlTitle -->
            <uc:menu ID="menu" runat="server" />
            <!-- E:UserControlTitle -->
            <div class="contentsarea">
                <ul id="ulTabnav" class="tabnav" runat="server">
                </ul>
            </div>
            <div class="gab"></div>
            <div id="divTabcontent" class="tabcontent" runat="server">
            </div>
        </div>	
    </div>
	<!--//E: contentsarea -->
</asp:Content>