﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Order.Master" AutoEventWireup="true" CodeBehind="OrderInfoByOrderNum.aspx.cs" Inherits="KTSSolutionWeb.OrderInfoByOrderNum" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
		function ChkOrderInfo(status) {

			var msg = "";
			var chkreason = false;

            if (status == "Accept") {
				msg = "현재 오더를 수령하시겠습니까?";
			}
			else if (status == "Succ") {
                msg = "현재 오더를 작업완료 하시겠습니까?";
			}
			else if (status == "Impossible") {
				msg = "현재 오더를 작업불가처리 하시겠습니까?";
				chkreason = true;
			}
			else if (status == "Hold") {
                msg = "현재 오더를 작업보류 하시겠습니까?";
                chkreason = true;
			}
			else {
				alert("잘못된 접근입니다.");
				return false;
			}

            if (confirm(msg)) {
                var reason = $("#<%=txbReason.ClientID %>").val();
                if (chkreason) {
                    if (reason.length == 0) {
                        alert("사유를 입력해주세요.");
                        return false;
                    }
                    else {
                        return true;
                    }
				}
                else {
                    if (reason.length == 0) {
                        alert("작업내용을 입력해주세요.")
                        return false;
                    }
                    else {
                        return true;
                    }
				}
            }
            else {
                return false;
            }
		}


        function PageMoveMain(SumupDate) {
            var param = {
                pSUMUPDATE: SumupDate
            };

            var form = createForm("/Mobile/OrderInfoByEmp", param);

            form.submit();
        }


        function DelFileCheck() {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function AddFileUpload() {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;
            
            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:58%;");

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "fileTypeChk('fu" + AddFileUpload.lastAssignedId + "', this.value);");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("accept", "image/*");

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.innerText = "찾아보기";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        }

        function fileTypeChk(id, val) {
            var filechk = false;

            var extIndex = val.lastIndexOf('.');

            var extList = ["tiff", "pjp", "jfif", "bmp", "gif", "svg", "png", "xbm", "dib", "jxl", "jpeg", "svgz", "jpg", "webp", "ico", "tif", "pjpeg", "avif"];

            if (extIndex > 1) {

                var fileExt = val.substring(extIndex + 1, val.length).toLowerCase();

                if (extList.indexOf(fileExt) > 0) {
                    filechk = true;
                }
            }
            if (filechk) {
                document.getElementById(id + "-value").value = val;
            }
            else {
                alert("이미지파일만 업로드 가능합니다.");

                document.getElementById(id).value = "";
                document.getElementById(id + "-value").value = "";
            }
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">

		<div class="page">
			<asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
				<ContentTemplate>	
					<h2>오더상세정보 - <asp:Label ID="lblWorkState" runat="server"></asp:Label></h2>		
					<table>
						<colgroup>
                            <col style="width:20%" />
                            <col style="width:30%" />
                            <col style="width:20%" />
                            <col style="width:30%" />
						</colgroup>
                        <tbody>
                            <tr>
                                <th style="text-align:left;">등록일자</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbSumupDate" runat="server" Width="100px" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
                            </tr>
							<tr>
                                <th style="text-align:left;">등록자소속</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbRegOrgNm" runat="server" ReadOnly="true"></asp:TextBox>
									</p>
								</td>
							</tr>
							<tr>
                                <th style="text-align:left;">등록자사번</th>
                                <td style="padding-right:2px;">
                                    <p class="inpbox">
										<asp:TextBox ID="txbEmpNo" runat="server" ReadOnly="true"></asp:TextBox>
									</p>
								</td>
                                <th style="text-align:left;">등록자명</th>
                                <td style="padding-right:2px;">
                                    <p class="inpbox">
										<asp:TextBox ID="txbEmpNm" runat="server" ReadOnly="true"></asp:TextBox>
									</p>
								</td>
							</tr>
							<tr>
                                <th style="text-align:left;">오더구분</th>
                                <td style="padding-right:2px;">
                                    <p class="inpbox">
										<asp:TextBox ID="txbOrderName" runat="server" ReadOnly="true"></asp:TextBox>
									</p>
								</td>
                                <th style="text-align:left;">업무구분</th>
                                <td style="padding-right:2px;">
                                    <p class="inpbox">
										<asp:TextBox ID="txbWorkType" runat="server" ReadOnly="true"></asp:TextBox>
									</p>
								</td>
							</tr>
							<tr>
                                <th id="thName" runat="server" style="text-align:left;">이름</th>
                                <td style="padding-right:2px;">
                                    <p class="inpbox">
										<asp:TextBox ID="txbFranchiseNm" runat="server" ReadOnly="true"></asp:TextBox>
									</p>
								</td>
                                <th id="thNum" runat="server" style="text-align:left;">번호</th>
                                <td style="padding-right:2px;">
                                    <p class="inpbox">
										<asp:TextBox ID="txbCorpNo" runat="server" ReadOnly="true" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
									</p>
								</td>
							</tr>
							<tr>
                                <th style="text-align:left;">주소</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbAddr" runat="server" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
							</tr>
							<tr>
                                <th id="thPhoneNo" runat="server" style="text-align:left;">연락처</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbPhoneNo" runat="server" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
							</tr>
							<tr>
                                <th style="text-align:left;">방문요청일</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbHopDt" class="date" runat="server" Width="120px" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
							</tr>
							<tr>
                                <th id="thEquipment" runat="server" style="text-align:left;">사용장비</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbEquipment" runat="server" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
							</tr>
							<tr>
                                <th id="thOrderState" runat="server" style="text-align:left;">상태</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbOrderState" runat="server" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
							</tr>
							<tr>
                                <th style="text-align:left;">비고</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbNote" runat="server" MaxLength="8000" style="min-height:100px;height:100px;" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
							</tr>
							<tr id="trReason" runat="server">
                                <th style="text-align:left;">사유</th>
                                <td colspan="3">
                                    <p class="inpbox">
										<asp:TextBox ID="txbReason" runat="server" MaxLength="8000" style="min-height:100px;height:100px;" TextMode="MultiLine" placeholder="작업 완료 시에는 작업내용을 입력해주시고&#13;&#10;작업불가/작업보류 시 사유를 입력해주세요."></asp:TextBox>
                                    </p>
                                </td>
							</tr>
                            <tr id="trImage" runat="server">
                                <th style="text-align:left;"></th>
                                <td colspan="3">                   
                                    <asp:UpdatePanel ID="updPanelImg" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <span id="spanImg" runat="server">
                                            </span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
							<tr id="trFile" runat="server">
                                <th style="text-align:left;"></th>
                                <td colspan="3">
                                    <div class="scrollbox" id="divFileList" runat="server" style="max-height:90px;overflow-y:auto;">
                                        <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptFile" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                                        <p><%# Eval("FILENM") %>&nbsp;<button type="button" id="btnDelete" runat="server" onclick="return DelFileCheck();" onserverclick="btnDelete_ServerClick" style="color:black;">삭제</button></p>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div id="divFile" runat="server">
                                        <div id="filebox">
                                            <p class="filebox">
                                                <input type="text" id="fu1-value" style="float:left;width:58%" />
                                                <span class="file">
                                                    <input type="file" accept="image/*" id="fu1" name="fu1" onchange="fileTypeChk('fu1', this.value);" /> 
                                                    <label for="fu1">찾아보기</label>
                                                </span>
                                                <button type="button" onclick="AddFileUpload();" class="btn-replysave">추가</button>
                                            </p>
                                        </div>
                                    </div>
                                </td>
							</tr>
						</tbody>
					</table>

					<div style="text-align:center;margin-top:40px;">
						<asp:Button ID="btnAccept" runat="server" class="btn-green" style="width:80px" OnClientClick="return ChkOrderInfo('Accept');" OnClick="btnAccept_Click" BorderStyle="None" Visible="false" Enabled="false" Text="오더수령" />
						<asp:Button ID="btnSucc" runat="server" class="btn-green" style="width:80px" OnClientClick="return ChkOrderInfo('Succ');" OnClick="btnSucc_Click" BorderStyle="None" Visible="false" Enabled="false" Text="작업완료" />
						<asp:Button ID="btnImpossible" runat="server" class="btn-black" style="width:80px" OnClientClick="return ChkOrderInfo('Impossible');" OnClick="btnImpossible_Click" BorderStyle="None" Visible="false" Enabled="false" Text="작업불가" />
						<asp:Button ID="btnHold" runat="server" class="btn-black" style="width:80px" OnClientClick="return ChkOrderInfo('Hold');" OnClick="btnHold_Click" BorderStyle="None" Visible="false" Enabled="false" Text="작업보류" />
						<asp:Button ID="btnBack" runat="server" class="btn-black" style="width:80px" OnClientClick="window.location.href='/Mobile/OrderInfoByEmp'" Text="이전화면" />
					</div>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnAccept" EventName="click" />
					<%--<asp:AsyncPostBackTrigger ControlID="btnSucc" EventName="click" />
					<asp:AsyncPostBackTrigger ControlID="btnImpossible" EventName="click" />
					<asp:AsyncPostBackTrigger ControlID="btnHold" EventName="click" />--%>
                    <asp:PostBackTrigger ControlID="btnSucc" />
                    <asp:PostBackTrigger ControlID="btnImpossible" />
                    <asp:PostBackTrigger ControlID="btnHold" />
				</Triggers>
			</asp:UpdatePanel>
		</div>		
	</div>
	<!--//E: contentsarea -->
</asp:Content>