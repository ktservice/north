﻿//------------------------------------------------------------------------------
// <자동 생성됨>
//     이 코드는 도구를 사용하여 생성되었습니다.
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다. 
// </자동 생성됨>
//------------------------------------------------------------------------------

namespace KTSSolutionWeb
{


    public partial class RegInspectionInfoByCompM
    {

        /// <summary>
        /// txbChkDt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbChkDt;

        /// <summary>
        /// txbChkOrgNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbChkOrgNm;

        /// <summary>
        /// txbChkEmpNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbChkEmpNm;

        /// <summary>
        /// txbCompany 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbCompany;

        /// <summary>
        /// txbPartnersEmpNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbPartnersEmpNm;

        /// <summary>
        /// txbNoti 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbNoti;

        /// <summary>
        /// txbLineNo 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbLineNo;

        /// <summary>
        /// txbCustNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbCustNm;

        /// <summary>
        /// txbLv1Grp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbLv1Grp;

        /// <summary>
        /// txbLv2Grp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbLv2Grp;

        /// <summary>
        /// txbLv3Grp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbLv3Grp;

        /// <summary>
        /// txbReqNoti 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbReqNoti;

        /// <summary>
        /// trActImgNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow trActImgNm;

        /// <summary>
        /// btnActAdd 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlButton btnActAdd;

        /// <summary>
        /// btnActImg 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlButton btnActImg;

        /// <summary>
        /// txbActNoti 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbActNoti;

        /// <summary>
        /// btnRegAct 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlButton btnRegAct;

        /// <summary>
        /// hReject 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl hReject;

        /// <summary>
        /// tableReject 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable tableReject;

        /// <summary>
        /// txbReject 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbReject;

        /// <summary>
        /// btnActFin 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton btnActFin;

        /// <summary>
        /// hdfInspection_No 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfInspection_No;

        /// <summary>
        /// hdfChkStatus 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfChkStatus;

        /// <summary>
        /// hdfActStatus 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfActStatus;

        /// <summary>
        /// hdfResult 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfResult;

        /// <summary>
        /// hdfReqImg 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfReqImg;

        /// <summary>
        /// hdfActImg 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfActImg;

        /// <summary>
        /// hdfStdt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfStdt;

        /// <summary>
        /// hdfEndt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfEndt;

        /// <summary>
        /// hdfActCode 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfActCode;

        /// <summary>
        /// hdfLv1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfLv1;
    }
}
