﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PassInfoM.aspx.cs" Inherits="KTSSolutionWeb.PassInfoM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">  
		function DateType(obj) {
			if (obj == "M") {
                $("#<%=rbMonth.ClientID %>").attr("checked", true);
				$("#<%=rbDate.ClientID %>").attr("checked", false);

                $("#<%=txbStMonth.ClientID %>").show();
				$("#<%=txbEnMonth.ClientID %>").show();

                $("#<%=txbStDate.ClientID %>").hide();
				$("#<%=txbEnDate.ClientID %>").hide();
                
                $("#strTitle").show();
                $("#divPassStat").show();
			}
            else {
                $("#<%=rbMonth.ClientID %>").attr("checked", false);
				$("#<%=rbDate.ClientID %>").attr("checked", true);

                $("#<%=txbStMonth.ClientID %>").hide();
				$("#<%=txbEnMonth.ClientID %>").hide();

                $("#<%=txbStDate.ClientID %>").show();
				$("#<%=txbEnDate.ClientID %>").show();

                $("#strTitle").hide();
                $("#divPassStat").hide();
			}
        }
		
        function DeleteChk() {
            if (confirm("PASS를 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function PagePassRegM(procid, custsignyn) {
            var form = document.forms["form1"];

			form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/PassRegM");

            addInput(form, "pPROCID", procid);
            addInput(form, "pCUSTSIGNYN", custsignyn);

            form.submit();
		}

        function ModalEmpInfo(empno) {
			$("#<%=hfEmpNo.ClientID %>").val(empno);
			$("#divModal").show();
			__doPostBack("<%=updPanelModal.ClientID %>", "");            
		}

		function ModalClose() {
			$("#divModal").hide();
			return false;
		}
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div class="page-clothes-request">
		    <h2>PASS 정보 조회</h2>
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>조회 조건</legend>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">성명</label><br />
						<asp:TextBox ID="txbCustNm" runat="server" MaxLength="50" style="width:92% !important"></asp:TextBox>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">TEL</label><br />
						<asp:TextBox ID="txbCustTelNo" runat="server" MaxLength="20" style="width:92% !important" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
					</span>
					<span class="optionbox" style="width:100%; margin:3px 0 0 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">구분</label><br />
						<asp:DropDownList ID="ddlPassType" runat="server" style="width:46% !important">
						</asp:DropDownList>
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">조회일자</label><br />
						<input id="rbMonth" runat="server" style="margin-top:8px;margin-left:0px;width:18px;height:18px; border:1px solid #dfdfdf;float:left;" type="radio" checked="true" onclick="javascript:DateType('M');" />
						<span style="display:inline-block;width:70px;padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:400;float:left;">집계년월</span>
						<input id="rbDate" runat="server" style="margin-top: 8px; margin-left: 0px; width: 18px; height: 18px; border: 1px solid #dfdfdf; float: left;" type="radio" onclick="javascript:DateType('D');" />
						<span style="display:inline-block;width:70px;padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:400;float:left;">당월일자</span>
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<input type="text" id="txbStMonth" runat="server" class="month" readonly="true" style="width:46% !important;" />
						<input type="text" id="txbStDate" runat="server" class="date" readonly="true" style="width:46% !important;display:none;" />
						<label style="width:10px">~</label>
						<input type="text" id="txbEnMonth" runat="server" class="month" readonly="true" style="width:46% !important;" />
						<input type="text" id="txbEnDate" runat="server" class="date" readonly="true" style="width:46% !important;display:none;" />
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
						<asp:Button id="btnSelect" runat="server" OnClick="btnSelect_Click" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;" Text="조회" />
					</span>
				</fieldset>
			</div>
            <!-- S: scrollBox -->
			<strong id="strTitle" style="margin-top:2px;margin-right:10px;float:left;">월별 Pass 통계</strong>
            <div id="divPassStat" class="scrollbox" style="margin-bottom:10px;">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<table>
									<thead>
										<th>No,</th>
										<th>년월</th>
										<th>구분</th>
										<th id="thSvcType2" runat="server">상품</th>
										<th>PASS</th>
										<th>Shooting</th>
										<th>성공</th>
										<th>실패</th>
									</thead>
									<tbody>
                                        <asp:Repeater ID="rptResult1" runat="server">
                                            <ItemTemplate>
												<tr>
													<td id="tdNo" runat="server"><%# Eval("NUM") %></td>
													<td id="tdDt" runat="server"><%# Eval("CREATEDT") %></td>
													<td id="tdPassType" runat="server"><%# Eval("PASSTYPENM") %></td>
													<td id="tdSvcType" runat="server"><%# Eval("SVCTYPE") %></td>
													<td><%# Eval("PASSCNT") %></td>
													<td><%# Eval("SHOOTINGCNT") %></td>
													<td><%# Eval("SUCCCNT") %></td>
													<td><%# Eval("FAILCNT") %></td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
							</Triggers>
						</asp:updatepanel>
					</div>
				</div>
			</div>
			<!--E: scrollBox -->
            <!-- S: scrollBox -->
			<strong style="margin-top:2px;margin-right:10px;float:left">Pass 상세 정보</strong>
            <div class="scrollbox">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel2" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<table>
									<thead>
										<th>No,</th>
										<th>일자</th>
										<th>구분</th>
										<th>성명</th>
										<th>전화번호</th>
										<th>관계</th>
										<th id="thSvcType" runat="server">희망상품</th>
										<th id="thMotType" runat="server">타입</th>
										<th id="thConNo" runat="server">접속번호</th>
										<th>비고</th>
										<th>상태</th>
										<th>성공여부</th>
										<th>수정</th>
									</thead>
									<tbody>
                                        <asp:Repeater ID="rptResult2" runat="server">
                                            <ItemTemplate>
												<tr id="tr" runat="server">
													<td><%# Eval("NUM") %></td>
													<td><%# Eval("PASSDT") %></td>
													<td><%# Eval("PASSTYPENM") %></td>
													<td><%# Eval("CUSTNM") %></td>
													<td><%# Eval("CUSTTELNO") %></td>
													<td><%# Eval("RELATIONTYPE") %></td>
													<td id="tdSvcType" runat="server"><%# Eval("SVCTYPE") %></td>
													<td id="tdMotType" runat="server"><%# Eval("MOTTYPE") %></td>
													<td id="tdConNo" runat="server"><%# Eval("CONNO") %></td>
													<td><%# Eval("NOTI1") %></td>
													<td>
														<asp:Label ID="lblPassStatNm" runat="server" Text='<%# Eval("PASSSTATNM") %>'></asp:Label>
														<asp:Label ID="lblTrtEmpNo" runat="server" Text='<%# Eval("TRTEMPNO") %>' visible="false"></asp:Label>
														<asp:Label ID="lblPassStat" runat="server" Text='<%# Eval("PASSSTAT") %>' visible="false"></asp:Label>
													</td>
													<td><%# Eval("SUCCYN") %></td>
													<td>
														<asp:Label ID="lblProcId" runat="server" Text='<%# Eval("PROCID") %>' visible="false"></asp:Label>
														<asp:Label ID="lblCustSignYn" runat="server" Text='<%# Eval("CUSTSIGNYN") %>' visible="false"></asp:Label>
														<asp:Button id="btnSave" runat="server" visible="false" Enabled="false" class="btn-save" Text="수정"/>
														<asp:Button id="btnDelete" runat="server" visible="false" Enabled="false" class="btn-save" Text="삭제" OnClientClick="return DeleteChk();" OnClick="btnDelete_Click"/>
													</td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
								<asp:AsyncPostBackTrigger ControlID="paging" />
							</Triggers>
						</asp:updatepanel>
					</div>
				</div>
			</div>
			<uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
			<!--E: scrollBox -->
			
            <div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:30%;top:30%;left:0;background:rgb(255, 255, 255); border-radius:10px;">
				<div class="requestlist" style="width:90%;text-align:center;margin-left:5%">
					<strong style="margin-top:2px;margin-right:10px;float:left;">Shooting 담당자 정보</strong>
                    <div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<asp:updatepanel id="updPanelModal" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<asp:HiddenField ID="hfEmpNo" runat="server" OnValueChanged="hfEmpNo_ValueChanged" />
								<table>
									<thead>
										<th>부서</th>
										<th>성명</th>
										<th>직책</th>
										<th>전화번호</th>
									</thead>
									<tbody>
										<asp:Repeater ID="rptResult3" runat="server">
											<ItemTemplate>
												<tr>
													<td><%# Eval("ORGFULLNM") %></td>
													<td><%# Eval("EMPNM") %></td>
													<td><%# Eval("TITLENM") %></td>
													<td><a href='tel:<%# Eval("MOBILE") %>'><%# Eval("MOBILE") %></a></td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="hfEmpNo" EventName="ValueChanged" />
							</Triggers>
						</asp:updatepanel>
					</div>
					<asp:Button id="btnModalClose" runat="server" class="btn-black" BorderStyle="None" style="width:33px;margin:0 !important;" OnClientClick="return ModalClose();" Text="닫기" />
				</div>
			</div>
        </div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>