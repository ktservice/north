﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="UserCarMoveInfo.aspx.cs" Inherits="KTSSolutionWeb.UserCarMoveInfo" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function btnRegCheck() {
            var CarNum = $("#<%= ddlCarNum.ClientID %> option:selected").val();
            var WorkDt = $("#<%= txbWorkDt.ClientID %>").val();
			var Enkm = $("#<%= txbEnkm.ClientID %>").val();
            var Leaveyn = $("#<%= chkReqLeave.ClientID %>").is(":checked");

            if (CarNum.length == 0) {
                alert("차량을 선택하세요");
                return false;
            }
            else if (WorkDt.length == 0) {
                alert("운행일자를 입력하세요");
                return false;
            }
            else if (Enkm.length == 0) {
                alert("종료km를 입력하세요");
                return false;
            }
            else {
                if (Leaveyn) {
                    if (confirm("운행일지 등록 및 직퇴승인 요청하시겠습니까?")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (confirm("운행일지를 등록 하시겠습니까?")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        }

        function btnSaveCheck(obj) {

            var text = obj.value;

            if (text == "저장") {
                if (confirm("저장하시겠습니까?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                if (confirm("수정하시겠습니까?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function btnConfirmCheck() {
            if (confirm("직퇴를 승인하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnReturnCheck() {
            if (confirm("직퇴를 반려하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDeleteCheck() {
            if (confirm("삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function InitValue() {
            $("#<%= txbWorkDt.ClientID %>").val('');
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		
        <div class="page-clothes-request">
			<h2>운행일지관리</h2>
			
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>운행일지등록</legend>
					<span class="optionbox" style="width:50%; margin:3px 0 0 0; font-size:0;">
						<asp:UpdatePanel ID="updPanelCarNum" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label style="margin:0 !important;margin-left:0;">차량번호</label><br />
								<asp:DropDownList ID="ddlCarNum" runat="server" style="width:84% !important" AutoPostBack="true" OnSelectedIndexChanged="ddlCarNum_SelectedIndexChanged"></asp:DropDownList>
								<asp:HiddenField ID="hfStKm" runat="server" />
								<asp:HiddenField ID="hfRegYn" runat="server" />
							</ContentTemplate>
						</asp:UpdatePanel>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">운행일자</label><br />
						<asp:TextBox ID="txbWorkDt" runat="server" class="date" ReadOnly="true" style="width:84% !important"></asp:TextBox>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<asp:UpdatePanel ID="updPanelFuels" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label style="margin:0 !important;margin-left:0;">주유(ℓ)</label><br />
								<asp:TextBox ID="txbFuels" runat="server" style="width:84% !important" MaxLength="11" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
							</ContentTemplate>
						</asp:UpdatePanel>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<asp:UpdatePanel ID="updPanelEnKm" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label style="margin:0 !important;margin-left:0;">종료km</label><br />
								<asp:TextBox ID="txbEnkm" runat="server" style="width:84% !important" MaxLength="11" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
							</ContentTemplate>
						</asp:UpdatePanel>
					</span>
					<span class="ickbox" style="width:100%; margin:3px 0 3px 0;">
						<asp:UpdatePanel ID="updPanelLeave" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label style="display:inline-block;width:50px; padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:800;float:left;">직퇴요청</label><br />
								<input id="chkReqLeave" runat="server" style="margin-top:8px;margin-left:10px;width:18px;height:18px; border:1px solid #dfdfdf;"  type="checkbox" />
							</ContentTemplate>
						</asp:UpdatePanel>
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<asp:UpdatePanel ID="updPanelNoti" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<label style="margin:0 !important;margin-left:0;">공지</label><br />
								<asp:TextBox ID="txbNoti" runat="server" ReadOnly="true" style="width:95% !important"></asp:TextBox>
							</ContentTemplate>
						</asp:UpdatePanel>
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
						<asp:Button id="btnRegCar" runat="server" OnClientClick="return btnRegCheck();" OnClick="btnRegCar_Click" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;" Text="등록" />
					</span>
				</fieldset> 
			</div>
			<div style="border-bottom:2px solid #dfdfdf;">
				<!-- S:fieldset -->
				<fieldset>
					<legend>조회조건</legend>
					<span class="inpbox" id="spanKtsEmpNo" runat="server" visible="false" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">kts사번</label><br />
						<asp:TextBox ID="txbKtsEmpNo" runat="server" MaxLength="20" style="width:84% !important" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanKtsEmpNm" runat="server" visible="false" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">이름</label><br />
						<asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:84% !important"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanCarNum" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">차량번호</label><br />
						<asp:TextBox ID="txbCarNum" runat="server" MaxLength="20" style="width:84% !important"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanDate" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">운행일자</label><br />
						<asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
						<label style="width:10px">~</label>
						<asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
					<asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;" Text="조회" />
					</span>
				</fieldset>
				<!-- //E:fieldset -->
			</div>
            <!-- S: scrollBox -->
            <div class="scrollbox">
				<div class="requestlist">
					<strong style="margin-top:2px;margin-right:10px;float:left">운행목록</strong>
					
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
							<ContentTemplate>	
								<table>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr id="trOrg" runat="server">
													<th style="min-width:15px;padding:10px 5px 10px 0px;">소속</th>
                                                    <td colspan="5" style="min-width:15px;padding:10px 5px 10px 0px;">
														<asp:Label ID="lblOrgFullNm" Width="80%" runat="server" Text='<%# Eval("ORGFULLNM") %>' style="padding:0;text-align:center;"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trEmpInfo" runat="server">
													<th style="min-width:15px;width:10px;padding:10px 5px 10px 0px;">kts사번</th>
                                                    <td colspan="2" style="min-width:15px;width:40%;padding:10px 5px 10px 0px;">
														<asp:Label ID="lblKtsEmpNo" runat="server" Text='<%# Eval("KTSEMPNO") %>' style="padding:0;text-align:center;"></asp:Label>
														<asp:Label ID="lblEmpNo" runat="server" Visible="false" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                                    </td>
													<th style="min-width:15px;width:10px;padding:10px 5px 10px 0px;">이름</th>
                                                    <td colspan="2" style="min-width:15px;width:40%;padding:10px 5px 10px 0px;">
														<asp:Label ID="lblEmpNm" runat="server" Text='<%# Eval("EMPNM") %>' style="padding:0;text-align:center;"></asp:Label>
													</td>
                                                </tr>
												<tr>
													<th style="min-width:15px;width:10px;padding:10px 5px 10px 0px;">차량번호</th>
                                                    <td colspan="2" style="min-width:15px;width:40%;padding:10px 5px 10px 0px;">
														<asp:Label ID="lblCarNum" runat="server" Text='<%# Eval("CARNUM") %>'></asp:Label>
                                                    </td>
													<th style="min-width:15px;width:10px;padding:10px 5px 10px 0px;">운행일자</th>
                                                    <td colspan="2" style="min-width:15px;width:40%;padding:10px 5px 10px 0px;">
															<asp:Label ID="lblWorkDt" runat="server" Text='<%# Eval("WORKDATE") %>' style="padding:0;text-align:center;"></asp:Label>
                                                    </td>
												</tr>
												<tr>
													<th style="min-width:15px; padding:10px 5px 10px 0px;">시작km</th>
                                                    <td style="min-width:15px; padding:10px 5px 10px 0px;">
														<asp:Label ID="lblStKm" runat="server" Text='<%# Eval("STKM") %>' style="padding:0;text-align:center;"></asp:Label>
													</td>
													<th style="min-width:15px; padding:10px 5px 10px 0px;">종료km</th>
                                                    <td style="min-width:15px; padding:10px 5px 10px 0px;">
														<asp:Label ID="lblEnKm" runat="server" Text='<%# Eval("ENKM") %>' style="padding:0;text-align:center;"></asp:Label>
														<asp:TextBox id="txbEnKm2" runat="server" Visible="false" Text='<%# Eval("ENKM") %>' style="width:100px;height:25px;padding:0;text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
													</td>
													<th style="min-width:15px; padding:10px 5px 10px 0px;">운행거리</th>
                                                    <td style="min-width:15px; padding:10px 5px 10px 0px;">
														<asp:Label ID="lblWorkDist" runat="server" Text='<%# Eval("WORKDISTANCE") %>' style="padding:0;text-align:center;"></asp:Label>
													</td>
												</tr>
												<tr>
													<th style="min-width:15px; padding:10px 5px 10px 0px;">직퇴결과</th>
                                                    <td style="min-width:15px; padding:10px 5px 10px 0px;"> 
														<asp:Label ID="lblLeaveKm" runat="server" Text='<%# Eval("LEAVEKM") %>' style="padding:0;text-align:center;"></asp:Label>
														<asp:TextBox id="txbLeaveKm" runat="server" Visible="false" Text='<%# Eval("LEAVEKM") %>' style="width:70px;height:25px;padding:0;text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
													</td>
													<th style="min-width:15px; padding:10px 5px 10px 0px;">직퇴거리</th>
                                                    <td style="min-width:15px; padding:10px 5px 10px 0px;">
														<asp:Label ID="lblLeaveDist" runat="server" Text='<%# Eval("LEAVEDISTANCE") %>' style="padding:0;text-align:center;"></asp:Label>
													</td>
													<th style="min-width:15px; padding:10px 5px 10px 0px;">최종km</th>
                                                    <td style="min-width:15px; padding:10px 5px 10px 0px;">
														<asp:Label ID="lblResultKm" runat="server" Text='<%# Eval("RESULTKM") %>' style="padding:0;text-align:center;"></asp:Label>
													</td>
												</tr>
                                                <tr>
													<th style="min-width:15px;padding:10px 5px 10px 0px;">주유(ℓ)</th>
                                                    <td colspan="5" style="min-width:15px;padding:10px 5px 10px 0px;">
														<asp:Label ID="lblFuels" Width="80%" runat="server" Text='<%# Eval("FUELS") %>' style="padding:0;text-align:center;"></asp:Label>
														<asp:TextBox ID="txbFuels2" runat="server" Visible="false" Text='<%# Eval("FUELS") %>' MaxLength="11" style="width:70px;height:25px;padding:0;text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trConfirm" runat="server">
                                                    <td colspan="6" style="min-width:15px; padding:10px 12px 10px 0px;">
														<asp:Label ID="lblLeaveYn" runat="server" visible="false" Text='<%# Eval("LEAVEYN") %>'></asp:Label>
														<asp:Label ID="lblModifyYn" runat="server" visible="false" Text='<%# Eval("MODIYN") %>'></asp:Label>
														<asp:Button id="btnSave" runat="server" visible="false" Enabled="false" class="btn-save" Text="저장"  OnClientClick="return btnSaveCheck(this);" OnClick="btnSave_ServerClick"/>
														<asp:Button id="btnConfirm" runat="server" visible="false" Enabled="false" class="btn-save" Text="승인" OnClientClick="return btnConfirmCheck();" OnClick="btnConfirm_Click"/>
														<asp:Button id="btnReturn" runat="server" visible="false" Enabled="false" class="btn-del" Text="반려" OnClientClick="return btnReturnCheck();" OnClick="btnConfirm_Click"/>
														<asp:Button id="btnDelete" runat="server" visible="false" Enabled="false" class="btn-del" Text="삭제" OnClientClick="return btnDeleteCheck();" OnClick="btnDelete_Click"/>
                                                    </td>
                                                </tr>
                                                <tr id="trStatus" runat="server" visible="false">
                                                    <td id="td" runat="server" colspan="6" style="min-width:15px; padding:10px 12px 10px 0px;">
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnRegCar" EventName="Click"/>
								<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
								<asp:AsyncPostBackTrigger ControlID="paging" />
							</Triggers>
						</asp:updatepanel>
					</div>
					<uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
				</div>
			</div>
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>