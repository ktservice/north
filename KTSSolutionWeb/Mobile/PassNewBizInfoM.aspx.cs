﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Pass;

namespace KTSSolutionWeb
{
    public partial class PassNewBizInfoM : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    if (GetbPassNewBizAuth())
                    {
                        this.txbStDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                        this.txbEnDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else
                    {     
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('신사업 Pass Shooting 권한이 없습니다.');", true);

                        PageUtility pageUtil = new PageUtility(this.Page);
                        pageUtil.PageMove("/MobileMain");
                    }
                }
            }
        }
        #endregion

        #region btnSelect_Click
        /// <summary>
        /// btnSelect_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region paging_PreRender
        /// <summary>
        /// paging_PreRender
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult1);

                        for (int i = 0; i < rptResult1.Items.Count; i++)
                        {
                            Label lblEmpNm = (Label)rptResult1.Items[i].FindControl("lblEmpNm");
                            Label lblEmpNo = (Label)rptResult1.Items[i].FindControl("lblEmpNo");
                            DropDownList ddlSuccYn = (DropDownList)rptResult1.Items[i].FindControl("ddlSuccYn");
                            Label lblSuccYn = (Label)rptResult1.Items[i].FindControl("lblSuccYn");
                            Button btnSave = (Button)rptResult1.Items[i].FindControl("btnSave");

                            string strEmpNo = lblEmpNo.Text;
                            lblEmpNm.Attributes.Add("onClick", "ModalEmpInfo('" + strEmpNo + "');");

                            string strSuccYn = lblSuccYn.Text;

                            ddlSuccYn.SelectedValue = strSuccYn;

                            AsyncPostBackTrigger asyncBtnSave = new AsyncPostBackTrigger();
                            asyncBtnSave.ControlID = btnSave.UniqueID;
                            asyncBtnSave.EventName = "Click";

                            updPanel1.Triggers.Add(asyncBtnSave);
                        }
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnSave_Click
        /// <summary>
        /// btnSave_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = (Button)sender;

                Label lblProcId = (Label)btnSave.Parent.FindControl("lblProcId");
                DropDownList ddlSuccYn = (DropDownList)btnSave.Parent.FindControl("ddlSuccYn");

                string strProcId = lblProcId.Text;
                string strSuccYn = ddlSuccYn.SelectedValue;

                using (PassMgmt pass = new PassMgmt())
                {
                    pass.SetPassProcessNewBiz(strProcId, strSuccYn);
                }

                GetDataList();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('PASS 상태가 저장되었습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('저장 중 오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region hfEmpNo_ValueChanged
        /// <summary>
        /// hfEmpNo_ValueChanged
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void hfEmpNo_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            try
            {
                string strEmpNo = this.hfEmpNo.Value;

                if (!strEmpNo.Equals(""))
                {
                    using (PassMgmt pass = new PassMgmt())
                    {
                        ds = pass.GetPassEmpInfo(strEmpNo);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];

                        rptResult3.DataSource = dt;
                        rptResult3.DataBind();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다..');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #endregion

        #region Method

        #region GetbPassNewBizOper
        private bool GetbPassNewBizAuth()
        {
            bool bAuth = false;
            DataSet ds = new DataSet();

            try
            {
                if(Session["EMPNO"] != null)
                { 
                    string strEmpNo = Session["EMPNO"].ToString();

                    using (PassMgmt pass = new PassMgmt())
                    {
                        ds = pass.GetPassNewBizAuth(strEmpNo);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            bAuth = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return bAuth;
        }
        #endregion

        #region GetDataList
        /// <summary>
        /// GetDataList
        /// </summary>
        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = Session["EMPNO"].ToString();
                string strStDate = Request.Form[this.txbStDate.UniqueID].ToString();
                string strEnDate = Request.Form[this.txbEnDate.UniqueID].ToString();

                string strStDt = strStDate + " 00:00:00";
                string strEnDt = strEnDate + " 23:59:59";

                paging.bPreRender = true;

                using (PassMgmt pass = new PassMgmt())
                {
                    ds = pass.GetPassProcessNewBiz(strEmpNo, strStDt, strEnDt);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #endregion
    }
}