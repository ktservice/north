﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Order;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;

namespace KTSSolutionWeb
{
    public partial class VanConfirmation : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    initializeControls();
                    GetDataList();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                string strOrderNo = Request.Form["pORDERNO"] == null ? "" : Request.Form["pORDERNO"].ToString();

                if (strOrderNo.Equals(""))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');window.close();", true);
                }
                else
                {
                    this.hdfOrderNo.Value = strOrderNo;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strOrderNo = this.hdfOrderNo.Value;

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "VANORDERINFO";
                string jobGb = "GET";
                
                List<string> aData = new List<string>();
                aData.Add(strOrderNo);
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.lblEmpNm.Text = ds.Tables[0].Rows[0]["FULLWORKEREMPNM"].ToString();
                        string strSuccDt = ds.Tables[0].Rows[0]["SUCCDT"].ToString();

                        if (ds.Tables[0].Rows[0]["SUCCDT"].ToString().Length >= 10)
                        {
                            this.lblInstallDt.Text = ds.Tables[0].Rows[0]["SUCCDT"].ToString().Substring(0, 10);
                        }
                        else
                        {
                            this.lblInstallDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                        }
                        
                        this.lblFranchiseNm.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                        this.lblCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();

                        this.lblFranchiseNm2.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                        this.lblPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();
                    }
                }
                else
                {
                    PageUtility pageUtil = new PageUtility(this.Page);
                    pageUtil.PageMove("/MobileSessionExpire");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetConf(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANCONFIRMATIONINSERT";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
    }
}