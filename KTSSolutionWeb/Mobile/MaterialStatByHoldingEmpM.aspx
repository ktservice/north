﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="MaterialStatByHoldingEmpM.aspx.cs" Inherits="KTSSolutionWeb.MaterialStatByHoldingEmpM" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
	
    <style type="text/css">
        .divChkBackground {
            width: 36px !important;
            height: 20px;
            background: rgb(242 242 242);
            border-width: 2px;
            border-radius: 10px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
        }

        .chkOn {
            width: 18px;
            height: 18px;
            background: rgb(15 255 28);
            border-radius: 9px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
            margin-left: 16px;
        }

        .chkOff {
            width: 18px;
            height: 18px;
            background: rgb(252 31 31);
            border-radius: 9px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
            margin-left: 0px;
        }
    </style>

        <script type="text/javascript">
            var grid;
            var callGb;

            function PageReady() {

                var fn_CellNumber = function (cell, formatterParams, onRendered) {
                    var value = cell.getValue();
                    value = priceToString(value);

                    return value;
                };


                var Columns = [
                    { formatter: "responsiveCollapse", width: 25, minWidth: 25, resizable: false, headerSort: false },
                    { headerHozAlign: "center", title: "UTP 옥외 2P", field: "UTPO2P", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "UTP 옥외 4P", field: "UTPO4P", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "UTP 옥내 2P", field: "UTPI2P", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "UTP 옥내 4P", field: "UTPI4P", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "UTP 옥내 6P", field: "UTPI6P", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "광옥외선", field: "FIBN", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "광케이블 편단", field: "FIBS", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "광케이블 양단", field: "FIBD", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "광 커넥터", field: "FIBCON", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "광 커넥터 접속함", field: "FIBBOX", responsive: 2 },
                    { headerHozAlign: "center", title: "인입클램프", field: "INNCLP", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "HDMI 케이블", field: "HDMCBL", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "컴포넌트 케이블", field: "CMPCBL", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "UTP 패치케이블", field: "UTPPAT", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "몰딩", field: "MOL", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "배선정리함", field: "LINBOX", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "랜카드", field: "LANCRD", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "공유기(유/무선)", field: "LANSHA", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "스위치(허브)", field: "LANSWI", width: 400, responsive: 2 },
                    { headerHozAlign: "center", title: "PoE", field: "LANPOE", width: 400, responsive: 2 }
                ];

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridCollapseMtrue('ktsGrid', 'fitDataFill', true, null); // div의 grid id 값

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

            }
            $(document).ready(PageReady);

            function fn_Search() {
                    //입력 파라미터
                    var aData = [];
                    aData[0] = $("#<%= txbEmpNo.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

                }


        </script>

	<!-- S: contentsarea -->
	<div class="contentsarea">		
        <div class="page-clothes-request">
			<h2>개인보유현황</h2>			
			<div id="divSearchBox" style="border-bottom:2px solid #dfdfdf;">
				<!-- S:fieldset -->
				<fieldset>
					<span class="inpbox" id="spanEmpNo" runat="server" style="display:none;width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">사번</label><br />
						<asp:TextBox ID="txbEmpNo" runat="server" MaxLength="20" style="width:84% !important"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanEmpNm" runat="server" style="display:none;width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">이름</label><br />
						<asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:84% !important"></asp:TextBox>
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnSelect" type="button" class="btn-green" style="width:33px;margin-right:0.8px !important;border-style:none;margin-right:5px;" value="조회" />
					</span>
				</fieldset>
				<!-- //E:fieldset -->
			</div>
            <!-- S: scrollBox -->
            <div id="divSearchList" class="scrollbox">
				<div class="requestlist">
					<strong style="margin-top:2px;margin-right:10px;float:left">개인 보유 현황 조회 결과</strong>
                    <!-- S:datalist -->
                    <div class="datalist" style="margin-top:10px;">
			            <!-- S:scrollbox -->
			            <div class="scrollbox" onload="fn_Search();">
                            <div id="ktsGrid"></div>
                        </div>
			            <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
				</div>
			</div>
		</div>	
	</div >


	<!--//E: contentsarea -->

    <script type="text/javascript">
        $(document).ready(function () {
            fn_Search();
        });
    </script>

</asp:Content>