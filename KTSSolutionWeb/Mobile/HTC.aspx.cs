﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Mobile;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class HTC : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageUtility pageUtil = new PageUtility(this.Page);

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    GetEmpUserInfo();

                    string strEmpNo = Page.Session["EMPNO"].ToString();
                    pageUtil.DiligAlertMobile(strEmpNo, "MobileHTC_Dilig");
                }
            }
        }

        private void GetEmpUserInfo()
        {
            DataSet ds = new DataSet();

            try
            {
                this.txbEmpNo.Text = this.Page.Session["EMPNO"].ToString();
                this.txbEmpNm.Text = this.Page.Session["EMPNM"].ToString();
                this.txbOrgNm.Text = this.Page.Session["ORGFULLNM"].ToString();

                using (HTCProcess htc = new HTCProcess())
                {
                    ds = htc.GetHTCCompList();

                    ddlCompanyUrl.Items.Insert(0, new ListItem("선택하세요.", ""));

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                ListItem item = new ListItem();

                                item.Value = ds.Tables[0].Rows[i]["URL"].ToString();
                                item.Text = ds.Tables[0].Rows[i]["COMPNM"].ToString();

                                ddlCompanyUrl.Items.Add(item);
                            }
                        }
                    }

                    updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSendMsg_Click(object sender, EventArgs e)
        {
            try
            {
                string strEmpNo = this.txbEmpNo.Text;

                string strCustPhoneNum = txbPhone1.Text + txbPhone2.Text + txbPhone3.Text;
                string strMessage = "[kt service 북부]" + ddlCompanyUrl.SelectedItem + ":" + ddlCompanyUrl.SelectedValue;

                strMessage += " " + txbContent.Text;


                if (strEmpNo.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
                else if (ddlCompanyUrl.SelectedValue.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업체를 선택해주세요.');", true);
                }
                else if (strCustPhoneNum.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('전화번호를 입력해주세요.');", true);
                }
                else
                {
                    using (HTCProcess htc = new HTCProcess())
                    {
                        htc.InsHTCMessage(strEmpNo, strCustPhoneNum, strMessage);
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SendOk", "alert('전송완료 했습니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnToday_Click(object sender, EventArgs e)
        {
            try
            {
                PageUtility pageUtil = new PageUtility(this.Page);

                string strEmpNo = Page.Session["EMPNO"].ToString();

                pageUtil.SetDiligCookieM(strEmpNo, "MobileHTC_Dilig");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalClose", "ModalClose();", true);

            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}