﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class RegInspectionInfoM : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    initializeControls();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                string strInspection_No = Request.Form["pINSPECTION_NO"] == null ? "" : Request.Form["pINSPECTION_NO"].ToString();
                string strStDt = Request.Form["pSTDT"] == null ? "" : Request.Form["pSTDT"].ToString();
                string strEnDt = Request.Form["pENDT"] == null ? "" : Request.Form["pENDT"].ToString();
                string strChkCode = Request.Form["pCHKCODE"] == null ? "" : Request.Form["pCHKCODE"].ToString();
                string strLv1 = Request.Form["pLV1CD"] == null ? "" : Request.Form["pLV1CD"].ToString();

                this.hdfInspection_No.Value = strInspection_No;
                this.hdfStdt.Value = strStDt;
                this.hdfEndt.Value = strEnDt;
                this.hdfChkCode.Value = strChkCode;
                this.hdfLv1.Value = strLv1;

                SetDDLCompany();
                SetDDLLv1Grp();
                GetInspectionInfo();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetDDLCompany()
        {
            DataSet ds = new DataSet();

            try
            {
                this.ddlCompany.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "PARTNERSINFO";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add(""); //파라미터 추가 시
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    this.ddlCompany.Items.Add(new ListItem("선택하세요", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        this.ddlCompany.Items.Add(new ListItem(ds.Tables[0].Rows[i]["COMPNM"].ToString(), ds.Tables[0].Rows[i]["COMPCD"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("협력사 정보를 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetDDLLv1Grp()
        {
            DataSet ds = null;

            try
            {
                ddlLv1Grp.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "INSPECTIONLV1GRPLIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    ddlLv1Grp.Items.Add(new ListItem("선택하세요", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["LV1GRPNM"].ToString().Equals(""))
                        {
                            continue;
                        }

                        ddlLv1Grp.Items.Add(new ListItem(ds.Tables[0].Rows[i]["LV1GRPNM"].ToString(), ds.Tables[0].Rows[i]["LV1GRPCD"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("대분류를 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetInspectionInfo()
        {
            DataSet ds = null;

            try
            {
                string strInspection_No = this.hdfInspection_No.Value;

                if (strInspection_No.Equals(""))
                {
                    this.hdfChkOrgCd.Value = Session["ORGCD"].ToString();
                    this.hdfChkEmpNo.Value = Session["EMPNO"].ToString();
                    this.txbChkOrgNm.Text = Session["ORGFULLNM"].ToString();
                    this.txbChkEmpNm.Text = Session["EMPNM"].ToString();

                    this.txbChkDt.Attributes.Add("class", "date");
                    this.txbChkOrgNm.Attributes.Add("onclick", "javascript:fn_ModalOrgCd();");
                    this.txbChkEmpNm.Attributes.Add("onclick", "javascript:fn_ModalKtsUser();");

                    this.btnDelete.Visible = false;
                    this.btnDelete.Attributes.Remove("onclick");

                    this.ddlCompany.Enabled = true;
                    this.txbPartnersEmpNm.Attributes.Add("onclick", "javascript:fn_ModalPartnersUser();");

                    this.btnChkList.Attributes.CssStyle.Add("display", "block");
                    this.btnChkList.Attributes.CssStyle.Add("margin-top", "-12px");
                    this.btnChkList.Value = "점검등록";
                    this.btnChkList.Attributes.Add("onclick", "javascript:fn_GetChkList();");

                    this.btnReject.Visible = false;
                    this.btnChkFin.Visible = false;
                }
                else
                {
                    string resultSession = string.Empty;
                    string rptCd = "INSPECTIONINFOSELECT";
                    string jobGb = "GET";

                    List<string> aData = new List<string>();

                    string strEmpNo = Session["EMPNO"].ToString();

                    aData.Add(strInspection_No);
                    aData.Add(strEmpNo);
                    aData.Add("F"); //파라미터 추가 시

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        ds = dbTransaction.GetDataSet();

                        this.txbChkDt.Text = ds.Tables[0].Rows[0]["CHK_DATE"].ToString();
                        this.txbChkOrgNm.Text = ds.Tables[0].Rows[0]["CHK_ORGNM"].ToString();
                        this.hdfChkOrgCd.Value = ds.Tables[0].Rows[0]["CHK_ORGCD"].ToString();
                        this.txbChkEmpNm.Text = ds.Tables[0].Rows[0]["CHK_USERNM"].ToString();
                        this.hdfChkEmpNo.Value = ds.Tables[0].Rows[0]["CHK_USER"].ToString();

                        string strCompanyCD = ds.Tables[0].Rows[0]["COMPANYCD"].ToString();
                        string strActUser = ds.Tables[0].Rows[0]["ACT_USERNM"].ToString();

                        if (strCompanyCD.IndexOf("_Y") > 0)
                        {
                            strActUser = "협력사 대표자";
                        }

                        this.ddlCompany.SelectedValue = strCompanyCD;
                        this.txbPartnersEmpNm.Text = strActUser;
                        this.hdfPartnersEmpNo.Value = ds.Tables[0].Rows[0]["ACT_USER"].ToString();
                        this.txbNoti.Text = ds.Tables[0].Rows[0]["NOTE"].ToString();

                        this.txbLineNo.Text = ds.Tables[0].Rows[0]["LINENO"].ToString();
                        this.txbCustNm.Text = ds.Tables[0].Rows[0]["CUSTNM"].ToString();

                        this.ddlLv1Grp.SelectedValue = ds.Tables[0].Rows[0]["GRPCD_LV1"].ToString();
                        this.hdfLv1Grp.Value = ds.Tables[0].Rows[0]["GRPCD_LV1"].ToString();
                        this.hdfLv2Grp.Value = ds.Tables[0].Rows[0]["GRPCD_LV2"].ToString();
                        this.hdfLv3Grp.Value = ds.Tables[0].Rows[0]["GRPCD_LV3"].ToString();

                        this.txbReqNoti.Text = ds.Tables[0].Rows[0]["FAULTY_NOTE"].ToString();
                        this.txbActNoti.Text = ds.Tables[0].Rows[0]["ACT_NOTE"].ToString();

                        string strReject = ds.Tables[0].Rows[0]["REJECT_NOTE"].ToString();
                        this.txbReject.Text = strReject;

                        string strChkStatus = ds.Tables[0].Rows[0]["CHK_CD"].ToString();
                        this.hdfChkStatus.Value = strChkStatus;
                        string strActStatus = ds.Tables[0].Rows[0]["ACT_CD"].ToString();
                        this.hdfActStatus.Value = strActStatus;

                        string strCreateUser = ds.Tables[0].Rows[0]["CREATE_USER"].ToString();
                        string strAuthid = Session["AUTHID"].ToString();

                        //점검결과
                        string result = string.Empty;
                        string rptCd2 = "INSPECTIONCHKRESULT";
                        string jobGb2 = "GET";

                        List<string> aData2 = new List<string>();

                        aData2.Add(strInspection_No);
                        aData2.Add("F"); //파라미터 추가 시

                        dbTransaction = new DbTransaction(HttpContext.Current, rptCd2, jobGb2, aData2);

                        result = dbTransaction.GetJsonData();

                        this.hdfResult.Value = result;

                        //중분류
                        string resultLv2 = string.Empty;
                        string rptCdLv2 = "INSPECTIONLV2GRPLIST";
                        string jobGbLv2 = "GET";
                        List<string> aDataLv2 = new List<string>();

                        aDataLv2.Add(ds.Tables[0].Rows[0]["GRPCD_LV1"].ToString());
                        aDataLv2.Add("F"); //파라미터 추가 시

                        dbTransaction = new DbTransaction(HttpContext.Current, rptCdLv2, jobGbLv2, aDataLv2);

                        resultLv2 = dbTransaction.GetJsonData();

                        this.hdfLv2.Value = resultLv2;

                        //소분류
                        string resultLv3 = string.Empty;
                        string rptCdLv3 = "INSPECTIONLV3GRPLIST";
                        string jobGbLv3 = "GET";

                        List<string> aDataLv3 = new List<string>();

                        aDataLv3.Add(ds.Tables[0].Rows[0]["GRPCD_LV2"].ToString());
                        aDataLv3.Add("F"); //파라미터 추가 시

                        dbTransaction = new DbTransaction(HttpContext.Current, rptCdLv3, jobGbLv3, aDataLv3);

                        resultLv3 = dbTransaction.GetJsonData();

                        this.hdfLv3.Value = resultLv3;

                        bool bReqImg = true;

                        //이미지 조회 추가
                        string resultReqImg = string.Empty;

                        if (strChkStatus.Equals("F") && strActStatus.Equals('U'))
                        {
                            bReqImg = false;
                        }

                        if (bReqImg)
                        {
                            string rptCdImg = "INSPECTIONIMGDATASELECT";
                            string jobGbImg = "GET";

                            List<string> aDataImg = new List<string>();

                            aDataImg.Add(strInspection_No);
                            aDataImg.Add("R");
                            aDataImg.Add("F"); //파라미터 추가 시

                            dbTransaction = new DbTransaction(HttpContext.Current, rptCdImg, jobGbImg, aDataImg);

                            resultReqImg = dbTransaction.GetJsonData();
                        }

                        this.hdfReqImg.Value = resultReqImg;

                        bool bActImg = false;

                        if (strActStatus.Equals("F"))
                        {
                            bActImg = true;
                        }

                        string resultActImg = string.Empty;

                        if (bActImg)
                        {
                            //조치사항 이미지뷰 추가
                            string rptCdImg = "INSPECTIONIMGDATASELECT";
                            string jobGbImg = "GET";

                            List<string> aDataImg = new List<string>();

                            aDataImg.Add(strInspection_No);
                            aDataImg.Add("A");
                            aDataImg.Add("F"); //파라미터 추가 시

                            dbTransaction = new DbTransaction(HttpContext.Current, rptCdImg, jobGbImg, aDataImg);

                            resultActImg = dbTransaction.GetJsonData();
                        }

                        this.hdfActImg.Value = resultActImg;

                        bool bReq = false;
                        bool bRegInfo = false;
                        bool bRegImg = false;

                        if (strEmpNo.Equals(strCreateUser) || strAuthid.Equals("AUTH0001"))
                        {
                            bReq = true;

                            this.btnDelete.Visible = true;
                            this.btnDelete.Attributes.Add("onclick", "javascript:fn_DelInfoChk('" + strInspection_No + "');");
                        }
                        else
                        {
                            this.btnDelete.Visible = false;
                            this.btnDelete.Attributes.Remove("onclick");
                        }

                        if (bReq && strChkStatus.Equals("T"))
                        {
                            //점검상태 등록중
                            this.txbChkDt.Attributes.Add("class", "date");
                            this.txbChkOrgNm.Attributes.Add("onclick", "javascript:fn_ModalOrgCd();");
                            this.txbChkEmpNm.Attributes.Add("onclick", "javascript:fn_ModalKtsUser();");

                            this.ddlCompany.Enabled = true;
                            this.txbPartnersEmpNm.Attributes.Add("onclick", "javascript:fn_ModalPartnersUser();");

                            this.btnChkList.Attributes.CssStyle.Add("display", "block");
                            this.btnChkList.Value = "점검수정";
                            this.btnChkList.Attributes.Add("onclick", "javascript:fn_GetChkList();");
                            this.trReqImgNm.Visible = true;

                            this.btnReqImg.Attributes.Add("onclick", "javascript:fn_RegFileCheck('R');");
                            this.btnReqImg.Visible = true;
                            this.btnReqAdd.Attributes.Add("onclick", "javascript:AddFileUpload();");
                            this.btnReqAdd.Visible = true;

                            this.txbReqNoti.ReadOnly = false;

                            this.btnReqAct.Visible = true;
                            this.btnReqAct.Attributes.Add("onclick", "javascript:fn_SetStatus('R');");

                            this.hReject.Visible = false;
                            this.tableReject.Visible = false;
                            this.txbReject.ReadOnly = false;

                            this.btnReject.Visible = false;
                            this.btnChkFin.Visible = true;
                            this.btnChkFin.Attributes.Add("onclick", "javascript:fn_SetStatus('F');");
                            //등록중일 경우 정보/요청사항 수정가능
                            bRegInfo = true;
                            bRegImg = true;
                        }
                        else if (bReq && (strChkStatus.Equals("R") || strChkStatus.Equals("B")))
                        {
                            //점검상태 조치요청중 or 반려
                            this.txbChkDt.Attributes.Remove("class");
                            this.txbChkOrgNm.Attributes.Remove("onclick");
                            this.txbChkEmpNm.Attributes.Remove("onclick");

                            this.ddlCompany.Enabled = false;
                            this.txbPartnersEmpNm.Attributes.Remove("onclick");

                            this.txbNoti.ReadOnly = true;
                            this.txbCustNm.ReadOnly = true;

                            this.ddlLv1Grp.Enabled = false;

                            this.btnChkList.Attributes.CssStyle.Add("display", "none");
                            this.btnChkList.Attributes.Remove("onclick");

                            //조치사항 이미지뷰 추가

                            if (strActStatus.Equals("F"))
                            {
                                //조치상태 조치완료
                                this.trReqImgNm.Visible = true;

                                this.btnReqImg.Attributes.Add("onclick", "javascript:fn_RegFileCheck('R');");
                                this.btnReqImg.Visible = true;
                                this.btnReqAdd.Attributes.Add("onclick", "javascript:AddFileUpload();");
                                this.btnReqAdd.Visible = true;

                                this.txbReqNoti.ReadOnly = false;

                                this.btnReqAct.Visible = false;

                                this.hReject.Visible = true;
                                this.tableReject.Visible = true;
                                this.txbReject.ReadOnly = false;

                                this.btnReject.Visible = true;
                                this.btnReject.Attributes.Add("onclick", "javascript:fn_SetStatus('B');");

                                this.btnChkFin.Visible = true;
                                this.btnChkFin.Attributes.Add("onclick", "javascript:fn_SetStatus('F');");
                                //조치상태가 완료일 경우 요청사항 수정가능(반려가 가능한 경우)
                                bRegImg = true;
                            }
                            else
                            {
                                //조치상태 요청중
                                this.trReqImgNm.Visible = false;
                                this.txbReqNoti.ReadOnly = true;

                                this.btnReqImg.Visible = false;
                                this.btnReqAdd.Visible = false;
                                this.btnReqAct.Visible = false;
                                this.btnReqAct.Visible = false;

                                if (!strReject.Equals(""))
                                {
                                    this.hReject.Visible = true;
                                    this.tableReject.Visible = true;
                                }
                                else
                                {
                                    this.hReject.Visible = false;
                                    this.tableReject.Visible = false;
                                }

                                this.txbReject.ReadOnly = true;

                                this.btnReject.Visible = false;
                                this.btnChkFin.Visible = false;
                            }
                        }
                        else if (strChkStatus.Equals("F"))
                        {
                            //감리완료
                            this.txbChkDt.Attributes.Remove("class");
                            this.txbChkOrgNm.Attributes.Remove("onclick");
                            this.txbChkEmpNm.Attributes.Remove("onclick");

                            this.ddlCompany.Enabled = false;
                            this.txbPartnersEmpNm.Attributes.Remove("onclick");

                            this.txbNoti.ReadOnly = true;
                            this.txbCustNm.ReadOnly = true;

                            this.ddlLv1Grp.Enabled = false;

                            this.btnChkList.Attributes.CssStyle.Add("display", "none");
                            this.btnChkList.Attributes.Remove("onclick");

                            this.trReqImgNm.Visible = false;
                            this.txbReqNoti.ReadOnly = true;
                            this.btnReqImg.Visible = false;
                            this.btnReqAdd.Visible = false;
                            this.btnReqAct.Visible = false;
                            this.btnReqAct.Visible = false;

                            this.hReject.Visible = false;
                            this.tableReject.Visible = false;
                            this.txbReject.ReadOnly = true;

                            this.btnReject.Visible = false;
                            this.btnChkFin.Visible = false;
                        }
                        else
                        {
                            //기타 정상적인 상태일경우 해당사항없음
                            this.txbChkDt.Attributes.Remove("class");
                            this.txbChkOrgNm.Attributes.Remove("onclick");
                            this.txbChkEmpNm.Attributes.Remove("onclick");

                            this.ddlCompany.Enabled = false;
                            this.txbPartnersEmpNm.Attributes.Remove("onclick");

                            this.txbNoti.ReadOnly = true;
                            this.txbCustNm.ReadOnly = true;

                            this.ddlLv1Grp.Enabled = false;

                            this.btnChkList.Attributes.CssStyle.Add("display", "none");
                            this.btnChkList.Attributes.Remove("onclick");

                            this.trReqImgNm.Visible = false;
                            this.txbReqNoti.ReadOnly = true;
                            this.btnReqImg.Visible = false;
                            this.btnReqAdd.Visible = false;
                            this.btnReqAct.Visible = false;
                            this.btnReqAct.Visible = false;

                            this.hReject.Visible = false;
                            this.tableReject.Visible = false;
                            this.txbReject.ReadOnly = true;

                            this.btnReject.Visible = false;
                            this.btnChkFin.Visible = false;
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "initControl", "fn_SetControls(" + bRegInfo.ToString().ToLower() + ", " + bRegImg.ToString().ToLower() + ");", true); ;
                    }

                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLv2GrpList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONLV2GRPLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLv3GrpList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONLV3GRPLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetChkListData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONCHKLIST";
            string jobGb = "GET";

            //aData[0] = inspection_No;
            aData.Add("");
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetChkResultData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONCHKRESULT";
            string jobGb = "GET";

            //aData[0] = inspection_No;
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetOrgData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARORGCDINFO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetKtsUser(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "USERINFOBYORGCD";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPartnersUser(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PARTNERSUSERINFOBYCOMPCD";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsInspectionInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONINFOINSERT";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string UpdInspectionInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONINFOUPDATE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelInspectionInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONINFODELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetImgData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONIMGDATASELECT";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelImgData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONIMGDATADELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetInspectionStatus(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONSTATUSUPDATE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}