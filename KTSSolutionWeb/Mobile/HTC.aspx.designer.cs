﻿//------------------------------------------------------------------------------
// <자동 생성됨>
//     이 코드는 도구를 사용하여 생성되었습니다.
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다. 
// </자동 생성됨>
//------------------------------------------------------------------------------

namespace KTSSolutionWeb
{


    public partial class HTC
    {

        /// <summary>
        /// updPanel1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel updPanel1;

        /// <summary>
        /// txbEmpNo 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbEmpNo;

        /// <summary>
        /// txbEmpNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbEmpNm;

        /// <summary>
        /// txbOrgNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbOrgNm;

        /// <summary>
        /// ddlCompanyUrl 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlCompanyUrl;

        /// <summary>
        /// txbContent 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbContent;

        /// <summary>
        /// txbPhone1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbPhone1;

        /// <summary>
        /// txbPhone2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbPhone2;

        /// <summary>
        /// txbPhone3 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txbPhone3;

        /// <summary>
        /// btnSendMsg 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSendMsg;

        /// <summary>
        /// UdpToday 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UdpToday;

        /// <summary>
        /// btnToday 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnToday;
    }
}
