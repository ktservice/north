﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="InspectionListM.aspx.cs" Inherits="KTSSolutionWeb.InspectionListM" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />	

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnReg").click(function (e) {
                    fn_PopupRegInspection('');
                });
            }
        };

        var fn_GridInit = function () {

            var fn_buttons = function (cell) {
                var rowData = cell.getRow().getData();

                var inspection_No = rowData.INSPECTION_NO;

                var input = "<input id='btnCellSave' type='button' onclick='fn_PopupRegInspection(\"" + inspection_No + "\");' value='상세' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";

                return input;
            };

            var Columns = [
                { formatter: "responsiveCollapse", width: 25, minWidth: 25, hozAlign: "center", resizable: false, headerSort: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", resizable: false, width: 60, responsive: 0, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조치ID", field: "INSPECTION_ID", sorter: "string", resizable: false, width: 100, minWidth: 100, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "검사일", field: "CHK_DATE", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENM", sorter: "string", resizable: false, width: 80, minWidth: 80, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "상세", field: "DETAIL", resizable: false, width: 80, minWidth: 80, formatter: fn_buttons, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GRPNM_LV1", resizable: false, width: 80, minWidth: 80, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "점검상태", field: "CHK_NM", resizable: false, sorter: "string", width: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "조치상태", field: "ACT_NM", resizable: false, sorter: "string", width: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "CREATE_DATE", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM_LV1", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM_LV2", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM_LV3", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "OFFICENM", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "GRPNM_LV1", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류", field: "GRPNM_LV2", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "소분류", field: "GRPNM_LV3", sorter: "string", responsive: 2},
                { headerHozAlign: "center", hozAlign: "center", title: "회선번호", field: "LINENO", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "경과일", field: "ELAPSED_DATE", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "조치일", field: "ACT_DATE", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "주요내용", field: "NOTE", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "불량내용", field: "FAULTY_NOTE", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "조치요구번호", field: "INSPECTION_NO", sorter: "string", responsive: 2, visible: false  }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridCollapseM('ktsGrid', 'fitDataFill', true, null); // div의 grid id 값
        };

        var fn_Load = function () {
            fn_InitControl();
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {

                ktsGrid.setData(JSON.parse(json)["Table"]);
                ktsGridRowCount(ktsGrid.getDataCount());
                ktsGridSearch();
            }
        };

        var fn_InitControl = function () {
            var chkCode = $("#<%= hdfChkCode.ClientID %>").val();

            if (chkCode != "" && chkCode != undefined) {
                $("#ddlChkCode").val(chkCode);
                fn_Search();
            }
        };

        var fn_Search = function () {

            var sdt = new Date($("#<%= txbStDt.ClientID %>").val());
            var edt = new Date($("#<%= txbEnDt.ClientID %>").val());

            var dateDiff = Math.ceil((edt.getTime() - sdt.getTime()) / (1000 * 3600 * 24));

            if (dateDiff > 32) {
                alert("31일 이상 조회 할 수 없습니다.");
            }
            else {

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= txbStDt.ClientID %>").val();
                aData[1] = $("#<%= txbEnDt.ClientID %>").val();
                aData[2] = $("#ddlChkCode option:selected").val();
                aData[3] = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
        };

        var fn_PopupRegInspection = function (inspection_no) {
            var form;

            var stdt = $("#<%= txbStDt.ClientID %>").val();
            var endt = $("#<%= txbEnDt.ClientID %>").val();
            var chkCode = $("#ddlChkCode option:selected").val();
            var lv1GrpCd = $("#<%= ddlLv1Grp.ClientID %> option:selected").val();

            var param = {
                pSTDT: stdt,
                pENDT: endt,
                pCHKCODE: chkCode,
                pLV1CD: lv1GrpCd,
                pINSPECTION_NO: inspection_no
            };

            form = createForm("/Mobile/RegInspectionInfoM", param);

            form.submit();
        };
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">		
        <div class="page-clothes-request">
			<h2>조치요구서 현황</h2>
			<div id="divSearchBox" style="border-bottom:2px solid #dfdfdf;">
				<!-- S:fieldset -->
				<fieldset>
					<legend>조회조건</legend>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">검사일자</label><br />
						<asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
						<label style="width:10px">~</label>
						<asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
					</span>
					<span class="optionbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">점검상태</label><br />
                        <select id="ddlChkCode" style="width:50% !important">
                            <option value="" selected="selected">전체</option>
                            <option value="T">등록중</option>
                            <option value="R">조치요청중</option>
                            <option value="F">조치완료</option>
                        </select>
                        <asp:HiddenField ID="hdfChkCode" runat="server" />
					</span>
					<span class="optionbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">대분류</label><br />
                        <asp:DropDownList ID="ddlLv1Grp" style="width:43%;" runat="server">
                        </asp:DropDownList>
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnSelect" type="button" class="btn-green" style="width:33px;margin-right:4px !important;border-style:none;" value="조회" />
                        <input id="btnReg" type="button" class="btn-green" style="width:33px;border-style:none;" value="등록" />
					</span>
				</fieldset>
				<!-- //E:fieldset -->
			</div>
            <!-- S: scrollBox -->
            <div id="divSearchList" class="scrollbox">
				<div class="requestlist">
					<strong style="margin-top:2px;margin-right:10px;float:left">조회결과</strong>
                    <!-- S:datalist -->
                    <div class="datalist" style="margin-top:10px;">
			            <!-- S:scrollbox -->
			            <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
			            <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
				</div>
			</div>
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>