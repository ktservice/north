﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Order;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;

namespace KTSSolutionWeb
{
    public partial class VanOrderInfoByOrderNo : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    initializeControls();
                    GetDataList(false);
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList(true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnImgSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                InsOrderImageFiles();

                GetDataList(true);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "alert('파일 추가를 완료 했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnDownload = (Button)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                string strOrderNo = this.hdfOrderNo.Value;

                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    mgmt.DelOrderImageFile(strOrderNo, lblSeq.Text);
                }

                GetDataList(true);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "alert('파일 삭제를 완료 했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void initializeControls()
        {
            try
            {
                string strOrderNo = Request.Form["pORDERNO"] == null ? "" : Request.Form["pORDERNO"].ToString();

                if (strOrderNo.Equals(""))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');window.close();", true);
                }
                else
                {
                    this.hdfOrderNo.Value = strOrderNo;

                    ddlRsvTime.Items.Clear();

                    for (int i = 0; i < 24; i++)
                    {
                        string strTime = i.ToString().PadLeft(2, '0');

                        ddlRsvTime.Items.Add(new ListItem(strTime + "시", strTime));
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList(bool bLoad)
        {
            DataSet ds = new DataSet();
            DataSet dsFile = new DataSet();

            try
            {
                string strOrderNo = this.hdfOrderNo.Value;

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "VANORDERINFO";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add(strOrderNo);
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        bool bSetting = false;
                        string strWorkState = ds.Tables[0].Rows[0]["WORKSTATE"].ToString();

                        this.hdfStatus.Value = strWorkState;

                        string strReceiptType = ds.Tables[0].Rows[0]["RECEIPT_TYPE"].ToString();
                        string strRcvTypeCd = ds.Tables[0].Rows[0]["RCVTYPECD"].ToString();
                        this.lblWorkState.Text = " : " + ds.Tables[0].Rows[0]["WORKSTATENM"].ToString();

                        this.lblReceiptType.Text = strReceiptType;
                        this.lblRegOrgNm.Text = ds.Tables[0].Rows[0]["REGORGNM"].ToString();
                        this.lblRegEmpNo.Text = ds.Tables[0].Rows[0]["REGEMPNO"].ToString();
                        this.lblRegEmpNm.Text = ds.Tables[0].Rows[0]["REGEMPNM"].ToString();
                        this.lblRcvType.Text = ds.Tables[0].Rows[0]["RCVTYPENM"].ToString();
                        this.lblAgencyNm.Text = ds.Tables[0].Rows[0]["AGENCYNM"].ToString();
                        this.lblPartnersYn.Text = ds.Tables[0].Rows[0]["PARTNERSYN"].ToString();
                        this.lblFranchiseNm.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                        this.lblCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();
                        this.lblAddr.Text = ds.Tables[0].Rows[0]["ADDR"].ToString();
                        this.lblDtlAddr.Text = ds.Tables[0].Rows[0]["DTLADDR"].ToString();
                        this.lblTelNo.Text = ds.Tables[0].Rows[0]["TELNO"].ToString();
                        this.lblPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();
                        this.lblRcvDt.Text = ds.Tables[0].Rows[0]["RCVDT"].ToString();
                        this.hdfRsvDt.Value = ds.Tables[0].Rows[0]["RSVDT"].ToString() + " " + ds.Tables[0].Rows[0]["RSVTIME"].ToString() + ":00:00";
                        this.txbRsvDt.Text = ds.Tables[0].Rows[0]["RSVDT"].ToString();
                        this.ddlRsvTime.SelectedValue = ds.Tables[0].Rows[0]["RSVTIME"].ToString();

                        string strHopeDt = ds.Tables[0].Rows[0]["HOPEDT"].ToString();
                        string strHopeTime = "";
                        string strDayWeek = ds.Tables[0].Rows[0]["DAYWEEK_TYPE"].ToString();

                        try
                        {
                            strHopeTime = " " + ds.Tables[0].Rows[0]["HOPETIME"].ToString() + ":00:00";
                        }
                        catch
                        {
                            strHopeTime = "";
                        }

                        if (strDayWeek.Length > 0)
                        {
                            strDayWeek = " " + strDayWeek;
                        }

                        this.lblHopeDt.Text = strHopeDt + strHopeTime + strDayWeek;

                        this.txbRcvNote.Text = ds.Tables[0].Rows[0]["RCVNOTE"].ToString();
                        this.txbReqNote.Text = ds.Tables[0].Rows[0]["REQNOTE"].ToString();
                        this.lblRtnAddr.Text = ds.Tables[0].Rows[0]["RTNADDR"].ToString();
                        this.lblRtnDtlAddr.Text = ds.Tables[0].Rows[0]["RTNDTLADDR"].ToString();
                        this.txbReport.Text = ds.Tables[0].Rows[0]["REPORT"].ToString();
                        this.txbProcinfo.Text = ds.Tables[0].Rows[0]["PROCINFO"].ToString();
                        this.hdfAcceptYn.Value = ds.Tables[0].Rows[0]["ACCEPTYN"].ToString();

                        string strStartYn = ds.Tables[0].Rows[0]["STARTYN"].ToString();
                        this.hdfStartYn.Value = strStartYn;

                        string strConYn = ds.Tables[0].Rows[0]["CONFYN"].ToString();

                        if (strReceiptType.Equals("VAN365"))
                        {
                            string strDayType = ds.Tables[0].Rows[0]["HOLIDAYFLAG"].ToString().Equals("Y") ? "주말" : "평일";
                            this.hdfHoliDayFlag.Value = strDayType;
                            this.lblRsvDt.Text = ds.Tables[0].Rows[0]["RSVDT"].ToString() + " " + ds.Tables[0].Rows[0]["RSVTIME"].ToString() + ":00:00 " + strDayType;
                            string strRcvType = this.lblRcvType.Text.Replace("신규", "일반");
                            this.lblRcvType.Text = strRcvType;
                            string strDevice = ds.Tables[0].Rows[0]["DEVICE"].ToString();

                            this.trDevice.Visible = true;
                            this.lblDevice.Text = strDevice;

                            this.trPolicyBasic.Visible = true;
                            this.lblPolicyBasic.Text = ds.Tables[0].Rows[0]["POLICY_BASIC"].ToString();

                            this.trPolicyExtra.Visible = true;
                            this.trPolicyExtraList.Visible = true;

                            DataTable dtExtra = new DataTable();
                            string strChkExtra = "";

                            try
                            {
                                ApiTransaction apiTran = new ApiTransaction();
                                NameValueCollection data = new NameValueCollection();

                                string strMethod = "getVan365PolicyExtra";

                                DataTable resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_extra", data);

                                resultDt.Columns.Add("CNT");
                                dtExtra = resultDt.Clone();

                                string[] strDbExtra = ds.Tables[0].Rows[0]["POLICY_EXTRA"].ToString().Split('|');
                                string[] strExtraContent = new string[strDbExtra.Length];
                                string[] strExtraCnt = new string[strDbExtra.Length];

                                string strSearch = "";

                                if (strDbExtra.Length > 0)
                                {
                                    int i = 0;

                                    foreach (string str in strDbExtra)
                                    {
                                        if (!str.Equals(""))
                                        {
                                            string[] strExtra = str.Split('^');

                                            if (strExtra.Length == 2)
                                            {
                                                string strContent = strExtra[0].Trim();
                                                string strCnt = strExtra[1].Trim();

                                                if (strContent.Length == 0 || !int.TryParse(strCnt, out int n))
                                                {
                                                    continue;
                                                }
                                                else
                                                {
                                                    if (!strSearch.Equals(""))
                                                    {
                                                        strSearch += ",";
                                                    }

                                                    strExtraContent[i] = strContent;
                                                    strExtraCnt[i] = strCnt;

                                                    strSearch = strSearch + "'" + strContent + "'";
                                                }
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }

                                        i++;
                                    }
                                }

                                if (!strSearch.Equals(""))
                                {
                                    string dtFilter = string.Format("RECEIPT_DIVISION = '{0}' AND DEVICE LIKE '*{1}*' AND CONTENT IN ({2}) ", strRcvType, strDevice, strSearch);

                                    string dtSort = "POLICY_EXTRA_PK ASC";

                                    if (resultDt.Rows.Count > 0)
                                    {
                                        DataRow[] dr = resultDt.Select(dtFilter, dtSort);

                                        foreach (DataRow Ndr in dr)
                                        {
                                            int n = 0;

                                            foreach (string content in strExtraContent)
                                            {
                                                if (Ndr["CONTENT"].ToString().Equals(content))
                                                {
                                                    Ndr["CNT"] = strExtraCnt[n];

                                                    if (strChkExtra.Length > 0)
                                                    {
                                                        strChkExtra += "|";
                                                    }

                                                    strChkExtra = strChkExtra + Ndr["POLICY_EXTRA_PK"] + "," + strExtraCnt[n];
                                                    break;
                                                }

                                                n++;
                                            }

                                            dtExtra.ImportRow(Ndr);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            finally
                            {
                                result = Utility.DataTableToJson(dtExtra);
                            }

                            this.hdfPolicyExtra.Value = result;
                            this.hdfChkPolicyExtra.Value = strChkExtra;
                            this.hdfChkPolicyExtra2.Value = strChkExtra;

                            this.trTerminalYN.Visible = true;
                            this.lblTerminalYN.Text = ds.Tables[0].Rows[0]["TERMINALYN"].ToString();

                            this.trDispatch.Visible = true;
                            this.hdfDispatch.Value = ds.Tables[0].Rows[0]["DISPATCH_TYPE"].ToString().Replace("출동", "") == "일반" ? "Dispatch_Normal" : "Dispatch_Emergency";

                            this.trDlvrDt.Visible = false;
                            this.lblDlvrDt.Text = "";

                            this.trTermialAddr.Visible = true;
                            string strTerminalJJ = ds.Tables[0].Rows[0]["TERMINALADDR_JJ"].ToString();
                            string strTerminalAddr = "";

                            if (strTerminalJJ.Equals("가맹점"))
                            {
                                strTerminalAddr = "(" + ds.Tables[0].Rows[0]["TERMINALADDR_JJ"].ToString() + ")" + ds.Tables[0].Rows[0]["TERMINALADDR"].ToString();
                            }
                            else
                            {
                                strTerminalAddr = ds.Tables[0].Rows[0]["TERMINALADDR_JJ"].ToString();
                            }

                            //this.hdfTerminalAddr.Value = ds.Tables[0].Rows[0]["TERMINALADDR_JJ"].ToString() == "가맹점" ? "Addr" : "AddrKTS";
                            //this.txbTerminalAddr.Text = ds.Tables[0].Rows[0]["TERMINALADDR"].ToString();

                            this.lblTerminalAddr.Text = strTerminalAddr;

                            this.trSwContent.Visible = true;
                            this.lblSwContent.Text = ds.Tables[0].Rows[0]["SW_CONTENT"].ToString();

                            this.trSwSetting.Visible = true;

                            if (ds.Tables[0].Rows[0]["SW_SETTING"].ToString().Equals("세팅필요"))
                            {
                                this.hdfSwSetting.Value = "SettingC";
                                bSetting = true;
                            }
                            else
                            {
                                this.hdfSwSetting.Value = "SettingN";
                                bSetting = false;
                            }

                            this.trSwSettingHost.Visible = true;
                            this.ddlSWSettingHost.SelectedValue = ds.Tables[0].Rows[0]["SW_SETTINGHOST"].ToString();

                            this.trVan.Visible = true;
                            this.lblVan.Text = ds.Tables[0].Rows[0]["VAN"].ToString();

                            this.trCat.Visible = true;
                            this.trCatList.Visible = true;
                            this.hdfCatList.Value = ds.Tables[0].Rows[0]["PM_NM"].ToString();

                            this.trAspName.Visible = true;
                            this.lblAspName.Text = ds.Tables[0].Rows[0]["ASP_NAME"].ToString();
                            this.trAspID.Visible = true;
                            this.lblAspID.Text = ds.Tables[0].Rows[0]["ASP_ID"].ToString();
                            this.trAspPW.Visible = true;
                            this.lblAspPW.Text = ds.Tables[0].Rows[0]["ASP_PW"].ToString();

                            this.trSolutionName.Visible = true;
                            this.lblSolutionName.Text = ds.Tables[0].Rows[0]["SOLUTION_NAME"].ToString();
                            this.trSolutionID.Visible = true;
                            this.lblSolutionID.Text = ds.Tables[0].Rows[0]["SOLUTION_ID"].ToString();
                            this.trSolutionPW.Visible = true;
                            this.lblSolutionPW.Text = ds.Tables[0].Rows[0]["SOLUTION_PW"].ToString();
                        }
                        else
                        {
                            this.lblRsvDt.Text = ds.Tables[0].Rows[0]["RSVDT"].ToString() + " " + ds.Tables[0].Rows[0]["RSVTIME"].ToString() + ":00:00";
                            this.trDevice.Visible = false;
                            this.trPolicyBasic.Visible = false;
                            this.trPolicyExtra.Visible = false;
                            this.trPolicyExtraList.Visible = false;
                            this.trTerminalYN.Visible = false;
                            this.trDispatch.Visible = false;

                            this.trDlvrDt.Visible = true;
                            this.lblDlvrDt.Text = ds.Tables[0].Rows[0]["DLVRDT"].ToString();

                            this.trTermialAddr.Visible = false;
                            this.trSwContent.Visible = false;
                            this.trSwSetting.Visible = false;
                            this.trSwSettingHost.Visible = false;
                            this.trVan.Visible = false;
                            this.trCat.Visible = false;
                            this.trCatList.Visible = false;
                            this.trAspName.Visible = false;
                            this.trAspID.Visible = false;
                            this.trAspPW.Visible = false;
                            this.trSolutionName.Visible = false;
                            this.trSolutionID.Visible = false;
                            this.trSolutionPW.Visible = false;
                        }

                        this.trProcinfo.Visible = false;

                        if (!strWorkState.Equals("2") && !strWorkState.Equals("7"))
                        {
                            using (OrderMgmt mgmt = new OrderMgmt())
                            {
                                dsFile = mgmt.GetOrderFile(strOrderNo);
                            }

                            if (dsFile.Tables.Count > 0)
                            {
                                if (dsFile.Tables[0].Rows.Count > 0)
                                {

                                    rptFile.DataSource = dsFile.Tables[0];
                                    rptFile.DataBind();

                                    for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                    {
                                        if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                        {
                                            HtmlImage ImgBoard = new HtmlImage();

                                            string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                            byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                            ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                            ImgBoard.Attributes.Add("style", "max-width:100%;height:auto;");

                                            spanImg.Controls.Add(ImgBoard);
                                        }
                                    }
                                }
                            }
                        }

                        if (strReceiptType.Equals("VAN365"))
                        {
                            if (strWorkState.Equals("3.5"))
                            {
                                this.trProcinfo.Visible = true;

                                this.ddlSWSettingHost.Enabled = bSetting;
                                this.trRegVan365.Visible = true;
                                this.trRegVan365.Attributes.Add("onclick", "fn_RegVan365();");
                            }
                            else
                            {
                                if (strWorkState.Equals("3") || strWorkState.Equals("5"))
                                {
                                    this.trProcinfo.Visible = true;
                                }

                                this.divDispatchNormal.Attributes.Remove("onclick");
                                this.divDispatchEmergency.Attributes.Remove("onclick");

                                this.divSettingC.Attributes.Remove("onclick");
                                this.divSettingN.Attributes.Remove("onclick");

                                this.ddlSWSettingHost.Enabled = false;

                                this.trRegVan365.Visible = false;
                                this.btnTran.Attributes.Remove("onclick");
                            }
                        }
                        else
                        {
                            this.trRegVan365.Visible = false;
                            this.btnTran.Attributes.Remove("onclick");

                            if (strWorkState.Equals("3.5") || strWorkState.Equals("4") || strWorkState.Equals("5"))
                            {
                                this.trProcinfo.Visible = true;
                            }
                            else
                            {
                                if (strWorkState.Equals("3"))
                                {
                                    this.trProcinfo.Visible = true;
                                }
                            }
                        }

                        if (strWorkState.Equals("2") || strWorkState.Equals("7"))
                        {
                            this.txbProcinfo.ReadOnly = true;

                            this.trImg.Visible = false;
                            this.trFile.Visible = false;

                            this.spanMemo.Visible = false;

                            this.btnCancel.Visible = false;
                            this.btnCancel.Attributes.Remove("onclick");
                            this.btnSave.Visible = false;
                            this.btnSave.Attributes.Remove("onclick");

                            if (strWorkState.Equals("2"))
                            {
                                this.btnAcpt.Visible = true;
                                this.btnAcpt.Attributes.Add("onclick", "fn_OrderProc('A');");
                                this.btnRjct.Visible = true;
                                this.btnRjct.Attributes.Add("onclick", "fn_OpenModal('R');");
                                this.btnTran.Visible = true;
                                this.btnTran.Attributes.Add("onclick", "fn_OpenModal('T');");
                            }
                            else
                            {
                                this.btnAcpt.Visible = false;
                                this.btnAcpt.Attributes.Remove("onclick");
                                this.btnRjct.Visible = false;
                                this.btnRjct.Attributes.Remove("onclick");
                                this.btnTran.Visible = false;
                                this.btnTran.Attributes.Remove("onclick");
                            }

                            this.btnSucc.Visible = false;
                            this.btnSucc.Attributes.Remove("onclick");
                            this.btnHold.Visible = false;
                            this.btnHold.Attributes.Remove("onclick");

                        }
                        else if (strWorkState.Equals("4") || strWorkState.Equals("6"))
                        {
                            this.trProcinfo.Visible = true;
                            this.txbProcinfo.ReadOnly = true;

                            this.trImg.Visible = true;
                            this.trFile.Visible = true;

                            this.spanMemo.Visible = true;

                            this.btnCancel.Visible = false;
                            this.btnCancel.Attributes.Remove("onclick");
                            this.btnSave.Visible = false;
                            this.btnSave.Attributes.Remove("onclick");

                            this.btnAcpt.Visible = false;
                            this.btnAcpt.Attributes.Remove("onclick");
                            this.btnRjct.Visible = false;
                            this.btnRjct.Attributes.Remove("onclick");
                            this.btnSucc.Visible = false;
                            this.btnSucc.Attributes.Remove("onclick");
                            this.btnHold.Visible = false;
                            this.btnHold.Attributes.Remove("onclick");
                            this.btnTran.Visible = false;
                            this.btnTran.Attributes.Remove("onclick");
                        }
                        else
                        {
                            this.txbProcinfo.ReadOnly = false;

                            this.trImg.Visible = true;
                            this.trFile.Visible = true;

                            this.spanMemo.Visible = true;

                            this.btnAcpt.Visible = false;
                            this.btnAcpt.Attributes.Remove("onclick");
                            this.btnRjct.Visible = false;
                            this.btnRjct.Attributes.Remove("onclick");

                            this.btnSucc.Visible = true;

                            //버튼 조건 임시 주석처리 
                            if (strWorkState.Equals("3"))// || (strWorkState.Equals("5") && !strStartYn.Equals("Y")))
                            {
                                this.btnCancel.Visible = true;
                                this.btnCancel.Attributes.Add("onclick", "fn_OrderProc('C');");

                                this.btnSucc.Value = "작업시작";
                                this.btnSucc.Attributes.Add("onclick", "fn_OrderProc('W');");
                            }
                            else
                            {
                                this.btnCancel.Visible = false;
                                this.btnCancel.Attributes.Remove("onclick");

                                this.btnSucc.Value = "작업완료";
                                this.btnSucc.Attributes.Add("onclick", "fn_OrderProc('S');");
                            }

                            this.btnHold.Visible = true;
                            this.btnHold.Attributes.Add("onclick", "fn_OpenModal('H');");

                            this.btnTran.Visible = true;
                            this.btnTran.Attributes.Add("onclick", "fn_OpenModal('T');");

                        }

                        if (strWorkState.Equals("3"))
                        {
                            this.lblRsvDt.Visible = false;
                            this.txbRsvDt.Visible = true;
                            this.ddlRsvTime.Visible = true;

                            this.btnSave.Visible = true;
                            this.btnSave.Attributes.Add("onclick", "fn_OpenModal('D');");
                        }
                        else
                        {
                            this.lblRsvDt.Visible = true;
                            this.txbRsvDt.Visible = false;
                            this.ddlRsvTime.Visible = false;

                            this.btnSave.Visible = false;
                            this.btnSave.Attributes.Remove("onclick");
                        }

                        if ((strWorkState.Equals("3.5") || strWorkState.Equals("4")) && strRcvTypeCd.Equals("B"))
                        {
                            this.trConfirm.Visible = true;

                            if (strConYn.Equals("Y"))
                            {
                                this.btnConfirm.Visible = false;
                                this.btnConfirm.Attributes.Remove("onclick");
                                this.lblConfYn.Visible = true;
                            }
                            else
                            {
                                this.btnConfirm.Visible = true;
                                this.btnConfirm.Attributes.Add("onclick", "fn_ConfirmOpen();");
                                this.lblConfYn.Visible = false;
                            }
                        }
                        else
                        {
                            this.trConfirm.Visible = false;
                            this.btnConfirm.Attributes.Remove("onclick");
                        }
                    }

                    updPanel1.Update();
                    updPanelMemo.Update();

                    if (bLoad)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetControl", "fn_SetControl();", true);
                    }
                }
                else
                {
                    PageUtility pageUtil = new PageUtility(this.Page);
                    pageUtil.PageMove("/MobileSessionExpire");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void InsOrderImageFiles()
        {
            Stream stream = null;

            try
            {
                HttpFileCollection uploadFiles = Request.Files;

                string strOrderNo = this.hdfOrderNo.Value;

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];

                    stream = postedFIle.InputStream;

                    if (stream.Length == 0)
                        continue;

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    //type체크 필요 
                    if (strFileType.Contains("image"))
                    {
                        int nFileLen = Convert.ToInt32(stream.Length);

                        byte[] byteFile = new byte[nFileLen];

                        stream.Read(byteFile, 0, nFileLen);

                        using (OrderMgmt mgmt = new OrderMgmt())
                        {
                            mgmt.InsOrderImageFile(strOrderNo, strFileNm, byteFile, strFileType, nFileLen);
                        }
                    }

                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetVanOrderCancel(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFOCANCEL";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strReason = "(취소)" + aData[1] + " (작업자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add("북부 : " + strReason);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptCancel";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("CONTENT", strReason);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 취소처리 연동 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetVanOrderWorkState(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERWORKSTATEUPDATE";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strProcCd = aData[1];
            string strReason = "";
            string strAcceptYn = aData[3];
            string strStartYn = aData[4];
            bool bApiSend = true;

            aList.Add(strOrderNo);
            aList.Add(strProcCd);

            if (strProcCd.Equals("R"))
            {
                strReason = "(작업거부) " + aData[2] + " (작업자 : " + empNm + ")";
            }
            else if (strProcCd.Equals("H"))
            {
                strReason = "(보류) " + aData[2] + " (작업자 : " + empNm + ")";
            }
            else if (strProcCd.Equals("S"))
            {
                strReason = aData[2];
            }
            else if (strProcCd.Equals("W"))
            {
                strReason = "작업이 시작되었습니다. (작업자 : " + empNm + ")";

                if (strStartYn.Equals("Y"))
                {
                    bApiSend = false;
                }
            }

            if (strProcCd.Equals("A"))
            {
                aList.Add("");

                if (strAcceptYn.Equals("Y"))
                {
                    bApiSend = false;
                }
            }
            else if (strProcCd.Equals("S"))
            {
                aList.Add(strReason);
            }
            else
            {
                aList.Add("북부 : " + strReason);
            }

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M") && bApiSend)
                {
                    DataTable dt = new DataTable();
                    ApiTransaction apiTran = new ApiTransaction();
                    NameValueCollection data = new NameValueCollection();

                    string strProcNm = "";
                    string strMethod = "";

                    if (strProcCd.Equals("A"))
                    {
                        strProcNm = "수령";

                        strMethod = "setReceiptStateD";

                        data.Add("RECEIPT_PK", strOrderNo);
                    }
                    else if (strProcCd.Equals("R") || strProcCd.Equals("H"))
                    {
                        if (strProcCd.Equals("R"))
                        {
                            strProcNm = "거부";
                        }
                        else { 
                            strProcNm = "작업보류";
                        }

                        //    메모
                        strMethod = "setReceiptMemo";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("MEMO", strReason);
                    }
                    else if (strProcCd.Equals("W")) 
                    { 
                        strProcNm = "작업시작";

                        strMethod = "setReceiptStateS";

                        data.Add("RECEIPT_PK", strOrderNo);

                    }
                    else if (strProcCd.Equals("S"))
                    {
                        strProcNm = "완료";
                        //    완료처리(상태변경/메모 동시처리됨)

                        strMethod = "setReceiptStateC";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("CONTENT", aData[2]);
                    }

                    try
                    {
                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 " + strProcNm + "처리 연동 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetTransferWorker(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANTRANSFERWORKER";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strReason = "(작업이관) " + aData[2] + " (작업자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add(aData[1]);
            aList.Add("북부 : " + strReason);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptMemo";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("MEMO", strReason);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 메모 등록 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetDate(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFODTUPDATE";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strMemo = aData[2] + " (작업자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add(aData[1]);
            aList.Add("북부 : " + strMemo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                //if (errcd.Equals("00") && !aData[1].Equals(aData[4]) && !strOrderNo.Substring(0, 1).Equals("M"))
                //{
                //    DataTable dt = new DataTable();

                //    try
                //    {
                //        ApiTransaction apiTran = new ApiTransaction();

                //        NameValueCollection data = new NameValueCollection();

                //        string strMethod = "setRequestDateUp";

                //        data.Add("RECEIPT_PK", strOrderNo);
                //        data.Add("REQUEST_DATE", aData[1]);

                //        dt = apiTran.RestApiCallToInsert(strMethod, data);
                //        result = Utility.DataTableToJson(dt);
                //    }
                //    catch
                //    {
                //        dt = Utility.DataTableError("01", "VAN 오더 방문예정일 변경 연동 중 오류가 발생했습니다.", "");
                //        result = Utility.DataTableToJson(dt);
                //    }
                //}

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptMemo";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("MEMO", strMemo);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);

                        jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                        errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                        if (errcd.Equals("00") && !aData[1].Equals(aData[3]))
                        {
                            try
                            {
                                apiTran = new ApiTransaction();

                                data = new NameValueCollection();

                                strMethod = "setRequestDateUp";

                                data.Add("RECEIPT_PK", strOrderNo);
                                data.Add("REQUEST_DATE", aData[1]);

                                dt = apiTran.RestApiCallToInsert(strMethod, data);
                                result = Utility.DataTableToJson(dt);
                            }
                            catch
                            {
                                dt = Utility.DataTableError("03", "VAN 오더 방문예정일 변경 연동 중 오류가 발생했습니다.", "");
                                result = Utility.DataTableToJson(dt);
                            }
                        }
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 메모 연동 등록 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetMemoData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERMEMOLIST";
            string jobGb = "GET";

            string strOrderNo = aData[0];
            string strChk = strOrderNo.Substring(0, 1);

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                if (strChk.Equals("M"))
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        string strMethod = "getReceiptMemo";

                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        data.Add("RECEIPT_PK", aData[0]);

                        dt = apiTran.RestApiCallToDataTable(strMethod, "receipt_memo", data);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 호출 중 오류가 발생했습니다.", "");
                    }

                    result = Utility.DataTableToJson(dt);
                }
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsMemo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERMEMOINSERT";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strChk = strOrderNo.Substring(0, 1);
            string strMemo = aData[1] + " (작성자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add("북부 : " + strMemo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                if (strChk.Equals("M"))
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptMemo";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("MEMO", strMemo);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 메모 연동 등록 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetEmpData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "USERINFOBYWORKERNM";
            string jobGb = "GET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPolicyExtra(List<string> aData)
        {
            string result = string.Empty;

            DataTable dt = new DataTable();

            try
            {
                string strRcvType = aData[0].Replace("신규", "일반");
                string strDevice = aData[1];
                string strSearch = aData[2];

                ApiTransaction apiTran = new ApiTransaction();
                NameValueCollection data = new NameValueCollection();

                string strMethod = "getVan365PolicyExtra";

                DataTable resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_extra", data);

                if (!resultDt.Columns.Contains("CNT"))
                {
                    resultDt.Columns.Add("CNT");
                }

                dt = resultDt.Clone();

                string dtFilter = string.Format("RECEIPT_DIVISION = '{0}' AND DEVICE LIKE '*{1}*' AND CONTENT LIKE '*{2}*' ", strRcvType, strDevice, strSearch);

                //임시 키 추후 수정
                string dtSort = "POLICY_EXTRA_PK ASC";

                if (resultDt.Rows.Count > 0)
                {
                    //dt = resultDt.Select(dtFilter, dtSort).CopyToDataTable();
                    DataRow[] dr = resultDt.Select(dtFilter, dtSort);

                    foreach (DataRow Ndr in dr)
                    {
                        //Ndr["CNT"] = "1";
                        dt.ImportRow(Ndr);
                    }
                }
            }
            catch
            {
                dt = Utility.DataTableError("01", "추가견적 조회 중 오류가 발생했습니다.", "");
            }
            finally
            {
                result = Utility.DataTableToJson(dt);
            }


            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetChkPolicyExtra(List<string> aData)
        {
            string result = string.Empty;

            DataTable dt = new DataTable();

            try
            {
                DataTable resultDt = new DataTable();
                string[] strJsonChkExtra = aData[0].Split('|');
                string[] strExtraKey = new string[strJsonChkExtra.Length];
                string[] strExtraCnt = new string[strJsonChkExtra.Length];

                ApiTransaction apiTran = new ApiTransaction();
                NameValueCollection data = new NameValueCollection();

                string strMethod = "getVan365PolicyExtra";

                resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_extra", data);

                if (!resultDt.Columns.Contains("CNT"))
                {
                    resultDt.Columns.Add("CNT");
                }

                dt = resultDt.Clone();

                string strWhere = "";
                int i = 0;

                foreach (string strKey in strJsonChkExtra)
                {
                    string[] strKeyVal = strKey.Split(',');

                    if (strWhere.Length > 0)
                    {
                        strWhere += ",";
                    }

                    strExtraKey[i] = strKeyVal[0];
                    strExtraCnt[i] = strKeyVal[1];

                    strWhere += "'" + strKeyVal[0] + "'";

                    i++;
                }

                string dtFilter = string.Format("POLICY_EXTRA_PK IN ({0}) ", strWhere);

                //임시 키 추후 수정
                string dtSort = "POLICY_EXTRA_PK ASC";

                i = 0;

                if (resultDt.Rows.Count > 0)
                {
                    //dt = resultDt.Select(dtFilter, dtSort).CopyToDataTable();
                    DataRow[] dr = resultDt.Select(dtFilter, dtSort);

                    foreach (DataRow Ndr in dr)
                    {
                        //Ndr["CNT"] = "1";
                        if (Ndr["POLICY_EXTRA_PK"].ToString().Equals(strExtraKey[i]))
                        {
                            Ndr["CNT"] = strExtraCnt[i];
                            i++;
                        }
                        dt.ImportRow(Ndr);

                    }
                }
            }
            catch
            {
                dt = Utility.DataTableError("01", "추가견적 조회 중 오류가 발생했습니다.", "");
            }
            finally
            {
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetHoliDayFlag(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "HOLIDAYFLAG";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                ds = dbTransaction.GetDataSet();

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        result = ds.Tables[0].Rows[0]["HOLIDAYFLAG"].ToString();
                    }
                }
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetVan365Info(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VAN365INFOUPDATE";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strMemo = "VAN365오더정보를 수정했습니다. (작업자 : " + empNm + ")";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            string strRsvDt = aData[1];//.Split(' ')[0];
            string strHolidayFlag = aData[2];
            string strPolicyExtra = aData[3];
            string strDispatch = aData[5];
            string strSwSetting = aData[6].Equals("세팅완료") ? "미진행" : "진행";
            string strSwSettingHost = aData[7];

            aData.RemoveAt(3);
            aData.RemoveAt(2);

            aData.Add("북부 : " + strMemo);
            aData.Add(empNm);

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptVan365Update";

                        JObject jData = new JObject();

                        jData.Add("RECEIPT_PK", strOrderNo);
                        jData.Add("DISPATCH_TYPE", strDispatch.Replace("출동", ""));
                        jData.Add("DISPATCH_HOPE_DATE", strRsvDt);
                        jData.Add("DAYWEEK_TYPE", strHolidayFlag);
                        jData.Add("SW_SETTING_YN", strSwSetting);
                        jData.Add("SW_SETTING_CONTENT", strSwSettingHost);
                        jData.Add("POLICY_EXTRA", strPolicyExtra);

                        //dt = apiTran.RestApiCallToInsert(strMethod, strJson);
                        dt = apiTran.RestApiCallToInsert(strMethod, jData);

                        //dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);

                        jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                        errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                        if (errcd.Equals("00"))
                        {
                            try
                            {
                                apiTran = new ApiTransaction();

                                data = new NameValueCollection();

                                strMethod = "setReceiptMemo";

                                data.Add("RECEIPT_PK", strOrderNo);
                                data.Add("MEMO", strMemo);

                                dt = apiTran.RestApiCallToInsert(strMethod, data);
                                result = Utility.DataTableToJson(dt);

                                jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                                errcd = jsonObj["SERVICE_ERROR_CD"].ToString();
                            }
                            catch
                            {
                                dt = Utility.DataTableError("03", "VAN 오더 메모 연동 등록 중 오류가 발생했습니다.", "");
                                result = Utility.DataTableToJson(dt);
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 정보 수정 연동 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
    }
}