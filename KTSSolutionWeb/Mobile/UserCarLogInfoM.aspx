﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="UserCarLogInfoM.aspx.cs" Inherits="KTSSolutionWeb.UserCarLogInfoM" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
	
    <style type="text/css">
        .divChkBackground {
            width: 36px !important;
            height: 20px;
            background: rgb(242 242 242);
            border-width: 2px;
            border-radius: 10px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
        }

        .chkOn {
            width: 18px;
            height: 18px;
            background: rgb(15 255 28);
            border-radius: 9px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
            margin-left: 16px;
        }

        .chkOff {
            width: 18px;
            height: 18px;
            background: rgb(252 31 31);
            border-radius: 9px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
            margin-left: 0px;
        }
    </style>

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            $("#<%= btnDelLog.ClientID %>").hide();

            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                value = priceToString(value);

                return value;
            };

            var fn_buttons = function (cell) {
                var rowData = cell.getRow().getData();

                var rowNo = rowData.id;
                var rowCarNo = rowData.CARNO;
                var rowLogdate = rowData.LOGDATE;
                var rowLastLogdate = rowData.LASTLOGDATE;
                var rowAuth = rowData.AUTHID;
                var rowEmpno = rowData.EMPNO;
                var rowSEmpno = rowData.SEMPNO;
                var rowLeave = rowData.LEAVEYN;
                var rowModify = rowData.MODIYN;
                var rowDel = rowData.DELYN;

                var input = "";

                var bSave = false;
                var bConfirm = false;
                var bDelete = false;

                if (rowLeave == "Y") {
                    bSave = true;
                }
                else if (rowLeave == "R") {
                    if (rowEmpno == rowSEmpno) {
                        bSave = true;
                    }

                    if (rowAuth == "AUTH0001" || rowAuth == "AUTH0002" || rowAuth == "AUTH0003" || rowAuth == "AUTH0004") {
                        bConfirm = true;
                    }
                }
                else if (rowLeave == "C") {
                    if (rowEmpno == rowSEmpno && rowLogdate == rowLastLogdate) {
                        bSave = true;
                    }
                }
                else if (rowModify == "Y") {
                    bSave = true;
                }
                if (rowDel == "Y") {
                    bDelete = true;
                }

                if (bSave) {
                    input += "<input id='btnCellSave' type='button' onclick='fn_ShowSaveDiv(\"" + rowNo + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                }

                if (bConfirm) {
                    if (input.length > 0)
                        input += "&nbsp;";

                    input += "<input id='btnCellConfirm' type='button' onclick='fn_ConfirmCheck(\"" + rowNo + "\", \"" + rowCarNo + "\", \"" + rowLogdate + "\");' value='승인' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                    input += "&nbsp;<input id='btnCellReturn' type='button' onclick='fn_ReturnCheck(\"" + rowNo + "\", \"" + rowCarNo + "\", \"" + rowLogdate + "\");' value='반려' class='btn-del' style='height:23px;line-height:0px;font-size:15px'>";
                }

                if (bDelete) {
                    if (input.length > 0)
                        input += "&nbsp;";

                    input += "<input id='btnCellDel' type='button' onclick='fn_DelCheck(\"" + rowNo + "\", \"" + rowCarNo + "\", \"" + rowLogdate + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";
                }

                return input;
            };

            var Columns = [
                { formatter: "responsiveCollapse", width: 25, minWidth: 25, hozAlign: "center", resizable: false, headerSort: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", resizable: false, width: 60, responsive: 0, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "운행일자", field: "LOGDATE", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "저장", field: "SAVE", resizable: false, width: 160, minWidth: 160, formatter: fn_buttons, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", sorter: "string", width: 400, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "시작km", field: "STKM", sorter: "string", width: 100, formatter: fn_CellNumber, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "종료km", field: "ENKM", sorter: "string", width: 100, formatter: fn_CellNumber, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "운행거리", field: "WORKDISTANCE", sorter: "string", width: 100, formatter: fn_CellNumber, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴결과", field: "LEAVEKM", sorter: "string", width: 100, formatter: fn_CellNumber, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴거리", field: "LEAVEDISTANCE", sorter: "string", formatter: fn_CellNumber, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "최종결과", field: "RESULTKM", sorter: "string", width: 100, formatter: fn_CellNumber, responsive: 2},
                { headerHozAlign: "center", hozAlign: "center", title: "주유(ℓ)", field: "FUELAMOUNT", sorter: "string", width: 100, formatter: fn_CellNumber, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴사유", field: "REASON", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "ETC", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "DELYN", sorter: "string", width: 100, visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "LEAVEYN", sorter: "string", width: 100, visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "MODIYN", sorter: "string", width: 100, visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "AUTHID", sorter: "string", width: 100, visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "SEMPNO", sorter: "string", width: 100, visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "LASTLOGDATE", sorter: "string", width: 100, visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴요청상태", field: "LEAVESTAT", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "주유카드", field: "FUELCARD", sorter: "string", width: 240, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "하이패스카드", field: "HIPASSCARD", sorter: "string", width: 240, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "전기충전카드", field: "CHARGECARD", sorter: "string", width: 240, responsive: 2 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridCollapseM('ktsGrid', 'fitDataFill', true, null); // div의 grid id 값

            $("#<%= ddlCarNo.ClientID %>").change(function (e) {
                fn_CarNoChange();
            });

            $("#<%= ddlProcType.ClientID %>").change(function (e) {
                $("#txbLogDt").val("");

                var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();
                   
                if (proctype == "SAVE") {
                    $("#spanFuels").show();
                    $("#spanStkm").show();
                    $("#spanEnkm").show();
                    $("#spanReqLeave").show();
                    $("#spanCarChk").show();
                    
                    $("#btnInsLog").show();
                    $("#<%= btnDelLog.ClientID %>").hide();
                }
                else {
                    $("#spanFuels").hide();
                    $("#spanStkm").hide();
                    $("#spanEnkm").hide();
                    $("#spanReqLeave").hide();
                    $("#spanCarChk").hide();

                    $("#btnInsLog").hide();
                    $("#<%= btnDelLog.ClientID %>").show();
                }
            });

            $("#txbLogDt").change(function (e) {
                fn_LogDtChange();
            });

            $("#<%= chkReqLeave.ClientID %>").change(function (e) {
                $("#<%= ddlReason.ClientID %> option:selected").val('');

                if ($("#<%= chkReqLeave.ClientID %>").is(":checked")) {
                    $("#spanReason").show();
                }
                else {
                    $("#spanReason").hide();
                }
            });

            $("#btnInsLog").click(function (e) {
                fn_RegLogCheck();
            });
            
            $("#btnRegChk1").click(function (e) {
                fn_RegCarChk('');
            });

            $("#btnRegChk2").click(function (e) {
                fn_RegCarChk('TOWERWAGON');
            });


            $("#<%= btnDelLog.ClientID %>").click(function (e) {
                fn_DelLogCheck();
            });

            $("#btnDivInsClose").click(function (e) {
                $("#divInsLog").hide();
                $("#divRegLog").hide();
                $("#divSearchBox").show();
                $("#divSearchList").show();
                $("#divCarChk1").hide();
                $("#divCarChk2").hide();

                fn_ResetCarInfo();
            });

            $("#btnRegLog").click(function (e) {
                fn_SaveCheck();
            });
            
            $("#btnDivRegClose").click(function (e) {
                $("#divInsLog").hide();
                $("#divRegLog").hide();
                $("#divSearchBox").show();
                $("#divSearchList").show();
                $("#divCarChk1").hide();
                $("#divCarChk2").hide();

                fn_ResetCarInfo();
            });

            $("#btnChk1Close").click(function (e) {
                $("#divInsLog").show();
                $("#divRegLog").hide();
                $("#divSearchBox").hide();
                $("#divSearchList").hide();
                $("#divCarChk1").hide();
                $("#divCarChk2").hide();
            });

            $("#btnChk2Close").click(function (e) {
                $("#divInsLog").show();
                $("#divRegLog").hide();
                $("#divSearchBox").hide();
                $("#divSearchList").hide();
                $("#divCarChk1").hide();
                $("#divCarChk2").hide();
            });            
        });

        var ajaxCallEx = function (param, url, gridAlert) {
            $.ajax({
                type: 'post',
                url: url,
                data: param,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    var json = data.d;

                    ajaxCallSuccess(json, gridAlert);
                },
                beforeSend: function () {
                    $("#updateProgress").css('display', 'block');
                },
                complete: function () {
                    $("#updateProgress").css('display', 'none');
                },
                error: function (request, status, error) {
                    ajaxFail(request, status, error, gridAlert);
                }
            })
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetCarInfo') {
                    fn_SetCarInfo(json);
                }
                else if (callGb == 'GetCarChkInfo') {
                    fn_GetCarChkInfo(json);
                }
                else {
                    $.each(JSON.parse(json), function (idx, item) {
                        if (item.ERRCD == '00') {
                            if (callGb == 'InsOk') {
                                alert('운행일지가 등록 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'ConfirmOk') {
                                ktsGridAlert('직퇴가 승인 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'ConfirmNo') {
                                ktsGridAlert('직퇴가 반려 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'LeaveOk') {
                                alert('직퇴 정보가 등록되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'RegOk') {
                                alert('운행일지가 수정되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'DelOk') {
                                ktsGridAlert('운행일지가 삭제되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'InsChkOk') {
                                alert('검차내용이 저장되었습니다.', 2000);
                            }
                            else if (callGb == 'ConfAllOk') {
                                ktsGridAlert('직퇴 일괄승인이 완료되었습니다.', 2000);
                                fn_Search();
                            }
                        }
                    });
                }
            }

            callGb = '';
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                //ktsGrid.setData(json);
                //ktsGridRowCount(ktsGrid.getDataCount());
                //ktsGridSearch();

                ktsGrid.setData(JSON.parse(json)["Table"]);
                ktsGridRowCount(ktsGrid.getDataCount());
                ktsGridSearch();

                $("#<%= hdfLeaveCnt.ClientID %>").val(JSON.parse(json)["Table1"][0].CNT);
                __doPostBack("<%= hdfLeaveCnt.ClientID %>", "");
            }
        }

        function fn_CarNoChange() {
            var carNo = $("#<%= ddlCarNo.ClientID %> option:selected").val();
            $("#<%= ddlProcType.ClientID %>").val("SAVE");
            $("#<%= ddlProcType.ClientID %>").change();

            if (carNo == '') {
                fn_ResetCarInfo();
            }
            else {
                var aData = [];
                aData[0] = carNo;

                callGb = 'GetCarInfo';
                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarInfo');

                ajaxCallEx(jsonData, ajaxUrl, false);
            }
        }

        function fn_LogDtChange() {
            var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();
            
            if (proctype == "SAVE") {

                callGb = 'GetCarChkInfo';

                //검차여부 조회
                var aData = [];
                aData[0] = $("#<%= ddlCarNo.ClientID %> option:selected").val();
                aData[1] = $("#txbLogDt").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarChkInfo');

                ajaxCallEx(jsonData, ajaxUrl, false);
            }
        }

        function fn_SetCarInfo(json) {
            if (JSON.parse(json).length > 0) {
                $.each(JSON.parse(json)[0], function (key, value) {
                    switch (key) {
                       // case 'LOGDATE':
                          //  $("#txbLogDt").val(value);
                          //  break
                        case 'STKM':
                            if (value == 0) {
                                $("#txbStkm").removeAttr('readonly');
                            }
                            else {
                                $("#txbStkm").attr('readonly', 'true');
                            }
                            $("#txbStkm").val(value);
                            break
                        case 'NOTICE':
                            $("#txbNoti").val(value);
                            break
                        case 'CARTYPE':
                            $("#hfCarType").val(value);
                            break
                        case 'CARCHK':
                            $("#lblCarChk").text(value);

                            if (value == "검차완료") {
                                $("#lblCarChk").css("color", 'rgb(38 220 28)');
                            }
                            else {
                                $("#lblCarChk").css("color", 'rgb(224 31 31)');
                            }
                            break
                        case 'LEAVEYN':
                            $("#hfLeaveYn").val(value);
                            break
                        case 'LASTLOGDATE':
                            $("#hfLastLogDate").val(value);
                            break
                        default:
                            break
                    }
                });
                var todayDate = new Date().toISOString().slice(0, 10);
                $("#txbLogDt").val(todayDate);
            }
        }

        function fn_GetCarChkInfo(json) {
            var carcheck = "";

            if (JSON.parse(json).length > 0) {
                carcheck = JSON.parse(json)[0]["CARCHK"];
                $("#lblCarChk").text(carcheck);

                if (carcheck == "검차완료") {
                    $("#lblCarChk").css("color", 'rgb(38 220 28)');
                }
                else {
                    $("#lblCarChk").css("color", 'rgb(224 31 31)');
                }
            }
        }

        function fn_ResetCarInfo() {
            $("#<%= ddlCarNo.ClientID %>").val("");
            $("#<%= ddlProcType.ClientID %>").val("SAVE");

            $("#spanFuels").show();
            $("#spanStkm").show();
            $("#spanEnkm").show();
            $("#spanReqLeave").show();
            $("#spanCarChk").show();
            $("#btnInsLog").show();
            $("#<%= btnDelLog.ClientID %>").hide();

            $("#txbLogDt").val("");
            $("#txbFuels").val("");
            $("#txbStkm").removeAttr('readonly');
            $("#txbStkm").val("");
            $("#txbEnkm").val("");       
            $("#txbNoti").val("");
            $("#lblCarChk").text("");
            $("#<%= chkReqLeave.ClientID %>").prop("checked", false);
            $("#spanReason").hide();
            $("#txbEtc").val("");

            $("#txbOrgNm").val("");
            $("#hfRowNo").val("");
            $("#txbRegLogDt").val("");
            $("#txbRegEmpNm").val("");
            $("#txbRegEmpNo").val("");
            $("#txbRegKtsEmpNo").val("");
            $("#txbRegCarId").val("");
            $("#txbRegCarNo").val("");
            $("#txbRegStkm").val("");
            $("#txbRegEnkm").val("");
            $("#txbRegWorkDistance").val("");
            $("#txbRegLeavekm").val("");
            $("#txbRegLeaveDistance").val("");
            $("#txbRegResultKm").val("");
            $("#txbRegFuels").val("");
            $("#txbLeaveStat").val("");
            $("#txbRegEtc").val("");
            $("#hfLeaveYn").val("");
            $("#hfLastLogDate").val("");
        }

        function fn_CarChk(chktype) {
            if ($("#chk" + chktype).is(":checked")) {
                $("#chk" + chktype).prop("checked", false);
                $("#divChk" + chktype).removeClass("chkOn");
                $("#divChk" + chktype).addClass("chkOff");
                $("#lblChk" + chktype).text("불량");
                $("#lblChk" + chktype).css("color", 'rgb(224 31 31)');
            }
            else {
                $("#chk" + chktype).prop("checked", true);
                $("#divChk" + chktype).removeClass("chkOff");
                $("#divChk" + chktype).addClass("chkOn");
                $("#lblChk" + chktype).text("양호");
                $("#lblChk" + chktype).css("color", 'rgb(38 220 28)');
            }
        }

        function fn_RegLogCheck() {
            //검차체크기능 추가 필요

            var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();
            var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();
            var logdt = $("#txbLogDt").val();
            var stkm = $("#txbStkm").val();
            var enkm = $("#txbEnkm").val();
            var fuels = $("#txbFuels").val();
            var leaveyn = 'N';
            var carcheck = $("#lblCarChk").text();
            var cartype = $("#hfCarType").val();
            var reason = $("#<%= ddlReason.ClientID %> option:selected").val();
            var etc = $("#txbEtc").val();
            var lastleavechk = $("#hfLeaveYn").val();
            var lastlogdate = $("#hfLastLogDate").val();

            if ($("#<%= chkReqLeave.ClientID %>").is(":checked")) {
                leaveyn = 'R';
            }
            var chk = true;
            var message = '';

            if (proctype == "SAVE") {
                if (carid.length == 0) {
                    chk = false;
                    message = '차량을 선택하세요';
                }
                else if (logdt.length == 0) {
                    chk = false;
                    message = '운행일자를 선택하세요.';
                }
                else if (lastlogdate != "" && lastlogdate == logdt) {
                    chk = false;
                    message = '해당 차량은 운행일자가 같은 운행정보가 이미 등록 되어있습니다.';
                }
                else if (lastlogdate != "" && lastlogdate > logdt) {
                    chk = false;
                    message = '마지막 운행일지 등록일보다 이전 날짜는 등록할 수 없습니다.';
                }
                else if (lastleavechk == "Y" || lastleavechk == "R") {
                    chk = false;
                    message = '해당 차량은 직퇴승인 대기중이거나 직퇴결과가 입력되지 않았습니다.';
                }
                else if (enkm.length == 0) {
                    chk = false;
                    message = '종료km 를 입력해주세요.';
                }
                else if (parseInt(stkm) >= parseInt(enkm)) {
                    chk = false;
                    message = '종료km는 시작km 보다 커야 합니다.';
                }
                else if ((parseInt(enkm) - parseInt(stkm)) > 500) {
                    chk = false;
                    message = '주행거리가 많이 입력되었습니다. 종료km를 확인해주세요';
                }
                else if (fuels != '' && parseInt(fuels) >= 999) {
                    chk = false;
                    message = '주유(ℓ)가 잘못되었습니다. ℓ단위로 입력해주세요.';
                }
                else if (carcheck != "검차완료") {
                    chk = false;
                    message = '선택한 일자에 검차가 저장되지 않았습니다. 검차 저장 후 다시 등록해주세요.';

                    $("#divInsLog").hide();
                    if (cartype == "TOWERWAGON") {
                        $("#divCarChk2").show();
                    }
                    else {
                        $("#divCarChk1").show();
                    }
                }
                else if (leaveyn == 'R' && (reason == '' || reason == undefined)) {
                    chk = false;
                    message = '직퇴사유를 선택해주세요.'
                }
            }
            else {
                chk = false;
                message = '잘못된 접근입니다.';
            }

            if (chk) {
                if (leaveyn == 'R') {
                    message = '운행일지 등록 및 직퇴승인 요청하시겠습니까?';
                }
                else {
                    message = '운행일지를 등록 하시겠습니까?';
                }

                if (confirm(message)) {

                    callGb = 'InsOk';

                    //입력 파라미터
                    var aData = [];
                    aData[0] = carid;
                    aData[1] = logdt;
                    aData[2] = enkm;
                    aData[3] = leaveyn;
                    aData[4] = fuels;
                    aData[5] = reason;
                    aData[6] = etc;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'InsCarLog');

                    ajaxCallEx(jsonData, ajaxUrl, false);
                }
            }
            else {
                alert(message);
            }
        }

        function fn_DelLogCheck() {
            //검차체크기능 추가 필요

            var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();
            var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();
            var logdt = $("#txbLogDt").val();

            var chk = true;
            var message = '';

            if (proctype == "DELETE") {
                if (carid.length == 0) {
                    chk = false;
                    message = '차량을 선택하세요';
                }
                else if (logdt.length == 0) {
                    chk = false;
                    message = '운행일자를 선택하세요.';
                }
            }
            else {
                chk = false;
                message = '잘못된 접근입니다.';
            }

            if (chk) {
                message = "선택한 차량의 " + logdt + "부터 등록 된 모든 운행일지가 삭제됩니다. 삭제하시겠습니까?";
                if (confirm(message)) {
                    callGb = 'DelOk';

                    //입력 파라미터
                    var aData = [];
                    aData[0] = carid;
                    aData[1] = logdt;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'DelCarLog');

                    ajaxCallEx(jsonData, ajaxUrl, false);
                }
            }
            else {
                ktsGridAlert(message);
            }
        }

        function fn_RegCarChk(cartype) {
            if (confirm("검차결과를 저장하시겠습니까?")) {
                var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();
                var logdt = $("#txbLogDt").val();

                var method = "";

                var ChkList;
                var Notice = "";

                var bChk = true;

                if (cartype == "TOWERWAGON") {
                    method = 'InsCarChk2';
                    ChkList = ["Crane1", "Remote", "Outrigger", "Slewing", "Crane2", "WorkTable", "OilLv", "Safe", "Oil2", "Pin", "OilPump", "OilChg", "OilCylinder", "BlackBox1", "Etc2"];
                    Notice = $("#txaNotice2").val();
                }
                else {
                    method = 'InsCarChk1';
                    ChkList = ["Damage", "Oil1", "Tire", "Break", "Light", "Luggage","BlackBox", "Etc1"];
                    Notice = $("#txaNotice1").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = logdt;
                aData[1] = carid;

                var i = 2;

                ChkList.forEach(function (str) {
                    if (!$("#chk" + str).is(":checked")) {
                        bChk = false;
                        aData[i] = "불량";
                    }
                    else {
                        aData[i] = "양호";
                    }
                    i++;
                });

                if (!bChk && Notice.length == 0) {
                    alert("불량항목이 있는 경우 특이사항을 반드시 입력해야합니다.");
                }
                else {
                    callGb = 'InsChkOk';

                    aData[i] = Notice;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, method);

                    ajaxCallEx(jsonData, ajaxUrl, false);

                    ChkList.forEach(function (str) {
                        $("#chk" + str).prop("checked", true);
                        $("#divChk" + str).removeClass("chkOff");
                        $("#divChk" + str).addClass("chkOn");
                        $("#lblChk" + str).text("양호");
                        $("#lblChk" + str).css("color", 'rgb(38 220 28)');
                    });

                    $("#lblCarChk").text('검차완료');
                    $("#lblCarChk").css("color", 'rgb(38 220 28)');

                    $("#txaNotice1").val();
                    $("#txaNotice2").val();

                    $("#divInsLog").show();
                    $("#divCarChk1").hide();
                    $("#divCarChk2").hide();
                }
            }
        }

        function fn_Search() {

            var sdt = new Date($("#<%= txbStDt.ClientID %>").val());
            var edt = new Date($("#<%= txbEnDt.ClientID %>").val());

            var dateDiff = Math.ceil((edt.getTime() - sdt.getTime()) / (1000 * 3600 * 24));

            if (dateDiff > 32) {
                alert(" 조회기간은 1개월을 초과 할 수 없습니다.");
            }
            else {

                $("#divInsLog").hide();
                $("#divRegLog").hide();
                $("#divSearchBox").show();
                $("#divSearchList").show();
                $("#divCarChk1").hide();
                $("#divCarChk2").hide();

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= txbStDt.ClientID %>").val();
                aData[1] = $("#<%= txbEnDt.ClientID %>").val();
                aData[2] = $("#<%= txbEmpNo.ClientID %>").val();
                aData[3] = $("#<%= txbEmpNm.ClientID %>").val();
                aData[4] = $("#<%= txbCarNo.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });
                $("#hdfSelect").val(jsonData);

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $("#<%= ddlCarNo.ClientID %>").val("");

                fn_ResetCarInfo();
            }
        }

        function searchBtnClick() {
            fn_Search();
        }

        function LogViewBtnClick() {
            $("#divInsLog").show();
            $("#divRegLog").hide();
            $("#divSearchBox").hide();
            $("#divSearchList").hide();
            $("#divCarChk1").hide();
            $("#divCarChk2").hide();
        }

        function fn_ShowSaveDiv(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            $("#divInsLog").hide();
            $("#divRegLog").show();
            $("#divSearchBox").hide();
            $("#divSearchList").hide();
            $("#divCarChk1").hide();
            $("#divCarChk2").hide();

            $("#txbOrgNm").val(rowData.ORGFULLNM);
            $("#hfRowNo").val(id);
            $("#txbRegLogDt").val(rowData.LOGDATE);
            $("#txbRegEmpNm").val(rowData.EMPNM);
            $("#txbRegEmpNo").val(rowData.EMPNO);
            $("#txbRegKtsEmpNo").val(rowData.KTSEMPNO);
            $("#txbRegCarId").val(rowData.CARID);
            $("#txbRegCarNo").val(rowData.CARNO);
            $("#txbRegStkm").val(rowData.STKM);
            $("#txbRegEnkm").val(rowData.ENKM);
            $("#txbRegWorkDistance").val(rowData.WORKDISTANCE);
            $("#txbRegLeavekm").val(rowData.LEAVEKM);
            $("#txbRegLeaveDistance").val(rowData.LEAVEDISTANCE);
            $("#txbRegResultKm").val(rowData.RESULTKM);
            $("#txbRegFuels").val(rowData.FUELAMOUNT);
            $("#txbLeaveStat").val(rowData.LEAVESTAT);
            $("#txbRegEtc").val(rowData.ETC);
            $("#<%= ddlRegReason.ClientID %>").val(rowData.REASON);

            if (rowData.LEAVEYN == 'Y' || rowData.LEAVEYN == 'E') {
                $("#txbRegLeavekm").removeAttr('readonly');
            }
            else {
                $("#txbRegLeavekm").attr('readonly', 'true');
            }

            if (rowData.LEAVEYN != 'N') {
                $("#spanRegReason").show();
            }
            else {
                $("#spanRegReason").hide();
            }
        }

        function fn_SaveCheck() {
            var rowNo = $("#hfRowNo").val();
            var CarNo = $("#txbRegCarNo").val();
            var LogDate = $("#txbRegLogDt").val();
            var Leavekm = $("#txbRegLeavekm").val();
            var Enkm = $("#txbRegEnkm").val();

            if (rowNo == "" || rowNo == undefined || CarNo == "" || CarNo == undefined || LogDate == "" || LogDate == undefined) {
                alert("잘못된 접근입니다.");
            }
            else if ((parseInt(Leavekm) - parseInt(Enkm)) > 500) {
                alert("주행거리가 많이 입력되었습니다. 종료km를 확인해주세요");
            }
            else {
                if (confirm(CarNo + " 차량 " + LogDate + " 운행정보를 수정 하시겠습니까?")) {
                    SetCarLoginfo(rowNo);
                }
                else {
                    return false;
                }
            }
        }

        function fn_ConfirmCheck(rowNo, rowCarNo, rowLogdate) {
            if (confirm(rowCarNo + " 차량 " + rowLogdate + " 직퇴요청을 승인 하시겠습니까?")) {
                SetLeaveConfirm(rowNo, "Y");
            }
            else {
                return false;
            }
        }

        function fn_ReturnCheck(rowNo, rowCarNo, rowLogdate) {
            if (confirm(rowCarNo + " 차량 " + rowLogdate + " 직퇴요청을 반려 하시겠습니까?")) {
                SetLeaveConfirm(rowNo, "C");
            }
            else {
                return false;
            }
        }

        function fn_DelCheck(rowNo, rowCarNo, rowLogdate) {
            if (confirm(rowCarNo + " 차량 " + rowLogdate + " 운행정보를 삭제 하시겠습니까?")) {
                DelCarLoginfo(rowNo);
            }
            else {
                return false;
            }
        }

        function SetCarLoginfo(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var enkm = $("#txbRegEnkm").val();
            var leavekm =$("#txbRegLeavekm").val();
            var fuels = $("#txbRegFuels").val();
            var reason = $("#<%= ddlRegReason.ClientID %>").val();
            var etc = $("#txbRegEtc").val();

            callGb = 'RegOk';

            var aData = [];

            aData[0] = rowData.LOGDATE;
            aData[1] = rowData.CARID;
            aData[2] = rowData.CARNO;
            aData[3] = rowData.EMPNO;
            aData[4] = rowData.STKM;
            aData[5] = enkm;
            aData[6] = leavekm;
            aData[7] = fuels;
            aData[8] = reason;
            aData[9] = etc;
            aData[10] = rowData.LEAVEYN;
            aData[11] = rowData.MODIYN;
            aData[12] = rowData.LASTLOGDATE;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetCarLoginfo');

            ajaxCallEx(jsonData, ajaxUrl, false);
        }

        function SetLeaveConfirm(id, ctype) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            if (ctype == "Y") {
                callGb = 'ConfirmOk';
            }
            else {
                callGb = 'ReturnOk';
            }

            var aData = [];

            aData[0] = rowData.LOGDATE;
            aData[1] = rowData.CARID;
            aData[2] = rowData.CARNO;
            aData[3] = ctype;
            aData[4] = rowData.LEAVEYN;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetLeaveConfirm');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function DelCarLoginfo(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            callGb = 'DelOk';

            var aData = [];

            aData[0] = rowData.LOGDATE;
            aData[1] = rowData.CARID;
            aData[2] = rowData.CARNO;
            aData[3] = rowData.DELYN;
            aData[4] = rowData.LASTLOGDATE;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'DelCarLoginfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function SetLeaveConfirm_All() {
            if (confirm("조회된 운행일지 중 모든 직퇴요청을 승인하시겠습니까? ")) {
                var jsonData = $("#hdfSelect").val();

                if (jsonData == undefined || jsonData == "" || jsonData == null) {
                    ktsGridAlert('조회가 되지않았습니다. 일괄 승인을 위해서는 조회를 먼저해야합니다.', 2000);
                }
                else {
                    var DataCnt = ktsGrid.getDataCount();

                    if (DataCnt > 0) {
                        //WebMethod Url
                        var ajaxUrl = ajaxCallGetUrl(location.href, 'SetLeaveConfirm_All');

                        callGb = 'ConfAllOk';
                        ajaxCallEx(jsonData, ajaxUrl, true);
                    }
                    else {
                        ktsGridAlert('조회된 데이터가 없습니다.', 2000);
                    }
                }
            }
        }

    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">		
        <div class="page-clothes-request">
			<h2>운행일지관리</h2>			
			<div id="divInsLog" style="border-bottom:2px solid #dfdfdf;display:none;">
				<fieldset>
					<strong style="margin-top:2px;margin-right:10px;float:left">운행일지등록</strong>
					<span class="optionbox" id="spanICarNo" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">차량번호</label><br />
                        <asp:DropDownList ID="ddlCarNo" runat="server" AutoPostBack="false" style="width:120px"></asp:DropDownList>
                        <input type="hidden" id="hfCarType" value="" />
                        <input type="hidden" id="hfLeaveYn" value="" />
                        <input type="hidden" id="hfLastLogDate" value="" />
					</span>
                    <span class="optionbox" id="spanProcType" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
					    <label style="margin:0 !important;margin-left:0;">등록/삭제</label><br />
                        <asp:DropDownList ID="ddlProcType" runat="server" AutoPostBack="false" style="width:80px">
                            <asp:ListItem Text="등록" Value="SAVE" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
				    </span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">운행일자</label><br />
                        <input type="text" id="txbLogDt" class="date" style="width:84% !important" readonly="true" />
					</span>
					<span class="inpbox" id="spanFuels" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">주유(ℓ)</label><br />
                        <input type="text" id="txbFuels" style="width:84% !important;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="3"/>
					</span>
					<span class="inpbox" id="spanStkm" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">시작km</label><br />
                        <input type="text" id="txbStkm" style="width:84% !important;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11" readonly="true" />
					</span>
					<span class="inpbox" id="spanEnkm" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">종료km</label><br />
                        <input type="text" id="txbEnkm" style="width:84% !important;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11"/>
					</span>
					<span class="ickbox" id="spanReqLeave" style="width:50%; margin:3px 0 3px 0;">
						<label style="display:inline-block;width:50px; padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:800;float:left;">직퇴요청</label><br />
                        <input id="chkReqLeave" runat="server" style="margin-top:8px;margin-left:10px;width:18px;height:18px; border:1px solid #dfdfdf;" type="checkbox" />
					</span>
					<span class="inpbox" id="spanCarChk" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="width:50px;padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000;font-size:13px;font-weight:800;float:left;">검차여부</label><br />
						<label id="lblCarChk" style="width:100px;padding-top:8px;padding-bottom:8px;vertical-align:middle;font-size:13px;font-weight:800;color:rgb(38 220 28);"></label>                 
					</span>
                    <span class="optionbox" id="spanReason" style="width:100%; margin:3px 0 3px 0;display:none;">
						<label style="margin:0 !important;margin-left:0;">직퇴사유</label><br />
                        <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="false" style="width:150px">
                            <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="거주지 원거리" Value="거주지 원거리"></asp:ListItem>
                            <asp:ListItem Text="작업지역 원거리" Value="작업지역 원거리"></asp:ListItem>
                            <asp:ListItem Text="주52시간 근로" Value="주52시간 근로"></asp:ListItem>
                            <asp:ListItem Text="주차상면 부족" Value="주차상면 부족"></asp:ListItem>
                            <asp:ListItem Text="긴급출동 대기" Value="긴급출동 대기"></asp:ListItem>
                        </asp:DropDownList>
                    </span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">비고</label><br />
                        <input type="text" id="txbEtc" style="width:95% !important" />
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">공지</label><br />
                        <input type="text" id="txbNoti" style="width:95% !important" readonly="true" />
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnInsLog" type="button" class="btn-green" style="width:33px;margin:0 !important;border-style:none;" value="등록" />
                        <input id="btnDelLog" type="button" class="btn-black " style="width:33px;margin:0 !important;border-style:none;" value="삭제" runat="server" />
                        <input id="btnDivInsClose" type="button" class="btn-black last" style="width:33px;margin:0 !important;border-style:none;" value="닫기" />
					</span>
				</fieldset> 
			</div>		

			<div id="divRegLog" style="border-bottom:2px solid #dfdfdf;display:none;">
				<fieldset>
					<strong style="margin-top:2px;margin-right:10px;float:left">운행일지수정</strong>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">소속</label><br />
                        <input type="text" id="txbOrgNm" style="width:92% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
                        <input type="hidden" id="hfRowNo" />
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">운행일자</label><br />
                        <input type="text" id="txbRegLogDt" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">이름</label><br />
                        <input type="text" id="txbRegEmpNm" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">사번</label><br />
                        <input type="text" id="txbRegEmpNo" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">kts사번</label><br />
                        <input type="text" id="txbRegKtsEmpNo" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">차대번호</label><br />
                        <input type="text" id="txbRegCarId" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">차량번호</label><br />
                        <input type="text" id="txbRegCarNo" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">시작km</label><br />
                        <input type="text" id="txbRegStkm" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">종료km</label><br />
                        <input type="text" id="txbRegEnkm" style="width:84% !important;padding:0;text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11"/>
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">운행거리</label><br />
                        <input type="text" id="txbRegWorkDistance" style="width:42% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">직퇴km</label><br />
                        <input type="text" id="txbRegLeavekm" style="width:84% !important;padding:0;text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">직퇴거리</label><br />
                        <input type="text" id="txbRegLeaveDistance" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">최종결과</label><br />
                        <input type="text" id="txbRegResultKm" style="width:84% !important;padding:0;text-align:center;border-style:none;" readonly="true"/>
					</span>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">주유(ℓ)</label><br />
                        <input type="text" id="txbRegFuels" style="width:84% !important;padding:0;text-align:center;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11"/>
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0;">
						<label style="margin:0 !important;margin-left:0;">직퇴요청결과</label><br />
                        <input type="text" id="txbLeaveStat" style="width:95% !important;padding:0;text-align:center;border-style:none;" readonly="true" />
					</span>
					<span class="optionbox" id="spanRegReason" style="width:100%; margin:3px 0 3px 0;">
						<label style="margin:0 !important;margin-left:0;">직퇴사유</label><br />
                        <asp:DropDownList ID="ddlRegReason" runat="server" AutoPostBack="false" style="width:150px">
                            <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="거주지 원거리" Value="거주지 원거리"></asp:ListItem>
                            <asp:ListItem Text="작업지역 원거리" Value="작업지역 원거리"></asp:ListItem>
                            <asp:ListItem Text="주52시간 근로" Value="주52시간 근로"></asp:ListItem>
                            <asp:ListItem Text="주차상면 부족" Value="주차상면 부족"></asp:ListItem>
                            <%--<asp:ListItem Text="코로나 집합 금지" Value="코로나 집합 금지"></asp:ListItem>--%>
                            <asp:ListItem Text="긴급출동 대기" Value="긴급출동 대기"></asp:ListItem>
                        </asp:DropDownList>
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0;">
						<label style="margin:0 !important;margin-left:0;">비고</label><br />
                        <input type="text" id="txbRegEtc" style="width:95% !important;padding:0;text-align:center;border-style:none;" />
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnRegLog" type="button" class="btn-green" style="width:33px;margin:0 !important;border-style:none;" value="수정" />
                        <input id="btnDivRegClose" type="button" class="btn-black last" style="width:33px;margin:0 !important;border-style:none;" value="닫기" />
					</span>
				</fieldset> 
			</div>

			<div id="divCarChk1" style="border-bottom:2px solid #dfdfdf;display:none;">
				<fieldset>
					<strong style="margin-top:2px;margin-right:10px;width:100%">일반차량검차</strong>
                        <span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%;">차량파손</label><br />
                        <label id="lblChkDamage" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Damage');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Damage');">
                            <div id="divChkDamage" class="chkOn"></div>
                        </div>
                        <input id="chkDamage" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%;">오일누수</label><br />
                        <label id="lblChkOil11" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Oil1');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Oil1');">
                            <div id="divChkOil1" class="chkOn"></div>
                        </div>
                        <input id="chkOil1" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%;">타이어</label><br />
                        <label id="lblChkTire" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Tire');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Tire');">
                            <div id="divChkTire" class="chkOn"></div>
                        </div>
                        <input id="chkTire" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%;">브레이크</label><br />
                        <label id="lblChkBreak" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Break');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Break');">
                            <div id="divChkBreak" class="chkOn"></div>
                        </div>
                        <input id="chkBreak" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%;">라이트/계기판</label><br />
                        <label id="lblChkLight" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Light');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Light');">
                            <div id="divChkLight" class="chkOn"></div>
                        </div>
                        <input id="chkLight" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%;">적재물안전</label><br />
                        <label id="lblChkLuggage" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Luggage');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Luggage');">
                            <div id="divChkLuggage" class="chkOn"></div>
                        </div>
                        <input id="chkLuggage" type="checkbox" style="display:none;" checked="checked"/>
					</span>
                    <span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
					    <label style="margin:0 !important;margin-left:0;width:100%"l> 블랙박스작동</label><br />
                        <label id="lblChkBlackBox" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('BlackBox');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('BlackBox');">
                            <div id="divChkBlackBox" class="chkOn"></div>
                        </div>
                        <input id="chkBlackBox" type="checkbox" style="display:none;" checked="checked"/>
                    </span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%;">기타(직접기재)</label><br />
                        <label id="lblChkEtc1" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Etc1');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Etc1');">
                            <div id="divChkEtc1" class="chkOn"></div>
                        </div>
                        <input id="chkEtc1" type="checkbox" style="display:none;" checked="checked"/>
					</span>
                    <span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
                        <textarea id="txaNotice1" rows="5" maxlength="1500" style="width:95%; height:100px;" placeholder="특이사항(불량항목이 있는 경우 반드시 입력)" ></textarea>
                    </span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnRegChk1" type="button" class="btn-green" style="width:75px;margin:0 !important;border-style:none;" value="검차저장" />
                        <input id="btnChk1Close" type="button" class="btn-black last" style="width:33px;margin:0 !important;border-style:none;" value="닫기" />
					</span>
				</fieldset> 
			</div>

			<div id="divCarChk2" style="border-bottom:2px solid #dfdfdf;display:none;">
				<fieldset>
					<strong style="margin-top:2px;margin-right:10px;width:100%">고소차량검차</strong>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">붐대외관</label><br />
                        <label id="lblChkCrane1" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Crane1');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Crane1');">
                            <div id="divChkCrane1" class="chkOn"></div>
                        </div>
                        <input id="chkCrane1" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">리모콘작동</label><br />
                        <label id="lblChkRemote" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Remote');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Remote');">
                            <div id="divChkRemote" class="chkOn"></div>
                        </div>
                        <input id="chkRemote" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">아우트리거작동</label><br />
                        <label id="lblChkOutrigger" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Outrigger');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Outrigger');">
                            <div id="divChkOutrigger" class="chkOn"></div>
                        </div> 
                        <input id="chkOutrigger" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">선회장치작동</label><br />
                        <label id="lblChkSlewing" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Slewing');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Slewing');">
                            <div id="divChkSlewing" class="chkOn"></div>
                        </div>
                        <input id="chkSlewing" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">붐대작동</label><br />
                        <label id="lblChkCrane2" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Crane2');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Crane2');">
                            <div id="divChkCrane2" class="chkOn"></div>
                        </div>
                        <input id="chkCrane2" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">작업대작동</label><br />
                        <label id="lblChkWorkTable" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('WorkTable');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('WorkTable');">
                            <div id="divChkWorkTable" class="chkOn"></div>
                        </div>
                        <input id="chkWorkTable" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">오일양점검</label><br />
                        <label id="lblChkOilLv" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('OilLv');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('OilLv');">
                            <div id="divChkOilLv" class="chkOn"></div>
                        </div>
                        <input id="chkOilLv" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">안전장치작동</label><br />
                        <label id="lblChkSafe" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Safe');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Safe');">
                            <div id="divChkSafe" class="chkOn"></div>
                        </div>
                        <input id="chkSafe" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">오일누수</label><br />
                        <label id="lblChkOil2" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Oil2');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Oil2');">
                            <div id="divChkOil2" class="chkOn"></div>
                        </div>
                        <input id="chkOil2" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">핀,너트풀림</label><br />
                        <label id="lblChkPin" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Pin');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Pin');">
                            <div id="divChPin" class="chkOn"></div>
                        </div>
                        <input id="chkPin" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">유압펌프점검</label><br />
                        <label id="lblChkOilPump" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('OilPump');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('OilPump');">
                            <div id="divChkOilPump" class="chkOn"></div>
                        </div>
                        <input id="chkOilPump" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">유압오일교체</label><br />
                        <label id="lblChkOilChg" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('OilChg');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('OilChg');">
                            <div id="divChkOilChg" class="chkOn"></div>
                        </div>
                        <input id="chkOilChg" type="checkbox" style="display:none;" checked="checked"/>
					</span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">유압실린더점검</label><br />
                        <label id="lblChkOilCylinder" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('OilCylinder');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('OilCylinder');">
                            <div id="divChkOilCylinder" class="chkOn"></div>
                        </div>
                        <input id="chkOilCylinder" type="checkbox" style="display:none;" checked="checked"/>
					</span>
                    <span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
					    <label style="margin:0 !important;margin-left:0;width:100%"l> 블랙박스작동</label><br />
                        <label id="lblChkBlackBox1" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('BlackBox1');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('BlackBox1');">
                            <div id="divChkBlackBox1" class="chkOn"></div>
                        </div>
                        <input id="chkBlackBox1" type="checkbox" style="display:none;" checked="checked"/>
                    </span>
					<span class="ickbox" style="width:33%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;width:100%">기타(직접기재)</label><br />
                        <label id="lblChkEtc2" style="font: bold 12px;color:rgb(38 220 28); margin-right:25% !important;float: right;" onclick="fn_CarChk('Etc2');">양호</label>
                        <div class="divChkBackground" onclick="fn_CarChk('Etc2');">
                            <div id="divChkEtc2" class="chkOn"></div>
                        </div>
                        <input id="chkEtc2" type="checkbox" style="display:none;" checked="checked"/>
					</span>
                    <span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
                        <textarea id="txaNotice2" rows="5" maxlength="1500" style="width:95%; height:100px;" placeholder="특이사항(불량항목이 있는 경우 반드시 입력)" ></textarea>
                    </span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnRegChk2" type="button" class="btn-green" style="width:75px;margin:0 !important;border-style:none;" value="검차저장" />
                        <input id="btnChk2Close" type="button" class="btn-black last" style="width:33px;margin:0 !important;border-style:none;" value="닫기" />
					</span>
				</fieldset> 
			</div>

			<div id="divSearchBox" style="border-bottom:2px solid #dfdfdf;">
				<!-- S:fieldset -->
				<fieldset>
					<legend>조회조건</legend>
					<span class="inpbox" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">운행일자</label><br />
						<asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
						<label style="width:10px">~</label>
						<asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanCarNo" runat="server" style="width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">차량번호</label><br />
						<asp:TextBox ID="txbCarNo" runat="server" MaxLength="20" style="width:84% !important"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanEmpNo" runat="server" style="display:none;width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">사번</label><br />
						<asp:TextBox ID="txbEmpNo" runat="server" MaxLength="20" style="width:84% !important"></asp:TextBox>
					</span>
					<span class="inpbox" id="spanEmpNm" runat="server" style="display:none;width:50%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">이름</label><br />
						<asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:84% !important"></asp:TextBox>
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <asp:UpdatePanel ID="udpBtn" runat="server">
                            <ContentTemplate>
                                <input id="btnSelect" type="button" runat="server" class="btn-green" style="width:33px;margin-right:0.8px !important;border-style:none;" value="조회" />
                                <input id="btnDivLogView" type="button" runat="server" class="btn-green" style="width:100px;margin-right:0.8px !important;border-style:none;" value="운행일지등록" />
                                <input id="btnAllConf" runat="server" type="button" class="btn-green last" style="width:100px;margin:0 !important;border-style:none;" visible="false" value="일괄승인" />
                                <asp:HiddenField ID="hdfLeaveCnt" runat="server" Value="0" OnValueChanged="hdfLeaveCnt_ValueChanged"/>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger  ControlID="hdfLeaveCnt" EventName="ValueChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <input id="hdfSelect" type="hidden" />
					</span>
				</fieldset>
				<!-- //E:fieldset -->
			</div>
            <!-- S: scrollBox -->
            <div id="divSearchList" class="scrollbox">
				<div class="requestlist">
					<strong style="margin-top:2px;margin-right:10px;float:left">운행이력</strong>
                    <!-- S:datalist -->
                    <div class="datalist" style="margin-top:10px;">
			            <!-- S:scrollbox -->
			            <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
			            <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
				</div>
			</div>
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>