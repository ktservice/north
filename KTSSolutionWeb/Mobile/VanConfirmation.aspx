﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Order.Master" AutoEventWireup="true" CodeBehind="VanConfirmation.aspx.cs" Inherits="KTSSolutionWeb.VanConfirmation" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>
    	.scrollbox {width:100%;overflow-x:hidden;overflow-y:auto;}
        .inpbox span {display:inline-block; width:100%; height:30px; vertical-align:middle; color:#000; text-align:left; font-size:14px; font-weight:800; margin:3px 0 3px 0; padding:5px 3px 0 8px; overflow-y:auto;}
        .inpbox input {display:inline-block; width:100%; height:30px; padding:0 14px; border:1px solid #dfdfdf; border-radius:2px; line-height:30px; vertical-align:middle; color:#000; font-size:13px; margin-top:2px; margin-bottom:2px}
        .inpbox label {margin:0 !important; padding-top:7px;margin-left:0;}
        .btn-gray {display:inline-block; min-width:55px; width:55px; height:33px; padding:0 2px; border-style:none; background:#999; text-align:center; line-height:33px; border-radius:2px; color:#fff; font-size:14px; font-weight:800}
		.btn-green {display:inline-block; min-width:55px; width:55px; height:33px; padding:0 2px; border-style:none; background:#009687; text-align:center; line-height:33px; border-radius:2px; color:#fff; font-size:14px; font-weight:800}		
        .div-radio {
            border: 1px solid darkgray;
            width: 65px;
            height: 30px;
            cursor: pointer;
            padding-top: 2px;
            text-align: left;
            border-radius: 3px;
            background-color: white;
			float:left;
        }
    	.input-check {
			width: 30px !important;
			height: 20px !important;
    	}
        .label-rbInner {
            cursor: pointer !important;
            padding-left: 2px !important;
			padding-top: 2px !important;
            font-size:15px !important;
        }
    </style>
    <script type="text/javascript">
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
				fn_Load();
				//SetCanvas();
            },
			eventbind: function () {
            }
        };
        
        var fn_Load = function () {
        };

		var fn_numberChk = function (e, data, len) {
            if (data.length >= len) return false;
            if (e.key > 47 && e.key < 58) return true;
            else return false;
        }

        var SetCanvas = function () {
            var drawCanvas = document.getElementById("cvSign");
            var rect = drawCanvas.getBoundingClientRect();
            var drawBackup = new Array();

            if (typeof drawCanvas.getContext == 'function') {
                var ctx = drawCanvas.getContext("2d");

                ctx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
                ctx.beginPath();
                localStorage.removeItem("ImgCanvas");
                $("#hdfSignYn").val("N");

                var isDraw = false;
                var pDraw = $("#cvSign").offset();

                var currP = null;
                ctx.clearRect(0, 0, drawCanvas.offsetwidth, drawCanvas.offsetHeight);

                var draw_line = function (p) {
                    ctx.lineWidth = "3px";
                    ctx.lineCap = "round";
                    ctx.lineTo(p.X, p.Y);
                    ctx.moveTo(p.X, p.Y);
                    ctx.strokeStyle = "#000000";
                    ctx.stroke();
                }

                var saveCanvas = function () {
                    drawBackup.push(ctx.getImageData(0, 0, drawCanvas.width, drawCanvas.height));
                }

                $("#cvSign").bind('mousedown', function (e) {
                    if (e.button == 0) {
                        saveCanvas();
                        e.preventDefault();
                        ctx.beginPath();

                        isDraw = true;
                    }
                });

                $("#cvSign").bind('mousemove', function (e) {
                    var event = e.originalEvent;
                    e.preventDefault();
                    currP = { X: event.ClientX - pDraw.Left, Y: event.ClientY - pDraw.Top };

                    if (isDraw) {
                        $("#hdfSignYn").val("Y");
						draw_line(currP);
					}
				});

				$("#cvSign").bind('mouseup', function (e) {
					e.preventDefault();

					isDraw = false;
				});

				$("#cvSign").bind('mouseleave', function (e) {
					isDraw = false;
				});

				$("#cvSign").bind('touchstart', function (e) {
					saveCanvas();
					e.preventDefault();
					ctx.beginPath();

				});

				$("#cvSign").bind('touchmove', function (e) {
					var event = e.originalEvent;
					e.preventDefault();

					currP = { X: event.touches[0].pageX - pDraw.left, Y: event.touches[0].pageY - pDraw.top };

                    $("#hdfSignYn").val("Y");
                    draw_line(currP);
                });

                $("#cvSign").bind('touchend', function (e) {
                    e.preventDefault();
                });
            }
        };

        var saveImage = function () {
			var canvas = document.getElementById("cvSign");
            localStorage.setItem("ImgCanvas", canvas.toDataURL("image/png"));

            var img = document.getElementById("imgSign");
			var imgData = canvas.toDataURL("image/png");
			img.src = imgData;

            return imgData;
        }

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'ConfOk') {
							alert('포스 설치 및 교육 확인서가 저장되었습니다.');
							fn_PageMoveDetail();
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }

            callGb = '';
		};

        var fn_Prev = function () {
            $("#btnConf").hide();
            if ($("#div6").is(':visible')) {
                $("#div6").hide();
                $("#div5").show();
                $("#div4").hide();
                $("#div3").hide();
                $("#div2").hide();
                $("#div1").hide();
                $("#btnPrev").show();
                $("#btnNext").show();
			}
            else if ($("#div5").is(':visible')) {
                $("#div6").hide();
                $("#div5").hide();
                $("#div4").show();
                $("#div3").hide();
                $("#div2").hide();
                $("#div1").hide();
                $("#btnPrev").show();
                $("#btnNext").show();
            }
            else if ($("#div4").is(':visible')) {
                $("#div6").hide();
                $("#div5").hide();
                $("#div4").hide();
                $("#div3").show();
                $("#div2").hide();
                $("#div1").hide();
                $("#btnPrev").show();
                $("#btnNext").show();
            }
            else if ($("#div3").is(':visible')) {
                $("#div6").hide();
                $("#div5").hide();
                $("#div4").hide();
                $("#div3").hide();
                $("#div2").show();
                $("#div1").hide();
                $("#btnPrev").show();
                $("#btnNext").show();
            }
            else if ($("#div2").is(':visible')) {
                $("#div6").hide();
                $("#div5").hide();
                $("#div4").hide();
                $("#div3").hide();
                $("#div2").hide();
                $("#div1").show();
                $("#btnPrev").hide();
                $("#btnNext").show();
            }
		};

        var fn_Next = function () {
            if ($("#div1").is(':visible')) {
                $("#div6").hide();
                $("#div5").hide();
                $("#div4").hide();
                $("#div3").hide();
                $("#div2").show();
                $("#div1").hide();
                $("#btnNext").show();
                $("#btnPrev").show();
            }
            else if ($("#div2").is(':visible')) {
                $("#div6").hide();
                $("#div5").hide();
                $("#div4").hide();
                $("#div3").show();
                $("#div2").hide();
                $("#div1").hide();
                $("#btnNext").show();
                $("#btnPrev").show();
            }
            else if ($("#div3").is(':visible')) {
                $("#div6").hide();
                $("#div5").hide();
                $("#div4").show();
                $("#div3").hide();
                $("#div2").hide();
                $("#div1").hide();
                $("#btnNext").show();
                $("#btnPrev").show();
            }
            else if ($("#div4").is(':visible')) {
                $("#div6").hide();
                $("#div5").show();
                $("#div4").hide();
                $("#div3").hide();
                $("#div2").hide();
                $("#div1").hide();
                $("#btnNext").show();
                $("#btnPrev").show();
            }
            else if ($("#div5").is(':visible')) {
                $("#div6").show();
                $("#div5").hide();
                $("#div4").hide();
                $("#div3").hide();
                $("#div2").hide();
                $("#div1").hide();
                $("#btnNext").hide();
                $("#btnPrev").show();
				$("#btnConf").show();
				SetCanvas();
            }
		};

		var fn_ValidationChk = function () {
			var msg = "";
			//var div2Radio = $("#div2").contents().find("input[type=radio]:checked");
			//var div3Radio = $("#div3").contents().find("input[type=radio]:checked");
			//var div4Radio = $("#div4").contents().find("input[type=radio]:checked");
			//var div5Radio = $("#div5").contents().find("input[type=radio]:checked");

			//var NCnt2 = 0;
			//var NCnt3 = 0;
			//var NCnt4 = 0;
			//var NCnt5 = 0;

			var custNm = $("#txbCustNm").val();
			var signVal = $("#hdfSignYn").val();

			//div2Radio.each(function () {
			//	var chkVal = $(this).val();

			//	if (chkVal == "N") {
			//		NCnt2++;
			//	}
			//});

			//div3Radio.each(function () {
			//	var chkVal = $(this).val();

			//	if (chkVal == "N") {
			//		NCnt3++;
			//	}
			//});

			//div4Radio.each(function () {
			//	var chkVal = $(this).val();
			//	var controlNm = $(this).attr('name');

			//	if (chkVal == "N") {
			//		if (controlNm == "rbPosChk") {
			//			var ModelNm = $("#txbPosNm").val();

			//			if (ModelNm.length > 0) {
			//				NCnt4++;
			//			}
			//		}
			//		else if (controlNm == "rbTerminalChk") {
			//			var ModelNm = $("#txbTerminalNm").val();

			//			if (ModelNm.length > 0) {
			//				NCnt4++;
			//			}
			//		}
			//		else if (controlNm == "rbPrintChk") {
			//			var ModelNm = $("#txbPrintNm").val();

			//			if (ModelNm.length > 0) {
			//				NCnt4++;
			//			}
			//		}
			//	}
			//});

			//div5Radio.each(function () {
			//	var chkVal = $(this).val();

			//	if (chkVal == "N") {
			//		NCnt5++;
			//	}
			//});

			//if (NCnt2 > 0) {
			//	msg = "설치점검사항에 누락된 점검 항목이 있습니다.";
			//	return
			//}
			//else if (NCnt3 > 0) {
			//	msg = "원격프로그램 설치 및 가맹점 안내 점검 항목이 누락되었습니다.";
			//}
			//else if (NCnt4 > 0) {
            //    msg = "설치내역에 누락된 항목이 있습니다. 모델명이 입력된 경우 점검확인을 해야합니다.";
			//}
			//else if (NCnt5 > 0) {
            //    msg = "교육내역에 누락된 항목이 있습니다.";
			//}
			
			if (custNm.length == 0 || signVal != "Y") {
                msg = "확인자명과 서명을 입력해 주세요.";
			}

			return msg;
        };

        var fn_Confirm = function () {
			if (confirm("포스 설치/교육확인서를 저장 하시겠습니까?")) {
				var msg = fn_ValidationChk();

				if (msg != "") {
					alert(msg);
				}
                else {
                    var odrno = $("#<%=hdfOrderNo.ClientID %>").val();

					//설치점검사항
					var chk1 = $("input[name=rbChk1]:checked").val();
                    var chk1Note = $("#txbNoteChk1").val();
                    var chk2 = $("input[name=rbChk2]:checked").val();
                    var chk2Note = $("#txbNoteChk2").val();
                    var chk3 = $("input[name=rbChk3]:checked").val();
                    var chk3Note = $("#txbNoteChk3").val();
                    var chk4 = $("input[name=rbChk4]:checked").val();
                    var chk4Note = $("#txbNoteChk4").val();
                    var chk5 = $("input[name=rbChk5]:checked").val();
                    var chk5Note = $("#txbNoteChk5").val();
                    var chk6 = $("input[name=rbChk6]:checked").val();
                    var chk6Note = $("#txbNoteChk6").val();
                    var chk7 = $("input[name=rbChk7]:checked").val();
                    var chk7Note = $("#txbNoteChk7").val();
                    var chk8 = $("input[name=rbChk8]:checked").val();
                    var chk8Note = $("#txbNoteChk8").val();
                    var chk9 = $("input[name=rbChk9]:checked").val();
                    var chk9Note = $("#txbNoteChk9").val();
                    var chk10 = $("#txbChk10_1").val() + " 조 / " + $("#txbChk10_2").val() +" M";
                    var chk10Note = $("#txbNoteChk10").val();

					//원격 프로그램 즐겨찾기 추가설치
                    var chkPgm = $("input[name=rbPgm]:checked").val();
					var pgmNote = $("#txbNotePgm").val();

					//설치 내역
                    var posNm = $("#txbPosNm").val();
                    var posCnt = $("#txbPosCnt").val();
                    var posChk = $("input[name=rbPosChk]:checked").val();
					var posNote = $("#txbNotePos").val();

                    var terminalNm = $("#txbTerminalNm").val();
                    var terminalCnt = $("#txbTerminalCnt").val();
                    var terminalChk = $("input[name=rbTerminalChk]:checked").val();
                    var terminalNote = $("#txbNoteTerminal").val();

                    var printNm = $("#txbPrintNm").val();
                    var printCnt = $("#txbPrintCnt").val();
                    var printChk = $("input[name=rbPrintChk]:checked").val();
                    var printNote = $("#txbNotePrint").val();
                    
                    //교육 내역
                    var traning1 = $("input[name=rbTraning1]:checked").val();
                    var traning1Note = $("#txbNoteTraning1").val();
                    var traning2 = $("input[name=rbTraning2]:checked").val();
                    var traning2Note = $("#txbNoteTraning2").val();
                    var traning3 = $("input[name=rbTraning3]:checked").val();
                    var traning3Note = $("#txbNoteTraning3").val();
                    var traning4 = $("input[name=rbTraning4]:checked").val();
                    var traning4Note = $("#txbNoteTraning4").val();
                    var traning5 = $("input[name=rbTraning5]:checked").val();
                    var traning5Note = $("#txbNoteTraning5").val();
                    var traning6 = $("input[name=rbTraning6]:checked").val();
                    var traning6Note = $("#txbNoteTraning6").val();
                    var traning7 = $("input[name=rbTraning7]:checked").val();
                    var traning7Note = $("#txbNoteTraning7").val();
                    var traning8 = $("input[name=rbTraning8]:checked").val();
                    var traning8Note = $("#txbNoteTraning8").val();

					//서명
                    var custNm = $("#txbCustNm").val();
					var custSign = saveImage();

                    callGb = 'ConfOk'

                    var aData = [];

                    aData[0] = odrno;
                    aData[1] = chk1;
                    aData[2] = chk1Note;
                    aData[3] = chk2;
                    aData[4] = chk2Note;
                    aData[5] = chk3;
                    aData[6] = chk3Note;
                    aData[7] = chk4;
                    aData[8] = chk4Note;
                    aData[9] = chk5;
                    aData[10] = chk5Note;
                    aData[11] = chk6;
                    aData[12] = chk6Note;
                    aData[13] = chk7;
                    aData[14] = chk7Note;
                    aData[15] = chk8;
                    aData[16] = chk8Note;
                    aData[17] = chk9;
                    aData[18] = chk9Note;
                    aData[19] = chk10;
                    aData[20] = chk10Note;
                    aData[21] = chkPgm;
                    aData[22] = pgmNote;
                    aData[23] = posNm;
                    aData[24] = posCnt;
                    aData[25] = posChk;
                    aData[26] = posNote;
                    aData[27] = terminalNm;
                    aData[28] = terminalCnt;
                    aData[29] = terminalChk;
                    aData[30] = terminalNote;
                    aData[31] = printNm;
                    aData[32] = printCnt;
                    aData[33] = printChk;
                    aData[34] = printNote;
                    aData[35] = traning1;
                    aData[36] = traning1Note;
                    aData[37] = traning2;
                    aData[38] = traning2Note;
                    aData[39] = traning3;
                    aData[40] = traning3Note;
                    aData[41] = traning4;
                    aData[42] = traning4Note;
                    aData[43] = traning5;
                    aData[44] = traning5Note;
                    aData[45] = traning6;
                    aData[46] = traning6Note;
                    aData[47] = traning7;
                    aData[48] = traning7Note;
                    aData[49] = traning8;
                    aData[50] = traning8Note;
                    aData[51] = custNm;
                    aData[52] = custSign;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetConf");

                    ajaxCallEx(jsonData, ajaxUrl, true);
				}
			}
		};

        var fn_PageMoveDetail = function () {
            var odrno = $("#<%=hdfOrderNo.ClientID %>").val();

			var param = {
                pORDERNO: odrno
			};

			var form = createForm("/Mobile/VanOrderInfoByOrderNo", param);

			form.submit();
		};

        </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<div class="page">
            <h2 id="htitle" style="margin-bottom:5px;padding-bottom:5px;">포스 설치 / 교육 확인서</h2>
            <!-- S: div -->
			<div id="div1" class="scrollbox">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom:15px">
                        <h4 style="color:black;border-bottom: 2px;border-bottom-style: ridge;">◈ 설치정보</h4>
						<span class="inpbox" style="width:100%">
							<label>설치담당자</label>
							<asp:Label ID="lblEmpNm" runat="server" BackColor="#dedede"></asp:Label>
						</span>
						<span class="inpbox" style="width:100%">
							<label>설치일자</label>
							<asp:Label ID="lblInstallDt" runat="server" BackColor="#dedede"></asp:Label>
						</span>
						<span class="inpbox" style="width:100%">
							<label>매장명</label>
							<asp:Label ID="lblFranchiseNm" runat="server" BackColor="#dedede"></asp:Label>
						</span>
						<span class="inpbox" style="width:100%">
							<label>사업자번호</label>
							<asp:Label ID="lblCorpNo" runat="server" BackColor="#dedede"></asp:Label>
						</span>
					</div>
				</div>
			</div>
			<div id="div2" class="scrollbox" style="display:none;">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom: 15px">
                        <h4 style="color:black;border-bottom: 2px;border-bottom-style: ridge;">◈ 설치점검사항</h4>
						<div style="height:30px;margin: 8px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">1. 설치시간 준수</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk1" id="rbChk1Y" value="Y" /><label for="rbChk1Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk1" id="rbChk1N" value="N" checked="checked" /><label for="rbChk1N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk1" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">2. 메뉴등록 완료</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk2" id="rbChk2Y" value="Y" /><label for="rbChk2Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk2" id="rbChk2N" value="N" checked="checked" /><label for="rbChk2N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk2" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">3. 메뉴 정상 출력</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk3" id="rbChk3Y" value="Y" /><label for="rbChk3Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk3" id="rbChk3N" value="N" checked="checked" /><label for="rbChk3N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk3" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">4. 신용카드 결제 테스트(결제/취소 진행)</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk4" id="rbChk4Y" value="Y" /><label for="rbChk4Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk4" id="rbChk4N" value="N" checked="checked" /><label for="rbChk4N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk4" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">5. 현금영수증 테스트(발행/취소 진행)</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk5" id="rbChk5Y" value="Y" /><label for="rbChk5Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk5" id="rbChk5N" value="N" checked="checked" /><label for="rbChk5N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk5" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">6. 감열프린트 설치 시 영수증 출력 테스트</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk6" id="rbChk6Y" value="Y" /><label for="rbChk6Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk6" id="rbChk6N" value="N" checked="checked" /><label for="rbChk6N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk6" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">7. 포스 모니터 터치(좌표) 이상유무</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk7" id="rbChk7Y" value="Y" /><label for="rbChk7Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk7" id="rbChk7N" value="N" checked="checked" /><label for="rbChk7N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk7" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">8. 듀얼모니터 설치 시 이상유무</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk8" id="rbChk8Y" value="Y" /><label for="rbChk8Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk8" id="rbChk8N" value="N" checked="checked" /><label for="rbChk8N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk8" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">9. 전원 ON/OFF 이상유무</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">확인유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbChk9" id="rbChk9Y" value="Y" /><label for="rbChk9Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbChk9" id="rbChk9N" value="N" checked="checked" /><label for="rbChk9N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk9" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">10. LAN포설 길이 기재(ex 5M,10M,15M 등)</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">확인유무</label>
								<input type="text" id="txbChk10_1" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" style="width:30px;margin-right:5px;text-align:center;padding:0;" maxlength="3" />
								<label style="width:20px;">조</label>
								<input type="text" id="txbChk10_2" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" style="width:30px;margin-right:5px;text-align:center;padding:0;" maxlength="3" />
								<label style="width:20px;">M</label>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteChk10" style="width:85%;" maxlength="5000" placeholder="업무지원 거증사진 첨부 必" />
							</span>
						</div>
					</div>
				</div>	
			</div>
			<div id="div3" class="scrollbox" style="display:none;">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom:15px">
                        <h4 style="color:black;border-bottom: 2px;border-bottom-style: ridge;">◈ 원격 프로그램 즐겨찾기 추가설치</h4>
						<div style="height:30px;margin: 8px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">1. 원격 프로그램 설치 및 가맹점 안내 유무</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbPgm" id="rbPgmY" value="Y" /><label for="rbPgmY" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbPgm" id="rbPgmN" value="N" checked="checked" /><label for="rbPgmN" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNotePgm" style="width:85%;" maxlength="5000" placeholder="오더확인 후 아래 1 or 2 선택 설치" />
							</span>
							<span class="inpbox" style="width:100%">
								<label style="color:red;">1. CUN : "http://377.co.kr" 접속 -> 하단 "원격고객용설치" -> 상단 <image src="/Resource/images/icon-377.png"></image> 클릭 -> 이름변경 'S원격지원 15660365'</label>
								<label style="color:red;">2. KTS기술지원 : "http://367.co.kt/ktspos/" 접속 -> 즐겨찾기 추가</label>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div id="div4" class="scrollbox" style="display:none;">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom:15px">
                        <h4 style="color:black;border-bottom: 2px;border-bottom-style: ridge;">◈ 설치 내역</h4>
						<div style="height:30px;margin: 8px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">포스</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">모델명</label>
								<input type="text" id="txbPosNm" style="width:85%;" />
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">수량</label>
								<input type="text" id="txbPosCnt" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" style="width:50px;text-align:center;padding:0;" maxlength="3" />
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbPosChk" id="rbPosY" value="Y" /><label for="rbPosY" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbPosChk" id="rbPosN" value="N" checked="checked" /><label for="rbPosN" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNotePos" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">카드단말기</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">모델명</label>
								<input type="text" id="txbTerminalNm" style="width:85%;" />
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">수량</label>
								<input type="text" id="txbTerminalCnt" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" style="width:50px;text-align:center;padding:0;" maxlength="3" />
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTerminalChk" id="rbTerminalY" value="Y" /><label for="rbTerminalY" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTerminalChk" id="rbTerminalN" value="N" checked="checked" /><label for="rbTerminalN" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTerminal" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">프린트</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">모델명</label>
								<input type="text" id="txbPrintNm" style="width:85%;" />
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">수량</label>
								<input type="text" id="txbPrintCnt" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" style="width:50px;text-align:center;padding:0;" maxlength="3" />
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbPrintChk" id="rbPrintY" value="Y" /><label for="rbPrintY" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbPrintChk" id="rbPrintN" value="N" checked="checked" /><label for="rbPrintN" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNotePrint" style="width:85%;" maxlength="5000" />
							</span>
						</div>
					</div>
				</div>
			</div>
			<div id="div5" class="scrollbox" style="display:none;">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom: 15px">
                        <h4 style="color:black;border-bottom: 2px;border-bottom-style: ridge;">◈ 교육 내역</h4>
						<div style="height:30px;margin: 8px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">1. 전원ON/OFF 방법 설명</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning1" id="rbTraning1Y" value="Y" /><label for="rbTraning1Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning1" id="rbTraning1N" value="N" checked="checked" /><label for="rbTraning1N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning1" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">2. 프로그램 시작방법(로그인/개점처리) 설명</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning2" id="rbTraning2Y" value="Y" /><label for="rbTraning2Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning2" id="rbTraning2N" value="N" checked="checked" /><label for="rbTraning2N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning2" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">3. 원격지원 요청방법 설명</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning3" id="rbTraning3Y" value="Y" /><label for="rbTraning3Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning3" id="rbTraning3N" value="N" checked="checked" /><label for="rbTraning3N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning3" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">4. 신결제/취소 방법 설명</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning4" id="rbTraning4Y" value="Y" /><label for="rbTraning4Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning4" id="rbTraning4N" value="N" checked="checked" /><label for="rbTraning4N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning4" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">5. 영수증/주문서 출력 방법 설명</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning5" id="rbTraning5Y" value="Y" /><label for="rbTraning5Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning5" id="rbTraning5N" value="N" checked="checked" /><label for="rbTraning5N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning5" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">6. 매출 확인 방법 설명</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning6" id="rbTraning6Y" value="Y" /><label for="rbTraning6Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning6" id="rbTraning6N" value="N" checked="checked" /><label for="rbTraning6N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning6" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">7. 마감처리 방법 설명</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning7" id="rbTraning7Y" value="Y" /><label for="rbTraning7Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning7" id="rbTraning7N" value="N" checked="checked" /><label for="rbTraning7N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning7" style="width:85%;" maxlength="5000" />
							</span>
						</div>
						<div style="height:30px;margin: 20px 0 2px 0; border-bottom:1px dashed black;">
							<label style="height:30px;font-size:14px;font-weight:bold;color:black;">8. 장애 접수 연락처 확인 및 안내(액정에 표기됨)</label>
						</div>
						<div style="border-bottom:1px dashed black;">
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;float:left">점검유무</label>
								<div class="div-radio" style="width:60px;margin-right:5px">
									<input type="radio" class="input-check" name="rbTraning8" id="rbTraning8Y" value="Y" /><label for="rbTraning8Y" class="label-rbInner">Y</label>
								</div>
								<div class="div-radio" style="width:60px;"">
									<input type="radio" class="input-check" name="rbTraning8" id="rbTraning8N" value="N" checked="checked" /><label for="rbTraning8N" class="label-rbInner">N</label>
								</div>
							</span>
							<span class="inpbox" style="width:100%;">
								<label style="width:14%;">특이사항</label>
								<input type="text" id="txbNoteTraning8" style="width:85%;" maxlength="5000" placeholder="단말기 액정 관리점 번호기재" />
							</span>
						</div>
					</div>
				</div>	
			</div>
			<div id="div6" class="scrollbox" style="display:none;">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom:15px">
                        <h4 style="color:black;border-bottom: 2px;border-bottom-style: ridge;">◈ 설치정보</h4>
						<span class="inpbox" style="width:100%">
							<label style="width:20%;height:30px;">매장명</label>
                            <asp:Label style="width:79%;height:30px;" ID="lblFranchiseNm2" runat="server" BackColor="#dedede"></asp:Label>
						</span>
						<span class="inpbox" style="width:100%">
							<label style="width:20%;height:30px;">확인자</label>
							<input type="text" id="txbCustNm" style="width:79%;" maxlength="50" />
						</span>
						<span class="inpbox" style="width:100%">
							<label style="width:20%;height:30px;">연락처(HP)</label>
                            <asp:Label style="width:79%;height:30px;" ID="lblPhoneNo" runat="server" BackColor="#dedede"></asp:Label>
						</span>
						<canvas id="cvSign" style="border:1px solid #000000;background-image:url('/Resource/Mobile_images/signBackground.png');background-size:100% 100%;width:100%;height:100%;"></canvas>
						<img id="imgSign" src="" style="border:1px solid black; width:100%; height:100%;display:none; background-color:white;" />
						<input type="hidden" id="hdfSignYn" />
					</div>
				</div>
			</div>
			<!--E: div -->
			<div style="text-align:center;margin-top:40px;">
                <input id="btnPrev" type="button" class="btn-gray" onclick="fn_Prev();" style="display:none;" value="이전"/>
                <input id="btnNext" type="button" class="btn-gray" onclick="fn_Next();" value="다음"/>
                <input id="btnConf" type="button" class="btn-green" onclick="fn_Confirm();" style="display:none;" value="확인"/>
			</div>
            <asp:HiddenField ID="hdfOrderNo" runat="server" />
		</div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>