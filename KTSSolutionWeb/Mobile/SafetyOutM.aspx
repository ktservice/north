﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="SafetyOutM.aspx.cs" Inherits="KTSSolutionWeb.SafetyOutM" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="../Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="../Resource/css/tabs.css?20240624" rel="stylesheet" />
	
    <style type="text/css">

        body {
            font-size: 12px;
        }

        .tabulator {
            font-size: 12px !important;
        }

        .tabulator .tabulator-footer .tabulator-paginator .tabulator-page {
            font-size: 12px !important;
        }

        /* CSS 코드 */
        .page {
          position: relative;
          padding-bottom: 10%; /* 16:9 비율에 맞게 설정 */
          height: 0;
        }

        .page > * {
          position: absolute;
          overflow: auto; /* 요소 내부에 스크롤 생성 */
          left: 0;
          width: 100%;
          height: 100vh;
          z-index: 1; /* 겹치는 순서를 결정 */
        }
        /*//*/

        .page > object {
          border: none; /* 기본적으로 iframe에 적용되는 테두리 제거 */
        }

        .fieldsetSpan1{
            width:100%;
            margin:3px 0 3px 0;
            font-size:0;
            display:flex !important;
            flex-direction: row;
            align-items: center;
        }
        

    </style>

        <script type="text/javascript">
            var form = document.getElementById("form1");
            form.setAttribute("enctype", "multipart/form-data");

            //document.addEventListener("DOMContentLoaded", function () {
            //    const frames = document.querySelectorAll(".frame");

            //    frames.forEach((frame) => {
            //        const src = frame.dataset.src;
            //        const iframe = document.createElement("iframe");
            //        iframe.src = src;
            //        frame.appendChild(iframe);
            //    });
            //});


            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var grid;
            var callGb;
            var dt;
            var selReq;
            var selChgReasonJson;

            var page = {
                init: function () {
                    fn_Init();
                    fn_GridInit();
                    fn_Load();

                    $('.tabnav a').eq(0).addClass('active');
                },
                eventbind: function () {
                    $("#selField").change(function (e) {
                        fn_Field();
                    });

                    $("#inputValue").keyup(function (e) {
                        var fieldEl = document.getElementById("selField");
                        var valueEl = document.getElementById("inputValue");

                        ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                    });

                    //$("#txbBaseDt").datepicker({ dateFormat: 'yy-mm-dd' });


                    $("#btnSelect").click(function (e) {
                        $('.tabnav a').eq(0).click();
                    });

                    $("#btnEmp").click(function (e) {
                        fn_Emp();
                    });


                    $('.tabnav a').click(function (e) {
                        $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                        $('.tabnav a').removeClass('active');
                        $(this).addClass('active');

                        if (this.text === '이력 조회') {
                            fn_Search('HIST', '', '', '', $("#selOut").val(), $("#<%= txtEmpNoS.ClientID %>").val());
                        }
                        else {
                            fn_Search('SEARCH', '', '', '', $("#selOut").val(), $("#<%= txtEmpNoS.ClientID %>").val());
                        }

                        return false;
                    });


                }
            };

            var fn_Init = function () {

            }


            var fn_Load = function () {
                fn_SetSelField();
            }

            var fn_SetSelField = function () {
                var obj1 = $('#selField');
                obj1.empty();
                obj1.append("<option value=''>선택</option>");
                obj1.append("<option value='ITEMNM'>품목</option>");
            }


            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                value = priceToString(value);
                return value;
            }

            var fn_CellReg = function (cell, formatterParams, onRendered) {
                var html = '';
                var saveYn = cell.getRow().getData().SAVEYN;
                cellNo = cell.getRow().getData().id;
                

                if ($("#hdnOut").val() == 'A') {
                    var btnNm = '등록';

                    if (saveYn == 'Y') btnNm = '변경';
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\", \"" + $("#hdnOut").val() + "\");' value='" + btnNm + "' class='btn-save' style='height:23px;line-height:0px;font-size:12px;margin-right:10px;'>";
                }
                else if ($("#hdnOut").val() == 'B') {
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\", \"" + $("#hdnOut").val() + "\");' value='보기' class='btn-save' style='height:23px;line-height:0px;font-size:12px;margin-right:10px;'>";
                }
                else if ($("#hdnOut").val() == 'C') {
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\", \"" + $("#hdnOut").val() + "\");' value='보기' class='btn-save' style='height:23px;line-height:0px;font-size:12px;margin-right:10px;'>";
                }
                else if ($("#hdnOut").val() == 'D') {
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\", \"" + $("#hdnOut").val() + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:12px;margin-right:10px;'>";
                }

                return html;
            }

            var fn_CellHist = function (cell, formatterParams, onRendered) {
                var html = '';
                cellNo = cell.getRow().getData().id;

                html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\", \"H\");' value='보기' class='btn-save' style='height:23px;line-height:0px;font-size:12px;margin-right:10px;'>";

                return html;
            }

            var fn_GridInit = function () {
                //field: "id" 명으로 주어야 UPDATE 됨.
                var Columns = [
                    { formatter: fn_CellReg, width: 46, minWidth: 30, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 60, resizable:false },
                    { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 140, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목순번", field: "ITEMSEQ", sorter: "number", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "사용용도코드", field: "USECD", sorter: "string", visible: false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "제품상태코드", field: "ITEMCD_STATUSCD", sorter: "string", visible: false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "제품상태", field: "ITEMNM_STATUSNM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "운용상태코드", field: "OPER_STATUSCD", sorter: "string", visible: false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "운용상태", field: "OPER_STATUSNM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "보유형태코드", field: "HOLD_TYPECD", sorter: "string", visible: false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "보유형태", field: "HOLD_TYPENM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "저장유무", field: "SAVEYN", sorter: "string", visible: false, resizable: false },
                ];

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            }

            var fn_HistGridInit = function () {
                //field: "id" 명으로 주어야 UPDATE 됨.
                var Columns = [
                    { formatter: fn_CellHist, width: 46, minWidth: 30, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 60, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "이력일자", field: "HIST_DATE", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "등록자명", field: "REG_EMPNM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "등록자사번", field: "REG_EMPNO", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 140, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "품목순번", field: "ITEMSEQ", sorter: "number", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "교체사유", field: "CHG_REASONNM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "요청구분", field: "REQ_GBNM", sorter: "string", width: 100, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "비고(교환)", field: "MEMO1", sorter: "string", visible:false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "이력순번", field: "HIST_SEQ", visible: false, resizable: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "저장유무", field: "SAVEYN", sorter: "string", visible: false, resizable: false },
                ];

                //컬럼 세팅
                ktsColumns = Columns;

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsHistGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            }

            var fn_Reg = function (id, outType) {
                
                fn_DetailMng(id, outType);

            };

            var detailParams = [];
            var fn_DetailMng = function (id, outType) {
                var row = ktsGrid.getRow(id);
                var rowData = row.getData();

                var vGiveDate = rowData.GIVE_DATE;
                var vItemCd = rowData.ITEMCD;
                var vItemSeq = rowData.ITEMSEQ;
                var vEmpno = rowData.EMPNO;
                var vSaveYn = rowData.SAVEYN;

                detailParams[0] = outType;
                detailParams[1] = vGiveDate;
                detailParams[2] = vItemCd;
                detailParams[3] = vItemSeq;
                detailParams[4] = vEmpno;
                detailParams[5] = vSaveYn;
                

                if (outType == 'H') {
                    detailParams[6] = rowData.HIST_SEQ;
                }
                else {
                    detailParams[6] = '';
                }

                var frames = document.querySelectorAll(".page");
                var src = '/Mobile/SafetyOutDetailMngM.aspx';
                var iObject = document.createElement("object");

                frames.forEach((frame) => {
                    iObject.data = src;
                    //iObject.width = "100%";
                    //iObject.height = "100%";
                    frame.appendChild(iObject);
                });

                $('.page').show();
                $('.page-clothes-request').hide();

            }

            var fn_DetailViewClose = function (searchYn) {
                $('.page').html('');
                $('.page').hide();
                $('.page-clothes-request').show();

                if (searchYn == 'Y') {
                    fn_Search('SEARCH', '', '', '', detailParams[0], detailParams[4]);
                }
            }


            var ajaxCallBack = function (json) {

                if (callGb == 'Emp') {

                    $("#<%= txtEmpNmS.ClientID %>").val('');

                    $.each(JSON.parse(json), function (idx, item) {
                        $("#<%= txtEmpNoS.ClientID %>").val(item.EMPNO);
                        $("#<%= txtEmpNmS.ClientID %>").val(item.EMPNM);
                    });

                    callGb = '';
                }
            }


            var fn_Field = function () {
                var fieldEl = document.getElementById("selField");
                var valueEl = document.getElementById("inputValue");
                valueEl.value = '';

                if (fieldEl.value == "") {
                    valueEl.setAttribute('disabled', 'disabled');
                    ktsGrid.setFilter('NUM', 'like', '');
                }
                else {
                    valueEl.removeAttribute('disabled');
                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                }
            }

            var fn_Emp = function () {
                callGb = 'Emp';

                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= txtEmpNoS.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetEmp');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

            var fn_Search = function (gubun, giveDate, itemCd, itemSeq, out, empNoS) {
                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = out;
                aData[2] = empNoS;
                aData[3] = giveDate;
                aData[4] = itemCd;
                aData[5] = itemSeq;
                aData[6] = '';

                $("#hdnOut").val($("#selOut").val());
                
                callGb = gubun;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                if (gubun == 'SEARCH') {

                    fn_GridInit();
                    fn_GridHeight('ktsGrid');
                    ktsRowCountClass = 'searchrowcount';

                    grid.getDataBind(jsonData, ajaxUrl);
                    $(".table-option").css('display', 'block');

                    $(".page-clothes-request").show();
                    $("#divSearchBox").show();
                    $(".datalist").show();
                    $(".page").hide();
                    $("#divImgView").hide();

                    $("#lblResultTitle").text($("#selOut option:selected").text());
                    
                }
                else if (gubun == 'HIST') {
                    fn_HistGridInit();
                    fn_GridHeight('ktsHistGrid');
                    ktsRowCountClass = 'histrowcount';
                    grid.getDataBind(jsonData, ajaxUrl);

                    $(".table-option").css('display', 'block');

                    $(".page-clothes-request").show();
                    $("#divSearchBox").show();
                    $(".datalist").show();
                    $(".page").hide();
                    $("#divImgView").hide();
                }
                
            }

            var fn_GridHeight = function (id) {
                var element = document.getElementById(id);
                var rect = element.getBoundingClientRect();
                var windowHeight = window.innerHeight;
                var windowWidth = window.innerWidth;
                var height = windowHeight - rect.top - 60;

                if (windowWidth > windowHeight) { //가로방향일 경우
                    ktsGrid.setHeight(''); //height 자동
                }
                else {
                    ktsGrid.setHeight(height + 'px');
                }
            }


        </script>

    <!-- S: contentsarea -->
    <div class="contentsarea">
        <div class="page-clothes-request">
            <h2>안전용품불출</h2>
            <div id="divSearchBox" style="border-bottom: 2px solid #dfdfdf;">
                <!-- S:fieldset -->
                <fieldset>
                    <ul style="margin: 3px 0 3px 0;">
                        <li>
                        <span class="inpbox fieldsetSpan1" id="spanEmpNo" runat="server" style="margin:3px 0 3px 0;font-size:0;">
                        <label style="margin: 0 !important; margin-left: 0;">사번</label>
                        <asp:TextBox ID="txtEmpNoS" runat="server" MaxLength="20" Style="width: 30vw !important;margin-left: 16px;"></asp:TextBox>
                        <asp:TextBox ID="txtEmpNmS" runat="server" MaxLength="50" Style="width: 30vw !important"></asp:TextBox>
                        <img id="btnEmp" src="/Resource/Mobile_images/icon_search.png" />
                    </span>
                        </li>
                        <li><span class="optionbox fieldsetSpan1" style="margin:3px 0 3px 0;font-size:0;">
                            <label style="margin: 0 !important; width:23px">구분</label>
                            <select id="selOut" style="width: 60vw !important;margin-left: 16px;">
                                <option selected="selected" value="A">불출대상</option>
                                <option value="B">불출제외대상</option>
                                <option value="C">불출완료</option>
                                <option value="D">임시저장</option>
                            </select>
                            <input id="btnSelect" type="button" class="btn-green fieldsetBtn" style="margin: 0 0 0 10px;border:none;color: #ffff !important;" value="조회" />
                            <input type="hidden" id="hdnOut" />
                        </span>
                        </li>
                    </ul>
                </fieldset>
                <!-- //E:fieldset -->
            </div>
            <!-- S:datalist -->
            <div class="datalist">
                <ul class="tabnav">
                    <li><a href="#tab01">안전용품 현황</a></li>
                    <li><a href="#tab02">이력 조회</a></li>
                </ul>
                <div class="tabcontent">
                    <div id="tab01">
                        <div class="list-top">
                            <strong><label id="lblResultTitle"></label> 조회 결과</strong>
                            <label class="searchrowcount"></label>
                        </div>
                        <div class="scrollbox">
                            <div class="table-search" style="display: none">
                                <span>
                                    <select id="selField" style="font-size:12px">
                                    </select>
                                    : 
                                    <input id="inputValue" style="font-size:12px;width:40vw" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                            <div id="gridButtom"></div>
                        </div>
                    </div>
                    <div id="tab02" style="display:none">
                        <div class="list-top">
                            <strong>이력 조회 결과</strong>
                            <label class="histrowcount"></label>
                        </div>
                        <div class="scrollbox">
                            <div id="ktsHistGrid"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- E:datalist -->
        </div>
        <div class="page" style="display:none">
        </div>
    </div>
    <!--//E: contentsarea -->
</asp:Content>