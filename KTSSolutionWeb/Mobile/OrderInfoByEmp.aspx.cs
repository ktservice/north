﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Order;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class OrderInfoByEmp : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    string strSumupdate = Request.Form["pSUMUPDATE"] == null ? "" : Request.Form["pSUMUPDATE"].ToString();

                    if (strSumupdate.Length < 8)
                    {
                        this.txbStDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                        this.txbEnDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        this.txbStDate.Value = strSumupdate.Substring(0, 4) + "-" + strSumupdate.Substring(4, 2) + "-" + strSumupdate.Substring(6, 2);
                        this.txbEnDate.Value = strSumupdate.Substring(0, 4) + "-" + strSumupdate.Substring(4, 2) + "-" + strSumupdate.Substring(6, 2);
                        GetDataList();
                    }
                }
            }
        }
        #endregion

        #region btnSelect_Click
        /// <summary>
        /// btnSelect_Click
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region paging_PreRender
        /// <summary>
        /// paging_PreRender
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult1);

                        for (int i = 0; i < rptResult1.Items.Count; i++)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult1.Items[i].FindControl("tr");
                            Label lblOrderNum = (Label)rptResult1.Items[i].FindControl("lblOrderNum");
                            Label lblWorkState = (Label)rptResult1.Items[i].FindControl("lblWorkState");
                            Label lblRcvType = (Label)rptResult1.Items[i].FindControl("lblRcvType");

                            string strOrderNo = lblOrderNum.Text;
                            string strWorkState = lblWorkState.Text;
                            string strRcvType = lblRcvType.Text;

                            if (strWorkState.Equals("2") && !strRcvType.Equals("K"))
                            {
                                tr.Attributes.Add("onClick", "fn_ModalOdrInfo('" + strOrderNo + "');");
                            }
                            else
                            {
                                tr.Attributes.Add("onClick", "fn_PageMoveDetail('" + strOrderNo + "', '" + strRcvType + "');");
                            }
                        }
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region GetDataList
        /// <summary>
        /// GetDataList
        /// </summary>
        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = Session["EMPNO"].ToString();
                string strStDate = this.txbStDate.Value.ToString().Replace("-", "");
                string strEnDate = this.txbEnDate.Value.ToString().Replace("-", "");
                string strOrderCode = this.ddlOrderCode.SelectedValue;

                paging.bPreRender = true;


                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    ds = mgmt.GetOrderStateInfoByEmp(strStDate, strEnDate, strOrderCode, strEmpNo);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetAccect(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERWORKSTATEUPDATE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataTable dt = new DataTable();

                try
                {
                    string strOrderNo = aData[0];

                    if (strOrderNo.Length == 0)
                    {
                        dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    }
                    else
                    {
                        //기존 오더 처리
                        using (OrderMgmt mgmt = new OrderMgmt())
                        {
                            mgmt.UpdateOrderNumWorkState(strOrderNo, "3", "", "");
                            dt = Utility.DataTableError("00", "", "");
                            result = Utility.DataTableToJson(dt);
                        }
                    }
                }
                catch
                {
                    dt = Utility.DataTableError("01", "작업수령 중 오류가 발생했습니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetNonAccect(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                try
                {
                    string strOrderNo = aData[0];
                    string strReason = aData[1];


                    if (strOrderNo.Length == 0 || strReason.Length == 0)
                    {
                        dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    }
                    else
                    {
                        using (OrderMgmt mgmt = new OrderMgmt())
                        {
                            mgmt.UpdateOrderNumWorkState(strOrderNo, "7", strReason, "");
                            dt = Utility.DataTableError("00", "", "");
                            result = Utility.DataTableToJson(dt);
                        }
                    }
                }
                catch
                {
                    dt = Utility.DataTableError("01", "작업수령 중 오류가 발생했습니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetTransferWorker(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANTRANSFERWORKER";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);//파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        #endregion
    }
}