﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.IO;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTSSolutionWeb
{
    public partial class SafetyOutDetailMngM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        DataSet SetDbFile(string strGubun, int strSeq, string strFileNm, byte[] bfile, string strFileType, int nFileSize)
        {
            DataSet ds = new DataSet();

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];
                parameters[0] = new MySqlParameter("@pGUBUN", MySqlDbType.VarChar, 1);
                parameters[0].Value = strGubun;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strSeq;
                parameters[2] = new MySqlParameter("@pFILENM", MySqlDbType.VarChar, 150);
                parameters[2].Value = strFileNm;
                parameters[3] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[3].Value = bfile;
                parameters[4] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[4].Value = strFileType;
                parameters[5] = new MySqlParameter("@pFILESIZE", MySqlDbType.VarChar, 20);
                parameters[5].Value = nFileSize;

                CommonData data = new CommonData("UP_SAFETYIMAGEFILES_TRANSACTION", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        void imageFileUpload(string fileId)
        {
            string result = string.Empty;

            DataSet ds = new DataSet();

            Stream stream = null;
            try
            {
                HttpPostedFile postedFIle = HttpContext.Current.Request.Files[fileId];

                stream = postedFIle.InputStream;
                string strFileNm = Path.GetFileName(postedFIle.FileName);
                string strFileType = postedFIle.ContentType;

                //type체크 필요 
                if (strFileType.Contains("image"))
                {
                    int nFileLen = Convert.ToInt32(stream.Length);

                    byte[] byteFile = new byte[nFileLen];

                    stream.Read(byteFile, 0, nFileLen);

                    ds = SetDbFile("I", -1, strFileNm, byteFile, strFileType, nFileLen);

                }

                stream.Close();
                stream.Dispose();

                if (fileId.Equals("fu1"))
                {
                    hdnFileSeq1.Value = ds.Tables[0].Rows[0]["SEQ"].ToString();
                    hdnFileSeqNm1.Value = ds.Tables[0].Rows[0]["FILENM"].ToString();
                    aFile1.InnerText = ds.Tables[0].Rows[0]["FILENM"].ToString();
                    pFileA1.Attributes.CssStyle.Add("display", "flex");
                    imgFile1.Attributes.CssStyle.Add("display", "block");
                    pFilebox1.Attributes.CssStyle.Add("display", "none");
                }
                else if (fileId.Equals("fu2"))
                {
                    hdnFileSeq2.Value = ds.Tables[0].Rows[0]["SEQ"].ToString();
                    hdnFileSeqNm2.Value = ds.Tables[0].Rows[0]["FILENM"].ToString();
                    aFile2.InnerText = ds.Tables[0].Rows[0]["FILENM"].ToString();
                    pFileA2.Attributes.CssStyle.Add("display", "flex");
                    imgFile2.Attributes.CssStyle.Add("display", "block");
                    pFilebox2.Attributes.CssStyle.Add("display", "none");
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string FileDownload(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYIMAGEFILE";
            string jobGb = "GET";
            string errCd = string.Empty;
            string errMsg = string.Empty;
            HttpContext context = HttpContext.Current;
            Page page = context.CurrentHandler as Page;

            DataSet ds = new DataSet();

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                (ds, errCd, errMsg) = dbTransaction.GetTableRow();
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    //SELECT FILENM, FILEDATA, FILETYPE, FILESIZE 
                    int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                    byte[] bFile = new byte[nFileLen];
                    bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];
                    string fileData = Convert.ToBase64String(bFile);
                    string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                    DataTable dt = new DataTable();
                    dt.Columns.Add("FILESIZE", typeof(int));
                    dt.Columns.Add("FILEDATA", typeof(string));
                    dt.Columns.Add("FILENM", typeof(string));
                    dt.Rows.Add(nFileLen, fileData, strFileNm);

                    result = Utility.DataTableToJson(dt);

                }

            }

            return result;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetTransaction(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYOUTMTRAN";
            string jobGb = "SET";
            string empNo = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCode(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CODECOLLECTION";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYOUTM";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        protected void btnFileAdd1_Click(object sender, EventArgs e)
        {
            try
            {
                imageFileUpload("fu1");
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnFileAdd2_Click(object sender, EventArgs e)
        {
            try
            {
                imageFileUpload("fu2");
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}