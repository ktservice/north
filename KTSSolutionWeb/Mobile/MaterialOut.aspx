﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="MaterialOut.aspx.cs" Inherits="KTSSolutionWeb.MaterialOut" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="../Resource/js/jsQR.js"></script>
    <script type="text/javascript" src="../Resource/js/kts_qr.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        @media(orientation: portrait)
        {
            .tabulator{
            height: 26vh !important;
            font-size: 12px !important;
            }

        }
        @media(orientation: landscape)
        {
            .tabulator{
            height: 50vh !important;
            font-size: 12px !important;
            }
        }

        textarea {
            min-height: 31px;
        }

        .inpbox input {
            line-height: 0px !important;
        }

        #canvas {
            width: 100%;
            height: 100%;
        }

        #jsQr {
            width: 100%;
            height: 100%;
            z-index: 999;
            position: absolute;
            text-align: center;
        }

        #jsQr > div {
            float: left;
            width: 98%;
            height: 98%;
        }

        div#output {
            background-color: #BDBEBD;
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        div#frame {
            border: 2px solid #005666;
            background-color: #FFFFFF;
            margin-left: 10px;
            margin-right: 10px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
            padding-bottom: 8px;
        }

        #container h2 {
            margin-bottom: 10px;
        }
        .topContent
        {
            border-bottom:2px solid #dfdfdf;
        }

        .listTd1 {
            min-width:15px !important;
            width:10px !important;
            padding:10px 5px 10px 0px !important;
            text-align: left !important;
        }

        .listTd2 {
            min-width:15px !important;
            width:40% !important;
            padding:10px 5px 10px 0px !important;
            text-align: left !important;
        }

        .listLabel {
            padding:0 !important;
            text-align:left !important;
        }

        .listTitle{
            margin-top:2px;
            margin-right:10px;
            float:left;
        }

        .listDiv{
            margin-top:10px;
        }

        .listTextBox{
            width:90px;
            height:25px;
            text-align:right;
        }

        .fieldsetSpan1{
            width:100%;
            margin:3px 0 3px 0;
            font-size:0;
        }

        .fieldsetSpan2{
            width:100%;
            margin:11px 0 11px 0;
            font-size:0;
            text-align:center;
        }

        .fieldsetLabel{
            margin:0 !important;
        }

        .fieldsetTextbox{
            width:54% !important;
        }

        .fieldsetBtnImg
        {
            max-width:65px !important;
            background:url('../Resource/Mobile_images/icon_camera.png') no-repeat 50% #009687 !important;
        }

        .fieldsetBtn
        {
            width:33px !important;
            margin:0 !important;
            color: #fff !important;
        }

        .transactionTd
        {
            min-width:15px !important;
            padding:10px 12px 10px 0px !important;
        }

        .transactionBtn
        {
            font-size: 13px !important;
        }

        .gridWInput1
        {
            width:96px !important;
            height:21px !important;
            margin-left: 14px !important;
        }

        .gridWInput2
        {
            width:221px;
            height:21px;
        }

        .gridWSel
        {
            width: 44px;
            height: 21px;
            float: left;
            font-size: 11px;
            padding: 0 10px 0 10px !important;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;
        var oldCntValue = '';
        var dt;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#txbBaseDt").datepicker({ dateFormat: 'yy-mm-dd' });

                $("#txbBaseDt").change(function (e) {
                    fn_Search(this.value);
                });
                
                $("#btnSelect").click(function (e) {
                    fn_Scan();
                });

                $("#btnSave").click(function (e) {
                    if (this.value == '등록') {
                        fn_Reg('Insert');
                    }
                    else {
                        fn_Reg('Modify');
                    }
                    
                });

                $("#btnDel").click(function (e) {
                    fn_Reg('Delete');
                });

                $("#btnCamera").click(function (e) {
                    fn_Camera();
                });

                $("#btnQrClose").click(function (e) {
                    qrClose();
                });

                $("#txtCnt").click(function (e) {
                    oldCntValue = this.value;
                    this.value = '';
                });

                $("#txtCnt").blur(function (e) {
                    if (numCheck(this.value)) {
                        ktsGridAlert(this.value + " 입력값은 숫자가 아닙니다.<br/>숫자를 입력하세요.");
                        oldCntValue = '';
                        this.value = '';
                        return false;
                    }
                    if (this.value == '') {
                        this.value = oldCntValue;
                    }
                });

            }
        };

        var fn_Init = function () {
            $("#jsQr").hide();
            $("#btnDel").hide();
            $("#btnSave").hide();
            $(".table-option").css('display', 'none');
        }

        var fn_ScanCtlInit = function () {
            $("#lblDate").text('');
            $("#lblItemNm").text('');
            $("#hdnItemCd").val('');
            $("#txtCnt").val('');
            $("#txtMemo").val('');
            $("#hdnStatus").val('');
            $("#lblUnit").text('');
            $("#hdnStatus").val('');
        }

        var fn_Load = function () {
            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = fn_Pad(today.getMonth() + 1, 2);  // 월
            var date = fn_Pad(today.getDate(), 2);  // 날짜
            var day = today.getDay();  // 요일
            dt = year + '-' + month + '-' + date;

            $("#txbBaseDt").val(dt);
            fn_Search(dt);
        }

        var fn_Pad = function (num, length) {
            var value = '' + num;
            while (value.length < length) {
                value = '0' + value;
            }
            
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellWorkCheck = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == 'Y') {
                cell.getRow().select();
            }

            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 60, frozen:true },
                { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "WORKDATE", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "right", title: "수량", field: "WORKCNT", sorter: "number", width: 80, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "대분류", field: "LVNM1", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "center", title: "중분류", field: "LVNM2", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "center", title: "소분류", field: "LVNM3", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업체크", field: "WORKCHECK", sorter: "string", visible: false, formatter: fn_CellWorkCheck }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, null); // div의 grid id 값
        }

        var fn_Scan = function () {

            if (fn_ScanChk()) {
                callGb = 'scan';

                fn_ScanCtlInit();

                //입력 파라미터
                var aData = [];
                aData[0] = dt;
                aData[1] = $("#txtBarcode").val();
                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetScan');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        var fn_Reg = function (btn) {

            if (fn_RegChk()) {
                callGb = 'reg';

                var status = 'I';

                if (btn == 'Delete') {
                    status = 'D';
                }
                else if (btn == 'Insert') {
                    status = 'I';
                }
                else {
                    status = 'M';
                }
                var memo = $("#txtMemo").val();
                memo = memo.replace(/\n/gi, "<br>");

                if (memo.length > 120) {
                    memo = memo.substr(0, 120);
                }

                //입력 파라미터
                var aData = [];
                aData[0] = dt;
                aData[1] = $("#txtBarcode").val();
                aData[2] = $("#hdnItemCd").val();
                aData[3] = $("#txtCnt").val();
                aData[4] = status;
                aData[5] = memo;

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetOutTransaction');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }

        };

        var fn_ScanChk = function () {

            var barcode = $("#txtBarcode").val();

            if (barcode.length == 0) {
                ktsGridAlert("바코드를 스캔하세요.");
                return false;
            }

            return true;
        }

        var fn_RegChk = function () {
            var cnt = $("#txtCnt").val();

            if (cnt.length == 0) {
                ktsGridAlert("수량을 입력하세요.");
                return false;
            }
            else {
                if (cnt == '0') {
                    ktsGridAlert("수량을 입력하세요.");
                    return false;
                }
            }

            return true;
        }

        var ajaxCallBack = function (json) {
            if (callGb == 'scan') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ITEMCD != null) {
                        if (item.ITEMCD != '') {
                            var memo = item.MEMO;

                            if (memo.indexOf('<br>') >= 0)
                            {
                                memo = memo.replace(/<br>/gi, "\n");
                            }

                            $("#lblDate").text($("#txbBaseDt").val());
                            $("#lblItemNm").text(item.ITEMNM);
                            $("#lblUnit").text(item.UNIT);
                            $("#hdnItemCd").val(item.ITEMCD);
                            $("#txtCnt").val(item.CNT);
                            $("#hdnStatus").val(item.STATUS);
                            $("#txtMemo").val(memo);

                            if (item.STATUS == 'I') {
                                $("#btnSave").val('등록');
                                $("#btnSave").show();
                                $("#btnDel").hide();
                            }
                            else {
                                $("#btnSave").val('변경');
                                $("#btnSave").show();
                                $("#btnDel").show();
                            }
                        }
                        else {
                            fn_ScanCtlInit();

                            $("#lblItemNm").text('해당 바코드번호의 정보가 존재하지 않습니다.');
                        }
                    }
                });

                $("#txbBaseDt").val(dt);
                fn_Search(dt);
            }
            else if (callGb == 'reg') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD != null) {
                        ktsGridAlert('정상 처리됐습니다.');
                        $("#btnSave").hide();
                        $("#btnDel").hide();
                    }
                });

                $("#txbBaseDt").val(dt);
                fn_Search(dt);
                fn_ScanCtlInit();
            }
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function (sDt) {
            //입력 파라미터
            var aData = [];
            aData[0] = sDt;
            aData[1] = dt; //workDate
            aData[2] = $("#hdnItemCd").val();
            
            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-option").css('display', 'block');
        }

        var fn_Camera = function () {
            $("#jsQr").show();

            video = document.createElement("video");
            canvasElement = document.getElementById("canvas");
            canvas = canvasElement.getContext("2d");
            loadingMessage = document.getElementById("loadingMessage");
            outputContainer = document.getElementById("output");
            outputMessage = document.getElementById("outputMessage");
            outputData = document.getElementById("txtBarcode");

            ktsQr('jsQr');
        }

        var qrCallBack = function (data) {
            var barcode = $("#txtBarcode").val();
            if (barcode.length > 19) {
                $("#txtBarcode").val(barcode.substring(0, 20));
            }
            fn_Scan();
        };

    </script>

    

	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div id="jsQr" style="display:none">
            <div>
                <div>
                    <img id="btnQrClose" src="../Resource/images/icon_close_02.png"/>
                </div>
                <div id="frame">
                    <div id="loadingMessage">
                        🎥 비디오 스트림에 액세스 할 수 없습니다<br />
                        카메라가 활성화되어 있는지 확인하십시오
                    </div>
                    <canvas id="canvas"></canvas>
                    <div id="output">
                        <div id="outputMessage">QR코드를 카메라에 노출시켜 주세요</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-clothes-request">
			<h2>자재불출</h2>
			<div class="topContent">
				<!-- S:fieldset -->
				<fieldset>
					<legend>조회조건</legend>
                    <span class="inpbox fieldsetSpan1">
						<label class="fieldsetLabel">바코드번호</label><br />
                        <input id="txtBarcode" type="text" class="fieldsetTextbox" maxlength="20"/>
                        <input id="btnCamera" type="button" class="btn-green fieldsetBtnImg" style="float: right;" />
                        <input id="btnSelect" type="button" class="btn-green fieldsetBtn" style="float: right;" value="조회" />
					</span>
                    <span class="fieldsetSpan2">
						
					</span>
				</fieldset>
				<!-- //E:fieldset -->
			</div>
            <!-- S: scrollBox -->
            <div class="scrollbox">
				<div class="requestlist">
					<strong class="listTitle">바코드정보</strong>
                    <div class="datalist listDiv">
                        <table>
                            <tbody>
                                <tr>
                                    <th class="listTd1">일자</th>
                                    <td class="listTd2">
                                        <span id="lblDate" class="listLabel" />
                                    </td>
                                    <th class="listTd1">수량</th>
                                    <td class="listTd2">
                                        <input id="txtCnt" type="text" class="listTextBox" maxlength="11" onkeypress="return OnlyNumber();" />
                                    </td>
                                </tr>
                                <tr>
                                    <th class="listTd1">품목</th>
                                    <td colspan="4" class="listTd1">
                                        <span id="lblItemNm" class="listLabel" style="display: inline-block; width: 80%;" />
                                    </td>
                                </tr>
                                <tr>
                                    <th class="listTd1">단위</th>
                                    <td colspan="4" class="listTd1">
                                        <span id="lblUnit" class="listLabel" style="display: inline-block; width: 80%;" />
                                    </td>
                                </tr>
                                <tr>
                                    <th class="listTd1">비고</th>
                                    <td colspan="4" class="listTd1">
                                        <textarea id="txtMemo" type="text" maxlength="120" rows="2"></textarea>
                                    </td>
                                </tr>
                                <tr id="trConfirm" runat="server">
                                    <td colspan="4" class="transactionTd">
                                        <input id="btnSave" type="button" value="등록" class="btn-save transactionBtn" />
                                        <input id="hdnItemCd" type="hidden" />
                                        <input id="hdnStatus" type="hidden" />
                                        <input id="btnDel" type="button" value="삭제" class="btn-del transactionBtn" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
				</div>
			     <!-- S:datalist -->
                <div class="table-search">
                    <span class="inpbox">
					    <label>기준일자</label>
                        <input name="txbBaseDt" type="text" readonly="readonly" id="txbBaseDt" class="gridWInput1" />
                    </span>
                    <br />
                    <span class="table-option">
                        <select id="selField" class="gridWSel">
                            <option value="">선택</option>
                            <option value="ORGNM">부서</option>
                            <option value="EMPNO">사번</option>
                            <option value="EMPNM">이름</option>
                            <option value="ITEMNM">품목</option>
                        </select>
                        :<input id="inputValue" disabled="disabled" type="text" class="gridWInput2"/>
                    </span>
                </div>
                <div id="ktsGrid"></div>
                <!-- E:datalist -->
            </div>
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>