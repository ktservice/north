﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="SeverancePayUserinfoM.aspx.cs" Inherits="KTSSolutionWeb.SeverancePayUserinfoM" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/notice_M.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
        .noticebox > .noticecontent1 {
            line-height: 1rem;
            position: relative;
            overflow: hidden;
            display: inline;
            color: #ffffff;
            margin: 5px 2px;
        }

        .noticecontent1 > .noticecontenttext1 {
            position: relative;
            top: 0;
            font-size: 13px;
            display: inline-flex;
            flex-direction: column;
            gap: 10px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
            }
        };

        var fn_Init = function () {
            //$("#lblContentTitle").text('CS 실적 조회');
        }

        var fn_Load = function () {
            fn_NoticeInfo();
        }

        var ajaxCallBack = function (json) {
            if (callGb == 'NOTICE') {
                $.each(JSON.parse(json), function (idx, item) {
                    $('.noticetitletext').html(replaceBrTag(item.TITLE));
                    $('.noticecontenttext1').html(replaceBrTag(item.CONTENTS));
                });

                callGb = '';

                fn_Data();
            }
            else if (callGb == 'DATA') {
                var jHeaderData = [];
                var jData = [];
                var jsonData = JSON.parse(json);
                var baseMon = '';
                var titleYn = '';
                var html = '';

                // jsonData[0]의 EMPNM을 제외한 값을 jHeaderData에 추가
                Object.entries(jsonData[0]).forEach(function ([key, value]) {
                    if (key !== "TITLEYN") {
                        jHeaderData.push(value);
                    }
                });

                // jsonData[1]의 EMPNM을 제외한 값을 jData에 추가
                Object.entries(jsonData[1]).forEach(function ([key, value]) {
                    if (key !== "TITLEYN") {
                        jData.push(value);
                    }
                });

                html += '<tbody>';
                for (var i = 0; i < jHeaderData.length; i++) {
                    html += '<tr>';
                    html += '<td class="table-cell border_right" style="font-weight: bold;">' + jHeaderData[i] + '</td>';
                    html += ' <td class="table-cell td_backwhite"><span>' + jData[i] + '</span></td>';
                    html += '</tr>';
                }
                html += '</tbody>';

                $('#tbl').html(html);

                baseMon = jsonData[1].BASEMON;
                titleYn = jsonData[1].TITLEYN;

                if (baseMon != '' && titleYn == 'Y') {
                    var titleObj = $("#<%= menu.getTitleId() %>");
                    var title = '';

                    title = titleObj.text();
                    title += ' [기준월:' + baseMon + ']';
                    titleObj.text(title);
                }

                callGb = '';
            }

        }

        function replaceBrTag(str) {
            if (str == undefined || str == null) {
                return "";
            }

            str = str.replace(/\r\n/ig, '<br>');
            str = str.replace(/\\n/ig, '<br>');
            str = str.replace(/\n/ig, '<br>');
            return str;
        }

        var fn_NoticeInfo = function () {
            callGb = 'NOTICE';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetNoticeInfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function () {
            callGb = 'DATA';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }
    </script>

	<!-- S: contentsarea -->
    <div class="contentsarea">
        <div>
            <!-- S:UserControlTitle -->
            <uc:menu ID="menu" runat="server" />
            <!-- E:UserControlTitle -->
            <div class="noticebox">
                <div class="noticetitle">
                    <span class="noticetitletext"></span>
                </div>
                <div class="noticecontent1">
                    <span class="noticecontenttext1"></span>
                </div>
            </div>
            <h2></h2>
            <!-- S: scrollBox -->
            <div class="scrollbox mobile-body">
                <fieldset class="view-data-list-body">
                    <div class="data-group">
                        <div id="divMyInfo">
                            <div class="total-data">
                                <table id="tbl" class="table table-bordered">
                                </table>
                            </div>
                            <h2></h2>
                        </div>
                    </div>
                </fieldset>
            </div>
            <!-- E: scrollBox -->
        </div>
    </div>
	<!--//E: contentsarea -->
</asp:Content>