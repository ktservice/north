﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class BoardViewM : PageBase
    {
        private string BoardId
        {
            get
            {
                if (ViewState["BoardId"] != null)
                    return ViewState["BoardId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["BoardId"] = value;
            }
        }

        private string BoardType
        {
            get
            {
                if (ViewState["BoardType"] != null)
                    return ViewState["BoardType"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["BoardType"] = value;
            }
        }

        private string WriterId
        {
            get
            {
                if (ViewState["WriterId"] != null)
                    return ViewState["WriterId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["WriterId"] = value;
            }
        }

        private string FileSeq
        {
            get
            {
                if (ViewState["FileSeq"] != null)
                    return ViewState["FileSeq"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["FileSeq"] = value;
            }
        }

        private string EMPNO
        {
            get
            {
                if (ViewState["EMPNO"] != null)
                {
                    return ViewState["EMPNO"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["EMPNO"] = value;
            }
        }

        private string AUTHID
        {
            get
            {
                if (ViewState["AUTHID"] != null)
                {
                    return ViewState["AUTHID"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["AUTHID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                initializeControls();

                if (!IsPostBack)
                {
                    GetDataList();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    BoardId = Request.Form["pBOARDID"] == null ? "" : Request.Form["pBOARDID"].ToString();
                    BoardType = Request.Form["pBOARDTYPE"] == null ? "" : Request.Form["pBOARDTYPE"].ToString();
                    WriterId = Request.Form["pWRITERID"] == null ? "" : Request.Form["pWRITERID"].ToString();
                    EMPNO = Request.Form["pEMPNO"] == null ? "" : Request.Form["pEMPNO"].ToString();
                    AUTHID = Request.Form["pAUTHID"] == null ? "" : Request.Form["pAUTHID"].ToString();
                }

                if (AUTHID.Equals("AUTH0001") || EMPNO.Equals(WriterId))
                {
                    this.btnRegBoard.OnClientClick = "javascript:BoardWrite('" + BoardId + "', '" + BoardType + "', '" + WriterId + "', '" + EMPNO + "', '" + AUTHID + "');";
                    this.btnDelBoard.Click += new EventHandler(BtnDelBoard_ServerClick);

                    this.btnRegBoard.Visible = true;
                    this.btnDelBoard.Visible = true;
                }
                else
                {

                    this.btnRegBoard.Visible = false;
                    this.btnDelBoard.Visible = false;
                }

                updPanelBtn.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();
            DataSet dsFile = new DataSet();

            try
            {
                if (BoardId != "")
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardList(BoardId, BoardType, "", "");
                        dsFile = Noti.GetBoardFile(BoardId, "");
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.sTitle.InnerText = ds.Tables[0].Rows[0]["TITLE"].ToString();
                            this.pWriterNm.InnerHtml = ds.Tables[0].Rows[0]["EMPNM"].ToString();
                            this.divContent.InnerHtml = "<p>" + ds.Tables[0].Rows[0]["CONTENTS"].ToString().Replace("\r\n", "<br/>") + " </p>";

                            if (dsFile.Tables.Count > 0)
                            {
                                if (dsFile.Tables[0].Rows.Count > 0)
                                {

                                    rptFile.DataSource = dsFile.Tables[0];
                                    rptFile.DataBind();

                                    for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                    {
                                        if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                        {
                                            HtmlImage ImgBoard = new HtmlImage();

                                            string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                            byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                            ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                            ImgBoard.Attributes.Add("style", "max-width:100%;height:auto;");

                                            spanImg.Controls.Add(ImgBoard);
                                            spanImg.Controls.Add(new LiteralControl("<br />"));
                                            spanImg.Controls.Add(new LiteralControl(dsFile.Tables[0].Rows[i]["FILENM"].ToString().Split('.')[0]));
                                            spanImg.Controls.Add(new LiteralControl("<br />"));
                                        }
                                    }

                                    if (this.Session.Count > 0)
                                    {
                                        divFileList.Visible = true;
                                    }
                                    else
                                    {
                                        divFileList.Visible = false;
                                    }
                                }
                                else
                                {
                                    divFileList.Visible = false;
                                }
                            }

                            GetReplyDataList();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "BoardRefrash", "BoardList();", true);
                        }
                    }
                }

                updPanelImg.Update();
                updPanel1.Update();

                updPanelFile.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (ds != null)
                    dsFile.Dispose();
            }
        }

        #region GetReplyDataList
        /// <summary>
        /// GetReplyDataList
        /// </summary>
        private void GetReplyDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                if (BoardId != "")
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardReplyList(BoardId);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        DataRow dr = dt.NewRow();

                        dt.Rows.Add(dr);

                        rptReply.DataSource = dt;
                        rptReply.DataBind();
                        for (int i = 0; i < rptReply.Items.Count; i++)
                        {
                            Label lblReEmpNo = (Label)rptReply.Items[i].FindControl("lblReEmpNo");
                            TextBox txbRePly = (TextBox)rptReply.Items[i].FindControl("txbRePly");
                            //HtmlInputText txbRePly = (HtmlInputText)rptReply.Items[i].FindControl("txbRePly");
                            HtmlGenericControl ddReply = (HtmlGenericControl)rptReply.Items[i].FindControl("ddReply");
                            Button btnReReg = (Button)rptReply.Items[i].FindControl("btnReReg");
                            Button btnReDel = (Button)rptReply.Items[i].FindControl("btnReDel");

                            if (lblReEmpNo.Text.Equals(EMPNO))
                            {
                                txbRePly.Visible = true;
                                ddReply.Visible = false;
                                btnReReg.Visible = true;
                                btnReDel.Visible = true;
                                btnReReg.Text = "수정";

                                AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
                                trigger1.ControlID = btnReReg.UniqueID;
                                trigger1.EventName = "Click";
                                updPanel1.Triggers.Add(trigger1);

                                AsyncPostBackTrigger trigger2 = new AsyncPostBackTrigger();
                                trigger2.ControlID = btnReDel.UniqueID;
                                trigger2.EventName = "Click";
                                updPanel1.Triggers.Add(trigger1);
                            }
                            else if (lblReEmpNo.Text.Equals(""))
                            {
                                txbRePly.Visible = true;
                                ddReply.Visible = false;
                                btnReReg.Visible = true;
                                btnReDel.Visible = false;
                                btnReReg.Text = "등록";

                                btnReReg.Attributes.Add("onClientClick", "return ReRegChk()");

                                AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
                                trigger1.ControlID = btnReReg.UniqueID;
                                trigger1.EventName = "Click";
                                updPanel1.Triggers.Add(trigger1);

                            }
                            else
                            {
                                txbRePly.Visible = false;
                                ddReply.Visible = true;
                                btnReReg.Visible = false;
                                btnReDel.Visible = false;
                            }
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region BtnDelBoard_ServerClick
        /// <summary>
        /// BtnDelBoard_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelBoard_ServerClick(object sender, EventArgs e)
        {
            try
            {
                using (Notice Noti = new Notice())
                {
                    Noti.DelBoardList(BoardId);
                    Noti.DelBoardFile(BoardId, "");
                    Noti.DelBoardReply(BoardId, "");
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "BoardDelete", "alert('게시글 삭제를 완료 했습니다.');BoardList();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnDownload_ServerClick
        /// <summary>
        /// BtnDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnDownload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlAnchor btnDownload = (HtmlAnchor)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                FileSeq = lblSeq.Text;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDownLoad", "FileDownload();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnFileDownload_ServerClick
        /// <summary>
        /// BtnFileDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnFileDownload_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                if (FileSeq.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다..');window.close();", true);
                }
                else
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardFile(BoardId, FileSeq);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                            byte[] bFile = new byte[nFileLen];
                            bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                            string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                            using (FileUtil fileUtil = new FileUtil())
                            {
                                fileUtil.FileDownLoad_M(this, bFile, strFileNm, nFileLen);

                                string strEmpNo = Page.Session["EMPNO"].ToString();
                                string strFile = "";
                                string strLoginType = Page.Session["LOGINTYPE"].ToString();


                                if (strLoginType.Equals("mkate"))
                                {
                                    string strFileOnlyNm = "";
                                    for (int i = 0; i < strFileNm.Split('.').Length - 1; i++)
                                    {
                                        if (strFileOnlyNm.Length > 0)
                                        {
                                            strFileOnlyNm+= ".";
                                        }

                                        strFileOnlyNm += strFileNm.Split('.')[i];
                                    }
                                    strFile = strFileNm + "||https://bss.ktservice.co.kr/FileDownload/" + strEmpNo + "/" + strFileNm;
                                }
                                else
                                {
                                    strFile = "/FileDownload/" + strEmpNo + "/" + strFileNm;
                                }

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDownload", "MoblieFileDownload('" + strFile + "', '" + strLoginType + "');", true);
                            } 
                        }
                    }

                    FileSeq = "";
                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region btnReReg_ServerClick
        /// <summary>
        /// btnReReg_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReReg_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnReReg = (Button)sender;
                Label lblReSeq = (Label)btnReReg.Parent.FindControl("lblReSeq");
                Label lblReEmpNo = (Label)btnReReg.Parent.FindControl("lblReEmpNo");
                TextBox txbRePly = (TextBox)btnReReg.Parent.FindControl("txbRePly");
                //HtmlInputText txbRePly = (HtmlInputText)btnReReg.Parent.FindControl("txbRePly");
                HtmlGenericControl ddReply = (HtmlGenericControl)btnReReg.Parent.FindControl("ddReply");

                using (Notice Noti = new Notice())
                {
                    if (lblReEmpNo.Text.Equals(""))
                    {
                        Noti.InsBoardReply(BoardId, txbRePly.Text, EMPNO);
                        GetReplyDataList();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Insert", "alert('댓글을 등록했습니다.');", true);
                    }
                    else
                    {
                        if (txbRePly.Visible)
                        {
                            Noti.UpdBoardReply(BoardId, lblReSeq.Text, txbRePly.Text);
                            GetReplyDataList();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Update", "alert('댓글이 수정되었습니다.');", true);
                        }
                        else
                        {
                            ddReply.Visible = false;
                            txbRePly.Visible = true;

                            btnReReg.Attributes.Add("onclick", "return ReRegChk()");

                            updPanel1.Update();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnReDel_ServerClick
        /// <summary>
        /// btnReDel_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReDel_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnReDel = (Button)sender;
                Label lblReSeq = (Label)btnReDel.Parent.FindControl("lblReSeq");

                using (Notice Noti = new Notice())
                {
                    Noti.DelBoardReply(BoardId, lblReSeq.Text);
                }

                GetReplyDataList();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('댓글이 삭제되었습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion
    }
}