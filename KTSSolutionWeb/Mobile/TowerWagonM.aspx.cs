﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class TowerWagonM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }

            if (!IsPostBack)
            {                
                SetPageInit();
            }
        }

        private void SetPageInit()
        {
            try
            {                
                string strRegYn = Request.Form["pREGYN"] == null ? "" : Request.Form["pREGYN"].ToString();

                if (strRegYn.Equals("Y"))
                {
                    string strStDate = Request.Form["pSTDATE"] == null ? "" : Request.Form["pSTDATE"].ToString();
                    string strEnDate = Request.Form["pENDATE"] == null ? "" : Request.Form["pENDATE"].ToString();
                    string strEmpNm = Request.Form["pEMPNM"] == null ? "" : Request.Form["pEMPNM"].ToString();
                    string strCarNo = Request.Form["pCARNO"] == null ? "" : Request.Form["pCARNO"].ToString();
                    this.txbStDt.Text = strStDate;
                    this.txbEnDt.Text = strEnDate;
                    this.txbEmpNm.Text = strEmpNm;
                    this.txbCarNo.Text = strCarNo;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Search", "searchBtnClick();", true);
                }
                else
                {
                    this.txbStDt.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                    this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }

                string authId = HttpContext.Current.Session["AUTHID"].ToString();

                if (this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                    || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                {
                    this.txbEmpNm.Text = "";
                    this.hfEmpNo.Value = "";
                    this.txbEmpNm.ReadOnly = false;
                }
                else
                {
                    this.txbEmpNm.Text = Session["EMPNM"].ToString();
                    this.hfEmpNo.Value = Session["EMPNO"].ToString();
                    this.txbEmpNm.ReadOnly = true;
                }

                string strMonth = this.txbEnDt.Text.Replace("-", "").Substring(0, 6);

                this.hfOrgCd.Value = Session["ORGCD"].ToString();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOWERWAGONLOGINFOFORMOBLIE";
            string jobGb = "GET";

            string orgcd = HttpContext.Current.Session["ORGCD"].ToString();

            aData.Add(orgcd); //파라미터 추가 시
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelTowerWagonLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOWERWAGONLOGDELETE";
            string jobGb = "SET";

            //aData[0] = rowData.LOGDATE;
            //aData[1] = rowData.CARID;
            //aData[2] = rowData.LOGID;

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}