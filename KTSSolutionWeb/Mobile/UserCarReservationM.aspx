﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="UserCarReservationM.aspx.cs" Inherits="KTSSolutionWeb.UserCarReservationM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .optionbox {
            width:100%;
            margin:3px 0 3px 0 !important; 
            font-size:0;
            display: inline-block;
            position: relative;
            vertical-align: middle;
        }

        .inpbox {
            width:50%;
            margin:3px 0 3px 0 !important; 
            font-size:0;
            display: inline-block;
            position: relative;
            vertical-align: middle;
        }

        .inpbox .txbDisabled {
            width:95%;
            background-color:#fff !important;
            border:0;
            color:black !important;
        }

        #divModal1 {
            display:none;
            position:absolute;
            width:100%;
            height:100%;
            top:67px;
            left:0;
            background:#edf1f1;
            overflow:hidden;
            z-index:99;
        }

        .ui-datepicker {
            width:100%;
            height:235px;
            overflow:hidden;
            margin:2px 0 0 0;
            padding:0;
            background:#fff;
            border-radius:0;
            box-shadow:2px 2px 4px 2px rgba(0,0,0,0.1)
        }

        .ui-icon {
            font-size:15px;
        }
        
        .ui-datepicker .ui-datepicker-calendar tbody td .ui-state-active {
            background:#0eaba7;
            border-radius:50%;
            color:#fff;
        }

        .div-TimeLine {
            width:100%;
            min-height:100%;
            max-height:100%;
            padding:20px;
            overflow:hidden;
            float:right;
            border:none;
        }

        .div-TimeLine .div-ReserveY {
            width:97%;
            height:60px;
            border:1px solid #d5e8f5;
            padding:5px;
            border-radius:10px;
            background-color:#e2e2e2;
            margin-bottom:5px;
        }

        .div-TimeLine .div-ReserveN {
            width:97%;
            height:60px;
            border:1px solid #d5e8f5;
            padding:5px;
            border-radius:10px;
            background-color:#f9fcfd;
            margin-bottom:5px;
        }

        .div-TimeLine .div-ReserveChk {
            width:97%;
            height:60px;
            border:1px solid #d5e8f5;
            padding:5px;
            border-radius:10px;
            background-color:#0eaba7;
            margin-bottom:5px;
        }

        input[type="button"]{
            margin:0 !important;
            border-style:none;
        }

        .div-Screenlock {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            background-color: #ffffff !important;
            z-index: 99;
            opacity: 0.7;
            overflow: hidden;
            display:none;
        }

        .div-modal {
            position: absolute;
            width: 94%;
            height: 240px;
            top: 30%;
            left: 3%;
            background: rgb(255, 255, 255);
            border-radius: 10px;
            border: 1px solid gray;
            overflow: hidden;
            z-index:100;
        }

        .div-modal-inner {
            width:95%;
            text-align:center;
            margin-left:5%;
        }

        .div-modal-content {
            margin-top:15px;
            margin-bottom: 15px;
        }

        .div-modal-content > p {
            padding-top: 5px;
            font-weight:bold;
            text-align:left;
        }
    </style>
    
    <script type="text/javascript">  
        var callGb;

        $(document).ready(function () {

            $(function () {
                $("#datepicker").datepicker({
                    dateFormat: "yy-mm-dd",
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    showOn: "both",
                    showOtherMonths: false,
                    selectOtherMonths: false,
                    constrainInput: false,
                    prevText: "<",
                    nextText: ">",
                    onSelect: function (dateText) {
                        $("#<%= txbDate.ClientID %>").datepicker("setDate", $(this).datepicker("getDate"));
                        fn_Search();
                    }
                });
            });

            $(function () {
                $("#<%= txbDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" });
            });

            $("#<%= ddlCarNo.ClientID %>").change(function (e) {
                fn_Search();
            });
            
            $("#btnResvInfo").click(function (e) {

                var carId = $("#<%= ddlCarNo.ClientID %>").val();
                var resvDt = $("#<%= txbDate.ClientID %>").val();

                if (carId != "" && resvDt != "") {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 0);

                    $('html, body').css("overflow-y", "hidden");

                    var nHeight = window.screen.height;
                    var nHeight2 = 0;

                    var display = $("#spanReason").css('display');

                    if (display == 'block') {
                        nHeight2 = nHeight - 266.5;
                    }
                    else {
                        nHeight2 = nHeight - 226.5;
                    }

                    $('#divTimeLine').css("height", nHeight2);
                    $('#divTimeLine').css("max-height", nHeight2);
                    $('#divTimeLine').css("min-height", nHeight2);

                    $("#divModal1").show();

                    fn_GetResvInfo();
                }
                else {
                    alert("공용차량과 예약일을 선택해주세요.");
                }
            });

            $("#btnConfirm").click(function (e) {
                fn_ShowResvinfo();
            });

            $("#btnReg").click(function (e) {
                fn_SetResvinfo();
            });
        });

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetCarInfo') {
                    callGb = '';

                    if (fn_dateChk()) {
                        $("#spanReason").show();
                        $("#btnConfirm").show();
                    }
                    else {
                        $("#spanReason").hide();
                        $("#btnConfirm").hide();
                    }

                    fn_ControlBind_Carinfo(json);
                }
                else if (callGb == 'GetResvInfo') {
                    callGb = '';
                    //임시
                    fn_ControlBind_TimeLine(json);
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'InsOk') {
                            callGb = '';
                            alert('차량예약이 완료되었습니다.');

                            fn_Modal1Close();
                            fn_Modal2Close();
                            $("#<%= ddlCarNo.ClientID %>").change();
                        }
                        else if (callGb == 'DelOk') {
                            callGb = '';
                            alert('차량예약이 취소되었습니다.');

                            fn_Modal1Close();
                            $("#<%= ddlCarNo.ClientID %>").change();
                        }
                    }
                    else {
                        alert(item.ERRMSG);
                    }
                });
            }
        };

        var fn_Search = function () {
            $("#txbCarId").val('');
            $("#txbMgmtStatus").val('');
            $("#txbCarTypeNm").val('');
            $("#txbModelNm").val('');
            $("#txbNotice").val('');
            $("#txbNotice2").val('');
            $("#<%= txbReason.ClientID %>").val('');
            $("#<%= hdfStTime.ClientID %>").val('');
            $("#<%= hdfEnTime.ClientID %>").val('');

            $("#divTimeLine").empty();

            fn_GetCarInfo();
        };

        var fn_Modal1Close = function () {
            $('html, body').css("overflow-y", "auto");
            $("#divModal1").hide();
        };

        var fn_Modal2Close = function () {
            $("#pConfirm").text('');
            $("#divScreenLock").hide();
            $("#divModal2").hide();
        };

        var fn_GetCarInfo = function () {
            var carId = $("#<%= ddlCarNo.ClientID %>").val();

            if (carId != "") {
                var aData = [];
                aData[0] = carId;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarInfo');

                callGb = 'GetCarInfo';

                ajaxCall(jsonData, ajaxUrl, false);
            }
            else {
                $("#spanReason").hide();
                $("#btnConfirm").hide();
            }
        };

        var fn_GetResvInfo = function () {
            $("#<%= hdfStTime.ClientID %>").val('');
            $("#<%= hdfEnTime.ClientID %>").val('');

            var carId = $("#<%= ddlCarNo.ClientID %>").val();
            var resvDt = $("#<%= txbDate.ClientID %>").val();

            if (carId != "" && resvDt != "") {
                var aData = [];
                aData[0] = carId;
                aData[1] = resvDt;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetResvInfo');

                callGb = 'GetResvInfo';

                ajaxCall(jsonData, ajaxUrl, false);
            }
            else {
                $("#spanReason").hide();
                $("#btnConfirm").hide();
            }
        };

        var fn_ShowResvinfo = function () {
            var carId = $("#<%= ddlCarNo.ClientID %>").val();
            var carNo = $("#<%= ddlCarNo.ClientID %> option:selected").text();
            var resvReason = $("#<%= txbReason.ClientID %>").val();
            var resvDt = $("#<%= txbDate.ClientID %>").val();
            var StTime = $("#<%= hdfStTime.ClientID %>").val();
            var EnTime = $("#<%= hdfEnTime.ClientID %>").val();

            if (carId == "") {
                alert("예약할 차량을 선택해주세요.");
            }
            else if (resvDt == "") {
                alert("예약일을 선택해주세요.");
            }
            else if (resvReason == "") {
                alert("예약사유를 입력해주세요.");
            }
            else if (StTime == "") {
                alert("예약시간을 선택해주세요.");
            }
            else {
                $("#divScreenLock").show();
                $("#divModal2").show();

                $("#pConfirm").text(carNo + ' 차량의 사용 예약을 하시겠습니까?');
            }
        };

        var fn_SetResvinfo = function () {
            var carId = $("#<%= ddlCarNo.ClientID %>").val();
            var carNo = $("#<%= ddlCarNo.ClientID %> option:selected").text();
            var resvReason = $("#<%= txbReason.ClientID %>").val();
            var resvDt = $("#<%= txbDate.ClientID %>").val();
            var StTime = $("#<%= hdfStTime.ClientID %>").val();
            var EnTime = $("#<%= hdfEnTime.ClientID %>").val();

            if (carId == "" || resvDt == "" || resvReason == "" || StTime == "") {
                alert("잘못된 접근입니다.");
            }
            else {
                if (EnTime == "") {
                    EnTime = StTime;
                }

                callGb = 'InsOk';

                var aData = [];

                aData[0] = carId;
                aData[1] = resvReason;
                aData[2] = resvDt;
                aData[3] = StTime;
                aData[4] = EnTime;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'InsResvinfo');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        };

        var fn_DelResvInfo = function (resvTime) {
            var carId = $("#<%= ddlCarNo.ClientID %>").val();
            var carNo = $("#<%= ddlCarNo.ClientID %> option:selected").text();
            var resvDt = $("#<%= txbDate.ClientID %>").val();
            var empNo = $("#<%= hdfEmpNo.ClientID %>").val();
            var resvEmpNo = $("#hdf_" + resvTime.replace(":", "_")).val();

            if (empNo != resvEmpNo || resvEmpNo == "" || empNo == "") {
                alert("잘못된 접근입니다.");
                return false;
            }
            else {
                if (confirm(carNo + ' 차량의 사용 예약을 취소하시겠습니까?')) {
                    callGb = 'DelOk';

                    var aData = [];

                    aData[0] = carId;
                    aData[1] = resvDt;
                    aData[2] = resvTime;
                    aData[3] = resvEmpNo;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'DelResvinfo');

                    ajaxCallEx(jsonData, ajaxUrl, true);

                }
            }
        };

        var fn_SetResvTime = function (resvTime) {
            var StTime = $("#<%= hdfStTime.ClientID %>");
            var EnTime = $("#<%= hdfEnTime.ClientID %>");

            if (EnTime.val() != "") {
                StTime.val('');
                $("#divTimeLine").find(".div-ReserveChk").each(function (index, item) {
                    $(item).removeClass();
                    $(item).addClass("div-ReserveN");
                });
            }
            if (StTime.val() == "") {
                EnTime.val('');
                StTime.val(resvTime);

                $("#div_" + resvTime.replace(":", "_")).removeClass();
                $("#div_" + resvTime.replace(":", "_")).addClass("div-ReserveChk");
            }
            else {
                if (StTime.val() > resvTime) {
                    $("#div_" + StTime.val().replace(":", "_")).removeClass();
                    $("#div_" + StTime.val().replace(":", "_")).addClass("div-ReserveN");

                    $("#div_" + resvTime.replace(":", "_")).removeClass();
                    $("#div_" + resvTime.replace(":", "_")).addClass("div-ReserveChk");

                    StTime.val(resvTime);
                    EnTime.val('');
                }
                else if (StTime.val() == resvTime) {
                    EnTime.val(resvTime);
                }
                else {
                    var startTime = new Date();
                    var endTime = new Date();

                    let sh = parseInt(StTime.val().split(':')[0]);
                    let sm = parseInt(StTime.val().split(':')[1]);
                    let rh = parseInt(resvTime.split(':')[0]);
                    let rm = parseInt(resvTime.split(':')[1]);

                    startTime.setHours(sh, sm, 0, 0);
                    endTime.setHours(rh, rm, 0, 0);

                    var bResvChk = true;

                    while (startTime <= endTime) {
                        let hour = String(startTime.getHours()).padStart(2, '0');
                        let minute = String(startTime.getMinutes()).padStart(2, '0');

                        let divId = "div_" + hour + "_" + minute;

                        var classNm = $("#" + divId).attr("class");

                        if (classNm == "div-ReserveY") {
                            bResvChk = false;
                        }

                        $("#" + divId).removeClass();
                        $("#" + divId).addClass("div-ReserveChk");

                        startTime.setMinutes(startTime.getMinutes() + 30);
                    }

                    if (bResvChk) {
                        EnTime.val(resvTime);
                    }
                    else {
                        $("#divTimeLine").find(".div-ReserveChk").each(function (index, item) {
                            var id = $(item).attr("id");
                            var bResv = $("#" + id.replace("div_", "hdf_")).val() != "" ? true : false;

                            $(item).removeClass();

                            if (bResv) {
                                $(item).addClass("div-ReserveY");
                            }
                            else {
                                $(item).addClass("div-ReserveN");
                            }
                        });

                        EnTime.val('');
                        StTime.val(resvTime);

                        $("#div_" + resvTime.replace(":", "_")).removeClass();
                        $("#div_" + resvTime.replace(":", "_")).addClass("div-ReserveChk");
                    }
                }
            }
        };

        var fn_ControlBind_Carinfo = function (json) {
            $.each(JSON.parse(json), function (idx, item) {
                $("#txbCarId").val(item.CARID);
                $("#txbMgmtStatus").val(item.MGMTSTATUS);
                $("#txbCarTypeNm").val(item.CARTYPENM);
                $("#txbModelNm").val(item.MODELNM);
                $("#txbNotice").val(item.NOTICE);
                $("#txbNotice2").val(item.NOTICE2);
            });
        };

        var fn_ControlBind_TimeLine = function (json) {
            var empNo = $("#<%= hdfEmpNo.ClientID %>").val();

            var startTime = new Date();
            var endTime = new Date();

            startTime.setHours(0, 0, 0, 0);
            endTime.setHours(23, 30, 0, 0);

            while (startTime <= endTime) {
                let hour = String(startTime.getHours()).padStart(2, '0');
                let minute = String(startTime.getMinutes()).padStart(2, '0');

                var div = document.createElement("div");

                div.className = "div-ReserveN"; //예약시 "div-ReserveY"
                div.setAttribute("id", "div_" + hour + "_" + minute);

                if (fn_dateChk()) {
                    div.setAttribute("onClick", "fn_SetResvTime('" + hour + ":" + minute + "');"); //예약시작 종료시간 세팅
                }

                var divTime = document.createElement("div");
                divTime.setAttribute("id", "divTime_" + hour + "_" + minute);
                divTime.className = "div-Time";
                divTime.innerText = hour + ":" + minute;

                var divReason = document.createElement("div");
                divReason.setAttribute("id", "divReason_" + hour + "_" + minute);

                var hidden = document.createElement("input");

                hidden.setAttribute("id", "hdf_" + hour + "_" + minute);
                hidden.type = "hidden";

                div.append(divTime);
                div.append(divReason);
                div.append(hidden);

                $("#divTimeLine").append(div);

                startTime.setMinutes(startTime.getMinutes() + 30);
            }

            $.each(JSON.parse(json), function (idx, item) {
                var div = $("#divTimeLine").find("#div_" + item.RESVTIME.replace(":", "_"))[0];
                var divReason = $("#divTimeLine").find("#divReason_" + item.RESVTIME.replace(":", "_"))[0];
                var hdfResvEmpNo = $("#divTimeLine").find("#hdf_" + item.RESVTIME.replace(":", "_"))[0];

                $(div).removeClass();
                $(div).addClass("div-ReserveY");
                $(div).removeAttr("onClick");

                $(hdfResvEmpNo).val(item.EMPNO);

                if (empNo == item.EMPNO && item.LOGYN == "N") {
                    $(div).attr("onClick", "fn_DelResvInfo('" + item.RESVTIME + "');"); //예약취소
                }

                if (item.RESVIDX == 1) {
                    $(divReason).html("- 예약자 : " + item.EMPNM + "(" + item.EMPNO + ")");
                }
            });
        };

        var fn_dateChk = function () {
            var bChk = false;

            var date = $("#<%= txbDate.ClientID %>").val();

            var now = new Date();
            var year = now.getFullYear();
            var month = String(now.getMonth() + 1).padStart(2, '0');
            var day = String(now.getDate()).padStart(2, '0');
            var today = year + "-" + month + "-" + day;

            if (date >= today) {
                bChk = true;
            }

            return bChk;
        };
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div class="page-clothes-request">
		    <h2>공용차량예약</h2>
			<div>                
		        <fieldset>
				    <legend>예약차량정보</legend>
                    <span class="optionbox">
				        <label>공용차량</label><br />
                        <asp:DropDownList ID="ddlCarNo" runat="server" AutoPostBack="false" style="width:50%"></asp:DropDownList>
			        </span>
                    <hr class="blank" />
                    <span class="inpbox">
				        <label style="width:80px;">차대번호</label><br />
                        <input class="txbDisabled" type="text" id="txbCarId" disabled="disabled" />
			        </span>
                    <span class="inpbox">
				        <label style="width:120px;">운용관리상태</label><br />
                        <input class="txbDisabled" type="text" id="txbMgmtStatus" disabled="disabled" />
			        </span>
                    <hr class="blank" />
                    <span class="inpbox">
				        <label style="width:80px;">차종</label><br />
                        <input class="txbDisabled" type="text" id="txbCarTypeNm" disabled="disabled"/>
			        </span>
                    <span class="inpbox">
				        <label style="width:80px;">모델</label><br />
                        <input class="txbDisabled" type="text" id="txbModelNm" disabled="disabled"/>
			        </span>
                    <hr class="blank" />
                    <span class="inpbox" style="width:100%">
				        <label style="width:80px;">공지</label><br />
                        <input class="txbDisabled" type="text" id="txbNotice" style="width:100%;" disabled="disabled"/>
			        </span>
                    <hr class="blank" />
                    <span class="inpbox" style="width:100%">
				        <label style="width:80px;">비고</label><br />
                        <input class="txbDisabled" type="text"id="txbNotice2" style="width:100%;" disabled="disabled"/>
                    </span>
                    <hr class="blank" />
                    <span class="inpbox" style="width:100%">
				        <label style="width:80px;">예약일</label><br />
                        <asp:TextBox class="txbDisabled" ID="txbDate" runat="server" style="display:none;"></asp:TextBox>
                        <div id="datepicker"></div>
                    </span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input type="button" class="btn-green" id="btnResvInfo" value="예약정보" />
					</span>
                    <asp:HiddenField ID="hdfStTime" runat="server" />
                    <asp:HiddenField ID="hdfEnTime" runat="server" />
                    <asp:HiddenField ID="hdfEmpNo" runat="server" />
                </fieldset>
			</div>
        </div>			
	</div>
	<!--//E: contentsarea -->
    <!-- S: divModal1 -->
	<div id="divModal1" class="scrollbox">
        <div class="div-TimeLine">
		    <fieldset>
                <span id="spanReason" class="inpbox" style="display:none;width:100%">
				    <label>예약사유</label><br />
                    <asp:TextBox ID="txbReason" runat="server" style="width:75% !important;" MaxLength="200"></asp:TextBox>
                    <input id="btnConfirm" type="button" class="btn-green" value="예약" style="width:33px;display:none;color:#fff;float:right;" />
                </span>
            </fieldset>
            <hr class="blank" />
            <div id="divTimeLine" style="overflow-x:hidden;overflow-y:auto">
            </div>
		    <fieldset>
			    <span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
			        <input id="btnModal1Close" type="button" class="btn-black" onclick="fn_Modal1Close();" style="width:33px;" value="이전" />
			    </span>
            </fieldset>
        </div>
	</div>		
	<!--E: divModal1 -->
    <div id="divScreenLock" class="div-Screenlock"></div>
    <!-- S: divModal2 -->
	<div id="divModal2" class="div-modal" style="display:none;">
        <!-- S:popcontents -->
        <div class="div-modal-inner">
            <div class="div-modal-content">
                <p>1. FMS 장착된 공용차량 운행 시 개인위치정보 수집 이용에 동의합니다.</p>
                <p style="margin-bottom:20px">2. 공용차량 운행 시 부과된 과태료, 면책금 개인납부에 동의합니다.</p>
                <p id="pConfirm" style="text-align:center;"></p>
            </div>
            <!-- S:btncenter -->
		    <fieldset>
			    <span style="width:100%; margin:0; font-size:0; text-align:center;">
                    <input id="btnReg" type="button" class="btn-green" style="margin-right:3px !important" value="예약" />
                    <input id="btnModal2Close" type="button" class="btn-black" onclick="fn_Modal2Close();" value="닫기" />
			    </span>
            </fieldset>
            <!-- //E:btncenter -->
        </div>
	</div>
    <!-- E: divModal2 -->
</asp:Content>