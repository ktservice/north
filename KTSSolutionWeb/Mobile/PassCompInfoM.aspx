﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PassCompInfoM.aspx.cs" Inherits="KTSSolutionWeb.PassCompInfoM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">  
		function DateType(obj) {
			if (obj == "M") {
                $("#<%=rbMonth.ClientID %>").attr("checked", true);
				$("#<%=rbDate.ClientID %>").attr("checked", false);

                $("#<%=txbStMonth.ClientID %>").show();
				$("#<%=txbEnMonth.ClientID %>").show();

                $("#<%=txbStDate.ClientID %>").hide();
				$("#<%=txbEnDate.ClientID %>").hide();
			}
            else {
                $("#<%=rbMonth.ClientID %>").attr("checked", false);
				$("#<%=rbDate.ClientID %>").attr("checked", true);

                $("#<%=txbStMonth.ClientID %>").hide();
				$("#<%=txbEnMonth.ClientID %>").hide();

                $("#<%=txbStDate.ClientID %>").show();
				$("#<%=txbEnDate.ClientID %>").show();
			}
        }

        function PagePassRegM(val) {
            var form = document.forms["form1"];

			form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/PassRegM");

			addInput(form, "pPROCID", val);

            form.submit();
		}
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div class="page-clothes-request">
		    <h2>칭찬 정보 조회</h2>
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>조회 조건</legend>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">조회일자</label><br />
						<input id="rbMonth" runat="server" style="margin-top:8px;margin-left:0px;width:18px;height:18px; border:1px solid #dfdfdf;float:left;" type="radio" checked="true" onclick="javascript:DateType('M');" />
						<span style="display:inline-block;width:70px;padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:400;float:left;">집계년월</span>
						<input id="rbDate" runat="server" style="margin-top: 8px; margin-left: 0px; width: 18px; height: 18px; border: 1px solid #dfdfdf; float: left;" type="radio" onclick="javascript:DateType('D');" />
						<span style="display:inline-block;width:70px;padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:400;float:left;">당월일자</span>
					</span>
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<input type="text" id="txbStMonth" runat="server" class="month" readonly="true" style="width:46% !important;" />
						<input type="text" id="txbStDate" runat="server" class="date" readonly="true" style="width:46% !important;display:none;" />
						<label style="width:10px">~</label>
						<input type="text" id="txbEnMonth" runat="server" class="month" readonly="true" style="width:46% !important;" />
						<input type="text" id="txbEnDate" runat="server" class="date" readonly="true" style="width:46% !important;display:none;" />
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
						<asp:Button id="btnSelect" runat="server" OnClick="btnSelect_Click" class="btn-green" BorderStyle="None" style="width:33px;margin:0 !important;" Text="조회" />
					</span>
				</fieldset>
			</div>
            <!-- S: scrollBox -->
			<strong style="margin-top:2px;margin-right:10px;float:left">칭찬 정보</strong>
            <div class="scrollbox">
				<div class="requestlist">
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<table>
									<thead>
										<th>No,</th>
										<th>일자</th>
										<th>구분</th>
										<th>칭찬하는 직원소속</th>
										<th>칭찬하는 직원이름</th>
										<th>관계</th>
										<th>비고</th>
									</thead>
									<tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
												<tr>
													<td><%# Eval("NUM") %></td>
													<td><%# Eval("PASSDT") %></td>
													<td><%# Eval("PASSTYPENM") %></td>
													<td><%# Eval("ORGFULLNM") %></td>
													<td><%# Eval("EMPNM") %></td>
													<td><%# Eval("RELATIONTYPE") %></td>
													<td><%# Eval("NOTI1") %></td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
								<asp:AsyncPostBackTrigger ControlID="paging" />
							</Triggers>
						</asp:updatepanel>
					</div>
				</div>
			</div>
			<uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
			<!--E: scrollBox -->			
        </div>
		<div class="page-main">
			<button type="button" onclick="window.location.href='/Mobile/PassRegM'" title="칭찬릴레이" class="btn-gray">칭찬릴레이에 참여해주세요.</button>
		</div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>