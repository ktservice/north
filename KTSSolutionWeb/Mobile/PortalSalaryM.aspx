﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PortalSalaryM.aspx.cs" Inherits="KTSSolutionWeb.PortalSalaryM" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/moment.js"></script>
    <script type="text/javascript" src="../Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="../Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="../Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
        
        fieldset > * {
            font-size: 11px;
        }
        fieldset > li {
            list-style: disc;
            margin: 0 12px;
        }
        .table-bordered {
            margin-bottom: 5px;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var vProvTypeCd = '';
        var vProvType = '';
        var vProvDt = '';

        var vSumProvTotAmt = 0; //누계지급총액
        var vSumRealProvAmt = 0; //누계지급총액
        var vSumSubTotAmt = 0; //누계공제합계
        var vPayTotAmt = 0; //급여 총액
        var vRealProvAmt = 0; //실지급 총액
        var vSubTotAmt = 0; //공제 총액

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };
                $("#txtMdate").monthpicker(options);

                $("#txtMdate").change(function () {
                    fn_PayData();
                });

                $("#btnSummary").click(function () {
                    //$("#lblContentTitle").text('급여명세서');
                    $("#MainContent_menu_lblContentTitle").text('급여명세서');
                    
                    $("#divDetail").hide();
                    $("#divSummary").show();

                    $("#txtMdate").show();
                    $("#txtMdateReadonly").hide();
                });
                
            }
        };

        var fn_Init = function () {
            //$("#lblContentTitle").text('급여명세서');
            $("#MainContent_menu_lblContentTitle").text('급여명세서');
            
            $(".mdate").val($("#<%=hdnMdate.ClientID %>").val());
            $("#divDetail").hide();

            $("#txtMdateReadonly").val($("#<%=hdnMdate.ClientID %>").val());
            $("#txtMdateReadonly").hide();
        }

        var fn_Load = function () {
            fn_PayData();
        }

        var ajaxCallBack = function (json) {
            if (callGb == 'PayData') {
                var html = '';
                var provTypeCd = '';
                var provType = '';
                var provDt = '';

                $.each(JSON.parse(json), function (idx, item) {
                    var htmlData = '';

                    provTypeCd = item.PROV_TYPE_CD;
                    provType = item.PROV_TYPE;
                    provDt = moment(item.PROV_DT).format("YYYY-MM-DD");

                    htmlData += '<div class="total-data">';
                    htmlData += '   <div class="tit-area">';
                    htmlData += '       <h3>' + provType + '</h3>';
                    htmlData += '		<input onclick="fn_PayDtlData(\'' + provTypeCd + '\',\'' + provDt + '\')" style="right:20px;font-size:12px;position:absolute" class="btn-lightgray" type="button" title="상세 보기" value="상세 보기" />';
                    htmlData += '   </div>';
                    htmlData += '	<table class="table table-bordered">';
                    /*htmlData += '		<thead>';
                    htmlData += '			<tr>';
                    htmlData += '				<th class="table-first-header" style="position:relative" colspan="2">';
                    htmlData += '					<span style="line-height:32px">' + provType + '</span>';
                    htmlData += '					<input onclick="fn_PayDtlData(\'' + provTypeCd + '\',\'' + provDt + '\')" style="right:10px;font-size:12px;position:absolute" class="btn-lightgray" type="button" title="상세 보기" value="상세 보기" />';
                    htmlData += '				</th>';
                    htmlData += '			</tr>';
                    htmlData += '		</thead>';*/
                    htmlData += '		<tbody>';
                    htmlData += '			<tr>';
                    htmlData += '				<td class="table-cell border_right" style="font-weight: bold;">지급 총액</td>';
                    htmlData += '				<td class="table-cell td_backwhite td_right">' + priceToString(item.PAY_TOT_AMT) + '</span></td>';
                    htmlData += '			</tr>';
                    htmlData += '			<tr>';
                    htmlData += '				<td class="table-cell border_right" style="font-weight: bold;">공제 총액</td>';
                    htmlData += '				<td class="table-cell td_backwhite td_right">' + priceToString(item.SUB_TOT_AMT) + '</span></td>';
                    htmlData += '			</tr>';
                    htmlData += '			<tr>';
                    htmlData += '				<td class="table-cell border_right" style="font-weight: bold;">실지급 총액</td>';
                    htmlData += '				<td class="table-cell td_backwhite td_right">' + priceToString(item.REAL_PROV_AMT) + '</span></td>';
                    htmlData += '			</tr>';
                    htmlData += '			<tr>';
                    htmlData += '				<td class="table-cell border_right" style="font-weight: bold;">과세</td>';
                    htmlData += '				<td class="table-cell td_backwhite td_right">' + priceToString(item.TAX_AMT) + '</span></td>';
                    htmlData += '			</tr>';
                    htmlData += '			<tr>';
                    htmlData += '				<td class="table-cell border_right" style="font-weight: bold;">비과세</td>';
                    htmlData += '				<td class="table-cell td_backwhite td_right">' + priceToString(item.TAX_FREE) + '</span></td>';
                    htmlData += '			</tr>';
                    htmlData += '			<tr>';
                    htmlData += '				<td class="table-cell border_right" style="font-weight: bold;">소득세</td>';
                    htmlData += '				<td class="table-cell td_backwhite td_right">' + priceToString(item.INCOME_TAX) + '</span></td>';
                    htmlData += '			</tr>';
                    htmlData += '			<tr>';
                    htmlData += '				<td class="table-cell border_right" style="font-weight: bold;">지방소득세</td>';
                    htmlData += '				<td class="table-cell td_backwhite td_right">' + priceToString(item.RES_TAX) + '</span></td>';
                    htmlData += '			</tr>';
                    htmlData += '		</tbody>';
                    htmlData += '	</table>';
                    htmlData += '</div>';
                    htmlData += '<h2></h2>';

                    html += htmlData;
                });

                $('#divSummary').html(html);

                if (provTypeCd === undefined) {
                    $('#divSummary').hide();
                    $('#divAlert').show();
                }
                else if (html != '') {
                    $('#divSummary').show();
                    $('#divAlert').hide();
                }
                else {
                    $('#divSummary').hide();
                    $('#divAlert').show();
                }

                callGb = '';
                
            }
            else if (callGb == 'PayDtlData') {
                var html1 = '';
                var html2 = '';
                var html3 = '';
                var htmlData = '';

                var vPayAmt = 0; //지급총액
                var vSumPayAmt = 0; //누계지급총액

                vSumProvTotAmt = 0; //누계지급총액
                vSumRealProvAmt = 0; //누계지급총액
                vSumSubTotAmt = 0; //누계공제합계
                vPayTotAmt = 0; //지급 총액
                vRealProvAmt = 0; //실지급 총액
                vSubTotAmt = 0; //공제 총액

                

                $.each(JSON.parse(json), function (idx, item) {
                    htmlData = '';
                    var allowCd1 = '';

                    allowCd1 = item.allow_cd1;

                    if (idx === 0) {
                        vSumProvTotAmt = item.sum_prov_tot_amt;
                        vSumRealProvAmt = item.sum_real_prov_amt;
                        vSumSubTotAmt = item.sum_sub_tot_amt;
                        vPayTotAmt = item.PAY_TOT_AMT;
                        vRealProvAmt = item.REAL_PROV_AMT;
                        vSubTotAmt = item.SUB_TOT_AMT;
                    }

                    if (allowCd1 == 'A98') //급여총액
                    {
                        vPayAmt = priceToString(item.allow);
                        vSumPayAmt = priceToString(item.allow2);
                    }

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width10 border_right" style="font-weight: bold;">' + item.allow_nm1 + '</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right border_right">' + priceToString(item.allow) + '</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right last">' + priceToString(item.allow2) + '</td>';
                    htmlData += '</tr>';

                    html2 += htmlData;
                    
                });


                if (html2 != '') {
                    //당월차인지급액

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width50 border_right">' + priceToString(vRealProvAmt) + '</td>';
                    htmlData += '    <td class="table-cell td_backwhite">' + priceToString(vSumRealProvAmt) + '</td>';
                    htmlData += '</tr>';

                    html1 += htmlData;

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width10 border_right" style="font-weight: bold;">지급총액</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right border_right">' + priceToString(vPayTotAmt) + '</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right last">' + priceToString(vSumProvTotAmt) + '</td>';
                    htmlData += '</tr>';

                    html3 += htmlData;

                    html2 += html3;


                    $('#bodyData1').html(html1);
                    $('#bodyData2').html(html2);

                }
                
                fn_PayDtlSubData(vProvTypeCd, vProvDt);
                
            }
            else if (callGb == 'PayDtlSubData') {

                var html1 = '';
                var html2 = '';
                var htmlData = '';

                $.each(JSON.parse(json), function (idx, item) {
                    htmlData = '';

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width10 border_right" style="font-weight: bold;">' + item.allow_nm1 + '</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right border_right">' + priceToString(item.sub_amt) + '</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right last">' + priceToString(item.sub_amt2) + '</td>';
                    htmlData += '</tr>';

                    html1 += htmlData;

                });

                if (html1 != '') {

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width10 border_right" style="font-weight: bold;">공제합계</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right border_right">' + priceToString(vSubTotAmt) + '</td>';
                    htmlData += '    <td class="table-cell td_width45 td_right last">' + priceToString(vSumSubTotAmt) + '</td>';
                    htmlData += '</tr>';

                    html2 += htmlData;

                    html1 += html2;

                    $('#bodyData3').html(html1);
                }

                callGb = '';
            }

        }

        function replaceBrTag(str) {
            if (str == undefined || str == null) {
                return "";
            }

            str = str.replace(/\r\n/ig, '<br>');
            str = str.replace(/\\n/ig, '<br>');
            str = str.replace(/\n/ig, '<br>');
            return str;
        }

        var fn_PayData = function () {
            callGb = 'PayData';

            $("#txtMdateReadonly").val($(".mdate").val());

            //입력 파라미터
            var aData = [];
            aData[0] = $(".mdate").val().replace(/-/gi, "");

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPayData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_PayDtlData = function (provTypeCd, provDt) {
            callGb = 'PayDtlData';

            vProvTypeCd = provTypeCd;
            vProvDt = provDt;

            var val = $(".mdate").val().split('-');
            var vTitle = '';
            vTitle = '급여명세서';
            vTitle += ' (' + val[0] + '년';
            vTitle += ' ' + val[1] + '월';
            vTitle += ' 정기급여명세)';

            //$("#lblContentTitle").text(vTitle);
            $("#MainContent_menu_lblContentTitle").text(vTitle);

            $("#divDetail").show();
            $("#divSummary").hide();

            $("#txtMdate").hide();
            $("#txtMdateReadonly").show();

            //입력 파라미터
            var aData = [];
            aData[0] = $(".mdate").val().replace(/-/gi, "");
            aData[1] = '';
            aData[2] = provTypeCd;
            aData[3] = provDt;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPayDtlData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_PayDtlSubData = function (provTypeCd, provDt) {
            callGb = 'PayDtlSubData';

            //입력 파라미터
            var aData = [];
            aData[0] = $(".mdate").val().replace(/-/gi, "");
            aData[1] = '';
            aData[2] = provTypeCd;
            aData[3] = provDt;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPayDtlSubData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


    </script>

	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div>
            <!-- S:UserControlTitle -->
            <uc:menu ID="menu" runat="server" />
            <!-- E:UserControlTitle -->
            <h2></h2>
            <div class="topContent">
                <!-- S:fieldset -->
                <fieldset>
                    <legend>조회조건</legend>
                    <span class="inpbox fieldsetSpan">
                        <label class="fieldsetLabel" for="txtMdate">월</label>
                        <input type="text" id="txtMdate" class="mdate" />
                        <input type="text" id="txtMdateReadonly" class="mdate read-only" />
                        <input type="hidden" id="hdnMdate" runat="server" />
                    </span>
                    <span>
                        <label id="lblRegDt" class="fieldsetLabel2"></label></span>
                </fieldset>
                <!-- //E:fieldset -->
            </div>
            <h2></h2>
            <!-- S: scrollBox -->
            <div class="scrollbox">
                <fieldset class="view-data-list-body">
                    <div class="data-group">
                        <div id="divSummary"></div>
                        <div id="divDetail">
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">차인지급액</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header td_width50 table-header border_right">당월차인지급액</th>
                                                <th class="table-header">누계차인지급액</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyData1">
				                        </tbody>
                                    </table>
                                    <div>
                                        <ul>
                                            <li>당월차인지급액 = 당월지급총액 - 당월공제합계
                                            </li>
                                            <li>누계차인지급액 = 누계지급총액 - 누계공제합계
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">지급항목</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header td_width10 table-header border_right">지급항목</th>
                                                <th class="td_width45 table-header border_right">지급</th>
                                                <th class="td_width45 table-header">누계</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyData2">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">공제항목</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header td_width10 table-header border_right">공제항목</th>
                                                <th class="td_width45 table-header border_right">공제</th>
                                                <th class="td_width45 table-header">누계</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyData3">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <h2></h2>
                            <div style="font-size:12px">
                                <span style="font-weight:600">보수에 관한 비밀준수 의무 안내</span>
                                <table style="border:1px dashed;width:100%;height:42px;border-color: #bbb;">
                                    <tbody style="font-size:12px;font-family: kt,Dotum,Arial,sans-serif;">
                                        <tr>
                                            <td><span style="color:red;font-weight:500;padding: 0 7px;">-보수규정 제6조(비밀의 준수)-</span></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0 7px;"><span style="color:#000;">직원은 보수에 관한 사항을 사,내외에 누설하여서는 아니된다.</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <span style="color:#888888">※ 관련 사규에 따라 급여내용은 타인에게 알리거나 제공할 수 없사오니, 임직원 여러분의 각별한 주의를 부탁 드립니다.</span>
                            </div>
                            <h2></h2>
                            <input id="btnSummary" class="btn-lightgray" type="button" title="요약 보기" value="요약 보기" />
                        </div>
                        <div id="divAlert">
                            <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <!-- E: scrollBox -->
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>