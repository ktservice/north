﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class RegTowerWagonM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }

            if (!IsPostBack)
            {                
                SetDropdownlist();

                initializeControls();
            }
        }

        private void SetDropdownlist()
        {
            DataSet ds1 = null;
            DataSet ds2 = null;

            try
            {
                string resultSession1 = string.Empty;
                string rptCd1 = "USERCARLIST";
                string jobGb1 = "GET";

                List<string> aData1 = new List<string>();

                string empNo = HttpContext.Current.Session["EMPNO"].ToString();

                aData1.Add(empNo); //파라미터 추가 시
                aData1.Add("TOWERWAGON");
                aData1.Add("F");

                DbTransaction dbTransaction1 = new DbTransaction(HttpContext.Current, rptCd1, jobGb1, aData1);
                resultSession1 = dbTransaction1.SessionCheck();

                if (resultSession1.Equals("")) //세션 유지일 경우 정상
                {
                    ds1 = dbTransaction1.GetDataSet();
                }

                ddlCarNo.Items.Clear();
                ddlCarNo.Items.Add(new ListItem("선택", ""));

                if (ds1.Tables.Count > 0)
                {
                    for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                    {
                        ddlCarNo.Items.Add(new ListItem(ds1.Tables[0].Rows[i]["CARNO"].ToString(), ds1.Tables[0].Rows[i]["CARID"].ToString()));
                    }
                }

                string resultSession2 = string.Empty;
                string rptCd2 = "TOWERWAGONLOGCODE";
                string jobGb2 = "GET";

                List<string> aData2 = new List<string>();

                aData2.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction2 = new DbTransaction(HttpContext.Current, rptCd2, jobGb2, aData2);
                resultSession2 = dbTransaction2.SessionCheck();

                if (resultSession2.Equals("")) //세션 유지일 경우 정상
                {
                    ds2 = dbTransaction2.GetDataSet();
                }

                if (ds2.Tables.Count > 0)
                {
                    string strWhere = string.Format("CODETYPE = '{0}'", "WORKTYPE");
                    DataRow[] dr = ds2.Tables[0].Select(strWhere);

                    ddlWorkType.Items.Clear();
                    ddlWorkType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlWorkType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "SVCTYPE");
                    dr = ds2.Tables[0].Select(strWhere);

                    ddlSvcType.Items.Clear();
                    ddlSvcType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlSvcType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "CABLETYPE");
                    dr = ds2.Tables[0].Select(strWhere);

                    ddlCableType.Items.Clear();
                    ddlCableType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlCableType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "BLDTYPE");
                    dr = ds2.Tables[0].Select(strWhere);

                    ddlBldType.Items.Clear();
                    ddlBldType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlBldType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "POSITIONTYPE");
                    dr = ds2.Tables[0].Select(strWhere);

                    ddlPositionType.Items.Clear();
                    ddlPositionType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlPositionType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds1 != null)
                {
                    ds1.Dispose();
                }
                if (ds2 != null)
                {
                    ds2.Dispose();
                }
            }
        }

        private void initializeControls()
        {
            DataSet ds = null;

            try
            {
                string strWorkDate = Request.Form["pWORKDATE"] == null ? "" : Request.Form["pWORKDATE"].ToString();
                string strCarId = Request.Form["pCARID"] == null ? "" : Request.Form["pCARID"].ToString();
                string strLogNo = Request.Form["pLOGNO"] == null ? "" : Request.Form["pLOGNO"].ToString();
                string strStDate = Request.Form["pSTDATE"] == null ? "" : Request.Form["pSTDATE"].ToString();
                string strEnDate = Request.Form["pENDATE"] == null ? "" : Request.Form["pENDATE"].ToString();
                string strEmpNm = Request.Form["pEMPNM"] == null ? "" : Request.Form["pEMPNM"].ToString();
                string strCarNo = Request.Form["pCARNO"] == null ? "" : Request.Form["pCARNO"].ToString();

                this.hdfStDate.Value = strStDate;
                this.hdfEnDate.Value = strEnDate;
                this.hdfEmpNm.Value = strEmpNm;
                this.hdfCarNo.Value = strCarNo;

                if (!strWorkDate.Equals("") && !strCarId.Equals("") && !strLogNo.Equals("") && !strWorkDate.Equals("null") && !strCarId.Equals("null") && !strLogNo.Equals("null"))
                {
                    this.hdfLogNo.Value = strLogNo;
                    this.ddlCarNo.SelectedValue = strCarId;
                    this.txbWorkDt.Text = strWorkDate;

                    this.ddlCarNo.Enabled = false;
                    this.txbWorkDt.Attributes.Remove("class");

                    this.txbCompany.ReadOnly = true;
                    this.txbOrgLv2Nm.ReadOnly = true;
                    this.txbOrgLv3Nm.ReadOnly = true;
                    this.txbOrgLv4Nm.ReadOnly = true;

                    string result = string.Empty;
                    string resultSession = string.Empty;
                    string rptCd = "TOWERWAGONLOGINFOFORREGMOBILE";
                    string jobGb = "GET";

                    List<string> aData = new List<string>();

                    aData.Add(strWorkDate); //파라미터 추가 시
                    aData.Add(strCarId); //파라미터 추가 시
                    aData.Add(strLogNo); //파라미터 추가 
                    aData.Add("F"); //파라미터 추가 시

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        ds = dbTransaction.GetDataSet();
                    }

                    if (ds.Tables.Count > 0)
                    {
                        this.txbCompany.Text = ds.Tables[0].Rows[0]["COMPANY"].ToString();
                        this.hdfCarOrgCd.Value = ds.Tables[0].Rows[0]["CARORGCD"].ToString();
                        this.txbOrgLv2Nm.Text = ds.Tables[0].Rows[0]["ORGLV2NM"].ToString();
                        this.txbOrgLv3Nm.Text = ds.Tables[0].Rows[0]["ORGLV3NM"].ToString();
                        this.txbOrgLv4Nm.Text = ds.Tables[0].Rows[0]["ORGLV4NM"].ToString();
                        this.hdfReqEmpNo.Value = ds.Tables[0].Rows[0]["REQEMPNO"].ToString();
                        this.txbReqEmpNm.Text = ds.Tables[0].Rows[0]["REQEMPNM"].ToString();
                        this.hdfEmpNo.Value = ds.Tables[0].Rows[0]["EMPNO"].ToString();
                        this.txbReqEmpNm.Text = ds.Tables[0].Rows[0]["EMPNM"].ToString();
                        this.ddlWorkType.SelectedValue = ds.Tables[0].Rows[0]["WORKTYPE"].ToString();
                        this.ddlSvcType.SelectedValue = ds.Tables[0].Rows[0]["SVCTYPE"].ToString();
                        this.txbOdrNum.Text = ds.Tables[0].Rows[0]["ODRNUM"].ToString();
                        this.ddlCableType.SelectedValue = ds.Tables[0].Rows[0]["CABLETYPE"].ToString();
                        this.ddlBldType.SelectedValue = ds.Tables[0].Rows[0]["BLDTYPE"].ToString();
                        this.ddlPositionType.SelectedValue = ds.Tables[0].Rows[0]["POSITIONTYPE"].ToString();
                        this.txbNotice.Text = ds.Tables[0].Rows[0]["NOTICE"].ToString();
                    }
                    else
                    {
                        PageUtility pageUtil = new PageUtility(this.Page);
                        pageUtil.PageMove("/MobileMain");

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }
                }
                else
                {
                    this.txbWorkDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                {
                    ds.Dispose();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOWERWAGONINFOBYCARID";
            string jobGb = "GET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;

            string rptCd = "USERINFOBYEMPNM";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsTowerWagonLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOWERWAGONLOGINSERT";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetTowerWagonLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOWERWAGONLOGUPDATE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}