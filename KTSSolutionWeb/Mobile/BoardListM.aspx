﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Board.Master" AutoEventWireup="true" CodeBehind="BoardListM.aspx.cs" Inherits="KTSSolutionWeb.BoardListM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Resource/css/tabs.css?20240624" rel="stylesheet" />
    <style type="text/css">   
        .contentsarea ul{
             white-space: nowrap;
            overflow-x: auto; 
            overflow-y: hidden;
            flex-wrap: inherit !important;
            scrollbar-width: none; 
        }
 
        .contentsarea ul::-webkit-scrollbar { 
          display: none; }
        
        .btn_today_close {
            width: 100%;
            height: 45px;
            margin-top:10px;
            background-color: #333;
            text-align: center;
            vertical-align:middle;
            color: #fff;
            font-size: 14px;
            display: block;
            border:0 none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
            },
            eventbind: function () {
                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    var boardType = $(this).attr("id").replace("tab", "");

                    $("#<%= hdfBoardType.ClientID %>").val(boardType);

                    fn_Search();

                }).filter(':eq(0)').click();
            }
        };

        var fn_Search = function () {
            $("#<%= btnSelect.ClientID %>").click();
        };

        function BoardWrite(boardid, boardtype, writerid, empno, authid) {
            var param = {
                pBOARDID: boardid,
                pBOARDTYPE: boardtype,
                pWRITERID: writerid,
                pEMPNO: empno,
                pAUTHID: authid
            };

            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/BoardWriteM");

            if (param != null) {
                for (var name in param) {
                    var valObj = param[name];
                    addInput(form, name, valObj);
                }
            }

            form.submit();
        }

        function BoardView(boardid, boardtype, writerid, empno, authid) {
            var param = {
                pBOARDID: boardid,
                pBOARDTYPE: boardtype,
                pWRITERID: writerid,
                pEMPNO: empno,
                pAUTHID: authid
            };

            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/BoardViewM");

            if (param != null) {
                for (var name in param) {
                    var valObj = param[name];
                    addInput(form, name, valObj);
                }
            }

            form.submit();
        }

        var ModalOpen = function () {
            $("#divModal").show();
        }

        var ModalClose = function () {
            $("#divModal").hide();
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<h2>기술공유 게시판</h2>
        <!-- S:searchbox -->
        <div class="searchboxPopUp">
		    <fieldset>
                <span class="inpbox" style="width:75%; margin:3px 0 3px 0; font-size:0; text-align:left;">
                    <label style="width:17%;padding:0">제목</label>
                    <asp:TextBox ID="txbTitle" runat="server" MaxLength="150" style="width:81% !important;padding:0"></asp:TextBox>
                </span>
                <span style="width:25%; margin:3px 0 3px 0; font-size:0; text-align:right">
                    <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green" BorderStyle="None" style="width:50px !important;" Text="조회" />
                    <asp:HiddenField ID="hdfBoardType" runat="server" Value="1" />
                </span>
                <span class="inpbox" style="width:75%; margin:3px 0 3px 0; font-size:0; text-align:left;">
                    <label style="width:17%">작성자</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" style="width:45% !important"></asp:TextBox>
                </span>
                <span style="width:25%; margin:3px 0 3px 0; font-size:0; text-align:right">
                    <asp:Button id="btnWrite" runat="server" class="btn-green last" BorderStyle="None" style="width:50px !important" Text="글쓰기" />
                </span>
            </fieldset>
        </div>
        <!-- E:searchbox -->
        <!-- S: contentsarea -->
        <div class="contentsarea">
            <ul id="tabnav" runat="server" class="tabnav">
            </ul>
        </div>
        <!-- E: contentsarea -->			
        <!-- S:boardlist -->
        <div class="boardlist" style="margin-top:0px;">
		    <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
			    <ContentTemplate>	
                    <ul>
                        <asp:Repeater ID="rptResult" runat="server">
                            <ItemTemplate>
                                <li id="li" runat="server">
                                    <span class="num"><%# Eval("NUM") %></span>
                                    <strong class="title">
                                        <asp:Label ID="lblBoardID" runat="server" Visible="false" Text='<%# Eval("BOARDID") %>'></asp:Label>
                                        <asp:Label ID="lblBoardType" runat="server" Visible="false" Text='<%# Eval("BOARDTYPE") %>'></asp:Label>
                                        <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                        <%# Eval("TITLE") %><%--<a id="aTitle" runat="server" style="cursor:pointer"></a>--%>
                                    </strong>
                                    <em class="writer"><%# Eval("EMPNM") %></em>
                                    <span class="date"><%# Eval("WRITEDT") %></span>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
					<asp:AsyncPostBackTrigger ControlID="paging" />
				</Triggers>
			</asp:UpdatePanel>
        </div>
		<uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        <!-- //E:boardlist -->

        <!-- S: divModal -->
		<div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;height:340px;top:15%;background:rgb(255, 255, 255);border-radius:10px;border:1px solid gray;">
            <!-- S:popcontents -->
            <div class="popcontents" style="text-align:center;vertical-align:middle;font-size:14px;padding-top:10px;">
                금일은 귀하의 휴일 또는 휴가일로<br/>
                근로제공의무가 없는 날이 오니<br/>
                업무처리를 삼가하시고 귀가하시기 바랍니다.<br/><br/>

                만약 부서장의 근무지시가 있었다면<br/>
                지금 즉시 복무시스템에 복무를<br/>
                처리하여 주시기 바랍니다.<br/><br/>

                휴일근무한 경우 휴일근무를 신청하고<br/>
                휴가일인 경우 휴가 취소를 신청하시어<br/>
                부서장 승인을 받으시기 바랍니다.<br/>                
                <!-- S:btncenter -->
                <button type="button" id="btnModalClose"  onclick="ModalClose();" title="닫기" class="btn-gray" style="margin-top:10px;">닫기</button>
                <!-- //E:btncenter -->
            </div>
            <asp:UpdatePanel ID="UdpToday" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:button ID="btnToday" runat="server" class="btn_today_close" OnClick="btnToday_Click" Text="오늘 하루 자동 팝업을 띄우지 않습니다." />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnToday" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>
            <%--<button type="button" id="btnToday" class="btn_today_close"><span>오늘 하루 자동 팝업을 띄우지 않습니다.</span></button>--%>
		</div>
		<!--E: divModal -->
	</div>
	<!--//E: contentsarea -->
</asp:Content>