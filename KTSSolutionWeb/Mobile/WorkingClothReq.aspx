﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="WorkingClothReq.aspx.cs" Inherits="KTSSolutionWeb.WorkingClothReq" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<style>
		.loading {position: fixed; z-index: 99; top: 0px; left: 0px; right: 0px; height: 100%; background-color: #f9f9f9 !important; opacity: 0.8;}
		.loadingContent {z-index: 98; padding: 20%; text-align: center; top: 27%; left: 20%; position: fixed;}
	</style>
    <script type="text/javascript">
        var ModalOpen = function () {
            $("#divModal").show();
        }

        var ModalClose = function () {
            $("#divModal").hide();
		}

        function RegChk() {
            var msg;
            var btnTxt = $("#<%= btnReg.ClientID %>").val();
            var NoReqYn = $("#<%= hdfNoReqCheckedYn.ClientID %>").val();
            var ReqTitle = $("#<%= ReqTitle.ClientID %>").html();

            if (NoReqYn == "Y") {
                msg = "유니폼 미신청/기준이하 신청시 양호한 유니폼 여분이 없는 경우 관리자가 반려할 수 있습니다. " + btnTxt + " 하시겠습니까?";
            }
            else {
                if (ReqTitle == "추가 신청") {
                    msg = "추가유니폼 신청시 신청포인트에 비례하여 추후 지급되는 포인트가 감소하게 됩니다. " + btnTxt + " 하시겠습니까?";
                }
                else {
                    msg = btnTxt + " 하시겠습니까?";
                }
			}

            if (confirm(msg)) {
                $("#updateProgress").css('display', 'block');
                return true;
            } else {
                $("#updateProgress").css('display', 'block');
                return false;
            }
        }

        function DelChk() {
            if (confirm("삭제 하시겠습니까?")) {
                $("#updateProgress").css('display', 'block');
                return true;
            } else {
                return false;
            }
		}
    </script>

	<!-- S: contentsarea -->
	<div class="contentsarea">

		<div id="divModal" class="scrollbox" style="display:none;position:absolute;width:100%;top:15%;background:rgb(255, 255, 255);border-radius:10px;border:1px solid gray;z-index:1050;">
        <!-- S:popcontents -->
        <div class="popcontents" style="text-align:center;vertical-align:middle;font-size:14px;padding-top:10px;padding-bottom:10px;">
            <asp:image id="summerClothImg" style="width: 100%;" src="/Resource/Mobile_images/img_summerCloth.png" runat="server" /><br/>
			<asp:image id="winterClothimg" style="width: 100%;" src="/Resource/Mobile_images/img_winterCloth.png" runat="server" /><br/>
            <!-- S:btncenter -->
            <button type="button" id="btnModalClose"  onclick="ModalClose();" title="닫기" class="btn-gray" style="margin-top:10px;">닫기</button>
            <!-- //E:btncenter -->
        </div>
		</div>
		
        <div class="page-clothes-request">
			<h2>피복신청</h2>
			
			<div class="pointlist">
				<asp:UpdatePanel ID="updPanelInfo" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<fieldset>
							<legend>포인트내역 입력폼</legend>
							<span class="inpbox" style="display:block; width:100%; margin:11px 0 0 0; font-size:0;">
								<label style="width:15%; margin:0 !important;margin-left:0;">소속</label>
								<asp:TextBox ID="txbOrgFullNm" runat="server" ReadOnly="true" style="width:84% !important"></asp:TextBox>
							</span>
							<span class="inpbox" style="display:block; width:100%; margin:11px 0 11px 0; font-size:0;">
								<label style="width:15%; margin:0 !important;margin-left:0;">지급기준</label>
								<asp:TextBox ID="txbTitle" runat="server" ReadOnly="true" style="width:84% !important"></asp:TextBox>
							</span>
						</fieldset>
						<!-- S:fieldset -->
						<fieldset id="fieldset" runat="server" visible="false">
							<legend>포인트내역 입력폼</legend>
							<span class="inpbox">
								<label>총 포인트</label>
								<asp:TextBox ID="txbTotPoint" runat="server" ReadOnly="true"></asp:TextBox>
							</span>
							<span class="inpbox">
								<label>사용</label>
								<asp:TextBox ID="txbUsePoint" runat="server" ReadOnly="true"></asp:TextBox>
							</span>
							<span class="inpbox">
								<label>잔여</label>
                                <asp:TextBox ID="txbRestPoint" runat="server" ReadOnly="true"></asp:TextBox>
							</span>
						</fieldset>
						<!-- //E:fieldset -->
					</ContentTemplate>
					<Triggers>
					</Triggers>
				</asp:UpdatePanel>
			</div>
			<br />
            <!-- S: scrollBox -->
            <div class="scrollbox">
				<div class="requestlist">
					<strong id="ReqTitle" runat="server" style="margin-top:2px;margin-right:10px;float:left">추가 신청</strong>
					<button type="button" id="btnModalOpen" onclick="ModalOpen();" class="btn-gray" style="margin-right:10px;float:right">피복 이미지 확인</button>
					<asp:Button id="btnAddList" runat="server" onClick="btnAddList_Click" class="btn-plus" BorderStyle="None" Text="+" />
					
                    <div class="datalist" style="margin-top:10px;">
						<asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
							<ContentTemplate>	
								<table>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">동/하복</th>
                                                    <td style="min-width:15px; padding:10px 12px 10px 0px;">
														<p class="inpbox" style="text-align:left;">
															<asp:TextBox ID="txbClothGbn" Width="80%" runat="server" ReadOnly="true" Text='<%# Eval("CLOTHGBN") %>' style="padding:0;text-align:center;"></asp:TextBox>
														</p>
                                                    </td>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">종류</th>
                                                    <td style="min-width:15px; padding:10px 15px 10px 0px;text-align:left;">
                                                        <p class="optionbox" style="text-align:left;">
                                                            <asp:Label ID="lblRequestID" runat="server" Visible="false" Text='<%# Eval("REQUESTID") %>'></asp:Label>
															<asp:Label ID="lblDetailSeq" runat="server" Visible="false" Text='<%# Eval("DETAILSEQ") %>'></asp:Label>
															<asp:TextBox ID="txbClothType" runat="server" Width="90%" ReadOnly="true" Text='<%# Eval("CLOTHTYPE") %>' style="padding:0;text-align:center;"></asp:TextBox>
															<asp:DropDownList ID="ddlAddClothType" Width="90%" runat="server" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlAddClothType_SelectedIndexChanged" style="background:url('/Resource/Mobile_images/icon_select_arr.png') #fff no-repeat 100% 50% / 14px auto;"></asp:DropDownList>
														</p>
                                                    </td>
                                                </tr>
                                                <tr>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">사이즈</th>
                                                    <td style="min-width:15px; padding:10px 12px 10px 0px;">
														<p class="optionbox" style="text-align:left;">
															<asp:TextBox ID="txbSize" runat="server" Width="55px" Visible="false" ReadOnly="true" Text='<%# Eval("CLOTHSIZE") %>' style="padding:0;text-align:center;"></asp:TextBox>
                                                            <asp:DropDownList ID="ddlSize1" Width="55px" runat="server" style="background:url('/Resource/Mobile_images/icon_select_arr.png') #fff no-repeat 100% 50% / 14px auto;"></asp:DropDownList>
                                                            <asp:DropDownList ID="ddlSize2" Width="93px" runat="server" Visible="false" style="padding:0 5px;background:url('/Resource/Mobile_images/icon_select_arr.png') #fff no-repeat 100% 50% / 14px auto;"></asp:DropDownList>
														</p>
                                                    </td>
													<th style="min-width:15px; padding:10px 12px 10px 0px;">수량</th>
                                                    <td style="min-width:15px; padding:10px 12px 10px 0px;">
														<p class="inpbox" style="text-align:left;">
															<asp:TextBox ID="txbReqCnt" AutoPostBack="true" Width="90%" MaxLength="3" runat="server" Text='<%# Eval("REQCNT") %>' onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" OnTextChanged="txbReqCnt_TextChanged" style="padding:0;text-align:center;"></asp:TextBox>
														</p>
													</td>
                                                </tr>
                                                <tr id="tr" runat="server">
													<th id="thPass" runat="server" style="min-width:15px; padding:10px 12px 10px 0px;">미신청</th>
                                                    <td id="tdPass" runat="server" style="min-width:15px; padding:10px 15px 10px 0px;text-align:left;">
														<p class="optionbox" style="text-align:left;top:-3px;">
															<asp:CheckBox ID="chkNoReq" runat="server" width="20px" height="20px" AutoPostBack="true" OnCheckedChanged="chkNoReq_CheckedChanged" />
															<asp:TextBox ID="txbReqStatus" runat="server" Visible="false" Text='<%# Eval("REQSTATUS") %>'></asp:TextBox>
														</p>
													</td>
													<th id="thPoint" runat="server" style="min-width:15px; padding:10px 12px 10px 0px; border-bottom:1px solid #000;">포인트</th>
                                                    <td id="tdPoint" runat="server" style="min-width:15px; padding:10px 12px 10px 0px; border-bottom:1px solid #000;">
														<p class="inpbox" style="text-align:left;">
															<asp:TextBox ID="txbPoint" runat="server" Width="100px" ReadOnly="true" Text='<%# Eval("USEPOINT") %>' style="padding:0;text-align:center;"></asp:TextBox>
														</p>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
								</table>
                            <asp:HiddenField ID="hdfNoReqCheckedYn" runat="server" Value="N" />
							</ContentTemplate>
							<Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnAddList" EventName="Click"/>
                                <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click"/>
                                <asp:AsyncPostBackTrigger ControlID="btnDel" EventName="Click"/>
							</Triggers>
						</asp:updatepanel>
					</div>
				</div>
			</div>
		</div>	
        <!-- S:btncenter -->
		<div class="list-top" style="margin-top:20px;">
			<div style="text-align:center;">
				<asp:Button id="btnReg" runat="server" OnClientClick="return RegChk();" OnClick="btnReg_Click" class="btn-green" Width="80px" BorderStyle="None" Text="신청" />
				<asp:Button id="btnDel" runat="server" OnClientClick="return DelChk();" OnClick="btnDel_Click" class="btn-green" Width="80px" BorderStyle="None" Text="삭제" />
			</div>
        </div>
        <!-- //E:btncenter -->	
	</div>
	<!--//E: contentsarea -->
</asp:Content>