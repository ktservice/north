﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using KTS.KTSSolution.BSL.Oper;

namespace KTSSolutionWeb
{
    public partial class RegInspectionInfoByCompM : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    initializeControls();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                string strInspection_No = Request.Form["pINSPECTION_NO"] == null ? "" : Request.Form["pINSPECTION_NO"].ToString();
                string strStDt = Request.Form["pSTDT"] == null ? "" : Request.Form["pSTDT"].ToString();
                string strEnDt = Request.Form["pENDT"] == null ? "" : Request.Form["pENDT"].ToString();
                string strActCode = Request.Form["pACTCODE"] == null ? "" : Request.Form["pACTCODE"].ToString();
                string strLv1 = Request.Form["pLV1CD"] == null ? "" : Request.Form["pLV1CD"].ToString();

                this.hdfInspection_No.Value = strInspection_No;
                this.hdfStdt.Value = strStDt;
                this.hdfEndt.Value = strEnDt;
                this.hdfActCode.Value = strActCode;
                this.hdfLv1.Value = strLv1;

                if (strInspection_No.Length > 0)
                {
                    this.hdfInspection_No.Value = strInspection_No;

                    GetInspectionInfo();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "validationChk", "alert('잘못된 접근입니다.');fn_PageMoveMain();", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetInspectionInfo()
        {
            DataSet ds = null;

            try
            {
                string strInspection_No = this.hdfInspection_No.Value;

                string resultSession = string.Empty;
                string rptCd = "INSPECTIONINFOSELECT";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                string strEmpNo = Session["EMPNO"].ToString();

                aData.Add(strInspection_No);
                aData.Add(strEmpNo);
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    this.txbChkDt.Text = ds.Tables[0].Rows[0]["CHK_DATE"].ToString();
                    this.txbChkOrgNm.Text = ds.Tables[0].Rows[0]["CHK_ORGNM"].ToString();
                    this.txbChkEmpNm.Text = ds.Tables[0].Rows[0]["CHK_USERNM"].ToString();

                    string strCompanyCD = ds.Tables[0].Rows[0]["COMPANYCD"].ToString();
                    string strActUser = ds.Tables[0].Rows[0]["ACT_USERNM"].ToString();

                    if (strCompanyCD.IndexOf("_Y") > 0)
                    {
                        strActUser = "협력사 대표자";
                    }

                    this.txbCompany.Text = ds.Tables[0].Rows[0]["COMPANYNM"].ToString();
                    this.txbPartnersEmpNm.Text = strActUser;
                    this.txbNoti.Text = ds.Tables[0].Rows[0]["NOTE"].ToString();

                    this.txbLineNo.Text = ds.Tables[0].Rows[0]["LINENO"].ToString();
                    this.txbCustNm.Text = ds.Tables[0].Rows[0]["CUSTNM"].ToString();

                    this.txbLv1Grp.Text = ds.Tables[0].Rows[0]["GRPNM_LV1"].ToString();
                    this.txbLv2Grp.Text = ds.Tables[0].Rows[0]["GRPNM_LV2"].ToString();
                    this.txbLv3Grp.Text = ds.Tables[0].Rows[0]["GRPNM_LV3"].ToString();

                    this.txbReqNoti.Text = ds.Tables[0].Rows[0]["FAULTY_NOTE"].ToString();
                    this.txbActNoti.Text = ds.Tables[0].Rows[0]["ACT_NOTE"].ToString();

                    string strReject = ds.Tables[0].Rows[0]["REJECT_NOTE"].ToString();
                    this.txbReject.Text = strReject;

                    string strChkStatus = ds.Tables[0].Rows[0]["CHK_CD"].ToString();
                    this.hdfChkStatus.Value = strChkStatus;
                    string strActStatus = ds.Tables[0].Rows[0]["ACT_CD"].ToString();
                    this.hdfActStatus.Value = strActStatus;

                    string strActUserNo = ds.Tables[0].Rows[0]["ACT_USER"].ToString();
                    string strDelegateNo = ds.Tables[0].Rows[0]["EMPNO"].ToString();
                    string strDelegateNo2 = ds.Tables[0].Rows[0]["EMPNO2"].ToString();

                    string strCreateUser = ds.Tables[0].Rows[0]["CREATE_USER"].ToString();
                    string strAuthid = Session["AUTHID"].ToString();

                    //점검결과
                    string result = string.Empty;
                    string rptCd2 = "INSPECTIONCHKRESULT";
                    string jobGb2 = "GET";

                    List<string> aData2 = new List<string>();

                    aData2.Add(strInspection_No);
                    aData2.Add("F"); //파라미터 추가 시

                    dbTransaction = new DbTransaction(HttpContext.Current, rptCd2, jobGb2, aData2);

                    result = dbTransaction.GetJsonData();

                    this.hdfResult.Value = result;

                    bool bImg = true;

                    //이미지 조회 추가
                    string resultReqImg = string.Empty;
                    string resultActImg = string.Empty;

                    if (strChkStatus.Equals("F") && strActStatus.Equals('U'))
                    {
                        bImg = false;
                    }

                    if (bImg)
                    {
                        string rptCdReqImg = "INSPECTIONIMGDATASELECT";
                        string jobGbReqImg = "GET";

                        List<string> aDataReqImg = new List<string>();

                        aDataReqImg.Add(strInspection_No);
                        aDataReqImg.Add("R");
                        aDataReqImg.Add("F"); //파라미터 추가 시

                        dbTransaction = new DbTransaction(HttpContext.Current, rptCdReqImg, jobGbReqImg, aDataReqImg);

                        resultReqImg = dbTransaction.GetJsonData();

                        string rptCdActImg = "INSPECTIONIMGDATASELECT";
                        string jobGbActImg = "GET";

                        List<string> aDataActImg = new List<string>();

                        aDataActImg.Add(strInspection_No);
                        aDataActImg.Add("A");
                        aDataActImg.Add("F"); //파라미터 추가 시

                        dbTransaction = new DbTransaction(HttpContext.Current, rptCdActImg, jobGbActImg, aDataActImg);

                        resultActImg = dbTransaction.GetJsonData();
                    }

                    this.hdfReqImg.Value = resultReqImg;
                    this.hdfActImg.Value = resultActImg;

                    bool bReq = false;
                    bool bRegImg = false;

                    if (strEmpNo.Equals(strActUserNo) || strEmpNo.Equals(strDelegateNo) || strEmpNo.Equals(strDelegateNo2))
                    {
                        bReq = true;
                    }

                    if (bReq && (strActStatus.Equals("R") || strActStatus.Equals("T")))
                    {
                        //조치상태 등록중 or 조치대기중
                        this.trActImgNm.Visible = true;

                        this.btnActImg.Attributes.Add("onclick", "javascript:fn_RegFileCheck('A');");
                        this.btnActImg.Visible = true;
                        this.btnActAdd.Attributes.Add("onclick", "javascript:AddFileUpload();");
                        this.btnActAdd.Visible = true;

                        this.txbActNoti.ReadOnly = false;

                        this.btnRegAct.Visible = true;
                        this.btnRegAct.Attributes.Add("onclick", "javascript:fn_SetActStatus('T');");

                        this.btnActFin.Visible = true;
                        this.btnActFin.Attributes.Add("onclick", "javascript:fn_SetActStatus('F');");
                        //등록중일 경우 정보/요청사항 수정가능

                        bRegImg = true;
                    }
                    else if (strActStatus.Equals("F"))
                    {
                        //감리완료
                        this.trActImgNm.Visible = false;

                        this.btnActImg.Visible = false;
                        this.btnActAdd.Visible = false;

                        this.txbActNoti.ReadOnly = true;

                        this.btnRegAct.Visible = false;
                        this.btnRegAct.Visible = false;

                        this.btnActFin.Visible = false;
                    }
                    else
                    {
                        //기타 정상적인 상태일경우 해당사항없음
                        this.trActImgNm.Visible = false;

                        this.btnActImg.Visible = false;
                        this.btnActAdd.Visible = false;

                        this.txbActNoti.ReadOnly = true;

                        this.btnRegAct.Visible = false;
                        this.btnRegAct.Visible = false;

                        this.btnActFin.Visible = false;
                    }

                    if (!strActStatus.Equals("F") && !strReject.Equals(""))
                    {
                        this.hReject.Visible = true;
                        this.tableReject.Visible = true;
                    }
                    else
                    {
                        this.hReject.Visible = false;
                        this.tableReject.Visible = false;
                    }

                    this.txbReject.ReadOnly = true;

                    if (!strActStatus.Equals("F") && !strReject.Equals(""))
                    {
                        this.hReject.Visible = true;
                        this.tableReject.Visible = true;
                    }
                    else
                    {
                        this.hReject.Visible = false;
                        this.tableReject.Visible = false;
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "initControl", "fn_SetControls(" + bRegImg.ToString().ToLower() + ");", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetImgData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONIMGDATASELECT";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelImgData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONIMGDATADELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetInspectionActStatus(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "INSPECTIONACTSTATUSUPDATE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}