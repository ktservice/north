﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Order.Master" AutoEventWireup="true" CodeBehind="VanOrderInfoByOrderNo.aspx.cs" Inherits="KTSSolutionWeb.VanOrderInfoByOrderNo" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>        
        .scrollbox2 {overflow-x:hidden; overflow-y:auto; width:100%; height:auto; min-height:30px; margin-top:2px; margin-bottom:2px}
        .inpbox span {display:inline-block; width:100%; vertical-align:middle; color:#000; font-size:13px;}
        .inpbox input {display:inline-block; width:100%; height:30px; padding:0 14px; border:1px solid #dfdfdf; border-radius:2px; line-height:30px; vertical-align:middle; color:#000; font-size:13px;}
        .tabulator {
            position:relative;
            background-color:#fff;
            overflow:hidden;
            font-size:11px;
            text-align:left;
            -ms-transform:translateZ(0);
            transform:translateZ(0);
        }

        .div-radio {
            cursor: pointer;
            padding-top: 3px;
            padding-left: 2px;
            margin-right: 10px;
            text-align: left;
            border-radius: 3px;
            float: left;
        }

        .label-rbInner {
            cursor: pointer;
            padding-left: 2px;
            font-size:13px;
        }

        .btn-inner {
            display: inline-block;
            width: 50px;
            min-width: 50px;
            height: 40px;
            line-height: 0;
            padding: 0;
            border: 0;
            border-radius: 2px;
            background: #333;
            font-size: 11px;
            font-weight: 800;
            text-align: center;
            color: #fff;
        }

        .checkbox-extra {
            width: 20px;
            margin-left: 10px;
            float: left;
        }
        .label-extra-content {
            float: left;
            font-size: 12px;
            /* padding-left: 5px; */
            padding-right: 0px;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .div-extra-minus {
            border: 1px solid #dfdfdf; 
            width: 20px; 
            height: 20px; 
            float: left; 
            border-radius: 3px; 
        }

        .div-extra-plus {
            border: 1px solid #dfdfdf;
            width: 20px; 
            height: 20px; 
            float: left; 
            border-radius: 3px; 
        }

        .label-extra-calc {
            font-size: 20px;
            line-height: 20px;
            width: 20px; 
            text-align: center;    
        }

        .label-extra-cnt {
            width: 20px;
            height: 20px;
            float: left;
            text-align: center;
            margin-left:8px;
            margin-right:8px !important;
        }

        .div-extra-del {
            background-color: darkgray;
            width: 20px;
            height: 20px;
            float: left;
            border-radius: 15px;
            margin-left: 20px;
            cursor: pointer;
        }

        .div-extra-del-inner {
            height: 20px;
            margin-top: -6px;
            padding-left: 2.5px;
            display: inline-block;
            float: left;
            cursor: pointer;
        }

        .div-extra-del-inner:after {            
            display: inline-block;
            content: "\00d7"; 
            font-size:20pt;
            color: white;
        }
        /* 스타일 추가 240524 */V
        #divExtraList {width: 100% !important;}
        #divExtraList .divExtra {margin-bottom: 10px; border-bottom: 1px dashed #dfdfdf;}
        #divExtraList .divExtra:last-child {margin-bottom: 0; border-bottom: 0;}
        #divExtraList .divExtra .label-extra-content {margin-bottom: 5px;}
        #divExtraChk {width: 100% !important;}
        .pageView {table-layout: fixed; border-collapse: collapse; border-spacing: 0; width: 100%; background: #ffffff;}
        .pageViewTh {font-size: 13px; text-align: left; border-bottom: 1px solid #dfdfdf; padding: 6px 10px; background: #f9f9f9;}
        .pageViewTd {font-size: 13px; text-align: left; border-bottom: 1px solid #dfdfdf; padding: 6px 10px;}
        /*p.inpbox {display: flex;}*/
        p.inpbox input {flex: 1;}
        .inpbox label {margin-right: 0;}
        .tableInTitle {display:block !important; justify-content:center; text-align: center; background-color: #eaf7f6; padding: 6px 0; margin-bottom: 10px;}
        #container h2#htitle {margin-bottom: 0;}
        .hopeHolidayChk {display: flex;}
        .hopeDateTime {display: flex; width: 100%; margin-top: 5px;}
        .hopeDateTime .inpbox {width: 110px;}
        .hopeDateTime .optionbox {width: calc(100% - 115px); margin-left: 5px;}
        .hopeDateTime .optionbox select {width: 100%; height: 30px;}
        .terminalAddrData {margin-top: 5px;}
        .inpbox textarea {padding: 10px 14px;}
        .inpbox textarea:read-only {background: #f9f9f9; border: 1px solid #dfdfdf;}
        .inpbox input:read-only {background: #f9f9f9; border: 1px solid #dfdfdf;}
        .label-extra-content:last-child {margin-bottom: 0;}
        #btnMemo.btn-gray {width:100%; border: none;}
        .btnWrap {display: flex; justify-content: center; width: 100%; margin-top: 30px;}
        .btnWrap input {min-width: 55px; padding: 0 10px; margin-right: 5px; border: none;}
        .btnWrap input:last-child {margin-right: 0;}
        .filebox .file {display: flex; float: left; width: 30px !important;}

    </style>
    <script type="text/javascript">
        var ktsGrid1;
        var ktsGrid2;
        var grid1;
        var grid2;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
                fn_SetControl();
                fn_GridInit1();
                fn_GridInit2();
            },
            eventbind: function () {

                $("#btnEmpSelect").click(function (e) {
                    var EmpNm = $("#<%= txbSearchEmpNm.ClientID %>").val();

                    if (EmpNm.length == 0) {
                        alert("이름을 입력해주세요.");
                        return false;
                    } else {
                        //입력 파라미터
                        var aData = [];
                        aData[0] = EmpNm;

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = grid1.getUrl(location.href, 'GetEmpData');

                        //grid Bind
                        grid1.getDataBind(jsonData, ajaxUrl, 'ktsGrid1');
                    }
                });
            }
        };

        var fn_Load = function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $("input.date").datepicker({
                    dateFormat: "yy-mm-dd",
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    showOtherMonths: false,
                    selectOtherMonths: false,
                    constrainInput: false,
                    prevText: "<",
                    nextText: ">",
                });
            }
        };

        var fn_SetControl = function () {

            $("#chkExtraAll").click(function () {
                if ($("#chkExtraAll").is(":checked")) {
                    $("input[name=chkExtra]").prop("checked", true);
                }
                else {
                    $("input[name=chkExtra]").prop("checked", false);
                }

                fn_SetChkExtraList();
            });


            $("input[name=rbSWChk]").change(function () {
                if ($("input[name=rbSWChk]:checked").val() == "세팅필요") {
                    $("#<%= ddlSWSettingHost.ClientID %>").prop("disabled", false);
                }
                else {
                    $("#<%= ddlSWSettingHost.ClientID %>").val('');
                    $("#<%= ddlSWSettingHost.ClientID %>").prop("disabled", true);
                }
            });

            var recipttype = $("#<%= lblReceiptType.ClientID %>").text();
            var status = $("#<%= hdfStatus.ClientID %>").val();

            if (recipttype == "VAN365" && status == '3.5') {
                $("#divExtraSearch").show();
                $("#btnExtraList").show();
            } else {
                $("#divExtraSearch").hide();
                $("#btnExtraList").hide();

                $("input[name=rbDispatchChk]").prop('disabled', true);
                $("input[name=rbSWChk]").prop('disabled', true);
            }

            if (recipttype == "VAN365") {                
                //$("#pExtraSearch").width($("#htitle").width() - 43);
                //$("#divExtraChk").width($("#htitle").width() + 10);
                //$("#lblExtraSearch").width($("#htitle").width() + 10);                
                //$("#lblExtraChk").width($("#htitle").width() - 48);
                //$("#divExtraList").width($("#htitle").width() + 10);
                
                fn_radioBoxChk($("#<%= hdfDispatch.ClientID %>").val());
                fn_radioBoxChk($("#<%= hdfSwSetting.ClientID %>").val());

                fn_SetCATList();

                var ExtraList = $("#<%= hdfPolicyExtra.ClientID %>").val();
                fn_ControlBind_PolicyExtra(ExtraList);
            }

            if (!recipttype == "VAN365" || status != '3.5') {
                $("#<%= ddlSWSettingHost.ClientID %>").prop("disabled", true);
            }
        };

        var fn_GridInit1 = function () {

            var fn_Choicebutton = function (cell) {
                var rowData = cell.getRow().getData();

                var orgnm = rowData.ORGNM;
                var empno = rowData.EMPNO;
                var empnm = rowData.EMPNM;

                var input = "<input id='btnCell' type='button' onclick='fn_SetEmpNm(\"" + orgnm + "\", \"" + empno + "\", \"" + empnm + "\");' value='선택' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;border-style:none;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80, resizable: false, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 80, formatter: fn_Choicebutton, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", minWidth: 30, maxWidth: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", minWidth: 30, maxWidth: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", minWidth: 30, maxWidth: 400, resizable: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid1 = ktsTabulator;
            grid1.gridDefault('ktsGrid1', 'fitColumns', true, null); // div의 grid id 값
            ktsGrid1 = ktsGrid;

            ktsGrid1.setHeight("340px");
        };

        var fn_GridInit2 = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", field: "id", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "메모", field: "CONTENT", sorter: "string", minWidth: 30, maxWidth: 600, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "ENROLLMENT_DATE", sorter: "string", minWidth: 30, maxWidth: 160, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "등록자", field: "WRITER_NAME", sorter: "string", minWidth: 30, maxWidth: 120, resizable: false },
                { headerHozAlign: "center", hozAlign: "center", title: "오더번호", field: "ORDERNO", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid2 = ktsTabulator;
            grid2.gridDefault('ktsGrid2', 'fitData', false, null); // div의 grid id 값
            ktsGrid2 = ktsGrid;

            ktsGrid2.setHeight("320px");

            $(".tabulator-col").css('height', '');
            $(".tabulator-col").css('padding-top', '');
        }

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                if (ktsGridId == 'ktsGrid1') {
                    ktsGrid1.setData(json);
                }
                else {
                    ktsGrid2.setData(json);
                }
            }
        }

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'TransOk') {
                            alert('작업이관이 완료되었습니다.');
                            $("#<%= txbReason.ClientID %>").val('');
                            $("#<%= hfEmpNo.ClientID %>").val('');
                            $("#<%= txbEmpNm.ClientID %>").val('');
                            $("#<%= txbSearchEmpNm.ClientID %>").val('');
                            $("#btnBack").click();
                        }
                        else if (callGb == 'InsMemoOk') {
                            alert('메모가 추가되었습니다.');
                            $("#<%= txbContent.ClientID %>").val('');
                            $("#<%= txbContent.ClientID %>").val('');
                            fn_searchMemo();
                        }
                        else {
                            if (callGb == 'AcceptOk') {
                                alert('작업을 수령했습니다.');
                            }
                            else if (callGb == 'RejectOk') {
                                alert('작업을 거부했습니다.');
                            }
                            else if (callGb == 'SuccOk') {
                                alert('완료처리 되었습니다.');
                            }
                            else if (callGb == 'WorkStartOk') {
                                alert('작업을 시작 했습니다.');
                            }
                            else if (callGb == 'CancelOk') {
                                alert('취소처리 되었습니다.');
                            }
                            else if (callGb == 'HoldOk') {
                                alert('작업보류 되었습니다.');
                                $("#<%= txbReason.ClientID %>").val('');
                            }
                            else if (callGb == 'DateOk') {
                                alert('일정이 수정되었습니다.');
                                $("#<%= txbReason.ClientID %>").val('');
                            }
                            else if (callGb == 'UpdOk') {
                                alert('VAN오더가 수정되었습니다.');
                            }                            

                            $("#divModal").hide();
                            $("#divModal2").hide();
                            $("#divModal3").hide();
                            $("#MainContent_btnSearch").click();
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }

            callGb = '';
        }; 

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetPolicyExtra') {
                    fn_ControlBind_ChkPolicyExtra(json);

                    callGb = '';
                }
                else if (callGb == 'GetChkPolicyExtra') {
                    fn_ControlBind_PolicyExtra(json);

                    callGb = '';
                }
            }
        };

        var fn_OrderProc = function (odrProc) {
            var chk = false;
            var msg = "";

            if (odrProc == "A") {
                msg = "수령";
                chk = true;
            }
            else if (odrProc == "R") {
                msg = "거부";
                chk = true;
            }
            else if (odrProc == "C") {
                msg = "취소";
                chk = true;
            }
            else if (odrProc == "S") {
                msg = "완료";
                chk = true;
            }
            else if (odrProc == "W") {
                msg = "작업시작";
                chk = true;
            }
            else if (odrProc == "H") {
                msg = "작업보류";
                chk = true;
            }

            if (chk) {
                if (confirm("현재 오더를 " + msg + " 하시겠습니까?")) {
                    var odrno = $("#<%=hdfOrderNo.ClientID %>").val();
                    var acceptyn = $("#<%=hdfAcceptYn.ClientID %>").val();
                    var startyn = $("#<%=hdfStartYn.ClientID %>").val();
                    var Reason = "";

                    if (odrProc == "C" || odrProc == "S") {
                        msg = msg + " 시 처리내용을 입력해주세요.";

                        Reason = $("#<%= txbProcinfo.ClientID %>").val();
                    }
                    else if (odrProc != "A" && odrProc != "W") {
                        msg = msg + " 시 사유 입력해주세요.";

                        Reason = $("#<%= txbReason.ClientID %>").val();
                    }

                    if (odrno.length == 0) {
                        alert("잘못된 접근입니다.");
                    }
                    else if (Reason.length == 0 && odrProc != "A" && odrProc != "W") {
                        alert(msg);
                    }
                    else {
                        var method = "";
                        var aData = [];

                        aData[0] = odrno;

                        if (odrProc == "C") {
                            callGb = 'CancelOk'
                            method = "SetVanOrderCancel";

                            aData[1] = Reason;
                        }
                        else {
                            if (odrProc == "S") {
                                callGb = 'SuccOk'
                            }
                            else if (odrProc == "W") {
                                callGb = 'WorkStartOk'
                            }
                            else if (odrProc == "A") {
                                callGb = 'AcceptOk'
                            }
                            else if (odrProc == "R") {
                                callGb = 'RejectOk'
                            }
                            else if (odrProc == "H") {
                                callGb = 'HoldOk'
                            }

                            method = "SetVanOrderWorkState";
                            aData[0] = odrno;
                            aData[1] = odrProc;
                            aData[2] = Reason;
                            aData[3] = acceptyn
                            aData[4] = startyn
                        }

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = ajaxCallGetUrl(location.href, method);

                        ajaxCallEx(jsonData, ajaxUrl, true);
                    }
                }
            }
            else {
                alert("잘못된 접근입니다.");
            }
        };

        var fn_SetDate = function () {
            if (confirm("일정을 변경 하시겠습니까?")) {
                var reason = $("#<%=txbReason.ClientID %>").val();

                if (reason.length == 0) {
                    alert("일정 변경 사유를 입력해주세요.");
                }
                else {

                    var odrno = $("#<%=hdfOrderNo.ClientID %>").val();
                    var rsvdt = $("#<%= txbRsvDt.ClientID %>").val();
                    var rsvtime = $("#<%= ddlRsvTime.ClientID %> option:selected").val();
                    var prersvdt = $("#<%= hdfRsvDt.ClientID %>").val();

                    callGb = 'DateOk'

                    var aData = [];

                    aData[0] = odrno;
                    aData[1] = rsvdt + " " + rsvtime + ":00:00";
                    aData[2] = reason;
                    aData[3] = prersvdt;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetDate");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        };

        var fn_TransferWorker = function () {
            if (confirm("작업을 이관할 경우 선택한 작업자에게 오더발행 문자가 발송됩니다. 작업이관 하시겠습니까?")) {
                var odrno = $("#<%=hdfOrderNo.ClientID %>").val();
                var empno = $("#<%=hfEmpNo.ClientID %>").val();
                var reason = $("#<%=txbReason.ClientID %>").val();

                if (odrno.length == 0) {
                    alert("잘못된 접근입니다.");
                    window.close();
                }
                else if (empno.length == 0) {
                    alert("작업이관 대상 작업자를 선택해주세요.");
                }
                else if (reason.length == 0) {
                    alert("작업이관 사유를 입력해주세요.");
                }
                else {
                    var aData = [];

                    aData[0] = odrno;
                    aData[1] = empno;
                    aData[2] = reason;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransferWorker');
                    callGb = 'TransOk';

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        };

        var fn_OpenModal = function (state) {
            if (state == "T") {
                $("#divModal").css('height', '330px');
                $("#btnReg").attr("onclick", "fn_TransferWorker();");
                $("#btnReg").val("이관");
                $("#spanEmpInfo").show();

                $("#divModal").show();
            }
            else if (state == "H" || state == "R") {
                $("#divModal").css('height', '260px');
                $("#btnReg").attr("onclick", "fn_OrderProc('" + state + "');");
                if (state == "H") {
                    $("#btnReg").val("보류");
                }
                else {
                    $("#btnReg").val("거부");
                }
                $("#spanEmpInfo").hide();
                $("#divModal").show();
            }
            else if (state == "D") {
                $("#divModal").css('height', '260px');
                $("#btnReg").attr("onclick", "fn_SetDate();");
                $("#btnReg").val("저장");
                $("#spanEmpInfo").hide();
                $("#divModal").show();
            }
            else {
                alert("잘못된 접근입니다.");
            }
        };

        var fn_searchMemo = function () {
            var odrno = $("#<%=hdfOrderNo.ClientID %>").val();

            //입력 파라미터
            var aData = [];

            aData[0] = odrno;
            aData[1] = "N";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid2.getUrl(location.href, 'GetMemoData');

            //grid Bind
            grid2.getDataBind(jsonData, ajaxUrl, 'ktsGrid2');
        };

        var fn_InsMemoInfo = function () {
            var odrno = $("#<%=hdfOrderNo.ClientID %>").val();
            var content = $("#<%= txbContent.ClientID %>").val();

            if (confirm("메모를 등록 하시겠습니까?")) {
                if (odrno == undefined || odrno.length == 0) {
                    alert("잘못된 접근입니다.");
                }
                else if (content == undefined || content.length == 0) {
                    alert("메모를 입력해주세요.");
                }
                else {
                    var aData = [];

                    aData[0] = odrno;
                    aData[1] = content;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'InsMemo');

                    callGb = 'InsMemoOk';
                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        };

        var fn_ModalClose = function () {
            $("#<%= txbReason.ClientID %>").val('');
            $("#<%= hfEmpNo.ClientID %>").val('');
            $("#<%= txbEmpNm.ClientID %>").val('');
            $("#<%= txbSearchEmpNm.ClientID %>").val('');
            $("#divModal").hide();
        };

        var fn_EmpModalOpen = function () {
            $("#divModal2").show();
        };

        var fn_EmpModalClose = function () {
            $("#<%= hfEmpNo.ClientID %>").val('');
            $("#<%= txbEmpNm.ClientID %>").val('');
            $("#<%= txbSearchEmpNm.ClientID %>").val('');
            $("#divModal2").hide();
        };

        var fn_MemoModalOpen = function () {
            $("#divModal1").hide();
            $("#divModal2").hide();
            $("#divModal3").show();

            fn_searchMemo();
        };

        var fn_MemoModalClose = function () {
            $("#divModal3").hide();
        };

        var fn_SetEmpNm = function (orgnm, empno, empnm) {
            $("#<%= hfEmpNo.ClientID %>").val(empno);
            $("#<%= txbEmpNm.ClientID %>").val(orgnm + ' ' + empnm);
            $("#<%= txbSearchEmpNm.ClientID %>").val('');

            $("#divModal2").hide();
        };

        var PageMoveMain = function () {
            var sumupDate = $("#<%= hdfSumupdate.ClientID %>").val();

            var param = {
                pSUMUPDATE: sumupDate
            };

            var form = createForm("/Mobile/OrderInfoByEmp", param);

            form.submit();
        };

        var SaveFileCheck = function () {
            if (confirm("참조 파일들 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        };

        var DelFileCheck = function () {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        };

        var AddFileUpload = function () {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:65% !important;");

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "fileTypeChk('fu" + AddFileUpload.lastAssignedId + "', this.value);");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("accept", "image/*");

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.innerText = "...";
            newFileLabel.setAttribute("style", "width:30px !important;");

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fileTypeChk = function (id, val) {
            var filechk = false;

            var extIndex = val.lastIndexOf('.');

            var extList = ["tiff", "pjp", "jfif", "bmp", "gif", "svg", "png", "xbm", "dib", "jxl", "jpeg", "svgz", "jpg", "webp", "ico", "tif", "pjpeg", "avif"];

            if (extIndex > 1) {

                var fileExt = val.substring(extIndex + 1, val.length).toLowerCase();

                if (extList.indexOf(fileExt) > 0) {
                    filechk = true;
                }
            }
            if (filechk) {
                document.getElementById(id + "-value").value = val;
            }
            else {
                alert("이미지파일만 업로드 가능합니다.");

                document.getElementById(id).value = "";
                document.getElementById(id + "-value").value = "";
            }
        }

        var fn_GetChkPolicyExtra = function () {
            var chkextralist = $("#<%= hdfChkPolicyExtra2.ClientID %>").val();

            var aData = [];

            aData[0] = chkextralist;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetChkPolicyExtra');

            callGb = 'GetChkPolicyExtra';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetPolicyExtra = function () {
            var rcvtype = $("#<%= lblRcvType.ClientID %>").text();
            var device = $("#<%= lblDevice.ClientID %>").text();
            var search = $("#txbExtraSearch").val();
            var extralist = $("#<%= hdfPolicyExtra.ClientID %>").val();

            $("#chkExtraAll").show();
            $("#divExtraList").show();
            $("#divExtraChk").show();
            $("#pExtraSearch").show();
            $("#lblExtraSearch").hide();
            $("#txbExtraSearch").show();
            $("#btnExtraList").hide();
            $("#btnExtraSearh").show();

            $("#pExtraSearch").width('80%');
            $("#lblExtraChk").css('margin-left', '-20px');

            $("#btnExtraAdd").show();

            $("#btnExtraAdd").click(function () {
                var SingleCnt = $("#<%= hdfChkSingleCnt.ClientID %>").val();

                if (SingleCnt > 1) {
                    alert("LAN 노출 가설은 하나만 입력할 수 있습니다.");
                }
                else {
                    $("#<%= hdfChkPolicyExtra2.ClientID %>").val($("#<%= hdfChkPolicyExtra.ClientID %>").val());
                    fn_GetChkPolicyExtra();
                }
            });

            var aData = [];

            aData[0] = rcvtype;
            aData[1] = device;
            aData[2] = search;
            aData[3] = extralist;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPolicyExtra');

            callGb = 'GetPolicyExtra';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_ControlBind_ChkPolicyExtra = function (json) {
            var divExtraList = $("#divExtraList");
            var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');

            var chkExtraList = '';

            divExtraList.empty();

            if (JSON.parse(json).length > 0) {

                $("#btnExtraListClose").show();

                $.each(JSON.parse(json), function (idx, item) {

                    var divParent = document.createElement("div");

                    divParent.id = "divExtra" + item.POLICY_EXTRA_PK;
                    divParent.className = "divExtra";
                    divParent.style.border = "none";
                    divParent.style.backgroundColor = "white";
                    divParent.style.width = "470px";

                    var p = document.createElement("p");

                    p.className = "inpbox";

                    var checkBox = document.createElement("input");

                    checkBox.id = "chkExtra" + item.POLICY_EXTRA_PK;
                    checkBox.name = "chkExtra";
                    checkBox.type = "checkbox";
                    checkBox.className = "checkbox-extra";
                    checkBox.style.width = "20px";
                    checkBox.style.height = "25px";
                    checkBox.style.marginLeft = "10px";
                    checkBox.style.marginRight = "10px";
                    checkBox.value = item.POLICY_EXTRA_PK;

                    for (var i = 0; i < chkExtra.length; i++) {
                        var extraKeyVal = chkExtra[i].split(',');

                        if (extraKeyVal.length > 1) {
                            if (extraKeyVal[0] == item.POLICY_EXTRA_PK) {
                                checkBox.checked = true;

                                if (chkExtraList.length > 0) {
                                    chkExtraList += "|";
                                }

                                chkExtraList += item.POLICY_EXTRA_PK;
                                chkExtraList += "," + extraKeyVal[1];
                            }
                        }
                    }
                    p.append(checkBox);

                    var labelContent = document.createElement("label");

                    labelContent.id = "lblExtraContent" + item.POLICY_EXTRA_PK;
                    labelContent.innerText = item.CONTENT;
                    labelContent.className = "label-extra-content";
                    labelContent.style.fontWeight = "200";
                    labelContent.style.paddingTop = "5px";


                    p.append(labelContent);

                    divParent.append(p);
                    divExtraList.append(divParent);

                    labelContent.onclick = function () {
                        $("#chkExtra" + item.POLICY_EXTRA_PK).click;
                    }

                    var hdfCountYn = document.createElement("input");
                    hdfCountYn.id = "hdfSingleChk" + item.POLICY_EXTRA_PK;
                    hdfCountYn.type = "hidden";
                    hdfCountYn.className = "extra-singlechk";
                    hdfCountYn.value = item.COUNT_YN;

                    p.append(hdfCountYn);
                });

                $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtraList);

                if (chkExtra.length > 0) {
                    var total = $("input[name=chkExtra]").length;
                    var checked = $("input[name=chkExtra]:checked").length;

                    if (total != checked) {
                        $("#chkExtraAll").prop("checked", false);
                    }
                    else {
                        $("#chkExtraAll").prop("checked", true);
                    }
                }

                $("input[name=chkExtra]").click(function () {
                    var total = $("input[name=chkExtra]").length;
                    var checked = $("input[name=chkExtra]:checked").length;

                    if (total != checked) {
                        $("#chkExtraAll").prop("checked", false);
                    }
                    else {
                        $("#chkExtraAll").prop("checked", true);
                    }

                    fn_SetChkExtraList();
                });

                //var button = document.createElement("input");

                //button.id = "btnExtraAdd";
                //button.type = "button";
                //button.className = "btn-green";
                //button.style.width = "100%";
                //button.style.border = "none";
                //button.style.marginTop = "5px";
                //button.value = "가져오기";

                <%--button.onclick = function () {
                    var SingleCnt = $("#<%= hdfChkSingleCnt.ClientID %>").val();

                    if (SingleCnt > 1) {
                        alert("LAN 노출 가설은 하나만 입력할 수 있습니다.");
                    }
                    else {
                        $("#<%= hdfChkPolicyExtra2.ClientID %>").val($("#<%= hdfChkPolicyExtra.ClientID %>").val());
                        fn_GetChkPolicyExtra();
                    }
                }--%>

                //divExtraList.append(button);
            }
            else {
                fn_ExtraListClose();
            }
        };

        var fn_ControlBind_PolicyExtra = function (json) {
            var divExtraList = $("#divExtraList");
            var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');

            var status = $("#<%= hdfStatus.ClientID %>").val();
            var recipttype = $("#<%= lblReceiptType.ClientID %>").text();

            divExtraList.empty();

            $("#divExtraList").show();
            $("#chkExtraAll").hide();
            $("#pExtraSearch").hide();
            $("#lblExtraSearch").hide();
            $("#txbExtraSearch").hide();

            $("#pExtraSearch").width('100%');
            $("#lblExtraChk").css('margin-left', '');

            $("#btnExtraAdd").hide();
            $("#btnExtraAdd").off('click');

            if (recipttype == "VAN365" && status == '3.5') {
                $("#divExtraSearch").show();
                $("#btnExtraList").show();
                $("#btnExtraListClose").hide();
                //$("#btnCATAdd").show();
            } else {
                $("#divExtraSearch").hide();
                $("#btnExtraList").hide();
                $("#btnExtraListClose").hide();
                //$("#btnCATAdd").hide();
            }

            $("#btnExtraSearh").hide();

            if (JSON.parse(json).length > 0) {

                $.each(JSON.parse(json), function (idx, item) {
                    var divParent = document.createElement("div");

                    divParent.id = "divExtra" + item.POLICY_EXTRA_PK;
                    divParent.className = "divExtra";
                    // divParent.style.border = "none";
                    divParent.style.backgroundColor = "white";
                    // divParent.style.width = "430px";
                    divParent.style.paddingBottom = "5px";


                    var hdfPk = document.createElement("input");

                    hdfPk.id = "hdfExtraPk" + item.POLICY_EXTRA_PK;
                    hdfPk.type = "hidden";
                    hdfPk.className = "extra-pk";
                    hdfPk.value = item.POLICY_EXTRA_PK;

                    divParent.append(hdfPk);

                    var p = document.createElement("p");

                    p.className = "inpbox";

                    var labelContent = document.createElement("label");

                    labelContent.id = "lblExtraContent" + item.POLICY_EXTRA_PK;
                    labelContent.innerText = item.CONTENT;
                    labelContent.className = "label-extra-content";
                    labelContent.style.fontWeight = "200";

                    p.append(labelContent);

                    divParent.append(p);

                    var p2 = document.createElement("p");
                    p2.className = "inpbox";

                    var divMinus = document.createElement("div");
                    var divPlus = document.createElement("div");
                    var labelMinus = document.createElement("label");
                    var labelPlus = document.createElement("label");
                    var labelCnt = document.createElement("label");

                    divMinus.className = "div-extra-minus";
                    divPlus.className = "div-extra-plus";

                    labelMinus.className = "label-extra-calc";
                    labelMinus.style.fontWeight = "lighter";
                    labelMinus.style.fontSize = "20px";
                    labelPlus.className = "label-extra-calc";
                    labelPlus.style.fontWeight = "lighter";
                    labelPlus.style.fontSize = "20px";

                    if (recipttype == "VAN365" && status == '3.5') {
                        if (item.COUNT_YN == "false") {
                            divMinus.style.backgroundColor = "#dfdfdf";
                            divPlus.style.backgroundColor = "#dfdfdf";
                        }
                        else {
                            divMinus.style.cursor = "pointer";
                            divPlus.style.cursor = "pointer";

                            divMinus.onclick = function () {
                                fn_ExtraCnt(item.POLICY_EXTRA_PK, "-");
                            }

                            divPlus.onclick = function () {
                                fn_ExtraCnt(item.POLICY_EXTRA_PK, "+");
                            }
                        }
                    }
                    else {
                        divMinus.style.backgroundColor = "#dfdfdf";
                        divPlus.style.backgroundColor = "#dfdfdf";
                    }

                    labelCnt.id = "lblExtra" + item.POLICY_EXTRA_PK;
                    labelCnt.className = "label-extra-cnt";

                    labelCnt.innerText = item.CNT;

                    labelMinus.innerText = "-";
                    labelPlus.innerText = "+";

                    divMinus.append(labelMinus);
                    p2.append(divMinus);

                    p2.append(labelCnt);

                    divPlus.append(labelPlus);
                    p2.append(divPlus);

                    if (recipttype == "VAN365" && status == '3.5') {
                        var divDel = document.createElement("div");
                        divDel.className = "div-extra-del";

                        divDel.onclick = function () {
                            fn_RemoveDivExtra(item.POLICY_EXTRA_PK);
                        }

                        var divDelInner = document.createElement("div");
                        divDelInner.className = "div-extra-del-inner";

                        divDelInner.onclick = function () {
                            fn_RemoveDivExtra(item.POLICY_EXTRA_PK);
                        }

                        divDel.append(divDelInner);

                        p2.append(divDel);
                    }
                    else {
                        divMinus.style.backgroundColor = "#dfdfdf";
                        divPlus.style.backgroundColor = "#dfdfdf";
                    }

                    divParent.append(p2);
                    divExtraList.append(divParent);
                });
            }
            else {
                fn_ExtraListClose();
            }
        };

        var fn_SetChkExtraList = function () {
            var chkExtra = $("input[name=chkExtra]:checked");
            var chkExtralist = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');

            var chkExtraNew = "";
            var SingleCnt = 0;

            $(chkExtra).each(function () {
                var chkVal = $(this).val();
                var Cnt = "1";

                $(this).parent().find('.extra-singlechk').each(function (index, item) {
                    if ($(item).val() == "false" || $(item).val() == false) {
                        SingleCnt += 1;
                    }
                });

                if (chkExtraNew != '') {
                    chkExtraNew += "|";
                }

                if (chkExtralist.length > 0 && chkExtralist != "") {
                    for (var i = 0; i < chkExtralist.length; i++) {
                        var keyVal = chkExtralist[i].split(',');

                        if (keyVal.length == 0 && keyVal[0] == "") {
                            continue;
                        }

                        var key = keyVal[0];
                        var val = keyVal[1];

                        if (chkVal == key) {
                            Cnt = val;
                            break;
                        }
                    }
                }

                chkExtraNew += chkVal + "," + Cnt;
            });

            $("#<%= hdfChkSingleCnt.ClientID %>").val(SingleCnt);
            $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtraNew);
        };

        var fn_radioBoxChk = function (val) {
            $("#rb" + val).prop('checked', true);
            $("#rb" + val).change();
        };

        var fn_ExtraCnt = function (key, calc) {

            var label = $("#lblExtra" + key);

            var Cnt = parseInt(label.text());

            if (calc == "-") {
                Cnt = Cnt - 1;
            }
            else {
                Cnt = Cnt + 1;
            }

            label.text(Cnt);

            if (Cnt <= 0) {
                fn_RemoveDivExtra(key);
            } else {
                var divExtraList = $("#divExtraList");

                var chkExtralist = "";

                //divExtra
                divExtraList.find('.extra-pk').each(function (index, item) {
                    if (chkExtralist.length > 0) {
                        chkExtralist += "|";
                    }

                    var key = $(item).val();
                    var cnt = $("#lblExtra" + key).text();

                    chkExtralist += key + "," + cnt;
                });

                $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtralist);
                $("#<%= hdfChkPolicyExtra2.ClientID %>").val(chkExtralist);
            }
        };

        var fn_RemoveDivExtra = function (delkey) {
            var div = $("#divExtra" + delkey);

            var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');
            var chkExtraNew = "";

            for (var i = 0; i < chkExtra.length; i++) {
                var keyval = chkExtra[i].split(',');

                if (keyval.length > 1) {
                    var key = keyval[0];

                    if (key != delkey) {
                        if (chkExtraNew != '') {
                            chkExtraNew += "|";
                        }

                        chkExtraNew += chkExtra[i];
                    }
                }
            }

            $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtraNew);
            $("#<%= hdfChkPolicyExtra2.ClientID %>").val(chkExtraNew);

            div.remove();

            fn_ExtraListClose();
        };

        var fn_ExtraListClose = function () {
            var chkExtraList = $("#<%= hdfChkPolicyExtra2.ClientID %>").val();

            if (chkExtraList.length > 0) {
                fn_GetChkPolicyExtra();
            }
            else {
                $("#chkExtraAll").show();
                $("#divExtraList").hide();
                $("#divExtraChk").hide();
                $("#pExtraSearch").show();
                $("#lblExtraSearch").show();
                $("#txbExtraSearch").hide();

                $("#pExtraSearch").width('100%');
                $("#lblExtraChk").css('margin-left', '-20px');
                $("#btnExtraListClose").hide();
                $("#divExtraSearch").show();

                var status = $("#<%= hdfStatus.ClientID %>").val();
                var recipttype = $("#<%= lblReceiptType.ClientID %>").text();

                if (recipttype == "VAN365" && status == '3.5') {
                    $("#btnExtraList").show();
                } else {
                    $("#btnExtraList").hide();
                }

                $("#btnExtraSearh").hide();
                $("#divExtraList").empty();
                $("#txbExtraSearch").val('');
                $("#<%= hdfChkSingleCnt.ClientID %>").val(0);
            }            
        };

        var fn_GetPolicyExtraText = function () {
            var ExtraList = "";

            if ($("#<%= hdfChkPolicyExtra2.ClientID %>").val() != '') {
                var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');

                for (var i = 0; i < chkExtra.length; i++) {
                    if (chkExtra[i] != "") {
                        var ExtraText = "";
                        var ExtraCnt = 0;

                        var lblExtraContent = $("#lblExtraContent" + chkExtra[i]);
                        var lblExtra = $("#lblExtra" + chkExtra[i]);

                        ExtraText = lblExtraContent.text();
                        ExtraCnt = lblExtra.text().replace(" ", "");

                        if (ExtraCnt > 0) {
                            if (ExtraList.length > 0) {
                                ExtraList += "| ";
                            }

                            ExtraList += ExtraText + " ^" + ExtraCnt;
                        }
                    }
                }
            }

            return ExtraList;
        };

        var fn_SetCATList = function () {

            var divCatList = $("#divCATList");
            var CatList = $("#<%= hdfCatList.ClientID %>").val().replace(", ", ",").split(",");

            var status = $("#<%= hdfStatus.ClientID %>").val();
            var recipttype = $("#<%= lblReceiptType.ClientID %>").text();

            var CatCnt = 0;

            if (CatList.length > 0) {
                for (var i = 0; i < CatList.length; i++) {
                    var item = CatList[i].replace(" : ", ":").split(":");

                    if (item.length > 1) {

                        CatCnt++;

                        let id = CatCnt;

                        var serial = item[0];
                        var catid = item[1];

                        var divParent = document.createElement("div");

                        divParent.id = "divCat" + CatCnt;
                        divParent.style.border = "none";
                        divParent.style.paddingBottom = "5px";

                        var p = document.createElement("p");

                        p.className = "inpbox";

                        var labelSerial = document.createElement("label");

                        labelSerial.id = "lblSerial" + CatCnt;
                        labelSerial.innerText = "단말기 시리얼 : " + serial;
                        labelSerial.className = "label-extra-content";
                        labelSerial.style.fontWeight = "300";
                        labelSerial.style.width = "110px";
                        labelSerial.style.float = "left";
                        labelSerial.style.paddingTop = "8px";

                        p.append(labelSerial);

                        var labelCatId = document.createElement("label");

                        labelCatId.id = "lblCatId" + CatCnt;
                        labelCatId.innerText = "CAT ID(TID) : " + catid;
                        labelCatId.className = "label-extra-content";
                        labelCatId.style.fontWeight = "300";
                        labelCatId.style.width = "110px";
                        labelCatId.style.float = "left";
                        labelCatId.style.paddingTop = "8px";

                        p.append(labelCatId);

                        divParent.append(p);

                        divCatList.append(divParent);
                    }
                }
            }
        };

        var fn_RegVan365 = function () {
            if (confirm("오더 정보를 수정 하시겠습니까?")) {

                var OrderNo = $("#<%= hdfOrderNo.ClientID %>").val();
                var HolicdayFlag = $("#<%= hdfHoliDayFlag.ClientID %>").val();
                var RsvDateTime = $("#<%= lblRsvDt.ClientID %>").text().replace(" " + HolicdayFlag, "");
                var PolicyExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val();
                var PolicyExtraText = fn_GetPolicyExtraText();

                var Dispatch = $("input[name=rbDispatchChk]:checked").val();
                var SwSetting = $("input[name=rbSWChk]:checked").val();

                var SwSettingHost = '';

                if (SwSetting == '세팅필요') {
                    SwSettingHost = $("#<%= ddlSWSettingHost.ClientID %> option:selected").val();
                }

                var bChk = false;
                var message = "";

                if (OrderNo == "") {
                    message = "잘못된 접근입니다.";
                }
                else {
                    bChk = true;

                    var aData = [];

                    aData[0] = OrderNo;
                    aData[1] = RsvDateTime;//RsvDt + " " + RsvTime + ":00:00";
                    aData[2] = HolicdayFlag;
                    aData[3] = PolicyExtra;
                    aData[4] = PolicyExtraText;
                    aData[5] = Dispatch;
                    aData[6] = SwSetting;
                    aData[7] = SwSettingHost;
                }

                if (bChk) {
                    callGb = 'UpdOk';

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetVan365Info");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
                else {
                    alert(message);
                    return false;
                }
            }
            else {
                return false;
            }
        };

        var fn_ConfirmOpen = function () {
            var odrno = $("#<%=hdfOrderNo.ClientID %>").val();

            var param = {
                pORDERNO: odrno
            };

            var form = createForm("/Mobile/VanConfirmation", param);

            form.submit();
        };
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<div class="page">
			<asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
                    <h2 id="htitle" style="margin-bottom:5px;padding-bottom:5px;">
                        VAN오더상세정보<asp:Label ID="lblWorkState" runat="server"></asp:Label>
			            <input id="btnCancel" runat="server" type="button" class="btn-black" style="width:40px;min-width:40px;height:24px;line-height:24px;padding:0px;margin-top:1px;border-style:none;float:right;" value="취소" />
                    </h2>
			        <table class="pageView">
				        <colgroup>
                            <col style="width:110px;" />
                            <col />
				        </colgroup>
                        <tbody>
					        <tr>
                                <th class="pageViewTh">접수구분</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblReceiptType" runat="server"></asp:Label>
							        </p>
						        </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">등록자소속</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblRegOrgNm" runat="server"></asp:Label>
							        </p>
						        </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">등록자사번</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblRegEmpNo" runat="server"></asp:Label>
							        </p>
						        </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">등록자명</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblRegEmpNm" runat="server"></asp:Label>
							        </p>
						        </td>
					        </tr>
                            <tr>
                                <th class="pageViewTh">접수유형</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblRcvType" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th class="pageViewTh">대리점명</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblAgencyNm" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">파트너사지역</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblPartnersYn" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th class="pageViewTh">사업자이름</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblFranchiseNm" runat="server"></asp:Label>
							        </p>
						        </td>
                            </tr>
                            <tr>
                                <th class="pageViewTh">사업자등록번호</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblCorpNo" runat="server"></asp:Label>
							        </p>
						        </td>
                            </tr>
					        <tr>
                                <th class="pageViewTh">주소</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblAddr" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">상세주소</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblDtlAddr" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
                            <tr>
                                <th class="pageViewTh">전화번호</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblTelNo" runat="server"></asp:Label>
							        </p>
						        </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">휴대폰번호</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblPhoneNo" runat="server"></asp:Label>
							        </p>
						        </td>
                            </tr>
					        <tr id="trDevice" runat="server">
                                <th class="pageViewTh">관련장비</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblDevice" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
                            <tr id="trPolicyBasic" runat="server">
                                <th class="pageViewTh">기본견적</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblPolicyBasic" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trPolicyExtra" runat="server">
                                <th class="pageViewTh">추가견적</th>
                                <th class="pageViewTh">
                                    <input id="btnExtraList" type="button" class="btn-inner" style="height:30px;float:right;" onclick="fn_GetPolicyExtra();" value="불러오기" />
                                    <input id="btnExtraListClose" type="button" class="btn-inner" style="height:30px;float:right;display:none;" onclick="fn_ExtraListClose();" value="닫기" />
                                </th>
                            </tr>
                            <tr id="trPolicyExtraList" runat="server">
                                <td class="pageViewTd" id="tdExtra" colspan="2">
                                    <div id="divExtraSearch" style="margin-bottom:3px;">
                                        <p id="pExtraSearch" class="inpbox" style="width:80%;">
                                            <label id="lblExtraSearch" style="width:100%; height:40px; padding-top:8px; text-align:center; font-weight:300; font-size: 14px; background-color: #DFDFDF;">추가 요청 사항이 없습니다.</label>
                                            <input id="txbExtraSearch" style="width:100%; text-align:left; padding-left: 10px; padding-left: 10px; font-weight:300; font-size: 14px;display:none;" />
                                        </p>
                                        <input id="btnExtraSearh" type="button" class="btn-inner" onclick="fn_GetPolicyExtra();" style="height:30px;display:none;float:right;" value="검색" />
                                    </div>
                                    <div id="divExtraChk">
                                        <p class="inpbox tableInTitle">
                                            <input type="checkbox" id="chkExtraAll" class="checkbox-extra" style="width:20px;margin-top:-5px"/>
                                            <label id="lblExtraChk">작업 내용</label>
                                        </p>
                                    </div>
                                    <div id="divExtraList" style="display:none;overflow-x:auto;overflow-y:hidden;">
                                    </div>
                                    <input id="btnExtraAdd" type="button" class="btn-green" style="display:none;width:100%;border-style:none;margin-top:5px;" value="가져오기"/>
                                </td>
                            </tr>
					        <tr>
                                <th class="pageViewTh">접수일</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblRcvDt" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">방문예정일</th>
                                <td class="pageViewTd">
                                    <span class="inpbox">
                                        <asp:Label ID="lblRsvDt" runat="server" Visible="false"></asp:Label>
                                        <asp:HiddenField ID="hdfRsvDt" runat="server"></asp:HiddenField>
                                        <asp:TextBox ID="txbRsvDt" class="date" runat="server" Width="100px" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                    </span>
                                    <span class="optionbox">
                                        <asp:DropDownList ID="ddlRsvTime" style="width: 60px;height: 30px;line-height: 30px;padding: 0 7px;" runat="server">
                                        </asp:DropDownList>
                                    </span>
                                    <span style="vertical-align:bottom;">
                                        <input id="btnSave" runat="server" type="button" class="btn-green" style="width:45px; min-width:45px; height :30px;line-height:26px; padding: 0; border-style:none;float:right" onclick="fn_SetDate();" value="저장"/>
                                    </span>
                                    <asp:HiddenField ID="hdfHoliDayFlag" runat="server" Value="" />
                                </td>
					        </tr>
                            <tr id="trTerminalYN" runat="server">
                                <th class="pageViewTh">단말기보유여부</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblTerminalYN" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trDispatch" runat="server">
                                <th class="pageViewTh">출동종류</th>
                                <td class="pageViewTd">
                                    <div id="divDispatchNormal" runat="server" class="div-radio" onclick="fn_radioBoxChk('Dispatch_Normal');">
                                        <input type="radio" name="rbDispatchChk" id="rbDispatch_Normal" value="일반" checked="checked" /><label for="rbDispatch_Normal" class="label-rbInner">일반 출동</label>
                                    </div>
                                    <div id="divDispatchEmergency" runat="server" class="div-radio" onclick="fn_radioBoxChk('Dispatch_Emergency');">
                                        <input type="radio" name="rbDispatchChk" id="rbDispatch_Emergency" value="긴급" /><label for="rbDispatch_Emergency" class="label-rbInner">긴급 출동</label>
                                    </div>
                                    <asp:HiddenField ID="hdfDispatch" runat="server" Value="Dispatch_Normal" />
                                </td>
                            </tr>  
                            <tr>
                                <th class="pageViewTh">출동희망일</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblHopeDt" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
					        <tr id="trDlvrDt" runat="server">
                                <th class="pageViewTh">택배배송일</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblDlvrDt" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
                            <tr id="trTermialAddr" runat="server">
                                <th class="pageViewTh">장비발송장소</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">                             
                                        <asp:Label ID="lblTerminalAddr" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
					        <tr>
                                <th class="pageViewTh">접수내용</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
								        <asp:TextBox ID="txbRcvNote" runat="server" MaxLength="8000" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">의뢰사항</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
								        <asp:TextBox ID="txbReqNote" runat="server" MaxLength="8000" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">회수배송주소</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblRtnAddr" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">회수상세주소</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblRtnDtlAddr" runat="server"></asp:Label>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <th class="pageViewTh">특이사항</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
								        <asp:TextBox ID="txbReport" runat="server" MaxLength="8000" style="min-height:100px;height:100px;" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                    </p>
                                </td>
					        </tr>
                            <tr id="trSwContent" runat="server">
                                <th class="pageViewTh">사용SW</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblSwContent" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trSwSetting" runat="server">
                                <th class="pageViewTh">SW세팅여부</th>
                                <td class="pageViewTd">
                                    <div id="divSettingC" runat="server" class="div-radio" onclick="fn_radioBoxChk('SettingC');">
                                        <input type="radio" name="rbSWChk" id="rbSettingC" value="세팅필요" /><label for="rbSettingC" class="label-rbInner">진행</label>
                                    </div>
                                    <div id="divSettingN" runat="server" class="div-radio" onclick="fn_radioBoxChk('SettingN');">
                                        <input type="radio" name="rbSWChk" id="rbSettingN" value="세팅완료" /><label for="rbSettingN" class="label-rbInner">미진행</label>
                                    </div>
                                    <asp:HiddenField ID="hdfSwSetting" runat="server" Value="SettingC" />
                                </td>
                            </tr>
                            <tr id="trSwSettingHost" runat="server">
                                <th class="pageViewTh">SW세팅주체</th>
                                <td class="pageViewTd">
                                    <p class="optionbox">
                                        <asp:DropDownList ID="ddlSWSettingHost" runat="server" Enabled="false">
                                            <asp:ListItem Text="선택하세요" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="엔지니어 자체처리 완결" Value="엔지니어 자체처리 완결"></asp:ListItem>
                                            <asp:ListItem Text="콜센터 지원 미완결" Value="콜센터 지원 미완결"></asp:ListItem>
                                            <asp:ListItem Text="대리점 지원 미진행" Value="대리점 지원 미진행"></asp:ListItem>
                                        </asp:DropDownList>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trVan" runat="server">
                                <th class="pageViewTh">VAN사</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblVan" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trCat" runat="server">
                                <th class="pageViewTh" colspan="2">
                                    <label>단말기&CAT_ID</label>
                                </th>
                            </tr>
                            <tr id="trCatList" runat="server">
                                <td class="pageViewTd" colspan="2">
                                    <div id="divCATList" style="margin-top: 5px; ">
                                    </div>
                                    <asp:HiddenField ID="hdfCatList" runat="server" Value="" />
                                </td>
                            </tr>
                            <tr id="trAspName" runat="server">
                                <th class="pageViewTh">ASP 이름</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblAspName" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trAspID" runat="server">
                                <th class="pageViewTh">ASP ID</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblAspID" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trAspPW" runat="server">
                                <th class="pageViewTh">ASP PW</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblAspPW" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trSolutionName" runat="server">
                                <th class="pageViewTh">솔루션 이름</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblSolutionName" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trSolutionID" runat="server">
                                <th class="pageViewTh">솔루션 ID</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblSolutionID" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trSolutionPW" runat="server">
                                <th class="pageViewTh">솔루션 PW</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
                                        <asp:Label ID="lblSolutionPW" runat="server"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr id="trRegVan365" runat="server">
                                <td class="pageViewTd" colspan="2">
                                    <input id="btnRegVan365" type="button" class="btn-green" style="width:100%;border-style:none;" value="오더정보수정"/>
                                </td>
                            </tr>
					        <tr id="trProcinfo" runat="server">
                                <th class="pageViewTh">처리내역</th>
                                <td class="pageViewTd">
                                    <p class="inpbox">
								        <asp:TextBox ID="txbProcinfo" runat="server" MaxLength="8000" style="min-height:100px;height:100px;" TextMode="MultiLine" placeholder="작업완료/취소 처리 시 반드시 입력해주세요. "></asp:TextBox>
                                    </p>
                                </td>
					        </tr>
					        <tr>
                                <td class="pageViewTd" colspan="2">
                                    <input id="btnMemo" type="button" class="btn-gray" onclick="fn_MemoModalOpen();" value="메모확인"/>
                                </td>
					        </tr>
                            <tr id="trImg" runat="server" visible="false">
                                <th class="pageViewTh" rowspan="2">참조</th>
                                <td class="pageViewTd">
                                    <div style="margin-top:2px; margin-bottom:2px;">
                                        <asp:UpdatePanel ID="updPanelImg" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <span id="spanImg" runat="server">
                                                </span>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
					        <tr id="trFile" runat="server" visible="false">
                                <td class="pageViewTd">
                                    <div class="scrollbox" style="max-height:90px;overflow-y:auto; margin-top:2px; margin-bottom:2px">
                                        <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptFile" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                                        <p><%# Eval("FILENM") %>&nbsp;<asp:Button id="btnDelete" runat="server" OnClick="btnDelete_ServerClick" OnClientClick="return DelFileCheck();" style="color:black;padding-top:5px;" BorderStyle="None" Text="삭제" /></p>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div>
                                        <div id="filebox">
                                            <p class="filebox">
                                                <input type="text" id="fu1-value" style="float:left;width:65% !important; margin-top:2px;" />
                                                <span class="file">
                                                    <input type="file" accept="image/*" id="fu1" name="fu1" onchange="fileTypeChk('fu1', this.value);" /> 
                                                    <label for="fu1" style="width:30px !important;height:31px;line-height:31px;margin-top:3px;">...</label>
                                                </span>
                                                <button type="button" onclick="AddFileUpload();" class="btn-replysave" style="height:31px;line-height:31px;margin-top:3px;margin-left:0;float:right;">추가</button>
                                            </p>
                                        </div>
                                        <asp:Button id="btnImgSave" runat="server" OnClick="btnImgSave_ServerClick" OnClientClick="return SaveFileCheck();" style="width:40px;height:26px;line-height:26px;margin-top:3px;padding-left:8px; border-style:none;float:right" class="btn-green" BorderStyle="None" Text="사진저장" />
                                    </div>
                                </td>
					        </tr>
					        <tr id="trConfirm" runat="server" visible="false">
                                <td class="pageViewTd" colspan="2">
                                    <input id="btnConfirm" type="button" runat="server" class="btn-green" style="width:100%;border-style:none;" visible="false" value="설치확인서작성"/>
                                    <asp:Label ID="lblConfYn" runat="server" Visible="false" style="font-size:13px;font-weight:bold" Text='설치확인서 작성완료'></asp:Label>
                                </td>
					        </tr>
				        </tbody>
			        </table>
			        <div style="text-align:center;margin-top:40px;">
                        <input id="btnAcpt" runat="server" type="button" class="btn-green" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="작업수령"/>
                        <input id="btnRjct" runat="server" type="button" class="btn-black" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="작업거부"/>
                        <input id="btnSucc" runat="server" type="button" class="btn-green" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="작업완료"/>
                        <input id="btnHold" runat="server" type="button" class="btn-gray" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="작업보류"/>
                        <input id="btnTran" runat="server" type="button" class="btn-gray" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="이관"/>
                        <input id="btnBack" type="button" class="btn-black" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" onclick="PageMoveMain();" value="이전화면"/>
				        <asp:Button ID="btnSearch" runat="server" class="btn-green" style="display:none;" OnClick="btnSearch_Click" />
			        </div>
                    <asp:HiddenField ID="hdfOrderNo" runat="server" />
                    <asp:HiddenField ID="hdfStatus" runat="server"/>
                    <asp:HiddenField ID="hdfSumupdate" runat="server" />
                    <asp:HiddenField ID="hdfAcceptYn" runat="server" />
                    <asp:HiddenField ID="hdfStartYn" runat="server" />
                    <asp:HiddenField ID="hdfPolicyExtra" runat="server"/>
                    <asp:HiddenField ID="hdfChkPolicyExtra" runat="server"/>
                    <asp:HiddenField ID="hdfChkPolicyExtra2" runat="server"/>
                    <asp:HiddenField ID="hdfChkSingleCnt" runat="server"/>
                </ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
					<asp:PostBackTrigger ControlID="btnImgSave"/>
				</Triggers>
            </asp:UpdatePanel>

            <!-- S: divModal -->
			<div id="divModal" class="scrollbox" style="display:none;position:fixed;width:100%;height:330px;top:20%;left:0;background:rgb(255, 255, 255); border: 1px solid black; border-radius:10px;">  
                <div id="divModalBg1" style="z-index: 97;position: fixed;top: 0;width: 100%;height: 100%;background-color: #ffffff !important;opacity: 0.7;"></div>              
				<div class="requestlist" style="z-index: 98;position: fixed;width:90%;text-align:center;margin-left:5%">
                    <div class="datalist" style="margin-top:5px;padding-bottom:5px;margin-bottom: 15px">
						<asp:HiddenField ID="hfRcvType" runat="server" />
						<span id="spanEmpInfo" class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0; text-align:left;">
							<label style="margin:0 !important; padding-top:7px;margin-left:0;">작업자변경</label>
							<asp:TextBox ID="txbEmpNm" runat="server" ReadOnly="true" onclick="fn_EmpModalOpen();" Width="100%"></asp:TextBox> 
							<asp:HiddenField ID="hfEmpNo" runat="server" />
						</span>
						<span class="inpbox" style="width:100%; margin:3px 0 3px 0; font-size:0; text-align:left;">
							<label style="margin:0 !important;padding-top:7px;margin-left:0;">사유</label>
							<asp:TextBox ID="txbReason" runat="server" MaxLength="8000" TextMode="MultiLine" placeholder="사유를 입력해주세요."></asp:TextBox> 
						</span>
					</div>
					<input id="btnReg" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" />
					<input id="btnModalClose" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ModalClose();" value="닫기" />
				</div>	
			</div>			
			<div id="divModal2" class="scrollbox" style="display:none;position:fixed;width:100%;height:480px;top:5%;left:0;background:rgb(255, 255, 255); border: 1px solid black; border-radius:10px;overflow-y:scroll;">            
				<div class="requestlist" style="z-index: 99;position: fixed;width:90%;text-align:center;margin-left:5%">
					<span class="inpbox" style="width:100%; margin:3px 0 3px 0; padding-top:20px; font-size:0;">
						<label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">사원명</label>
						<asp:TextBox ID="txbSearchEmpNm" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
						<input id="btnEmpSelect" class="btn-green" style="width:33px;margin:0 !important;color:white;border-style:none;" value="조회" />
					</span>

					<div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<!-- S:scrollbox -->
						<div class="scrollbox">
							<div id="ktsGrid1"></div>
						</div>
						<!-- E:scrollbox -->
					</div>

					<input id="btnEmpModalClose" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_EmpModalClose()" value="닫기" />
				</div>
			</div>
			<div id="divModal3" class="scrollbox" style="display:none;position:fixed;width:100%;height:480px;top:5%;left:0;background:rgb(255, 255, 255); border: 1px solid black; border-radius:10px;overflow-y:scroll;">            
                <div id="divModalBg2" style="z-index: 98;position: fixed;top: 0;width: 100%;height: 100%;background-color: #ffffff !important;opacity: 0.7;"></div>      
				<div class="requestlist" style="z-index: 99;position: fixed;width:90%;text-align:center;margin-left:5%">                    
				    <p style="color:black;text-align:left;font-weight:bold;margin-top: 10px;">메모</p>
					<div class="datalist" style="margin-top:10px;padding-bottom:10px;">
						<!-- S:scrollbox -->
						<div class="scrollbox">
							<div id="ktsGrid2"></div>
						</div>
						<!-- E:scrollbox -->
					</div>
                    
                    <asp:UpdatePanel ID="updPanelMemo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <span id="spanMemo" runat="server" class="inpbox" style="width:100%; margin:3px 0 3px 0; padding-top:20px; font-size:0;">
						        <label style="margin:0 !important;padding-top:7px;padding-right:10px;margin-left:0;float:left">메모등록</label>
						        <asp:TextBox ID="txbContent" runat="server" MaxLength="50" style="width:60% !important;float:left"></asp:TextBox>
						        <input id="btnMemoSave" type="button" onclick="fn_InsMemoInfo();" class="btn-green" style="width:33px;margin-top:2px;color:white;border-style:none;background-color:#2CBBB7 !important" value="저장" />
					        </span>
                        </ContentTemplate>
                    </asp:UpdatePanel>

					<input id="btnMemoModalClose" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_MemoModalClose()" value="닫기" />
				</div>
			</div>
			<!--E: divModal -->
		</div>		        
	</div>
	<!--//E: contentsarea -->
</asp:Content>