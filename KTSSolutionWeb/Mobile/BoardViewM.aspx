﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Board.Master" AutoEventWireup="true" CodeBehind="BoardViewM.aspx.cs" Inherits="KTSSolutionWeb.BoardViewM" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function BoardWrite(boardid, boardtype, writerid, empno, authid) {
            var param = {
                pBOARDID: boardid,
                pBOARDTYPE: boardtype,
                pWRITERID: writerid,
                pEMPNO: empno,
                pAUTHID: authid
            };

            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/BoardWriteM");

            if (param != null) {
                for (var name in param) {
                    var valObj = param[name];
                    addInput(form, name, valObj);
                }
            }

            form.submit();
        }

        function DelCheck() {
            if (confirm("현재 게시글을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ReRegChk() {
            if (confirm("댓글을 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ReDelChk() {
            if (confirm("댓글을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function FileDownload() {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }

        function MoblieFileDownload(fileinfo, logintype) {
            if (logintype == "mkate") {
                document.location.href = "mkate://download?info=" + fileinfo;
            }
            else {
                document.location.href = fileinfo;
            }
        }

        function BoardList() {
            var form = document.forms["form1"];

            form.setAttribute("method", "post");
            form.setAttribute("action", "/Mobile/BoardListM");

            form.submit();
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<h2><strong id="sTitle" runat="server"></strong></h2>
        <div class="boardview">
            <strong>작성자 : <span id="pWriterNm" runat="server"></span></strong>

            <asp:HiddenField ID="hfImgWidth" runat="server" />
            <asp:UpdatePanel ID="updPanelImg" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <span id="spanImg" runat="server">
                    </span>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="contents-text" id="divContent" runat="server" style="padding-bottom:15px;border-bottom:2px solid #dfdfdf;">
            </div>

            <div class="contents-text" id="divFileList" runat="server" style="padding-top:15px; max-height:70px;overflow-y:auto;">
                <strong>첨부파일</strong>
                <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Repeater ID="rptFile" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                <p><a id="btnDownload" runat="server" onserverclick="BtnDownload_ServerClick" ><%# Eval("FILENM") %></a>&nbsp;</p>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <!-- S: comment -->
            <div class="comment">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="comment-list">
                            <asp:Repeater ID="rptReply" runat="server">
                                <ItemTemplate>
                                    <dl class="comment-item" style="min-height:33px;max-height:80px;overflow-y:auto">
                                        <asp:Button id="btnReDel" runat="server" OnClientClick="setScroll();return ReDelChk();" OnClick="btnReDel_ServerClick" class="btn-replydel" style="margin-right:2px;float:right" Text="삭제" />
                                        <asp:Button id="btnReReg" runat="server" OnClientClick="setScroll();return ReRegChk();" OnClick="btnReReg_ServerClick" class="btn-replysave last" style="margin-left:2px;margin-right:2px;float:right" Text="저장" />
                                        <dt><%# Eval("WRITERNM") %></dt>
                                        <asp:Label ID="lblReSeq" runat="server" Visible="false" Text='<%# Eval("REPLYSEQ") %>'></asp:Label>
                                        <asp:Label ID="lblReEmpNo" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                        <dd id="ddReply" runat="server"><asp:Label ID="lblContents" runat="server" Text='<%# Eval("CONTENTS") %>'></asp:Label></dd>
                                        <%--<input type="text" id="txbRePly" runat="server" style="width:60% !important" value='<%# Eval("CONTENTS") %>' visible="false" />--%>
                                        <asp:TextBox id="txbRePly" runat="server" style="margin-left:50px;padding-top:4px;width:62%;min-height:40px;max-height:78px" TextMode="MultiLine" Text='<%# Eval("CONTENTS") %>' visible="false" ></asp:TextBox>

                                    </dl>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- //E: comment -->
                
            <!-- S: button -->
            <div style="margin-top:10px;text-align:center">
                <asp:UpdatePanel ID="updPanelBtn" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button id="btnRegBoard" runat="server" style="margin-right:5px" class="btn-green" BorderStyle="None" Text="수정" />
                        <asp:Button id="btnDelBoard" runat="server" OnClientClick="return DelCheck();" style="margin-right:5px" class="btn-black" BorderStyle="None" Text="삭제" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button id="btnFileDownload" OnClick="BtnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
            </div>
            <!-- //E: button -->
        </div>
	</div>
	<!--//E: contentsarea -->
</asp:Content>