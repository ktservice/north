﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.IO;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTSSolutionWeb
{
    public partial class SafetyOutM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                Session["TESTSAMPLE"] = "Test";

                if (!IsPostBack)
                {
                    SetPageInit();
                }
            }
        }


        private void SetPageInit()
        {
            try
            {
                this.spanEmpNo.Style.Remove("display");

                this.txtEmpNoS.Text = Session["EMPNO"].ToString();
                this.txtEmpNmS.Text = Session["EMPNM"].ToString();

                //this.txtEmpNoS.ReadOnly = true;
                //this.txtEmpNmS.ReadOnly = true;

            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetEmp(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "USERORGINFO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYOUTM";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

    }
}