﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="WorkingClothInfo.aspx.cs" Inherits="KTSSolutionWeb.WorkingClothInfo" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function SearchChk() {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length > 0 && PointSeq.length > 0) {
                return true;
            } else {
                alert("연도와 지급기준을 선택해주세요.");
                return false;
            }
        }

        function RegRequestCloth(strYears, strPointSeq, strEmpNo, strReqType) {

            var param = {
                YEARS: strYears,
                POINTSEQ: strPointSeq,
                EMPNO: strEmpNo,
                REQTYPE: strReqType
            };

            $("#form1").attr("method", "post");
            $("#form1").attr("action", "/Mobile/WorkingClothReq");

            for (var name in param) {
                var valObj = param[name];

                var input = document.createElement("input");

                input.setAttribute("name", name);
                input.setAttribute("type", "hidden");
                input.setAttribute("value", valObj);

                $("#form1").append(input);
            }

            $("#form1").target = self;

            $("#form1").submit();
            $("#form1").removeAttr("action");
        }

        function RegRequestPassAll() {
            var Years = $("#<%= ddlYears.ClientID %> option:selected").val();
            var PointSeq = $("#<%= ddlPointSeq.ClientID %> option:selected").val();

            if (Years.length > 0 && PointSeq.length > 0) {
                var PointSeqTxt = $("#<%= ddlPointSeq.ClientID %> option:selected").text();
                var confMsg = Years + "년도 " + PointSeqTxt + "을 일괄미신청 하시겠습니까?";
                if (confirm(confMsg)) {
                    return true;
                }
                else {
                    return false;
                }
            } else {
                alert("잘못된 접근입니다.");
                return false;
            }
        }
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <!-- S:page-clothes-request -->
        <div class="page-clothes-list">
			<h2>피복신청</h2>
			<!-- S:fieldset -->
			<asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional" style="margin-bottom:10px;">
				<ContentTemplate>
			        <fieldset>
						<span class="optionbox" style="width:80%">
							<label style="width:50px">연도</label>
							<asp:DropDownList style="width:80%" ID="ddlYears" runat="server" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
						</span>
			        </fieldset>
			        <fieldset>                            
						<span class="optionbox" style="width:80%">
							<label style="width:50px">지급기준</label>
							<asp:DropDownList ID="ddlPointSeq" runat="server">
                                <asp:ListItem Selected="True" Text="선택하세요." Value=""></asp:ListItem>
							</asp:DropDownList>
						</span>
					    <span style="width:20%; margin:0; font-size:0; text-align:center; float:right;">
                            <asp:Button ID="btnSearh" runat="server" OnClientClick="return SearchChk();" OnClick="btnSearh_Click" class="btn-green" BorderStyle="None" Text="조회" />
                        </span>
			        </fieldset>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearh" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>
			<!-- E:feileset -->

			<asp:UpdatePanel ID="updPanelBtn" runat="server" UpdateMode="Conditional" style="margin-bottom:10px;text-align:right;">
				<ContentTemplate>
                    <asp:Button ID="btnPassAll" runat="server" style="margin-left:5px;" class="btn-gray" BorderStyle="None" OnClientClick="return false;" OnClick="btnPassAll_ServerClick" Visible="false" Text="일괄미신청" />
                    <asp:Button ID="btnReq" runat="server" style="margin-left:5px;" class="btn-green" BorderStyle="None" Visible="false" Text="신청" />
                    <%--<button id="btnPassAll" runat="server" class="btn-gray" style="margin-left:5px;border-style:none;" visible="false">일괄미신청</button>--%>
                    <%--<button id="btnReq" runat="server" class="btn-green" style="margin-left:5px;border-style:none;" visible="false">신청</button>--%>
			    </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlYears" EventName="SelectedIndexChanged" />
                </Triggers>                
		    </asp:UpdatePanel>
			
            <!-- S: scrollBox -->
            <div class="scrollbox">
                <div class="requestlist">
                    <div class="datalist">
						<asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
							<ContentTemplate>	
								<table>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr id="tr1" runat="server">
                                                    <th style="min-width:15px; padding:10px 15px 10px 0px;">동/하복</th>
                                                    <td style="min-width:15px; padding:10px 15px 10px 0px;"><%# Eval("CLOTHGBN") %></td>
                                                    <th style="min-width:15px; padding:10px 15px 10px 0px;">종류</th>
                                                    <td style="min-width:15px; padding:10px 15px 10px 0px;"><%# Eval("CLOTHTYPE") %></td>
                                                    <th style="min-width:15px; padding:10px 15px 10px 0px;">사이즈</th>
                                                    <td style="min-width:15px; padding:10px 15px 10px 0px;"><%# Eval("CLOTHSIZE") %></td>
                                                </tr>
                                                <tr id="tr2" runat="server">
                                                    <th style="min-width:15px; padding:10px 15px 10px 0px; border-bottom:1px solid #000;">수량</th>
                                                    <td style="min-width:15px; padding:10px 15px 10px 0px; border-bottom:1px solid #000;"><%# Eval("REQCNT") %></td>
                                                    <th style="min-width:15px; padding:10px 15px 10px 0px; border-bottom:1px solid #000;">상태</th>
                                                    <td id="tdReqStat" runat="server" style="min-width:15px; padding:10px 15px 10px 0px; border-bottom:1px solid #000;">
                                                        <%# Eval("REQSTATUS") %>
                                                        <asp:Label ID="lblYears" runat="server" Visible="false" Text='<%# Eval("YEARS") %>'></asp:Label>
                                                        <asp:Label ID="lblPointSeq" runat="server" Visible="false" Text='<%# Eval("POINTSEQ") %>'></asp:Label>
                                                        <asp:Label ID="lblEmpNo" runat="server" Visible="false" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                                        <asp:Label ID="lblReqType" runat="server" Visible="false" Text='<%# Eval("REQTYPE") %>'></asp:Label>
                                                    </td>
                                                    <th id="thPoint" runat="server" style="min-width:15px; padding:10px 15px 10px 0px; border-bottom:1px solid #000;">포인트</th>
                                                    <td id="tdPoint" runat="server" style="min-width:15px; padding:10px 15px 10px 0px; border-bottom:1px solid #000;"><%# Eval("USEPOINT") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
								</table>
							</ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSearh" EventName="click" />
                                <asp:AsyncPostBackTrigger ControlID="btnPassAll" EventName="click" />
                            </Triggers>
						</asp:UpdatePanel>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<!--//E: contentsarea -->
</asp:Content>