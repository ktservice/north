﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;

namespace KTSSolutionWeb
{
    public partial class MaterialStatByHoldingEmpM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetPageInit();
                }
            }
        }


        private void SetPageInit()
        {
            try
            {
                this.spanEmpNo.Style.Remove("display");
                this.spanEmpNm.Style.Remove("display");

                this.txbEmpNo.Text = Session["EMPNO"].ToString();
                this.txbEmpNm.Text = Session["EMPNM"].ToString();

                this.txbEmpNo.ReadOnly = true;
                this.txbEmpNm.ReadOnly = true;

            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "MATERIALSTATBYHOLDINGFORMOBILE";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            DataSet dataSet = new DataSet();

            dataSet = dbTransaction.GetDataSet();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "조회된 보유 이력이 없습니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
                       
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}