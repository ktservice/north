﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PortalCsResultM.aspx.cs" Inherits="KTSSolutionWeb.PortalCsResultM" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="../Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabulator.min.css" rel="stylesheet" />
    <link href="../Resource/css/notice_M.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
       
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };
                $("#txtMdate").monthpicker(options);

                $("#txtMdate").change(function () {
                    fn_Data();
                });
            }
        };

        var fn_Init = function () {
            //$("#lblContentTitle").text('CS 실적 조회');

            $(".table-option").css('display', 'none');
            $(".mdate").val($("#<%=hdnMdate.ClientID %>").val());
        }

        var fn_Load = function () {
            fn_NoticeInfo('CSR');
        }

        var ajaxCallBack = function (json) {
            if (callGb == 'CSR') {
                $.each(JSON.parse(json), function (idx, item) {
                    $('.noticetitletext').html(replaceBrTag(item.TITLE));
                    $('.noticecontenttext').html(replaceBrTag(item.CONTENTS));
                });

                callGb = '';

                fn_Data();
            }
            else if (callGb == 'DATA') {
                var vTemp = '';

                $.each(JSON.parse(json), function (idx, item) {
                    $('#lblSalesOpen').text(priceToString(item.SALES_OPEN));
                    $('#lblPointOpen').text(priceToString(item.POINT_OPEN));
                    $('#lblSalesOpenAvg').text(priceToString(item.SALES_OPEN_AVG));
                    $('#lblPointOpenAvg').text(priceToString(item.POINT_OPEN_AVG));

                    $('#lblSalesAs').text(priceToString(item.SALES_AS));
                    $('#lblPointAs').text(priceToString(item.POINT_AS));
                    $('#lblSalesAsAvg').text(priceToString(item.SALES_AS_AVG));
                    $('#lblPointAsAvg').text(priceToString(item.POINT_AS_AVG));

                    $('#lblSalesMan').text(priceToString(item.SALES_MAN));
                    $('#lblPointMan').text(priceToString(item.POINT_MAN));
                    $('#lblSalesManAvg').text(priceToString(item.SALES_MAN_AVG));
                    $('#lblPointManAvg').text(priceToString(item.POINT_MAN_AVG));

                    $('#lblSales4pgw').text(priceToString(item.SALES_4PAGW));
                    $('#lblPoint4pgw').text(priceToString(item.POINT_4PAGW));
                    $('#lblSales4pgwAvg').text(priceToString(item.SALES_4PAGW_AVG));
                    $('#lblPoint4pgwAvg').text(priceToString(item.POINT_4PAGW_AVG));

                    $('#lblSalesGigaEyes').text(priceToString(item.SALES_GIGAEYES));
                    $('#lblPointGigaEyes').text(priceToString(item.POINT_GIGAEYES));
                    $('#lblSalesGigaEyesAvg').text(priceToString(item.SALES_GIGAEYES_AVG));
                    $('#lblPointGigaEyesAvg').text(priceToString(item.POINT_GIGAEYES_AVG));

                    $('#lblSales2people1').text(priceToString(item.SALES_2PEOPLE1));
                    $('#lblPoint2people1').text(priceToString(item.POINT_2PEOPLE1));
                    $('#lblSales2people1Avg').text(priceToString(item.SALES_2PEOPLE1_AVG));
                    $('#lblPoint2people1Avg').text(priceToString(item.POINT_2PEOPLE1_AVG));

                    $('#lblSalesCancel').text(priceToString(item.SALES_CANCEL));
                    $('#lblPointCancel').text(priceToString(item.POINT_CANCEL));
                    $('#lblSalesCancelAvg').text(priceToString(item.SALES_CANCEL_AVG));
                    $('#lblPointCancelAvg').text(priceToString(item.POINT_CANCEL_AVG));

                    $('#lblSalesBucket').text(priceToString(item.SALES_BUCKET));
                    $('#lblPointBucket').text(priceToString(item.POINT_BUCKET));
                    $('#lblSalesBucketAvg').text(priceToString(item.SALES_BUCKET_AVG));
                    $('#lblPointBucketAvg').text(priceToString(item.POINT_BUCKET_AVG));

                    $('#lblSalesSinhosu').text(priceToString(item.SALES_SINHOSU));
                    $('#lblPointSinhosu').text(priceToString(item.POINT_SINHOSU));
                    $('#lblSalesSinhosuAvg').text(priceToString(item.SALES_SINHOSU_AVG));
                    $('#lblPointSinhosuAvg').text(priceToString(item.POINT_SINHOSU_AVG));

                    $('#lblSalesDispatch').text(priceToString(item.SALES_DISPATCH));
                    $('#lblPointDispatch').text(priceToString(item.POINT_DISPATCH));
                    $('#lblSalesDispatchAvg').text(priceToString(item.SALES_DISPATCH_AVG));
                    $('#lblPointDispatchAvg').text(priceToString(item.POINT_DISPATCH_AVG));

                    $('#lblSalesVan').text(priceToString(item.SALES_VAN));
                    $('#lblPointVan').text(priceToString(item.POINT_VAN));
                    $('#lblSalesVanAvg').text(priceToString(item.SALES_VAN_AVG));
                    $('#lblPointVanAvg').text(priceToString(item.POINT_VAN_AVG));

                    $('#lblSalesWifi').text(priceToString(item.SALES_WIFI));
                    $('#lblPointWifi').text(priceToString(item.POINT_WIFI));
                    $('#lblSalesWifiAvg').text(priceToString(item.SALES_WIFI_AVG));
                    $('#lblPointWifiAvg').text(priceToString(item.POINT_WIFI_AVG));

                    $('#lblSalesDanja').text(priceToString(item.SALES_DANJA));
                    $('#lblPointDanja').text(priceToString(item.POINT_DANJA));
                    $('#lblSalesDanjaAvg').text(priceToString(item.SALES_DANJA_AVG));
                    $('#lblPointDanjaAvg').text(priceToString(item.POINT_DANJA_AVG));

                    /*
                    $('#lblSalesTerminal').text(priceToString(item.SALES_TERMINAL));
                    $('#lblPointTerminal').text('');
                    $('#lblSalesTerminalAvg').text(priceToString(item.SALES_TERMINAL_AVG));
                    $('#lblPointTerminalAvg').text('');
                    */

                    $('#lblSalesPairing').text(priceToString(item.SALES_PAIRING));
                    $('#lblPointPairing').text(priceToString(item.POINT_PAIRING));
                    $('#lblSalesPairingAvg').text(priceToString(item.SALES_PAIRING_AVG));
                    $('#lblPointPairingAvg').text(priceToString(item.POINT_PAIRING_AVG));

                    /*
                    $('#lblSalesEvent1').text(priceToString(item.SALES_EVENT1));
                    $('#lblPointEvent1').text(priceToString(item.POINT_EVENT1));
                    $('#lblSalesEvent1Avg').text(priceToString(item.SALES_EVENT1_AVG));
                    $('#lblPointEvent1Avg').text(priceToString(item.POINT_EVENT1_AVG));

                    $('#lblSalesEvent2').text(priceToString(item.SALES_EVENT2));
                    $('#lblPointEvent2').text(priceToString(item.POINT_EVENT2));
                    $('#lblSalesEvent2Avg').text(priceToString(item.SALES_EVENT2_AVG));
                    $('#lblPointEvent2Avg').text(priceToString(item.POINT_EVENT2_AVG));
                    */


                    $('#lblSalesSum').text(priceToString(item.SALES_SUM));
                    $('#lblPointSum').text(priceToString(item.POINT_SUM));
                    $('#lblSalesSumAvg').text(priceToString(item.SALES_SUM_AVG));
                    $('#lblPointSumAvg').text(priceToString(item.POINT_SUM_AVG));

                    $('#lblCntOpen').text(priceToString(item.CNT_OPEN));
                    $('#lblCntAs').text(priceToString(item.CNT_AS));
                    $('#lblCntOpenAvg').text(priceToString(item.CNT_OPEN_AVG));
                    $('#lblCntAsAvg').text(priceToString(item.CNT_AS_AVG));

                    $('#lblCntSum').text(priceToString(item.CNT_SUM));
                    $('#lblCntSumAvg').text(priceToString(item.CNT_SUM_AVG));


                    $('#lblSalesCsResult').text(priceToString(item.SALES_CSRESULT));
                    $('#lblSalesTerminal').text(priceToString(item.SALES_TERMINAL));

                    $('#lblRegDt').text(item.REGDT);

                    vTemp = item.REGDT;
                });

                if (vTemp != '') {
                    $('#divData').show();
                    $('#divAlert').hide();
                    $('#divAlert').css('display', 'none');
                }
                else {
                    $('#divData').hide();
                    $('#divAlert').show();
                    $('#divAlert').css('display', 'block');
                    $('#lblRegDt').text('');
                }

                callGb = '';
            }

        }

        function replaceBrTag(str)
        {
            if (str == undefined || str == null) {
                return "";
            }

            str = str.replace(/\r\n/ig, '<br>');
            str = str.replace(/\\n/ig, '<br>');
            str = str.replace(/\n/ig, '<br>');
            return str;
        }

        var fn_NoticeInfo = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];
            aData[0] = gb;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetNoticeInfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function () {
            callGb = 'DATA';

            //입력 파라미터
            var aData = [];
            aData[0] = $(".mdate").val();

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }
    </script>

	<!-- S: contentsarea -->
    <div class="contentsarea">
        <div>
            <!-- S:UserControlTitle -->
            <uc:menu ID="menu" runat="server" />
            <!-- E:UserControlTitle -->
            <div class="noticebox">
                <div class="noticetitle">
                    <span class="noticetitletext"></span>
                </div>
                <div class="noticecontent">
                    <span class="noticecontenttext"></span>
                </div>
            </div>
            <h2></h2>
            <div class="topContent">
                <!-- S:fieldset -->
                <fieldset>
                    <legend>조회조건</legend>
                    <span class="inpbox fieldsetSpan">
                        <label class="fieldsetLabel" for="txtMdate">월</label>
                        <input type="text" id="txtMdate" class="mdate" />
                        <input type="hidden" id="hdnMdate" runat="server" />
                    </span>
                    <span>
                        <label id="lblRegDt" class="fieldsetLabel2"></label></span>
                </fieldset>
                <!-- //E:fieldset -->
            </div>
            <h2></h2>
            <!-- S: scrollBox -->
            <div class="scrollbox">
                <fieldset class="view-data-list-body">
                    <div class="data-group">
                        <div id="divData">
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">당월 건수 조회</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header table-header td_width10 border_right">매출</th>
                                                <th class="table-header td_width45 border_right">개요</th>
                                                <th class="table-header">전사평균</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">개통</td>
                                                <td class="table-cell td_width45 border_right"><span id="lblCntOpen"></span></td>
                                                <td class="table-cell last"><span id="lblCntOpenAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">AS</td>
                                                <td class="table-cell td_width45 border_right"><span id="lblCntAs"></span></td>
                                                <td class="table-cell last"><span id="lblCntAsAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 td_bordertop border_right" style="font-weight: bold;">계</td>
                                                <td class="table-cell td_width45 td_bordertop border_right"><span id="lblCntSum"></span></td>
                                                <td class="table-cell td_bordertop last"><span id="lblCntSumAvg"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">전월 매출 조회</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header table-header td_width10 border_right">매출</th>
                                                <th colspan="2" class="table-header border_right">개요</th>
                                                <th colspan="2" class="table-header">전사평균</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">개통</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesOpen"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointOpen"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesOpenAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointOpenAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">AS</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesAs"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointAs"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesAsAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointAsAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">수작업</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesMan"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointMan"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesManAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointManAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">4P-AGW</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSales4pgw"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPoint4pgw"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSales4pgwAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPoint4pgwAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">GIGAEyes</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesGigaEyes"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointGigaEyes"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesGigaEyesAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointGigaEyesAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">2인1조</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSales2people1"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPoint2people1"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSales2people1Avg"></span></td>
                                                <td class="table-cell last"><span id="lblPoint2people1Avg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">무효화</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesCancel"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointCancel"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesCancelAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointCancelAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">버킷</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesBucket"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointBucket"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesBucketAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointBucketAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">신호수</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesSinhosu"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointSinhosu"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesSinhosuAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointSinhosuAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">파견</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesDispatch"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointDispatch"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesDispatchAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointDispatchAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">VAN</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesVan"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointVan"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesVanAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointVanAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">공공WiFi</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesWifi"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointWifi"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesWifiAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointWifiAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">하이오더</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesDanja"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointDanja"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesDanjaAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointDanjaAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">페어링차감</td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesPairing"></span></td>
                                                <td class="table-cell td_width10 border_right"><span id="lblPointPairing"></span></td>
                                                <td class="table-cell td_width35 border_right"><span id="lblSalesPairingAvg"></span></td>
                                                <td class="table-cell last"><span id="lblPointPairingAvg"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 td_bordertop border_right" style="font-weight: bold;">계</td>
                                                <td class="table-cell td_width35 td_bordertop border_right"><span id="lblSalesSum"></span></td>
                                                <td class="table-cell td_width10 td_bordertop border_right"><span id="lblPointSum"></span></td>
                                                <td class="table-cell td_width35 td_bordertop border_right"><span id="lblSalesSumAvg"></span></td>
                                                <td class="table-cell td_bordertop last"><span id="lblPointSumAvg"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <legend class="fieldset-legend"><span class="legend">전월 실적급 조회</span></legend>
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header table-header td_width10 border_right">매출</th>
                                                <th class="table-header">금액</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="table-cell td_width10 border_right" style="font-weight: bold;">CS실적급</td>
                                                <td class="table-cell last"><span id="lblSalesCsResult"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="table-cell td_width10 td_bordertop border_right" style="font-weight: bold;">단말실적급</td>
                                                <td class="table-cell td_bordertop last"><span id="lblSalesTerminal"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                        <div id="divAlert" style="display: none;">
                            <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <!-- E: scrollBox -->
        </div>
    </div>
	<!--//E: contentsarea -->
</asp:Content>