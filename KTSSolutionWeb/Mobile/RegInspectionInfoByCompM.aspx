﻿<%@ Page Language="C#" MasterPageFile="~/Site.M_Order.Master" AutoEventWireup="true" CodeBehind="RegInspectionInfoByCompM.aspx.cs" Inherits="KTSSolutionWeb.RegInspectionInfoByCompM" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>        
        .scrollbox2 {overflow-x:hidden; overflow-y:auto; width:100%; height:auto; min-height:30px; margin-top:2px; margin-bottom:2px}
        .inpbox span {display:inline-block; width:100%; vertical-align:middle; color:#000; font-size:13px;}
        .inpbox input {display:inline-block; width:100%; height:30px; padding:0 14px; border:1px solid #dfdfdf; border-radius:2px; line-height:30px; vertical-align:middle; color:#000; font-size:13px;}
        .hTitle {height:35px; padding-top:13px}
        .tabulator {position:relative;background-color:#fff;overflow:hidden;font-size:11px;text-align:left;-ms-transform:translateZ(0);transform:translateZ(0);}

        .div-chktitle {width:100%;height:25px;font-weight:bold;font-size:15px;text-align:left;}
        .div-list {overflow:auto;}
        .div-chkgroup {width: 100%;border: 1px solid black;border-radius: 10px;padding: 5px 5px;margin-bottom:5px}
        .div-chkupper {font-weight: bold;font-size: 14px;line-height: 20px;text-align: left;margin-bottom:5px}
        .div-chklist {vertical-align: middle;font-weight: normal;font-size: 14px;line-height: 14px;text-align: left;padding: 2px 10px;}
        .div-radio-box {width: 100%;height: 24px;padding: 0 10px;margin-bottom:5px;text-align: right;display: flex;justify-content: flex-end;}
        .div-radio {cursor: pointer;padding-top: 1px;padding-left: 4px;padding-right: 4px;margin-right: 4px;text-align: left;border-radius: 3px;border: 1px solid gray;float: left;background-color:white}

        .pageView {table-layout: fixed; border-collapse: collapse; border-spacing: 0; width: 100%; background: #ffffff;}
        .pageViewTh {font-size: 13px; text-align: left; border-bottom: 1px solid #dfdfdf; padding: 6px 10px; background: #f9f9f9;}
        .pageViewTd {font-size: 13px; text-align: left; border-bottom: 1px solid #dfdfdf; padding: 6px 10px;}
        /*p.inpbox {display: flex;}*/
        p.inpbox input {flex: 1; padding:0 10px;}
        .inpbox label {margin-right: 0;}
        .tableInTitle {display:block !important; justify-content:center; text-align: center; background-color: #eaf7f6; padding: 6px 0; margin-bottom: 10px;}
        #container h2#htitle {margin-bottom: 0;}
        .inpbox textarea {padding: 10px 14px;}
        .inpbox textarea:read-only {background: #f9f9f9; border: 1px solid #dfdfdf;}
        .inpbox input:read-only {background: #f9f9f9; border: 1px solid #dfdfdf;}
        .btnWrap {display: flex; justify-content: center; width: 100%; margin-top: 30px;}
        .btnWrap input {min-width: 55px; padding: 0 10px; margin-right: 5px; border: none;}
        .btnWrap input:last-child {margin-right: 0;}
        .filebox .file {display: flex; float: left; width: 30px !important;}

    </style>
    <script type="text/javascript">
        var ktsGrid1;
        var ktsGrid2;
        var ktsGrid3;
        var grid1;
        var grid2;
        var grid3;
        var callGb;
        var reloadGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
            },
            eventbind: function () {
            }
        };

        var fn_Load = function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $("input.date").datepicker({
                    dateFormat: "yy-mm-dd",
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    showOtherMonths: false,
                    selectOtherMonths: false,
                    constrainInput: false,
                    prevText: "<",
                    nextText: ">",
                });
            }

            var chkStatus = $("#<%= hdfChkStatus.ClientID %>").val();
            var actStatus = $("#<%= hdfActStatus.ClientID %>").val();

            if (chkStatus != "") {
                if (chkStatus == 'F' && actStatus == 'U') {
                    $("#hReq").hide();
                    $("#tableReq").hide();
                    $("#hAct").hide();
                    $("#tableAct").hide();
                }
                else {
                    $("#hReq").show();
                    $("#tableReq").show();
                    $("#hAct").show();
                    $("#tableAct").show();
                }

                if (actStatus == 'F') {
                    $("#stitle").text('품질검사 조치내역서 [조치완료]');
                }
                else if (actStatus == 'T') {
                    $("#stitle").text('품질검사 조치내역서 [등록중]');
                }
                else if (actStatus == 'R') {
                    $("#stitle").text('품질검사 조치내역서 [조치대기중]');
                }
                else if (chkStatus == 'F') {
                    $("#stitle").text('품질검사 조치내역서 [완료]');
                }
                else {
                    $("#stitle").text('품질검사 조치요청서');
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'FileInsOk') {
                            alert('요청사항 이미지가 등록 되었습니다.');
                            $("#filebox").empty();
                            $("#divActImg").empty();
                            callGb = '';

                            fn_GegImgData("A");
                        }
                        else if (callGb == 'FileDelOk') {
                            alert('요청사항 이미지가 삭제 되었습니다.');
                            $("#filebox").empty();
                            $("#divActImg").empty();
                            callGb = '';

                            fn_GegImgData("A");
                        }
                        else if (callGb == 'SetStatusOK_T') {
                            alert('조치내역을 임시저장 하였습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }
                        else if (callGb == 'SetStatusOK_F') {
                            alert('조치완료 처리 되었습니다.');
                            reloadGb = "Y";

                            callGb = '';
                        }

                        if (reloadGb == "Y") {
                            var inspection_No = "";

                            if (item.INSPECTION_NO != undefined && item.INSPECTION_NO != '') {
                                inspection_No = item.INSPECTION_NO;

                            }
                            else {
                                inspection_No = $("#<%= hdfInspection_No.ClientID %>").val();
                            }

                            if (inspection_No != '' && inspection_No != undefined) {
                                fn_Reload(inspection_No);
                            }

                            reloadGb = '';
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetImgData') {
                    var status = $("#<%= hdfActStatus.ClientID %>").val();
                    var bEnable = false;

                    if (status == 'T' || status == 'R') {
                        bEnable = true;
                    }

                    fn_ControlBind_Img(json, bEnable, 'A');
                }
            }
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://mbss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var fn_ArrayToBase64 = function (buffer) {

            var binary = '';

            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;

            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }

            return window.btoa(binary);
        };

        var fn_SetControls = function (bRegImg) {
            var chkList = $("#<%= hdfResult.ClientID %>").val();
            var imgReqData = $("#<%= hdfReqImg.ClientID %>").val();
            var imgActData = $("#<%= hdfActImg.ClientID %>").val();

            fn_ControlBind_ResultList(chkList);

            if (imgReqData != '') {
                fn_ControlBind_Img(imgReqData, false, 'R');
            }

            if (imgActData != '') {
                fn_ControlBind_Img(imgActData, bRegImg, 'A');
            }

            $("#lblList").show();
            $("#tableList").show();
        };

        var fn_ControlBind_ResultList = function (json) {
            //감리항목 바인드
            var tList = $("#tList");

            tList.empty();

            var i = 0;
            var preUpperCd = "";

            $.each(JSON.parse(json), function (idx, item) {
                i++;

                var tr = document.createElement("tr");
                var topLine = false;

                if (preUpperCd != item.UPPERLISTCD) {

                    var tdTitle = document.createElement("td");

                    tdTitle.className = "pageViewTd";
                    tdTitle.style.border = "none";
                    tdTitle.style.borderRight = "1px solid #dfdfdf";
                    tdTitle.style.borderBottom = "1px solid #dfdfdf";
                    tdTitle.style.whiteSpace = "break-spaces";
                    tdTitle.rowSpan = item.ROWCNT;

                    tdTitle.innerText = item.UPPERLISTNM;

                    tr.append(tdTitle);
                    tList.append(tr);

                    topLine = true;
                }

                preUpperCd = item.UPPERLISTCD;


                var td1 = document.createElement("td");

                td1.className = "pageViewTd";
                td1.style.textAlign = "left";
                td1.style.border = "none";
                td1.style.borderRight = "1px solid #dfdfdf";
                td1.style.whiteSpace = "break-spaces";

                if (topLine) {
                    td1.style.borderTop = "1px solid #dfdfdf";
                }

                td1.innerText = "- " + item.CHKLIST;

                tr.append(td1);

                var td2 = document.createElement("td");

                td2.className = "pageViewTd";
                td2.style.border = "none";
                td2.style.textAlign = "left";

                if (topLine) {
                    td2.style.borderTop = "1px solid #dfdfdf";
                }

                var resultText = "";

                if (item.RESULTCD == "S") {
                    resultText = "정상";
                }
                else if (item.RESULTCD == "F") {
                    resultText = "불량";
                }
                else {
                    resultText = "미대상";
                }

                td2.innerText = resultText;

                tr.append(td2);

                tList.append(tr);
            });

            if (i == 0) {
                var tr = document.createElement("tr");

                var td = document.createElement("td");

                td.className = "pageViewTd";
                td.style.textAlign = "left";
                td.colSpan = 2;
                td.innerText = "등록된 점검항목이 없습니다.";

                tr.append(td);

                tList.append(tr);
            }

            $("#hList").show();
            $("#tableList").show();
        };

        var fn_ControlBind_Img = function (json, bEnabled, typeCd) {
            var tdImg;
            var divImgList;

            if (typeCd == 'A') {
                tdImg = $("#tdActImg");
                divImgList = $("#divActImg");
            }
            else {
                tdImg = $("#tdReqImg");
            }

            tdImg.empty();

            $.each(JSON.parse(json), function (idx, item) {
                var img = document.createElement("img");
                var imgData = fn_ArrayToBase64(item.IMGDATA);

                img.setAttribute("id", "img" + item.IMG_NO);
                img.src = "data:" + item.IMGTYPE + "; base64," + imgData;
                img.style.maxWidth = "100%";

                tdImg.append(img);

                if (typeCd == 'A') {
                    var p = document.createElement("p");
                    p.className = "inpbox";
                    p.style.display = "block";
                    p.style.textAlign = "left";

                    var label = document.createElement("label");
                    label.setAttribute("id", "lblImg" + item.IMG_NO);
                    label.setAttribute("name", "lblActFileNm");
                    label.style.height = "20px";
                    label.style.marginBottom = "3px";
                    label.style.maxWidth = "80%";
                    label.innerText = item.IMGNAME;

                    p.append(label);

                    if (bEnabled) {
                        var button = document.createElement("button");
                        button.type = "button";
                        button.setAttribute("id", "btnImgDel" + item.IMG_NO);
                        button.setAttribute("onclick", "javascript:fn_DelFileCheck(" + item.IMG_NO + ", 'A');");
                        button.style.width = "20px";
                        button.style.height = "20px";
                        button.style.lineHeight = "0";
                        button.style.color = "black"; 
                        button.style.padding = "0";
                        button.style.margin = "0";
                        button.style.border = "none";
                        button.innerText = "X";

                        p.append(button);
                    }

                    divImgList.append(p);
                }
            });
        };


        var fn_RegFileCheck = function (typeCd) {
            if (confirm("참조 파일을 저장 하시겠습니까?")) {
                fn_RegImgData(typeCd);
            }
        };

        var fn_DelFileCheck = function (imgNo, typeCd) {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                fn_DelImgData(imgNo, typeCd);
            }
        };

        var fn_RegImgData = function (typeCd) {
            var imgFiles = $("#filebox").find("input[type='file']");
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo == "") {
                alert("잘못된 접근입니다.");
            }
            else if (imgFiles.length == 0) {
                alert("선택된 파일이 없습니다.");
            }
            else {
                var formData = new FormData();

                for (var i = 0; i < imgFiles.length; i++) {
                    var keyNm = inspectionNo + "_" + typeCd + "_" + i;

                    formData.append(keyNm, imgFiles[i].files[0]);
                }

                var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/QC/FileUploadHandler.ashx");

                callGb = "FileInsOk";

                ajaxCallFileUpload(formData, ajaxUrl);
            }
        };

        var fn_DelImgData = function (imgNo, typeCd) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var status = $("#<%= hdfActStatus.ClientID %>").val();
            var message = "";

            var bChk = false;
            var aData = [];

            if (inspectionNo == "" || imgNo.length == 0 || imgNo == undefined || typeCd == "" || typeCd == undefined) {
                message = "잘못된 접근입니다.";
            }
            else {
                bChk = true;

                callGb = 'FileDelOk';

                aData[0] = inspectionNo;
                aData[1] = typeCd;
                aData[2] = imgNo;
            }

            if (bChk) {
                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "DelImgData");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            else {
                alert(message);
            }
        };

        var fn_GegImgData = function (typeCd) {

            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();

            if (inspectionNo != "" && inspectionNo != undefined) {
                //입력 파라미터
                var aData = [];
                aData[0] = inspectionNo;
                aData[1] = typeCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetImgData');

                callGb = 'GetImgData';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_SetActStatus = function (status) {
            var inspectionNo = $("#<%= hdfInspection_No.ClientID %>").val();
            var ActStatus = $("#<%= hdfActStatus.ClientID %>").val();
            var actNoti = $("#<%= txbActNoti.ClientID %>").val();
            var message = '';

            var bChk = false;
            var aData = [];

            if (inspectionNo != "" && (ActStatus == 'R' || ActStatus == 'T')) {
                if (status == 'T') {
                    if (actNoti != '') {
                        message = "조치내역을 임시 저장하시겠습니까?";
                        callGb = 'SetStatusOK_T';
                        bChk = true;
                    }
                    else {
                        message = "조치결과을 입력해주세요.";
                    }
                }
                else if (status == 'F') {
                    if (actNoti != '') {
                        message = "조치완료 처리 하시겠습니까?";
                        callGb = 'SetStatusOK_F';
                        bChk = true;
                    }
                    else {
                        message = "조치결과을 입력해주세요.";
                    }
                }
                else {
                    message = "잘못된 접근입니다.";
                }
            }
            else {
                message = "잘못된 접근입니다.";
            }

            if (bChk) {
                if (confirm(message)) {
                    bChk = true;

                    aData[0] = inspectionNo;
                    aData[1] = status;
                    aData[2] = actNoti;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetInspectionActStatus");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
            else {
                alert(message);
            }
        };

        var AddFileUpload = function () {
            var cnt1 = $("#filebox").find("input[type='file']").length;
            var cnt2 = $("#divActImg").find("label[name='lblActFileNm']").length;

            if (cnt1 + cnt2 >= 4) {
                return;
            }

            if (!document.getElementById && !document.createElement)
                return;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-right:0;");
            newFileBox.setAttribute("name", "reqfile");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:90%;");
            newFileText.style.marginLeft = "0";

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");
            newFileSpan.style.width = "30px";

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "fileTypeChk('fu" + AddFileUpload.lastAssignedId + "', this.value);");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("accept", "image/*");

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.style.width = "30px";
            newFileLabel.innerText = "...";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fileTypeChk = function (id, val) {
            var filechk = false;

            var extIndex = val.lastIndexOf('.');

            var extList = ["tiff", "pjp", "jfif", "bmp", "gif", "svg", "png", "xbm", "dib", "jxl", "jpeg", "svgz", "jpg", "webp", "ico", "tif", "pjpeg", "avif"];

            if (extIndex > 1) {

                var fileExt = val.substring(extIndex + 1, val.length).toLowerCase();

                if (extList.indexOf(fileExt) > 0) {
                    filechk = true;
                }
            }
            if (filechk) {
                document.getElementById(id + "-value").value = val;
            }
            else {
                alert("이미지파일만 업로드 가능합니다.");

                document.getElementById(id).value = "";
                document.getElementById(id + "-value").value = "";
            }
        };

        var fn_PageMoveMain = function () {
            var stdt = $("#<%= hdfStdt.ClientID %>").val();
            var endt = $("#<%= hdfEndt.ClientID %>").val();
            var chkCode = $("#<%= hdfActCode.ClientID %>").val();
            var lv1GrpCd = $("#<%= hdfLv1.ClientID %>").val();

            var param = {
                pSTDT: stdt,
                pENDT: endt,
                pCHKCODE: chkCode,
                pLV1CD: lv1GrpCd
            };

            var form = createForm("/Mobile/InspectionListByCompM", param);

            form.submit();
        };

        var fn_Reload = function (inspection_no) {
            var stdt = $("#<%= hdfStdt.ClientID %>").val();
            var endt = $("#<%= hdfEndt.ClientID %>").val();
            var actCode = $("#<%= hdfActCode.ClientID %>").val();
            var lv1GrpCd = $("#<%= hdfLv1.ClientID %>").val();

            var param = {
                pSTDT: stdt,
                pENDT: endt,
                pACTCODE: actCode,
                pLV1CD: lv1GrpCd,
                pINSPECTION_NO: inspection_no
            };

            var form = createForm("/Mobile/RegInspectionInfoByCompM", param);

            form.submit();
        };
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<div class="page">
            <h2 style="margin-bottom:5px;padding-bottom:5px;">
                <strong id="stitle">품질검사 조치요청서</strong>
            </h2>
            <h4>kt service 북부 검사원&nbsp;<span style="color:red;text-align:center;">*</span></h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">검사일</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbChkDt" runat="server" Width="100px" ReadOnly="true" BackColor="white" placeholder="YYYY-MM-DD"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">소속</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbChkOrgNm" runat="server" BackColor="white" ReadOnly="true"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">검사원</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbChkEmpNm" runat="server" Width="180px" BackColor="white" ReadOnly="true"></asp:TextBox>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">검사대상&nbsp;<span style="color:red;text-align:center;">*</span></h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">업체명</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbCompany" runat="server" Width="180px" BackColor="white" ReadOnly="true"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">현장대리인</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbPartnersEmpNm" runat="server" Width="180px" BackColor="white" ReadOnly="true"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">주요내용</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbNoti" runat="server" BackColor="white" ReadOnly="true"></asp:TextBox>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">고객정보</h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">회선번호</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbLineNo" runat="server" Width="180px" BackColor="white"></asp:TextBox>
							</p>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">고객명</th>
                        <td class="pageViewTd">
                            <p class="inpbox">
                                <asp:TextBox ID="txbCustNm" Width="180px" runat="server" BackColor="white"></asp:TextBox>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">조치항목 분류&nbsp;<span style="color:red;text-align:center;">*</span></h4>
			<table class="pageView">
				<colgroup>
                    <col style="width:110px;" />
                    <col />
				</colgroup>
                <tbody>
					<tr>
                        <th class="pageViewTh">대분류</th>
                        <td class="pageViewTd">
                            <span class="inpbox">
                                <asp:TextBox ID="txbLv1Grp" runat="server" style="width:180px;" BackColor="white" ReadOnly="true"></asp:TextBox>
                            </span>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">중분류</th>
                        <td class="pageViewTd">
                            <span class="inpbox">
                                <asp:TextBox ID="txbLv2Grp" runat="server" style="width:180px;" BackColor="white" ReadOnly="true"></asp:TextBox>
                            </span>
						</td>
					</tr>
					<tr>
                        <th class="pageViewTh">소분류</th>
                        <td class="pageViewTd">
                            <span class="inpbox">
                                <asp:TextBox ID="txbLv3Grp" runat="server" style="width:180px;" BackColor="white" ReadOnly="true"></asp:TextBox>
                            </span>
						</td>
					</tr>
				</tbody>
			</table>
            <h4 class="hTitle">
                <label id="lblList" style="display:none;">검사목록</label>
            </h4>
			<table id="tableList" class="pageView" style="display:none;">
				<colgroup>
                    <col style="width:20%;min-width:20%;max-width:20%;" />
                    <col style="width:65%;min-width:65%;max-width:65%;" />
                    <col />
				</colgroup>
                <thead>
                    <tr>
                        <th class="pageViewTh" colspan="2" style="border-right:1px solid #dfdfdf">항목</th>
                        <th class="pageViewTh">결과</th>
                    </tr>
                </thead>
                <tbody id="tList">
				</tbody>
			</table>
            <h4 id="hReq" style="display:none;" class="hTitle">조치요청</h4>
			<table id="tableReq" class="pageView" style="display:none;">
                <thead>
                    <tr>
                        <th class="pageViewTh">요청사항</th>
                    </tr>
                </thead>
                <tbody >
					<tr>
                        <td id="tdReqImg" class="pageViewTd" style="height:200px;vertical-align:top;white-space:normal">
                        </td>
					</tr>
					<tr>
                        <td class="pageViewTd">
                            <p class="inpbox">                                                    
                                <asp:TextBox ID="txbReqNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px;min-height:100px" ReadOnly="true"></asp:TextBox>
                            </p>   
                        </td>
					</tr>
				</tbody>
			</table>
            <h4 id="hAct" style="display:none;" class="hTitle">요청사항 조치결과</h4>
			<table id="tableAct" class="pageView" style="display:none;">
                <thead>
                    <tr>
                        <th class="pageViewTh">조치결과</th>
                    </tr>
                </thead>
                <tbody >
					<tr>
                        <td id="tdActImg" class="pageViewTd" style="height:200px;vertical-align:top;white-space:normal">
                        </td>
					</tr>
					<tr id="trActImgNm" runat="server" style="height:80px;vertical-align:top;">
                        <td class="pageViewTd">
			                <div style="text-align:right;">
                                <button type="button" id="btnActAdd" runat="server" class="btn-replysave" style="width:30px;height:33px;">추가</button>
                                <button type="button" id="btnActImg" runat="server" class="btn-green">사진저장</button>
			                </div>
                            <div>
                                <div id="filebox">
                                </div>                                                    
                            </div>
                            <div id="divActImg" style="width:100%;overflow-x:hidden;overflow-y:auto; margin-top:2px; margin-bottom:2px">
                            </div>
                        </td>
					</tr>
					<tr>
                        <td class="pageViewTd">
                            <p class="inpbox">                                                    
                                <asp:TextBox ID="txbActNoti" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px;min-height:100px" ReadOnly="true"></asp:TextBox>
                            </p>
                            <button type="button" id="btnRegAct" runat="server" class="btn-green" style="margin-top:6px;float:right">저장</button>
                        </td>
					</tr>
				</tbody>
			</table>
            <h4 id="hReject" class="hTitle" runat="server" visible="false">반려사유</h4>
			<table id="tableReject" class="pageView" runat="server" visible="false">
                <tbody>
					<tr>
                        <td class="pageViewTd">
                            <p class="inpbox">                                                    
                                <asp:TextBox ID="txbReject" runat="server" TextMode="MultiLine" Width="100%" Height="100px" BorderColor="#dfdfdf" style="padding:3px;min-height:100px" ReadOnly="true"></asp:TextBox>
                            </p>
                        </td>
					</tr>
				</tbody>
			</table>
			<div style="text-align:center;margin-top:20px;">
                <input id="btnActFin" runat="server" type="button" class="btn-green" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" value="조치완료"/>
                <input id="btnBack" type="button" class="btn-black" style="min-width:55px;width:55px;padding:0 2px;border-style:none;" onclick="fn_PageMoveMain();" value="이전화면"/>
			</div>
            <asp:HiddenField ID="hdfInspection_No" runat="server"/>
            <asp:HiddenField ID="hdfChkStatus" runat="server"/>
            <asp:HiddenField ID="hdfActStatus" runat="server"/>
            <asp:HiddenField ID="hdfResult" runat="server"/>
            <asp:HiddenField ID="hdfReqImg" runat="server"/>
            <asp:HiddenField ID="hdfActImg" runat="server"/>
            <asp:HiddenField ID="hdfStdt" runat="server"/>
            <asp:HiddenField ID="hdfEndt" runat="server"/>
            <asp:HiddenField ID="hdfActCode" runat="server"/>
            <asp:HiddenField ID="hdfLv1" runat="server"/>
	    </div>
	    <!--//E: contentsarea -->
    </div>
</asp:Content>