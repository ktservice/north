﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PortalMotResultM.aspx.cs" Inherits="KTSSolutionWeb.PortalMotResultM" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="../Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="../Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="../Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="../Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="../Resource/js/xlsx.full.min.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="../Resource/css/notice_M.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
        .table-cell.td_backwhite {
            width: 50%;
            text-align: center;
        }

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };
                $("#txtMdate").monthpicker(options);

                $("#txtMdate").change(function () {
                    fn_Data('DATA');
                });

                $("#btnSummary").click(function () {
                    $("#divDetail").hide();
                    $("#divData").show();

                    $("#txtMdate").show();
                    $("#txtMdateReadonly").hide();
                });

                $("#btnDetail").click(function () {
                    $("#divDetail").show();
                    $("#divData").hide();

                    $("#txtMdate").hide();
                    $("#txtMdateReadonly").show();
                });
                
            }
        };

        var fn_Init = function () {
            //$("#lblContentTitle").text('MOT 실적 조회');
            
            $(".table-option").css('display', 'none');
            $(".mdate").val($("#<%=hdnMdate.ClientID %>").val());
            $("#divDetail").hide();

            $("#txtMdateReadonly").val($("#<%=hdnMdate.ClientID %>").val());
            $("#txtMdateReadonly").hide();
            $("#txtMdateReadonly").attr('readonly', 'true');
        }

        var fn_Load = function () {
            fn_NoticeInfo('MOTR');
        }

        var ajaxCallBack = function (json) {
            if (callGb == 'MOTR') {
                $.each(JSON.parse(json), function (idx, item) {
                    $('.noticetitletext').html(replaceBrTag(item.TITLE));
                    $('.noticecontenttext').html(replaceBrTag(item.CONTENTS));
                });

                callGb = '';

                fn_Data('DATA');
            }
            else if (callGb == 'DATA') {
                var vTemp = '';
                var html = '';
                var motPoint = 0;

                $.each(JSON.parse(json), function (idx, item) {
                    var htmlData = '';

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell border_right" style="font-weight: bold;">' + item.SERVICECNM+'</td>';
                    htmlData += '    <td class="table-cell td_backwhite last">' + priceToString(item.SERVICECNT)+'</td>';
                    htmlData += '</tr>';

                    motPoint = priceToString(item.MOTPOINT);

                    $('#lblSellerLv').text(priceToString(item.SELLER_LV));
                    $('#lblYearSum').text(priceToString(item.YEARSUM));
                    $('#lblMonthSum').text(priceToString(item.MONTHSUM));
                    
                    $('#lblRegDt').text(item.REGDT);

                    vTemp = item.REGDT;
                    html += htmlData;
                });


                if (vTemp != '') {

                    html += '<tr>';
                    html += '    <td class="table-cell td_width50 border_right" style="font-weight: bold;">MOT 판매실적급 포인트</td>';
                    html += '    <td class="table-cell last">' + motPoint + '</td>';
                    html += '</tr>';

                    $('#bodyData').html(html);

                    $('#divData').show();
                    $('#divData').css('display', 'block');
                    $('#divAlert').hide();
                    $('#divAlert').css('display', 'none');

                    fn_Data('DETAIL');
                }
                else {
                    $('#divData').hide();
                    $('#divData').css('display', 'none');
                    $('#divAlert').show();
                    $('#divAlert').css('display', 'block');
                    $('#lblRegDt').text('');

                    callGb = '';
                }
                
            }
            else if (callGb == 'DETAIL') {
                var html = '';

                $.each(JSON.parse(json), function (idx, item) {
                    var htmlData = '';

                    htmlData = '<tr>';
                    htmlData += '    <td class="table-cell td_width20 border_right" style="font-weight: bold;">' + item.OPEN_DATE + '</td>';
                    htmlData += '    <td class="table-cell td_width40 td_left border_right">' + item.CUSTOMERNM + '</td>';
                    htmlData += '    <td class="table-cell td_width40 td_left last">' + item.PRODUCTNM + '</td>';
                    htmlData += '</tr>';

                    html += htmlData;
                });

                $('#bodyDataDetail').html(html);

                callGb = '';
            }

        }

        function replaceBrTag(str) {
            if (str == undefined || str == null) {
                return "";
            }

            str = str.replace(/\r\n/ig, '<br>');
            str = str.replace(/\\n/ig, '<br>');
            str = str.replace(/\n/ig, '<br>');
            return str;
        }

        var fn_NoticeInfo = function (gb) {
            callGb = gb;

            //입력 파라미터
            var aData = [];
            aData[0] = gb;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetNoticeInfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function (gubun) {
            callGb = gubun;

            $("#txtMdateReadonly").val($(".mdate").val());

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = $(".mdate").val();

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


    </script>

	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div>
            <!-- S:UserControlTitle -->
            <uc:menu ID="menu" runat="server" />
            <!-- E:UserControlTitle -->
            <div class="noticebox">
                <div class="noticetitle">
                    <span class="noticetitletext"></span>
                </div>
                <div class="noticecontent">
                    <span class="noticecontenttext"></span>
                </div>
            </div>
            <h2></h2>
            <div class="topContent">
                <!-- S:fieldset -->
                <fieldset>
                    <legend>조회조건</legend>
                    <span class="inpbox fieldsetSpan">
                        <label class="fieldsetLabel" for="txtMdate">월</label>
                        <input type="text" id="txtMdate" class="mdate" />
                        <input type="text" id="txtMdateReadonly" class="mdate read-only" />
                        <input type="hidden" id="hdnMdate" runat="server" />
                    </span>
                    <span>
                        <label id="lblRegDt" class="fieldsetLabel2"></label></span>
                </fieldset>
                <!-- //E:fieldset -->
            </div>
            <h2></h2>
            <!-- S: scrollBox -->
            <div class="scrollbox">
                <fieldset class="view-data-list-body">
                    <div class="data-group">
                        <div id="divData" style="display: none;">
                            <fieldset class="fieldset mobile-search">
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header td_width50 table-header">연간 누적 그룹 판매자 레벨</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="table-cell td_backwhite"><span id="lblSellerLv"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header td_width50 table-header border_right">연 누적실적 합계</th>
                                                <th class="table-header">당월 실적 합계</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="table-cell td_backwhite border_right"><span id="lblYearSum"></span></td>
                                                <td class="table-cell td_backwhite"><span id="lblMonthSum"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset mobile-search">
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                        </thead>
                                        <tbody id="bodyData">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <input id="btnDetail" class="btn-lightgray" type="button" title="세부 내용 보기" value="세부 내용 보기" />
                        </div>
                        <div id="divDetail">
                            <fieldset class="fieldset mobile-search">
                                <div class="total-data">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="table-first-header td_width20 table-header border_right">개통일</th>
                                                <th class="td_width40 table-header border_right">고객명</th>
                                                <th class="td_width40 table-header">상품</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyDataDetail">
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                            <input id="btnSummary" class="btn-lightgray" type="button" title="요약 내용 보기" value="요약 내용 보기" />
                        </div>
                        <div id="divAlert" style="display: none;">
                            <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <!-- E: scrollBox -->
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>