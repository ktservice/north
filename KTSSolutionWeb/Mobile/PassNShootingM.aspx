﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PassNShootingM.aspx.cs" Inherits="KTSSolutionWeb.PassNShootingM" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Scripts/Pass/PassControls.js?20250307"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
	
    <style type="text/css">
        fieldset > .inpbox {margin: 3px 0 3px 0; font-size: 0;}
        fieldset > .optionbox {margin: 3px 0 3px 0; font-size: 0;}
        fieldset > .btnbox {margin:11px 0 11px 0; font-size:0; text-align:center;}
        fieldset > .optionbox > label {margin-left:0; margin: 3px 0 3px 0;}
        fieldset > .inpbox > label {margin:0 !important; margin-left:0;}
        fieldset > .inpbox > span {display:inline-block; width:70px; padding-top:8px; padding-bottom:8px; vertical-align:middle; color:#000; font-size:13px; font-weight:400; float:left;}
        fieldset > .inpbox > em {width: 30px; display: inline-block; font-size: 16px; font-weight: bold; text-align: center; vertical-align: middle;}
        fieldset > .inpbox > input[type="radio"] {margin-top:8px; margin-left:0px; margin-right:5px; width:18px; height:18px; border:1px solid #dfdfdf; float:left;}
        fieldset > .inpbox > table > tbody > tr > td {width:50% !important;height:24px !important;text-align:center !important;font-size:13px !important;font-weight:bold !important;}
        fieldset > .inpbox > table > tbody > tr > td > input[type="checkbox"]{width:20px}
        .div-Screenlock {position: absolute;top: 0px;left: 0px;width: 100%;height: 200%;background-color: #ffffff !important;z-index: 99;opacity: 0.7;overflow: hidden;display:none;}
        .div-modal {position: fixed;width: 90%;height: 180px;top: 40%;left: 50%;transform: translate(-50%, -50%);background: rgb(255, 255, 255);border-radius: 10px;border: 1px solid gray;overflow: hidden;z-index:100;}
    </style>

    <script type="text/javascript">
        var ktsGrid1;
        var ktsGrid2;
        var grid1;
        var grid2;
        var Columns1;
        var Columns2;
        var callGb;
        var regGb;
        var rctTypeBind;
        var consultBind;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });
        var page = {
            init: function () {
                fn_GridInit1();
                fn_Load();
            },
            eventbind: function () {
                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnRegShow").click(function (e) {
                    fn_InitControls();

                    $("#divSearch").hide();
                    $("#strTitle").hide();
                    $("#divSearchList").hide();
                    $("#h2Title").html("PASS 정보 등록");
                    $("#divRegPass").show();
                    $("#cvSign").show();

                    $("#btnReg").show();

                    $("#btnReg").on("click", function () {
                        fn_RegChk();
                    });

                    SetCanvas();
                });

                $("#btnRegHide").click(function (e) {
                    fn_InitControls();

                    $("#divSearch").show();
                    $("#strTitle").show();
                    $("#divSearchList").show();
                    $("#h2Title").html("PASS & 슈팅 정보 조회");
                    $("#divRegPass").hide();

                    SetCanvas();
                });                

                $("input[name=rbCalendar]").change(function () {
                    var datetype = $("input[name=rbCalendar]:checked").val();
                    var today = new Date();
                    var year = today.getFullYear(); // 년도
                    var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
                    var date = ("0" + today.getDate()).slice(-2);  // 날짜

                    var todayDate = "";

                    if (datetype == "M") {
                        $("#txbStDt").attr('class', 'month');
                        $("#txbEnDt").attr('class', 'month');

                        todayDate = year + '-' + month;

                        $("#txbStDt").val(todayDate);
                        $("#txbEnDt").val(todayDate);
                    }
                    else {
                        $("#txbStDt").attr('class', 'date');
                        $("#txbEnDt").attr('class', 'date');

                        todayDate = year + '-' + month + '-' + date;
                    }

                    $("#txbStDt").val(todayDate);
                    $("#txbEnDt").val(todayDate);
                });

                $("#ddlPassType2").change(function (e) {
                    fn_ViewControls();
                    fn_GetRctType();
                    SetCanvas();
                });

                $("#ddlRctType").change(function (e) {
                    fn_ViewControls();
                    SetCanvas();
                });

                $("#chkOtpSkip").change(function (e) {
                    if ($("#chkOtpSkip").is(":checked")) {
                        $("#txbOTP").val('');
                        $("#txbOTP").attr("disabled", "disabled");
                    }
                    else {
                        $("#txbOTP").removeAttr("disabled");
                    }
                });

                $("#btnOtpChk").click(function (e) {
                    var bSkip = $("#chkOtpSkip").is(":checked");
                    var procId = $("#<%= hdfProcId.ClientID %>").val();

                    if (!bSkip) {
                        var otpVal = $("#txbOTP").val();
                        var otpNo = $("#<%= hdfOTPNo.ClientID %>").val();

                        if (otpVal == otpNo) {
                            fn_SetRegType(procId);
                        }
                        else {
                            alert('입력한 인증번호가 맞지 않습니다. 인증번호를 확인해주세요.');
                        }
                    }
                    else {
                        if (confirm("인증없이 PASS 등록하시겠습니까?")) {
                            fn_SetRegType(procId);
                        }
                    }
                });

                $("#btnOtpClose").click(function (e) {
                    if (confirm("인증을 하지 않으면 담당자가 PASS상품을 확인 할 수 없습니다. 이대로 종료하시겠습니까?")) {
                        $("#btnRegHide").click();
                        $("#btnSelect").click();
                    }
                });
            }
        };

        var fn_GridInit1 = function () {
            var fn_buttons = function (cell) {
                var rowData = cell.getRow().getData();

                var procId = rowData.PROCID;
                var passStat = rowData.PASSSTAT;
                var succYn = rowData.SUCCYN;
                var uploadYn = rowData.UPLOADYN;
                var regType = rowData.REGTYPE;
                var otpProcYn = $("#<%= hdfOTPProcYn.ClientID %>").val();
                
                var btnVal = "";

                if (passStat == "P" || (passStat == "S" && (succYn == "H" || succYn == "R"))) {
                    btnVal = "수정";
                } else {
                    btnVal = "상세";
                }
                
                var input = "<input id='btnCellSave' type='button' onclick='fn_RegPassView(\"" + procId + "\");' value='" + btnVal + "' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";

                if (uploadYn == "Y" && regType == 'T' && otpProcYn == 'Y') {
                    input += "<input id='btnCellOTP' type='button' onclick='fn_SetOtp(\"" + procId + "\");' value='인증' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-left:10px'>";
                }

                return input;
            };

            Columns1 = [
                { formatter: "responsiveCollapse", width: 25, minWidth: 25, hozAlign: "center", resizable: false, headerSort: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", resizable: false, width: 60, responsive: 0, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일자", field: "RCTDATE", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "PASSSTATNM", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "성공여부", field: "SUCCYNNM", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "수정", field: "SAVE", resizable: false, width: 120, minWidth: 120, formatter: fn_buttons, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수유형", field: "RCTTYPENM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "접속번호", field: "CONNO", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처", field: "CUSTTELNO", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "관계", field: "RELATIONS", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "PASSTYPENM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품목록", field: "SVCTYPE", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGFULLNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원명", field: "EMPNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "전화번호", field: "MOBILE", sorter: "string", width: 100, responsive: 2},
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "요청사항", field: "NOTE1",  sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자소속", field: "TRTORGNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자사번", field: "TRTEMPNO", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "TRTEMPNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅상품", field: "SHTSVC", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅일", field: "SHOOTINGDT", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "실패사유", field: "FAILNOTE", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTE2", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "PROCID", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSTYPE", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSSTAT", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "RCTTYPE", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "SUCCYN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "CUSTSIGNYN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "IMGDATA", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "UPLOADYN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "MONTHFIN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "REGTYPE", visible: false, responsive: 2 }
            ];

            Columns2 = [
                { formatter: "responsiveCollapse", width: 25, minWidth: 25, hozAlign: "center", resizable: false, headerSort: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", resizable: false, width: 60, responsive: 0, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일자", field: "RCTDATE", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "PASSSTATNM", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "성공여부", field: "SUCCYNNM", sorter: "string", resizable: false, width: 90, minWidth: 90, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "수정", field: "SAVE", resizable: false, width: 120, minWidth: 120, formatter: fn_buttons, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "희망상품", field: "HOPESVC", sorter: "string", width: 90, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처", field: "CUSTTELNO", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "관계", field: "RELATIONS", sorter: "string", width: 160, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "PASSTYPENM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGFULLNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원명", field: "EMPNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "전화번호", field: "MOBILE", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "요청사항", field: "NOTE1", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자소속", field: "TRTORGNM", sorter: "string", width: 100, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자사번", field: "TRTEMPNO", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "TRTEMPNM", sorter: "string", width: 240, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅일", field: "SHOOTINGDT", sorter: "string", width: 240, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "실패사유", field: "FAILNOTE", sorter: "string", width: 240, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTE2", sorter: "string", width: 240, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "PROCID", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSTYPE", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSSTAT", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "RCTTYPE", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "SUCCYN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "CUSTSIGNYN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "IMGDATA", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "UPLOADYN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "MONTHFIN", visible: false, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", field: "REGTYPE", visible: false, responsive: 2 }
            ];

            //컬럼 세팅
            ktsColumns = Columns1;

            //그리드 초기 세팅
            grid1 = ktsTabulator;
            grid1.gridCollapseM('ktsGrid1', 'fitDataFill', false, null); // div의 grid id 값
            ktsGrid1 = ktsGrid;
        };

        var fn_Load = function () {
            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
            var date = ("0" + today.getDate()).slice(-2);  // 날짜

            var todayDate = year + '-' + month + '-' + date;

            $("#txbStDt").val(todayDate);
            $("#txbEnDt").val(todayDate);

            fn_GetPassType();
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://bss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                if (ktsGridId == 'ktsGrid1') {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid1.setData(json);
                    ktsGridRowCount(ktsGrid1.getDataCount());

                    var calendaType = $("input[name=rbCalendar]:checked").val();

                    if (calendaType == "M") {
                        fn_SearchSts();
                    }
                }
                else {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid2.setData(json);
                    ktsGridRowCount(ktsGrid2.getDataCount());
                }
            }
        };

        var ajaxCallEx = function (param, url, gridAlert) {
            $.ajax({
                type: 'post',
                url: url,
                data: param,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    var json = data.d;

                    ajaxCallSuccess(json, gridAlert);
                },
                beforeSend: function () {
                    $("#updateProgress").css('display', 'block');
                },
                complete: function () {
                    $("#updateProgress").css('display', 'none');
                },
                error: function (request, status, error) {
                    ajaxFail(request, status, error, gridAlert);
                }
            })
        };

        var successCallBack = function (json) {
            if (callGb == 'GetPassType') {
                callGb = '';

                fn_ControlBind(json, 'PASS2');
                fn_GetOTPProcYn();
            }
            if (callGb == 'GetOTPProcYn') {
                callGb = '';

                $("#<%= hdfOTPProcYn.ClientID %>").val(json);

            }
            else if (callGb == 'GetRctType') {
                callGb = '';
                fn_ControlBind(json, 'RCT');

                if (rctTypeBind != "" && rctTypeBind.match("_Y")) {
                    //파일목록 조회추가
                    fn_GetPassFile();
                }

                rctTypeBind = "";
            }
            else if (callGb == 'GetConsult') {
                callGb = '';

                fn_ControlBind(json, 'CON');

                consultBind = "";
            }
            else if (callGb == 'GetPassFile') {
                callGb = '';

                fn_ControlBind_Files(json);
            }
            else if (callGb == 'SetPassInfo') {
                callGb = '';

                var procId = "";
                $.each(JSON.parse(json), function (idx, item) {
                    procId = item.PROCID;
                    return false;
                });

                $("#<%= hdfProcId.ClientID %>").val(procId);

                if ($("#spanUpload").is(":visible")) {
                    fn_RegFileData();
                }
                else {
                    if (regGb == "R") {
                        regGb = '';
                        alert('PASS 저장이 완료되었습니다.');
                        $("#btnRegHide").click();
                        $("#btnSelect").click();
                    }
                    else {
                        regGb = '';

                        fn_SetOtp(procId);
                    }
                }
            }
            else if (callGb == 'DelPassInfo') {
                callGb = '';

                alert('PASS 정보가 삭제되었습니다.');
                $("#btnRegHide").click();
                $("#btnSelect").click();

            }
            else if (callGb == 'FileDelOk') {
                callGb = '';

                alert('첨부파일이 삭제되었습니다.');
                fn_GetPassFile();
            }
            else if (callGb == 'SetRegType') {
                callGb = '';

                regGb = '';
                alert('OTP인증에 성공하여 PASS 저장이 완료되었습니다.');
                $("#btnRegHide").click();
                $("#btnSelect").click();
            }
            else if (callGb == 'SetOtp') {
                callGb = '';
                $("body").css("overflow", "hidden");
                $("#divScreenLock").show();
                $("#divOtpChk").show();

                var procId = "";
                var otpNo = "";
                $.each(JSON.parse(json), function (idx, item) {
                    procId = item.PROCID;
                    otpNo = item.OTP;
                    return false;
                });

                $("#<%= hdfProcId.ClientID %>").val(procId);
                $("#<%= hdfOTPNo.ClientID %>").val(otpNo);
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb == 'FileInsOk') {
                callGb = '';

                if (regGb == "R") {
                    regGb = '';
                    alert('PASS 저장이 완료되었습니다.');

                    $("#btnRegHide").click();
                    $("#btnSelect").click();
                }
                else {
                    regGb = '';
                    var procId = $("#<%= hdfProcId.ClientID %>").val();

                    fn_SetOtp(procId);
                }
            }
            else if (callGb == 'FileDownOk') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    var link = document.createElement('a');
                    link.href = 'data:application/octet-stream;base64,' + item.FILEDATA;
                    link.download = item.FILENM;
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                });
            }
        };

        var fn_GetPassType = function () {
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPassType');

            callGb = 'GetPassType';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetOTPProcYn = function () {
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetOTPProcYn');

            callGb = 'GetOTPProcYn';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetRctType = function () {
            var PassType = $("#ddlPassType2 option:selected").val();

            var aData = [];

            aData[0] = PassType;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetRctType');

            callGb = 'GetRctType';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetConsult = function () {
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetConsult');

            callGb = 'GetConsult';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_Search = function () {
            var custNm = $("#txbCustNm").val();
            var custTelNo = $("#txbCustTelNo").val();
            var passType = $("#ddlPassType option:selected").val();
            var stDt = $("#txbStDt").val();
            var enDt = $("#txbEnDt").val();

            if (passType == "MOT" || passType == "") {
                ktsColumns = Columns1;
            }
            else {
                ktsColumns = Columns2;
            }

            //입력 파라미터
            var aData = [];

            aData[0] = custNm;
            aData[1] = custTelNo;
            aData[2] = passType;
            aData[3] = stDt;
            aData[4] = enDt;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid1.getUrl(location.href, 'GetData');

            //grid Bind
            grid1.getDataBind(jsonData, ajaxUrl, 'ktsGrid1', false);
        };

        var fn_SearchSts = function () {
        };

        var fn_GetPassFile = function () {
            $("#divFileList").empty();

            var procId = $("#<%= hdfProcId.ClientID %>").val();

            if (procId == "") {
                alert("잘못된 접근입니다.");
                window.close();
            }
            else {

                var aData = [];

                aData[0] = procId;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPassFile');

                callGb = 'GetPassFile';

                ajaxCall(jsonData, ajaxUrl, false);
            };
        };

        var fn_RegChk = function () {
            var procId = $("#<%= hdfProcId.ClientID %>").val();
            var passType = $("#ddlPassType2 option:selected").val();
            var rctType = $("#ddlRctType option:selected").val();
            var trtEmpNo = $("#ddlConsult option:selected").val();
            var bAgree = $("#chkAgree").is(":checked");
            var signYn = $("#<%= hdfSignYn.ClientID %>").val();
            var otpProcYn = $("#<%= hdfOTPProcYn.ClientID %>").val();
            var uploadYn = "N";

            if (rctType.match("_Y")) {
                uploadYn = "Y";
            }

            var regType = "";
            var bReg = false;

            if (procId == "") {
                if (passType == "") {
                    alert("구분을 선택해주세요.");
                }
                else if (rctType == "") {
                    if (passType == "MOT") {
                        alert("접수유형을 선택해주세요.");
                    }
                    else {
                        alert("희망상품을 선택해주세요.");
                    }
                }
                else {
                    if (!bAgree) {
                        alert("성명 및 전화번호 제공에 동의 하셔야 등록 가능합니다.");
                    }
                    else {
                        if (signYn != "Y") {
                            alert("성명 및 전화번호 제공에 동의 하셔야 등록 가능합니다.");
                        }
                        else {
                            saveImage();
                            confMsg = "Pass를 등록 하시겠습니까?";
                            regType = "I";
                            bReg = true;
                        }
                    }
                }
            }
            else {
                if (procId == "" || passType == "" || rctType == "") {
                    alert("잘못된 접근입니다.");
                }
                else {
                    confMsg = "Pass를 수정하시겠습니까?";
                    regType = "U";
                    bReg = true;
                }
            }

            if (bReg) {
                if (uploadYn == "Y" && otpProcYn == "Y" && regType == "I") {
                    fn_RegPassInfo(regType, "T");
                }
                else {
                    if (confirm(confMsg)) {
                        fn_RegPassInfo(regType, "R");
                    }
                    else {
                        return false;
                    }
                }
            }
        };

        var fn_RegPassInfo = function (regType, otpChk) {
            var procId = $("#<%= hdfProcId.ClientID %>").val();
            var Passtype = $("#ddlPassType2 option:selected").val();
            var RctType = $("#ddlRctType option:selected").val().split('_')[0];
            var CustNm = $("#txbCustNm2").val();
            var TelNo = $("#txbCustTelNo2").val();
            var RctDt = $("#txbRctDt").val();
            var Relations = $("#txbRelations").val();
            var ConNo = $("#txbConNo").val();
            var Consult = "";

            if (Passtype == "BIZ") {
                Consult = $("#ddlConsult option:selected").val();
            }

            var Svc = "";

            if (Passtype == "MOT") {
                var chkSvc = $("input[name=chbSvc]:checked");

                $(chkSvc).each(function () {
                    if (Svc != "") {
                        Svc += "^";
                    }

                    Svc += $(this).val();
                });
            }
            
            var Note1 = $("#txaNote1").val();
            var CustSign = $("#<%= hdfImgSrc.ClientID %>").val();

            var aData = [];

            aData[0] = Passtype;
            aData[1] = RctType;
            aData[2] = CustNm;
            aData[3] = TelNo;
            aData[4] = RctDt;
            aData[5] = Relations;
            aData[6] = ConNo;
            aData[7] = Consult;
            aData[8] = Svc;
            aData[9] = Note1;
            aData[10] = CustSign;
            aData[11] = otpChk;
            aData[12] = procId;
            aData[13] = regType;

            callGb = "SetPassInfo";
            regGb = otpChk;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, "SetPassNShootingInfo");

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_DelPassInfo = function () {
            var procId = $("#<%= hdfProcId.ClientID %>").val();
            var passType = $("#ddlPassType2 option:selected").val();

            if (procId == "" || passType == "") {
                alert("잘못된 접근입니다.");
            }
            else {
                if (confirm("PASS 정보를 삭제하시겠습니까?")) {
                    var aData = [];

                    aData[0] = procId;
                    aData[1] = passType;

                    callGb = "DelPassInfo";

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "DelPassInfo");

                    ajaxCall(jsonData, ajaxUrl, false);
                }
            }

        };

        var fn_RegFileData = function () {
            var procId = $("#<%= hdfProcId.ClientID %>").val();

            if (procId == "") {
                alert("잘못된 접근입니다.");
            }
            else {
                var Files = $("#divFileUpload").find("input[type='file']");
                var empNo = $("#<%= hdfEmpNo.ClientID %>").val();

                var formData = new FormData();

                var Keys = procId + "_" + empNo;

                for (var i = 0; i < Files.length; i++) {
                    formData.append(Keys, Files[i].files[0]);
                }

                var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/Pass/FileUploadHandler.ashx");

                callGb = "FileInsOk";

                ajaxCallFileUpload(formData, ajaxUrl);
            }
        };

        var fn_DelFileData = function (seq) {
            var procId = $("#<%= hdfProcId.ClientID %>").val();

            if (confirm("첨부파일을 삭제하시겠습니까?")) {
                var aData = [];

                aData[0] = procId;
                aData[1] = seq;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'DelPassFile');

                callGb = 'FileDelOk';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_FileDownLoad = function (seq) {
            var procId = $("#<%= hdfProcId.ClientID %>").val();

            //입력 파라미터
            var aData = [];

            aData[0] = procId;
            aData[1] = seq;

            callGb = 'FileDownOk';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'FileDownload');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };

        var fn_SetRegType = function (procId) {
            var aData = [];

            aData[0] = procId;

            callGb = "SetRegType";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, "SetRegType");

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_SetOtp = function (procId) {
            var aData = [];

            aData[0] = procId;

            callGb = "SetOtp";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, "SetOtp");

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_InitControls = function () {
            $("body").css("overflow", "auto");
            $("#<%= hdfProcId.ClientID %>").val("");
            $("#<%= hdfOTPNo.ClientID %>").val("");
            
            consultBind = "";
            rctTypeBind = "";

            $("#ddlPassType2").val('');
            $("#ddlRctType").val('');
            $("#txbCustNm2").val('');
            $("#txbCustTelNo2").val('');

            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
            var date = ("0" + today.getDate()).slice(-2);  // 날짜

            var todayDate = year + '-' + month + '-' + date;

            $("#txbRctDt").val(todayDate);
            $("#txbRelations").val('');
            $("#txbConNo").val('');
            $("#ddlConsult").val('');
            $("#txaNote1").val('');

            $("#ddlPassType2").removeAttr("disabled");
            $("#ddlRctType").removeAttr("disabled");
            $("#txbCustNm2").removeAttr("disabled");
            $("#txbCustTelNo2").removeAttr("disabled");
            $("#txbRctDt").removeAttr("disabled");
            $("#txbRelations").removeAttr("disabled");
            $("#txaNote1").removeAttr("disabled");
            $("#ddlConsult").removeAttr("disabled");
            $("#txbConNo").removeAttr("disabled");

            $("#spanConNo").hide();
            $("#spanConsult").hide();
            $("#spanSvcList").hide();

            var chkSvc = $("input[name=chbSvc]");

            $(chkSvc).each(function () {
                $(this).removeAttr("disabled");
                $(this).prop("checked", false);
            });

            $("#spanFileList").hide();
            $("#spanUpload").hide();
            $("#divFileList").empty();

            $("#divFileUpload").empty();

            $("#<%= hdfSignYn.ClientID %>").val("N");
            $("#<%= hdfImgSrc.ClientID %>").val('');

            $("#chkAgree").removeAttr("disabled");
            $("#chkAgree").prop("checked", false);
            $("#cvSign").hide();
            $("#imgSign").hide();
            $("#imgSign").attr("src", "");            

            $("#txbOTP").val('');
            $("#txbOTP").removeAttr("disabled");
            $("#chkOtpSkip").prop("checked", false);
            $("#divScreenLock").hide();
            $("#divOtpChk").hide();

            $("#btnReg").hide();
            $("#btnDel").hide();

            $("#btnReg").off("click");
            $("#btnDel").off("click");
        };

        var fn_RegPassView = function (procId) {
            fn_InitControls();

            var rows = ktsGrid1.searchRows("PROCID", "=", procId);
            var rowData = rows[0].getData();

            var empNo = rowData.EMPNO;
            var passStat = rowData.PASSSTAT;
            var passType = rowData.PASSTYPE;
            var custNm = rowData.CUSTNM;
            var custTelNo = rowData.CUSTTELNO;
            var rctDate = rowData.RCTDATE;
            var relations = rowData.RELATIONS;
            var conNo = rowData.CONNO;
            var svcType = rowData.SVCTYPE;
            var trtEmpno = rowData.TRTEMPNO;
            var note1 = rowData.NOTE1;
            var monthFinYn = rowData.MONTHFIN;
            var uploadYn = rowData.UPLOADYN;
            var rctType = rowData.RCTTYPE + "_" + uploadYn;
            var imgData = rowData.IMGDATA;
            var succYn = rowData.SUCCYN;
            var regType = rowData.REGTYPE;

            $("#divSearch").hide();
            $("#strTitle").hide();
            $("#divSearchList").hide();

            $("#<%= hdfProcId.ClientID %>").val(procId);

            $("#divRegPass").show();

            $("#ddlPassType2").val(passType);
            $("#txbCustNm2").val(custNm);
            $("#txbCustTelNo2").val(custTelNo);
            $("#txbRctDt").val(rctDate);
            $("#txbRelations").val(relations);
            $("#txaNote1").val(note1);

            $("#ddlPassType2").attr("disabled", "disabled");
            $("#ddlRctType").attr("disabled", "disabled");

            if (passStat != "P") {
                $("#txbCustNm2").attr("disabled", "disabled");
                $("#txbCustTelNo2").attr("disabled", "disabled");
                $("#txbRctDt").attr("disabled", "disabled");
                $("#txbRelations").attr("disabled", "disabled");;

                if (passStat == "S" && (succYn == "H" || succYn == "R")) {
                    $("#txaNote1").removeAttr("disabled");
                    $("#btnReg").show();
                    $("#btnDel").show();

                    $("#btnReg").on("click", function () {
                        fn_RegChk();
                    });

                    $("#btnDel").on("click", function () {
                        fn_DelPassInfo();
                    });
                }
                else {
                    $("#txaNote1").attr("disabled", "disabled");
                    $("#btnReg").hide();
                    $("#btnDel").hide();
                }

                $("#ddlConsult").attr("disabled", "disabled");
                $("#txbConNo").attr("disabled", "disabled");
                $("#chkAgree").attr("disabled", "disabled");
            }
            else {
                $("#btnReg").show();
                $("#btnDel").show();

                $("#btnReg").on("click", function () {
                    fn_RegChk();
                });

                $("#btnDel").on("click", function () {
                    fn_DelPassInfo();
                });
            }

            if (passType == "MOT") {
                $("#lblRctType").text("접수유형");
                $("#spanConNo").show();
                $("#txbConNo").val(conNo);

                $("#spanSvcList").show();

                var svcList = svcType.split(',');
                var chkSvc = $("input[name=chbSvc]");

                $(chkSvc).each(function () {
                    var svc = $(this).val();
                    var bChk = false;

                    $.each(svcList, function (index, value) {
                        if (svc == value) {
                            bChk = true;
                            return false;
                        }
                    });

                    if (bChk) {
                        $(this).prop('checked', true);
                    }

                    if (passStat != "P") {
                        $(this).attr("disabled", "disabled");
                    }
                });
                
                if (uploadYn == "Y") {
                    if (passStat == "P" || (passStat == "S" && (succYn == "H" || succYn == "R"))) {
                        $("#spanFileList").show();
                        $("#spanUpload").show();
                        AddFileUpload();
                    }
                    else {
                        $("#spanFileList").show();
                    }
                }
            }
            else {
                $("#lblRctType").text("희망상품");

                if (passType == "BIZ") {
                    $("#spanConsult").show();

                    consultBind = trtEmpno;
                }
            }

            if (imgData.length > 0) {
                $("#<%= hdfSignYn.ClientID %>").val("Y");
                $("#<%= hdfImgSrc.ClientID %>").val(imgData);
                var img = $("#imgSign");

                img.attr("src", imgData);
                img.show();
            }
            $("#cvSign").hide();
            $("#chkAgree").attr("disabled", "disabled");
            $("#chkAgree").prop("checked", true);

            rctTypeBind = rctType;
            fn_GetRctType();
        };

        var fn_ViewControls = function () {
            var ProcId = $("#<%= hdfProcId.ClientID %>").val();
            var PassType = $("#ddlPassType2 option:selected").val();
            var RctType = $("#ddlRctType option:selected").val().split('_');
            $("#divFileList").empty();
            $("#divFileUpload").empty();
            
            if (PassType == "MOT") {
                $("#lblRctType").text("접수유형");
                $("#spanConNo").show();

                $("#spanConsult").hide();
                $("#spanSvcList").show();

                var UploadYn = "";

                if (RctType.length == 2) {
                    UploadYn = RctType[1];
                }

                $("#spanFileList").hide();

                if (UploadYn == "Y") {
                    $("#spanUpload").show();
                    AddFileUpload();
                }
                else {
                    $("#spanUpload").hide();
                }
            }
            else {
                $("#lblRctType").text("희망상품");
                $("#spanConNo").hide();

                if (PassType == "BIZ") {
                    $("#spanConsult").show();
                }
                else {
                    $("#spanConsult").hide();
                }

                $("#spanSvcList").hide();
                
                $("#spanFileList").hide();
                $("#spanUpload").hide();
            }
        };

        var AddFileUpload = function () {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 1;

            var divfu = document.getElementById("divFileUpload");

            var divWidth = $("#divFileUpload").width();
            var textWidth = divWidth - 55;

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-right:0;");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:" + textWidth + "px;");

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");
            newFileSpan.setAttribute("style", "width:30px;margin-left:5px;");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "document.getElementById('fu" + AddFileUpload.lastAssignedId + "-value').value=this.value;");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.setAttribute("style", "width:30px;float:left;");
            newFileLabel.innerText = "...";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fn_ControlBind_Files = function (json) {
            var divFileList = $("#divFileList");

            divFileList.empty();

            var divWidth = divFileList.width();
            var textWidth = divWidth - 50;

            $.each(JSON.parse(json), function (idx, item) {
                var p = document.createElement("p");
                p.className = "inpbox";
                p.style.display = "block";
                p.style.textAlign = "left";

                var link = document.createElement('a');
                link.setAttribute("onclick", "javascript:fn_FileDownLoad(" + item.SEQ + ");");

                var label = document.createElement("label");
                label.setAttribute("id", "lblFile" + item.SEQ);
                label.setAttribute("name", "lblFileNm");
                label.setAttribute("style", "width: " + textWidth + "px;height: 35px;padding-top: 10px;text-overflow: ellipsis;white-space: nowrap;");
                label.innerText = item.id + ". " + item.FILENM;

                link.append(label);

                p.append(link);

                var button = document.createElement("button");
                button.type = "button";
                button.setAttribute("id", "btnDelFile" + item.SEQ);
                button.setAttribute("onclick", "javascript:fn_DelFileData(" + item.SEQ + ");");
                button.style.width = "30px";
                button.style.height = "30px";
                button.style.lineHeight = "0";
                button.style.color = "black";
                button.style.padding = "0";
                button.style.margin = "0";
                button.style.border = "none";
                button.innerText = "X";

                p.append(button);

                divFileList.append(p);
            });
        };

        var SetCanvas = function () {
            var drawCanvas = document.getElementById("cvSign");
            var rect = drawCanvas.getBoundingClientRect();
            var drawBackup = new Array();

            if (typeof drawCanvas.getContext == 'function') {
                var ctx = drawCanvas.getContext("2d");

                ctx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
                ctx.beginPath();
                localStorage.removeItem("ImgCanvas");

                $("#<%= hdfSignYn.ClientID %>").val("N");

                var isDraw = false;
                var pDraw = $("#cvSign").offset();
                var currP = null;
                ctx.clearRect(0, 0, drawCanvas.offsetwidth, drawCanvas.offsetHeight);

                $("#cvSign").unbind('mousedown');
                $("#cvSign").bind('mousedown', function (e) {
                    if (e.button == 0) {
                        saveCanvas();
                        e.preventDefault();
                        ctx.beginPath();

                        isDraw = true;
                    }
                });

                $("#cvSign").unbind('mousemove');
                $("#cvSign").bind('mousemove', function (e) {
                    var event = e.originalEvent;
                    e.preventDefault();

                    currP = { X: (event.clientX - pDraw.left) / 2, Y: (event.pageY - pDraw.top) / 2 };

                    if (isDraw) {
                        $("#<%= hdfSignYn.ClientID %>").val("Y");
                        draw_line(currP);
                    }
                });

                $("#cvSign").unbind('mouseup');
                $("#cvSign").bind('mouseup', function (e) {
                    e.preventDefault();

                    isDraw = false;
                });

                $("#cvSign").unbind('mouseleave');
                $("#cvSign").bind('mouseleave', function (e) {
                    isDraw = false;
                });

                $("#cvSign").unbind('touchstart');
                $("#cvSign").bind('touchstart', function (e) {
                    saveCanvas();
                    e.preventDefault();
                    ctx.beginPath();

                });

                $("#cvSign").unbind('touchmove');
                $("#cvSign").bind('touchmove', function (e) {
                    var event = e.originalEvent;
                    e.preventDefault();

                    currP = { X: event.touches[0].pageX - pDraw.left, Y: event.touches[0].pageY - pDraw.top };

                    $("#<%= hdfSignYn.ClientID %>").val("Y");
                    draw_line(currP);
                });

                $("#cvSign").unbind('touchend');
				$("#cvSign").bind('touchend', function (e) {
					e.preventDefault();
				});

                var draw_line = function (p) {
                    ctx.lineWidth = "3px";
                    ctx.lineCap = "round";
                    ctx.lineTo(p.X, p.Y);
                    ctx.moveTo(p.X, p.Y);
                    ctx.strokeStyle = "#000000";
                    ctx.stroke();
                };

                var saveCanvas = function () {
                    drawBackup.push(ctx.getImageData(0, 0, drawCanvas.width, drawCanvas.height));
                };
			}
        };

        var saveImage = function () {
            var canvas = document.getElementById("cvSign");
            localStorage.setItem("ImgCanvas", canvas.toDataURL("image/png"));

            var img = $("#imgSign");
            var imgurl = canvas.toDataURL("image/png");

            img.attr("src", imgurl);

            $("#<%= hdfImgSrc.ClientID %>").val(imgurl);
        };
    </script>
	<!-- S: contentsarea -->
	<div class="contentsarea">		
        <div class="page-clothes-request">
		    <h2 id="h2Title">PASS & 슈팅 정보 조회</h2>
			<div id="divSearch" style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>조회 조건</legend>
					<span class="inpbox" style="width:50%;">
						<label>성명</label><br />
                        <input type="text" id="txbCustNm" maxlength="20" style="width:92% !important" />
					</span>
					<span class="inpbox" style="width:50%;">
						<label>TEL</label><br />
                        <input type="text" id="txbCustTelNo" maxlength="50" style="width:92% !important" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"/>
					</span>
					<span class="optionbox" style="width:100%;">
						<label>구분</label><br />
                        <select id="ddlPassType" style="width:46% !important;">
                            <option value="">선택하세요</option>
                        </select>
					</span>
					<span class="inpbox" style="width:100%;">
						<label>조회일자</label><br />
						<input id="rbMonth" type="radio" name="rbCalendar" disabled="disabled" value="M"/>
						<span>집계년월</span>
						<input id="rbDate" type="radio" name="rbCalendar" checked="checked" value="D" />
						<span>당월일자</span>
					</span>
					<span class="inpbox" style="width:100%;">
						<input type="text" id="txbStDt" class="date" readonly="true" style="width:44% !important;" />
                        <em>~</em>
						<input type="text" id="txbEnDt" class="date" readonly="true" style="width:44% !important;" />
					</span>
					<span style="width:100%; margin-bottom:5px; text-align:right;">
                        <input type="button" id="btnSelect" class="btn-green" style="border:none;width:33px;margin-top:0 !important;margin-right:8px !important;" value="조회" />
                        <input type="button" id="btnRegShow" class="btn-green" style="border:none;width:33px;margin-top:0 !important;margin-right:8px !important;" value="등록" />
					</span>
				</fieldset>
			</div>
			<strong id="strTitle" style="margin-top:2px;margin-right:10px;float:left">Pass 상세 정보</strong>
            <div id="divSearchList" class="scrollbox">
				<div class="requestlist">
                    <!-- S:datalist -->
                    <div class="datalist" style="margin-top:10px;">
			            <!-- S:scrollbox -->
			            <div class="scrollbox">
                            <div id="ktsGrid1"></div>
                        </div>
			            <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
				</div>
			</div>
			<div id="divRegPass" style="border-bottom:2px solid #dfdfdf;display:none;">
				<fieldset>
                    <span class="optionbox" style="width:100%;">
					    <label>구분</label><br />
                        <select id="ddlPassType2" style="width:50% !important;">
                            <option value="">선택하세요</option>
                        </select>
				    </span>
					<span class="optionbox" style="width:100%;">
						<label id="lblRctType">접수유형</label><br />
                        <select id="ddlRctType" style="width:92% !important;">
                            <option value="">선택하세요</option>
                        </select>
					</span>
					<span class="inpbox" style="width:50%;">
						<label>고객명</label><br />
                        <input type="text" id="txbCustNm2" style="width:92% !important" maxlength="50"/>
					</span>
					<span class="inpbox" style="width:50%;">
						<label>연락처</label><br />
                        <input type="text" id="txbCustTelNo2" style="width:92% !important;" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11"/>
					</span>
					<span class="inpbox" style="width:50%;">
						<label style="width:100%">접수일자</label><br />
                        <input type="text" id="txbRctDt" class="date" readonly="readonly"style="width:92% !important"/>
					</span>
					<span class="inpbox" style="width:50%;">
						<label>관계</label><br />
                        <input type="text" id="txbRelations" style="width:92% !important" maxlength="50"/>
					</span>
					<span id="spanConNo" class="inpbox" style="width:100%;display:none;">
						<label>접속번호</label><br />
                        <input type="text" id="txbConNo" style="width:92% !important" maxlength="50"/>
					</span>
					<span id="spanConsult" class="optionbox" style="width:100%;display:none;">
						<label>컨설팅</label><br />
                        <select id="ddlConsult" style="width:92% !important;">
                            <option value="">선택하세요</option>
                        </select>
					</span>
					<span id="spanSvcList" class="inpbox" style="width:100%;display:none;">
						<label>상품선택</label><br />
                        <table style="width:92% !important">
                            <tr style="border-bottom:1px solid #cccccc">
                                <td>인터넷</td>
                                <td><input id="chbInternet" name="chbSvc" type="checkbox" value="인터넷" /></td>
                            </tr>
                            <tr style="border-bottom:1px solid #cccccc">
                                <td>TV</td>
                                <td><input id="chbTv" name="chbSvc" type="checkbox" value="TV" /></td>
                            </tr>
                            <tr style="border-bottom:1px solid #cccccc">
                                <td>모바일</td>
                                <td><input id="chbMobile" name="chbSvc" type="checkbox" value="모바일" /></td>
                            </tr>
                            <tr style="border-bottom:1px solid #cccccc">
                                <td>전화</td>
                                <td><input id="chbTel" name="chbSvc" type="checkbox" value="전화" /></td>
                            </tr>
                            <tr style="border-bottom:1px solid #cccccc">
                                <td>AP</td>
                                <td><input id="chbAp" name="chbSvc" type="checkbox" value="AP" /></td>
                            </tr>
                            <tr style="border-bottom:1px solid #cccccc">
                                <td>업셀링</td>
                                <td><input id="chbUpcelling" name="chbSvc" type="checkbox" value="업셀링" /></td>
                            </tr>
                            <tr>
                                <td>기타</td>
                                <td><input id="chbEtc" name="chbSvc" type="checkbox" value="기타" /></td>
                            </tr>
                        </table>
					</span>
					<span class="inpbox" style="width:100%;">
						<label>요청사항</label><br />
                        <textarea id="txaNote1" style="width:96.5%;height:240px;padding:5px;border:1px solid #dfdfdf;"></textarea>
					</span>
					<span id="spanFileList" class="inpbox" style="width:100%;display:none;margin-bottom:10px;">
						<label>파일목록</label><br />
                        <div id="divFileList" style="height:100px;overflow-y:auto;">
                        </div>
					</span>
					<span id="spanUpload" class="inpbox" style="width:100%;display:none;">
						<label>파일첨부</label><button type="button" onclick="AddFileUpload();" class="btn-save" style="width:33px !important; height:25px; font-size:13px; float:right; margin-right:5%; margin-bottom:5px;">추가</button><br />
                        <div id="divFileUpload" style="width:96%;height:140px;overflow-y:auto;">
                            <p class="filebox" style="margin-right:0;">
                                <input type="text" id="fu1-value" style="width:90%" />
                                <span class="file" style="width:30px;margin-left:5px;">
                                    <input type="file" id="fu1" name="fu1" onchange="document.getElementById('fu1-value').value=this.value;" /> 
                                    <label for="fu1" style="width:30px;float:left;">...</label>
                                </span>
                            </p>
                        </div>
					</span>
					<span id="spanAgree" class="ickbox" style="width:100%; margin:3px 0 0 0;">
						<span style="display:inline-block;width:96%; padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:800;">
							※성명 및 전화번호 제공에 동의하시겠습니까?
						<input id="chkAgree" style="margin-left:5px;width:18px;height:18px; border:1px solid #dfdfdf;" type="checkbox" />
						</span>
						<div id="divCustSign">
							<canvas id="cvSign" style="border:1px solid #000000;background-image:url('/Resource/Mobile_images/signBackground.png');background-size:100% 100%;width:96%;height:100%;display:none;"></canvas>
							<img id="imgSign" src="" style="border:1px solid black; width:96%; height:100%;display:none; background-color:white;" />
						</div>
					</span>
					<span style="width:100%; margin-bottom:5px; text-align:right;">
                        <input id="btnReg" type="button" class="btn-green" style="border:none;width:33px;margin-top:0 !important;margin-right:5px !important;" value="저장" />
                        <input id="btnDel" type="button" class="btn-black " style="border:none;width:33px;margin-top:0 !important;margin-right:5px !important;" value="삭제" />
                        <input id="btnRegHide" type="button" class="btn-black" style="border:none;width:33px;margin-top:0 !important;margin-right:8px !important;" value="닫기" />
					</span>
				</fieldset> 
			</div>
		</div>
	</div>
    <div id="divScreenLock" class="div-Screenlock"></div>
	<div id="divOtpChk" class="div-modal" style="display:none;">
		<fieldset>
			<span class="inpbox" style="width:100%;padding:10px;">
				<label>인증번호 :&nbsp;</label><br />
                <input type="text" id="txbOTP" style="width:100% !important" maxlength="6"/>
			</span>
			<span class="ickbox" style="width:100%; margin:3px 0 0 0;padding-left:10px;">
				<span style="display:inline-block;width:96%; padding-top:8px;padding-bottom:8px;vertical-align:middle; color:#000; font-size:13px; font-weight:800;display:none;">
					※인증을 할 수 없는 경우 체크하세요?
				<input id="chkOtpSkip" style="margin-left:5px;width:18px;height:18px; border:1px solid #dfdfdf;" type="checkbox" />
				</span>
			</span>
			<span style="width:100%; margin-bottom:5px; text-align:center;">
                <input id="btnOtpChk" type="button" class="btn-green" style="border:none;width:33px;margin-top:0 !important;margin-right:5px !important;" value="확인" />
                <input id="btnOtpClose" type="button" class="btn-black" style="border:none;width:33px;margin-top:0 !important;margin-right:8px !important;" value="닫기" />
			</span>
		</fieldset> 
	</div>				
	<asp:HiddenField ID="hdfOTPProcYn" runat="server" />
	<asp:HiddenField ID="hdfOTPNo" runat="server" />
	<asp:HiddenField ID="hdfSignYn" runat="server" />
	<asp:HiddenField ID="hdfImgSrc" runat="server" />
    <asp:HiddenField ID="hdfProcId" runat="server"/>
    <asp:HiddenField ID="hdfEmpNo" runat="server"/>
	<!--//E: contentsarea -->
</asp:Content>