﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="WTerminalStatbyInvenM.aspx.cs" Inherits="KTSSolutionWeb.WTerminalStatbyInvenM" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/notice_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
        .tabulator {
            position:relative;
            background-color:#fff;
            overflow:hidden;
            font-size:11px;
            text-align:left;
            -ms-transform:translateZ(0);
            transform:translateZ(0);
        }
    </style>

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#btnSelect").click(function (e) {
                    fn_Search();
                });
            }
        };

        var fn_Init = function () {

            var fn_CellICnt = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                if (value == null) value = '0';

                value = priceToString(value);

                if (value != '0') {
                    cell.getElement().style.color = "#077078";
                }
                return value;
            }

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "품명", field: "PRODUCTNM", sorter: "string", minWidth: 100, maxWidth: 250, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", minWidth: 60, maxWidth: 100, resizable: false },
                { headerHozAlign: "center", hozAlign: "right", title: "재고", field: "GOOD_CNT", headerSort: false, minWidth: 50, maxWidth: 100, resizable: false, formatter: fn_CellICnt },
                { headerHozAlign: "center", hozAlign: "right", title: "단가", field: "PRICE", headerSort: false, minWidth: 50, maxWidth: 100, resizable: false, formatter: fn_CellICnt }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, null); // div의 grid id 값
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_Search = function () {
            var nHeight = window.screen.height;

            $('#container').css("height", nHeight - 66.5);
            var gridHeight = nHeight - 250;

            ktsGrid.setHeight(gridHeight + "px");

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);
        }

    </script>
      
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-clothes-request -->
        <div class="page-clothes-request">
			<h2 id="idh2" style="margin-bottom: 0;">무선단말</h2> 
			<div id="iddiv" style="border-bottom:2px solid #dfdfdf;">

            <div class="noticebox">
            <div class="noticecontent">
                <span class="noticecontenttext">
                    재고는 실시간 출고되어 정확하지 않을 수 있으니<br>
                    정확한 재고 확인은 무선개통파트로 문의 부탁드립니다<br>
                    실시간 출고된 수량으로 실재고와 상이할 수 있으니<br>
                    소량 남은 재고는 무선개통파트로 문의 부탁드립니다.<br>
                </span>
            </div>
            </div>
				<fieldset>
					<legend>조회조건</legend>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnSelect" type="button" class="btn-green last" style="border-style:none;width:100%" value="새로고침" />
					</span>
				</fieldset>
            </div>
            <!-- S: scrollBox -->
            <div class="scrollbox">
			    <div class="requestlist">
					<strong style="margin-right:10px;float:left">재고현황</strong>
                    <!-- S:datalist -->
                    <div style="margin-top:10px;">
			            <!-- S:scrollbox -->
			            <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
			            <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
				</div>
		        <!-- E:requestlist -->
			</div>
		    <!-- Escrollbox -->
        </div>
		<!-- E:page-clothes-request -->

    </div>
	<!-- E: contentsarea -->
</asp:Content>
