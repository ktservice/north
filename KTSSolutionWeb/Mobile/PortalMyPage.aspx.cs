﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace KTSSolutionWeb
{
    public partial class PortalMyPage : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetPageInit();
                }
            }
        }

        string authCheck()
        {
            string rtn = string.Empty;
            string authId = string.Empty;
            authId = this.Session["AUTHID"].ToString();

            if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004"))
            {
                rtn = "A";
            }
            else
            {
                rtn = "B";
            }

            return rtn;
        }

        private void SetPageInit()
        {
            //this.hdnMdate.Value = DateTime.Now.ToString("yyyy-MM");
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PORTALMYEMPINFO";
            string jobGb = "GET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetIdmsEmpNo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PORTALIDMSUPDATE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetLaptopMac(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOOLNOTEBOOKINFOEMPTRAN";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PORTALMYCAR";
            string jobGb = "GET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetToolData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PORTALMYTOOL";
            string jobGb = "GET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLaptopData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOOLNOTEBOOKINFOEMP";
            string jobGb = "GET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetSafetyData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PORTALMYSAFETY";
            string jobGb = "GET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

    }
}
