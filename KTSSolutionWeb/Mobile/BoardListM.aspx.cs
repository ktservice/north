﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;  
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;

namespace KTSSolutionWeb
{
    public partial class BoardListM : PageBase
    {
        private string EMPNO
        {
            get
            {
                if (ViewState["EMPNO"] != null)
                {
                    return ViewState["EMPNO"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["EMPNO"] = value;
            }
        }
        private string AUTHID
        {
            get
            {
                if (ViewState["AUTHID"] != null)
                {
                    return ViewState["AUTHID"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["AUTHID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PageUtility pageUtil = new PageUtility(this.Page);
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                EMPNO = this.Session["EMPNO"].ToString();
                AUTHID = this.Session["AUTHID"].ToString();

                btnWrite.Attributes.Add("onclick", "BoardWrite('', '', '" + EMPNO + "', '" + AUTHID + "');");

                if (!IsPostBack)
                {
                    SetDownloadDir();
                    //GetDataList();
                    GetBoardType();

                    string strEmpNo = Page.Session["EMPNO"].ToString();
                    pageUtil.DiligAlertMobile(strEmpNo, "MobileBoard_Dilig");
                }
            }
        }

        private void SetDownloadDir()
        {
            string strEmpNo = Page.Session["EMPNO"].ToString();
            
            string strSource = ConfigurationSettings.AppSettings["mKateDownloadPath"];
            byte[] str2byte = Encoding.UTF8.GetBytes(ConfigurationSettings.AppSettings["ServerNum"].ToString());

            string strPath = DeCryption.Decryption.DecryptString(strSource, ConfigurationSettings.AppSettings["ServerNum"].ToString(), str2byte);

            strPath = strPath + strEmpNo;

            DirectoryInfo di = new DirectoryInfo(strPath);

            if (di.Exists)
            {
                FileInfo[] fis = di.GetFiles();

                for (int i = 0; i < fis.Length; i++)
                {
                    fis[i].Delete();
                }
            }
        }

        private void GetBoardType()
        {
            try
            {
                DataSet ds = new DataSet();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "BOARDTYPELIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                hdfBoardType.Value = dt.Rows[i]["BOARDTYPE"].ToString();
                            }

                            string strInner = "<li><a id=\"tab" + dt.Rows[i]["BOARDTYPE"].ToString() + "\" href=\"#tab" + dt.Rows[i]["BOARDTYPE"].ToString() + "\">" + dt.Rows[i]["CATEGORY"].ToString() + "</a></li>";
                            tabnav.InnerHtml += strInner;
                        }
                    }
                }
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PageError", "alert('페이지 로드 중 오류가 발생했습니다.');window.close();", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strBoardType = hdfBoardType.Value;
                string strTitle = txbTitle.Text.ToString();
                string strEmpnm = txbEmpNm.Text.ToString();

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetBoardList("", strBoardType, strTitle, strEmpnm);
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblBoardID = (Label)rptResult.Items[i].FindControl("lblBoardID");
                            Label lblBoardType = (Label)rptResult.Items[i].FindControl("lblBoardType");
                            Label lblWriterID = (Label)rptResult.Items[i].FindControl("lblWriterID");
                            HtmlGenericControl li = (HtmlGenericControl)rptResult.Items[i].FindControl("li");                            

                            li.Attributes.Add("onclick", "BoardView('" + lblBoardID.Text + "', '" + lblBoardType.Text + "', '" + lblWriterID.Text + "', '" + EMPNO + "', '" + AUTHID + "');");
                        }
                    }

                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnToday_Click(object sender, EventArgs e)
        {
            try
            {
                PageUtility pageUtil = new PageUtility(this.Page);

                string strEmpNo = Page.Session["EMPNO"].ToString();

                pageUtil.SetDiligCookieM(strEmpNo, "MobileBoard_Dilig");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalClose", "ModalClose();", true);

            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}