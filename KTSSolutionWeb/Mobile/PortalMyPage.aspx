﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="PortalMyPage.aspx.cs" Inherits="KTSSolutionWeb.PortalMyPage" %>
<%@ Register Src="~/Controls/MenuControlM.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="../Resource/js/lengthCheck.js"></script>
    <link href="../Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="../Resource/css/tabs.css?20240624" rel="stylesheet" />
    <link href="../Resource/css/themaPortal_M.css?20240624" rel="stylesheet" />

    <style type="text/css">
        .contentsarea ul{
             white-space: nowrap;
            overflow-x: auto; 
            overflow-y: hidden;
            flex-wrap: inherit !important;
            scrollbar-width: none; 
        }

        .optionbox select {
            border: 1px solid #dcfeff;
            border-radius: 2px;
            background: url(/Resource/Mobile_images/icon_select_arr.png) #bdf2f4 no-repeat 100% 50% / 20px auto !important;
            font-size: 14px;
            width: 100% !important;
            padding: 0 24px !important;
        }
        
        fieldset > * {
            font-size: 11px;
        }
        fieldset > li {
            list-style: disc;
            margin: 0 12px;
        }

        .td-widthAI {
          width: 28px !important;
          height: 30px !important;
          padding: 0 !important;
          border-top: 1px solid #9f9f9f !important;
          border-bottom: 1px solid #9f9f9f !important;
          border-left: 0px solid !important;
          border-right: 0px solid !important;
          text-align:center;
          margin-left:auto;
          margin-right:auto;
        }

        .td-widthAS {
          width: 28px !important;
          height: 30px !important;
          padding: 0px 2px 0px 8px !important;
          border-top: 1px solid #9f9f9f !important;
          border-bottom: 1px solid #9f9f9f !important;
          border-left: 1px solid #9f9f9f !important;
          border-right: 0px solid !important;
          text-align:center;
          margin-left:auto;
          margin-right:auto;
        }

        .td-widthAE {
          width: 28px !important;
          height: 30px !important;
          padding: 0px 8px 0px 2px !important;
          border-top: 1px solid #9f9f9f !important;
          border-bottom: 1px solid #9f9f9f !important;
          border-right: 1px solid #9f9f9f !important;
          border-left: 0px solid !important;
          text-align:center;
          margin-left:auto;
          margin-right:auto;
        }

        .td-widthA {
          width: 10px !important;
          height: 30px !important;
          padding: 0px !important;
          border-top: 1px solid #9f9f9f !important;
          border-bottom: 1px solid #9f9f9f !important;
          border-left: 0px solid !important;
          border-right: 0px solid !important;
          text-align:center;
          margin-left:auto;
          margin-right:auto;
        }

        .table-cell-input {
            border: 1px solid #9f9f9f !important;
            width: 100px !important;
            height: 30px !important;
            border-radius: 3px !important;
            /*font-family: 나눔고딕, NanumGothic, qportal_font, 돋움, Dotum, 맑은고딕, MalgunGothic, Arial;*/
            text-align: center;
            /*font-weight: bold;*/
            font-size: 13px;
            margin-left:auto;
            margin-right:auto;
        }

        .table-cell-input2 {
            border: 1px solid #9f9f9f !important;
            width: 90% !important;
            height: 30px !important;
            border-radius: 3px !important;
            text-align: center;
            font-size: 13px;
            margin-left:auto;
            margin-right:auto;
        }

        .colTitle {
            background-color: #fafafc !important;
            border:none;
        }


        .tit-Laptop {
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: #bdf2f4;
            height: 46px;
            border-radius: 6px;
            margin-bottom: 10px;
        }
 
        .contentsarea ul::-webkit-scrollbar { 
          display: none; }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    var tabName = $(this).text();

                    if (tabName == "인사정보") {
                        fn_MyData();
                    }
                    else if (tabName == "기동장비") {
                        fn_MyCarData();
                    }
                    else if (tabName == "공기구") {
                        fn_MyToolData();
                    }
                    else if (tabName == "노트북") {
                        fn_MyLaptopSn();
                    }
                    else if (tabName == "안전용품") {
                        fn_MySafetyData();
                    }

                    return false;
                }).filter(':eq(0)').click();

                $("#selSn").change(function (e) {
                    $('#hdnSn').val(this.value);
                    fn_MyLaptopData('SN', this.value);
                });
            }
        };

        var fn_Init = function () {
            //$("#lblContentTitle").text('마이페이지');
        };

        var fn_Load = function () {
            //fn_MyData();
        };

        function fn_Blur(obj) {
            var id = obj.id;
            var newVal = obj.value.toUpperCase().replace(/[^a-zA-Z-_0-9]/g, '');
            obj.value = newVal;
            $("#" + id.replace('txt', 'hdn') + "").val($("#" + id + "").val());
        }

        function fn_Keypress(obj) {
            //영어문자, 0부터9, 숫자모음(0~9), 백스페이스
            if ((event.keyCode >= 65 && event.keyCode <= 70) || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 97 && event.keyCode <= 102) || (event.keyCode == 8)) {
                return true;
            }
            else {
                return false;
            }
        }

        function fn_Keyup(obj) {
            var mac1 = 'txtMacCable';
            var mac2 = 'txtMacWireless';
            var mac = '';
            var id = obj.id;
            var idx = 0;
            var newVal = obj.value.toUpperCase().replace(/[^a-zA-Z-_0-9]/g, '');
            var oldVal = $("#" + id.replace('txt', 'hdn') + "").val();

            if (!((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode == 8))) {
                return false;
            }

            obj.value = newVal;

            if (obj.value.length == 2) {

                if (newVal == oldVal) {
                    return false;
                }

                $("#" + id.replace('txt', 'hdn') + "").val($("#" + id + "").val());

                if (id.indexOf(mac1) >= 0) {
                    mac = mac1;
                }

                if (id.indexOf(mac2) >= 0) {
                    mac = mac2;
                }

                if (mac != '') {
                    idx = id.replace(mac, '');
                    idx++;
                    id = mac + idx;

                    if (idx > 0 && idx < 7) {
                        $("#" + id + "").focus();
                    }
                }
            }
        }


        var ajaxCallBack = function (json) {
            if (callGb == 'MyData') {
                var empno = '';

                $.each(JSON.parse(json), function (idx, item) {
                    empno = item.EMPNO;

                    $("#spanEmpNm").text(item.EMPNM);
                    $("#spanKtsEmpNo").text(item.KTSEMPNO);
                    $("#spanEmpNo").text(item.EMPNO);
                    $("#txbIdmsEmpNo").val(item.IDMSEMPNO);
                    $("#spanPreIdmsEmpNo").text(item.PREIDMSEMPNO);
                    $("#spanMobile").text(item.MOBILE);
                    $("#spanEmail").text(item.EMAIL);
                });

                if (empno == undefined || empno == "") {
                    $('#divMyInfo').hide();
                    $('#divMyInfoAlert').show();
                }
                else {
                    $('#divMyInfo').show();
                    $('#divMyInfoAlert').hide();
                }

                callGb = '';
            }
            else if (callGb == 'MyCarData') {
                var empno = '';
                var html = '';

                $.each(JSON.parse(json), function (idx, item) {
                    empno = item.EMPNO;

                    var htmlData = '';

                    htmlData += '   <div class="total-data">';
                    htmlData += '       <div class="tit-area">';
                    htmlData += '           <h3>' + item.CARNO + '</h3>';
                    htmlData += '       </div>';
                    htmlData += '       <table class="table table-bordered">';
                    /*htmlData += '           <thead>';
                    htmlData += '               <tr>';
                    htmlData += '	                <th class="table-first-header" style="position:relative" colspan="2">';
                    htmlData += '		                <span style="line-height:32px;font-size: 15px;font-weight: bold;color: #000;">' + item.CARNO + '</span>';
                    htmlData += '	                </th>';
                    htmlData += '               </tr>';
                    htmlData += '           </thead>';*/
                    htmlData += '           <tbody>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">차대번호</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.CARID + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">차종</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.CARTYPENM + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">차명(모델)</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.MODELNM + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">주유카드</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.FUELCARD + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">하이패스카드</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.HIPASSCARD + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">충전카드</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.CHARGECARD + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '           </tbody>';
                    htmlData += '       </table>';
                    htmlData += '   </div>';
                    htmlData += '   <h2></h2>';

                    html += htmlData;
                });

                $('#divMyCar').html(html);

                if (empno == undefined || empno == "") {
                    $('#divMyCar').hide();
                    $('#divMyCarAlert').show();
                }
                else if (html != '') {
                    $('#divMyCar').show();
                    $('#divMyCarAlert').hide();
                }
                else {
                    $('#divMyCar').hide();
                    $('#divMyCarAlert').show();
                }

                callGb = '';
            }
            else if (callGb == 'MyToolData' || callGb == 'MySafetyData') {
                var empno = '';
                var html = '';

                $.each(JSON.parse(json), function (idx, item) {
                    empno = item.EMPNO;

                    var htmlData = '';

                    htmlData += '   <div class="total-data">';
                    htmlData += '       <div class="tit-area">';
                    htmlData += '           <h3>' + item.ITEMNM + '</h3>';
                    htmlData += '       </div>';
                    htmlData += '       <table class="table table-bordered">';
                    /*htmlData += '           <thead>';
                    htmlData += '               <tr>';
                    htmlData += '	                <th class="table-first-header" style="position:relative" colspan="2">';
                    htmlData += '		                <span style="line-height:32px;font-size: 15px;font-weight: bold;color: #000;">' + item.ITEMNM + '</span>';
                    htmlData += '	                </th>';
                    htmlData += '               </tr>';
                    htmlData += '           </thead>';*/
                    htmlData += '           <tbody>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">구매일자</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.BUY_DATE + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">내용연수</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.SERVICELIFE + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">사용용도</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.USENM + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">제품상태</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.ITEMNM_STATUSNM + '</td>';
                    htmlData += '               </tr>';

                    if (callGb == 'MySafetyData') {
                        htmlData += '               <tr>';
                        htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">보유형태</td>';
                        htmlData += '	                <td class="table-cell td_backwhite">' + item.HOLD_TYPENM + '</td>';
                        htmlData += '               </tr>';
                    }

                    htmlData += '           </tbody>';
                    htmlData += '       </table>';
                    htmlData += '   </div>';
                    htmlData += '   <h2></h2>';

                    html += htmlData;
                });

                if (callGb == 'MyToolData') {
                    $('#divMyTool').html(html);

                    if (empno == undefined || empno == "") {
                        $('#divMyTool').hide();
                        $('#divMyToolAlert').show();
                    }
                    else if (html != '') {
                        $('#divMyTool').show();
                        $('#divMyToolAlert').hide();
                    }
                    else {
                        $('#divMyTool').hide();
                        $('#divMyToolAlert').show();
                    }
                }
                else {
                    $('#divMySafety').html(html);

                    if (empno == undefined || empno == "") {
                        $('#divMySafety').hide();
                        $('#divMySafetyAlert').show();
                    }
                    else if (html != '') {
                        $('#divMySafety').show();
                        $('#divMySafetyAlert').hide();
                    }
                    else {
                        $('#divMySafety').hide();
                        $('#divMySafetyAlert').show();
                    }
                }

                callGb = '';
            }
            if (callGb == 'MyLaptopSn') {
                obj = $('#selSn');
                callGb = '';
                obj.empty();

                var cd = '';
                var nm = '';
                var htmlData = '';
                var cnt = 0;
                $.each(JSON.parse(json), function (idx, item) {

                    cd = item.CD;
                    nm = item.NM;

                    obj.append("<option value='" + cd + "'>" + nm + "</option>");
                    cnt++;
                });

                if (cnt == 1) {
                    $('#divMyLaptopAlert').hide();
                    $('#btnLaptopReg').show();
                    obj.hide();

                    htmlData = '<h3>' + nm +'</h3>';
                    $('.tit-Laptop').html(htmlData);

                    $('#hdnSn').val(cd);
                    fn_MyLaptopData('SN', cd);

                }
                else if (cnt > 1) {
                    $('#divMyLaptopAlert').hide();
                    $('#btnLaptopReg').show();
                    obj.show();

                    if ($('#hdnSn').val() != '') {
                        $("#selSn").val($('#hdnSn').val()).prop("selected", true);
                    }
                    else {
                        $('#hdnSn').val($("#selSn").val());
                        fn_MyLaptopData('SN', $("#selSn").val());
                    }
                    
                }
                else {
                    $('#btnLaptopReg').hide();
                    $('.tit-Laptop').remove();
                    $('.data-Laptop').remove();
                    $('.total-data-Laptop').remove();
                }

            }
            else if (callGb == 'MyLaptopData') {
                var empno = '';
                var html = '';

                $.each(JSON.parse(json), function (idx, item) {
                    empno = item.EMPNO;

                    var vMacCable = item.MAC_CABLE;
                    var vMacWireless = item.MAC_WIRELESS;

                    var vMacCable1 = '';
                    var vMacCable2 = '';
                    var vMacCable3 = '';
                    var vMacCable4 = '';
                    var vMacCable5 = '';
                    var vMacCable6 = '';

                    var vMacWireless1 = '';
                    var vMacWireless2 = '';
                    var vMacWireless3 = '';
                    var vMacWireless4 = '';
                    var vMacWireless5 = '';
                    var vMacWireless6 = '';


                    if (fn_MacAddressCheck(vMacCable)) {
                        var arrMacCable = vMacCable.split('-')

                        vMacCable1 = arrMacCable[0];
                        vMacCable2 = arrMacCable[1];
                        vMacCable3 = arrMacCable[2];
                        vMacCable4 = arrMacCable[3];
                        vMacCable5 = arrMacCable[4];
                        vMacCable6 = arrMacCable[5];
                    }

                    if (fn_MacAddressCheck(vMacWireless)) {
                        var arrMacWireless = vMacWireless.split('-')

                        vMacWireless1 = arrMacWireless[0];
                        vMacWireless2 = arrMacWireless[1];
                        vMacWireless3 = arrMacWireless[2];
                        vMacWireless4 = arrMacWireless[3];
                        vMacWireless5 = arrMacWireless[4];
                        vMacWireless6 = arrMacWireless[5];
                    }

                    var htmlData = '';

                    htmlData += '       <table class="table table-bordered">';
                    htmlData += '           <tbody>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">S/N</td>';
                    htmlData += '	                <td class="table-cell td_backwhite"><span id="lblNoteBookSN">' + item.SN + '</span></td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">모델명</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.MODELNM + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">제조사</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.MAKERNM + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">색상</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.COLOR + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">속도</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.SPEED + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">지급일자</td>';
                    htmlData += '	                <td class="table-cell td_backwhite">' + item.GIVE_DATE + '</td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">MAC주소(유선)</td>';
                    htmlData += '	                <td class="table-cell td_backwhite" ><span style="DISPLAY: inline-flex">';
                    htmlData += '	                <input type="text" id="txtMacCable1" maxlength="2" tabindex="1" class="td-widthAS" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacCable1 + '"/> ';
                    htmlData += '	                <input type="text" value="-" class="td-widthA" readonly="readonly"/>                 ';                                                                  
                    htmlData += '	                <input type="text" id="txtMacCable2" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacCable2 + '"/> ';
                    htmlData += '	                <input type="text" value="-" class="td-widthA" readonly="readonly"/>               ';                                                                    
                    htmlData += '	                <input type="text" id="txtMacCable3" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacCable3 + '"/> ';
                    htmlData += '	                <input type="text" value="-" class="td-widthA" readonly="readonly"/>               ';                                                                    
                    htmlData += '	                <input type="text" id="txtMacCable4" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacCable4 + '"/> ';
                    htmlData += '	                <input type="text" value="-" class="td-widthA" readonly="readonly"/>               ';                                                                    
                    htmlData += '	                <input type="text" id="txtMacCable5" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacCable5 + '"/> ';
                    htmlData += '	                <input type="text" value="-" class="td-widthA" readonly="readonly"/>               ';                                                                    
                    htmlData += '	                <input type="text" id="txtMacCable6" maxlength="2" tabindex="1" class="td-widthAE" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacCable6 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacCable1" value="' + vMacCable1 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacCable2" value="' + vMacCable2 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacCable3" value="' + vMacCable3 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacCable4" value="' + vMacCable4 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacCable5" value="' + vMacCable5 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacCable6" value="' + vMacCable6 + '"/> ';
                    htmlData += '	                </span></td>';
                    htmlData += '               </tr>';
                    htmlData += '               <tr>';
                    htmlData += '	                <td class="table-cell border_right" style="font-weight: bold;">MAC주소(무선)</td>';
                    //htmlData += '	                <td class="table-cell td_backwhite"><input id="txbMacWireless" type="text" maxlength="30" class="table-cell-input2" value="' + item.MAC_WIRELESS + '" /></td > ';
                    htmlData += '	                <td class="table-cell td_backwhite" ><span style="DISPLAY: inline-flex">';
                    htmlData += '                   <input type="text" id="txtMacWireless1" maxlength="2" tabindex="1" class="td-widthAS" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacWireless1 + '"/> ';
                    htmlData += '                   <input type="text" value="-" class="td-widthA" readonly="readonly"/>                  ';                                                                    
                    htmlData += '                   <input type="text" id="txtMacWireless2" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacWireless2 + '"/> ';
                    htmlData += '                   <input type="text" value="-" class="td-widthA" readonly="readonly"/>                  ';                                                                    
                    htmlData += '                   <input type="text" id="txtMacWireless3" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacWireless3 + '"/> ';
                    htmlData += '                   <input type="text" value="-" class="td-widthA" readonly="readonly"/>                  ';                                                                    
                    htmlData += '                   <input type="text" id="txtMacWireless4" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacWireless4 + '"/> ';
                    htmlData += '                   <input type="text" value="-" class="td-widthA" readonly="readonly"/>                  ';                                                                    
                    htmlData += '                   <input type="text" id="txtMacWireless5" maxlength="2" tabindex="1" class="td-widthAI" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacWireless5 + '"/> ';
                    htmlData += '                   <input type="text" value="-" class="td-widthA" readonly="readonly"/>                  ';                                                                    
                    htmlData += '                   <input type="text" id="txtMacWireless6" maxlength="2" tabindex="1" class="td-widthAE" onkeypress="return fn_Keypress(this)" onkeyup="return fn_Keyup(this)" onblur="return fn_Blur(this)" value="' + vMacWireless6 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacWireless1" value="' + vMacWireless1 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacWireless2" value="' + vMacWireless2 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacWireless3" value="' + vMacWireless3 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacWireless4" value="' + vMacWireless4 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacWireless5" value="' + vMacWireless5 + '"/> ';
                    htmlData += '	                <input type="hidden" id="hdnMacWireless6" value="' + vMacWireless6 + '"/> ';
                    htmlData += '	                </span></td>';
                    htmlData += '               </tr>';
                    htmlData += '           </tbody>';
                    htmlData += '       </table>';

                    html += htmlData;
                });

                $('.data-Laptop').html(html);
                
                if (empno == undefined || empno == "") {
                    $('#btnLaptopReg').hide();
                    $('.tit-Laptop').remove();
                    $('.data-Laptop').remove();
                    $('.total-data-Laptop').remove();

                    $('#divMyLaptopAlert').show();
                }
                else if (html != '') {
                    $('#divMyLaptop').show();
                    $('#divMyLaptopAlert').hide();
                }
                else {
                    $('#btnLaptopReg').hide();
                    $('.tit-Laptop').remove();
                    $('.data-Laptop').remove();
                    $('.total-data-Laptop').remove();

                    $('#divMyLaptopAlert').show();
                }

                callGb = '';
            }
            else if (callGb == 'SetLaptopMac') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        alert('노트북 정보가 변경되었습니다.');
                        callGb = '';

                        fn_MyLaptopData('SN', $("#selSn").val());
                    } else if (item.ERRCD == '01') {
                        alert('노트북 정보 저장 중 오류가 발생했습니다.');
                        callGb = '';
                    } else if (item.ERRCD == '02') {
                        window.location.href = '/MobileSessionExpire';
                    }
                });

                callGb = '';
            }
            else {
                $.each(JSON.parse(json), function (idx, item) {
                    if (callGb == 'SetIdmsOk') {
                        if (item.ERRCD == '00') {
                            alert('IDMS사번이 수정되었습니다.');
                            callGb = '';
                            fn_MyData();
                        } else if (item.ERRCD == '01') {
                            alert('IDMS 저장 중 오류가 발생했습니다.');
                            callGb = '';
                        } else if (item.ERRCD == '02') {
                            window.location.href = '/MobileSessionExpire';
                        } else if (item.ERRCD == '03') {
                            alert('중복된 IDMS사번이 존재합니다.');
                            callGb = '';
                        }
                    }
                });
            }
        };

        var fn_MyData = function () {
            callGb = 'MyData';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };

        var fn_SetIdmsEmpNo = function () {
            if (confirm('IDMS 사번을 수정하시면 (구)IDMS사번은 변경 전 IDMS사번으로 저장됩니다. 수정 하시겠습니까? ')) {
                var idmsEmpNo = $("#txbIdmsEmpNo").val();

                if (idmsEmpNo.length > 0 && idmsEmpNo.substring(0, 1) == "8") {
                    alert('유효하지않은 IDMS 사번입니다. 확인해주세요.');
                }
                else {
                    callGb = 'SetIdmsOk';

                    //입력 파라미터
                    var aData = [];

                    aData[0] = idmsEmpNo;

                    var jsonData = JSON.stringify({ aData: aData });
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'SetIdmsEmpNo');

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        };

        var fn_SetLaptopMac = function () {
            if (confirm('정보를 변경 하시겠습니까? ')) {

                var mac1 = $("#txbMacCable").val();
                var mac2 = $("#txbMacWireless").val();

                mac1 = fn_AddressText($("#txtMacCable1").val(), $("#txtMacCable2").val(), $("#txtMacCable3").val(), $("#txtMacCable4").val(), $("#txtMacCable5").val(), $("#txtMacCable6").val());
                mac2 = fn_AddressText($("#txtMacWireless1").val(), $("#txtMacWireless2").val(), $("#txtMacWireless3").val(), $("#txtMacWireless4").val(), $("#txtMacWireless5").val(), $("#txtMacWireless6").val());

                if (mac1.length > 0) {
                    if (!fn_MacAddressCheck(mac1)) {
                        alert('MAC주소(유선)값이 잘못되어있습니다.');
                        return false;
                    }
                }
                
                if (mac2.length > 0) {
                    if (!fn_MacAddressCheck(mac2)) {
                        alert('MAC주소(무선)값이 잘못되어있습니다.');
                        return false;
                    }
                }

                if (!getStringLengthCheck(mac1, 30)) {
                    alert('MAC주소(유선) 입력값이 초과됐습니다.');
                    return false;
                }
                if (!getStringLengthCheck(mac2, 30)) {
                    alert('MAC주소(무선) 입력값이 초과됐습니다.');
                    return false;
                }

                callGb = 'SetLaptopMac';

                //입력 파라미터
                var aData = [];

                aData[0] = $("#lblNoteBookSN").text();
                aData[1] = mac1;
                aData[2] = mac2;
                

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetLaptopMac');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        };

        function fn_MacAddressCheck(value) {
            if (value == null) {
                value = '';
            }

            if (value.length == 17) {
                var macformat = /^([0-9A-F]{2}[:-]?){5}([0-9A-F]{2})$/;
                if (value.match(macformat)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        var fn_AddressText = function (str1, str2, str3, str4, str5, str6) {
            var val = '';

            if (str1 != '') val = str1;
            if (str2 != '') val += '-' + str2;
            if (str3 != '') val += '-' + str3;
            if (str4 != '') val += '-' + str4;
            if (str5 != '') val += '-' + str5;
            if (str6 != '') val += '-' + str6;

            return val.toUpperCase();
        }

        var fn_MyLaptopSn = function () {
            callGb = 'MyLaptopSn';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLaptopData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };

        var fn_MyCarData = function () {
            callGb = 'MyCarData';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };

        var fn_MyToolData = function () {
            callGb = 'MyToolData';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetToolData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };

        var fn_MyLaptopData = function (gubun, param) {
            callGb = 'MyLaptopData';

            var param1 = '';

            if (gubun == 'SN') {
                param1 = param;
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = param1;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLaptopData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };

        var fn_MySafetyData = function () {
            callGb = 'MySafetyData';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetSafetyData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };

    </script>

	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div>
            <!-- S:UserControlTitle -->
            <uc:menu ID="menu" runat="server" />
            <!-- E:UserControlTitle -->
            <h2></h2>
            <!-- S: contentsarea -->
            <div class="contentsarea">
                <ul class="tabnav">
                    <li><a href="#tab0" class="active">인사정보</a></li> <!-- 인사정보 -->
                    <li><a href="#tab1">기동장비</a></li> <!-- 기동장비 -->
                    <li><a href="#tab2">공기구</a></li> <!-- 공기구 -->
                    <li><a href="#tab3">노트북</a></li> <!-- 공기구(노트북) -->
                    <li><a href="#tab4">안전용품</a></li> <!-- 안전용품 -->
                </ul>
            </div>
            <!-- E: contentsarea -->
            <div class="gab"></div>

            <!-- S: scrollBox -->
            <div class="tabcontent">
                <div id="tab0">
                    <div class="scrollbox mobile-body">
                        <fieldset class="view-data-list-body">
                            <div class="data-group">
                                <div id="divMyInfo">
                                    <div class="total-data">
                                        <div class="tit-area">
                                            <h3>인사정보</h3>
                                        </div>
                    	                <table class="table table-bordered">
                    		                <%--<thead>
                    			                <tr>
                    				                <th class="table-first-header" style="position:relative" colspan="2">
                    					                <span style="line-height:32px;font-size: 15px;font-weight: bold;color: #000;">인사정보</span>
                    				                </th>
                    			                </tr>
                    		                </thead>--%>
                    		                <tbody>
                    			                <tr>
                    				                <td class="table-cell border_right" style="font-weight: bold;">이름</td>
                    				                <td class="table-cell td_backwhite"><span id="spanEmpNm"></span></td>
                    			                </tr>
                    			                <tr>
                    				                <td class="table-cell border_right" style="font-weight: bold;">kts사번</td>
                    				                <td class="table-cell td_backwhite"><span id="spanKtsEmpNo"></span></td>
                    			                </tr>
                    			                <tr>
                    				                <td class="table-cell border_right" style="font-weight: bold;">사번</td>
                    				                <td class="table-cell td_backwhite"><span id="spanEmpNo"></span></td>
                    			                </tr>
                    			                <tr>
                    				                <td class="table-cell border_right" style="font-weight: bold;">IDMS사번</td>
                    				                <td class="table-cell2 td_backwhite">
                                                        <input id="txbIdmsEmpNo" type="text" maxlength="20" class="table-cell-input"/>
                    				                </td>
                    			                </tr>
                    			                <tr>
                    				                <td class="table-cell border_right" style="font-weight: bold;">(구)IDMS사번</td>
                    				                <td class="table-cell td_backwhite"><span id="spanPreIdmsEmpNo"></span></td>
                    			                </tr>
                    			                <tr>
                    				                <td class="table-cell border_right" style="font-weight: bold;">전화번호</td>
                    				                <td class="table-cell td_backwhite"><span id="spanMobile"></span></td>
                    			                </tr>
                    			                <tr>
                    				                <td class="table-cell border_right" style="font-weight: bold;">이메일</td>
                    				                <td class="table-cell td_backwhite"><span id="spanEmail"></span></td>
                    			                </tr>
                    		                </tbody>
                    	                </table>
                                        <input onclick="fn_SetIdmsEmpNo();" style="font-size:12px;font-weight:bold;color:#000; float:right;" class="btn-lightgray" type="button" title="IDMS수정" value="IDMS수정" />
                                    </div>
                                    <h2></h2>
                                </div>
                                <div id="divMyInfoAlert">
                                    <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div id="tab1" style="display:none;">
                    <div class="scrollbox mobile-body">
                        <fieldset class="view-data-list-body">
                            <div class="data-group">
                                <div id="divMyCar">
                                </div>
                                <div id="divMyCarAlert">
                                    <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div id="tab2" style="display:none;">
                    <div class="scrollbox mobile-body">
                        <fieldset class="view-data-list-body">
                            <div class="data-group">
                                <div id="divMyTool">
                                </div>
                                <div id="divMyToolAlert">
                                    <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div id="tab3" style="display:none;">
                    <div class="scrollbox mobile-body">
                        <fieldset class="view-data-list-body">
                            <div class="data-group">
                                <div id="divMyLaptop">
                                    <div class="total-data-Laptop">
                                        <div class="tit-Laptop">
                                            <span id="lblLaptopTitle" class="optionbox">
                                               <select id="selSn" style="display:none;"></select>
                                            </span>
                                        </div>
                                        <div class="data-Laptop"></div>
                                        <input id="btnLaptopReg" onclick="fn_SetLaptopMac();" style="font-size:12px;font-weight:bold;color:#000; float:right;" class="btn-lightgray" type="button" title="정보변경" value="정보변경" />
                                        <h2></h2>
                                     </div>
                                </div>
                                <div id="divMyLaptopAlert">
                                    <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                                    <input id="hdnSn" type="hidden" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div id="tab4" style="display:none;">
                    <div class="scrollbox mobile-body">
                        <fieldset class="view-data-list-body">
                            <div class="data-group">
                                <div id="divMySafety">
                                </div>
                                <div id="divMySafetyAlert">
                                    <div class="alert alert-info">항목이 발견되지 않았습니다. </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <!-- E: scrollBox -->
		</div>	
	</div>
	<!--//E: contentsarea -->
</asp:Content>