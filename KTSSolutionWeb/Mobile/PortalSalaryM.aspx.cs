﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace KTSSolutionWeb
{
    public partial class PortalSalaryM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetPageInit();
                }
            }
        }

        string authCheck()
        {
            string rtn = string.Empty;
            string authId = string.Empty;
            authId = this.Session["AUTHID"].ToString();

            if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004"))
            {
                rtn = "A";
            }
            else
            {
                rtn = "B";
            }

            return rtn;
        }

        private void SetPageInit()
        {
            this.hdnMdate.Value = DateTime.Now.ToString("yyyy-MM");
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPayData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string empNo = string.Empty;
            string query = string.Empty;
            string storedProcedure = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            DbMsSqlTransaction dbTransaction = new DbMsSqlTransaction(HttpContext.Current);
            resultSession = dbTransaction.SessionCheck("/MLogin");

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                /*
                DataTable dt = new DataTable();
                DataTable dt2 = new DataTable();

                dt2.Columns.Add("NAME", typeof(string));
                dt2.Columns.Add("OC_EMP_NO", typeof(string));
                dt2.Columns.Add("PROV_TYPE_CD", typeof(string));
                dt2.Columns.Add("PROV_TYPE", typeof(string));

                dt2.Columns.Add("PROV_DT", typeof(DateTime));
                dt2.Columns.Add("PAY_TOT_AMT", typeof(long));
                dt2.Columns.Add("SUB_TOT_AMT", typeof(long));
                dt2.Columns.Add("REAL_PROV_AMT", typeof(long));
                dt2.Columns.Add("TAX_AMT", typeof(long));
                dt2.Columns.Add("TAX_FREE", typeof(long));

                dt2.Columns.Add("INCOME_TAX", typeof(long));
                dt2.Columns.Add("RES_TAX", typeof(long));

                dt2.Rows.Add("홍길동1", "91111111", "1", "타입명", DateTime.Now, 300, 250, 400, 300, 250, 400, 1000);
                dt2.Rows.Add("홍길동2", "91111112", "A", "타입명2", DateTime.Now, 400, 210, 420, 310, 220, 410, 1200);

                result = Utility.DataTableToJson(dt2);
                */
                
                storedProcedure = "[KTS_NORTH].[dbo].[USP_KTS_M_PAY_KO806]";
                aData.Add(empNo); //파라미터 추가 시

                query = dbTransaction.GetCallSpString(storedProcedure, aData.ToArray());
                result = dbTransaction.GetJsonData(query);

            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPayDtlData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string empNo = string.Empty;
            string query = string.Empty;
            string storedProcedure = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            DbMsSqlTransaction dbTransaction = new DbMsSqlTransaction(HttpContext.Current);
            resultSession = dbTransaction.SessionCheck("/MLogin");

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                /*
                DataTable dt = new DataTable();
                DataTable dt2 = new DataTable();

                dt2.Columns.Add("allow_cd1", typeof(string));
                dt2.Columns.Add("allow_nm1", typeof(string));
                dt2.Columns.Add("allow_seq", typeof(long));
                dt2.Columns.Add("allow", typeof(long));
                dt2.Columns.Add("allow2", typeof(long));
                dt2.Columns.Add("sum_prov_tot_amt", typeof(long));
                dt2.Columns.Add("sum_real_prov_amt", typeof(long));
                dt2.Columns.Add("sum_sub_tot_amt", typeof(long));

                dt2.Columns.Add("PAY_TOT_AMT", typeof(long));
                dt2.Columns.Add("REAL_PROV_AMT", typeof(long));
                dt2.Columns.Add("SUB_TOT_AMT", typeof(long));

                dt2.Rows.Add("code1", "codeNm1", 1, 100, 210, 320, 251, 401, 1121,2122, 3133);
                dt2.Rows.Add("code2", "codeNm2", 4, 110, 220, 350, 252, 402, 1131,2222, 3233);
                dt2.Rows.Add("code3", "codeNm3", 5, 102, 203, 301, 253, 403, 1141, 2322, 3333);
                dt2.Rows.Add("code4", "codeNm4", 6, 130, 240, 302, 254, 404, 1151, 2242, 3433);
                dt2.Rows.Add("code5", "codeNm5", 11, 140, 250, 340, 255, 405, 1611, 2522, 3353);
                dt2.Rows.Add("code6", "codeNm6", 2, 105, 206, 305, 256, 406, 1171, 2262, 3336);


                dt = (from table in dt2.AsEnumerable()
                      orderby table.Field<long>("allow_seq") ascending
                      select table).CopyToDataTable();

                result = Utility.DataTableToJson(dt);
                
                */
                DataSet ds = new DataSet();

                storedProcedure = "[KTS_NORTH].[dbo].[USP_KTS_M_PAY_DTL_KO806]";
                aData[1] = empNo;

                query = dbTransaction.GetCallSpString(storedProcedure, aData.ToArray());
                ds = dbTransaction.GetDataSet(query);

                if(ds != null)
                {
                    DataTable dt = new DataTable();

                        dt = ds.Tables[0];
                    /*
                    dt = (from table in ds.Tables[0].AsEnumerable()
                         orderby table.Field<Decimal>("allow_seq") ascending
                         select table).CopyToDataTable();
                    */
                    result = Utility.DataTableToJson(dt);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("00", "", "");
                    result = Utility.DataTableToJson(dt);
                }

            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPayDtlSubData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string empNo = string.Empty;
            string query = string.Empty;
            string storedProcedure = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            DbMsSqlTransaction dbTransaction = new DbMsSqlTransaction(HttpContext.Current);
            resultSession = dbTransaction.SessionCheck("/MLogin");

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                /*
                //테스트
                DataTable dt = new DataTable();
                DataTable dt2 = new DataTable();

                dt2.Columns.Add("allow_cd1", typeof(string));
                dt2.Columns.Add("allow_nm1", typeof(string));
                dt2.Columns.Add("allow_seq", typeof(long));
                dt2.Columns.Add("sub_amt", typeof(long));
                dt2.Columns.Add("sub_amt2", typeof(long));
                  
                dt2.Rows.Add("code1", "codeNm1", 1, 100, 210);
                dt2.Rows.Add("code2", "codeNm2", 5, 120, 202);
                dt2.Rows.Add("code3", "codeNm3", 11, 130, 230);
                dt2.Rows.Add("code4", "codeNm4", 2, 104, 240);
                dt2.Rows.Add("code5", "codeNm5", 3, 122, 205);
                dt2.Rows.Add("code6", "codeNm6", 4, 101, 260);


                dt = (from table in dt2.AsEnumerable()
                      orderby table.Field<long>("allow_seq") ascending
                      select table).CopyToDataTable();

                result = Utility.DataTableToJson(dt);
                
                */

                DataSet ds = new DataSet();

                storedProcedure = "[KTS_NORTH].[dbo].[USP_KTS_M_PAY_SUB_DTL_KO806]";
                aData[1] = empNo;

                query = dbTransaction.GetCallSpString(storedProcedure, aData.ToArray());
                ds = dbTransaction.GetDataSet(query);

                if (ds != null)
                {
                    DataTable dt = new DataTable();

                    dt = ds.Tables[0];
                    /*
                    dt = (from table in ds.Tables[0].AsEnumerable()
                          orderby table.Field<Decimal> ("allow_seq") ascending
                          select table).CopyToDataTable();
                    */
                    
                    result = Utility.DataTableToJson(dt);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("00", "", "");
                    result = Utility.DataTableToJson(dt);
                }
                
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}
