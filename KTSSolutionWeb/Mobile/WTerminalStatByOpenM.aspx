﻿<%@ Page Language="C#" MasterPageFile="~/Site.M.Master" AutoEventWireup="true" CodeBehind="WTerminalStatByOpenM.aspx.cs" Inherits="KTSSolutionWeb.WTerminalStatByOpenM" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .tabulator {
            position:relative;
            background-color:#fff;
            overflow:hidden;
            font-size:11px;
            text-align:left;
            -ms-transform:translateZ(0);
            transform:translateZ(0);
        }

        .tabulator-row .tabulator-responsive-collapse table {
          font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Init();
                //fn_Load();
            },
            eventbind: function () {
                $("#btnSelect").click(function (e) {
                    fn_Search();
                });
            }
        };

        var fn_Init = function () {

            var Columns = [
                { formatter: "responsiveCollapse", width: 25, minWidth: 25, hozAlign: "center", resizable: false, headerSort: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", resizable: false, width: 60, visible: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일자", field: "RECEIPT_DATE", sorter: "string", resizable: false, width: 80, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", minWidth: 60, maxWidth: 80, resizable: false, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "모델명", field: "MODELNM", sorter: "string", resizable: false, minWidth: 80, maxWidth: 100, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "PROGRESS_STATUS", resizable: false, minWidth: 80, maxWidth: 100, responsive: 0 },
                { headerHozAlign: "center", hozAlign: "center", title: "색상", field: "COLOR", sorter: "string", width: 400, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처", field: "MOBILE", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "가입유형", field: "JOIN_TYPE", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "요금제명", field: "PHONEPLAN", sorter: "string", width: 200, responsive: 2 },
                { headerHozAlign: "center", hozAlign: "center", title: "배송중유무", field: "SHIPPINGYN", sorter: "string", width: 200, responsive: 2, visible:false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridCollapseM('ktsGrid', 'fitDataFill', true, null); // div의 grid id 값
        }


        function fn_Search() {
            var sdt = new Date($("#<%= txbStDt.ClientID %>").val());
            var edt = new Date($("#<%= txbEnDt.ClientID %>").val());

            var dateDiff = Math.ceil((edt.getTime() - sdt.getTime()) / (1000 * 3600 * 24));

            if (dateDiff > 30) {
                alert(" 조회기간은 30일을 초과 할 수 없습니다.");
            }
            else {
                //입력 파라미터
                var aData = [];
                aData[0] = $("#<%= txbStDt.ClientID %>").val();
                aData[1] = $("#<%= txbEnDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
        }
    </script>        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-clothes-request -->
        <div class="page-clothes-request">
			<h2>무선개통현황</h2> 
			<div style="border-bottom:2px solid #dfdfdf;">
				<fieldset>
					<legend>조회조건</legend>
					<span class="inpbox" id="spanDate" runat="server" style="width:100%; margin:3px 0 3px 0; font-size:0;">
						<label style="margin:0 !important;margin-left:0;">일자</label><br />
						<asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
						<label style="width:10px">~</label>
						<asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;font-size:11px;padding-left:4px;padding-right:1px;"></asp:TextBox>
					</span>
					<span style="width:100%; margin:11px 0 11px 0; font-size:0; text-align:center;">
                        <input id="btnSelect" type="button" class="btn-green" style="border-style:none;margin-right:10px;" value="조회" />
					</span>
				</fieldset>
            </div>
            <!-- S: scrollBox -->
            <div class="scrollbox">
			    <div class="requestlist">
					<strong style="margin-top:2px;margin-right:10px;float:left">현황조회</strong>
                    <!-- S:datalist -->
                    <div class="datalist">
			            <!-- S:scrollbox -->
			            <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
			            <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
				</div>
		        <!-- E:requestlist -->
			</div>
		    <!-- Escrollbox -->
        </div>
		<!-- E:page-clothes-request -->

    </div>
	<!-- E: contentsarea -->
</asp:Content>
