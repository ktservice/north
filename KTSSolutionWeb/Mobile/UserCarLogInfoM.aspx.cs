﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;

namespace KTSSolutionWeb
{
    public partial class UserCarLogInfoM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");

            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    SetDDLCarNo();

                    SetPageInit();
                }

                this.btnSelect.Attributes.Add("onclick", "fn_Search();");
                this.btnDivLogView.Attributes.Add("onclick", "LogViewBtnClick();");
            }
        }

        private void SetDDLCarNo()
        {
            DataSet ds = null;

            try
            {
                string resultSession = string.Empty;
                string rptCd = "USERCARLIST2";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                string empNo = HttpContext.Current.Session["EMPNO"].ToString();

                aData.Add(empNo); //파라미터 추가 시
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PageInit", "alert('오류가 발생했습니다.');", true);
                }

                ddlCarNo.Items.Clear();
                ddlCarNo.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlCarNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CARNO"].ToString(), ds.Tables[0].Rows[i]["CARID"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetPageInit()
        {
            try
            {
                this.txbStDt.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");

                string authId = HttpContext.Current.Session["AUTHID"].ToString();

                if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003"))
                {
                    this.ddlProcType.Enabled = true;
                    
                    this.ddlProcType.Items.Add(new ListItem("삭제", "DELETE"));
                    this.btnDelLog.Visible = true;
                }
                else
                {
                    this.spanICarNo.Style.Remove("width");
                    this.spanICarNo.Style.Add("width", "100%");
                    this.spanProcType.Style.Add("display", "none");

                    this.ddlProcType.Enabled = false;

                    this.btnDelLog.Visible = false;
                }

                this.spanEmpNo.Style.Remove("display");
                this.spanEmpNm.Style.Remove("display");

                if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004"))
                {

                    this.txbEmpNo.Text = "";
                    this.txbEmpNm.Text = "";
                }
                else
                {
                    this.spanEmpNo.Style.Add("display", "none");
                    this.spanEmpNm.Style.Add("display", "none");

                    this.txbEmpNo.Text = Session["EMPNO"].ToString();
                    this.txbEmpNm.Text = Session["EMPNM"].ToString();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hdfLeaveCnt_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int nCnt = 0;
                string strLeaveCnt = this.hdfLeaveCnt.Value.ToString();

                int.TryParse(strLeaveCnt, out nCnt);
                string authId = HttpContext.Current.Session["AUTHID"].ToString();

                if ((authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004")) && nCnt > 0)
                {
                    this.btnAllConf.Visible = true;
                    this.btnAllConf.Attributes.Add("onClick", "SetLeaveConfirm_All();");
                }
                else
                {
                    this.btnAllConf.Visible = false;
                    this.btnAllConf.Attributes.Remove("onClick");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }

        private static bool ValidationChk(int nStKm, int nEnKm, int nLeaveKm, string strLeaveYn, out string strMsg)
        {
            bool bChk = false;
            strMsg = "";

            if (nEnKm == 0)
            {
                strMsg = "종료km 를 입력해주세요.";
            }
            else if (nEnKm <= nStKm)
            {
                strMsg = "종료km는 시작km 보다 커야합니다.";
            }
            else
            {
                bChk = true;
            }

            if (bChk && strLeaveYn.Equals("Y"))
            {
                if (nLeaveKm == 0)
                {
                    bChk = false;
                    strMsg = "직퇴km 를 입력해주세요.";
                }
                else if (nLeaveKm <= nEnKm)
                {
                    bChk = false;
                    strMsg = "직퇴km는 종료km 보다 커야합니다.";
                }
                else
                {
                    bChk = true;
                }
            }

            return bChk;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "USERCARLISTBYCARID";
            string jobGb = "GET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarChkInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARCHECKINFOBYCARID";
            string jobGb = "GET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCarChk1(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARCHECKINFO1";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCarChk2(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARCHECKINFO2";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCarLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARLOGINSERT";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCarLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARLOGDELETE";
            string jobGb = "SET";

            bool bChk = false;

            string authId = HttpContext.Current.Session["AUTHID"].ToString();

            if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004"))
            {
                bChk = true;
            }

            foreach (string val in aData)
            {
                if (val.Length == 0)
                {
                    bChk = false;

                    break;
                }
                else
                {
                    bChk = true;
                }
            }

            if (bChk)
            {
                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARLOGINFOFORMOBILE";
            string jobGb = "GET";

            string orgcd = HttpContext.Current.Session["ORGCD"].ToString();
            string authid = HttpContext.Current.Session["AUTHID"].ToString();
            string sessionempno = HttpContext.Current.Session["EMPNO"].ToString();

            //aData[0] = $("#<%= txbStDt.ClientID %>").val();
            //aData[1] = $("#<%= txbEnDt.ClientID %>").val();
            //aData[2] = $("#<%= txbKtsEmpNo.ClientID %>").val();
            //aData[3] = $("#<%= txbEmpNm.ClientID %>").val();
            //aData[4] = $("#<%= txbCarNo.ClientID %>").val();

            aData.Add(orgcd); //파라미터 추가 시
            aData.Add(authid); //파라미터 추가 시
            aData.Add(sessionempno); //파라미터 추가 시
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            DataSet dataSet = new DataSet();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                dataSet = dbTransaction.GetDataSet();

                result = Utility.DataSetToJson(dataSet);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetCarLoginfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARLOGINFOUPDATE";
            string jobGb = "SET";

            //aData[0] = rowData.LOGDATE;
            //aData[1] = rowData.CARID;
            //aData[2] = rowData.CARNO;
            //aData[3] = rowData.EMPNO;
            //aData[4] = rowData.STKM;
            //aData[5] = rowData.ENKM;
            //aData[6] = rowData.LEAVEKM;
            //aData[7] = rowData.FUELAMOUNT;
            //aData[8] = rowData.REASON;
            //aData[9] = rowData.ETC;
            //aData[10] = rowData.LEAVEYN;
            //aData[11] = rowData.MODIYN;
            //aData[12] = rowData.LASTLOGDATE;

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            bool bChk = false;
            string strMsg = "";

            string strLeaveYn = aData[10];
            bool bModify = aData[11] == "Y" ? true : false;
            bool bDate = aData[0] == aData[12] ? true : false;

            int stkm = 0;
            int enkm = 0;
            int leavekm = 0;
            int fuels = 0;

            int.TryParse(aData[4], out stkm);
            int.TryParse(aData[5], out enkm);
            int.TryParse(aData[6], out leavekm);
            int.TryParse(aData[7], out fuels);

            if (strLeaveYn == "Y")
            {
                bChk = ValidationChk(stkm, enkm, leavekm, strLeaveYn, out strMsg);
            }
            else if (strLeaveYn == "R")
            {
                if (aData[3] == empNo)
                {
                    bChk = ValidationChk(stkm, enkm, leavekm, "", out strMsg);
                }
            }
            else if (strLeaveYn == "C")
            {
                if (aData[3] == empNo && bDate)
                {
                    bChk = ValidationChk(stkm, enkm, leavekm, "", out strMsg);
                }
            }
            else if (bModify)
            {
                bChk = ValidationChk(stkm, enkm, leavekm, "", out strMsg);
            }

            if (bChk)
            {
                aData[7] = fuels.ToString();

                aData.RemoveAt(12);
                aData.RemoveAt(11);

                aData.Add(empNo); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                if (strMsg.Length == 0)
                {
                    strMsg = "잘못된 접근입니다.";
                }

                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", strMsg, "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetLeaveConfirm(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARLOGLEAVECONFIRM";
            string jobGb = "SET";

            bool bChk = false;

            //aData[0] = rowData.LOGDATE;
            //aData[1] = rowData.CARID;
            //aData[2] = rowData.CARNO;
            //aData[3] = ctype;
            //aData[4] = rowData.LEAVEYN;

            bool bConfirm = aData[4] == "R" ? true : false;
            string authId = HttpContext.Current.Session["AUTHID"].ToString();

            if (bConfirm && (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004")))
            {
                bChk = true;
            }

            if (bChk)
            {
                aData.RemoveAt(4);

                string empNo = HttpContext.Current.Session["EMPNO"].ToString();
                aData.Add(empNo); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCarLoginfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARLOGINFODELETE";
            string jobGb = "SET";

            bool bChk = false;

            bool bDelete = aData[3] == "Y" ? true : false;
            bool bDate = aData[0] == aData[4] ? true : false;
            string authId = HttpContext.Current.Session["AUTHID"].ToString();

            if (bDelete && bDate && (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003")))
            {
                bChk = true;
            }

            if (bChk)
            {
                aData.RemoveAt(4);
                aData.RemoveAt(3);

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetLeaveConfirm_All(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARLOGLEAVECONFIRMFORMOBILE_ALL";
            string jobGb = "SET";

            string orgcd = HttpContext.Current.Session["ORGCD"].ToString();


            string authid = HttpContext.Current.Session["AUTHID"].ToString();
            string sessionempno = HttpContext.Current.Session["EMPNO"].ToString();

            if (authid.Equals("AUTH0001") || authid.Equals("AUTH0002") || authid.Equals("AUTH0003") || authid.Equals("AUTH0004") || aData.Count != 5)
            {
                aData.Add(orgcd); //파라미터 추가 시
                aData.Add(sessionempno); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }
    }
}