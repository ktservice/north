﻿using System;
using System.Reflection;
using System.Data;
using System.Web.UI;
using System.Web.SessionState;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web;
using KTS.KTSSolution.BSL.Order;

namespace KTSSolutionWeb
{
    public partial class OrderAttachedImage : PageBase
    {
        #region Fields
        #endregion


        #region Event

        #region Page_load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string strOrderNo = Request.QueryString["ORDERNO"] == null ? string.Empty : Request.QueryString["ORDERNO"];

                    if (strOrderNo != "")
                    {
                        GetOrderImage(strOrderNo);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ssoAuth", "alert('잘못된 접근입니다.');fn_Close();", true);
                    }
                }
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region
        private void GetOrderImage(string strOrderNo)
        {
            DataSet dsFile = null;

            try
            {
                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    dsFile = mgmt.GetOrderFile(strOrderNo);
                }

                bool bClose = true;

                if (dsFile.Tables.Count > 0)
                {
                    DataTable dtImg = new DataTable();
                    dtImg.Columns.Add("FILENM");
                    dtImg.Columns.Add("SRC");

                    if (dsFile.Tables[0].Rows.Count > 0)
                    {
                        bClose = false;

                        for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                        {
                            if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                            {
                                DataRow dr = dtImg.NewRow();

                                string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];

                                dr["FILENM"] = dsFile.Tables[0].Rows[i]["FILENM"].ToString();
                                dr["SRC"] = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                dtImg.Rows.Add(dr);
                            }
                        }
                    }

                    rptImg.DataSource = dtImg;
                    rptImg.DataBind();

                    updPanel1.Update();

                }
                if (bClose)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ssoAuth", "alert('참조된 이미지가 없습니다.');fn_Close();", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region Event
        #endregion
    }
}