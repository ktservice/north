﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Odbc.aspx.cs" Inherits="KTSSolutionWeb.Odbc" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>       
				<span class="inpbox">
					<label>사번</label>
                    <asp:TextBox ID="txbEmpNo" name="txbEmpNo" runat="server" MaxLength="20" Width="239px" TabIndex="1"></asp:TextBox>
                </span>                
                <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                               <%-- <th>name</th>
                                <th>oc_emp_no</th>
                                <th>prov_type_cd</th>
                                <th>prov_type</th>
                                <th>prov_dt</th>
                                <th>pay_tot_amt</th>
                                <th>sub_tot_amt</th>
                                <th>real_prov_amt</th>
                                <th>tax_amt</th>
                                <th>tax_free</th>
                                <th>income_tax</th>
                                <th>res_tax</th>--%>
                                <th>dilig_id</th>
                                <th>div_no</th>
                                <th>emp_no</th>
                                <th>name</th>
                                <th>dilig_dt</th>
                                <th>dilig_nm</th>
                                <th>msg_txt</th>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("dilig_id") %></td>
                                            <td><%# Eval("div_no") %></td>
                                            <td><%# Eval("emp_no") %></td>
                                            <td><%# Eval("name") %></td>
                                            <td><%# Eval("dilig_dt") %></td>
                                            <td><%# Eval("dilig_nm") %></td>
                                            <td><%# Eval("msg_txt") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                    </Triggers>
                </asp:UpdatePanel>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
