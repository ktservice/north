﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Chart.aspx.cs" Inherits="KTSSolutionWeb.Sample.Chart" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script src='/Resource/js/chartjs/chart.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-datalabels.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/hammer.min.js' ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-zoom.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/utils.js' ></script>
    <script src='/Resource/js/chartjs/kts_chartjs.js' ></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/chartjs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_ChartInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "공기구 보유 현황.xlsx", { sheetName: "공기구 보유 현황" });
                });

                $(".containMove").click(function (e) {

                    if (this.title === '내리기') {
                        fn_LayerUp();
                    }
                    else {
                        fn_LayerDown();
                    }
                });

                $("#selChartType1").on("change", function () {
                    var val = $(this).val();

                    if (val != '') {
                        setChartType1(val, 'selChartDataSet', 'selChartType2', 'selChartFill');
                    }
                });

                $("#selChartType2").on("change", function () {
                    var val = $(this).val();

                    if (val != '') {
                        setChartType2(this.id, $("#selChartDataSet option:selected").val(), val, 'selChartFill');
                    }
                });

                $("#selChartDataLabel").on("change", function () {
                    setChartDataLabel($(this).val());
                });

                $("#selChartDataLabelBox").on("change", function () {
                    setChartDataLabelBox($(this).val());
                });

                $("#selChartAxesX").on("change", function () {
                    setChartAxesX($(this).val());
                });

                $("#selChartAxesY").on("change", function () {
                    setChartAxesY($(this).val());
                });

                $("#selChartFillAll").on("change", function () {
                    setChartFillAll($(this).val());
                });

                $("#selChartFill").on("change", function () {
                    var val = $(this).val();

                    if (val != '') {
                        setChartFill($("#selChartDataSet").val(), val);
                    }
                });

                $("#selChartZoomMode").on("change", function () {
                    setChartZoomMode($(this).val());
                });

                $("#chartZoomReset").on("click", function () {
                    setChartZoomReset();
                });

                $("#selChartGubun").on("change", function () {
                    fn_ChartGubunComboChange($(this).children("option:selected").text(), $(this).val());
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            obj = $('#selOutGb');
            obj.empty();
            obj.append("<option value='3'>지점별</option>");
            obj.append("<option value='2'>지사별</option>");
            obj.append("<option value='1'>본부별</option>");
        }

        var fn_ChartInit = function () {
            fn_ChartCombo();
            fn_ChartGubunCombo();
            fn_LayerInit();

            objAxesX = 'selChartAxesX'; //X축 콤보박스 ID값 설정
            objAxesY = 'selChartAxesY';//Y축 콤보박스 ID값 설정
        }

        var fn_ChartCombo = function (val) {
            CHARTDATAGUBUN = ["선로시험기", "속도측정기", "복합형광파워미터", "라쳇렌치", "전동드릴", "위성측정기"];
            CHARTDATAGUBUNCD = "itemnm";
            setSelChartDataSetList('selChartDataSet');
            setSelChartType1('selChartType1');
            setSelChartType2('selChartType2');
            setSelChartFill('selChartFill');

            $("#selChartFill").hide();

            setSelChartDataLabel('selChartDataLabel');
            setSelChartDataLabel('selChartDataLabelBox');
            setSelChartDataLabel('selChartAxesX');
            setSelChartDataLabel('selChartAxesY');
            setSelChartDataLabel('selChartFillAll');
            setSelChartDataLabel('selChartZoomMode');
        }

        var fn_ChartGubunCombo = function () {
            $(".chartCombobox").css('display', 'none');
            for (var i = 9; i < ktsColumns.length; i++) {
                $("#selChartGubun").append("<option value='" + ktsColumns[i].field + "'>" + ktsColumns[i].title + "</option>");
            }
        }

        var fn_ChartGubunComboChange = function (nm, val) {
            var title = nm + ' Chart';

            fn_ChartDataSet($("#selChartType1").val(), val, orgNm);

            setChartTitle(title, '', '');
            chartObj.update();
        }

        var fn_LayerInit = function () {
            fn_LayerDown();
        }

        var fn_LayerUp = function () {
            $(".chartWrapper").show();
            $(".chartWrapper").addClass("opendChart");
            $(".containMove").attr("src", "/Resource/images/icon_arr_move_up.png");
            $(".containMove").attr("title", "올리기");
            $(".containMoveText").html('<span>차트</span>닫기');
        }

        var fn_LayerDown = function () {
            $(".chartWrapper").hide();
            $(".chartWrapper").removeClass("opendChart");
            $(".containMove").attr("src", "/Resource/images/icon_arr_move_down.png");
            $(".containMove").attr("title", "내리기");
            $(".containMoveText").html('<span>차트</span>보기');
        }

        var fn_CellPopup = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            var value = cell.getValue();
            value = priceToString(value);

            var cellReqStatusCd = cell.getRow().getData().REQUEST_STATUSCD;

            if ($("#selRequest").val() == '01') {
                if (cellReqStatusCd == '00' || cellReqStatusCd == 'FF') { //00 미신청 FF	승인완료
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='신청' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
                }
                else if (cellReqStatusCd == '01') { //01	신청
                    html = "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='취소' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
                }
            }
            else {
                html = "<input id='btnReg' type='button' onclick='fn_Hist(\"" + cellNo + "\");' value='보기' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            }

            return html;
        }

        var fn_CellStock = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '4';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellMaster = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '1';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellExfire = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '3';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellUseless = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'TOOLSTATBYHOLDING';
            var gubun = '2';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun);
            }

            return '';
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "right", title: "보유수량", field: "ITEMCNT", sorter: "number", formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기준수량", field: "STOCK_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellStock },
                { headerHozAlign: "center", hozAlign: "right", title: "지급수량", field: "MASTER_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellMaster },
                { headerHozAlign: "center", hozAlign: "right", title: "회수수량", field: "EXFIRE_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellExfire },
                { headerHozAlign: "center", hozAlign: "right", title: "불용수량", field: "USELESS_CNT", sorter: "number", formatter: fn_CellICnt, cellClick: fn_CellUseless }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var ajaxCallBack = function (json) {
        }

        var fn_PopupDetail = function (id, rptCd, gubun) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var orgCd1 = rowData.ORGCD1;
            var orgCd2 = rowData.ORGCD2;
            var orgCd3 = rowData.ORGCD3;
            var itemCd = rowData.ITEMCD;

            if (orgCd2 == undefined) {
                orgCd2 = '';
            }
            if (orgCd3 == undefined) {
                orgCd3 = '';
            }

            var form = "/Tool/ToolStatByDetailPopup";
            var target = "ToolStatByDetailPopup";
            var param = {
                pRPTCD: rptCd,
                pGUBUN: gubun,
                pORGCD1: orgCd1,
                pORGCD2: orgCd2,
                pORGCD3: orgCd3,
                pITEMCD: itemCd
            };

            var nWidth = 780;
            var nHeight = 780;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Tool";
                var target = "OrgTree_Tool";
                var param = {
                    pORGCD: orgcd,
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        var fn_ChartDataCombo = function (comboId, chartType, keyNm, refKeyNm) {
            CHARTDATAGUBUN = ["선로시험기", "속도측정기", "복합형광파워미터", "라쳇렌치", "전동드릴", "위성측정기"];

            $("#" + comboId + "").empty(); //초기화
            $("#" + comboId + "").append("<option value=''>데이터 선택</option>");

            for (var i = 0; i < CHARTGUBUN.length; i++) {
                chartDataSetNm = CHARTGUBUN[i];
                setSelChartDataSet(comboId, chartDataSetNm, i);
            }

            $("#" + comboId + " option:eq(0)").prop("selected", true);
        }

        var fn_ChartDataSet = function (chartType, datakey, labelKey) {
            var dataset;

            chartDataColorIdx = 0; //초기화
            chartConfig.data.datasets = []; //초기화

            for (var i = 0; i < CHARTDATAGUBUN.length; i++) {
                var checkVal = CHARTDATAGUBUN[i]; //선로시험기

                chartData = dataFilter(labelKey, CHARTDATAGUBUNCD.toUpperCase(), checkVal, datakey);
                dataset = setDataSet(chartType, checkVal, chartData, datakey);

                chartConfig.data.datasets.push(dataset);
            }
        }

        var orgNm = '';

        var fn_Chart = function (ajaxJsonData) {
            $(".chartCombobox").css('display', 'block');
            $("#selChartType1 option:eq(0)").prop("selected", true);
            $("#selChartDataLabelBox option:eq(1)").prop("selected", true);
            $("#selChartZoomMode option:eq(1)").prop("selected", true);

            setChartZoom();
            setChartOption();
            setChartConfig();
            setChartTitle('보유수량 Chart', '', '');

            parseJsonData(ajaxJsonData);

            orgNm = '';
            if ($("#selOutGb").val() == '1') orgNm = 'ORGNM1';
            else if ($("#selOutGb").val() == '2') orgNm = 'ORGNM2';
            else if ($("#selOutGb").val() == '3') orgNm = 'ORGNM3';

            //차트 라벨컬럼들 세팅
            setChartDataLabels(orgNm);

            //차트 데이터 세팅 입력
            fn_ChartDataSet($("#selChartType1").val(), $("#selChartGubun").val(), orgNm);

            //라벨 플로그인 추가
            chartPlugins.push(ChartDataLabels);
            chartPlugins.push(ChartZoom);
            
            chartObjNm = 'ktsChart'; //차트 div id 지정
            chartParentObjNm = 'chartContainer'; //차트 div 부모 class 지정
            chartComboObjNm = 'chartCombobox'; //차트 div combobox class 지정

            //차트크기
            setChartWidthHeight();

            ctx = document.getElementById(chartObjNm).getContext('2d');
            
            //차트 그리기
            chartObj = new Chart(ctx, chartConfig);

            setChartZoomMode('n'); //확대 pen기능은 활성화 먼저하고 false해야 작동함.
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var outGb = $('#selOutGb').val();

                if (outGb == '1') {
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                }
                else if (outGb == '2') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                }
                else if (outGb == '3') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                }
                
                //입력 파라미터
                var aData = [];
                aData[0] = outGb;
                aData[1] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox" style="margin-right:20px">
                    <label>출력구분</label>
                    <select id="selOutGb">
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:chartWrapper -->
        <div class="chartWrapper">
            <div class="chartCombobox" style="display: none;">
                <fieldset>
                    <div class="optionbox">
                        <select id="selChartGubun"></select>
                    </div>
                    <div class="optionbox">
                        <select id="selChartType1"></select>
                    </div>
                    <div class="optionbox">
                        <select id="selChartType2"></select>
                    </div>
                    <div class="optionbox">
                        <select id="selChartFill"></select>
                    </div>
                    <div class="optionbox">
                        <label>데이터표시</label>
                        <select id="selChartDataLabel"></select>
                    </div>
                    <div class="optionbox">
                        <label>데이터박스</label>
                        <select id="selChartDataLabelBox"></select>
                    </div>
                    <div class="optionbox">
                        <label>x선</label>
                        <select id="selChartAxesX"></select>
                    </div>
                    <div class="optionbox">
                        <label>y선</label>
                        <select id="selChartAxesY"></select>
                    </div>
                    <div class="optionbox">
                        <label>채우기</label>
                        <select id="selChartFillAll"></select>
                    </div>
                    <div class="optionbox">
                        <label>확대모드</label>
                        <select id="selChartZoomMode"></select>
                    </div>
                    <div class="btnRefresh-wrap">                                        
                        <input id="chartZoomReset" class="btnRefresh" type="button" value="초기화" />
                    </div>
                </fieldset>
            </div>
            <div class="chartContainer">
                <canvas id="ktsChart"></canvas>
            </div>
        </div>
        <!-- E:chartWrapper -->
        <div class="datalistmove">
            <label class="containMoveText"><span>차트</span>보기</label>
            <img class="containMove" src="/Resource/images/icon_arr_move_up.png" title="올리기" />
        </div>
        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>공기구 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                            <option value="ITEMNM">품목</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>