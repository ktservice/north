﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.ExcelImport;
using OfficeOpenXml;
using System.Linq;

namespace KTSSolutionWeb
{
    public partial class Odbc : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                Session["TESTSAMPLE"] = "Test";
                PageInit();
            }

        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PageInit()
        {
            try
            {
                MenuTree_Load();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpno = txbEmpNo.Text;

                System.Data.Odbc.OdbcParameter[] parameters = new System.Data.Odbc.OdbcParameter[1];

                //parameters[0] = new System.Data.Odbc.OdbcParameter("@working_month", System.Data.Odbc.OdbcType.VarChar, 6);
                //parameters[0].Value = "202301";

                //parameters[1] = new System.Data.Odbc.OdbcParameter("@ocCode", System.Data.Odbc.OdbcType.VarChar, 8);
                //parameters[1].Value = strEmpno;
                parameters[0] = new System.Data.Odbc.OdbcParameter("@OC_EMP_NO", System.Data.Odbc.OdbcType.VarChar, 13);
                parameters[0].Value = strEmpno;


                using (KTS.KTSSolution.Common.DAL.DBConn dbConn = new KTS.KTSSolution.Common.DAL.DBConn())
                {
                    //ds = dbConn.ExecuteDataSetByMsSql(CommandType.Text, "{CALL [KTS_NORTH].[dbo].[USP_KTS_M_PAY_KO806](?, ?)}", parameters);

                    ds = dbConn.ExecuteDataSetByMsSql(CommandType.Text, "{CALL [KTS_NORTH].[dbo].[USP_KTS_M_DILIG_KO806](?)}", parameters);
                    
                    //ds = dbConn.ExecuteDataSetByMsSql(CommandType.Text, "SELECT Code, Name, Mobile, Email FROM [KTS_NORTH].[dbo].[1069_ORG_USER] WHERE Code= '14233237' ");//, parameters);
                }

                rptResult.DataSource = ds.Tables[0];
                rptResult.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ok", "alert('조회가완료되었습니다.');", true);
            }
            catch (Exception ex)
            {
                //KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "err", "alert('오류가 발생했습니다." + ex.Message + "');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", ex.Message, true);
            }
        }
    }
}