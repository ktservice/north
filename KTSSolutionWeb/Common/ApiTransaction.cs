﻿/*
 *  Copyright ⓒ 2022 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using KTS.KTSSolution.Framework.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace KTSSolutionWeb
{
    public class ApiTransaction
    {
        public DataTable RestApiCallToDataTable(string strMethod, string requestParam, NameValueCollection data)
        {
            DataTable Api_dt = new DataTable();
            DataTable dt = new DataTable();

            string strApi = "https://api.van365.co.kr/api/v1/";
            string result = string.Empty;

            string resultSession = string.Empty;
            string rptCd = "VANAPIDOMAIN";
            string jobGb = "GET";

            List<string> aData = new List<string>();
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = dbTransaction.GetDataSet();
                Api_dt = ds.Tables[0];
            }

            if (Api_dt.Rows.Count > 0)
            {
                strApi = Api_dt.Rows[0]["DOMAIN"].ToString();

                string strUrl = strApi + strMethod;

                WebClient client = new WebClient();
                client.Headers.Add("clientSecret", "GxVTIxhaCm5KKzpdhglFmWPSHJDPUcsr4uJPMlysCBGw5OvatyGjYpTDqYuvRIPR"); //client key 추가
                client.QueryString.Add(data);

                JObject jsonResult = null;

                using (Stream stream = client.OpenRead(strUrl))
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("euc-kr")))
                    {
                        string s = reader.ReadToEnd();
                        JObject jsonObj = JObject.Parse(s);

                        string Success = jsonObj["success"].ToString().ToLower();

                        if (Success.Equals("true"))
                        {
                            dt = JsonConvert.DeserializeObject<DataTable>(jsonObj[requestParam].ToString());
                        }
                        else
                        {
                            result = "오류가 발생했습니다." + " code:" + jsonObj["code"].ToString() + " message:" + jsonObj["message"].ToString();
                            dt = Utility.DataTableError("03", result, "");
                        }

                        reader.Close();
                        stream.Close();
                    }
                }
            }
            else
            {
                result = "오류가 발생했습니다.";
                dt = Utility.DataTableError("03", result, "");
            }

            return dt;
        }

        public string RestApiCallToString(string strMethod, string requestParam, NameValueCollection data)
        {
            DataTable Api_dt = new DataTable();

            string strApi = "https://api.van365.co.kr/api/v1/";
            string result = string.Empty;

            string resultSession = string.Empty;
            string rptCd = "VANAPIDOMAIN";
            string jobGb = "GET";

            List<string> aData = new List<string>();
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = dbTransaction.GetDataSet();
                Api_dt = ds.Tables[0];
            }

            if (Api_dt.Rows.Count > 0)
            {
                strApi = Api_dt.Rows[0]["DOMAIN"].ToString();

                string strUrl = strApi + strMethod;

                WebClient client = new WebClient();

                client.Headers.Add("clientSecret", "GxVTIxhaCm5KKzpdhglFmWPSHJDPUcsr4uJPMlysCBGw5OvatyGjYpTDqYuvRIPR"); //client key 추가
                client.QueryString.Add(data);

                using (Stream stream = client.OpenRead(strUrl))
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("euc-kr")))
                    {
                        string s = reader.ReadToEnd();
                        JObject jsonObj = JObject.Parse(s);

                        string Success = jsonObj["success"].ToString().ToLower();

                        if (Success.Equals("true"))
                        {
                            result = jsonObj[requestParam].ToString();
                        }
                        else
                        {
                            result = "오류가 발생했습니다." + " code:" + jsonObj["code"].ToString() + " message:" + jsonObj["message"].ToString();
                        }

                        reader.Close();
                        stream.Close();
                    }
                }
            }
            else
            {
                result = "오류가 발생했습니다.";
            }

            return result;
        }

        public DataTable RestApiCallToInsert(string strMethod, NameValueCollection data)
        {
            DataTable dt = new DataTable();

            try
            {
                DataTable Api_dt = new DataTable();

                string strApi = "https://api.van365.co.kr/api/v1/";
                string result = string.Empty;

                string resultSession = string.Empty;
                string rptCd = "VANAPIDOMAIN";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    DataSet ds = dbTransaction.GetDataSet();
                    Api_dt = ds.Tables[0];
                }

                if (Api_dt.Rows.Count > 0)
                {
                    strApi = Api_dt.Rows[0]["DOMAIN"].ToString();

                    string strUrl = strApi + strMethod;

                    WebClient client = new WebClient();

                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Ssl3;
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls;
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11;
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;

                    client.Headers.Add("clientSecret", "GxVTIxhaCm5KKzpdhglFmWPSHJDPUcsr4uJPMlysCBGw5OvatyGjYpTDqYuvRIPR"); //client key 추가
                    client.QueryString.Add(data);

                    using (Stream stream = client.OpenRead(strUrl))
                    {
                        using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("euc-kr")))
                        {
                            string s = reader.ReadToEnd();
                            JObject jsonObj = JObject.Parse(s);

                            string Success = jsonObj["success"].ToString().ToLower();

                            if (Success.Equals("true"))
                            {
                                dt = Utility.DataTableError("00", "", "");
                            }
                            else
                            {
                                result = "데이터 연동 중 오류가 발생했습니다." + " code:" + jsonObj["code"].ToString() + " message:" + jsonObj["message"].ToString();
                                dt = Utility.DataTableError("03", result, "");
                            }

                            reader.Close();
                            stream.Close();
                        }
                    }
                }
                else
                {
                    result = "오류가 발생했습니다.";
                    dt = Utility.DataTableError("03", result, "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public DataTable RestApiCallToInsert(string strMethod, JObject data)
        {
            DataTable dt = new DataTable();

            try
            {
                DataTable Api_dt = new DataTable();

                string strApi = "https://api.van365.co.kr/api/v1/";
                string result = string.Empty;

                string resultSession = string.Empty;
                string rptCd = "VANAPIDOMAIN";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    DataSet ds = dbTransaction.GetDataSet();
                    Api_dt = ds.Tables[0];
                }

                if (Api_dt.Rows.Count > 0)
                {
                    strApi = Api_dt.Rows[0]["DOMAIN"].ToString();

                    string strUrl = strApi + strMethod;
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(strUrl);
                    httpWebRequest.Headers.Add("clientSecret", "GxVTIxhaCm5KKzpdhglFmWPSHJDPUcsr4uJPMlysCBGw5OvatyGjYpTDqYuvRIPR");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Timeout = 3000;

                    httpWebRequest.KeepAlive = false;

                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Ssl3;
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls;
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11;
                    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;

                    using (StreamWriter writer = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        writer.Write(data);
                        writer.Flush();
                        writer.Close();
                    }

                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    using (StreamReader reader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.GetEncoding("euc-kr")))
                    {
                        string s = reader.ReadToEnd();
                        JObject jsonObj = JObject.Parse(s);

                        string Success = jsonObj["success"].ToString().ToLower();

                        if (Success.Equals("true"))
                        {
                            dt = Utility.DataTableError("00", "", "");
                        }
                        else
                        {
                            result = "데이터 연동 중 오류가 발생했습니다." + " code:" + jsonObj["code"].ToString() + " message:" + jsonObj["message"].ToString();
                            dt = Utility.DataTableError("03", result, "");
                        }

                        reader.Close();
                    }
                }
                else
                {
                    result = "오류가 발생했습니다.";
                    dt = Utility.DataTableError("03", result, "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
    }
}