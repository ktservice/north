﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class SearchUser : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
        }

        private void GetUserInfo()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNm = this.txbEmpNm.Text;

                using (UserMgmt user = new UserMgmt())
                {
                    ds = user.GetOperUserList("", strEmpNm, "", "");
                }

                rptResult.DataSource = ds.Tables[0];
                rptResult.DataBind();

                for (int i = 0; i < rptResult.Items.Count; i++)
                {
                    HtmlInputCheckBox cbEmpNo = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbEmpNo");

                    AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
                    trigger.ControlID = cbEmpNo.UniqueID;
                    trigger.EventName = "serverchange";
                    updPanel1.Triggers.Add(trigger);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetUserInfo();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (rptResult.Items.Count > 0)
                {
                    for (int i = 0; i < rptResult.Items.Count; i++)
                    {
                        HtmlInputCheckBox cbEmpNo = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbEmpNo");

                        Label lblOrgCd = (Label)rptResult.Items[i].FindControl("lblOrgCd");
                        Label lblEmpNo = (Label)rptResult.Items[i].FindControl("lblEmpNo");
                        Label lblEmpNm = (Label)rptResult.Items[i].FindControl("lblEmpNm");

                        if (cbEmpNo.Checked)
                        {
                            string strOrgCd = lblOrgCd.Text;
                            string strEmpNo = lblEmpNo.Text;
                            string strEmpNm = lblEmpNm.Text;

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "SendParentsForm('" + strOrgCd + "', '" + strEmpNo + "', '" + strEmpNm + "');", true);

                            break;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "SendParentsForm('', '', '');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void cbEmpNo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                HtmlInputCheckBox cbEmpNo = (HtmlInputCheckBox)sender;

                if (cbEmpNo.Checked)
                {
                    for (int i = 0; i < rptResult.Items.Count; i++)
                    {
                        HtmlInputCheckBox chb = (HtmlInputCheckBox)rptResult.Items[i].FindControl("cbEmpNo");

                        if (!cbEmpNo.UniqueID.Equals(chb.UniqueID))
                        {
                            chb.Checked = false;
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}