﻿using System;
using System.Web;
using System.Collections.Generic;
using KTS.KTSSolution.Framework.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class SerchUserList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');opener.SessionTimeOut();window.close();", true);
            }
            else
            {
                GetOrgTree();
            }
        }

        private void GetOrgTree()
        {
            DataSet ds = null;
            string resultSession = string.Empty;
            string rptCd = "ORGTREEVIEW";
            string jobGb = "GET";

            List<string> aData = new List<string>();

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                ds = dbTransaction.GetDataSet();

                Dictionary<string, string> nodeValPath = new Dictionary<string, string>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    TreeNode node = new TreeNode(dr["ORGNM"].ToString());
                    node.Value = dr["ORGCD"].ToString();

                    if (dr["ORGCD"].ToString().Split('|').Length > 1)
                    {
                        node.NavigateUrl = "javascript:fn_GetMobileByOrgCd('" + dr["ORGCD"].ToString().Split('|')[1] + "');";
                    }
                    else
                    {
                        node.NavigateUrl = "javascript:fn_GetMobileByOrgCd('" + dr["ORGCD"].ToString() + "');";
                    }
                    node.ShowCheckBox = false;
                    node.Checked = false;
                    node.SelectAction = TreeNodeSelectAction.Select;

                    if (nodeValPath.ContainsKey(dr["UPPERORGCD"].ToString()))
                    {
                        string strValPath = nodeValPath[dr["UPPERORGCD"].ToString()];

                        TreeNode findNode = OrgTree.FindNode(strValPath);
                        findNode.ChildNodes.Add(node);
                        nodeValPath.Add(node.Value, node.ValuePath);
                    }
                    else
                    {
                        OrgTree.Nodes.Add(node);
                        nodeValPath.Add(node.Value, node.ValuePath);
                    }
                }

                OrgTree.CollapseAll();
                OrgTree.Nodes[0].Expand();
            }
            else //세션이 끊겼을 경우
            {
                //result = resultSession;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetUserListByOrg(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "USERMOBILENOBYORGCD";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetUserList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "USERMOBILENO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}