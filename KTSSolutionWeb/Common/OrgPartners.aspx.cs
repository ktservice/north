﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;

namespace KTSSolutionWeb
{
    public partial class OrgPartners : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //세션 체크
                if (this.Page.Session.Count <= 5)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
                }
                else
                {

                    string pCheckType = Request.Form["pCheckType"] == null ? "" : Request.Form["pCheckType"].ToString();
                    string pType = Request.Form["pType"] == null ? "" : Request.Form["pType"].ToString();
                    string pUpperOrgCd = Request.Form["pUpperOrgCd"] == null ? "" : Request.Form["pUpperOrgCd"].ToString();
                    string pOfficeCd = Request.Form["pOfficeCd"] == null ? "" : Request.Form["pOfficeCd"].ToString();
                    string pTeamCd = Request.Form["pTeamCd"] == null ? "" : Request.Form["pTeamCd"].ToString();
                    string pKtsYn = Request.Form["pKtsYn"] == null ? "" : Request.Form["pKtsYn"].ToString();

                    hdnCheckType.Value = pCheckType;
                    hdnType.Value = pType;
                    hdnUpperOrgCd.Value = pUpperOrgCd;
                    hdnOfficeCd.Value = pOfficeCd;
                    hdnTeamCd.Value = pTeamCd;
                    hdnKtsYn.Value = pKtsYn;
                    
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "ORGPARTNERINFO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}