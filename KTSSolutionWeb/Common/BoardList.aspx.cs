﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace KTSSolutionWeb
{
    public partial class BoardList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                btnWrite.Attributes.Add("onclick", "BoardWrite('', '', '')");

                if (!IsPostBack)
                {
                    GetBoardType();
                    //GetDataList();
                }
            }
        }

        private void GetBoardType()
        {
            try
            {
                DataSet ds = new DataSet();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "BOARDTYPELIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                hdfBoardType.Value = dt.Rows[i]["BOARDTYPE"].ToString();
                            }

                            string strInner = "<li><a id=\"tab" + dt.Rows[i]["BOARDTYPE"].ToString() + "\" href=\"#tab" + dt.Rows[i]["BOARDTYPE"].ToString() + "\">" + dt.Rows[i]["CATEGORY"].ToString() + "</a></li>";
                            tabnav.InnerHtml += strInner;
                        }
                    }
                }
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PageError", "alert('페이지 로드 중 오류가 발생했습니다.');window.close();", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strBoardType = Request.Form[hdfBoardType.UniqueID] == null ? "1" : Request.Form[hdfBoardType.UniqueID].ToString();
                string strTitle = Request.Form[txbTitle.UniqueID] == null ? "" : Request.Form[txbTitle.UniqueID].ToString();
                string strEmpnm = Request.Form[txbEmpNm.UniqueID] == null ? "" : Request.Form[txbEmpNm.UniqueID].ToString();

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetBoardList("", strBoardType, strTitle, strEmpnm);
                }

                paging.PageNumber = 0;
                paging.PageSize = 10;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Label lblBoardID = (Label)rptResult.Items[i].FindControl("lblBoardID");
                            Label lblBoardType = (Label)rptResult.Items[i].FindControl("lblBoardType");
                            Label lblWriterID = (Label)rptResult.Items[i].FindControl("lblWriterID");
                            HtmlAnchor aTitle = (HtmlAnchor)rptResult.Items[i].FindControl("aTitle");

                            aTitle.Attributes.Add("onclick", "BoardView('" + lblBoardID.Text + "', '" + lblBoardType.Text + "', '" + lblWriterID.Text + "')");
                        }
                    }

                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}