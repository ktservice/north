﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchOrgInventoryCd.aspx.cs" Inherits="KTSSolutionWeb.SearchOrgInventoryCd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>조직검색</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var chkOrgCd = '';
        var chkOrgNm = '';
        var gubun;
        var rtn;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
            },
            eventbind: function () {
                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnConfirm").click(function (e) {
                    SendParentsForm(chkOrgCd, chkOrgNm);
                });

                $("#txbOrgNm").keydown(function (e) {
                    if (e.keyCode == 13) {
                        fn_Search();
                        return false;
                    }
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_checkBox = function (cell, formatterParams, onRendered) { //plain text value
            cellNo = cell.getRow().getData().id;

            chkOrgCd = '';
            chkOrgNm = '';

            var checkbox = document.createElement("input");
            checkbox.type = 'checkbox';
            checkbox.name = 'chk';

            checkbox.addEventListener("change", function (e) {
                var rows = ktsGrid.getRows();
                rows.forEach(function (row) {
                    row.deselect();
                });

                cell.getRow().select();
                $('input[type="checkbox"][name="chk"]').prop('checked', false);
                $(this).prop('checked', true);

                if (rtn == "1") {
                    chkOrgCd = cell.getRow().getData().ORGCD;
                    chkOrgNm = cell.getRow().getData().ORGNM;
                }
                else if (rtn == "2") {
                    chkOrgCd = cell.getRow().getData().CD;
                    chkOrgNm = cell.getRow().getData().NM;
                }

            });

            return checkbox;
        };



        var fn_GridInit = function () {

            var Columns;

            gubun = $("#<%= hdnGubun.ClientID %>").val();
            rtn = $("#<%= hdnRtn.ClientID %>").val();

            if (gubun == "1") {
                Columns = [
                    { formatter: fn_checkBox, hozAlign: "center", headerSort: false, width: 30, frozen: true },//, titleFormatter: "rowSelection" : 전체 선택 기능 추가 시
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                    { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", width: 90 },
                    { headerHozAlign: "center", hozAlign: "center", title: "조직명", field: "ORGNM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "창고코드", field: "CD", sorter: "string", width: 90 },
                    { headerHozAlign: "center", hozAlign: "center", title: "창고명", field: "NM", sorter: "string", width: 200 }
                ];
            }
            else if (gubun == "2") {
                Columns = [
                    { formatter: fn_checkBox, hozAlign: "center", headerSort: false, width: 30 },//, titleFormatter: "rowSelection" : 전체 선택 기능 추가 시
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80},
                    { headerHozAlign: "center", hozAlign: "center", title: "창고코드", field: "CD", sorter: "string", width: 90 },
                    { headerHozAlign: "center", hozAlign: "center", title: "창고명", field: "NM", sorter: "string"}
                ];
            }

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid', 'fitColumns', true, null); // div의 grid id 값
        }

        var fn_Search = function () {
            var OrgNm = $("#txbOrgNm").val();

            if (OrgNm.length == 0) {
                ktsGridAlert("조직명을 입력해주세요.");
                return false;
            }
      
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = OrgNm;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm(orgcd, orgnm) {

            if (orgcd == '') {
                ktsGridAlert('체크 박스를 선택 후 진행하세요.');
            }
            else {
                opener.SetDispOrgCd(orgcd, orgnm);
                window.close();
            }
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>조직검색</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <!-- S:searchbox -->
                    <div class="searchboxPopUp">
		                <fieldset>
                            <span class="inpbox first">
                                <label>조직명/업체명</label>
                                <input type="text" id="txbOrgNm" maxlength="50" tabindex="1" style="width:120px;" placeholder="값 입력" />
                                <input type="hidden" id="hdnGubun" runat="server" />
                                <input type="hidden" id="hdnRtn" runat="server" />
                            </span>
                            <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                        </fieldset>
                    </div>
                    <!-- E:searchbox -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnConfirm" type="button" class="btn-green last" value="선택" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
