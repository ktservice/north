﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web.UI.HtmlControls;
using System.Text;

namespace KTSSolutionWeb.Common
{
    public partial class NoticeWrite : PageBase
    {
        private string NotiId
        {
            get
            {
                if (ViewState["NotiId"] != null)
                    return ViewState["NotiId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["NotiId"] = value;
            }
        }

        private string WriterId
        {
            get
            {
                if (ViewState["WriterId"] != null)
                    return ViewState["WriterId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["WriterId"] = value;
            }
        }

        private string FileSeq
        {
            get
            {
                if (ViewState["FileSeq"] != null)
                    return ViewState["FileSeq"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["FileSeq"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                initializeControls();

                if (!IsPostBack)
                {
                    GetDataList();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    NotiId = Request.Form["pNOTICEID"] == null ? "" : Request.Form["pNOTICEID"].ToString();

                    WriterId = Request.Form["pWRITERID"] == null ? "" : Request.Form["pWRITERID"].ToString();
                }

                string strAuthId = this.Page.Session["AUTHID"] == null ? "" : this.Page.Session["AUTHID"].ToString();
                string strEmpNo = this.Page.Session["EMPNO"] == null ? "" : this.Page.Session["EMPNO"].ToString();

                if (strAuthId.Equals("AUTH0001") || strEmpNo.Equals(WriterId))
                {
                    this.btnRegNoti.Click += new EventHandler(BtnRegNoti_ServerClick);

                    if (!NotiId.Equals(""))
                    {
                        this.btnRegNoti.Text = "수정";
                        this.btnDelNoti.Click += new EventHandler(BtnDelNoti_ServerClick);
                        this.btnDelNoti.Visible = true;
                    }
                    else
                    {
                        this.btnRegNoti.Text = "등록";
                        this.btnDelNoti.Visible = false;
                    }

                    this.btnRegNoti.Visible = true;

                    this.txbTitle.ReadOnly = false;
                    this.txbContents.ReadOnly = false;

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');window.close();", true);
                }

                updPanelBtn.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();
            DataSet dsFile = new DataSet();

            try
            {
                string strAuthId = this.Page.Session["AUTHID"] == null ? "" : this.Page.Session["AUTHID"].ToString();                

                if (NotiId != "")
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetNotiList(NotiId, "", "", "");
                        dsFile = Noti.GetNoticeFile(NotiId, "");
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.txbStDt.Text = ds.Tables[0].Rows[0]["STDT"].ToString(); ;
                            this.txbEnDt.Text = ds.Tables[0].Rows[0]["ENDT"].ToString(); ;

                            this.txbTitle.Text = ds.Tables[0].Rows[0]["TITLE"].ToString();
                            this.txbContents.Text = ds.Tables[0].Rows[0]["CONTENTS"].ToString();

                            this.chkNotiPopupYn.Checked = ds.Tables[0].Rows[0]["POPUPYN"].ToString() == "Y" ? true : false;
                        }
                    }

                    if (dsFile.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            rptFile.DataSource = dsFile.Tables[0];
                            rptFile.DataBind();
                        }
                    }
                }

                updPanel1.Update();
                updPanelFile.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (ds != null)
                    dsFile.Dispose();
            }
        }

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strTitle">string</param>
        /// <param name="strContents">string</param>
        /// <param name="strPopupYn">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        /// <param name="strMsg">string</param>
        /// <returns>bool</returns>
        private bool ValidationCheck(string strTitle, string strContents, string strPopupYn, string strStDt, string strEnDt, ref string strMsg)
        {
            bool bResult = true;

            if (strTitle.Length == 0)
            {
                strMsg = "제목을 입력해주세요.";
                bResult = false;
            }
            else if (strContents.Length == 0)
            {
                strMsg = "내용을 입력해주세요.";
                bResult = false;
            }
            else if (strPopupYn.Equals("Y"))
            {
                if (strStDt.Length == 0 || strEnDt.Length == 0)
                {
                    strMsg = "긴급공지 기간을 입력해주세요.";
                    bResult = false;
                }
            }

            return bResult;
        }
        #endregion

        #region SetFileUpload
        /// <summary>
        /// SetFileUpload
        /// </summary>
        private void SetFileUpload()
        {
            Stream stream = null;
            try
            {
                HttpFileCollection uploadFiles = Request.Files;
                HttpPostedFile postedFIle1 = Request.Files["fu1"];

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];

                    stream = postedFIle.InputStream;

                    if (stream.Length == 0)
                        continue;

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    //type체크 필요 

                    int nFileLen = Convert.ToInt32(stream.Length);

                    byte[] byteFile = new byte[nFileLen];

                    stream.Read(byteFile, 0, nFileLen);

                    using (Notice Noti = new Notice())
                    {
                        Noti.InsNoticeFile(NotiId, strFileNm, byteFile, strFileType, nFileLen);
                    }

                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('파일 업로드 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }
        }
        #endregion

        #region BtnRegNoti_ServerClick
        /// <summary>
        /// BtnRegNoti_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void BtnRegNoti_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMsg = "";

                string strTitle = Request.Form[txbTitle.UniqueID] == null ? "" : Utility.ReplaceChar(Request.Form[txbTitle.UniqueID].ToString());

                string strStDt = Request.Form[txbStDt.UniqueID] == null ? "" : Utility.ReplaceChar(Request.Form[txbStDt.UniqueID].ToString());
                string strEnDt = Request.Form[txbEnDt.UniqueID] == null ? "" : Utility.ReplaceChar(Request.Form[txbEnDt.UniqueID].ToString());

                string strPopupYn = this.chkNotiPopupYn.Checked ? "Y" : "N";

                if (!ValidationCheck(strTitle, Utility.ReplaceChar(txbContents.Text), strPopupYn, strStDt, strEnDt, ref strMsg))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationError", "alert('" + strMsg + "');", true);
                }
                else
                {
                    if (NotiId != "")
                    {
                        using (Notice Noti = new Notice())
                        {
                            Noti.UpdNotiList(NotiId, strTitle, Utility.ReplaceChar(txbContents.Text), strStDt, strEnDt, "Y", strPopupYn, Session["EMPNO"].ToString());
                        }
                    }
                    else
                    {
                        using (Notice Noti = new Notice())
                        {
                            ds = Noti.InsNotiList(strTitle, Utility.ReplaceChar(txbContents.Text), strStDt, strEnDt, "Y", strPopupYn, Session["EMPNO"].ToString());
                        }

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                NotiId = ds.Tables[0].Rows[0]["NOTICEID"].ToString();
                            }
                        }
                    }

                    if (NotiId.Length > 0)
                    {
                        SetFileUpload();
                    }

                    GetDataList();

                    string strCookie = "Notice_" + NotiId;

                    if (Request.Cookies[strCookie] != null)
                    {
                        Request.Cookies[strCookie].Value = null;
                        Request.Cookies[strCookie].Expires = new DateTime(1999, 1, 1);
                        Request.Cookies.Remove(strCookie);
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetNotice", "alert('공지사항 저장을 완료했습니다.');window.close();RefreshList();", true);

                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region BtnDelNoti_ServerClick
        /// <summary>
        /// BtnDelNoti_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnDelNoti_ServerClick(object sender, EventArgs e)
        {
            try
            {
                using (Notice Noti = new Notice())
                {
                    Noti.DelNotiList(NotiId);
                    Noti.DelNoticeFile(NotiId, "");
                }

                string strCookie = "Notice_" + NotiId;

                if (Request.Cookies[strCookie] != null)
                {
                    Request.Cookies[strCookie].Value = null;
                    Request.Cookies[strCookie].Expires = new DateTime(1999, 1, 1);
                    Request.Cookies.Remove(strCookie);
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "NotiDelete", "alert('공지사항 삭제를 완료 했습니다.');window.close();RefreshList();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnDownload_ServerClick
        /// <summary>
        /// BtnDownload_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void BtnDownload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlAnchor btnDownload = (HtmlAnchor)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                FileSeq = lblSeq.Text;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDownLoad", "FileDownload();", true);
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnFileDownload_ServerClick
        /// <summary>
        /// BtnFileDownload_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void BtnFileDownload_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                if (FileSeq.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다..');", true);
                }
                else
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetNoticeFile(NotiId, FileSeq);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                            byte[] bFile = new byte[nFileLen];
                            bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                            string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                            using (FileUtil fileUtil = new FileUtil())
                            {
                                fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                            }
                        }
                    }

                    FileSeq = "";
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region BtnDelete_ServerClick
        /// <summary>
        /// BtnDelete_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void BtnDelete_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlAnchor btnDownload = (HtmlAnchor)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                using (Notice Noti = new Notice())
                {
                   Noti.DelNoticeFile(NotiId, lblSeq.Text);
                }

                GetDataList();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "alert('파일 삭제를 완료 했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

    }
}