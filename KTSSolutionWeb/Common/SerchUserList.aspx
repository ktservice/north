﻿ <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SerchUserList.aspx.cs" Inherits="KTSSolutionWeb.SerchUserList" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사용자 조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{cursor:pointer;}

        .popcontents {display:flex;justify-content:center;align-items:center;}
        .inner-left {width:221px;height:365px;border:1px solid #0eaba7;padding:10px 0 10px 0;margin-right:10px;justify-content:center;align-items:center;border-radius:10px}
        .inner-list {width:730px;height:300px;margin-top:42px;margin-left:8px;border:1px solid #d1ded5;overflow-y:scroll;}
        .inner-list-empty {display:flex;width:100%;height:271px;background-color:#e5e5e5;justify-content:center;align-items:center;color:gray;font-weight:bold}
        .inner-list-label {float: left;font-size: 13px !important;padding:5px 0px 0 5px;padding-right:0px;margin-right:10px !important; margin-bottom:10px;font-weight:200 !important}
        .inner-right {width:745px;height:365px;border:1px solid #0eaba7;padding:10px 0 10px 0;justify-content:center;align-items:center;border-radius:10px}
        .inner-right > input[type="text"] {width:171px;height:30px;padding:0 5px 0 5px;margin-left:8px;margin-right:10px;margin-bottom:10px;float:left;}
        .inner-right > input[type="button"] {width:70px;min-width:70px;height:30px;padding:0 10px;line-height:30px;float:left;}
        .inner-table > tbody > tr >  td {font-size:14px;text-align:center;white-space:nowrap; text-overflow:ellipsis;overflow:hidden;}
        .inner-bottom {width:978px;height:200px;border:1px solid #0eaba7;padding:10px 0 10px 0;margin-right:10px;justify-content:center;align-items:center;border-radius:10px}
        .inner-bottom-list {width:98%;height:180px;border:1px solid #d1ded5;margin:0 auto;overflow-y:scroll;}
        .btn_wrap {display:flex;justify-content:center;align-items:center;margin-top:20px;}
        

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
                fn_SetControl();
            },
            eventbind: function () {
                $("#txbEmpInfo").on("keydown", function (e) {
                    if (e.key === "Enter") {
                        e.preventDefault();
                        fn_GetMobile();
                    }
                });

                $("#btnSearch").click(function (e) {
                    fn_GetMobile();
                });

                $("#btnAdd").click(function (e) {
                    fn_AddList();
                });

                $("#btnDel").click(function (e) {
                    fn_DelList();
                });

                $("#btnPick").click(function (e) {
                    fn_PickMobileNo();
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });
            }
        };

        var fn_Load = function () {
        };

        var fn_SetControl = function () {

            $("#chkAll").click(function () {
                if ($("#chkAll").is(":checked")) {
                    $("input[name=chklist]").prop("checked", true);
                }
                else {
                    $("input[name=chklist]").prop("checked", false);
                }
            });

            $("#chkPickAll").click(function () {
                if ($("#chkPickAll").is(":checked")) {
                    $("input[name=chklistPick]").prop("checked", true);
                }
                else {
                    $("input[name=chklistPick]").prop("checked", false);
                }
            });
        };

        var successCallBack = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                alert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                alert(errMsg);
                window.close();
            }
            else //정상
            {
                if (callGb != '') {
                    if (callGb == 'SearchOk') {
                        fn_SetEmpList(json);
                    }
                    if (callGb == 'SearchByListOk') {
                        fn_SetEmpList(json);
                    }
                    else if (callGb == 'SendOk') {
                        alert("메세지 전송 요청했습니다.");
                    }
                }
            }

            callGb = '';
        };

        var fn_GetMobileByOrgCd = function (orgcd) {

            if (orgcd.length == 0) {
                alert("잘못된 접근입니다.");
            }
            else {
                callGb = 'SearchByListOk'

                var aData = [];

                aData[0] = orgcd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "GetUserListByOrg");

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_GetMobile = function () {
            var empinfo = $("#txbEmpInfo").val();

            if (empinfo.length == 0) {
                alert("사번이나 사원명을 입력 후 조회해 주세요.");
            }
            else {
                callGb = 'SearchOk'

                var aData = [];

                aData[0] = empinfo;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "GetUserList");

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_SetEmpList = function (json) {
            $("#chkAll").prop("checked", false);

            var tbody = $("#tbody");

            tbody.empty();

            if (JSON.parse(json).length > 0) {
                $("#divEmpInfoEmpty").hide();

                $.each(JSON.parse(json), function (idx, item) {
                    var tr = document.createElement("tr");
                    tr.id = "tr" + item.NO;
                    tr.style.height = "25px";
                    tr.style.cursor = "default";

                    var tdChk = document.createElement("td");
                    tdChk.id = "tdChk" + item.NO;

                    var checkBox = document.createElement("input");
                    checkBox.type = "checkbox";
                    checkBox.name = "chklist";
                    checkBox.id = "chklist" + item.NO;
                    checkBox.value = item.EMPNO;
                    checkBox.style.cursor = "pointer";

                    tdChk.append(checkBox);

                    tr.append(tdChk);

                    var tdEmpNm = document.createElement("td");
                    tdEmpNm.id = "tdEmpNm" + item.NO;
                    tdEmpNm.innerText = item.EMPNM;
                    tdEmpNm.style.maxWidth = "110px";

                    tr.append(tdEmpNm);

                    var tdLv = document.createElement("td");
                    tdLv.id = "tdLv" + item.NO;
                    tdLv.innerText = item.LEVELNM;
                    tdLv.style.maxWidth = "100px";
                    tdLv.title = item.LEVELNM;

                    tr.append(tdLv);

                    var tdOrgNm = document.createElement("td");
                    tdOrgNm.id = "tdOrgNm" + item.NO;
                    tdOrgNm.innerText = item.ORGNM;
                    tdOrgNm.style.maxWidth = "200px";
                    tdOrgNm.title = item.ORGNM;

                    tr.append(tdOrgNm);

                    var tdMobile = document.createElement("td");
                    tdMobile.id = "tdMobile" + item.NO;
                    tdMobile.innerText = item.MOBILE;
                    tdMobile.style.maxWidth = "140px";

                    tr.append(tdMobile);

                    var tdEmail = document.createElement("td");
                    tdEmail.id = "tdEmail" + item.NO;
                    tdEmail.innerText = item.EMAIL;
                    tdEmail.style.maxWidth = "140px";
                    tdEmail.title = item.EMAIL;

                    tr.append(tdEmail);

                    tbody.append(tr);
                });

                $("input[name=chklist]").click(function () {
                    var total = $("input[name=chklist]").length;
                    var checked = $("input[name=chklist]:checked").length;

                    if (total != checked) {
                        $("#chkAll").prop("checked", false);
                    }
                    else {
                        $("#chkAll").prop("checked", true);
                    }
                });
            }
            else {
                $("#divEmpInfoEmpty").show();
            }
        };

        var fn_AddList = function () {
            var tbodyPick = $("#tbodyPick");

            if ($("input[name='chklistPick']").length > 0) {
                var idx = Math.max.apply(null, $("input[name='chklistPick']").map(function () {
                    return parseInt($(this).attr("id").replace("chklistPick", ""), 10) || 0;
                }).get()) + 1;
            } else {
                var idx = 0;
            }

            $.each($("input[name='chklist']:checked"), function () {
                var id = $(this).attr("id").replaceAll("chklist", "");

                var tr = document.createElement("tr");
                tr.id = "trPick" + idx;
                tr.style.height = "25px";
                tr.style.cursor = "default";

                var tdChk = document.createElement("td");
                tdChk.id = "tdChkPick" + idx;

                var EmpNo = $("#chklist" + id).val();

                var checkBox = document.createElement("input");
                checkBox.type = "checkbox";
                checkBox.name = "chklistPick";
                checkBox.id = "chklistPick" + idx;
                checkBox.value = EmpNo;
                checkBox.style.cursor = "pointer";

                tdChk.append(checkBox);

                tr.append(tdChk);

                var EmpNm = $("#tdEmpNm" + id).text();

                var tdEmpNm = document.createElement("td");
                tdEmpNm.id = "tdChkEmpNm" + idx;
                tdEmpNm.innerText = EmpNm;
                tdEmpNm.style.maxWidth = "110px";

                tr.append(tdEmpNm);

                var LvNm = $("#tdLv" + id).text();

                var tdLv = document.createElement("td");
                tdLv.id = "tdChkLv" + idx;
                tdLv.innerText = LvNm;
                tdLv.style.maxWidth = "130px";

                tr.append(tdLv);

                var OrgNm = $("#tdOrgNm" + id).text();

                var tdOrgNm = document.createElement("td");
                tdOrgNm.id = "tdChkOrgNm" + idx;
                tdOrgNm.innerText = OrgNm;
                tdOrgNm.style.maxWidth = "310px";

                tr.append(tdOrgNm);

                var Mobile = $("#tdMobile" + id).text();
                var tdMobile = document.createElement("td");
                tdMobile.id = "tdChkMobile" + idx;
                tdMobile.innerText = Mobile;
                tdMobile.style.maxWidth = "150px";

                tr.append(tdMobile);

                var Email = $("#tdEmail" + id).text();
                var tdEmail = document.createElement("td");
                tdEmail.id = "tdChkEmail" + idx;
                tdEmail.innerText = Email;
                tdEmail.style.maxWidth = "240px";

                tr.append(tdEmail);

                tbodyPick.append(tr);

                $(this).prop("checked", false);
                $("#tr" + id).remove();
                idx++;
            });

            $("input[name=chklistPick]").click(function () {
                var total = $("input[name=chklistPick]").length;
                var checked = $("input[name=chklistPick]:checked").length;

                if (total != checked) {
                    $("#chkPickAll").prop("checked", false);
                }
                else {
                    $("#chkPickAll").prop("checked", true);
                }
            });

            $("#chkAll").prop("checked", false);
        };

        var fn_DelList = function () {

            $.each($("input[name='chklistPick']:checked"), function () {
                var id = $(this).attr("id").replaceAll("chklistPick", "");
                $("#trPick" + id).remove();
            });

            $("#chkPickAll").prop("checked", false);
        };

        var fn_PickMobileNo = function () {
            var list = $("input[name='chklistPick']:checked").map(function () {

                var id = $(this).attr("id").replace("chklistPick", "");

                var empnm = $("#tdChkEmpNm" + id).text();
                var mobile = $("#tdChkMobile" + id).text();

                return empnm + "^" + mobile;
            }).get();

            if (list.length > 0) {
                if (window.opener && typeof window.opener.fn_SetList === "function") {
                    window.opener.fn_SetList(list);
                    alert("선택한 연락처를 목록에 추가 했습니다.");
                    window.close();
                } else {
                    alert("MMS 발송팝업이 닫혔습니다. 다시 시도해주세요");
                }
            } else {
                alert("선택한 연락처가 없습니다.");
            }
        };

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <!-- S:windowpop-wrap -->
        <div id="windowpop-wrap">
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-notice-write" style="margin-bottom:15px">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title">
                        <strong>사용자 조회</strong>
                    </div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->            
            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <div class="inner-left">
                        <!-- S:organization-tree -->
                        <div class="organization-tree" style="height:100%">
                            <div style="height:100%;margin-bottom:15px;overflow-y:auto;">
                                <asp:TreeView ID="OrgTree" runat="server" Font-Size="14px" EnableClientScript="true" ShowLines="true" ExpandImageUrl="/Resource/images/icon_plus_03.png" CollapseImageUrl="/Resource/images/icon_minus_02.png">
                                </asp:TreeView>
                            </div>
                        </div>
                        <!-- E:organization-tree -->
                    </div>
                    <div class="inner-right">
                        <input id="txbEmpInfo" type="text" maxlength="50" />
                        <input id="btnSearch" type="button" class="btn-green" value="조회" />
                        <div id="divEmpList" class="inner-list">
                            <div id="divEmpInfo">
                                <table class="inner-table">
                                    <colgroup>
                                        <col style="width:20px" />
                                        <col style="width:110px" />
                                        <col style="width:100px" />
                                        <col style="width:200px" />
                                        <col style="width:140px" />
                                        <col style="width:140px" />
                                    </colgroup>
                                    <thead>
                                        <tr style="height:25px;border-top:2px solid;border-bottom:1px solid darkgray;">
                                            <th id="thChk"><input type="checkbox" id="chkAll" /></th>
                                            <th>성명</th>
                                            <th>직책/호칭</th>
                                            <th>소속</th>
                                            <th>연락처</th>
                                            <th>e-mail</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    </tbody>
                                </table>
                            </div>
                            <div id="divEmpInfoEmpty" class="inner-list-empty">
                                조회된 정보가 없습니다.
                            </div>
                        </div>
                    </div>
                </div>
                <!-- E:popcontents -->
                <!-- S:popcontents -->
                <div class="popcontents" style="display:block;padding:0 10px 0 10px" >
                    <div class="btn_wrap">
                        <input id="btnAdd" type="button" value="추가" class="btn-save" style="height:23px;line-height:0px;font-size:14px;margin-right:10px;padding-right:2px;" />
                        <input id="btnDel" type="button" value="삭제" class="btn-del" style="height:23px;line-height:0px;font-size:14px;padding-right:2px;" />
                    </div>
                    <div style="width:100%">
                        선택된 리스트
                    </div>
                    <div class="inner-bottom">
                        <div class="inner-bottom-list">
                            <table class="inner-table" style="margin:0 auto;">
                                <colgroup>
                                    <col style="width:20px" />
                                    <col style="width:110px" />
                                    <col style="width:130px" />
                                    <col style="width:310px" />
                                    <col style="width:150px" />
                                    <col style="width:240px" />
                                </colgroup>
                                <thead>
                                    <tr style="height:25px;border-top:2px solid;border-bottom:1px solid darkgray;">
                                        <th id="thPickChk"><input type="checkbox" id="chkPickAll" /></th>
                                        <th>성명</th>
                                        <th>직책/호칭</th>
                                        <th>소속</th>
                                        <th>연락처</th>
                                        <th>e-mail</th>
                                    </tr>
                                </thead>
                                <tbody id="tbodyPick">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->       
            
            <div class="btn_wrap">
                <input id="btnPick" type="button" class="btn-green" style="margin-right:10px" value="선택" />
                <input id="btnClose" type="button" class="btn-gray" value="닫기" />
            </div>
        </div>
        <!-- E:windowpop-wrap -->
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>