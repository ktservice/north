﻿using System;
using System.Web;
using System.Collections.Generic;
using KTS.KTSSolution.Framework.Web;
using System.Web.Services;
using System.Web.Script.Services;

namespace KTSSolutionWeb
{
    public partial class MMSSend : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPhoneNo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "USERMOBILENO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetSendMMS(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SENDMMS";
            string jobGb = "SET";

            DbTransaction dbTransaction = null;

            string[] strPhoneNo = aData[0].ToString().Split('|');
            string strMessage = aData[1].ToString();

            string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            for (int i = 0; i < strPhoneNo.Length; i++)
            {
                List<string> aList = new List<string>();

                aList.Add(strPhoneNo[i]);
                aList.Add(strMessage);
                aList.Add(strEmpNo);

                dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }

            return result;
        }
    }
}