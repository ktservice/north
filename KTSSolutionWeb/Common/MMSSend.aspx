﻿ <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMSSend.aspx.cs" Inherits="KTSSolutionWeb.MMSSend" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>MMS 전송</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{cursor:pointer;}

        .popcontents {display:flex;justify-content:center;align-items:center;}
        .inner-left {width:291px;height:365px;border:1px solid #0eaba7;padding:10px 0 10px 0;margin-right:10px;justify-content:center;align-items:center;border-radius:10px}
        .inner-left > input[type="text"] {width:171px;height:30px;padding:0 5px 0 5px;margin-left:8px;margin-right:10px;margin-bottom:10px;float:left;}
        .inner-left > input[type="button"] {width:30px;min-width:30px;height:30px;padding:2px 11px;line-height:30px;float:left;font-size:20px;}
        .inner-left > .btn-Org {width:50px !important;min-width:50px !important;float:left;text-align:center; padding:0 8px !important;font-size:13px !important;background-color:#cfcfcf;border:0;border-radius:3px;margin-left:10px;}
        .inner-list {width:273px;height:300px;margin-top:42px;margin-left:8px;border:1px solid #d1ded5;overflow-y:scroll;}
        .inner-list-btnDel {width:20px !important;min-width:20px !important;height:20px !important;margin-top:3px;padding:0 !important;font-size:10px !important;line-height:0px !important;}
        .inner-list-empty {display:flex;width:100%;height:100%;background-color:#e5e5e5;justify-content:center;align-items:center;color:gray;font-weight:bold}
        .inner-list-txb {height:25px !important;float: left;font-size: 13px !important;padding:0 5px !important;margin-right:10px !important; margin-bottom:10px;font-weight:200 !important;border:0 !important;cursor:default;}
        .inner-right {width:280px;height:365px;display:grid;border:1px solid #0eaba7;padding:10px 0 10px 0;justify-content:center;align-items:center;border-radius:10px}
        .inner-right > input {width:261px;height:30px;padding:0 5px 0 5px;margin-bottom:10px;}
        .inner-right > textarea {width:261px;height:342px;padding:5px 5px 5px 5px;margin:0 auto;overflow-y:scroll;border:1px solid #d1ded5;}
        .btn_wrap {display:flex;justify-content:center;align-items:center;margin-top:20px;}        
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
                //fn_SetControl();
            },
            eventbind: function () {
                $("#txbEmpInfo").keypress(function (e) {
                    if (e.keyCode == 13) {
                        fn_AddMoblieNo();
                    }
                });

                $("#btnAdd").click(function (e) {
                    fn_AddMoblieNo();
                });

                $("#btnOrg").click(function (e) {
                    var nWidth = 1000;
                    var nHeight = 790;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Common/SerchUserList", null);

                    Popupform.target = "SerchUserList";
                    var win = window.open("", "SerchUserList", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                });

                $("#btnSend").click(function (e) {
                    fn_SendMMS();
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });
            }
        };

        var fn_Load = function () {
        };

        var SessionTimeOut = function () {
            opener.SessionTImeOut()
            window.close();
        };

        var fn_OnlyPhoneNo = function (obj) {
            var phoneNo = obj.value.replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, '');

            $("#txbPhoneNo").val(phoneNo);

            if (event.keyCode != 8) {
                if (event.keyCode >= 48 && event.keyCode <= 57) {
                    if (phoneNo.length == 3 || phoneNo.length == 8) {
                        $("#txbPhoneNo").val(phoneNo + '-');
                    }
                }
            }
            else {
                if (phoneNo.substring(phoneNo.length - 1) == "-") {
                    $("#txbPhoneNo").val(phoneNo.substring(0, phoneNo.length - 1));
                }
            }
        };

        var successCallBack = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                alert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                alert(errMsg);
                window.close();
            }
            else //정상
            {
                if (callGb != '') {
                    if (callGb == 'SearchOk') {
                        fn_SetEmpList(json);
                    }
                    else if (callGb == 'SendOk') {
                        alert("메세지 전송 요청했습니다.");
                    }
                }
            }

            callGb = '';
        };

        var fn_AddMoblieNo = function () {
            var divEmpInfo = $("#divEmpInfo");
            var listCnt = $("#hdfCnt").val();
            var phoneNo = $("#txbPhoneNo").val();

            if (phoneNo != "") {
                var divParent = document.createElement("div");

                divParent.id = "divEmpList" + listCnt;
                divParent.style.border = "none";
                divParent.style.backgroundColor = "white";
                divParent.style.width = "100%";
                divParent.style.height = "30px";
                divParent.style.paddingTop = "2px";
                divParent.style.cursor = "default";

                var p = document.createElement("p");

                p.className = "inpbox";

                var txbEmpNm = document.createElement("input");

                txbEmpNm.type = "text";
                txbEmpNm.readOnly = true;
                txbEmpNm.id = "txbEmpNm" + listCnt;
                txbEmpNm.value = "수동입력";
                txbEmpNm.className = "inner-list-txb";
                txbEmpNm.style.width = "70px";
                txbEmpNm.style.marginLeft = "10px";
                txbEmpNm.name = "txbEmpNm";

                p.append(txbEmpNm);

                var txbMobile = document.createElement("input");


                txbMobile.type = "text";
                txbMobile.readOnly = true;
                txbMobile.id = "txbMobile" + listCnt;
                txbMobile.value = phoneNo;
                txbMobile.className = "inner-list-txb";
                txbMobile.style.width = "120px";
                txbMobile.name = "txbMobile";

                p.append(txbMobile);

                //삭제버튼 추가
                var inputDel = document.createElement("input");

                inputDel.id = "btnDel" + listCnt;
                inputDel.type = "button";
                inputDel.value = "삭제";
                inputDel.className = "inner-list-btnDel";

                p.append(inputDel);

                divParent.append(p);

                divEmpInfo.append(divParent);

                $("#btnDel" + listCnt).click(function (e) {
                    var Cnt = $("#hdfCnt").val();
                    Cnt = Cnt - 1;

                    var btnNo = $(this).attr("id").replace("btnDel", "");

                    $("#hdfCnt").val(Cnt);
                    $("#divEmpList" + btnNo).remove();
                });

                listCnt++;

                $("#hdfCnt").val(listCnt);
                $("#divEmpInfoEmpty").hide();
                $("#txbPhoneNo").val('');
                $("#txbPhoneNo").focus();
            }
        };

        var fn_SetList = function (list) {
            var listCnt = $("#hdfCnt").val();
            var divEmpInfo = $("#divEmpInfo");

            if (list.length > 0) {
                $("#divEmpInfoEmpty").hide();

                for (var i = 0; i < list.length; i++) {
                    var data = [];

                    data = list[i].split('^');

                    if (data.length > 1) {
                        var empnm = data[0];
                        var phoneNo = data[1];

                        var divParent = document.createElement("div");

                        divParent.id = "divEmpList" + listCnt;
                        divParent.style.border = "none";
                        divParent.style.backgroundColor = "white";
                        divParent.style.width = "100%";
                        divParent.style.height = "30px";
                        divParent.style.paddingTop = "2px";
                        divParent.style.cursor = "default";

                        var p = document.createElement("p");

                        p.className = "inpbox";

                        var txbEmpNm = document.createElement("input");

                        txbEmpNm.type = "text";
                        txbEmpNm.readOnly = true;
                        txbEmpNm.id = "txbEmpNm" + listCnt;
                        txbEmpNm.value = empnm;
                        txbEmpNm.className = "inner-list-txb";
                        txbEmpNm.style.width = "70px";
                        txbEmpNm.style.marginLeft = "10px";
                        txbEmpNm.name = "txbEmpNm";

                        p.append(txbEmpNm);

                        var txbMobile = document.createElement("input");
                        txbMobile.type = "text";
                        txbMobile.readOnly = true;
                        txbMobile.id = "txbMobile" + listCnt;
                        txbMobile.value = phoneNo;
                        txbMobile.className = "inner-list-txb";
                        txbMobile.style.width = "120px";
                        txbMobile.name = "txbMobile";

                        p.append(txbMobile);

                        //삭제버튼 추가
                        var inputDel = document.createElement("input");

                        inputDel.id = "btnDel" + listCnt;
                        inputDel.type = "button";
                        inputDel.value = "삭제";
                        inputDel.className = "inner-list-btnDel";

                        p.append(inputDel);

                        divParent.append(p);

                        divEmpInfo.append(divParent);

                        $("#btnDel" + listCnt).click(function (e) {
                            var Cnt = $("#hdfCnt").val();
                            Cnt = Cnt - 1;

                            var btnNo = $(this).attr("id").replace("btnDel", "");

                            $("#hdfCnt").val(Cnt);
                            $("#divEmpList" + btnNo).remove();
                        });

                        listCnt++;
                        $("#hdfCnt").val(listCnt);
                    }
                }
            }
        };

        var fn_SendMMS = function () {
            var message = $("#txaMessage").val();
            var list = "";

            $.each($("input[name='txbEmpNm']"), function () {
                var id = $(this).attr("id").replaceAll("txbEmpNm", "");

                var empnm = $("#txbEmpNm" + id).val();
                var mobile = $("#txbMobile" + id).val();

                if (list.length > 0) {
                    list += "|";
                }

                list = list + empnm + "^" + mobile;
            });

            if (list.length == 0 || message.length == 0) {
                alert("휴대폰번호와 메세지를 입력해주세요.");
            }
            else {
                callGb = 'SendOk'

                var aData = [];

                aData[0] = list;
                aData[1] = message;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "SetSendMMS");

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <!-- S:windowpop-wrap -->
        <div id="windowpop-wrap">
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-notice-write" style="margin-bottom:15px">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title">
                        <strong>MMS 전송</strong>
                    </div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->            
            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <div class="inner-left">
                        <input id="txbPhoneNo" type="text" maxlength="14" onkeypress="return OnlyNumber()" onkeyup="fn_OnlyPhoneNo(this);" autocomplete="off"/>
                        <input id="btnAdd" type="button" class="btn-green" value="+"/>
                        <input id="btnOrg" type="button" class="btn-Org" value="조직도" />
                        <div id="divEmpList" class="inner-list">
                            <div id="divEmpInfo"></div>
                            <div id="divEmpInfoEmpty" class="inner-list-empty">
                                메세지를 전송할 번호가 없습니다.
                            </div>
                            <input id="hdfCnt" type="hidden" value="0" />
                        </div>
                    </div>
                    <div class="inner-right">                        
                        <textarea id="txaMessage" rows="60" maxlength="1990"></textarea>
                    </div>
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->       
            
            <div class="btn_wrap">
                <input id="btnSend" type="button" class="btn-green" style="margin-right:10px" value="전송" />
                <input id="btnClose" type="button" class="btn-gray" value="닫기" />
            </div>
        </div>
        <!-- E:windowpop-wrap -->
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
