﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoticeView.aspx.cs" Inherits="KTSSolutionWeb.Common.NoticeView" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>공지사항</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">   
        function NoticeWrite(noticeid, writerid) {
            var nWidth = 690;
            var nHeight = 820;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pNOTICEID: noticeid,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/NoticeWrite", param);

            Popupform.target = "NoticeWrite";
            var win = window.open("", "NoticeWrite", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            win.opener = self.opener; 
            Popupform.submit();

            win.focus();
        }

        function DelCheck() {
            if (confirm("현재 공지사항을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function FileDownload() {
            __doPostBack("<%=btnFileDownload.ClientID %>", "");
        }

        function RefreshList() {
            opener.RefreshList();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">

            <!-- S:pop-organization -->
            <div class="windowpop pop-mainlist-item"> <!-- 팝업별로 클래스가 달라집니다 -->

                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <div class="list-wrapper">
                            <div class="list-top">
                                <strong id="sTitle" runat="server"></strong>
                                <p id="pWriterNm" runat="server"></p>
                            </div>
                            <div class="list-item" style="margin-bottom:30px;overflow-y:auto;height:350px" id="divListItem" runat="server">
                                <asp:UpdatePanel ID="updPanelImg" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divImg" runat="server">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="item-text" id="divContent" runat="server">
                                </div>
                            </div>
                            <!-- //E:list-item -->

                            <!-- S:scrollbox -->
                            <div class="scrollbox" id="divFileList" runat="server" style="max-height:70px;overflow-y:auto;">
                                <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table>
                                            <colgroup>
                                                <col style="width:70px" />
                                                <col style="width:auto" />
                                            </colgroup>
                                            <tbody>
                                                <tr class="upload-field">
                                                    <th style="vertical-align:middle;">첨부파일</th>
                                                    <td>
                                                        <asp:Repeater ID="rptFile" runat="server">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                                                <p><a id="btnDownload" runat="server" onserverclick="BtnDownload_ServerClick" ><%# Eval("FILENM") %></a>&nbsp;</p>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <br />
                            <!-- E:scrollbox -->
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin-top:10px;height:40px;">
                            <asp:UpdatePanel ID="updPanelBtn" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button id="btnRegNoti" runat="server" style="margin-right:5px" class="btn-green" Text="수정" />
                                    <asp:Button id="btnDelNoti" runat="server" OnClientClick="return DelCheck();" style="margin-right:5px" class="btn-black" Text="삭제" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button id="btnFileDownload" OnClick="BtnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- //E:popcontents -->

                </div>
                <!-- //E:popupwrap -->
                
                <div style="position:absolute; left:10px; bottom:10px;">
                    <asp:UpdatePanel ID="updPanelChk" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:CheckBox ID="chkPopYn" runat="server" AutoPostBack="true" OnCheckedChanged="chkPopYn_CheckedChanged" Text="오늘 하루 자동 팝업을 띄우지 않습니다." Visible="false" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="chkPopYn" EventName="CheckedChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
