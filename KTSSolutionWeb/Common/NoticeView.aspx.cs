﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

namespace KTSSolutionWeb.Common
{
    public partial class NoticeView : PageBase
    {
        private string NotiId
        {
            get
            {
                if (ViewState["NotiId"] != null)
                    return ViewState["NotiId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["NotiId"] = value;
            }
        }

        private string WriterId
        {
            get
            {
                if (ViewState["WriterId"] != null)
                    return ViewState["WriterId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["WriterId"] = value;
            }
        }

        private string FileSeq
        {
            get
            {
                if (ViewState["FileSeq"] != null)
                    return ViewState["FileSeq"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["FileSeq"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            initializeControls();

            if (!IsPostBack)
            {
                GetDataList();
            }
        }

        private void initializeControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    NotiId = Request.Form["pNOTICEID"] == null ? "" : Request.Form["pNOTICEID"].ToString();

                    WriterId = Request.Form["pWRITERID"] == null ? "" : Request.Form["pWRITERID"].ToString();
                }

                string strAuthId = this.Page.Session["AUTHID"] == null ? "" : this.Page.Session["AUTHID"].ToString();
                string strEmpNo = this.Page.Session["EMPNO"] == null ? "" : this.Page.Session["EMPNO"].ToString();

                if ((strAuthId.Equals("AUTH0001") || strEmpNo.Equals(WriterId)) && Session.Count > 0)
                {
                    this.btnRegNoti.OnClientClick = "javascript:NoticeWrite('" + NotiId + "', '" + WriterId + "'); window.close();";
                    this.btnDelNoti.Click += new EventHandler(BtnDelNoti_ServerClick);

                    this.btnRegNoti.Visible = true;
                    this.btnDelNoti.Visible = true;
                }
                else
                {
                    this.btnRegNoti.Visible = false;
                    this.btnDelNoti.Visible = false;
                }

                updPanelBtn.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();
            DataSet dsFile = new DataSet();

            try
            {
                string strAuthId = this.Page.Session["AUTHID"] == null ? "" : this.Page.Session["AUTHID"].ToString();                

                if (NotiId != "")
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetNotiList(NotiId, "", "", "");
                        dsFile = Noti.GetNoticeFile(NotiId, "");
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.sTitle.InnerText = ds.Tables[0].Rows[0]["TITLE"].ToString();
                            this.pWriterNm.InnerHtml = ds.Tables[0].Rows[0]["EMPNM"].ToString() + "<span>" + ds.Tables[0].Rows[0]["WRITEDT"].ToString() + "</span>";
                            this.divContent.InnerHtml = "<p>" + ds.Tables[0].Rows[0]["CONTENTS"].ToString().Replace("\r\n", "<br/>") + "</p>";

                            this.chkPopYn.Visible = ds.Tables[0].Rows[0]["POPUPYN"].ToString() == "Y" ? true : false;

                            string strCookie = "Notice_" + NotiId;

                            bool chk = false;

                            if (Request.Cookies[strCookie] != null)
                            {
                                HttpCookie Cookie = Request.Cookies[strCookie];

                                chk = Cookie["POPUPYN"].ToString() == "Y" ? true : false;
                            }

                            chkPopYn.Checked = chk;

                            if (dsFile.Tables.Count > 0)
                            {
                                if (dsFile.Tables[0].Rows.Count > 0)
                                {

                                    rptFile.DataSource = dsFile.Tables[0];
                                    rptFile.DataBind();

                                    for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                    {
                                        if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                        {
                                            HtmlImage ImgNoti = new HtmlImage();

                                            string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                            byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                            ImgNoti.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                            divImg.Controls.Add(ImgNoti);
                                            divImg.Controls.Add(new LiteralControl("<br />"));
                                            divImg.Controls.Add(new LiteralControl(dsFile.Tables[0].Rows[i]["FILENM"].ToString().Split('.')[0]));
                                            divImg.Controls.Add(new LiteralControl("<br />"));
                                        }
                                    }

                                    if (this.Session.Count > 0)
                                    {
                                        divFileList.Visible = true;
                                    }
                                    else
                                    {
                                        divFileList.Visible = false;
                                    }
                                }
                                else
                                {
                                    divFileList.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "NotiRefrash", "window.close();RefreshList();", true);
                        }
                    }
                }

                updPanelImg.Update();

                updPanelFile.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (ds != null)
                    dsFile.Dispose();
            }
        }

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strTitle"></param>
        /// <param name="sbContents"></param>
        /// <param name="strMsg"></param>
        /// <returns></returns>
        private bool ValidationCheck(string strTitle, StringBuilder sbContents, ref string strMsg)
        {
            bool bResult = true;

            if (strTitle.Length == 0)
            {
                strMsg = "제목을 입력해주세요.";
                bResult = false;
            }
            else if (sbContents.Length == 0)
            {
                strMsg = "내용을 입력해주세요.";
                bResult = false;
            }

            return bResult;
        }
        #endregion

        #region BtnDelNoti_ServerClick
        /// <summary>
        /// BtnDelNoti_ServerClick
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void BtnDelNoti_ServerClick(object sender, EventArgs e)
        {
            try
            {
                using (Notice Noti = new Notice())
                {
                    Noti.DelNotiList(NotiId);
                    Noti.DelNoticeFile(NotiId, "");
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "NotiDelete", "alert('공지사항 삭제를 완료 했습니다.');window.close();RefreshList();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnDownload_ServerClick
        /// <summary>
        /// BtnDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnDownload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlAnchor btnDownload = (HtmlAnchor)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                FileSeq = lblSeq.Text;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDownLoad", "FileDownload();", true);
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnFileDownload_ServerClick
        /// <summary>
        /// BtnFileDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnFileDownload_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                if (FileSeq.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다..');window.close();", true);
                }
                else
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetNoticeFile(NotiId, FileSeq);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                            byte[] bFile = new byte[nFileLen];
                            bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                            string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                            using (FileUtil fileUtil = new FileUtil())
                            {
                                fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                            }
                        }
                    }

                    FileSeq = "";
                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        protected void chkPopYn_CheckedChanged(object sender, EventArgs e)
        {
            string strCookie = "Notice_" + NotiId;

            if (chkPopYn.Checked)
            {
                HttpCookie Cookie = null;

                if (Request.Cookies[strCookie] == null)
                {
                    Cookie = new HttpCookie(strCookie);
                }
                else
                {
                    Cookie = Request.Cookies[strCookie];
                }

                Cookie["POPUPYN"] = "Y";
                Cookie["DATE"] = DateTime.Now.ToString("yyyyMMdd");
                Cookie.Expires = DateTime.Now.AddDays(1);
                Cookie.HttpOnly = true;
                Response.Cookies.Add(Cookie);
            }
            else
            {
                if (Request.Cookies[strCookie] != null)
                {
                    Request.Cookies[strCookie].Value = null;
                    Request.Cookies[strCookie].Expires = new DateTime(1999, 1, 1);
                    Request.Cookies.Remove(strCookie);
                }
            }

            updPanelChk.Update();
        }
    }
}