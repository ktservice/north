﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchCorpPopup.aspx.cs" Inherits="KTSSolutionWeb.SearchCorpPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사업자 정보 조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#btnSelect").click(function (e) {
                    fn_Search();
                });
            }
        };

        var fn_GridInit = function () {

            var fn_choice = function (cell) {
                var rowData = cell.getRow().getData();
                var rowid = rowData.id;

                var input = "<input id='btnChoice' type='button' onclick='fn_CorpChoice(\"" + rowid + "\");' value='선택' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "BUTTONS", width: 90, formatter: fn_choice },
                { headerHozAlign: "center", hozAlign: "left", title: "사업자명", field: "CORPNM", sorter: "string", minWidth: 100, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "사업자등록번호", field: "CORPNO", sorter: "string", minWidth: 150, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "left", title: "주소", field: "ADDR", sorter: "string", minWidth: 100, maxWidth: 1500 },
                { headerHozAlign: "center", hozAlign: "left", title: "주소상세", field: "ADDRDTL", sorter: "string", minWidth: 100, maxWidth: 1500 },
                { headerHozAlign: "center", hozAlign: "left", title: "연락처", field: "TELNO", sorter: "string", minWidth: 100, maxWidth: 200 },
                { headerHozAlign: "center", hozAlign: "left", title: "휴대폰번호", field: "PHONENO", sorter: "string", minWidth: 100, maxWidth: 200 },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, false); // div의 grid id 값
            //ktsGrid.setHeight("30vh");
        }

        var fn_Load = function () {
            //fn_Search();
        }

        var fn_Search = function () {

            var OrderCd = $("#<%= hdfOrderCode.ClientID %>").val();
            var CorpNm = $("#txbCorpNm").val();

            callGb = "getOk";

            //입력 파라미터
            var aData = [];
            aData[0] = OrderCd;
            aData[1] = CorpNm;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, "GetData");

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);
        }

        var fn_CorpChoice = function (rowid) {
            var rows = ktsGrid.searchRows("id", "=", rowid);

            var rowData = rows[0].getData();
            var CorpNm = rowData.CORPNM;
            var CorpNo = rowData.CORPNO;
            var Addr = rowData.ADDR;
            var AddrDtl = rowData.ADDRDTL;
            var TelNo = rowData.TELNO;
            var PhoneNo = rowData.PHONENO;

            opener.SetCorpInfo(CorpNm, CorpNo, Addr, AddrDtl, TelNo, PhoneNo);

            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">사업자 정보 조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <!-- S:searchbox -->
                    <div class="searchboxPopUp">
		                <fieldset>
                            <span class="inpbox first">
                                <label>사업자명</label>
                                <input type="text" id="txbCorpNm" maxlength="150" tabindex="1" style="width:150px;" />
                                <asp:HiddenField ID="hdfOrderCode" runat="server" />
                            </span>
                            <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                        </fieldset>
                    </div>
                    <!-- E:searchbox -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
