﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class OrgTree_PassAuth : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }

        private string PASSTYPE
        {
            get
            {
                if (ViewState["PASSTYPE"] != null)
                    return ViewState["PASSTYPE"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["PASSTYPE"] = value;
            }
        }

        private string OPERATOR
        {
            get
            {
                if (ViewState["OPERATOR"] != null)
                    return ViewState["OPERATOR"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["OPERATOR"] = value;
            }
        }

        private string VALTYPE
        {
            get
            {
                if (ViewState["VALTYPE"] != null)
                    return ViewState["VALTYPE"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["VALTYPE"] = value;
            }
        }

        private DataSet Ds
        {
            get
            {
                if (ViewState["Ds"] != null)
                    return (DataSet)ViewState["Ds"];
                else
                    return null;
            }
            set
            {
                ViewState["Ds"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    SetTreeView();

                    OrgTree.Attributes.Add("onclick", "postBackObject();");
                }
            }
        }

        private void SetTreeView()
        {
            try
            {
                ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                PASSTYPE = Request.Form["pPASSTYPE"] == null ? "" : Request.Form["pPASSTYPE"].ToString();
                OPERATOR = Request.Form["pOPERATOR"] == null ? "" : Request.Form["pOPERATOR"].ToString();
                VALTYPE = Request.Form["pVALTYPE"] == null ? "" : Request.Form["pVALTYPE"].ToString();

                using (OrgCodeTree org = new OrgCodeTree())
                {
                    Ds = org.GetPassOrgList_All();
                }

                if (Ds.Tables.Count > 0)
                {
                    Dictionary<string, string> nodeValPath = new Dictionary<string, string>();

                    foreach (DataRow dr in Ds.Tables[0].Rows)
                    {
                        TreeNode node = new TreeNode(dr["ORGNM"].ToString());
                        node.Value = dr["ORGCD"].ToString();

                        node.ShowCheckBox = true;

                        if (nodeValPath.ContainsKey(dr["UPPERORGCD"].ToString()))
                        {
                            string strValPath = nodeValPath[dr["UPPERORGCD"].ToString()];

                            TreeNode findNode = OrgTree.FindNode(strValPath);
                            findNode.ChildNodes.Add(node);
                            nodeValPath.Add(node.Value, node.ValuePath);
                        }
                        else
                        {
                            OrgTree.Nodes.Add(node);
                            nodeValPath.Add(node.Value, node.ValuePath);
                        }
                    }

                    OrgTree.CollapseAll();

                    if (OrgTree.Nodes.Count > 0)
                    {
                        OrgTree.Nodes[0].Expand();

                        if (ORGCD.Length > 0)
                        {
                            string[] arrOrgCd = ORGCD.Split(',');

                            for (int i = 0; i < OrgTree.Nodes.Count; i++)
                            {
                                CheckNode(OrgTree.Nodes[i], arrOrgCd);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void CheckNode(TreeNode node, string[] arrOrgCd)
        {
            bool bNodeChk = false;

            foreach (string strOrg in arrOrgCd)
            {
                if (node.Value.Equals(strOrg))
                {
                    bNodeChk = true;
                    break;
                }
                else
                {
                    if (node.Parent != null)
                    {
                        bNodeChk = node.Parent.Checked;
                    }
                }
            }

            node.Checked = bNodeChk;

            if (bNodeChk)
            {
                if (node.Parent != null)
                {
                    if (!(bool)node.Parent.Expanded)
                    {
                        node.Parent.Expand();

                        if (node.Parent.Parent != null)
                        {
                            if (!(bool)node.Parent.Parent.Expanded)
                            {
                                node.Parent.Parent.Expand();
                            }
                        }
                    }
                }
            }
            if (node.ChildNodes.Count > 0)
            {
                foreach (TreeNode item in node.ChildNodes)
                {
                    CheckNode(item, arrOrgCd);
                }
            }
        }

        private void UnCheckNode(TreeNode node, TreeNode chkNode)
        {
            if (node != null)
            {
                foreach (TreeNode item in node.ChildNodes)
                {
                    if ((bool)item.ShowCheckBox)
                    {
                        if (item != chkNode)
                            item.Checked = false;
                    }

                    if (item.ChildNodes.Count > 0)
                    {
                        UnCheckNode(item, chkNode);
                    }
                }
            }
        }

        private void CheckUnCheckNode(TreeNode node, bool bCheck)
        {
            try
            {
                if (node != null)
                {
                    foreach (TreeNode item in node.ChildNodes)
                    {
                        if ((bool)item.ShowCheckBox)
                        {
                            item.Checked = bCheck;
                        }

                        if (item.ChildNodes.Count > 0)
                        {
                            CheckUnCheckNode(item, bCheck);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CheckUnCheckParentNode(TreeNode node)
        {
            try
            {
                TreeNode pNode = node.Parent;

                bool bCheck = true;

                foreach (TreeNode child in pNode.ChildNodes)
                {
                    if (!child.Checked)
                        bCheck = false;
                }

                pNode.Checked = bCheck;

                if (pNode.Parent != null)
                {
                    CheckUnCheckParentNode(pNode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void OrgTree_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            try
            {

                bool bCheck = e.Node.Checked;

                CheckUnCheckNode(e.Node, bCheck);

                if (e.Node.Parent != null)
                {
                    CheckUnCheckParentNode(e.Node);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnConfirm_ServerClick(object sender, EventArgs e)
        {
            try
            {
                TreeNodeCollection checkNodes = OrgTree.CheckedNodes;

                string strOrgCd = "";

                for (int i = 0; i < checkNodes.Count; i++)
                {
                    DataRow[] dr = Ds.Tables[0].Select(string.Format("ORGCD = '{0}'", checkNodes[i].Value));

                    string strNodeLv = "1";

                    if (dr.Length > 0)
                        strNodeLv = dr[0]["ORGLEVEL"].ToString();

                    if (strNodeLv.Equals("0"))
                    {
                        strOrgCd = checkNodes[i].Value;
                        break;
                    }
                    else if (strNodeLv.Equals("1"))
                    {
                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                    else if (strNodeLv.Equals("2"))
                    {
                        string[] strOrgLvList = strOrgCd.Split(',');
                        bool bUpperChk = false;

                        if (checkNodes[i].Parent != null)
                        {
                            for (int j = 0; j < strOrgLvList.Length; j++)
                            {
                                if (strOrgLvList[j].Equals(checkNodes[i].Parent.Value))
                                {
                                    bUpperChk = true;
                                    break;
                                }
                            }
                        }

                        if (bUpperChk)
                        {
                            continue;
                        }

                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                    else if (strNodeLv.Equals("3"))
                    {
                        string[] strOrgLvList = strOrgCd.Split(',');
                        bool bUpperChk = false;

                        if (checkNodes[i].Parent != null)
                        {
                            for (int j = 0; j < strOrgLvList.Length; j++)
                            {
                                if (strOrgLvList[j].Equals(checkNodes[i].Parent.Value))
                                {
                                    bUpperChk = true;
                                    break;
                                }
                            }

                            if (checkNodes[i].Parent.Parent != null)
                            {
                                for (int j = 0; j < strOrgLvList.Length; j++)
                                {
                                    if (strOrgLvList[j].Equals(checkNodes[i].Parent.Parent.Value))
                                    {
                                        bUpperChk = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (bUpperChk)
                        {
                            continue;
                        }

                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                    else
                    {
                        string[] strOrgLvList = strOrgCd.Split(',');
                        bool bUpperChk = false;

                        if (checkNodes[i].Parent != null)
                        {
                            for (int j = 0; j < strOrgLvList.Length; j++)
                            {
                                if (strOrgLvList[j].Equals(checkNodes[i].Parent.Value))
                                {
                                    bUpperChk = true;
                                    break;
                                }
                            }

                            if (checkNodes[i].Parent.Parent != null)
                            {
                                for (int j = 0; j < strOrgLvList.Length; j++)
                                {
                                    if (strOrgLvList[j].Equals(checkNodes[i].Parent.Parent.Value))
                                    {
                                        bUpperChk = true;
                                        break;
                                    }
                                }

                                if (checkNodes[i].Parent.Parent.Parent != null)
                                {
                                    for (int j = 0; j < strOrgLvList.Length; j++)
                                    {
                                        if (strOrgLvList[j].Equals(checkNodes[i].Parent.Parent.Parent.Value))
                                        {
                                            bUpperChk = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (bUpperChk)
                        {
                            continue;
                        }

                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += checkNodes[i].Value;
                    }
                }

                string strValType = VALTYPE;

                strOrgCd = PASSTYPE + "|" + OPERATOR + "|" + strOrgCd;

                this.hdfOrgCd.Value = strOrgCd;

                updPanel1.Update();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Confirm", "SendParentsForm('" + strValType + "');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}