﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BoardList.aspx.cs" Inherits="KTSSolutionWeb.BoardList" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>기술공유 게시판</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabnav li a{position:relative; display:block; margin-left:30px; color: #9a9fb2; font-size: 18px; line-height: 40px; text-decoration:none;}
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
            },
            eventbind: function () {
                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    var boardType = $(this).attr("id").replace("tab", "");

                    $("#<%= hdfBoardType.ClientID %>").val(boardType);
                    fn_Search();

                }).filter(':eq(0)').click();
            }
        };

        var fn_Search = function () {
            $("#<%= btnSelect.ClientID %>").click();
        };

        function BoardWrite(boardid, boardtype, writerid) {
            var nWidth = 690;
            var nHeight = 780;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pBOARDID: boardid,
                pBOARDTYPE: boardtype,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/BoardWrite", param);

            Popupform.target = "BoardWrite";
            var win = window.open("", "BoardWrite", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function BoardView(boardid, boardtype, writerid) {
            var nWidth = 600;
            var nHeight = 900;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pBOARDID: boardid,
                pBOARDTYPE: boardtype,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/BoardView", param);

            Popupform.target = "BoardView";
            var win = window.open("", "BoardView", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function RefreshList() {
            fn_Search();//__doPostBack("<%= btnSelect.UniqueID %>", "OnClick");
            opener.RefreshList();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <!-- S:windowpop-wrap -->
        <div id="windowpop-wrap">
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title">
                        <strong>기술공유</strong>
                    </div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <!-- S:searchbox -->
                    <div class="searchboxPopUp">
		                <fieldset>
                            <span class="inpbox first">
                                <label>제 목&nbsp;&nbsp;</label>
                                <asp:TextBox ID="txbTitle" runat="server" MaxLength="150" Width="400px" TabIndex="1"></asp:TextBox>
                            </span>
                            <br />
                            <span class="inpbox first">
                                <label>작성자</label>
                                <asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" TabIndex="2"></asp:TextBox>
                            </span>
                            <asp:Button id="btnSelect" runat="server" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
                            <asp:HiddenField ID="hdfBoardType" runat="server" Value="1" />
                        </fieldset>
                    </div>
                    <!-- E:searchbox -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
                
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-notice-list">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:boardlist -->
                    <div class="boardlist">
                        <ul id="tabnav" runat="server" class="tabnav">
                        </ul>
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional" style="min-height:500px">
                            <ContentTemplate>
                                <table>
                                    <colgroup>
                                        <col style="width:68px;" />
                                        <col style="width:auto;" />
                                        <col style="width:100px;" />
                                        <col style="width:110px;" />
                                    </colgroup>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="number"><%# Eval("NUM") %></td>
                                                    <td class="list-title">
                                                        <asp:Label ID="lblBoardID" runat="server" Visible="false" Text='<%# Eval("BOARDID") %>'></asp:Label>
                                                        <asp:Label ID="lblBoardType" runat="server" Visible="false" Text='<%# Eval("BOARDTYPE") %>'></asp:Label>
                                                        <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                                        <a id="aTitle" runat="server" style="cursor:pointer"><%# Eval("TITLE") %></a>
                                                    </td>
                                                    <td  class="writer"><%# Eval("EMPNM") %></td>
                                                    <td  class="date"><%# Eval("WRITEDT") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="paging" />
                                <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click"/>
                                <asp:AsyncPostBackTrigger ControlID="btnWrite" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
                    </div>
                    <!-- E:boardlist -->
                    <!-- S:btncenter -->
                    <div style="margin-top:10px;margin-left:680px;">
                        <asp:Button id="btnWrite" runat="server" class="btn-green" Text="글쓰기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <!-- E:windowpop-wrap -->
    </form>
</body>
</html>
