﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartnersOfcPopup.aspx.cs" Inherits="KTSSolutionWeb.PartnersOfcPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>협력사 조직 정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var ajaxCallBack = function (json) {
            if (callGb != "") {
                if (callGb == "getOk") {
                    ktsGrid.setData(json);
                }
                else {
                    $.each(JSON.parse(json), function (idx, item) {
                        if (item.ERRCD == "00") {
                        }
                        else {
                            ktsGridAlert(item.ERRMSG);
                        }
                    });
                }
            }
        };

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#btnChoice").click(function (e) {
                    var rows = ktsGrid.searchRows("AUTHYN", "=", "Y");
                    var dataCnt = rows.length;
                    var officeCd = "";

                    for (var i = 0; i < dataCnt; i++) {
                        var rowData = rows[i].getData();
                        var authYn = rowData.AUTHYN;

                        if (officeCd.length > 0) {
                            officeCd = officeCd + ",";
                        }

                        officeCd = officeCd + rowData.OFFICECD;
                    }

                    if (officeCd.length > 0) {
                        var empNo = $("#<%= hdfEmpNo.ClientID %>").val();

                        var returnData;

                        if (empNo != undefined && empNo.length > 0) {
                            returnData = empNo + "|" + officeCd;
                        }
                        else {
                            returnData = officeCd;
                        }

                        opener.SetOrgCd(returnData, "M");
                        window.close();
                    }
                    else {
                        ktsGridAlert("선택된 조직이 없습니다.");
                    }
                });
            }
        };

        var fn_GridInit = function () {

            var inputCheck = function (cell, formatterPatterns) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var authYn = rowData.AUTHYN;

                var checkbox = document.createElement("input");
                checkbox.id = "cb_" + id;
                checkbox.name = "chkbox";
                checkbox.setAttribute("type", "checkbox");

                if (authYn == "Y") {
                    checkbox.checked = true;
                }
                else {
                    checkbox.checked = false;
                }

                function onChange() {
                    if (checkbox.checked) {
                        authYn = "Y";
                    }
                    else {
                        authYn = "N";
                    }

                    ktsGrid.updateData([{
                        id: id, AUTHYN: authYn
                    }])
                    .then(function () {
                        //run code after row
                    })
                    .catch(function (error) {
                        //handle error
                    });
                }

                //submit new value on blur or change
                checkbox.addEventListener("click", onChange);

                return checkbox;
            };

            var inputCheck_all = function (cell, formatterPatterns) {

                var checkbox = document.createElement("input");

                checkbox.setAttribute("type", "checkbox");

                function onChange() {
                    var dataCnt = ktsGrid.getDataCount();

                    $('input:checkbox[name="chkbox"]').each(function () {

                        if (checkbox.checked) {
                            this.checked = true; 
                        }
                        else {
                            this.checked = false; 
                        }
                    });

                    for (var i = 1; i <= dataCnt; i++) {
                        var row = ktsGrid.getRow(i);
                        var rowData = row.getData();

                        var id = rowData.id;
                        var authYn = rowData.AUTHYN;

                        if (checkbox.checked) {
                            authYn = "Y";
                        }
                        else {
                            authYn = "N";
                        }

                        ktsGrid.updateData([{
                            id: id, AUTHYN: authYn
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                }

                //submit new value on blur or change
                checkbox.addEventListener("click", onChange);

                return checkbox;
            };

            var inputRadio = function (cell, formatterPatterns) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var authYn;

                var radiobox = document.createElement("input");
                radiobox.id = "rb_" + id;
                radiobox.name = "radiobox";
                radiobox.setAttribute("type", "radio");

                function onChange() {
                    var dataCnt = ktsGrid.getDataCount();

                    for (var i = 1; i <= dataCnt; i++) {

                        var rowA = ktsGrid.getRow(i);
                        var rowDataA = rowA.getData();

                        var idA = rowDataA.id;

                        if (idA == id) {
                            authYn = "Y";
                        }
                        else {
                            authYn = "N";
                        }

                        ktsGrid.updateData([{
                            id: idA, AUTHYN: authYn
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                }

                //submit new value on blur or change
                radiobox.addEventListener("click", onChange);

                return radiobox;
            };

            var Columns;
            var gtype = $("#<%= hdfGrid.ClientID %>").val();

            if (gtype == "C") {
                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", field: "C", formatter: inputCheck, titleFormatter: inputCheck_all, hozAlign: "center", headerSort: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사명", field: "OFFICENM", width: 160, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", width: 160, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "권한여부", field: "AUTHYN", visible: false }
                ];
            }
            else {
                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", field: "R", formatter: inputRadio, hozAlign: "center", headerSort: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사명", field: "OFFICENM", width: 160, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", width: 160, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "권한여부", field: "AUTHYN", visible: false }
                ];
            }

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            //ktsGrid.setHeight("30vh");
        }

        var fn_Load = function () {
            fn_Search();
        }

        function fn_Search() {

            var companyCd = $("#<%= hdfCompanyCd.ClientID %>").val();
            var empNo = $("#<%= hdfEmpNo.ClientID %>").val();

            callGb = "getOk";

            //입력 파라미터
            var aData = [];
            aData[0] = companyCd;
            aData[1] = empNo;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            //var ajaxUrl = grid.getUrl(location.href, 'GetData');
            var ajaxUrl = ajaxCallGetUrl(location.href, "GetData");

            //grid Bind
            //grid.getDataBind(jsonData, ajaxUrl);
            ajaxCallEx(jsonData, ajaxUrl, true);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">협력사 조직권한 조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->

                    <!-- S:btncenter -->
                    <div class="btncenter" style="margin-top:30px;text-align:center;">
                        <input id="btnChoice" type="button" value="선택" class="btn-green" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:HiddenField ID="hdfCompanyCd" runat="server" />
        <asp:HiddenField ID="hdfEmpNo" runat="server" />
        <asp:HiddenField ID="hdfGrid" runat="server" />
    </form>
</body>
</html>
