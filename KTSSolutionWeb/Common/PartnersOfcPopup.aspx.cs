﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;

namespace KTSSolutionWeb
{
    public partial class PartnersOfcPopup : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //세션 체크
                if (this.Page.Session.Count <= 5)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
                }
                else
                {
                    this.hdfCompanyCd.Value = Request.Form["pCOMPCD"] == null ? "" : Request.Form["pCOMPCD"].ToString();
                    this.hdfEmpNo.Value = Request.Form["pEMPNO"] == null ? "" : Request.Form["pEMPNO"].ToString();
                    this.hdfGrid.Value = Request.Form["pTYPE"] == null ? "" : Request.Form["pTYPE"].ToString();

                    //updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            
            string authId = HttpContext.Current.Session["AUTHID"].ToString();

            //권한 체크
            if (aData[0].Length > 0)
            {
                string rptCd = "OFFICEINFOBYPARTNERS";
                string jobGb = "GET";

                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }
    }
}