﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

namespace KTSSolutionWeb.Common
{
    public partial class BoardView : PageBase
    {
        private string BoardId
        {
            get
            {
                if (ViewState["BoardId"] != null)
                    return ViewState["BoardId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["BoardId"] = value;
            }
        }

        private string BoardType
        {
            get
            {
                if (ViewState["BoardType"] != null)
                    return ViewState["BoardType"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["BoardType"] = value;
            }
        }

        private string WriterId
        {
            get
            {
                if (ViewState["WriterId"] != null)
                    return ViewState["WriterId"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["WriterId"] = value;
            }
        }

        private string FileSeq
        {
            get
            {
                if (ViewState["FileSeq"] != null)
                    return ViewState["FileSeq"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["FileSeq"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                initializeControls();

                if (!IsPostBack)
                {
                    GetDataList();
                }
            }
        }

        private void initializeControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    BoardId = Request.Form["pBOARDID"] == null ? "" : Request.Form["pBOARDID"].ToString();
                    BoardType = Request.Form["pBOARDTYPE"] == null ? "" : Request.Form["pBOARDTYPE"].ToString();
                    WriterId = Request.Form["pWRITERID"] == null ? "" : Request.Form["pWRITERID"].ToString();
                }

                string strAuthId = this.Page.Session["AUTHID"] == null ? "" : this.Page.Session["AUTHID"].ToString();
                string strEmpNo = this.Page.Session["EMPNO"] == null ? "" : this.Page.Session["EMPNO"].ToString();

                if (strAuthId.Equals("AUTH0001") || strEmpNo.Equals(WriterId))
                {
                    this.btnRegBoard.OnClientClick = "javascript:BoardWrite('" + BoardId + "', '" + BoardType + "', '" + WriterId + "'); window.close();";
                    this.btnDelBoard.Click += new EventHandler(BtnDelBoard_ServerClick);

                    this.btnRegBoard.Visible = true;
                    this.btnDelBoard.Visible = true;
                }
                else
                {

                    this.btnRegBoard.Visible = false;
                    this.btnDelBoard.Visible = false;
                }

                updPanelBtn.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();
            DataSet dsFile = new DataSet();

            try
            {
                string strAuthId = this.Page.Session["AUTHID"] == null ? "" : this.Page.Session["AUTHID"].ToString();                

                if (BoardId != "")
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardList(BoardId, BoardType, "", "");
                        dsFile = Noti.GetBoardFile(BoardId, "");
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.sTitle.InnerText = ds.Tables[0].Rows[0]["TITLE"].ToString();
                            this.pWriterNm.InnerHtml = ds.Tables[0].Rows[0]["EMPNM"].ToString() + "<span>" + ds.Tables[0].Rows[0]["WRITEDT"].ToString() + "</span>";
                            this.divContent.InnerHtml = "<p>" + ds.Tables[0].Rows[0]["CONTENTS"].ToString().Replace("\r\n", "<br/>") + " </p>";

                            if (dsFile.Tables.Count > 0)
                            {
                                if (dsFile.Tables[0].Rows.Count > 0)
                                {

                                    rptFile.DataSource = dsFile.Tables[0];
                                    rptFile.DataBind();

                                    for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                    {
                                        if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                        {
                                            HtmlImage ImgBoard = new HtmlImage();

                                            string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                            byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                            ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                            ImgBoard.Attributes.Add("style", "max-width:100%");

                                            divImg.Controls.Add(ImgBoard);
                                            divImg.Controls.Add(new LiteralControl("<br />"));
                                            divImg.Controls.Add(new LiteralControl(dsFile.Tables[0].Rows[i]["FILENM"].ToString().Split('.')[0]));
                                            divImg.Controls.Add(new LiteralControl("<br />"));
                                        }
                                    }

                                    if (this.Session.Count > 0)
                                    {
                                        divFileList.Visible = true;
                                    }
                                    else
                                    {
                                        divFileList.Visible = false;
                                    }
                                }
                                else
                                {
                                    divFileList.Visible = false;
                                }
                            }

                            GetReplyDataList();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "BoardRefrash", "window.close();RefreshList();", true);
                        }
                    }
                }

                updPanelImg.Update();
                updPanel1.Update();

                updPanelFile.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (ds != null)
                    dsFile.Dispose();
            }
        }

        #region GetReplyDataList
        /// <summary>
        /// GetReplyDataList
        /// </summary>
        private void GetReplyDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = this.Page.Session["EMPNO"] == null ? "" : this.Page.Session["EMPNO"].ToString();

                if (BoardId != "")
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardReplyList(BoardId);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        DataRow dr = dt.NewRow();

                        dt.Rows.Add(dr);

                        rptReply.DataSource = dt;
                        rptReply.DataBind();
                        for (int i = 0; i < rptReply.Items.Count; i++)
                        {
                            Label lblReEmpNo = (Label)rptReply.Items[i].FindControl("lblReEmpNo");
                            TextBox txbRePly = (TextBox)rptReply.Items[i].FindControl("txbRePly");
                            Label lblRePly = (Label)rptReply.Items[i].FindControl("lblRePly");
                            Button btnReReg = (Button)rptReply.Items[i].FindControl("btnReReg");
                            Button btnReDel = (Button)rptReply.Items[i].FindControl("btnReDel");

                            if (lblReEmpNo.Text.Equals(strEmpNo))
                            {
                                txbRePly.Visible = true;
                                lblRePly.Visible = false;
                                btnReReg.Visible = true;
                                btnReDel.Visible = true;
                                btnReReg.Text = "수정";

                                AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
                                trigger1.ControlID = btnReReg.UniqueID;
                                trigger1.EventName = "Click";
                                updPanel1.Triggers.Add(trigger1);

                                AsyncPostBackTrigger trigger2 = new AsyncPostBackTrigger();
                                trigger2.ControlID = btnReDel.UniqueID;
                                trigger2.EventName = "Click";
                                updPanel1.Triggers.Add(trigger1);
                            }
                            else if (lblReEmpNo.Text.Equals(""))
                            {
                                txbRePly.Visible = true;
                                lblRePly.Visible = false;
                                btnReReg.Visible = true;
                                btnReDel.Visible = false;
                                btnReReg.Text = "등록";

                                btnReReg.Attributes.Add("onClientClick", "return ReRegChk()");

                                AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
                                trigger1.ControlID = btnReReg.UniqueID;
                                trigger1.EventName = "Click";
                                updPanel1.Triggers.Add(trigger1);

                            }
                            else
                            {
                                txbRePly.Visible = false;
                                lblRePly.Visible = true;
                                btnReReg.Visible = false;
                                btnReDel.Visible = false;
                            }
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strTitle"></param>
        /// <param name="sbContents"></param>
        /// <param name="strMsg"></param>
        /// <returns></returns>
        private bool ValidationCheck(string strTitle, StringBuilder sbContents, ref string strMsg)
        {
            bool bResult = true;

            if (strTitle.Length == 0)
            {
                strMsg = "제목을 입력해주세요.";
                bResult = false;
            }
            else if (sbContents.Length == 0)
            {
                strMsg = "내용을 입력해주세요.";
                bResult = false;
            }

            return bResult;
        }
        #endregion

        #region BtnDelBoard_ServerClick
        /// <summary>
        /// BtnDelBoard_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelBoard_ServerClick(object sender, EventArgs e)
        {
            try
            {
                using (Notice Noti = new Notice())
                {
                    Noti.DelBoardList(BoardId);
                    Noti.DelBoardFile(BoardId, "");
                    Noti.DelBoardReply(BoardId, "");
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "BoardDelete", "alert('게시글 삭제를 완료 했습니다.');window.close();RefreshList();", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnDownload_ServerClick
        /// <summary>
        /// BtnDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnDownload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                HtmlAnchor btnDownload = (HtmlAnchor)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                FileSeq = lblSeq.Text;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDownLoad", "FileDownload();", true);
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region BtnFileDownload_ServerClick
        /// <summary>
        /// BtnFileDownload_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnFileDownload_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                if (FileSeq.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다..');window.close();", true);
                }
                else
                {
                    using (Notice Noti = new Notice())
                    {
                        ds = Noti.GetBoardFile(BoardId, FileSeq);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                            byte[] bFile = new byte[nFileLen];
                            bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                            string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                            using (FileUtil fileUtil = new FileUtil())
                            {
                                fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                            }
                        }
                    }

                    FileSeq = "";
                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region btnReReg_ServerClick
        /// <summary>
        /// btnReReg_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReReg_ServerClick(object sender, EventArgs e)
        {
            try
            {
                string strEmpNo = Session["EMPNO"] == null ? "" : this.Page.Session["EMPNO"].ToString();

                Button btnReReg = (Button)sender;
                Label lblReSeq = (Label)btnReReg.Parent.FindControl("lblReSeq");
                Label lblReEmpNo = (Label)btnReReg.Parent.FindControl("lblReEmpNo");
                Label lblRePly = (Label)btnReReg.Parent.FindControl("lblRePly");
                TextBox txbRePly = (TextBox)btnReReg.Parent.FindControl("txbRePly");

                using (Notice Noti = new Notice())
                {
                    if (lblReEmpNo.Text.Equals(""))
                    {
                        Noti.InsBoardReply(BoardId, txbRePly.Text, strEmpNo);
                        GetReplyDataList();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Insert", "alert('댓글을 등록했습니다.');", true);
                    }
                    else
                    {
                        if (txbRePly.Visible)
                        {
                            Noti.UpdBoardReply(BoardId, lblReSeq.Text, txbRePly.Text);
                            GetReplyDataList();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Update", "alert('댓글이 수정되었습니다.');", true);
                        }
                        else
                        {
                            lblRePly.Visible = false;
                            txbRePly.Visible = true;

                            btnReReg.Attributes.Add("onclick", "return ReRegChk()");

                            updPanel1.Update();
                        }
                    }
                }
                string strScrollPos = this.hdfScrollTop.Value.ToString();
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "scrollset", "$(\"#divResult\").scrollTop('" + strScrollPos + "');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnReDel_ServerClick
        /// <summary>
        /// btnReDel_ServerClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReDel_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Button btnReDel = (Button)sender;
                Label lblReSeq = (Label)btnReDel.Parent.FindControl("lblReSeq");

                using (Notice Noti = new Notice())
                {
                    Noti.DelBoardReply(BoardId, lblReSeq.Text);
                }

                GetReplyDataList();
                string strScrollPos = this.hdfScrollTop.Value.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('댓글이 삭제되었습니다.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "scrollset", "$(\"#divResult\").scrollTop('" + strScrollPos + "');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

    }
}