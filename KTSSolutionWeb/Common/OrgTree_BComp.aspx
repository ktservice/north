﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgTree_BComp.aspx.cs" Inherits="KTSSolutionWeb.OrgTree_BComp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>조직선택</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">
        function postBackObject() {
            var o = window.event.srcElement;

            if (o.tagName == "INPUT" || o.tagName == "checkbox") {
                __doPostBack("<%=updPanel1.ClientID %>", "");
            }
        }

        function SendParentsForm() {
            var orgcd = $("#hdfOrgCd").val();

            opener.SetOrgCode(orgcd);
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-organization -->
            <div class="windowpop pop-organization">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>조직선택</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>

                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:organization-tree -->
                        <div class="organization-tree">

                            <div style="height:500px;margin-bottom:15px;overflow-y:scroll;">
                                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TreeView ID="OrgTree" runat="server" Font-Size="14px" OnTreeNodeCheckChanged="OrgTree_TreeNodeCheckChanged" ExpandImageUrl="/Resource/images/icon_plus_03.png" CollapseImageUrl="/Resource/images/icon_minus_02.png">

                                        </asp:TreeView>
                                        <asp:HiddenField ID="hdfOrgCd" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="OrgTree" EventName="TreeNodeCheckChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="btnConfirm" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <!-- E:organization-tree -->

                        <!-- S:btncenter -->
                        <div class="btncenter">
                            <asp:Button ID="btnConfirm" runat="server" OnClick="btnConfirm_Click" class="btn-green" Text="확인"/>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-organization -->
        </div>
    </form>
</body>
</html>
