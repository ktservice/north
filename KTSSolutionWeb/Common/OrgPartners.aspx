﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgPartners.aspx.cs" Inherits="KTSSolutionWeb.OrgPartners" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>협력사조직정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;
        var sType = 'TEAM';
        var ctype = 'R';

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });
 
        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnChoice").click(function (e) {
                    var rows = ktsGrid.searchRows("CHECKYN", "=", "Y");
                    var dataCnt = rows.length;

                    var officeNm = "";
                    var officeCd = "";
                    var teamNm = "";
                    var teamCd = "";
                    var upperOrgNm = "";
                    var upperOrgCd = "";
                    var orgFullNm = "";

                    for (var i = 0; i < dataCnt; i++) {
                        var rowData = rows[i].getData();

                        if (officeCd.length > 0) {
                            officeNm = officeNm + ",";
                            officeCd = officeCd + ",";

                            if (sType == 'TEAM') {
                                teamNm = teamNm + ",";
                                teamCd = teamCd + ",";
                                upperOrgNm = upperOrgNm + ",";
                                upperOrgCd = upperOrgCd + ",";
                                orgFullNm = orgFullNm + ",";
                            }
                        }

                        officeNm += rowData.OFFICENM;
                        officeCd += rowData.OFFICECD;

                        if (sType == 'TEAM') {
                            teamNm += rowData.TEAMNM;
                            teamCd += rowData.TEAMCD;
                            upperOrgNm += rowData.UPPERORGNM;
                            upperOrgCd += rowData.UPPERORGCD;
                            orgFullNm += rowData.ORGFULLNM;
                        }
                        
                    }

                    SendParentsForm(officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm);
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            sType = $("#<%= hdnType.ClientID %>").val();
            ctype = $("#<%= hdnCheckType.ClientID %>").val();

            obj = $('#selField');
            obj.empty();
            obj.append("<option value=''>선택</option>");
            obj.append("<option value='OFFICENM'>국사명</option>");

            if (sType == 'TEAM') {
                obj.append("<option value='TEAMNM'>팀명</option>");
                obj.append("<option value='UPPERORGNM'>상위조직명</option>");
                obj.append("<option value='ORGFULLNM'>조직명</option>");
            }
        }

        var fn_GridInit = function () {

            var inputCheck = function (cell, formatterPatterns) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var checkYn = rowData.CHECKYN;

                var checkbox = document.createElement("input");
                checkbox.id = "cb_" + id;
                checkbox.name = "chkbox";
                checkbox.setAttribute("type", "checkbox");

                if (checkYn == "Y") {
                    checkbox.checked = true;
                }
                else {
                    checkbox.checked = false;
                }

                function onChange() {
                    if (checkbox.checked) {
                        checkYn = "Y";
                    }
                    else {
                        checkYn = "N";
                    }

                    ktsGrid.updateData([{
                        id: id, CHECKYN: checkYn
                    }])
                    .then(function () {
                        //run code after row
                    })
                    .catch(function (error) {
                        //handle error
                    });
                }

                //submit new value on blur or change
                checkbox.addEventListener("click", onChange);

                return checkbox;
            };

            var inputCheckAll = function (cell, formatterPatterns) {

                var checkbox = document.createElement("input");

                checkbox.setAttribute("type", "checkbox");

                function onChange() {
                    var dataCnt = ktsGrid.getDataCount();

                    $('input:checkbox[name="chkbox"]').each(function () {

                        if (checkbox.checked) {
                            this.checked = true; 
                        }
                        else {
                            this.checked = false; 
                        }
                    });

                    for (var i = 1; i <= dataCnt; i++) {
                        var row = ktsGrid.getRow(i);
                        var rowData = row.getData();

                        var id = rowData.id;
                        var checkYn = rowData.CHECKYN;

                        if (checkbox.checked) {
                            checkYn = "Y";
                        }
                        else {
                            checkYn = "N";
                        }

                        ktsGrid.updateData([{
                            id: id, CHECKYN: checkYn
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                }

                //submit new value on blur or change
                checkbox.addEventListener("click", onChange);

                return checkbox;
            };

            var inputRadio = function (cell, formatterPatterns) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;
                var checkYn;

                var radiobox = document.createElement("input");
                radiobox.id = "rb_" + id;
                radiobox.name = "radiobox";
                radiobox.setAttribute("type", "radio");

                function onChange() {
                    var dataCnt = ktsGrid.getDataCount();

                    for (var i = 1; i <= dataCnt; i++) {

                        var rowA = ktsGrid.getRow(i);
                        var rowDataA = rowA.getData();

                        var idA = rowDataA.id;

                        if (idA == id) {
                            checkYn = "Y";
                        }
                        else {
                            checkYn = "N";
                        }

                        ktsGrid.updateData([{
                            id: idA, CHECKYN: checkYn
                        }])
                        .then(function () {
                            //run code after row
                        })
                        .catch(function (error) {
                            //handle error
                        });
                    }
                }

                //submit new value on blur or change
                radiobox.addEventListener("click", onChange);

                return radiobox;
            };

            var gridCheck = function (cell, formatterPatterns) {
                if (ctype == 'C') {
                    return inputCheck(cell, formatterPatterns);
                }
                else {
                    return inputRadio(cell, formatterPatterns);
                }
            };

            var gridCheckAll = function (cell, formatterPatterns) {
                if (ctype == 'C') {
                    return inputCheckAll(cell, formatterPatterns);
                }
            };

            var Columns;

            if (sType == "TEAM") {
                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", field: "", formatter: gridCheck, titleFormatter: gridCheckAll, hozAlign: "center", headerSort: false, width: 40 },
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사명", field: "OFFICENM", width: 140, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", width: 120, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "팀명", field: "TEAMNM", width: 140, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "팀코드", field: "TEAMCD", width: 120, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "상위조직명", field: "UPPERORGNM", width: 140, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "상위조직코드", field: "UPPERORGCD", width: 120, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "조직명", field: "ORGFULLNM", width: 340, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "선택여부", field: "CHECKYN", visible: false }
                ];
            }
            else {
                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", field: "", formatter: gridCheck, titleFormatter: gridCheckAll, hozAlign: "center", headerSort: false, width: 40 },
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사명", field: "OFFICENM", width: 140, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "OFFICECD", width: 120, sorter: "string" },
                    { headerHozAlign: "center", hozAlign: "center", title: "선택여부", field: "CHECKYN", visible: false }
                ];
            }

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //ktsGrid.setHeight("30vh");
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        function fn_Search() {
            var pUpperOrgCd = $("#<%= hdnUpperOrgCd.ClientID %>").val();
            var pOfficeCd = $("#<%= hdnOfficeCd.ClientID %>").val();
            var pTeamCd = $("#<%= hdnTeamCd.ClientID %>").val();
            var pKtsYn = $("#<%= hdnKtsYn.ClientID %>").val();

            pType = sType;

            callGb = "getOk";

            //입력 파라미터
            var aData = [];
            aData[0] = pType;
            aData[1] = pUpperOrgCd;
            aData[2] = pOfficeCd;
            aData[3] = pTeamCd;
            aData[4] = pKtsYn;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm(officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm) {
            if (officeCd.length > 0) {
                opener.SetOrgPartner(officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm);
                window.close();
            }
            else {
                ktsGridAlert("선택된 조직이 없습니다.");
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">협력사조직 선택</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />
            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
			                </div>
                        </div>
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none;">
                                <span>
                                    <select id="selField">
                                    </select> : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->

                    <!-- S:btncenter -->
                    <div class="btncenter" style="margin-top:30px;text-align:center;">
                        <input id="btnChoice" type="button" value="선택" class="btn-green" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:HiddenField ID="hdnType" runat="server" />
        <asp:HiddenField ID="hdnCheckType" runat="server" />
        <asp:HiddenField ID="hdnUpperOrgCd" runat="server" />
        <asp:HiddenField ID="hdnOfficeCd" runat="server" />
        <asp:HiddenField ID="hdnTeamCd" runat="server" />
        <asp:HiddenField ID="hdnKtsYn" runat="server" />
    </form>
</body>
</html>