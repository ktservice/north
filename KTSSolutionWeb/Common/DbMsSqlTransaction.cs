﻿/*
 *  Copyright ⓒ 2022 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using KTS.KTSSolution.Common.DAL;
using KTS.KTSSolution.Framework.Web;
using System;
using System.Data;
using System.Web;

namespace KTSSolutionWeb
{
    public class DbMsSqlTransaction
    {
        string empno;

        HttpContext getHttpContext;

        public DbMsSqlTransaction(HttpContext httpContext)
        {
            getHttpContext = httpContext;
        }

        public string SessionCheck(string reDirect = "/Login")
        {
            string result = string.Empty;
            string domain = string.Empty;
            Uri url = null;
            bool sessionChk = Utility.SessionChk(getHttpContext);

            if (!sessionChk)
            {
                //세션 초기화
                getHttpContext.Session.Abandon();

                domain = getHttpContext.Request.Url.GetLeftPart(UriPartial.Authority);
                url = new Uri(new Uri(domain), reDirect);

                DataTable dt = new DataTable();
                dt = Utility.DataTableError("02", "세션이 만료되었습니다.", url.ToString());
                result = Utility.DataTableToJson(dt);
            }
            else
            {
                empno = getHttpContext.Session["EMPNO"].ToString();
            }

            return result;
        }



        public DataSet GetDataSet(string query)
        {
            string errCd = string.Empty;
            string errMsg = string.Empty;

            DataSet ds = new DataSet();
            (ds, errCd, errMsg) = GetTableRow(query);

            try
            {
                if (errCd.Equals("01"))
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError(errCd, errMsg, "");
                    ds.Clear();
                    ds.Tables.Add(dt);
                }
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            

            return ds;
        }


        public string GetJsonData(string query)
        {
            string result = string.Empty;
            string errCd = string.Empty;
            string errMsg = string.Empty;

            DataSet ds = new DataSet();
            (ds, errCd, errMsg) = GetTableRow(query);

            if (errCd.Equals("01"))
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError(errCd, errMsg, "");
                result = Utility.DataTableToJson(dt);
            }
            else
            {
                try
                {
                    if (ds == null)
                    {
                        DataTable dt = new DataTable();
                        dt = Utility.DataTableError(errCd, errMsg, "");
                        result = Utility.DataTableToJson(dt);
                    }
                    else if (ds.Tables[0] == null)
                    {
                        DataTable dt = new DataTable();
                        dt = Utility.DataTableError(errCd, errMsg, "");
                        result = Utility.DataTableToJson(dt);
                    }
                    else
                    {
                        result = Utility.DataTableToJson(ds.Tables[0]);
                        ds.Dispose();
                    }
                }
                catch(Exception ex)
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError(errCd, errMsg, "");
                    result = Utility.DataTableToJson(dt);
                }

            }

            return result;
        }

        #region GetParamString
        /// <summary>
        /// GetParamString
        /// </summary>
        /// <param name="param">파라미터</param>
        /// <returns>string</returns>
        public string GetParamString(string[] param)
        {
            string queryParam = string.Empty;

            queryParam = "(";
            for (int i = 0; i < param.Length; i++)
            {
                if (i > 0) queryParam += ",";

                queryParam += "'";
                queryParam += DSUtill.DbStringReplace(param[i]);
                queryParam += "'";
            }

            queryParam += ")";

            return queryParam;
        }
        #endregion


        #region GetCallSpString

        /// <summary>
        /// GetCallSpString
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <returns>string</returns>
        public string GetCallSpString(string storedProcedure, string[] param)
        {
            string query = string.Empty;
            string queryParam = string.Empty;

            if (param.Length > 0) queryParam = GetParamString(param);

            query = string.Format("CALL {0}{1}", storedProcedure, queryParam);
            query = "{" + query + "}";

            return query;
        }
        #endregion

        #region CallDbMsSqlDataSet
        /// <summary>
        /// CallDbMsSqlDataSet
        /// </summary>
        /// <param name="query">쿼리</param>
        /// <returns>DataSet, string, string</returns>
        public (DataSet, string, string) CallDbMsSqlDataSet(string query)
        {
            string errCd = "00"; //정상
            string errMsg = string.Empty;

            DataSet ds = null;

            try
            {
                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.ExecMsSqlInquiry(query);
                }
            }
            catch (Exception ex)
            {
                errCd = "01";
                errMsg = ex.ToString();
            }

            return (ds, errCd, errMsg);
        }
        #endregion


        public (DataSet, string, string) GetTableRow(string query)
        {
            string errCd = string.Empty;
            string errMsg = string.Empty;

            DataSet ds = new DataSet();

            try
            {
                (ds, errCd, errMsg) = CallDbMsSqlDataSet(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
            }

            return (ds, errCd, errMsg);
        }


    }
}