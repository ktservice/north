﻿var fn_SvcAdd = function () {
    var tbody = $("#tbody");

    var id = 0;
    var trCnt = tbody.find('tr').length;

    if (trCnt > 0)
    {
        id = tbody.find('tr:last').find("label[for='lblNo']").text();
    }

    id++;

    var tr = document.createElement("tr");
    tr.setAttribute("id", "tr_" + id);

    //순번
    var tdNo = document.createElement("td");
    tdNo.setAttribute("style", "border-left:0px;");

    var lblNo = document.createElement("label");
    lblNo.innerText = id;
    lblNo.className = "lblNo";
    lblNo.setAttribute("for", "lblNo");

    tdNo.append(lblNo);

    var lblSeq = document.createElement("label");
    lblSeq.setAttribute("style", "display:none;");
    lblSeq.innerText = "";
    lblSeq.setAttribute("id", "lblSeq_" + id);

    tdNo.append(lblSeq);

    tr.append(tdNo);

    //상품분류
    var tdSvcType = document.createElement("td");

    var ddlSvcType = document.createElement("select");
    ddlSvcType.setAttribute("id", "ddlSvcType_" + id);
    ddlSvcType.setAttribute("style", "width:100px;");

    var SvcTypeChange = function () {
        var SvcType = ddlSvcType.value;
        if (SvcType == "모바일") {
            maxlen = 9;
        }

        fn_ControlBind_SvcGbn(id, SvcType);
    }

    ddlSvcType.addEventListener("change", SvcTypeChange);

    var svctypelist = ["인터넷", "TV", "모바일", "전화", "AP", "업셀링", "기타"];

    var option = document.createElement("option");

    option.value = "";
    option.text = "선택하세요";
    option.selected = true;

    ddlSvcType.append(option);

    svctypelist.forEach(function (str) {
        options = document.createElement("option");

        options.value = str;
        options.text = str;

        ddlSvcType.append(options);
    });

    tdSvcType.append(ddlSvcType);

    tr.append(tdSvcType);

    //상품구분
    var tdSvcGbn = document.createElement("td");

    var ddlSvcGbn = document.createElement("select");
    ddlSvcGbn.setAttribute("id", "ddlSvcGbn_" + id);
    ddlSvcGbn.setAttribute("style", "width:270px;");
    ddlSvcGbn.setAttribute("disabled", "disabled");

    var SvcGbnChange = function () {
        var SvcType = ddlSvcType.value;
        var SvcGbn = ddlSvcGbn.value;

        fn_ControlBind_SvcList(id, SvcType, SvcGbn);
    }

    ddlSvcGbn.addEventListener("change", SvcGbnChange);

    option = document.createElement("option");

    option.value = "";
    option.text = "선택하세요";
    option.selected = true;

    ddlSvcGbn.append(option);
    tdSvcGbn.append(ddlSvcGbn);
    tr.append(tdSvcGbn);

    //상세상품
    var tdSvcList = document.createElement("td");

    var ddlSvcList = document.createElement("select");
    ddlSvcList.setAttribute("id", "ddlSvcList_" + id);
    ddlSvcList.setAttribute("style", "width:290px;");
    ddlSvcList.setAttribute("disabled", "disabled");

    option = document.createElement("option");

    option.value = "";
    option.text = "선택하세요";
    option.selected = true;

    ddlSvcList.append(option);
    tdSvcList.append(ddlSvcList);
    tr.append(tdSvcList);

    //모델 (모바일)
    var tdMobile = document.createElement("td");

    var ddlMobile = document.createElement("select");
    ddlMobile.setAttribute("id", "ddlMobile_" + id);
    ddlMobile.setAttribute("style", "width:180px;");
    ddlMobile.setAttribute("disabled", "disabled");

    tdMobile.append(ddlMobile);
    tr.append(tdMobile);

    //개통전화번호
    var tdTel = document.createElement("td");

    var txbTel = document.createElement("input");
    txbTel.setAttribute("id", "txbTel_" + id);
    txbTel.setAttribute("type", "Text");
    txbTel.setAttribute("maxlength", "20");
    txbTel.setAttribute("style", "width:130px;");
    txbTel.setAttribute("onkeypress", "return OnlyNumber();");
    txbTel.setAttribute("onkeydown", "ReplaceKorean(this);");

    tdTel.append(txbTel);
    tr.append(tdTel);

    //수량
    var tdCnt = document.createElement("td");

    var txbCnt = document.createElement("input");
    txbCnt.setAttribute("id", "txbCnt_" + id);
    txbCnt.setAttribute("type", "Text");
    txbCnt.setAttribute("maxlength", "11");
    txbCnt.setAttribute("style", "width:50px;");
    txbCnt.setAttribute("onkeypress", "return OnlyNumber();");
    txbCnt.setAttribute("onkeydown", "ReplaceKorean(this);");

    tdCnt.append(txbCnt);
    tr.append(tdCnt);

    //결합
    var tdCombine = document.createElement("td");

    var chbCombine = document.createElement("input");
    chbCombine.setAttribute("id", "chbCombine_" + id);
    chbCombine.setAttribute("type", "checkbox");

    tdCombine.append(chbCombine);
    tr.append(tdCombine);

    //반값초이스
    var tdHalfPrice = document.createElement("td");

    var chbHalfPrice = document.createElement("input");
    chbHalfPrice.setAttribute("id", "chbHalfPrice_" + id);
    chbHalfPrice.setAttribute("type", "checkbox");
    chbHalfPrice.setAttribute("disabled", "disabled");
 
    tdHalfPrice.append(chbHalfPrice);
    tr.append(tdHalfPrice);

    //회선구분
    var tdLine = document.createElement("td");

    var ddlLine = document.createElement("select");
    ddlLine.setAttribute("id", "ddlLine_" + id);
    ddlLine.setAttribute("style", "width:100px;");

    var linelist = ["자회선", "모회선"];

    option = document.createElement("option");

    option.value = "";
    option.text = "선택하세요";
    option.selected = true;

    ddlLine.append(option);

    linelist.forEach(function (str) {
        options = document.createElement("option");

        options.value = str;
        options.text = str;

        ddlLine.append(options);
    });

    tdLine.append(ddlLine);
    tr.append(tdLine);

    //약정갱신
    var tdRebew = document.createElement("td");

    var chbRebew = document.createElement("input");
    chbRebew.setAttribute("id", "chbRebew_" + id);
    chbRebew.setAttribute("type", "checkbox");

    tdRebew.append(chbRebew);
    tr.append(tdRebew);

    //업셀링
    var tdUpCelling = document.createElement("td"); 

    var chbUpCelling = document.createElement("input");
    chbUpCelling.setAttribute("id", "chbUpCelling_" + id);
    chbUpCelling.setAttribute("type", "checkbox");

    tdUpCelling.append(chbUpCelling);
    tr.append(tdUpCelling);

    //서비스계약번호
    var tdScn = document.createElement("td");

    var txbScn = document.createElement("input");
    txbScn.setAttribute("id", "txbScn_" + id);
    txbScn.setAttribute("type", "Text");
    txbScn.setAttribute("maxlength", "11");
    txbScn.setAttribute("style", "width:110px;");
    txbScn.setAttribute("onkeydown", "ReplaceKorean(this);");

    tdScn.append(txbScn);
    tr.append(tdScn);

    //비고
    var tdNote = document.createElement("td");

    var txbNote = document.createElement("input");
    txbNote.setAttribute("id", "txbNote_" + id);
    txbNote.setAttribute("type", "Text");
    txbNote.setAttribute("maxlength", "500");
    txbNote.setAttribute("style", "width:190px;");

    tdNote.append(txbNote);
    tr.append(tdNote);

    var tdDel = document.createElement("td");
    tdDel.setAttribute("id", "tdDel_" + id);

    if (!$("#thDel").is(":visible")) {
        tdDel.setAttribute("style", "display:none;");
    }

    var btnDel = document.createElement("input");
    btnDel.setAttribute("id", "btnDel_" + id);
    btnDel.setAttribute("type", "button");
    btnDel.setAttribute("class", "btn-del");
    btnDel.setAttribute("style", "width:40px;height:25px;font-size:16px;");
    if ($("#thDel").is(":visible")) {
        btnDel.setAttribute("onclick", "fn_DelShtSvc('" + id + "');");
    }
    btnDel.setAttribute("value", "삭제");

    tdDel.append(btnDel);
    tr.append(tdDel);

    tbody.append(tr);

    $("#chbCombine_" + id).change(function (e) {
        if ($("#chbCombine_" + id).is(":checked")) {
            $("#chbHalfPrice_" + id).removeAttr('disabled');
        }
        else {
            $("#chbHalfPrice_" + id).prop("checked", false);
            $("#chbHalfPrice_" + id).attr('disabled', 'disabled');
        }
    });

    fn_ControlBind_MobileList(id);
};

var fn_ControlBind_SvcGbn = function (id, svctype) {
    var selector = $("#ddlSvcGbn_" + id);
    selector.empty();

    var trtEmpNo = $("#lblTrtEmpNo").text();

    if (svctype == "모바일") {
        $("#txbScn_" + id).attr("maxlength", "9");
    }
    else {
        $("#txbScn_" + id).attr("maxlength", "11");
    }

    if (svctype == "인터넷" || svctype == "TV" || svctype == "모바일") {
        var option = document.createElement("option");

        option.value = "";
        option.text = "선택하세요";
        option.selected = true;

        selector.append(option);

        $.each(JSON.parse(Json_scvGbn), function (idx, item) {
            option = document.createElement("option");

            if (svctype == item.SVCTYPE) {
                option.value = item.SVCGBN;
                option.text = item.SVCGBN;
                selector.append(option);
            }
        });
    }
    else {
        var option = document.createElement("option");

        option.value = svctype;
        option.text = svctype;
        option.selected = true;

        selector.append(option);
    }

    var ddlLine = $("#ddlLine_" + id);

    var linelist = ["자회선", "모회선"];
    ddlLine.empty();
    if (svctype == "TV") {
        linelist = ["기본단말", "추가단말"];
    }

    var lineOption = document.createElement("option");

    lineOption.value = "";
    lineOption.text = "선택하세요";

    ddlLine.append(lineOption);

    linelist.forEach(function (str) {
        lineOption = document.createElement("option");

        lineOption.value = str;
        lineOption.text = str;

        ddlLine.append(lineOption);
    });

    if (svctype == "" || trtEmpNo == "") {
        selector.attr('disabled', 'disabled');
    }
    else {
        selector.removeAttr('disabled');

        if (svctype == "모바일") {
            $("#ddlMobile_" + id).removeAttr('disabled');
            $("#txbTel_" + id).removeAttr('disabled');
            $("#chbRebew_" + id).attr('disabled', 'disabled');
            $("#chbUpCelling_" + id).attr('disabled', 'disabled');
        }
        else {
            $("#ddlMobile_" + id).attr('disabled', 'disabled');
            $("#txbTel_" + id).attr('disabled', 'disabled');
            $("#chbRebew_" + id).removeAttr('disabled');
            $("#chbUpCelling_" + id).removeAttr('disabled');
        }
    }

    $("#ddlMobile_" + id).val('');

    fn_ControlBind_SvcList(id, svctype, '');
};

var fn_ControlBind_SvcList = function (id, svctype, svcgbn) {
    var selector = $("#ddlSvcList_" + id);
    selector.empty();

    if (svctype == "") {
        selector.attr('disabled', 'disabled');
    }
    else if (svctype == "인터넷" || svctype == "TV" || svctype == "모바일") {
        var option = document.createElement("option");

        option.value = "";
        option.text = "선택하세요";
        option.selected = true;

        selector.append(option);

        if (svcgbn == "") {
            selector.attr('disabled', 'disabled');
        }
        else {
            selector.removeAttr('disabled');

            $.each(JSON.parse(Json_scvList), function (idx, item) {
                options = document.createElement("option");

                if (svctype == item.SVCTYPE && svcgbn == item.SVCGBN) {
                    options.value = item.SVCNM;
                    options.text = item.SVCNM;
                    selector.append(options);
                }
            });
        }
    }
    else {
        selector.attr('disabled', 'disabled');

        selector.empty();

        var option = document.createElement("option");

        option.value = svctype;
        option.text = svctype;
        option.selected = true;

        selector.append(option);
    }
};

var fn_ControlBind_MobileList = function (id) {
    var selector = $("#ddlMobile_" + id);

    selector.empty();

    var option = document.createElement("option");

    option.value = "";
    option.text = "선택하세요";
    option.selected = true;

    selector.append(option);

    $.each(JSON.parse(Json_mobileList), function (idx, item) {
        options = document.createElement("option");

        options.value = item.model_code;
        options.text = item.model_code;

        selector.append(options);
    });
};

var fn_ControlSet_Modal = function (procid, upperYn, empNo) {
    var rows = ktsGrid1.searchRows("PROCID", "=", procid);

    var rowData = rows[0].getData();
    var passType = rowData.PASSTYPE;
    var passTypeNm = rowData.PASSTYPENM;
    var rctType = rowData.RCTTYPE;
    var hopeSvc = rowData.HOPESVC; //K-VIZERO(SPACE), BIZ사업(BIZ) 일 경우 rctType 대신
    var rctSvc = rowData.SVCTYPE;
    var rctTypeNm = rowData.RCTTYPENM;
    var rctDate = rowData.RCTDATE;
    var conNo = rowData.CONNO;  //K-VIZERO(SPACE), BIZ사업(BIZ) 일 경우 안씀
    var custNm = rowData.CUSTNM;
    var custTelNo = rowData.CUSTTELNO;
    var relations = rowData.RELATIONS;
    var note1 = rowData.NOTE1;
    var passStat = rowData.PASSSTAT;
    var passStatNm = rowData.PASSSTATNM;
    var trtOrgNm = rowData.TRTORGNM;
    var trtEmpNo = rowData.TRTEMPNO;
    var trtEmpNm = rowData.TRTEMPNM;
    var succYn = rowData.SUCCYN;
    var note2 = rowData.NOTE2;
    var failNote = rowData.FAILNOTE;
    var monthFinYn = rowData.MONTHFIN;
    var uploadYn = rowData.UPLOADYN;
    var choiceYn = rowData.CHOICEYN;
    var fileYn = rowData.FILEYN;

    if (passType == "SPACE" || passType == "BIZ") {
        $("#lblRctType").html('희망상품 :&nbsp;');
        $("#txbRctType").val(hopeSvc);
        $("#spanConNo").hide();
        $('#divDetailSvc').hide();
        $('#divModal1').css('height', 420);

        $('#btnAdd').hide();
    }
    else {
        $("#lblRctType").html('접수유형 :&nbsp;');
        $("#txbRctType").val(rctTypeNm);
        $("#spanConNo").show();
        $("#txbConNo").val(conNo);
        $('#divDetailSvc').show();
        $('#divModal1').css('height', 730);
    }

    $("#txbRctType").val(rctTypeNm);
    $("#txbRctDate").val(rctDate);
    $("#txbCusNm").val(custNm);
    $("#txbRelations").val(relations);
    $("#lblPassType").html(passType);
    $("#txbPassType").val(rctSvc);
    $("#txbPassStat").val(passStatNm);
    $("#lblPassStat").html(passStat);
    $("#txaNote1").val(note1);

    $("#txbCustTelNo").val(custTelNo);
    $("#ddlSuccYn").val(succYn);
    $("#txbNote2").val(note2);
    $("#txbFailNote").val(failNote);

    $("#btnTrtEmpNo").off("click");
    $("#btnChoice").off("click");
    $("#btnReg").off("click");
    $("#btnDel").off("click");

    $("#btnDel").hide();

    var choice = "";
    $("#lblTrtEmpChk").html('');

    if (trtEmpNo.length > 0) {
        choice = "선택완료";
        $("#lblTrtEmpChk").html('Y');
        $("#ddlSuccYn").removeAttr("disabled");
    }
    else {
        choice = "선택안됨";
        $("#ddlSuccYn").attr("disabled", "disabled");
    }

    $("#lblChoiceYn").html(choice);
    $("#lblTrtEmpNo").html(trtEmpNo);

    if (monthFinYn == "Y") {
        //월마감시 수정불가
        $("#spanChoice").show();
        $("#lblChoiceYn").show();
        $("#btnChoice").hide();
        $("#spanTrtEmpNo").hide();
        $("#thDel").hide();
        $("#btnReg").hide();

        $("#txbConNo").attr("readonly", "readonly");
        $("#txbCustTelNo").attr("readonly", "readonly");
        $("#ddlSuccYn").attr("disabled", "disabled");
        $("#txbNote2").attr("readonly", "readonly");
        $("#txbFailNote").attr("readonly", "readonly");
    }
    else {
        if (passType != "SPACE" && passType != "BIZ") {
            if (passStat == "S") {
                $('#btnAdd').show();
                $("#thDel").show();
            }
            else {
                $('#btnAdd').hide();
                $("#thDel").hide();
            }
        }

        if (upperYn == "Y") {
            $("#spanChoice").hide();
            $("#spanTrtEmpNo").show();
            $("#lblTrtEmpNm").html(trtOrgNm + " " + trtEmpNm);
            $("#btnDel").show();

            $("#btnTrtEmpNo").on("click", function () {
                fn_PopupPassUser(passType, empNo, 0);
            });

            $("#btnDel").on("click", function () {
                //삭제로직
                fn_DelPassInfo(procid, passType);
            });
        } 
        else {
            $("#spanChoice").show();

            if (trtEmpNo.length > 0) {
                $("#lblChoiceYn").show();
                $("#btnChoice").hide();
            }
            else if (choiceYn == "Y") {
                $("#lblChoiceYn").hide();
                $("#btnChoice").show();

                $("#btnChoice").on("click", function () {
                    fn_PassChoice(procid);
                });
            }
            else {
                $("#lblChoiceYn").show();
                $("#btnChoice").hide();
            }

            $("#spanTrtEmpNo").hide();
        }

        $("#btnReg").on("click", function () {
            //저장로직
            fn_SetPassInfo(procid);
        });
    }

    $("#btnFiles").off();
    var bAlert = false;

    if (uploadYn == "Y") {
        $("#btnFiles").on("click", function () {
            fn_PopupFiles(procid);
        });
        $("#btnFiles").show();

        if (fileYn == "Y") {
            bAlert = true;
        }
    } else {
        $("#btnFiles").hide();
    }

    if (bAlert) {
        $("#divModal1").show(100, function () { alert("첨부가 있습니다. 확인 부탁드립니다."); });
    }
    else {
        $("#divModal1").show(100);
    }

    if (passType != "SPACE" && passType != "BIZ") {
        fn_GetShtSvc();
    }
};

var fn_SetControl_HalfPrice = function (id) {
    if ($("#chbCombine_" + id).id(":checked")) {
        $("#chbHalfPrice_" + id).removeAttr('disabled');
    }
    else {
        $("#chbHalfPrice_" + id).prop("checked", false);
        $("#chbHalfPrice_" + id).attr('disabled', 'disabled');
    }
};

var fn_ControlBind_ShtSvc = function (json, procid) {
    let rows = ktsGrid1.searchRows("PROCID", "=", procid);
    let rowData = rows[0].getData();
    let monthFinYn = rowData.MONTHFIN;
    var trtEmpNo = $("#lblTrtEmpNo").text();

    let tbody = $("#tbody");
    tbody.empty();

    let id = 0;

    $.each(JSON.parse(json), function (idx, item) {
        id++;
        
        var tr = document.createElement("tr");
        tr.setAttribute("id", "tr_" + id);

        //순번
        let tdNo = document.createElement("td");
        tdNo.setAttribute("style", "border-left:0px;");

        var lblNo = document.createElement("label");
        lblNo.innerText = id;
        lblNo.className = "lblNo";
        lblNo.setAttribute("for", "lblNo");

        tdNo.append(lblNo);

        let lblSeq = document.createElement("label");
        lblSeq.setAttribute("style", "display:none;");
        lblSeq.innerText = item.SVCID;
        lblSeq.setAttribute("id", "lblSeq_" + id);

        tdNo.append(lblSeq);

        tr.append(tdNo);

        //상품분류
        let tdSvcType = document.createElement("td");

        var ddlSvcType = document.createElement("select");
        ddlSvcType.setAttribute("id", "ddlSvcType_" + id);
        ddlSvcType.setAttribute("name", "ddlSvcType");
        ddlSvcType.setAttribute("style", "width:100px;");

        if (trtEmpNo == "") {
            ddlSvcType.setAttribute("disabled", "disabled");
        }

        let svctypelist = ["인터넷", "TV", "모바일", "전화", "AP", "업셀링", "기타"];

        let option = document.createElement("option");

        option.value = "";
        option.text = "선택하세요";
        option.selected = true;

        ddlSvcType.append(option);

        svctypelist.forEach(function (str) {
            options = document.createElement("option");

            options.value = str;
            options.text = str;

            ddlSvcType.append(options);
        });

        tdSvcType.append(ddlSvcType);

        tr.append(tdSvcType);

        //상품구분
        let tdSvcGbn = document.createElement("td");

        let ddlSvcGbn = document.createElement("select");
        ddlSvcGbn.setAttribute("id", "ddlSvcGbn_" + id);
        ddlSvcGbn.setAttribute("name", "ddlSvcGbn");
        ddlSvcGbn.setAttribute("style", "width:270px;");
        ddlSvcGbn.setAttribute("disabled", "disabled");

        option = document.createElement("option");

        option.value = "";
        option.text = "선택하세요";
        option.selected = true;

        ddlSvcGbn.append(option);
        tdSvcGbn.append(ddlSvcGbn);
        tr.append(tdSvcGbn);

        //상세상품
        let tdSvcList = document.createElement("td");

        let ddlSvcList = document.createElement("select");
        ddlSvcList.setAttribute("id", "ddlSvcList_" + id);
        ddlSvcList.setAttribute("style", "width:290px;");
        ddlSvcList.setAttribute("disabled", "disabled");
        
        option = document.createElement("option");

        option.value = "";
        option.text = "선택하세요";
        option.selected = true;

        ddlSvcList.append(option);
        tdSvcList.append(ddlSvcList);
        tr.append(tdSvcList);

        //모델 (모바일)
        let tdMobile = document.createElement("td");
        
        let ddlMobile = document.createElement("select");
        ddlMobile.setAttribute("id", "ddlMobile_" + id);
        ddlMobile.setAttribute("style", "width:180px;");
        ddlMobile.setAttribute("disabled", "disabled");

        tdMobile.append(ddlMobile);
        tr.append(tdMobile);

        //개통전화번호
        let tdTel = document.createElement("td");

        let txbTel = document.createElement("input");
        txbTel.setAttribute("id", "txbTel_" + id);
        txbTel.setAttribute("type", "Text");
        txbTel.setAttribute("maxlength", "20");
        txbTel.setAttribute("style", "width:130px;");
        txbTel.setAttribute("onkeypress", "return OnlyNumber();");
        txbTel.setAttribute("onkeydown", "ReplaceKorean(this);");

        if (trtEmpNo == "") {
            txbTel.setAttribute("disabled", "disabled");
        }
        
        txbTel.value = item.TELNO;

        tdTel.append(txbTel);
        tr.append(tdTel);

        //수량
        let tdCnt = document.createElement("td");

        let txbCnt = document.createElement("input");
        txbCnt.setAttribute("id", "txbCnt_" + id);
        txbCnt.setAttribute("type", "Text");
        txbCnt.setAttribute("maxlength", "11");
        txbCnt.setAttribute("style", "width:50px;");
        txbCnt.setAttribute("onkeypress", "return OnlyNumber();");
        txbCnt.setAttribute("onkeydown", "ReplaceKorean(this);");

        if (trtEmpNo == "") {
            txbCnt.setAttribute("disabled", "disabled");
        }

        txbCnt.value = item.CNT;

        tdCnt.append(txbCnt);
        tr.append(tdCnt);

        //결합
        let tdCombine = document.createElement("td");

        let chbCombine = document.createElement("input");
        chbCombine.setAttribute("id", "chbCombine_" + id);
        chbCombine.setAttribute("name", "chbCombine");
        chbCombine.setAttribute("type", "checkbox"); 

        if (trtEmpNo == "") {
            chbCombine.setAttribute("disabled", "disabled");
        }

        if (item.COMBINEYN == "Y") {
            chbCombine.setAttribute("checked", "checked");
        }

        tdCombine.append(chbCombine);
        tr.append(tdCombine);

        //반값초이스
        let tdHalfPrice = document.createElement("td"); 

        let chbHalfPrice = document.createElement("input");
        chbHalfPrice.setAttribute("id", "chbHalfPrice_" + id);
        chbHalfPrice.setAttribute("type", "checkbox");

        if (trtEmpNo == "") {
            chbHalfPrice.setAttribute("disabled", "disabled");
        }

        if (item.HALFPRICEYN == "Y") {
            chbHalfPrice.setAttribute("checked", "checked");
        }

        if (item.COMBINEYN != "Y") {
            chbHalfPrice.setAttribute("disabled", "disabled");
        }

        tdHalfPrice.append(chbHalfPrice);
        tr.append(tdHalfPrice);

        //회선구분
        let tdLine = document.createElement("td"); 

        let ddlLine = document.createElement("select");
        ddlLine.setAttribute("id", "ddlLine_" + id);
        ddlLine.setAttribute("style", "width:100px;");

        if (trtEmpNo == "") {
            ddlLine.setAttribute("disabled", "disabled");
        }

        var linelist = ["자회선", "모회선"];

        option = document.createElement("option");

        option.value = "";
        option.text = "선택하세요";

        ddlLine.append(option);

        linelist.forEach(function (str) {
            options = document.createElement("option");

            options.value = str;
            options.text = str;

            ddlLine.append(options);
        });

        tdLine.append(ddlLine);
        tr.append(tdLine);

        //약정갱신
        let tdRebew = document.createElement("td");

        let chbRebew = document.createElement("input");
        chbRebew.setAttribute("id", "chbRebew_" + id);
        chbRebew.setAttribute("type", "checkbox"); 

        if (trtEmpNo == "") {
            chbRebew.setAttribute("disabled", "disabled");
        }

        if (item.REBEWYN == "Y") {
            chbRebew.setAttribute("checked", "checked");
        }

        tdRebew.append(chbRebew);
        tr.append(tdRebew);

        //업셀링
        let tdUpCelling = document.createElement("td");

        let chbUpCelling = document.createElement("input");
        chbUpCelling.setAttribute("id", "chbUpCelling_" + id);
        chbUpCelling.setAttribute("type", "checkbox"); 

        if (trtEmpNo == "") {
            chbUpCelling.setAttribute("disabled", "disabled");
        }

        if (item.UPCELLINGYN == "Y") {
            chbUpCelling.setAttribute("checked", "checked");
        }

        tdUpCelling.append(chbUpCelling);
        tr.append(tdUpCelling);

        //서비스계약번호
        let tdScn = document.createElement("td");

        let txbScn = document.createElement("input");
        txbScn.setAttribute("id", "txbScn_" + id);
        txbScn.setAttribute("type", "Text");
        txbScn.setAttribute("maxlength", "11");
        txbScn.setAttribute("style", "width:110px;");
        txbScn.setAttribute("onkeydown", "ReplaceKorean(this);"); 

        if (trtEmpNo == "") {
            txbScn.setAttribute("disabled", "disabled");
        }

        txbScn.value = item.SCN;

        tdScn.append(txbScn);
        tr.append(tdScn);

        //비고
        let tdNote = document.createElement("td");

        let txbNote = document.createElement("input");
        txbNote.setAttribute("id", "txbNote_" + id);
        txbNote.setAttribute("type", "Text");
        txbNote.setAttribute("maxlength", "500");
        txbNote.setAttribute("style", "width:190px;");

        if (trtEmpNo == "") {
            txbNote.setAttribute("disabled", "disabled"); 
        }

        txbNote.value = item.NOTE;

        tdNote.append(txbNote);
        tr.append(tdNote);

        let tdDel = document.createElement("td");
        tdDel.setAttribute("id", "tdDel_" + id);

        if (!$("#thDel").is(":visible")) {
            tdDel.setAttribute("style", "display:none;");
        }

        let btnDel = document.createElement("input");
        btnDel.setAttribute("id", "btnDel_" + id);
        btnDel.setAttribute("type", "button");
        btnDel.setAttribute("class", "btn-del");
        btnDel.setAttribute("style", "width:40px;height:25px;font-size:16px;");
        if ($("#thDel").is(":visible")) {
            btnDel.setAttribute("onclick", "fn_DelShtSvc('" + id + "');");
        }
        btnDel.setAttribute("value", "삭제");

        tdDel.append(btnDel);
        tr.append(tdDel);

        tbody.append(tr);

        fn_ControlBind_SvcGbn(id, item.SVCTYPE);
        fn_ControlBind_SvcList(id, item.SVCTYPE, item.SVCGBN);
        fn_ControlBind_MobileList(id);
        
        $("#ddlSvcType_" + id).val(item.SVCTYPE);
        $("#ddlSvcGbn_" + id).val(item.SVCGBN);
        $("#ddlSvcList_" + id).val(item.SVCLIST);
        $("#ddlMobile_" + id).val(item.MODEL);
        $("#ddlLine_" + id).val(item.LINEGBN);

        $("#ddlSvcType_" + id).change(function (e) {
            var ddlSvcTypeId = $(this).attr("id");
            var controlId = ddlSvcTypeId.split('_');
            var svcType = $(this).val();

            if (controlId.length > 1) {
                fn_ControlBind_SvcGbn(controlId[1], svcType);
            }
        });

        $("#ddlSvcGbn_" + id).change(function (e) {
            var ddlSvcTypeId = $(this).attr("id");
            var controlId = ddlSvcTypeId.split('_');
            var svcGbn = $(this).val();

            if (controlId.length > 1) {
                var svcType = $("#ddlSvcType_" + controlId[1]).val();
                fn_ControlBind_SvcList(controlId[1], svcType, svcGbn);
            }
        });

        $("#chbCombine_" + id).change(function (e) {
            var chbCombine = $(this).attr("id");
            var controlId = chbCombine.split('_');

            if (controlId.length > 1) {
                if ($(this).is(":checked")) {
                    if (monthFinYn != "Y") {
                        $("#chbHalfPrice_" + controlId[1]).removeAttr('disabled');
                    }
                }
                else {
                    $("#chbHalfPrice_" + controlId[1]).prop("checked", false);
                    $("#chbHalfPrice_" + controlId[1]).attr('disabled', 'disabled');
                }
            }
        });

        if (monthFinYn == "Y") {
            $("#ddlSvcType_" + id).attr("disabled", "disabled");
            $("#ddlSvcGbn_" + id).attr("disabled", "disabled");
            $("#ddlSvcList_" + id).attr("disabled", "disabled");
            $("#ddlMobile_" + id).attr("disabled", "disabled");
            $("#txbTel_" + id).attr("disabled", "disabled");
            $("#txbCnt_" + id).attr("disabled", "disabled");
            $("#chbCombine_" + id).attr("disabled", "disabled");
            $("#chbHalfPrice_" + id).attr("disabled", "disabled");
            $("#ddlLine_" + id).attr("disabled", "disabled");
            $("#chbRebew_" + id).attr("disabled", "disabled");
            $("#chbUpCelling_" + id).attr("disabled", "disabled");
            $("#txbScn_" + id).attr("disabled", "disabled");
            $("#txbNote_" + id).attr("disabled", "disabled");
        } 
    });
};

var fn_ControlBind = function (json, ctrl) {
    var selector;

    if (ctrl == 'PASS' || ctrl == 'PASS2') {
        selector = $("#ddlPassType");
    }
    else if (ctrl == 'RCT') {
        selector = $("#ddlRctType");
    }
    else {
        selector = $("#ddlConsult");
    }

    selector.empty();

    var options = document.createElement("option");

    options.className = "options";
    options.value = "";
    options.text = "선택하세요";

    selector.append(options);

    $.each(JSON.parse(json), function (idx, item) {
        options = document.createElement("option");

        options.className = "options";

        if (ctrl == 'PASS' || ctrl == 'PASS2') {
            options.value = item.PASSTYPE;
            options.text = item.PASSTYPENM;
        }
        else if (ctrl == 'RCT') {

            options.value = item.RCTTYPE;
            options.text = item.RCTTYPENM;
        }
        else {
            options.value = item.AUTHOPERATOR;
            options.text = item.AUTHOPERNM;
        }

        selector.append(options);
    });

    if (ctrl == 'PASS2') {
        $("#ddlPassType2").empty();

        $("#ddlPassType option").each(function () {
            options = $(this).clone();
            $("#ddlPassType2").append(options);
        });
    }

    var PassType = "";
    var PassType2 = $("#ddlPassType2 option:selected").val();

    if (ctrl == 'RCT') {
        if (PassType2 == undefined) {
            PassType = $("#ddlPassType option:selected").val();
        }
        else {
            PassType = $("#ddlPassType2 option:selected").val();
        }

        if (PassType == 'BIZ') {
            fn_GetConsult();
        }

        if (rctTypeBind != undefined && rctTypeBind != "") {
            $("#ddlRctType").val(rctTypeBind);
        }
    }

    if (ctrl == "CON") {
        if (consultBind != undefined && consultBind != "") {
            $("#ddlConsult").val(consultBind);
        }
    }
};