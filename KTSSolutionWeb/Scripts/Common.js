﻿function createForm(url, obj) {
    var form = document.createElement("form");

    form.setAttribute("method", "post");
    form.setAttribute("action", url);

    if (obj != null) {
        for (var name in obj) {
            var valObj = obj[name];
            addInput(form, name, valObj);
        }
    }

    document.body.appendChild(form);

    return form;
}

function addInput(form, name, value) {
    var input = document.createElement("input");

    input.setAttribute("name", name);
    input.setAttribute("type", "hidden");
    input.setAttribute("value", value);
    
    form.appendChild(input);
}

function requestDataForm(url, param, fn_Seccess, fn_Error) {
    $.ajax({
        type: "POST",
        url: url,
        data: param,
        success: function () {
            fn_Seccess;
        },
        error: function () {
            fn_Error;
        }
    });
    
}

function OnlyNumber(event) {
    event = event || window.event;
    var keyid = (event.which) ? event.which : event.keyCode;
    if ((keyid < 48) || (keyid > 57)) {
        return false;
    }
}

function OnlyNumber2(event, obj) {
    var val = obj.value;

    event = event || window.event;
    var keyid = (event.which) ? event.which : event.keyCode;
    if ((keyid >= 48 && keyid <= 57) || keyid == 46) {
        if (keyid == 46) {
            if (val.indexOf(".") < 1) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    }
    else {
        return false;
    }
}

function ReplaceKorean(obj) {
    if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 18 || event.keyCode == 46)
        return;

    obj.value = obj.value.replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, '');
}

//3자리 콤마
function priceToString(price) {
    if (numCheck(price)) return price;

    if (price == undefined) {
        return '';
    }
    else {
        if (price.toString().indexOf('.') >= 0) {
            var priceNum = price.toString().split('.');
            return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
        }
        else {
            return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        }
    }
}

//숫자체크 false이면 숫자임
function numCheck(val) {
    return isNaN(val);
}

var CompareMonToday = function (inputMdate) {
    var today = new Date();
    var year = today.getFullYear();
    var month = ('0' + (today.getMonth() + 1)).slice(-2);
    var vDate = year + '-' + month;

    if (vDate.replace('-', '') < inputMdate.replace('-', '')) {
        return false;
    }
    else {
        return true;
    }
}

var cmPostPopup = function (form, target, param, nWidth, nHeight) {
    var nTop = (document.body.offsetHeight / 2) - (nHeight / 2);
    var nLeft = (document.body.offsetWidth / 2) - (nWidth / 2);

    nTop += window.screenTop;
    nLeft += window.screenLeft;

    var Popupform = createForm(form, param);

    Popupform.target = target;
    var win = window.open("", target, "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
    Popupform.submit();

    win.focus();
}

var cmPostPopup2 = function (form, target, param, nWidth, nHeight, nLocation, nMenubar, nStatus, nToolbar, nScrollbars) {
    var nTop = (document.body.offsetHeight / 2) - (nHeight / 2);
    var nLeft = (document.body.offsetWidth / 2) - (nWidth / 2);
    var options = '';

    nTop += window.screenTop;
    nLeft += window.screenLeft;

    options = "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft;

    if (nLocation != '') options += ',location='+nLocation;
    if (nMenubar != '') options += ',menubar=' +nMenubar;
    if (nStatus != '') options += ',status=' +nStatus;
    if (nToolbar != '') options += ',toolbar=' + nToolbar;
    if (nScrollbars != '') options += ',scrollbars=' + nScrollbars;

    var Popupform = createForm(form, param);

    Popupform.target = target;
    var win = window.open("", target, options);
    Popupform.submit();

    win.focus();
}

var ajaxCallGetUrl = function (url, method) {
    var rtnUrl = '';

    if (url.indexOf('.aspx') >= 0) rtnUrl = url.replace(/#/gi, "") + '/' + method;
    else rtnUrl = url.replace(/#/gi, "") + '.aspx/' + method;

    return rtnUrl;
}

var ajaxCallEx = function (param, url, gridAlert) {
    $.ajax({
        type: 'post',
        url: url,
        data: param,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            var json = data.d;

            ajaxCallSuccess(json, gridAlert);
        },
        error: function (request, status, error) {
            ajaxFail(request, status, error, gridAlert);
        }
    })
}

var ajaxCallajaxFail = function (request, status, error, gridAlert) {
    var errmsg = "code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error;

    if (gridAlert) ktsGridAlert(errmsg, 5000);
    else alert(result.status);
}

var ajaxCallSuccess = function (json, gridAlert) {
    var errCd;
    var errMsg;
    var errRedirect;

    $.each(JSON.parse(json), function (idx, item) {
        if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
        if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
        if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
        //console.log(item.SERVICE_ERROR_CD);
        return false;
    });

    if (errCd == '01') //일반오류
    {
        if (gridAlert) ktsGridAlert(errMsg);
        else alert(errMsg);
        
    }
    else if (errCd == '02') //세션만료
    {
        if (gridAlert) ktsGridAlert(errMsg);
        else alert(errMsg);

        location.href = errRedirect;
    }
    else //정상
    {
        ajaxCallBack(json);
    }
}

//ajax 콜백을 위해 함수만 정의
var ajaxCallBack = function (json) {

}

var ajaxCall = function (param, url, bAsync) {
    if (bAsync == undefined || bAsync == 'undefined' || bAsync == null) {
        bAsync = true;
    }

    $.ajax({
        type: 'post',
        url: url,
        data: param,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: bAsync,
        success: function (data) {
            var json = data.d;
            successCallBack(json);
        },
        error: function (request, status, error) {
            alert("오류가 발생했습니다.");
        }
    })
};

var ajaxCallFileUpload = function (param, url) {
    $.ajax({
        type: "post",
        url: url,
        data: param,
        contentType: false,
        processData: false,
        success: function (data) {
            ajaxCallSuccess(data, false);
        },
        error: function (request, status, error) {
            ajaxFail(request, status, error, false);
        }
    })
}

//ajax 콜백을 위해 함수만 정의
var successCallBack = function (json) {

}