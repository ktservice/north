﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.Order;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class OrderInfoDetail : PageBase
    {
        public string strOdrNum
        {
            get
            {
                if (ViewState["OdrNum"] != null)
                {
                    return ViewState["OdrNum"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["OdrNum"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();

                    updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void PageInit()
        {
            DataSet ds = null;
            DataSet dsFile = null;

            try
            {
                strOdrNum = Request.Form["pORDERNUM"] == null ? "" : Request.Form["pORDERNUM"].ToString();

                if (!strOdrNum.Equals(""))
                {
                    using (OrderMgmt mgmt = new OrderMgmt())
                    {
                        ds = mgmt.GetOrderNumInfo(strOdrNum);
                        dsFile = mgmt.GetOrderFile(strOdrNum);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.ddlOrderCode.SelectedValue = ds.Tables[0].Rows[0]["ORDERCODE"].ToString();
                        this.txbWorktype.Text = ds.Tables[0].Rows[0]["WORKTYPE"].ToString();
                        this.txbFranchiseNm.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                        this.txbCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();
                        this.txbAddr.Text = ds.Tables[0].Rows[0]["ADDR"].ToString();
                        this.txbPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();
                        this.txbHopeDt.Text = ds.Tables[0].Rows[0]["HOPEDT"].ToString();
                        this.txbEquipment.Text = ds.Tables[0].Rows[0]["EQUIPMENT"].ToString();
                        this.txbOrderState.Text = ds.Tables[0].Rows[0]["ORDERSTATE"].ToString();
                        this.txbNote.Text = ds.Tables[0].Rows[0]["NOTE"].ToString();
                        this.txbReason.Text = ds.Tables[0].Rows[0]["REASON"].ToString();
                        this.txbEmpUserNm.Text = ds.Tables[0].Rows[0]["FULLWORKEREMPNM"].ToString();
                        this.hdfOrgCd.Value = ds.Tables[0].Rows[0]["WORKERORGCD"].ToString();

                        string strWorkState = ds.Tables[0].Rows[0]["WORKSTATE"].ToString();

                        if (dsFile.Tables.Count > 0)
                        {
                            if (dsFile.Tables[0].Rows.Count > 0)
                            {

                                for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                {
                                    if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                    {
                                        HtmlImage ImgBoard = new HtmlImage();

                                        string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                        byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                        ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                        ImgBoard.Attributes.Add("style", "max-width:100%;height:auto;");

                                        spanImg.Controls.Add(ImgBoard);
                                    }
                                }
                            }
                        }
                    }

                    trReason.Visible = true;
                    trImg.Visible = true;
                }
                else
                {
                    trReason.Visible = false;
                    trImg.Visible = false;
                }

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
    }
}