﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.Order;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace KTSSolutionWeb
{
    public partial class RegVanOrderInfo : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        private void PageInit()
        {
            SetDDLRcvType();
            SetDDLRsvTime();
            GetVanOrderInfo();
        }
        

        private void SetDDLRcvType()
        {
            DataSet ds = null;

            try
            {
                ddlRcvType.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "RCVTYPELIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add(""); //파라미터 추가 시
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    ddlRcvType.Items.Add(new ListItem("선택하세요", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlRcvType.Items.Add(new ListItem(ds.Tables[0].Rows[i]["RCVTYPENM"].ToString(), ds.Tables[0].Rows[i]["RCVTYPECD"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("접수구분을 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetDDLRsvTime()
        {
            ddlRsvTime.Items.Clear();
            ddlHopeTime.Items.Clear();

            ddlRsvTime.Items.Add(new ListItem("", "00"));
            ddlHopeTime.Items.Add(new ListItem("", "00"));
            for (int i = 9; i < 18; i++)
            {
                string strTime = i.ToString().PadLeft(2, '0');

                ddlRsvTime.Items.Add(new ListItem(strTime + "시", strTime));
                ddlHopeTime.Items.Add(new ListItem(strTime + "시", strTime));
            }
        } 

        private void GetVanOrderInfo()
        {
            DataSet ds = null;
            DataSet dsFile = null;

            try
            {
                string strOrderNo = Request.Form["pORDERNO"] == null ? "" : Request.Form["pORDERNO"].ToString();

                if (!strOrderNo.Equals(""))
                {
                    this.hdfOrderNo.Value = strOrderNo;
                    this.hdfStatus.Value = "R";

                    string result = string.Empty;
                    string resultSession = string.Empty;
                    string rptCd = "VANORDERINFO";
                    string jobGb = "GET";

                    List<string> aData = new List<string>();
                    aData.Add(strOrderNo);
                    aData.Add("F"); //파라미터 추가 시

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        ds = dbTransaction.GetDataSet();

                        using (OrderMgmt mgmt = new OrderMgmt())
                        {
                            dsFile = mgmt.GetOrderFile(strOrderNo);
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string strReceiptType = ds.Tables[0].Rows[0]["RECEIPT_TYPE"].ToString();
                            this.ddlReceiptType.SelectedValue = strReceiptType;
                            this.ddlReceiptType.Enabled = false;

                            
                            bool bManual = !strOrderNo.Substring(0, 1).Equals("M") ? false : strReceiptType.Equals("VAN365") ? true : false;

                            this.ddlRcvType.SelectedValue = ds.Tables[0].Rows[0]["RCVTYPECD"].ToString();
                            this.txbAgencyNm.Text = ds.Tables[0].Rows[0]["AGENCYNM"].ToString();
                            this.txbFranchiseNm.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                            this.txbCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();
                            this.txbAddr.Text = ds.Tables[0].Rows[0]["ADDR"].ToString();
                            this.txbDtlAddr.Text = ds.Tables[0].Rows[0]["DTLADDR"].ToString();
                            this.txbTelNo.Text = ds.Tables[0].Rows[0]["TELNO"].ToString();
                            this.txbPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();
                            this.lblRcvDt.Text = ds.Tables[0].Rows[0]["RCVDT"].ToString();
                            this.hdfRsvDt.Value = ds.Tables[0].Rows[0]["RSVDT"].ToString() + " " + ds.Tables[0].Rows[0]["RSVTIME"].ToString() + ":00:00";
                            this.ddlPartnersYn.SelectedValue = ds.Tables[0].Rows[0]["PARTNERSYN"].ToString();
                            this.txbRsvDt.Text = ds.Tables[0].Rows[0]["RSVDT"].ToString();

                            try
                            {
                                this.ddlRsvTime.SelectedValue = ds.Tables[0].Rows[0]["RSVTIME"].ToString();
                            }
                            catch
                            {
                                this.ddlRsvTime.SelectedValue = "00";
                            }
                            
                            this.txbHopeDt.Text = ds.Tables[0].Rows[0]["HOPEDT"].ToString();

                            //2024.04
                            try
                            {
                                this.ddlHopeTime.SelectedValue = ds.Tables[0].Rows[0]["HOPETIME"].ToString();
                            }
                            catch
                            {
                                this.ddlHopeTime.SelectedValue = "00";
                            }

                            this.txbRcvNote.Text = ds.Tables[0].Rows[0]["RCVNOTE"].ToString();
                            this.txbReqNote.Text = ds.Tables[0].Rows[0]["REQNOTE"].ToString();
                            this.lblWorkState.Text = ds.Tables[0].Rows[0]["WORKSTATENM"].ToString();
                            this.txbRtnAddr.Text = ds.Tables[0].Rows[0]["RTNADDR"].ToString();
                            this.txbRtnDtlAddr.Text = ds.Tables[0].Rows[0]["RTNDTLADDR"].ToString();
                            this.txbReport.Text = ds.Tables[0].Rows[0]["REPORT"].ToString();
                            this.txbEmpUserNm.Text = ds.Tables[0].Rows[0]["FULLWORKEREMPNM"].ToString();
                            this.txbProc.Text = ds.Tables[0].Rows[0]["PROCINFO"].ToString();

                            this.hdfPreEmpNo.Value = ds.Tables[0].Rows[0]["WORKEREMPNO"].ToString();
                            this.hdfEmpNo.Value = ds.Tables[0].Rows[0]["WORKEREMPNO"].ToString();
                            this.hdfEmpNm.Value = ds.Tables[0].Rows[0]["WORKEREMPNM"].ToString();
                            this.hdfOrgCd.Value = ds.Tables[0].Rows[0]["WORKERORGCD"].ToString();

                            string strWorkState = ds.Tables[0].Rows[0]["WORKSTATE"].ToString();

                            this.hdfWorkState.Value = strWorkState;

                            this.hdfBelong.Value = ds.Tables[0].Rows[0]["BELONG"].ToString();
                            this.hdfAssignYn.Value = ds.Tables[0].Rows[0]["ASSIGNYN"].ToString();

                            this.ddlRcvType.Enabled = false;
                            this.ddlRcvType.Style.Add("background-color", "#f9f9f9");

                            this.txbAgencyNm.ReadOnly = true;
                            this.txbAgencyNm.Style.Add("border", "none");
                            this.txbAgencyNm.Style.Add("background-color", "#f9f9f9");

                            this.txbFranchiseNm.ReadOnly = true;
                            this.txbFranchiseNm.Style.Add("border", "none");
                            this.txbFranchiseNm.Style.Add("background-color", "#f9f9f9");

                            this.btnCorpInfo.Visible = false;
                            this.btnCorpInfo.Attributes.Remove("onclick");

                            this.txbCorpNo.ReadOnly = true;
                            this.txbCorpNo.Style.Add("border", "none");
                            this.txbCorpNo.Style.Add("background-color", "#f9f9f9");

                            this.txbHopeDt.ReadOnly = true;

                            this.txbReport.ReadOnly = true;
                            this.txbReport.Style.Add("border", "none");
                            this.txbReport.Style.Add("background-color", "#f9f9f9");

                            this.trRcvDt.Visible = true;
                            this.trImg.Visible = true;
                            this.trMemo.Visible = true;
                            this.trProc.Visible = false;

                            this.txbAddr.ReadOnly = true;
                            this.txbAddr.Style.Add("border", "none");
                            this.txbAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbDtlAddr.ReadOnly = true;
                            this.txbDtlAddr.Style.Add("border", "none");
                            this.txbDtlAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbTelNo.ReadOnly = true;
                            this.txbTelNo.Style.Add("border", "none");
                            this.txbTelNo.Style.Add("background-color", "#f9f9f9");

                            this.txbPhoneNo.ReadOnly = true;
                            this.txbPhoneNo.Style.Add("border", "none");
                            this.txbPhoneNo.Style.Add("background-color", "#f9f9f9");

                            this.txbDlvrDt.ReadOnly = true;
                            this.txbDlvrDt.Style.Add("border", "none");
                            this.txbDlvrDt.Style.Add("background-color", "#f9f9f9");

                            this.txbRcvNote.ReadOnly = true;
                            this.txbRcvNote.Style.Add("border", "none");
                            this.txbRcvNote.Style.Add("background-color", "#f9f9f9");

                            this.txbReqNote.ReadOnly = true;
                            this.txbReqNote.Style.Add("border", "none");
                            this.txbReqNote.Style.Add("background-color", "#f9f9f9");

                            this.txbRtnAddr.ReadOnly = true;
                            this.txbRtnAddr.Style.Add("border", "none");
                            this.txbRtnAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbRtnDtlAddr.ReadOnly = true;
                            this.txbRtnDtlAddr.Style.Add("border", "none");
                            this.txbRtnDtlAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbDlvrDt.Attributes.Remove("class");
                            this.txbDlvrDt.Style.Add("border", "none");
                            this.txbDlvrDt.Style.Add("background-color", "#f9f9f9");

                            //2024.04
                            if (strReceiptType.Equals("VAN365"))
                            {
                                this.trDevice.Visible = true;
                                this.hdfDevice.Value = ds.Tables[0].Rows[0]["DEVICE"].ToString();

                                string strRcvType = this.ddlRcvType.SelectedItem.Text.Replace("신규", "일반");
                                string strDevice = ds.Tables[0].Rows[0]["DEVICE"].ToString();

                                DataTable dtBasic = new DataTable();

                                try
                                {
                                    ApiTransaction apiTran = new ApiTransaction();
                                    NameValueCollection data = new NameValueCollection();

                                    string strMethod = "getVan365PolicyBasic";

                                    dtBasic = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_basic", data);

                                    string dtFilter = string.Format("RECEIPT_DIVISION = '{0}' AND DEVICE = '{1}'", strRcvType, strDevice);
                                    string dtSort = "CONTENT ASC";

                                    if (dtBasic.Rows.Count > 0)
                                    {
                                        DataRow[] dr = dtBasic.Select(dtFilter, dtSort);

                                        foreach (DataRow Ndr in dr)
                                        {
                                            this.ddlPolicyBasic.Items.Add(new ListItem(Ndr["CONTENT"].ToString(), Ndr["CONTENT"].ToString()));
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                finally
                                {
                                    dtBasic.Dispose();
                                }

                                DataTable dtExtra = new DataTable();
                                string strChkExtra = "";

                                try
                                {
                                    ApiTransaction apiTran = new ApiTransaction();
                                    NameValueCollection data = new NameValueCollection();

                                    string strMethod = "getVan365PolicyExtra";

                                    DataTable resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_extra", data);

                                    resultDt.Columns.Add("CNT");
                                    dtExtra = resultDt.Clone();

                                    string[] strDbExtra = ds.Tables[0].Rows[0]["POLICY_EXTRA"].ToString().Split('|');
                                    string[] strExtraContent = new string[strDbExtra.Length];
                                    string[] strExtraCnt = new string[strDbExtra.Length];                                    

                                    string strSearch = "";

                                    if (strDbExtra.Length > 0)
                                    {
                                        int i = 0;

                                        foreach (string str in strDbExtra)
                                        {
                                            if (!str.Equals(""))
                                            {
                                                string[] strExtra = str.Split('^');

                                                if (strExtra.Length == 2)
                                                {
                                                    string strContent = strExtra[0].Trim();
                                                    string strCnt = strExtra[1].Trim();

                                                    if (strContent.Length == 0 || !int.TryParse(strCnt, out int n))
                                                    {
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        if (!strSearch.Equals(""))
                                                        {
                                                            strSearch += ",";
                                                        }

                                                        strExtraContent[i] = strContent;
                                                        strExtraCnt[i] = strCnt;

                                                        strSearch = strSearch + "'" + strContent + "'";
                                                    }
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }

                                            i++;
                                        }
                                    }

                                    if (!strSearch.Equals(""))
                                    {
                                        string dtFilter = string.Format("RECEIPT_DIVISION = '{0}' AND DEVICE LIKE '*{1}*' AND CONTENT IN ({2}) ", strRcvType, strDevice, strSearch);

                                        string dtSort = "POLICY_EXTRA_PK ASC";

                                        if (resultDt.Rows.Count > 0)
                                        {
                                            DataRow[] dr = resultDt.Select(dtFilter, dtSort);

                                            foreach (DataRow Ndr in dr)
                                            {
                                                int n = 0;

                                                foreach (string content in strExtraContent)
                                                {
                                                    if (Ndr["CONTENT"].ToString().Equals(content))
                                                    {
                                                        Ndr["CNT"] = strExtraCnt[n];

                                                        if (strChkExtra.Length > 0)
                                                        {
                                                            strChkExtra += "|";
                                                        }

                                                        strChkExtra = strChkExtra + Ndr["POLICY_EXTRA_PK"] + "," + strExtraCnt[n];
                                                        break;
                                                    }

                                                    n++;
                                                }

                                                dtExtra.ImportRow(Ndr);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                finally
                                {
                                    result = Utility.DataTableToJson(dtExtra);
                                }

                                this.trPolicyBasic.Visible = true;
                                this.ddlPolicyBasic.Enabled = false;
                                this.trPolicyExtra.Visible = true;
                                this.hdfPolicyExtra.Value = result;// ds.Tables[0].Rows[0]["POLICY_EXTRA"].ToString();
                                this.hdfChkPolicyExtra.Value = strChkExtra;// ds.Tables[0].Rows[0]["POLICY_EXTRA"].ToString();
                                this.hdfChkPolicyExtra2.Value = strChkExtra;//ds.Tables[0].Rows[0]["POLICY_EXTRA"].ToString();

                                this.trTerminalYN.Visible = true;
                                this.hdfTerminalYn.Value = ds.Tables[0].Rows[0]["TERMINALYN"].ToString() == "보유" ? "TerminalY" : "TerminalN";

                                this.trDispatch.Visible = true;
                                this.hdfDispatch.Value = ds.Tables[0].Rows[0]["DISPATCH_TYPE"].ToString().Replace("출동", "") == "일반" ? "Dispatch_Normal" : "Dispatch_Emergency";

                                this.divHolidayChk.Visible = true;
                                this.hdfHoliDayFlag.Value = ds.Tables[0].Rows[0]["DAYWEEK_TYPE"].ToString() == "평일" ? "HolidayN" : "HolidayY";

                                this.trDlvrDt.Visible = false;
                                this.txbDlvrDt.Text = "";

                                this.trTermialAddr.Visible = true;
                                this.hdfTerminalAddr.Value = ds.Tables[0].Rows[0]["TERMINALADDR_JJ"].ToString() == "가맹점" ? "Addr" : "AddrKTS";
                                this.txbTerminalAddr.Text = ds.Tables[0].Rows[0]["TERMINALADDR"].ToString();

                                this.trSwContent.Visible = true;
                                this.txbSwContent.Text = ds.Tables[0].Rows[0]["SW_CONTENT"].ToString();

                                this.trSwSetting.Visible = true;

                                bool bSetting = false;

                                if (ds.Tables[0].Rows[0]["SW_SETTING"].ToString().Equals("세팅필요"))
                                {
                                    this.hdfSwSetting.Value = "SettingC";
                                    bSetting = true;
                                }
                                else
                                {
                                    this.hdfSwSetting.Value = "SettingN";
                                    bSetting = false;
                                }

                                this.trSwSettingHost.Visible = true;
                                this.ddlSWSettingHost.SelectedValue = ds.Tables[0].Rows[0]["SW_SETTINGHOST"].ToString();

                                this.trVan.Visible = true;
                                this.txbVan.Text = ds.Tables[0].Rows[0]["VAN"].ToString();

                                this.trCatList.Visible = true;
                                this.hdfCatList.Value = ds.Tables[0].Rows[0]["PM_NM"].ToString();

                                this.trAspName.Visible = true;
                                this.txbAspName.Text = ds.Tables[0].Rows[0]["ASP_NAME"].ToString();
                                this.trAspAccount.Visible = true;
                                this.txbAspID.Text = ds.Tables[0].Rows[0]["ASP_ID"].ToString();
                                this.txbAspPW.Text = ds.Tables[0].Rows[0]["ASP_PW"].ToString();

                                this.trSolutionName.Visible = true;
                                this.txbSolutionName.Text = ds.Tables[0].Rows[0]["SOLUTION_NAME"].ToString();
                                this.trSolutionAccount.Visible = true;
                                this.txbSolutionID.Text = ds.Tables[0].Rows[0]["SOLUTION_ID"].ToString();
                                this.txbSolutionPW.Text = ds.Tables[0].Rows[0]["SOLUTION_PW"].ToString();

                                if (bManual)
                                {
                                    this.txbHopeDt.Enabled = true;
                                    this.ddlHopeTime.Enabled = true;
                                    this.txbHopeDt.Attributes.Add("class", "date");

                                    this.txbTerminalAddr.ReadOnly = false;
                                    this.txbSwContent.ReadOnly = false;
                                    this.ddlSWSettingHost.Enabled = bSetting;
                                    this.txbVan.ReadOnly = false;
                                    this.txbAspName.ReadOnly = false;
                                    this.txbAspID.ReadOnly = false;
                                    this.txbAspPW.ReadOnly = false;
                                    this.txbSolutionName.ReadOnly = false;
                                    this.txbSolutionID.ReadOnly = false;
                                    this.txbSolutionPW.ReadOnly = false;
                                }
                                else
                                {
                                    this.txbHopeDt.Enabled = false;
                                    this.txbHopeDt.Style.Add("border", "none");
                                    this.txbHopeDt.Style.Add("background-color", "#f9f9f9");
                                    this.txbHopeDt.Style.Add("font-weight", "bold");
                                    this.txbHopeDt.Style.Add("color", "black");

                                    this.ddlHopeTime.Enabled = false;

                                    this.txbTerminalAddr.ReadOnly = true;
                                    this.txbTerminalAddr.Style.Add("border", "none");
                                    this.txbTerminalAddr.Style.Add("background-color", "#f9f9f9");

                                    this.divTerminalY.Attributes.Remove("onclick");
                                    this.divTerminalN.Attributes.Remove("onclick");

                                    this.divDispatchNormal.Attributes.Remove("onclick");
                                    this.divDispatchEmergency.Attributes.Remove("onclick");

                                    this.divAddr.Attributes.Remove("onclick");
                                    this.divAddrKTS.Attributes.Remove("onclick");

                                    this.divSettingC.Attributes.Remove("onclick");
                                    this.divSettingN.Attributes.Remove("onclick");

                                    this.txbSwContent.ReadOnly = true;
                                    this.txbSwContent.Style.Add("border", "none");
                                    this.txbSwContent.Style.Add("background-color", "#f9f9f9");

                                    this.ddlSWSettingHost.Enabled = false;

                                    this.txbVan.ReadOnly = true;
                                    this.txbVan.Style.Add("border", "none");
                                    this.txbVan.Style.Add("background-color", "#f9f9f9");

                                    this.txbAspName.ReadOnly = true;
                                    this.txbAspName.Style.Add("border", "none");
                                    this.txbAspName.Style.Add("background-color", "#f9f9f9");

                                    this.txbAspID.ReadOnly = true;
                                    this.txbAspID.Style.Add("border", "none");
                                    this.txbAspID.Style.Add("background-color", "#f9f9f9");

                                    this.txbAspPW.ReadOnly = true;
                                    this.txbAspPW.Style.Add("border", "none");
                                    this.txbAspPW.Style.Add("background-color", "#f9f9f9");

                                    this.txbSolutionName.ReadOnly = true;
                                    this.txbSolutionName.Style.Add("border", "none");
                                    this.txbSolutionName.Style.Add("background-color", "#f9f9f9");

                                    this.txbSolutionID.ReadOnly = true;
                                    this.txbSolutionID.Style.Add("border", "none");
                                    this.txbSolutionID.Style.Add("background-color", "#f9f9f9");

                                    this.txbSolutionPW.ReadOnly = true;
                                    this.txbSolutionPW.Style.Add("border", "none");
                                    this.txbSolutionPW.Style.Add("background-color", "#f9f9f9");
                                }
                            }
                            else
                            {
                                this.trDevice.Visible = false;
                                this.trPolicyBasic.Visible = false;
                                this.trPolicyExtra.Visible = false;
                                this.trTerminalYN.Visible = false;
                                this.trDispatch.Visible = false;
                                this.divHolidayChk.Visible = false;
                                this.txbHopeDt.Attributes.Add("class", "date");

                                this.trDlvrDt.Visible = true;
                                this.txbDlvrDt.Text = ds.Tables[0].Rows[0]["DLVRDT"].ToString();

                                this.trTermialAddr.Visible = false;
                                this.trSwContent.Visible = false;
                                this.trSwSetting.Visible = false;
                                this.trSwSettingHost.Visible = false;
                                this.trVan.Visible = false;
                                this.trCatList.Visible = false;
                                this.trAspName.Visible = false;
                                this.trAspAccount.Visible = false;
                                this.trSolutionName.Visible = false;
                                this.trSolutionAccount.Visible = false;
                            }

                            if (!strWorkState.Equals("4") && !strWorkState.Equals("6"))
                            {
                                this.title.Text = "VAN오더 수정";
                                //this.txbRsvDt.Enabled = true;
                                this.ddlRsvTime.Enabled = true;
                                this.btnCanCel.Visible = true;

                                this.txbRsvDt.Attributes.Add("class", "date");

                                this.txbEmpUserNm.Attributes.Add("onclick", "PopupEmpUser();");
                                this.btnEmpUser.Attributes.Add("onclick", "PopupEmpUser();");

                                this.btnCanCel.Attributes.Add("onclick", "ModalOpen();");
                                this.btnConfirm.Attributes.Add("onclick", "fn_CancelVanOrder();");

                                this.btnReg.Value = "수정";
                                this.btnReg.Visible = true;
                                this.btnReg.Attributes.Add("onclick", "fn_RegVanOrder();");
                                
                            }
                            else
                            {
                                this.title.Text = "VAN오더 상세";

                                this.txbRsvDt.ReadOnly = true;
                                this.txbRsvDt.Style.Add("border", "none");
                                this.txbRsvDt.Style.Add("background-color", "#f9f9f9");
                                this.ddlRsvTime.Enabled = false;
                                this.ddlRsvTime.Style.Add("background-color", "#f9f9f9");
                                this.ddlPartnersYn.Enabled = false;
                                this.ddlPartnersYn.Style.Add("background-color", "#f9f9f9");
                                this.btnCanCel.Visible = false;

                                this.txbRsvDt.Attributes.Remove("class");

                                this.btnCanCel.Attributes.Remove("onclick");
                                this.btnConfirm.Attributes.Remove("onclick");
                                this.btnEmpUser.Visible = false;

                                this.txbEmpUserNm.Style.Add("border", "none");
                                this.txbEmpUserNm.Style.Add("background-color", "#f9f9f9");
                                this.txbEmpUserNm.Attributes.Remove("onclick");
                                this.btnEmpUser.Attributes.Remove("onclick");

                                if (strWorkState.Equals("4"))
                                {
                                    this.trProc.Visible = true;
                                    this.txbProc.ReadOnly = true;
                                    this.txbProc.Style.Add("border", "none");
                                    this.txbProc.Style.Add("background-color", "#f9f9f9");
                                }

                                this.btnReg.Visible = false;
                                this.btnReg.Attributes.Remove("onclick");
                            }

                            //if (strOrderNo.Substring(0, 1).Equals("M"))
                            //{
                                this.btnDel.Visible = true;
                                this.btnDel.Attributes.Add("onclick", "fn_DelVanOrder();");
                            //}
                            //else
                            //{
                            //    this.btnDel.Visible = false;
                            //    this.btnDel.Attributes.Remove("onclick");
                            //}


                            this.trImg.Visible = true;
                            this.trFile.Visible = true;

                            bool bImgView = false;


                            if (dsFile.Tables.Count > 0)
                            {
                                rptFile.DataSource = dsFile.Tables[0];
                                rptFile.DataBind();

                                DataTable dtImg = new DataTable();
                                dtImg.Columns.Add("SRC");

                                if (dsFile.Tables[0].Rows.Count > 0)
                                {
                                    bImgView = true;

                                    for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                    {
                                        if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                        {
                                            DataRow dr = dtImg.NewRow();

                                            string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                            byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];

                                            dr["SRC"] = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                            dtImg.Rows.Add(dr);
                                        }
                                    }
                                }

                                rptImg.DataSource = dtImg;
                                rptImg.DataBind();
                            }

                            if (bImgView)
                            {
                                tdImgList.Visible = true;
                                tdImgAdd.RowSpan = 1;
                            }
                            else
                            {
                                tdImgList.Visible = false;
                                tdImgAdd.RowSpan = 2;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');window.close();", true);
                        }
                    }
                    else //세션이 끊겼을 경우
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('세션이 만료되었습니다.');window.close();", true);
                    }
                }
                else
                {
                    this.hdfStatus.Value = "N";

                    string strEmpNo = Session["EMPNO"].ToString();
                    string strNow = DateTime.Now.ToString("yyyyMMddHHmmss");
                    this.hdfOrderNo.Value = "M" + strNow + strEmpNo;

                    this.title.Text = "VAN오더 등록";

                    this.lblWorkState.Text = "작업자지정";

                    this.ddlReceiptType.Enabled = true;
                    this.ddlRcvType.Enabled = true;
                    this.txbAgencyNm.Enabled = true;
                    this.txbFranchiseNm.Enabled = true;
                    this.btnCorpInfo.Visible = true;
                    this.btnCorpInfo.Attributes.Add("onclick", "PopupCorpInfo();");
                    this.txbCorpNo.Enabled = true;
                    this.txbAddr.Enabled = true;
                    this.txbDtlAddr.Enabled = true;
                    this.txbTelNo.Enabled = true;
                    this.txbPhoneNo.Enabled = true;
                    this.trRcvDt.Visible = false;
                    this.txbRsvDt.Enabled = true;
                    this.ddlRsvTime.Enabled = true;
                    this.txbHopeDt.Enabled = true;
                    this.txbDlvrDt.Enabled = true;
                    this.txbRcvNote.Enabled = true;
                    this.txbReqNote.Enabled = true;
                    this.txbRtnAddr.Enabled = true;
                    this.txbRtnDtlAddr.Enabled = true;
                    this.txbReport.Enabled = true;
                    this.trImg.Visible = false;
                    this.trMemo.Visible = false;
                    this.trProc.Visible = false;

                    this.trImg.Visible = false;
                    this.trFile.Visible = false;

                    this.btnReg.Value = "등록";
                    this.btnReg.Visible = true;
                    this.btnDel.Visible = false;
                    this.btnCanCel.Visible = false;

                    this.txbRsvDt.Attributes.Add("class", "date");
                    this.txbHopeDt.Attributes.Add("class", "date");
                    this.txbDlvrDt.Attributes.Add("class", "date");

                    this.txbEmpUserNm.Attributes.Add("onclick", "PopupEmpUser();");
                    this.btnEmpUser.Attributes.Add("onclick", "PopupEmpUser();");
                    this.btnReg.Visible = true;
                    this.btnReg.Attributes.Add("onclick", "fn_RegVanOrder();");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetRcvType(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RCVTYPELIST";
            string jobGb = "GET";
            
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPolicyBasicList(List<string> aData)
        {
            string result = string.Empty;

            DataTable dtBasic = new DataTable();

            try
            {
                ApiTransaction apiTran = new ApiTransaction();
                NameValueCollection data = new NameValueCollection();

                string strMethod = "getVan365PolicyBasic";

                dtBasic = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_basic", data);
            }
            catch
            {
                dtBasic = Utility.DataTableError("01", "기본견적 조회 중 오류가 발생했습니다.", "");
            }
            finally
            {
                result = Utility.DataTableToJson(dtBasic);
            }
            
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPolicyExtraList(List<string> aData)
        {
            string result = string.Empty;

            DataTable dtExtra = new DataTable();

            try
            {
                ApiTransaction apiTran = new ApiTransaction();
                NameValueCollection data = new NameValueCollection();

                string strMethod = "getVan365PolicyExtra";

                DataTable resultDt = new DataTable();

                resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_extra", data);

                resultDt.Columns.Add("CNT");

                dtExtra = resultDt.Clone();

                if (resultDt.Rows.Count > 0)
                {
                    foreach (DataRow Ndr in resultDt.Rows)
                    {
                        Ndr["CNT"] = "1";
                        dtExtra.ImportRow(Ndr);
                    }
                }
            }
            catch
            {
                dtExtra = Utility.DataTableError("01", "추가견적 조회 중 오류가 발생했습니다.", "");
            }
            finally
            {
                result = Utility.DataTableToJson(dtExtra);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPolicyBasic(List<string> aData)
        {
            string result = string.Empty;
            
            DataTable dt = new DataTable();

            try
            {
                string strRcvType = aData[0].Replace("신규", "일반");
                string strDevice = aData[1];

                DataTable dtBasic = new DataTable();
                ApiTransaction apiTran = new ApiTransaction();
                NameValueCollection data = new NameValueCollection();

                string strMethod = "getVan365PolicyBasic";

                DataTable resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_basic", data);

                dt = resultDt.Clone();                

                string dtFilter = string.Format("RECEIPT_DIVISION = '{0}' AND DEVICE = '{1}'", strRcvType, strDevice);
                string dtSort = "CONTENT ASC";

                if (resultDt.Rows.Count > 0)
                {
                    //dt = resultDt.Select(dtFilter, dtSort).CopyToDataTable();
                    DataRow[] dr = resultDt.Select(dtFilter, dtSort);

                    foreach (DataRow Ndr in dr)
                    {
                        dt.ImportRow(Ndr);
                    }
                }
            }
            catch
            {
                dt = Utility.DataTableError("01", "기본견적 조회 중 오류가 발생했습니다.", "");
            }
            finally
            {
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPolicyExtra(List<string> aData)
        {
            string result = string.Empty;

            DataTable dt = new DataTable();

            try
            {
                string strRcvType = aData[0].Replace("신규", "일반");
                string strDevice = aData[1];
                string strSearch = aData[2];

                ApiTransaction apiTran = new ApiTransaction();
                NameValueCollection data = new NameValueCollection();

                string strMethod = "getVan365PolicyExtra";

                DataTable resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_extra", data);

                if (!resultDt.Columns.Contains("CNT"))
                {
                    resultDt.Columns.Add("CNT");
                }

                dt = resultDt.Clone();

                string dtFilter = string.Format("RECEIPT_DIVISION = '{0}' AND DEVICE LIKE '*{1}*' AND CONTENT LIKE '*{2}*' ", strRcvType, strDevice, strSearch);

                //임시 키 추후 수정
                string dtSort = "POLICY_EXTRA_PK ASC";

                if (resultDt.Rows.Count > 0)
                {
                    //dt = resultDt.Select(dtFilter, dtSort).CopyToDataTable();
                    DataRow[] dr = resultDt.Select(dtFilter, dtSort);

                    foreach (DataRow Ndr in dr)
                    {
                        //Ndr["CNT"] = "1";
                        dt.ImportRow(Ndr);
                    }
                }
            }
            catch
            {
                dt = Utility.DataTableError("01", "추가견적 조회 중 오류가 발생했습니다.", "");
            }
            finally
            {
                result = Utility.DataTableToJson(dt);
            }


            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetChkPolicyExtra(List<string> aData)
        {
            string result = string.Empty;

            DataTable dt = new DataTable();

            try
            {
                DataTable resultDt = new DataTable();
                string[] strJsonChkExtra = aData[0].Split('|');
                string[] strExtraKey = new string[strJsonChkExtra.Length];
                string[] strExtraCnt = new string[strJsonChkExtra.Length];

                ApiTransaction apiTran = new ApiTransaction();
                NameValueCollection data = new NameValueCollection();

                string strMethod = "getVan365PolicyExtra";

                resultDt = apiTran.RestApiCallToDataTable(strMethod, "van365_policy_extra", data);

                if (!resultDt.Columns.Contains("CNT"))
                {
                    resultDt.Columns.Add("CNT");
                }

                dt = resultDt.Clone();

                string strWhere = "";
                int i = 0;

                foreach (string strKey in strJsonChkExtra)
                {
                    string[] strKeyVal = strKey.Split(',');

                    if (strWhere.Length > 0)
                    {
                        strWhere += ",";
                    }

                    strExtraKey[i] = strKeyVal[0];
                    strExtraCnt[i] = strKeyVal[1];

                    strWhere += "'" + strKeyVal[0] + "'";

                    i++;
                }

                string dtFilter = string.Format("POLICY_EXTRA_PK IN ({0}) ", strWhere);

                //임시 키 추후 수정
                string dtSort = "POLICY_EXTRA_PK ASC";

                i = 0;

                if (resultDt.Rows.Count > 0)
                {
                    //dt = resultDt.Select(dtFilter, dtSort).CopyToDataTable();
                    DataRow[] dr = resultDt.Select(dtFilter, dtSort);

                    foreach (DataRow Ndr in dr)
                    {
                        //Ndr["CNT"] = "1";
                        if (Ndr["POLICY_EXTRA_PK"].ToString().Equals(strExtraKey[i]))
                         {
                            Ndr["CNT"] = strExtraCnt[i];
                            i++;
                        }
                        dt.ImportRow(Ndr);

                    }
                }
            }
            catch 
            {
                dt = Utility.DataTableError("01", "추가견적 조회 중 오류가 발생했습니다.", "");
            }
            finally
            {
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetHoliDayFlag(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "HOLIDAYFLAG";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                ds = dbTransaction.GetDataSet();

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        result = ds.Tables[0].Rows[0]["HOLIDAYFLAG"].ToString();
                    }
                }
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetWorkerNm(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;

            string strOrgCd = aData[0];
            string strEmpNm = aData[1];

            using (UserMgmt userReg = new UserMgmt())
            {
                ds = userReg.GetOrgInfo(strOrgCd);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        result = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString() + " " + strEmpNm;
                    }
                }
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFOINSERT";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string UpdVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFOUPDATE";
            string jobGb = "SET";

            string strOrderNo = aData[0];
            string strRsvDt = aData[2];
            string strWorkerEmpno = aData[4];
            string strPreWorkerEmpno = aData[5];
            string strBelong = aData[6];
            string strPreRsvDt = aData[7];
            string strAssignYn = aData[8];
            string strHopeDt = aData[9];
            string strPolicyExtra = aData[11];
            string strDispatch = aData[13];
            string strHolidayFlag = aData[14];
            string strSwSetting = aData[18];
            string strSwSettingHost = aData[19];
            string strReceiptType = aData[28];
            string strWorkState = aData[29];

            if (strSwSetting != null)
            {
                strSwSetting = strSwSetting.Equals("세팅완료") ? "미진행" : "진행";
            }

            //aData.RemoveAt(17);
            aData.RemoveAt(29);
            aData.RemoveAt(28);
            aData.RemoveAt(11);
            aData.RemoveAt(8);
            aData.RemoveAt(7);
            aData.RemoveAt(6);
            aData.RemoveAt(5);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    string strErrMsg = "";
                    DataTable dt = new DataTable();

                    try
                    {
                        string strMethod = "";

                        ApiTransaction apiTran = new ApiTransaction();

                        if (!strRsvDt.Equals(strPreRsvDt))
                        {

                            strMethod = "setRequestDateUp";
                            NameValueCollection data = new NameValueCollection();

                            data.Add("RECEIPT_PK", strOrderNo);
                            data.Add("REQUEST_DATE", strRsvDt);

                            strErrMsg = "VAN 오더 방문예정일 변경 연동 중 오류가 발생했습니다.";

                            dt = apiTran.RestApiCallToInsert(strMethod, data);
                            result = Utility.DataTableToJson(dt);

                            jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                            errcd = jsonObj["SERVICE_ERROR_CD"].ToString();
                        }

                        if (errcd.Equals("00") && strReceiptType.Equals("VAN365") && strWorkState.Equals("3.5"))
                        {
                            strMethod = "setReceiptVan365Update";

                            JObject jdata = new JObject();

                            jdata.Add("RECEIPT_PK", strOrderNo);
                            jdata.Add("DISPATCH_TYPE", strDispatch.Replace("출동", ""));
                            jdata.Add("DISPATCH_HOPE_DATE", strHopeDt);
                            jdata.Add("DAYWEEK_TYPE", strHolidayFlag);
                            jdata.Add("SW_SETTING_YN", strSwSetting);
                            jdata.Add("SW_SETTING_CONTENT", strSwSettingHost);
                            jdata.Add("POLICY_EXTRA", strPolicyExtra);

                            strErrMsg = "VAN 오더 변경 연동 중 오류가 발생했습니다.";
                            
                            dt = apiTran.RestApiCallToInsert(strMethod, jdata);
                            result = Utility.DataTableToJson(dt);
                        }
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", strErrMsg, "");
                        result = Utility.DataTableToJson(dt);
                    }

                    jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                    errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                    if (errcd.Equals("00") && !strAssignYn.Equals("Y") && !strPreWorkerEmpno.Equals(strWorkerEmpno))
                    {
                        dt = new DataTable();

                        try
                        {
                            string strKVanEmpNo = "";

                            if (strBelong.Length > 0)
                            {
                                if (strBelong.Equals("강북"))
                                {
                                    strKVanEmpNo = "1111";
                                }
                                else if (strBelong.Equals("강남"))
                                {
                                    strKVanEmpNo = "2222";
                                }
                                else if (strBelong.Equals("서부"))
                                {
                                    strKVanEmpNo = "3333";
                                }
                                else
                                {
                                    strKVanEmpNo = "1111";
                                }
                            }

                            if (strBelong.Length > 0 && strKVanEmpNo.Length > 0)
                            {
                                ApiTransaction apiTran = new ApiTransaction();
                                NameValueCollection data = new NameValueCollection();
                                //JObject jdata = new JObject();

                                string strMethod = "setReceiptStateA";

                                data.Add("RECEIPT_PK", strOrderNo);
                                data.Add("EMPLOYEE_NUMBER", strKVanEmpNo);

                                dt = apiTran.RestApiCallToInsert(strMethod, data);
                                result = Utility.DataTableToJson(dt);

                                jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                                errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                                if (errcd.Equals("00"))
                                {
                                    string strOrgCd = HttpContext.Current.Session["ORGCD"].ToString();
                                    string strEmpNm = HttpContext.Current.Session["EMPNM"].ToString();

                                    using (UserMgmt userReg = new UserMgmt())
                                    {

                                        DataSet ds = userReg.GetOrgInfo(strOrgCd);

                                        if (ds.Tables.Count > 0)
                                        {
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                strEmpNm = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString() + " " + strEmpNm;
                                            }
                                        }
                                    }

                                    string strReason = "엔지니어배정완료 (작업자 : " + strEmpNm + ")";
                                    apiTran = new ApiTransaction();

                                    //NameValueCollection data = new NameValueCollection();
                                    data = new NameValueCollection();

                                    strMethod = "setReceiptMemo";

                                    data.Add("RECEIPT_PK", strOrderNo);
                                    data.Add("MEMO", strReason);

                                    dt = apiTran.RestApiCallToInsert(strMethod, data);
                                    result = Utility.DataTableToJson(dt);
                                }
                            }
                        }
                        catch
                        {
                            dt = Utility.DataTableError("03", "VAN 오더 배정처리 연동 중 오류가 발생했습니다.", "");
                            result = Utility.DataTableToJson(dt);
                        }
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFODELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string CancelVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFOCANCEL";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strReason = "(취소)" + aData[1] + " (작업자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add("북부 : " + strReason);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptCancel";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("CONTENT", strReason);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 취소처리 연동 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetMemoData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERMEMOLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            string strOrderNo = aData[0];
            string strChk = strOrderNo.Substring(0, 1);

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                if (strChk.Equals("M"))
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        string strMethod = "getReceiptMemo";

                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        data.Add("RECEIPT_PK", strOrderNo);

                        dt = apiTran.RestApiCallToDataTable(strMethod, "receipt_memo", data);

                        dt.Columns.Add("id");

                        if (dt.Rows.Count == 0 && dt.Columns.IndexOf("RECEIPT_PK") < 0)
                        {
                            dt.Columns.Add("RECEIPT_PK");
                            dt.Columns.Add("CONTENT");
                            dt.Columns.Add("ENROLLMENT_DATE");
                            dt.Columns.Add("WRITER_NAME");
                        }

                        DataRow dr = dt.NewRow();

                        dr["RECEIPT_PK"] = strOrderNo;
                        dr["CONTENT"] = "";
                        dr["ENROLLMENT_DATE"] = "";
                        dr["WRITER_NAME"] = "";
                        dt.Rows.Add(dr);
                        
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dt.Rows[i]["id"] = (i + 1).ToString();
                        }
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 호출 중 오류가 발생했습니다.", "");
                    }

                    result = Utility.DataTableToJson(dt);
                }
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsMemo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERMEMOINSERT";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();
            
            string strOrderNo = aData[0];
            string strChk = strOrderNo.Substring(0, 1);

            string strMemo = aData[1] + " (작성자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add("북부 : " + strMemo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                if (strChk.Equals("M"))
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptMemo";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("MEMO", strMemo);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("03", "VAN 오더 메모 연동 등록 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        private void InsOrderImageFiles()
        {
            Stream stream = null;

            try
            {
                HttpFileCollection uploadFiles = Request.Files;

                string strOrderNo = this.hdfOrderNo.Value;

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];

                    stream = postedFIle.InputStream;

                    if (stream.Length == 0)
                        continue;

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    //type체크 필요 
                    if (strFileType.Contains("image"))
                    {
                        int nFileLen = Convert.ToInt32(stream.Length);

                        byte[] byteFile = new byte[nFileLen];

                        stream.Read(byteFile, 0, nFileLen);

                        using (OrderMgmt mgmt = new OrderMgmt())
                        {
                            mgmt.InsOrderImageFile(strOrderNo, strFileNm, byteFile, strFileType, nFileLen);
                        }
                    }

                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetFileData()
        {

            try
            {
                DataSet dsFile = null;
                bool bImgView = false;

                string strOrderNo = this.hdfOrderNo.Value;

                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    dsFile = mgmt.GetOrderFile(strOrderNo);
                }

                if (dsFile.Tables.Count > 0)
                {
                    rptFile.DataSource = dsFile.Tables[0];
                    rptFile.DataBind();

                    DataTable dtImg = new DataTable();
                    dtImg.Columns.Add("SRC");

                    if (dsFile.Tables[0].Rows.Count > 0)
                    {
                        bImgView = true;

                        for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                        {
                            if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                            {
                                DataRow dr = dtImg.NewRow();

                                string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];

                                dr["SRC"] = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                dtImg.Rows.Add(dr);
                            }
                        }
                    }

                    rptImg.DataSource = dtImg;
                    rptImg.DataBind();
                }

                string strView = "N";

                if (bImgView)
                {
                    strView = "Y";
                }

                updPanelImg.Update();
                updPanelFile.Update();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "fn_SetImgTrView('" + strView + "');", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnImgSave_Click(object sender, EventArgs e)
        {
            try
            {
                InsOrderImageFiles();

                GetFileData();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "alert('파일 추가를 완료 했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnDownload = (Button)sender;
                Label lblSeq = (Label)btnDownload.Parent.FindControl("lblSeq");

                string strOrderNo = this.hdfOrderNo.Value;

                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    mgmt.DelOrderImageFile(strOrderNo, lblSeq.Text);
                }

                GetFileData();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "FileDelete", "alert('파일 삭제를 완료 했습니다.');", true);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }
    }
}