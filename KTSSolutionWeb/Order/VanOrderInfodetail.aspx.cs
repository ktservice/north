﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.Order;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;

namespace KTSSolutionWeb
{
    public partial class VanOrderInfodetail : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        private void PageInit()
        {
            SetDDLRcvType();
            GetVanOrderInfo();
        }
        

        private void SetDDLRcvType() 
        {
            DataSet ds = null;

            try
            {
                ddlRcvType.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "RCVTYPELIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add(""); //파라미터 추가 시
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    ddlRcvType.Items.Add(new ListItem("선택하세요", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlRcvType.Items.Add(new ListItem(ds.Tables[0].Rows[i]["RCVTYPENM"].ToString(), ds.Tables[0].Rows[i]["RCVTYPECD"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("접수구분을 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }


        private void GetVanOrderInfo()
        {
            DataSet ds = null;
            DataSet dsFile = null;

            try
            {
                string strOrderNo = Request.Form["pORDERNO"] == null ? "" : Request.Form["pORDERNO"].ToString();

                if (!strOrderNo.Equals(""))
                {
                    this.hdfOrderNo.Value = strOrderNo;
                    this.hdfStatus.Value = "R";

                    string result = string.Empty;
                    string resultSession = string.Empty;
                    string rptCd = "VANORDERINFO";
                    string jobGb = "GET";

                    List<string> aData = new List<string>();
                    aData.Add(strOrderNo);
                    aData.Add("F"); //파라미터 추가 시

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        ds = dbTransaction.GetDataSet();

                        using (OrderMgmt mgmt = new OrderMgmt())
                        {
                            dsFile = mgmt.GetOrderFile(strOrderNo);
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.ddlRcvType.SelectedValue = ds.Tables[0].Rows[0]["RCVTYPECD"].ToString();
                            this.txbAgencyNm.Text = ds.Tables[0].Rows[0]["AGENCYNM"].ToString();
                            this.txbFranchiseNm.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                            this.txbCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();
                            this.txbAddr.Text = ds.Tables[0].Rows[0]["ADDR"].ToString();
                            this.txbDtlAddr.Text = ds.Tables[0].Rows[0]["DTLADDR"].ToString();
                            this.txbTelNo.Text = ds.Tables[0].Rows[0]["TELNO"].ToString();
                            this.txbPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();
                            this.lblRcvDt.Text = ds.Tables[0].Rows[0]["RCVDT"].ToString();
                            this.ddlPartnersYn.SelectedValue = ds.Tables[0].Rows[0]["PARTNERSYN"].ToString();
                            this.txbRsvDt.Text = ds.Tables[0].Rows[0]["RSVDT"].ToString();
                            try
                            {
                                this.ddlRsvTime.SelectedValue = ds.Tables[0].Rows[0]["RSVTIME"].ToString();
                            }
                            catch
                            {
                                this.ddlRsvTime.SelectedValue = "00";
                            }
                            this.txbHopeDt.Text = ds.Tables[0].Rows[0]["HOPEDT"].ToString();
                            this.txbDlvrDt.Text = ds.Tables[0].Rows[0]["DLVRDT"].ToString();
                            this.txbRcvNote.Text = ds.Tables[0].Rows[0]["RCVNOTE"].ToString();
                            this.txbReqNote.Text = ds.Tables[0].Rows[0]["REQNOTE"].ToString();
                            this.lblWorkState.Text = ds.Tables[0].Rows[0]["WORKSTATENM"].ToString();
                            this.txbRtnAddr.Text = ds.Tables[0].Rows[0]["RTNADDR"].ToString();
                            this.txbRtnDtlAddr.Text = ds.Tables[0].Rows[0]["RTNDTLADDR"].ToString();
                            this.txbReport.Text = ds.Tables[0].Rows[0]["REPORT"].ToString();
                            this.txbEmpUserNm.Text = ds.Tables[0].Rows[0]["FULLWORKEREMPNM"].ToString();
                            this.txbProc.Text = ds.Tables[0].Rows[0]["PROCINFO"].ToString();

                            this.hdfPreEmpNo.Value = ds.Tables[0].Rows[0]["WORKEREMPNO"].ToString();
                            this.hdfEmpNo.Value = ds.Tables[0].Rows[0]["WORKEREMPNO"].ToString();
                            this.hdfEmpNm.Value = ds.Tables[0].Rows[0]["WORKEREMPNM"].ToString();
                            this.hdfOrgCd.Value = ds.Tables[0].Rows[0]["WORKERORGCD"].ToString();

                            string strWorkState = ds.Tables[0].Rows[0]["WORKSTATE"].ToString();

                            this.hdfBelong.Value = ds.Tables[0].Rows[0]["BELONG"].ToString();
                            this.hdfAssignYn.Value = ds.Tables[0].Rows[0]["ASSIGNYN"].ToString();

                            this.ddlRcvType.Enabled = false;
                            this.ddlRcvType.Style.Add("background-color", "#f9f9f9");

                            this.txbAgencyNm.ReadOnly = true;
                            this.txbAgencyNm.Style.Add("border", "none");
                            this.txbAgencyNm.Style.Add("background-color", "#f9f9f9");

                            this.txbFranchiseNm.ReadOnly = true;
                            this.txbFranchiseNm.Style.Add("border", "none");
                            this.txbFranchiseNm.Style.Add("background-color", "#f9f9f9");

                            this.btnCorpInfo.Visible = false;
                            this.btnCorpInfo.Attributes.Remove("onclick");

                            this.txbCorpNo.ReadOnly = true;
                            this.txbCorpNo.Style.Add("border", "none");
                            this.txbCorpNo.Style.Add("background-color", "#f9f9f9");

                            this.txbHopeDt.ReadOnly = true;
                            this.txbHopeDt.Style.Add("border", "none");
                            this.txbHopeDt.Style.Add("background-color", "#f9f9f9");

                            this.txbReport.ReadOnly = true;
                            this.txbReport.Style.Add("border", "none");
                            this.txbReport.Style.Add("background-color", "#f9f9f9");

                            this.trRcvDt.Visible = true;
                            this.trImg.Visible = true;
                            this.trMemo.Visible = true;
                            this.trProc.Visible = false;

                            this.txbAddr.ReadOnly = true;
                            this.txbAddr.Style.Add("border", "none");
                            this.txbAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbDtlAddr.ReadOnly = true;
                            this.txbDtlAddr.Style.Add("border", "none");
                            this.txbDtlAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbTelNo.ReadOnly = true;
                            this.txbTelNo.Style.Add("border", "none");
                            this.txbTelNo.Style.Add("background-color", "#f9f9f9");

                            this.txbPhoneNo.ReadOnly = true;
                            this.txbPhoneNo.Style.Add("border", "none");
                            this.txbPhoneNo.Style.Add("background-color", "#f9f9f9");

                            this.txbDlvrDt.ReadOnly = true;
                            this.txbDlvrDt.Style.Add("border", "none");
                            this.txbDlvrDt.Style.Add("background-color", "#f9f9f9");

                            this.txbRcvNote.ReadOnly = true;
                            this.txbRcvNote.Style.Add("border", "none");
                            this.txbRcvNote.Style.Add("background-color", "#f9f9f9");

                            this.txbReqNote.ReadOnly = true;
                            this.txbReqNote.Style.Add("border", "none");
                            this.txbReqNote.Style.Add("background-color", "#f9f9f9");

                            this.txbRtnAddr.ReadOnly = true;
                            this.txbRtnAddr.Style.Add("border", "none");
                            this.txbRtnAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbRtnDtlAddr.ReadOnly = true;
                            this.txbRtnDtlAddr.Style.Add("border", "none");
                            this.txbRtnDtlAddr.Style.Add("background-color", "#f9f9f9");

                            this.txbDlvrDt.Attributes.Remove("class");
                            this.txbDlvrDt.Style.Add("border", "none");
                            this.txbDlvrDt.Style.Add("background-color", "#f9f9f9");

                            if (!strWorkState.Equals("4") && !strWorkState.Equals("6"))
                            {
                                //this.txbRsvDt.Enabled = true;
                                this.ddlRsvTime.Enabled = true;
                                this.txbEmpUserNm.Attributes.Add("onclick", "PopupEmpUser();");

                            }
                            else
                            {

                                this.txbRsvDt.ReadOnly = true;
                                this.txbRsvDt.Style.Add("border", "none");
                                this.txbRsvDt.Style.Add("background-color", "#f9f9f9");
                                this.ddlRsvTime.Enabled = false;
                                this.ddlRsvTime.Style.Add("background-color", "#f9f9f9");
                                this.ddlPartnersYn.Enabled = false;
                                this.ddlPartnersYn.Style.Add("background-color", "#f9f9f9");

                                this.txbRsvDt.Attributes.Remove("class");

                                this.txbEmpUserNm.Style.Add("border", "none");
                                this.txbEmpUserNm.Style.Add("background-color", "#f9f9f9");
                                this.txbEmpUserNm.Attributes.Remove("onclick");


                                if (strWorkState.Equals("4"))
                                {
                                    this.trProc.Visible = true;
                                    this.txbProc.ReadOnly = true;
                                    this.txbProc.Style.Add("border", "none");
                                    this.txbProc.Style.Add("background-color", "#f9f9f9");
                                }
;
                            }

                            //if (strOrderNo.Substring(0, 1).Equals("M"))
                            //{

                            //}
                            //else
                            //{
                            //    this.btnDel.Visible = false;
                            //    this.btnDel.Attributes.Remove("onclick");
                            //}

                            if (dsFile.Tables.Count > 0)
                            {
                                if (dsFile.Tables[0].Rows.Count > 0)
                                {

                                    for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                    {
                                        if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                        {
                                            HtmlImage ImgBoard = new HtmlImage();

                                            string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                            byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                            ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                            ImgBoard.Attributes.Add("style", "max-width:100%;height:auto;");

                                            spanImg.Controls.Add(ImgBoard);
                                            spanImg.Controls.Add(new LiteralControl("<br/>"));
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');window.close();", true);
                        }
                    }
                    else //세션이 끊겼을 경우
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('세션이 만료되었습니다.');window.close();", true);
                    }
                }
                else
                {
                    this.hdfStatus.Value = "N";

                    string strEmpNo = Session["EMPNO"].ToString();
                    string strNow = DateTime.Now.ToString("yyyyMMddHHmmss");
                    this.hdfOrderNo.Value = "M" + strNow + strEmpNo;

                    this.lblWorkState.Text = "작업자지정";

                    this.ddlRcvType.Enabled = true;
                    this.txbAgencyNm.Enabled = true;
                    this.txbFranchiseNm.Enabled = true;
                    this.btnCorpInfo.Visible = true;
                    this.btnCorpInfo.Attributes.Add("onclick", "PopupCorpInfo();");
                    this.txbCorpNo.Enabled = true;
                    this.txbAddr.Enabled = true;
                    this.txbDtlAddr.Enabled = true;
                    this.txbTelNo.Enabled = true;
                    this.txbPhoneNo.Enabled = true;
                    this.trRcvDt.Visible = false;
                    this.txbRsvDt.Enabled = true;
                    this.ddlRsvTime.Enabled = true;
                    this.txbHopeDt.Enabled = true;
                    this.txbDlvrDt.Enabled = true;
                    this.txbRcvNote.Enabled = true;
                    this.txbReqNote.Enabled = true;
                    this.txbRtnAddr.Enabled = true;
                    this.txbRtnDtlAddr.Enabled = true;
                    this.txbReport.Enabled = true;
                    this.trImg.Visible = false;
                    this.trMemo.Visible = false;
                    this.trProc.Visible = false;


                    this.txbHopeDt.Attributes.Add("class", "date");
                    this.txbDlvrDt.Attributes.Add("class", "date");

                    this.txbEmpUserNm.Attributes.Add("onclick", "PopupEmpUser();");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetWorkerNm(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;

            string strOrgCd = aData[0];
            string strEmpNm = aData[1];

            using (UserMgmt userReg = new UserMgmt())
            {
                ds = userReg.GetOrgInfo(strOrgCd);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        result = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString() + " " + strEmpNm;
                    }
                }
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFOINSERT";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string UpdVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFOUPDATE";
            string jobGb = "SET";

            string strOrderNo = aData[0];
            string strRsvDt = aData[2];
            string strWorkerEmpno = aData[4];
            string strPreWorkerEmpno = aData[5];
            string strBelong = aData[6];
            string strPreRsvDt = aData[7];
            string strAssignYn = aData[8];

            aData.RemoveAt(8);
            aData.RemoveAt(7);
            aData.RemoveAt(6);
            aData.RemoveAt(5);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    if (!strRsvDt.Equals(strPreRsvDt))
                    {
                        DataTable dt = new DataTable();

                        try
                        {
                            ApiTransaction apiTran = new ApiTransaction();
                            NameValueCollection data = new NameValueCollection();

                            string strMethod = "setRequestDateUp";

                            data.Add("RECEIPT_PK", strOrderNo);
                            data.Add("REQUEST_DATE", strRsvDt);

                            dt = apiTran.RestApiCallToInsert(strMethod, data);
                            result = Utility.DataTableToJson(dt);
                        }
                        catch
                        {
                            dt = Utility.DataTableError("01", "VAN 오더 방문예정일 변경 연동 중 오류가 발생했습니다.", "");
                            result = Utility.DataTableToJson(dt);
                        }
                    }

                    if (!strAssignYn.Equals("Y") && !strPreWorkerEmpno.Equals(strWorkerEmpno))
                    {
                        DataTable dt = new DataTable();

                        try
                        {
                            string strKVanEmpNo = "";

                            if (strBelong.Length > 0)
                            {
                                if (strBelong.Equals("강북"))
                                {
                                    strKVanEmpNo = "1111";
                                }
                                else if (strBelong.Equals("강남"))
                                {
                                    strKVanEmpNo = "2222";
                                }
                                else if (strBelong.Equals("서부"))
                                {
                                    strKVanEmpNo = "3333";
                                }
                                else
                                {
                                    strKVanEmpNo = "1111";
                                }
                            }

                            if (strBelong.Length > 0 && strKVanEmpNo.Length > 0)
                            {
                                ApiTransaction apiTran = new ApiTransaction();
                                NameValueCollection data = new NameValueCollection();

                                string strMethod = "setReceiptStateA";

                                data.Add("RECEIPT_PK", strOrderNo);
                                data.Add("EMPLOYEE_NUMBER", strKVanEmpNo);
                                dt = apiTran.RestApiCallToInsert(strMethod, data);
                                result = Utility.DataTableToJson(dt);

                                string strOrgCd = HttpContext.Current.Session["ORGCD"].ToString();
                                string strEmpNm = HttpContext.Current.Session["EMPNM"].ToString();

                                using (UserMgmt userReg = new UserMgmt())
                                {

                                    DataSet ds = userReg.GetOrgInfo(strOrgCd);

                                    if (ds.Tables.Count > 0)
                                    {
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            strEmpNm = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString() + " " + strEmpNm;
                                        }
                                    }
                                }

                                string strReason = "엔지니어배정완료 (작업자 : " + strEmpNm + ")";
                                apiTran = new ApiTransaction();

                                data = new NameValueCollection();

                                strMethod = "setReceiptMemo";

                                data.Add("RECEIPT_PK", strOrderNo);
                                data.Add("MEMO", strReason);

                                dt = apiTran.RestApiCallToInsert(strMethod, data);
                                result = Utility.DataTableToJson(dt);
                            }
                        }
                        catch
                        {
                            dt = Utility.DataTableError("01", "VAN 오더 배정처리 연동 중 오류가 발생했습니다.", "");
                            result = Utility.DataTableToJson(dt);
                        }
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFODELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string CancelVanOrderInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERINFOCANCEL";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();

            string strOrderNo = aData[0];
            string strReason = "(취소)" + aData[1] + " (작업자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add("북부 : " + strReason);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();

                JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                if (errcd.Equals("00") && !strOrderNo.Substring(0, 1).Equals("M"))
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptCancel";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("CONTENT", strReason);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("01", "VAN 오더 취소처리 연동 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetMemoData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERMEMOLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            string strOrderNo = aData[0];
            string strChk = strOrderNo.Substring(0, 1);

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                if (strChk.Equals("M"))
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        string strMethod = "getReceiptMemo";

                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        data.Add("RECEIPT_PK", strOrderNo);

                        dt = apiTran.RestApiCallToDataTable(strMethod, "receipt_memo", data);

                        dt.Columns.Add("id");

                        if (dt.Rows.Count == 0 && dt.Columns.IndexOf("RECEIPT_PK") < 0)
                        {
                            dt.Columns.Add("RECEIPT_PK");
                            dt.Columns.Add("CONTENT");
                            dt.Columns.Add("ENROLLMENT_DATE");
                            dt.Columns.Add("WRITER_NAME");
                        }

                        DataRow dr = dt.NewRow();

                        dr["RECEIPT_PK"] = strOrderNo;
                        dr["CONTENT"] = "";
                        dr["ENROLLMENT_DATE"] = "";
                        dr["WRITER_NAME"] = "";
                        dt.Rows.Add(dr);
                        
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dt.Rows[i]["id"] = (i + 1).ToString();
                        }
                    }
                    catch
                    {
                        dt = Utility.DataTableError("01", "VAN 오더 호출 중 오류가 발생했습니다.", "");
                    }

                    result = Utility.DataTableToJson(dt);
                }
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsMemo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERMEMOINSERT";
            string jobGb = "SET";

            string empNm = HttpContext.Current.Session["EMPNM"].ToString();

            List<string> aList = new List<string>();
            
            string strOrderNo = aData[0];
            string strChk = strOrderNo.Substring(0, 1);

            string strMemo = aData[1] + " (작성자 : " + empNm + ")";

            aList.Add(strOrderNo);
            aList.Add("북부 : " + strMemo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aList);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                if (strChk.Equals("M"))
                {
                    result = dbTransaction.GetJsonData();
                }
                else
                {
                    DataTable dt = new DataTable();

                    try
                    {
                        ApiTransaction apiTran = new ApiTransaction();

                        NameValueCollection data = new NameValueCollection();

                        string strMethod = "setReceiptMemo";

                        data.Add("RECEIPT_PK", strOrderNo);
                        data.Add("MEMO", strMemo);

                        dt = apiTran.RestApiCallToInsert(strMethod, data);
                        result = Utility.DataTableToJson(dt);
                    }
                    catch
                    {
                        dt = Utility.DataTableError("01", "VAN 오더 메모 연동 등록 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
    }
}