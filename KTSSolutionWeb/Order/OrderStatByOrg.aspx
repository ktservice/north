﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderStatByOrg.aspx.cs" Inherits="KTSSolutionWeb.OrderStatByOrg" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />    
    
    <script type="text/javascript">

        $(document).ready(function () {
            $("#<%= txbStDt.ClientID %>").change(function () {
                $("#<%= txbStDt.ClientID %>").val($("#<%= txbStDt.ClientID %>").val());
                __doPostBack("<%=hfSumupDt.ClientID %>", "");
            });

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일자", field: "SUMUPDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자소속", field: "WORKERORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자사번", field: "WORKEREMPNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자명", field: "WORKEREMPNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "오더구분", field: "ORDERNAME", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "업무구분", field: "WORKTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "오더건수", field: "TOTALCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "성공건수", field: "SUCCCNT", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "실패건수", field: "FAILCNT", sorter: "number" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            var grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("empField");
            var ValueEl = document.getElementById("empValue");

            $("#empField").change(function (e) {
                ValueEl.value = '';
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $("#empValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {

                if (SearchChk()) {
                    //입력 파라미터
                    var aData = [];
                    aData[0] = $("#<%= hfOrgCd.ClientID %>").val();
                    aData[1] = $("#<%= txbStDt.ClientID %>").val();
                    aData[2] = $("#<%= txbEnDt.ClientID %>").val();
                    aData[3] = $("#<%= ddlOrderCode.ClientID %> option:selected").val();
                    
                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetData');

                    //grid Bind
                    grid.getDataBind(jsonData, ajaxUrl);

                    $(".table-search").css('display', 'block');
                }
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "오더 현황 통계.xlsx", { sheetName: "오더 현황 통계" });
            });

            $(".table-search").css('display', 'none');
        });

        function PopupOrgTree(orgcd, empno) {
            var BaseMonth = $("#<%= txbStDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("등록일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 720;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: BaseMonth
                };

                var form = "/Common/OrgTree_Terminal";
                var target = "OrgTree_Terminal";

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        function SetOrgCode(orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        function SearchChk() {
            var Stdt = $("#<%= txbStDt.ClientID %>").val();
            var Endt = $("#<%= txbEnDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (Stdt.length == 0 || Endt.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                alert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }
    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox">
					<label>등록일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" style="width:34px;height:34px;" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged" />
                            <asp:HiddenField ID="hfSumupDt" runat="server" OnValueChanged="hfSumupDt_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfSumupDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
				<span class="optionbox" style="margin-right:20px">
					<label>오더구분</label> 
                    <asp:DropDownList ID="ddlOrderCode" style="width:100px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="VAN" Value="1"></asp:ListItem>
                        <asp:ListItem Text="K-VIZERO" Value="2"></asp:ListItem>
                        <asp:ListItem Text="BIZ" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Gem" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>오더 현황</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="empField" style="font-weight:bold">
                            <option value="WORKEREMPNO">작업자사번</option>
                            <option value="WORKEREMPNM">작업자</option>
                        </select>
                        <label for="empValue">: </label>
                        <input id="empValue" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
