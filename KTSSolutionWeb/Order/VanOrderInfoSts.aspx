﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VanOrderInfoSts.aspx.cs" Inherits="KTSSolutionWeb.VanOrderInfoSts" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />    
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .tabulator-row > div {
            height:32px !important;
            padding-top:7px !important;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbEnDt.ClientID %>").change(function (e) {
                    fn_GetTeam();
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnVAN").click(function (e) {
                    fn_PopupKVANInfo();
                });

                $("#btnReg").click(function (e) {
                    fn_PopupRegVanOrder('', 'KVAN');
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.showColumn("REGORGNM");
                    ktsGrid.showColumn("REGEMPNO");
                    ktsGrid.showColumn("DLVRDT");
                    ktsGrid.showColumn("RCVNOTE");
                    ktsGrid.showColumn("REQNOTE");
                    ktsGrid.showColumn("RTNADDR");
                    ktsGrid.showColumn("RTNDTLADDR");
                    ktsGrid.showColumn("REPORT");
                    ktsGrid.showColumn("PROCINFO");
                    ktsGrid.showColumn("ACCEPTDT");
                    ktsGrid.showColumn("SUCCDT");
                    ktsGrid.showColumn("EQUIPMENT");
                    ktsGrid.showColumn("DEVICE");
                    ktsGrid.showColumn("POLICY_BASIC_CONTENT");
                    ktsGrid.showColumn("POLICY_EXTRA_CONTENT");
                    ktsGrid.showColumn("SW_CONTENT");
                    ktsGrid.showColumn("SW_SETTING");
                    ktsGrid.showColumn("van");
                    ktsGrid.showColumn("PM_NM");
                    ktsGrid.showColumn("ASP_NAME");
                    ktsGrid.showColumn("ASP_ID");
                    ktsGrid.showColumn("ASP_PW");
                    ktsGrid.showColumn("SOLUTION_NAME");
                    ktsGrid.showColumn("SOLUTION_ID");
                    ktsGrid.showColumn("SOLUTION_PW");
                    ktsGrid.showColumn("TERMINAL_YN");
                    ktsGrid.showColumn("SW_SETTINGHOST");
                    ktsGrid.showColumn("TERMINAL_ADDR_JIJUM");
                    ktsGrid.showColumn("TERMINAL_ADDRESS");
                    ktsGrid.showColumn("DISPATCH_TYPE");
                    ktsGrid.showColumn("DAYWEEK_TYPE");
                    ktsGrid.hideColumn("CONF_DOWNLOAD");

                    ktsGrid.download("xlsx", "VAN오더현황.xlsx", { sheetName: "VAN오더현황" });

                    ktsGrid.hideColumn("REGORGNM");
                    ktsGrid.hideColumn("REGEMPNO");
                    ktsGrid.hideColumn("DLVRDT");
                    ktsGrid.hideColumn("RCVNOTE");
                    ktsGrid.hideColumn("REQNOTE");
                    ktsGrid.hideColumn("RTNADDR");
                    ktsGrid.hideColumn("RTNDTLADDR");
                    ktsGrid.hideColumn("REPORT");
                    ktsGrid.hideColumn("PROCINFO");
                    ktsGrid.hideColumn("ACCEPTDT");
                    ktsGrid.hideColumn("SUCCDT");
                    ktsGrid.hideColumn("EQUIPMENT");
                    ktsGrid.hideColumn("DEVICE");
                    ktsGrid.hideColumn("POLICY_BASIC_CONTENT");
                    ktsGrid.hideColumn("POLICY_EXTRA_CONTENT");
                    ktsGrid.hideColumn("SW_CONTENT");
                    ktsGrid.hideColumn("SW_SETTING");
                    ktsGrid.hideColumn("van");
                    ktsGrid.hideColumn("PM_NM");
                    ktsGrid.hideColumn("ASP_NAME");
                    ktsGrid.hideColumn("ASP_ID");
                    ktsGrid.hideColumn("ASP_PW");
                    ktsGrid.hideColumn("SOLUTION_NAME");
                    ktsGrid.hideColumn("SOLUTION_ID");
                    ktsGrid.hideColumn("SOLUTION_PW");
                    ktsGrid.hideColumn("TERMINAL_YN");
                    ktsGrid.hideColumn("SW_SETTINGHOST");
                    ktsGrid.hideColumn("TERMINAL_ADDR_JIJUM");
                    ktsGrid.hideColumn("TERMINAL_ADDRESS");
                    ktsGrid.hideColumn("DISPATCH_TYPE");
                    ktsGrid.hideColumn("DAYWEEK_TYPE");
                    ktsGrid.showColumn("CONF_DOWNLOAD");
                });
            }
        };

        var fn_GridInit = function () {

            var fn_ConfDownload = function (cell) {
                var rowData = cell.getRow().getData();

                var orderNo = rowData.ORDERNO;
                var confYN = rowData.CONFYN;

                var input = "";

                if (confYN == "Y") {

                    input = "<input id='btnConfPrint' type='button' onclick='fn_PopupConfirm(\"" + orderNo + "\");' value='설치확인서' class='btn-save' style='width:120px;height:25px;font-size:15px;line-height:0px;margin-top:-3px;'>";
                }

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80},
                { headerHozAlign: "center", hozAlign: "center", title: "등록일자", field: "SUMUPDATE", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RCVDT", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수구분", field: "RECEIPT_TYPE", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "출동희망일", field: "HOPEDT", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "방문예정일", field: "RSVDT", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "완료일시", field: "SUCCDT", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "오더상태", field: "WORKSTATENM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "left", title: "작업자", field: "FULLWORKEREMPNM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "REGORGNM", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "등록자사번", field: "REGEMPNO", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "등록자명", field: "REGEMPNM", sorter: "string", minWidth: 80, maxWidth: 400},
                { headerHozAlign: "center", hozAlign: "center", title: "접수유형", field: "RCVTYPENM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "파트너사지역", field: "PARTNERSYN", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "대리점명", field: "AGENCYNM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "사업자이름", field: "FRANCHISENM", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "사업자등록번호", field: "CORPNO", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "left", title: "주소", field: "ADDR", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "left", title: "상세주소", field: "DTLADDR", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "전화번호", field: "TELNO", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "휴대폰번호", field: "PHONENO", sorter: "string", minWidth: 80, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "설치확인서", field: "CONF_DOWNLOAD", width: 140, formatter: fn_ConfDownload},

                { headerHozAlign: "center", hozAlign: "center", title: "택배배송일", field: "DLVRDT", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수내용", field: "RCVNOTE", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "의뢰사항", field: "REQNOTE", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "배송회수주소", field: "RTNADDR", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "배송회수상세주소", field: "RTNDTLADDR", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "특이사항", field: "REPORT", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },

                { headerHozAlign: "center", hozAlign: "left", title: "처리내역", field: "PROCINFO", sorter: "string", minWidth: 80, maxWidth: 400, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "수령일시", field: "ACCEPTDT", sorter: "string", minWidth: 80, maxWidth: 400, visible: false},
     

                { headerHozAlign: "center", hozAlign: "center", title: "관련장비", field: "EQUIPMENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "장비구분", field: "DEVICE", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "기본견적", field: "POLICY_BASIC_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "추가견적", field: "POLICY_EXTRA_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용SW", field: "SW_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "SW세팅여부", field: "SW_SETTING", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "VAN", field: "van", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "단말기&CAT_ID", field: "PM_NM", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "ASP계정이름", field: "ASP_NAME", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "ASP ID", field: "ASP_ID", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "ASP PW", field: "ASP_PW", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "솔루션계정이름", field: "SOLUTION_NAME", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "솔루션 ID", field: "SOLUTION_ID", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "솔루션 PW", field: "SOLUTION_PW", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "단말기보유여부", field: "TERMINAL_YN", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "SW세팅주체", field: "SW_SETTINGHOST", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "단말기발송장소", field: "TERMINAL_ADDR_JIJUM", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점발송주소", field: "TERMINAL_ADDRESS", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "출동종류", field: "DISPATCH_TYPE", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "주말여부", field: "DAYWEEK_TYPE", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },

                { headerHozAlign: "center", hozAlign: "center", title: "오더코드", field: "ORDERCODE", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "오더현황코드", field: "WORKSTATE", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "오더번호", field: "ORDERNO", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자", field: "WORKEREMPNM", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "확인서저장여부", field: "CONFYN", sorter: "string", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            var rowDoubleClick = function (e, row) {
                var rowData = row.getData();
                var orderno = rowData.ORDERNO;
                var receipttype = rowData.RECEIPT_TYPE;

                fn_PopupRegVanOrder(orderno, receipttype);
            };

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("sField");
            var ValueEl1 = document.getElementById("sValue1");
            var ValueEl2 = document.getElementById("sValue2");
            var ValueEl3 = document.getElementById("sValue3");

            $("#sField").change(function (e) {
                ValueEl1.value = '';
                ValueEl2.value = '';
                ValueEl3.value = '';

                ktsGrid.setFilter(FieldEl.value, 'like', '');

                if (FieldEl.value == "WORKSTATENM") {
                    $("#sValue1").hide();
                    $("#sValue2").show();
                    $("#sValue3").hide();
                }
                else if (FieldEl.value == "RECEIPT_TYPE") {
                    $("#sValue1").hide();
                    $("#sValue2").hide();
                    $("#sValue3").show();
                }
                else {
                    $("#sValue1").show();
                    $("#sValue2").hide();
                    $("#sValue3").hide();
                }
            });

            $("#sValue1").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl1.value);
            });

            $("#sValue2").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl2.value);
            });

            $("#sValue3").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl3.value);
            });

            $(".table-search").css('display', 'none');
        };

        var fn_Load = function () {
        
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                var org = json.split('|');

                if (org.length > 1) {
                    $("#<%= hfOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);
                }
            }

            callGb = '';
        };

        var fn_GetTeam = function () {
            var EnDate = $("#<%= txbEnDt.ClientID %>").val();

            if (EnDate != '') {
                var BaseMonth = EnDate.replace("-", "").substring(0, 6);

                var aData = [];
                aData[0] = OrgCd;
                aData[1] = BaseMonth;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                callGb = 'GetTeam';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
            }
        };

        var fn_Search = function () {
            var StDate = $("#<%= txbStDt.ClientID %>").val();
            var EnDate = $("#<%= txbEnDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (StDate.length == 0 || EnDate.length == 0) {
                alert("등록일자를 선택해주세요.");
            }
            if (OrgCd.length == 0) {
                alert("조회할 조직을 선택해 주세요.");
            } else {
                var RcvType = $("#<%= ddlRcvType.ClientID %> option:selected").val();
                var WorkState = $("#<%= ddlWorkState.ClientID %> option:selected").val();
                var SearchType = $("#<%= ddlSearchType.ClientID %> option:selected").val();

                //입력 파라미터
                var aData = [];
                aData[0] = OrgCd.replace(/,/gi, "^");;
                aData[1] = StDate;
                aData[2] = EnDate;
                aData[3] = RcvType;
                aData[4] = WorkState;
                aData[5] = SearchType;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        };

        var fn_PopupOrgTree = function (orgcd, empno) {
            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("등록일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 720;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: BaseMonth
                };

                var form = "/Common/OrgTree_Terminal";
                var target = "OrgTree_Terminal";

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        };

        var fn_PopupRegVanOrder = function (orderno, receipttype) {
            var form;

            form = "/Order/RegVanOrderInfo";
            var target = "VanOrderInfo";

            var param = { pORDERNO: orderno };

            var nWidth = 880;
            var nHeight = 900;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_PopupKVANInfo = function () {
            var form = "/Order/KVANOrderInfo";
            var target = "KVANOrderInfo";

            var param = {};

            var nWidth = 1000;
            var nHeight = 700;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var fn_PopupConfirm = function(orderNo) {

            var form = "/Order/VanConfirmation_Print";
            var target = "VanConfirm";

            var param = {
                pORDERNO: orderNo
            };

            var nWidth = 793.7;
            var nHeight = 1096;

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var SetOrgCode = function (orgcd) {
            this.focus();

            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var EnDate = $("#<%= txbEnDt.ClientID %>").val();
            var BaseMonth = EnDate.replace("-", "").substring(0, 6);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = BaseMonth;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        };
    </script>

    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
				<span class="optionbox first">
                    <asp:DropDownList ID="ddlSearchType" style="width:100px;font-weight:bold;" runat="server">
                        <asp:ListItem Text="등록일자" Value="SUMUPDATE" Selected="True" style="font-weight: bold"></asp:ListItem>
                        <asp:ListItem Text="완료일자" Value="SUCCDT" style="font-weight: bold"></asp:ListItem>
                        <asp:ListItem Text="출동희망일" Value="HOPEDT" style="font-weight: bold"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="inpbox ">
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
				    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfOrgCd" runat="server" />
				</span>
				<span class="optionbox" style="margin-right:20px">
					<label>접수유형</label> 
                    <asp:DropDownList ID="ddlRcvType" style="width:100px;" runat="server">
                    </asp:DropDownList>
                </span>
				<span class="optionbox" style="margin-right:20px">
					<label>오더현황</label> 
                    <asp:DropDownList ID="ddlWorkState" style="width:110px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="작업자지정" Value="1"></asp:ListItem>
                        <asp:ListItem Text="수령대기" Value="2"></asp:ListItem>
                        <asp:ListItem Text="출동" Value="3"></asp:ListItem>
                        <asp:ListItem Text="작업시작" Value="3.5"></asp:ListItem>
                        <asp:ListItem Text="작업완료" Value="4"></asp:ListItem>
                        <asp:ListItem Text="작업보류" Value="5"></asp:ListItem>
                        <asp:ListItem Text="취소" Value="6"></asp:ListItem>
                        <asp:ListItem Text="수령거부" Value="7"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>오더 현황</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <button id="btnVAN" type="button" class="btn-green">VAN연동</button>
                        <button id="btnReg" type="button" class="btn-green">등록</button>
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="sField" style="font-weight:bold">
                            <option value="SUMUPDATE">등록일자</option>
                            <option value="RECEIPT_TYPE">접수구분</option>
                            <option value="RSVDT">방문예정일</option>
                            <option value="WORKSTATENM">오더상태</option>
                            <option value="FULLWORKEREMPNM">작업자</option>
                            <option value="AGENCYNM">대리점</option>
                            <option value="FRANCHISENM">사업자이름</option>
                            <option value="CORPNO">사업자등록번호</option>
                        </select>
                         :
                        <input id="sValue1" type="text" />
                        <select id="sValue2" style="display:none;font-weight:bold;">
                            <option value="">전체</option>
                            <option value="작업자지정">작업자지정</option>
                            <option value="수령대기">수령대기</option>
                            <option value="출동">출동</option>
                            <option value="작업시작">작업시작</option>
                            <option value="작업완료">작업완료</option>
                            <option value="작업보류">작업보류</option>
                            <option value="취소">취소</option>
                            <option value="수령거부">수령거부</option>
                        </select>
                        <select id="sValue3" style="display:none;font-weight:bold;">
                            <option value="">전체</option>
                            <option value="KVAN">KVAN</option>
                            <option value="VAN365">VAN365</option>
                        </select>
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
