﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;

namespace KTSSolutionWeb
{
    public partial class KVANOrderInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    this.txbStDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }
        }

        private static string GetApiOrder(List<string> aData, string strMethod, string strType)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "VANORDERCHKINFO";
            string jobGb = "GET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, null);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = new DataSet();

                DataTable dt = new DataTable();
                DataTable Ndt = null;

                try
                {
                    ApiTransaction apiTran = new ApiTransaction();

                    NameValueCollection data = new NameValueCollection();
                    string OrgCd = HttpContext.Current.Session["ORGFULLNM"].ToString();

                    data.Add("START_DATE", aData[0]);
                    data.Add("END_DATE", aData[1]);

                    //TEST용
                    //if (strType.Equals("KVAN"))
                    //{
                    //    data.Add("STATE", "확인");
                    //}
                    //else
                    //{
                    //    data.Add("STATE", "접수");
                    //}
                    data.Add("STATE", "확인");
                    
                    if (OrgCd.Contains("강북"))
                    {
                        data.Add("BELONG", "강북");
                    }
                    else if (OrgCd.Contains("강남"))
                    {
                        data.Add("BELONG", "강남");
                    }
                    else if (OrgCd.Contains("서부"))
                    {
                        data.Add("BELONG", "서부");
                    }
                    else
                    {
                        data.Add("BELONG", "");
                    }

                    dt = apiTran.RestApiCallToDataTable(strMethod, "receipt", data);

                    string strChkOrderNo = "";

                    foreach (DataRow dr in dt.Rows)
                    {
                        strChkOrderNo = strChkOrderNo + dr["RECEIPT_PK"] + "^";
                    }

                    if (strChkOrderNo.Length > 0)
                    {
                        List<string> list = new List<string>();

                        list.Add(strChkOrderNo);

                        dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, list);
                        resultSession = dbTransaction.SessionCheck();

                        ds = dbTransaction.GetDataSet();
                    }

                    Ndt = dt.Clone();

                    Ndt.Columns.Add("id");
                    Ndt.Columns.Add("RECEIPT_TYPE");

                    string[] strColNm = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    int row = 0;

                    foreach (DataRow dr in dt.Rows)
                    {
                        string strWhere = string.Format("ORDERNO = '{0}'", dr["RECEIPT_PK"]);
                        DataRow[] drChk = ds.Tables[0].Select(strWhere);

                        if (drChk.Length == 0)
                        {
                            row++;
                            DataRow Ndr = Ndt.NewRow();

                            for (int i = 0; i < strColNm.Length; i++)
                            {
                                Ndr[strColNm[i]] = dr[strColNm[i]];
                            }

                            Ndr["id"] = row.ToString();
                            Ndr["RECEIPT_TYPE"] = strType;

                            Ndt.Rows.Add(Ndr);
                        }
                    }
                }
                catch
                {
                    Ndt = Utility.DataTableError("01", strType + " 오더 호출 중 오류가 발생했습니다.", "");
                }

                result = Utility.DataTableToJson(Ndt);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetApiVANOrder(List<string> aData)
        {
            string result = string.Empty;

            string strMethod = "getReceiptNorthern";

            result = GetApiOrder(aData, strMethod, "KVAN");

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetApiVAN365Order(List<string> aData)
        {
            string result = string.Empty;

            string strMethod = "getReceiptNorthernVan365";

            result = GetApiOrder(aData, strMethod, "VAN365");

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetApiVANNote(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = string.Empty;
            string jobGb = "GET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            string strMethod = "getReceiptMemo";

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataTable dt = new DataTable();

                try
                {
                    ApiTransaction apiTran = new ApiTransaction();

                    NameValueCollection data = new NameValueCollection();

                    data.Add("RECEIPT_PK", aData[0]);

                    dt = apiTran.RestApiCallToDataTable(strMethod, "receipt_memo", data);
                }
                catch
                {
                    dt = Utility.DataTableError("01", "VAN 오더 호출 중 오류가 발생했습니다.", "");
                }

                result = Utility.DataTableToJson(dt);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string LinkKVANOrder(List<List<string>> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = string.Empty;
            string jobGb = "SET";
            string strMethod = "getReceiptMemo";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, null);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상{
            {
                string empNo = HttpContext.Current.Session["EMPNO"].ToString();

                string strChkOrderNo = "";
                bool bErr = false;

                foreach (List<string> list in aData)
                {
                    if (bErr)
                    {
                        break;
                    }

                    strChkOrderNo = strChkOrderNo + list[0] + "^";

                    rptCd = "VANORDERINFOLINK";
                    list.Add(empNo);

                    dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, list);
                    resultSession = dbTransaction.SessionCheck();
                    result = dbTransaction.GetJsonData();
                    
                    JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                    string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                    if (errcd.Equals("01"))
                    {
                        bErr = true;
                        break;
                    }
                    /* kvan 오더 메모는 저장안하도록 변경 */
                    //else
                    //{
                    //    DataTable dt = new DataTable();

                    //    try
                    //    {
                    //        ApiTransaction apiTran = new ApiTransaction();

                    //        NameValueCollection data = new NameValueCollection();

                    //        data.Add("RECEIPT_PK", list[0]);

                    //        dt = apiTran.RestApiCallToDataTable(strMethod, "receipt_memo", data);

                    //        foreach (DataRow dr in dt.Rows)
                    //        {   
                    //            string strContent = dr["CONTENT"].ToString();
                    //            string strEnrollDt = dr["ENROLLMENT_DATE"].ToString();
                    //            string strWriter = dr["WRITER_NAME"].ToString();

                    //            List<string> note = new List<string>();

                    //            note.Add(list[0]);
                    //            note.Add(strContent);
                    //            note.Add(strEnrollDt);
                    //            note.Add(strWriter);

                    //            rptCd = "VANORDERMEMOLINK";

                    //            dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, note);
                    //            result = dbTransaction.GetJsonData();

                    //            jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                    //            errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                    //            if (errcd.Equals("01"))
                    //            {
                    //                bErr = true;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    catch
                    //    {
                    //        bErr = true;
                    //        break;
                    //    }
                    //}
                }

                if (bErr)
                {
                    if (strChkOrderNo.Length > 0)
                    {
                        List<string> list = new List<string>();
                        list.Add(strChkOrderNo);

                        rptCd = "VANORDERINFODELETE";

                        dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, list);
                        resultSession = dbTransaction.SessionCheck();
                        result = dbTransaction.GetJsonData();

                        DataTable dt = Utility.DataTableError("01", "VAN 오더 연동 중 오류가 발생했습니다.", "");
                        result = Utility.DataTableToJson(dt);
                    }
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
    }
}