﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegOrderInfo.aspx.cs" Inherits="KTSSolutionWeb.RegOrderInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>오더정보 등록/수정</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    
    <style>
        .td-button {
            width: 20px !important;
            padding:0px !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $("input.date").datepicker({
                    dateFormat: "yy-mm-dd",
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    showOtherMonths: false,
                    selectOtherMonths: false,
                    constrainInput: false,
                    prevText: "<",
                    nextText: ">",
                });
            }

            $('.ddlOrderCode').change(function () {
                var odrcd = $('.ddlOrderCode').val();

                if (odrcd == "1" || odrcd == "2") {
                    $('#thName').html('업체명&nbsp;<span style="color: red;">*</span>');
                    $('#thNum').html('사업자등록번호&nbsp;<span style="color: red; ">*</span>');
                }
                else if (odrcd == "3" || odrcd == "4") {
                    $('#thName').html('관리번호&nbsp;<span style="color: red;">*</span>');
                    $('#thNum').html('접속번호&nbsp;<span style="color: red; ">*</span>');
                }
                else {
                    $('#thName').html('이름&nbsp;<span style="color: red;">*</span>');
                    $('#thNum').html('번호&nbsp;<span style="color: red; ">*</span>');
                }
                
                if (odrcd == "3") {
                    $('#thPhone').html('장소명');
                    $('#thEquipment').html('모뎀위치&nbsp;<span style="color: red; ">*</span>');
                }
                else {
                    $('#thPhone').html('연락처');
                    $('#thEquipment').html('사용장비&nbsp;<span style="color: red; ">*</span>');
                }

                $("#<%= txbFranchiseNm.ClientID %>").val('');
                $("#<%= txbCorpNo.ClientID %>").val('');
                $("#<%= txbAddr.ClientID %>").val('');
                $("#<%= txbPhoneNo.ClientID %>").val('');
            });
        });

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;

            var param = {};
            var form = "/Common/SearchUser";
            var target = "SearchUser";

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            this.focus();

            $("#<%=hdfEmpNo.ClientID %>").val(empno);
            $("#<%=hdfEmpNm.ClientID %>").val(empnm);
            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);

            __doPostBack("<%=hdfEmpNo.ClientID %>", "");
        }

        function seachOrderInfo() {
            opener.searchBtnClick();
            window.close();
        }

        function btnRegCheck() {
            if (confirm("작업자가 등록된 경우 오더발행 문자가 발송됩니다. 오더 정보를 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnDelCheck() {
            if (confirm("오더 정보를 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        var PopupCorpInfo = function () {

            var orderCd = $("#<%= ddlOrderCode.ClientID %> option:selected").val();

            if (orderCd == "") {
                alert("오더구분을 먼저 선택해주세요.");
            }
            else {
                var nWidth = 1000;
                var nHeight = 500;

                var param = {
                    pORDERCODE: orderCd
                };
                var form = "/Common/SearchCorpPopup";
                var target = "SearchCorp";

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetCorpInfo = function (CorpNm, CorpNo, Addr, AddrDtl, TelNo, PhoneNo) {
            $("#<%= txbFranchiseNm.ClientID %>").val(CorpNm);
            $("#<%= txbCorpNo.ClientID %>").val(CorpNo);
            $("#<%= txbAddr.ClientID %>").val(Addr + " " + AddrDtl);
            $("#<%= txbPhoneNo.ClientID %>").val(TelNo);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>오더정보 저장</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th style="width:100px">오더구분&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="optionbox">
                                                            <asp:DropDownList ID="ddlOrderCode" style="width:150px;" runat="server" CssClass="ddlOrderCode" TabIndex="0">
                                                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="VAN" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="K-VIZERO" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="BIZ" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Gem" Value="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </p>
                                                    </td>
                                                    <th style="width:130px">업무구분&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbWorktype" runat="server" Width="230px" MaxLength="20" TabIndex="1"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thName" style="width:100px">이름&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbFranchiseNm" runat="server" Width="200px" MaxLength="150" TabIndex="2"></asp:TextBox>
                                                            <input type="button" id="btnCorpInfo" runat="server" class="td-button" value=".." visible="false" />
                                                        </p>
                                                    </td>
                                                    <th id="thNum" style="width:130px">번호&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbCorpNo" runat="server" Width="230px" MaxLength="20" TabIndex="3" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">주소&nbsp;<span style="color:red;">*</span></th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbAddr" runat="server" Width="550px" MaxLength="250" TabIndex="4"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thPhone" style="width:100px">연락처</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbPhoneNo" runat="server" Width="200px" MaxLength="50" TabIndex="5"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">방문요청일</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbHopeDt" runat="server" Width="230px" MaxLength="20" TabIndex="6" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thEquipment" style="width:100px">사용장비&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEquipment" runat="server" Width="200px" MaxLength="50" TabIndex="7"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th id="thOrderState" style="width:100px">상태&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbOrderState" runat="server" Width="230px" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">비고</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbNote" runat="server" Width="550px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="9"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">작업자지정</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:HiddenField ID="hdfEmpNo" runat="server" OnValueChanged="hdfEmpNo_ValueChanged" />
                                                            <asp:HiddenField ID="hdfEmpNm" runat="server" />
                                                            <asp:TextBox ID="txbEmpUserNm" runat="server" Width="500px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                        <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                                                    </td>
                                                </tr>
                                                <tr id="trReason" runat="server" visible="false">
                                                    <th style="width:100px">사유</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbReason" runat="server" Width="550px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="9"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trImg" runat="server" visible="false">
                                                    <th style="width:100px">참조</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <span id="spanImg" runat="server"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <asp:HiddenField ID="hdfOrgCd" runat="server"/>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="hdfEmpNo" EventName="ValueChanged" />
                                    </Triggers>
                                </asp:updatepanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력값입니다.
                        <div class="btncenter">
                            <asp:Button id="btnReg" runat="server" OnClientClick="return btnRegCheck()" OnClick="BtnReg_Click" class="btn-green" Text="저장" />
                            <asp:Button id="btnDel" runat="server" OnClientClick="return btnDelCheck()" OnClick="btnDel_Click" class="btn-black" Text="삭제" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
