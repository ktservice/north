﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderInfoDetail.aspx.cs" Inherits="KTSSolutionWeb.OrderInfoDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>오더정보 상세</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>오더정보 상세</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th style="width:100px">오더구분&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="optionbox">
                                                            <asp:DropDownList ID="ddlOrderCode" style="width:150px;" runat="server" CssClass="ddlOrderCode" Enabled="false" TabIndex="0">
                                                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="VAN" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="K-VIZERO" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="BIZ" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Gem" Value="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </p>
                                                    </td>
                                                    <th style="width:130px">업무구분&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbWorktype" runat="server" Width="230px" MaxLength="20" TabIndex="1" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thName" style="width:100px">이름&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbFranchiseNm" runat="server" Width="200px" MaxLength="150" TabIndex="2" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th id="thNum" style="width:130px">번호&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbCorpNo" runat="server" Width="230px" MaxLength="20" TabIndex="3" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">주소&nbsp;<span style="color:red;">*</span></th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbAddr" runat="server" Width="550px" MaxLength="250" TabIndex="4" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thPhone" style="width:100px">연락처</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbPhoneNo" runat="server" Width="200px" MaxLength="50" TabIndex="5" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">방문요청일</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbHopeDt" runat="server" Width="230px" MaxLength="20" TabIndex="6" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th id="thEquipment" style="width:100px">사용장비&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEquipment" runat="server" Width="200px" MaxLength="50" TabIndex="7" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th id="thOrderState" style="width:100px">상태&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbOrderState" runat="server" Width="230px" MaxLength="50" TabIndex="8" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">비고</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbNote" runat="server" Width="550px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="9" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">작업자지정</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbEmpUserNm" runat="server" Width="500px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trReason" runat="server" visible="false">
                                                    <th style="width:100px">사유</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbReason" runat="server" Width="550px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="9" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr id="trImg" runat="server" visible="false">
                                                    <th style="width:100px">참조</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <span id="spanImg" runat="server"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <asp:HiddenField ID="hdfOrgCd" runat="server"/>
                                    </ContentTemplate>
                                </asp:updatepanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
