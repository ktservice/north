﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.Order;
using System.Web.UI.HtmlControls;

namespace KTSSolutionWeb
{
    public partial class RegOrderInfo : PageBase
    {
        public string strOdrNum
        {
            get
            {
                if (ViewState["OdrNum"] != null)
                {
                    return ViewState["OdrNum"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["OdrNum"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();

                    updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void PageInit()
        {
            DataSet ds = null;
            DataSet dsFile = null;

            try
            {
                strOdrNum = Request.Form["pORDERNUM"] == null ? "" : Request.Form["pORDERNUM"].ToString();

                if (!strOdrNum.Equals(""))
                {
                    using (OrderMgmt mgmt = new OrderMgmt())
                    {
                        ds = mgmt.GetOrderNumInfo(strOdrNum);
                        dsFile = mgmt.GetOrderFile(strOdrNum);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.ddlOrderCode.SelectedValue = ds.Tables[0].Rows[0]["ORDERCODE"].ToString();
                        this.txbWorktype.Text = ds.Tables[0].Rows[0]["WORKTYPE"].ToString();
                        this.txbFranchiseNm.Text = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                        this.txbCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();
                        this.txbAddr.Text = ds.Tables[0].Rows[0]["ADDR"].ToString();
                        this.txbPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();
                        this.txbHopeDt.Text = ds.Tables[0].Rows[0]["HOPEDT"].ToString();
                        this.txbEquipment.Text = ds.Tables[0].Rows[0]["EQUIPMENT"].ToString();
                        this.txbOrderState.Text = ds.Tables[0].Rows[0]["ORDERSTATE"].ToString();
                        this.txbNote.Text = ds.Tables[0].Rows[0]["NOTE"].ToString();
                        this.txbReason.Text = ds.Tables[0].Rows[0]["REASON"].ToString();
                        this.hdfEmpNo.Value = ds.Tables[0].Rows[0]["WORKEREMPNO"].ToString();
                        this.hdfEmpNm.Value = ds.Tables[0].Rows[0]["WORKEREMPNM"].ToString();
                        this.txbEmpUserNm.Text = ds.Tables[0].Rows[0]["FULLWORKEREMPNM"].ToString();
                        this.hdfOrgCd.Value = ds.Tables[0].Rows[0]["WORKERORGCD"].ToString();

                        string strWorkState = ds.Tables[0].Rows[0]["WORKSTATE"].ToString();

                        if (dsFile.Tables.Count > 0)
                        {
                            if (dsFile.Tables[0].Rows.Count > 0)
                            {

                                for (int i = 0; i < dsFile.Tables[0].Rows.Count; i++)
                                {
                                    if (dsFile.Tables[0].Rows[i]["FILETYPE"].ToString().Contains("image"))
                                    {
                                        HtmlImage ImgBoard = new HtmlImage();

                                        string strFileType = dsFile.Tables[0].Rows[i]["FILETYPE"].ToString();
                                        byte[] imgByte = (byte[])dsFile.Tables[0].Rows[i]["FILEDATA"];
                                        ImgBoard.Src = "data:" + strFileType + "; base64," + Convert.ToBase64String(imgByte);

                                        ImgBoard.Attributes.Add("style", "max-width:100%;height:auto;");

                                        spanImg.Controls.Add(ImgBoard);
                                    }
                                }
                            }
                        }

                        if (strWorkState.Equals("1") || strWorkState.Equals("2") || strWorkState.Equals("7"))
                        {
                            this.txbEmpUserNm.Attributes.Add("onclick", "PopupEmpUser();");
                            this.btnEmpUser.Visible = true;
                            this.btnEmpUser.Attributes.Add("onclick", "PopupEmpUser();");

                            this.txbHopeDt.Attributes.Add("class", "date");

                            this.ddlOrderCode.Enabled = true;
                            this.txbWorktype.ReadOnly = false;
                            this.txbFranchiseNm.ReadOnly = false;
                            this.btnCorpInfo.Visible = true;
                            this.btnCorpInfo.Attributes.Add("onclick", "PopupCorpInfo();");
                            this.txbCorpNo.ReadOnly = false;
                            this.txbAddr.ReadOnly = false;
                            this.txbPhoneNo.ReadOnly = false;
                            this.txbHopeDt.ReadOnly = false;
                            this.txbEquipment.ReadOnly = false;
                            this.txbOrderState.ReadOnly = false;
                            this.txbNote.ReadOnly = false;
                            this.txbReason.ReadOnly = false;
                            this.txbEmpUserNm.ReadOnly = false;

                            this.btnReg.Visible = true;
                            this.btnReg.Enabled = true;
                        }
                        else
                        {
                            this.txbEmpUserNm.Attributes.Remove("onclick");
                            this.btnEmpUser.Visible = false;
                            this.btnEmpUser.Attributes.Remove("onclick");

                            this.txbHopeDt.Attributes.Remove("class");

                            this.ddlOrderCode.Enabled = false;
                            this.txbWorktype.ReadOnly = true;
                            this.txbFranchiseNm.ReadOnly = true;
                            this.btnCorpInfo.Visible = true;
                            this.btnCorpInfo.Attributes.Remove("onclick");
                            this.txbCorpNo.ReadOnly = true;
                            this.txbAddr.ReadOnly = true;
                            this.txbPhoneNo.ReadOnly = true;
                            this.txbHopeDt.ReadOnly = true;
                            this.txbEquipment.ReadOnly = true;
                            this.txbOrderState.ReadOnly = true;
                            this.txbNote.ReadOnly = true;
                            this.txbReason.ReadOnly = true;
                            this.txbEmpUserNm.ReadOnly = true;

                            this.btnReg.Visible = false;
                            this.btnReg.Enabled = false;
                        }
                    }

                    trReason.Visible = true;
                    trImg.Visible = true;
                }
                else
                {
                    this.btnCorpInfo.Visible = true;
                    this.btnCorpInfo.Attributes.Add("onclick", "PopupCorpInfo();");
                    this.txbEmpUserNm.Attributes.Add("onclick", "PopupEmpUser();");
                    this.btnEmpUser.Attributes.Add("onclick", "PopupEmpUser();");

                    this.txbHopeDt.Attributes.Add("class", "date");

                    trReason.Visible = false;
                    trImg.Visible = false;
                }

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private bool ValidationCheck(ref string strMsg)
        {
            bool bCheck = true;

            string strOrderCode = Request.Form[this.ddlOrderCode.UniqueID];
            string strWorktype = Request.Form[this.txbWorktype.UniqueID];
            string strFranchiseNm = Request.Form[this.txbFranchiseNm.UniqueID];
            string strCorpNo = Request.Form[this.txbCorpNo.UniqueID];
            string strAddr = Request.Form[this.txbAddr.UniqueID];
            string strEquipment = Request.Form[this.txbEquipment.UniqueID];
            string strOrderState = Request.Form[this.txbOrderState.UniqueID];

            if (strOrderCode.Equals(""))
            {
                strMsg = "오더구분을 선택해주세요.";
                return false;
            }

            if (strWorktype.Equals(""))
            {
                strMsg = "업무구분을 입력해주세요.";
                return false;
            }

            if (strFranchiseNm.Equals(""))
            {
                strMsg = "이름을 입력해주세요.";
                return false;
            }

            if (strCorpNo.Equals(""))
            {
                strMsg = "번호를 입력해주세요.";
                return false;
            }

            if (strAddr.Equals(""))
            {
                strMsg = "주소를 입력해주세요.";
                return false;
            }

            if (strEquipment.Equals(""))
            {
                strMsg = "사용장비를 입력해주세요.";
                return false;
            }

            if (strOrderState.Equals(""))
            {
                strMsg = "상태을 입력해주세요.";
                return false;
            }

            return bCheck;
        }

        protected void hdfEmpNo_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
                string strEmpNm = Request.Form[hdfEmpNm.UniqueID];
                string strHopeDt = Request.Form[this.txbHopeDt.UniqueID];

                bool bValCheck = true;

                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetOrgInfo(strOrgCd);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.txbEmpUserNm.Text = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString() + " " + strEmpNm;
                        }
                        else
                        {
                            bValCheck = false;
                        }
                    }
                    else
                    {
                        bValCheck = false;
                    }

                    if (!bValCheck)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }
                }
                this.txbHopeDt.Text = strHopeDt;

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void BtnReg_Click(object sender, EventArgs e)
        {
            try
            {
                string strMsg = "";

                if (ValidationCheck(ref strMsg))
                {
                    string strRegEmpNo = this.Session["EMPNO"].ToString();

                    string strOrderCode = Request.Form[this.ddlOrderCode.UniqueID];
                    string strWorktype = Request.Form[this.txbWorktype.UniqueID];
                    string strFranchiseNm = Request.Form[this.txbFranchiseNm.UniqueID];
                    string strCorpNo = Request.Form[this.txbCorpNo.UniqueID];
                    string strAddr = Request.Form[this.txbAddr.UniqueID];
                    string strPhoneNo = Request.Form[this.txbPhoneNo.UniqueID];
                    string strHopeDt = Request.Form[this.txbHopeDt.UniqueID];
                    string strEquipment = Request.Form[this.txbEquipment.UniqueID];
                    string strOrderState = Request.Form[this.txbOrderState.UniqueID];
                    string strNote = Request.Form[this.txbNote.UniqueID];
                    string strWorkerEmpNo = Request.Form[this.hdfEmpNo.UniqueID];

                    using (OrderMgmt orderReg = new OrderMgmt())
                    {
                        
                        if (strOdrNum.Equals(""))
                        {
                            orderReg.InsertOrderInfo(strRegEmpNo, strOrderCode, strWorktype, strFranchiseNm, strCorpNo, strAddr, strPhoneNo, strHopeDt, strEquipment, strOrderState, strNote, strWorkerEmpNo);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOK", "alert('오더등록이 완료되었습니다.');seachOrderInfo();", true);
                        }
                        else
                        {
                            orderReg.UpdateOrderInfo(strOdrNum, strOrderCode, strWorktype, strFranchiseNm, strCorpNo, strAddr, strPhoneNo, strHopeDt, strEquipment, strOrderState, strNote, strWorkerEmpNo);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOK", "alert('오더수정이 완료되었습니다.');seachOrderInfo();", true);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                using (OrderMgmt orderReg = new OrderMgmt())
                {
                    if (strOdrNum.Equals(""))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "DelOK", "alert('잘못된 접근입니다.');seachOrderInfo();", true);
                    }
                    else
                    {
                        orderReg.DeleteOrderInfo(strOdrNum);
                        orderReg.DelOrderImageFile(strOdrNum, "");

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "DelOK", "alert('오더가 삭제 되었습니다.');seachOrderInfo();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}