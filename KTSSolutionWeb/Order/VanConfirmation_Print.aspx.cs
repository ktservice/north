﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;

namespace KTSSolutionWeb
{
    public partial class VanConfirmation_Print : PageBase
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            //세션체크
            if (this.Page.Session.Count <= 5)
            {
                PageUtility pageUtil = new PageUtility(this.Page);
                pageUtil.PageMove("/MobileSessionExpire");
            }
            else
            {
                if (!IsPostBack)
                {
                    initializeControls();
                    GetDataList();
                }
            }
        }
        private void initializeControls()
        {
            try
            {
                string strOrderNo = Request.Form["pORDERNO"] == null ? "" : Request.Form["pORDERNO"].ToString();

                if (strOrderNo.Equals(""))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');window.close();", true);
                }
                else
                {
                    this.hdfOrderNo.Value = strOrderNo;
                }
            }
            catch// (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');window.close();", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strOrderNo = this.hdfOrderNo.Value;

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "VANCONFIRMATIONINFO";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add(strOrderNo);
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strInstallDt = ds.Tables[0].Rows[0]["INSTALLDT"].ToString().Substring(0, 10);
                        string strFranchiseNm = ds.Tables[0].Rows[0]["FRANCHISENM"].ToString();
                        this.lblAgencyNm.Text = ds.Tables[0].Rows[0]["AGENCYNM"].ToString();
                        this.lblEmpNm.Text = ds.Tables[0].Rows[0]["FULLWORKEREMPNM"].ToString();
                        this.lblInstallDt.Text = strInstallDt;
                        this.lblFranchiseNm.Text = strFranchiseNm;
                        this.lblCorpNo.Text = ds.Tables[0].Rows[0]["CORPNO"].ToString();

                        this.lblChk1.Text = ds.Tables[0].Rows[0]["CHK1"].ToString();
                        this.lblChk1Note.Text = ds.Tables[0].Rows[0]["CHK1NOTE"].ToString();
                        this.lblChk2.Text = ds.Tables[0].Rows[0]["CHK2"].ToString();
                        this.lblChk2Note.Text = ds.Tables[0].Rows[0]["CHK2NOTE"].ToString();
                        this.lblChk3.Text = ds.Tables[0].Rows[0]["CHK3"].ToString();
                        this.lblChk3Note.Text = ds.Tables[0].Rows[0]["CHK3NOTE"].ToString();
                        this.lblChk4.Text = ds.Tables[0].Rows[0]["CHK4"].ToString();
                        this.lblChk4Note.Text = ds.Tables[0].Rows[0]["CHK4NOTE"].ToString();
                        this.lblChk5.Text = ds.Tables[0].Rows[0]["CHK5"].ToString();
                        this.lblChk5Note.Text = ds.Tables[0].Rows[0]["CHK5NOTE"].ToString();
                        this.lblChk6.Text = ds.Tables[0].Rows[0]["CHK6"].ToString();
                        this.lblChk6Note.Text = ds.Tables[0].Rows[0]["CHK6NOTE"].ToString();
                        this.lblChk7.Text = ds.Tables[0].Rows[0]["CHK7"].ToString();
                        this.lblChk7Note.Text = ds.Tables[0].Rows[0]["CHK7NOTE"].ToString();
                        this.lblChk8.Text = ds.Tables[0].Rows[0]["CHK8"].ToString();
                        this.lblChk8Note.Text = ds.Tables[0].Rows[0]["CHK8NOTE"].ToString();
                        this.lblChk9.Text = ds.Tables[0].Rows[0]["CHK9"].ToString();
                        this.lblChk9Note.Text = ds.Tables[0].Rows[0]["CHK9NOTE"].ToString();
                        this.lblChk10.Text = ds.Tables[0].Rows[0]["CHK10"].ToString();
                        this.lblChk10Note.Text = ds.Tables[0].Rows[0]["CHK10NOTE"].ToString();

                        this.lblPgmChk.Text = ds.Tables[0].Rows[0]["CHKPGM"].ToString();
                        this.lblPgmNote.Text = ds.Tables[0].Rows[0]["PGMNOTE"].ToString();
                        
                        this.lblPosNm.Text = ds.Tables[0].Rows[0]["POSNM"].ToString();
                        this.lblPosCnt.Text = ds.Tables[0].Rows[0]["POSCNT"].ToString();
                        this.lblPosChk.Text = ds.Tables[0].Rows[0]["POSCHK"].ToString();
                        this.lblPosNote.Text = ds.Tables[0].Rows[0]["POSNOTE"].ToString();
                        this.lblTerminalNm.Text = ds.Tables[0].Rows[0]["TERMINALNM"].ToString();
                        this.lblTerminalCnt.Text = ds.Tables[0].Rows[0]["TERMINALCNT"].ToString();
                        this.lblTerminalChk.Text = ds.Tables[0].Rows[0]["TERMINALCHK"].ToString();
                        this.lblTerminalNote.Text = ds.Tables[0].Rows[0]["TERMINALNOTE"].ToString();
                        this.lblPrintNm.Text = ds.Tables[0].Rows[0]["PRINTNM"].ToString();
                        this.lblPrintCnt.Text = ds.Tables[0].Rows[0]["PRINTCNT"].ToString();
                        this.lblPrintChk.Text = ds.Tables[0].Rows[0]["PRINTCHK"].ToString();
                        this.lblPrintNote.Text = ds.Tables[0].Rows[0]["PRINTNOTE"].ToString();

                        this.lblTraningChk1.Text = ds.Tables[0].Rows[0]["TRANING1"].ToString();
                        this.lblTraning1Note.Text = ds.Tables[0].Rows[0]["TRANING1NOTE"].ToString();
                        this.lblTraningChk2.Text = ds.Tables[0].Rows[0]["TRANING2"].ToString();
                        this.lblTraning2Note.Text = ds.Tables[0].Rows[0]["TRANING2NOTE"].ToString();
                        this.lblTraningChk3.Text = ds.Tables[0].Rows[0]["TRANING3"].ToString();
                        this.lblTraning3Note.Text = ds.Tables[0].Rows[0]["TRANING3NOTE"].ToString();
                        this.lblTraningChk4.Text = ds.Tables[0].Rows[0]["TRANING4"].ToString();
                        this.lblTraning4Note.Text = ds.Tables[0].Rows[0]["TRANING4NOTE"].ToString();
                        this.lblTraningChk5.Text = ds.Tables[0].Rows[0]["TRANING5"].ToString();
                        this.lblTraning5Note.Text = ds.Tables[0].Rows[0]["TRANING5NOTE"].ToString();
                        this.lblTraningChk6.Text = ds.Tables[0].Rows[0]["TRANING6"].ToString();
                        this.lblTraning6Note.Text = ds.Tables[0].Rows[0]["TRANING6NOTE"].ToString();
                        this.lblTraningChk7.Text = ds.Tables[0].Rows[0]["TRANING7"].ToString();
                        this.lblTraning7Note.Text = ds.Tables[0].Rows[0]["TRANING7NOTE"].ToString();
                        this.lblTraningChk8.Text = ds.Tables[0].Rows[0]["TRANING8"].ToString();
                        this.lblTraning8Note.Text = ds.Tables[0].Rows[0]["TRANING8NOTE"].ToString();

                        this.lblFranchiseNm2.Text = strFranchiseNm;
                        this.lblCustNm.Text = ds.Tables[0].Rows[0]["CUSTNM"].ToString();
                        this.imgSign.Src = ds.Tables[0].Rows[0]["CUSTSIGN"].ToString().Replace("¸", ",");
                        this.lblPhoneNo.Text = ds.Tables[0].Rows[0]["PHONENO"].ToString();

                        this.hdfFileNm.Value = DateTime.Now.ToString("yyyyMMdd") + "_" + strFranchiseNm + "_설치확인서";

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('조회된 확인서가 없습니다.');window.close();", true);
                    }
                }
                else
                {
                    PageUtility pageUtil = new PageUtility(this.Page);
                    pageUtil.PageMove("/MobileSessionExpire");
                }
            }
            catch// (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');window.close();", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

    }
}