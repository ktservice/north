﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VanConfirmation_Print.aspx.cs" Inherits="KTSSolutionWeb.VanConfirmation_Print" %>

<!DOCTYPE html> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VAN 오더 포스 설치/ 교육 확인서</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/html2canvas.js"></script>

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        
        @page {size:21cm 29.7cm;margin: 15px}
        @media print{
	        #header, .pop-notice-write, .btn_wrap {display:none}
            * {
                -webkit-print-color-adjust: exact;
                print-color-adjust: exact;
              } 
            .windowpop {
                height:auto;
                margin: 0;
                padding: 0;
              } 
        }

        .tablelist {padding:30px 20px 0 20px;}
        .tablelist table {min-width:100%; white-space:nowrap; margin-bottom:10px;}
        .tablelist table tbody tr td {border:1px solid #000; text-align:center; color:#000; font-size:13px; font-weight:200}

        .btn_wrap {background-color:white; text-align:center;padding-top:10px;padding-bottom:80px}

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
            },
            eventbind: function () {
                $("#btnDownload").click(function (e) {
                    PrintDownload();
                });

                //$("#btnPrint").click(function (e) {
                //    window.print();
                //});

                $("#btnClose").click(function (e) {
                    window.close();
                });
            }
        };

        var fn_Load = function () {
            //fn_Search();
        }

        var PrintDownload = function () {
            var div = $("#divConfirm");
            var fileNm = $("#<%= hdfFileNm.ClientID %>").val();

            html2canvas(div[0]).then(function (canvas) {
                var printImage = canvas.toDataURL();

                var a = document.createElement("a");
                a.download = fileNm;
                a.href = printImage;
                document.body.appendChild(a);
                a.click();
            });
        };
    </script>
</head>
<body style="overflow-x:hidden;overflow-y:auto;">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">VAN 오더 포스 설치/ 교육 확인서</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <div id="divConfirm" class="windowpop" style="background-color:white;height:1050px" >
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:tablelist -->
                    <div class="tablelist">
                        <h1 style="text-align:center; margin-bottom:20px;">(&nbsp;<asp:Label ID="lblAgencyNm" runat="server" Text="대리점명" Font-Bold="true"></asp:Label>&nbsp;) 포스 설치 / 교육 확인서</h1>
                        <h4 style="margin-bottom:2px;"> ☎ KTS 설치자가 설치일정 조정이 필요한 경우 기술지원센터 또는 대리점에 전달 必</h4>
                        <table>
                            <colgroup>
                                <col width="23%" />
                                <col width="27%" />
                                <col width="23%" />
                                <col width="27%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>설치담당자(소속/성명)</td>
                                    <td><asp:Label ID="lblEmpNm" runat="server"></asp:Label></td>
                                    <td>설치일자</td>
                                    <td><asp:Label ID="lblInstallDt" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>매장명</td>
                                    <td><asp:Label ID="lblFranchiseNm" runat="server"></asp:Label></td>
                                    <td>사업자번호</td>
                                    <td><asp:Label ID="lblCorpNo" runat="server"></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                        <h4 style="margin-bottom:2px;"> ◈ 설치점검사항 </h4>
                        <table>
                            <colgroup>
                                <col width="5%" />
                                <col width="50%" />
                                <col width="15%" />
                                <col width="30%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>NO</td>
                                    <td>항목</td>
                                    <td>확인유무</td>
                                    <td>특이사항</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>설치시간준수</td>
                                    <td><asp:Label ID="lblChk1" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk1Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>메뉴등록 완료</td>
                                    <td><asp:Label ID="lblChk2" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk2Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>메뉴 정상 출력</td>
                                    <td><asp:Label ID="lblChk3" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk3Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>신용카드 결제 테스트(결제/취소 진행)</td>
                                    <td><asp:Label ID="lblChk4" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk4Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>현금영수증 테스트(발행/취소 진행)</td>
                                    <td><asp:Label ID="lblChk5" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk5Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>감열프린트 설치 시 영수증 출력 테스트</td>
                                    <td><asp:Label ID="lblChk6" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk6Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>포스 모니터 터치(좌표) 이상유무</td>
                                    <td><asp:Label ID="lblChk7" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk7Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>듀얼모니터 설치 시 이상유무</td>
                                    <td><asp:Label ID="lblChk8" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk8Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>전원 ON/OFF 이상유무</td>
                                    <td><asp:Label ID="lblChk9" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblChk9Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>LAN 포설 길이 기재(ex 5M, 10M, 15M 등)</td>
                                    <td><asp:Label ID="lblChk10" runat="server"></asp:Label></td>
                                    <td style="background-color:#ededed"><asp:Label ID="lblChk10Note" runat="server" ForeColor="Gray" Text="업무지원 거증사진 첨부 必"></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                        <h4 style="margin-bottom:2px;"> ◈ 원격프로그램 즐겨찾기 추가 설치</h4>
                        <table>
                            <colgroup>
                                <col width="5%" />
                                <col width="50%" />
                                <col width="15%" />
                                <col width="30%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>NO</td>
                                    <td>항목</td>
                                    <td>설치유무</td>
                                    <td>특이사항</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>원격 프로그램 설치 및 가맹점 안내 유무</td>
                                    <td><asp:Label ID="lblPgmChk" runat="server"></asp:Label></td>
                                    <td style="background-color:#ededed"><asp:Label ID="lblPgmNote" runat="server" ForeColor="Gray" Text="오더확인 후 아래 1 or 2 선택 설치"></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
						<span class="inpbox" style="width:100%;margin-bottom:10px;margin-top:-5px;">
							<label style="color:red;font-size:12px;margin-bottom:10px;">1. CUN : "http://377.co.kr" 접속 -> 하단 "원격고객용설치" -> 상단 <image src="/Resource/images/icon-377.png"></image> 클릭 -> 이름변경 'S원격지원 15660365'</label><br />
							<label style="color:red;font-size:12px;">2. KTS기술지원 : "http://367.co.kt/ktspos/" 접속 -> 즐겨찾기 추가</label>
						</span>
                        <h4 style="margin-bottom:2px;"> ◈ 설치 내역</h4>
                        <table>
                            <colgroup>
                                <col width="15%" />
                                <col width="35%" />
                                <col width="10%" />
                                <col width="15%" />
                                <col width="25%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>명칭</td>
                                    <td>모델명</td>
                                    <td>수량</td>
                                    <td>점검유무</td>
                                    <td>특이사항</td>
                                </tr>
                                <tr>
                                    <td>포스</td>
                                    <td><asp:Label ID="lblPosNm" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPosCnt" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPosChk" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPosNote" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>카드단말기</td>
                                    <td><asp:Label ID="lblTerminalNm" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTerminalCnt" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTerminalChk" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTerminalNote" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>프린트</td>
                                    <td><asp:Label ID="lblPrintNm" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPrintCnt" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPrintChk" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPrintNote" runat="server"></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                        <h4 style="margin-bottom:2px;"> ◈ 교육 내역 </h4>
                        <table>
                            <colgroup>
                                <col width="5%" />
                                <col width="50%" />
                                <col width="15%" />
                                <col width="30%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>NO</td>
                                    <td>항목</td>
                                    <td>확인유무</td>
                                    <td>특이사항</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>전원ON/OFF 방법 설명</td>
                                    <td><asp:Label ID="lblTraningChk1" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTraning1Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>프로그램 시작방법(로그인/개점처리) 설명</td>
                                    <td><asp:Label ID="lblTraningChk2" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTraning2Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>원격지원 요청방법 설명</td>
                                    <td><asp:Label ID="lblTraningChk3" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTraning3Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>결제/취소 방법 설명</td>
                                    <td><asp:Label ID="lblTraningChk4" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTraning4Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>영수증/주문서 출력 방법 설명</td>
                                    <td><asp:Label ID="lblTraningChk5" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTraning5Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>매출 확인 방법 설명</td>
                                    <td><asp:Label ID="lblTraningChk6" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTraning6Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>마감처리 방법 설명</td>
                                    <td><asp:Label ID="lblTraningChk7" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTraning7Note" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>장애 접수 연락처 확인 및 안내(액정에 표기됨)</td>
                                    <td><asp:Label ID="lblTraningChk8" runat="server"></asp:Label></td>
                                    <td style="background-color:#ededed"><asp:Label ID="lblTraning8Note" runat="server" ForeColor="Gray" Text="단말기 액정 관리점 번호기재"></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tr>
                                <td style="border:2px solid #000;height:35px;font-size:22px;font-weight:bold; ">
                                    상기 내용에 대한 포스 설치와 교육을 하였습니다.
                                </td>
                            </tr>
                        </table>
                        <h4 style="margin-bottom:2px;"> ◈ 가맹점주 또는 영업담당자 확인</h4>
                        <table style="margin-bottom:30px;">
                            <colgroup>
                                <col width="20%" />
                                <col width="20%" />
                                <col width="15%" />
                                <col width="20%" />
                                <col width="25%" />
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td >매장명</td>
                                    <td colspan="4"><asp:Label ID="lblFranchiseNm2" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>확인자(성명)</td>
                                    <td><asp:Label ID="lblCustNm" runat="server"></asp:Label></td>
                                    <td style="text-align:right;padding-right:10px;border-left-style:hidden;color:#808080;">
                                        (서명)<img id="imgSign" runat="server" src="" style="position:absolute;width:100px; height:66px;margin-top:-30px;margin-left:-60px;" />
                                    </td>
                                    <td>연락처(HP)</td>
                                    <td><asp:Label ID="lblPhoneNo" runat="server"></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hdfOrderNo" runat="server" />
                        <asp:HiddenField ID="hdfFileNm" runat="server" />
                    </div>
                    <!-- E:tablelist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
            
            <div class="btn_wrap">
                <input id="btnDownload" type="button" class="btn-green" value="다운로드" />
                <%--<input id="btnPrint" type="button" class="btn-green" value="출력" />--%>
                <input id="btnClose" type="button" class="btn-gray" value="닫기" />
            </div>
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
