﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VanOrderInfodetail.aspx.cs" Inherits="KTSSolutionWeb.VanOrderInfodetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title id="title" runat="server">VAN오더 상세</title> 
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>


    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>
        .scrollbox {overflow-x:hidden; overflow-y:auto; width:100%; height:650px;}
        .scrollbox2 {overflow-x:hidden; overflow-y:auto; width:586px; height:auto; min-height:30px;}
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-button {
            width: 20px !important;
            padding:0px !important;
        }
        
        .th-line { 
            width: 100px;
            border-right: 1px solid #dfdfdf;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                var Status = $("#<%= hdfStatus.ClientID %>").val();

                if (Status == 'R') {
                    fn_GridInit();
                    fn_Load();
                }
            },
            eventbind: function () {
            }
        };

        var fn_GridInit = function () {

            var fn_input = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var rowData = cell.getRow().getData();
                var writerNM = rowData.WRITER_NAME;

                if (writerNM.length == 0) {
                    //create and style editor
                    var editor = document.createElement("input");

                    editor.setAttribute("type", "text");

                    //create and style input
                    editor.style.padding = "4px";
                    editor.style.width = "100%";
                    editor.style.boxSizing = "border-box";
                    editor.style.height = "100%";

                    var cellValue = cell.getValue()

                    if (cellValue != undefined) {
                        editor.value = cellValue;
                    }

                    //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                    onRendered(function () {
                        editor.focus();
                        editor.style.css = "100%";
                    });

                    function successFunc() {
                        success(editor.value);
                    }

                    editor.addEventListener("blur", successFunc);

                    //return the editor element
                    return editor;
                }
                else {
                    return cell.getValue();
                }
            };

            var fn_btnMemoAdd = function (cell) {
                var rowData = cell.getRow().getData();
                var rowid = rowData.id;
                var writerNM = rowData.WRITER_NAME;

                var input = "";

                if (writerNM.length == 0) {
                    input = "<input id='btnMemoAdd' type='button' onclick='btnMemoAddCheck(" + rowid + ");' value='등록' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
                }

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", field: "id", visible: false  },
                { headerHozAlign: "center", hozAlign: "center", title: "메모", field: "CONTENT", sorter: "string", editor: fn_input, width: 316 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "ENROLLMENT_DATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록자", field: "WRITER_NAME", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "오더번호", field: "ORDERNO", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', false, null); // div의 grid id 값
            ktsGrid.setHeight("20vh");
        }

        var fn_Load = function () {
            fn_GetMemo();
        }

        var successCallBack = function (json) {
            if (callGb == 'GetWorkerNm') {
                var empnm = json;
                $("#<%= txbEmpUserNm.ClientID %>").val(empnm);

                if (empnm.length > 0) {
                    $("#<%= lblWorkState.ClientID %>").text("수령대기");
                    
                }
                else {
                    $("#<%= lblWorkState.ClientID %>").text("작업자지정");
                }                
            }

            callGb = '';
        };

        var SetDispEmpNo = function (orgcd, empno, empnm) {
            this.focus();

            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);
            $("#<%=hdfEmpNo.ClientID %>").val(empno);
            $("#<%=hdfEmpNm.ClientID %>").val(empnm);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = empnm;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetWorkerNm');
            callGb = 'GetWorkerNm';

            ajaxCall(jsonData, ajaxUrl);
        };

        var fn_ParentSearch = function () {
            opener.fn_Search();
            window.close();
        };

        var fn_GetMemo = function () {
            callGb = '';
            var OrderNo = $("#<%= hdfOrderNo.ClientID %>").val();

            //입력 파라미터
            var aData = [];

            aData[0] = OrderNo;
            aData[1] = "Y";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetMemoData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);
        }

        var SetCorpInfo = function (CorpNm, CorpNo, Addr, AddrDtl, TelNo, PhoneNo) {
            $("#<%= txbFranchiseNm.ClientID %>").val(CorpNm);
            $("#<%= txbCorpNo.ClientID %>").val(CorpNo);
            $("#<%= txbAddr.ClientID %>").val(Addr);
            $("#<%= txbDtlAddr.ClientID %>").val(AddrDtl);
            $("#<%= txbTelNo.ClientID %>").val(TelNo);
            $("#<%= txbPhoneNo.ClientID %>").val(PhoneNo);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>오더정보 조회</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents" style="padding-top: 10px;">
                        <!-- S:datalist -->
                        <div class="datalist">

                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th class="th-line">접수유형&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlRcvType" style="width:150px;" runat="server" TabIndex="0" ReadOnly="true">
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                            <th class="th-line">대리점명&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbAgencyNm" runat="server" Width="200px" MaxLength="20" TabIndex="1" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">사업자이름&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbFranchiseNm" runat="server" Width="180px" MaxLength="150" TabIndex="2" ReadOnly="true"></asp:TextBox>
                                                    <input type="button" id="btnCorpInfo" runat="server" class="td-button" value=".." visible="false" />
                                                </p>
                                            </td>
                                            <th class="th-line" style="width:130px;">사업자등록번호&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCorpNo" runat="server" Width="200px" MaxLength="20" TabIndex="3" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this); " ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">주소&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbAddr" runat="server" Width="586px" MaxLength="250" TabIndex="4" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">상세주소</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbDtlAddr" runat="server" Width="586px" MaxLength="250" TabIndex="5" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">전화번호</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbTelNo" runat="server" Width="200px" MaxLength="50" TabIndex="6" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th id="thPhone" class="th-line">휴대폰번호</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbPhoneNo" runat="server" Width="200px" MaxLength="50" TabIndex="7" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trRcvDt" runat="server">
                                            <th class="th-line">접수일</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:Label ID="lblRcvDt" style="font-size:15px; font-weight:bold; padding: 5px 5px 5px; text-align:left;" runat="server" Width="200px" ReadOnly="true"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">파트너사지역&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlPartnersYn" style="width:150px;" runat="server" TabIndex="8" ReadOnly="true">
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">방문예정일&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width: 210px">
                                                    <asp:TextBox ID="txbRsvDt" runat="server" Width="200px" MaxLength="20" TabIndex="9" ReadOnly="true" placeholder="YYYY-MM-DD" ></asp:TextBox>
                                                </p>
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlRsvTime" style="width: 65px;padding: 0 7px;" runat="server" TabIndex="10" ReadOnly="true">
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">출동희망일&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbHopeDt" runat="server" Width="200px" MaxLength="20" TabIndex="11" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th class="th-line">택배배송일&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbDlvrDt" runat="server" Width="200px" MaxLength="20" TabIndex="12" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">접수내용</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbRcvNote" runat="server" Width="586px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="13" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">의뢰사항&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbReqNote" runat="server" Width="200px" MaxLength="250" TabIndex="14" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th class="th-line">상태</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                   <asp:Label style="font-size:15px; font-weight:bold; color:red; padding: 5px 0 5px;" ID="lblWorkState" runat="server" Width="200px" ReadOnly="true"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">회수배송주소&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbRtnAddr" runat="server" Width="586px" MaxLength="250" TabIndex="15" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">회수상세주소</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbRtnDtlAddr" runat="server" Width="586px" MaxLength="250" TabIndex="16" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">특이사항</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbReport" runat="server" Width="586px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="17" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trProc" runat="server">
                                            <th class="th-line">처리내역</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbProc" runat="server" Width="586px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="18" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trMemo" runat="server">
                                            <th class="th-line">메모</th>
                                            <td colspan="3" style="text-align:left">
                                                <div class="scrollbox2">
                                                    <div id="ktsGrid"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">작업자지정</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:HiddenField ID="hdfPreEmpNo" runat="server" />
                                                    <asp:HiddenField ID="hdfEmpNo" runat="server" />
                                                    <asp:HiddenField ID="hdfEmpNm" runat="server" />
                                                    <asp:HiddenField ID="hdfBelong" runat="server" />
                                                    <asp:HiddenField ID="hdfAssignYn" runat="server" />
                                                    <asp:TextBox ID="txbEmpUserNm" runat="server" Width="500px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trImg" runat="server" visible="false">
                                            <th class="th-line">참조</th>
                                            <td colspan="3" style="text-align:left">
                                                <span id="spanImg" runat="server"></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <asp:HiddenField ID="hdfOrderNo" runat="server"/>
                                <asp:HiddenField ID="hdfOrgCd" runat="server"/>
                                <asp:HiddenField ID="hdfStatus" runat="server"/>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>

        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
