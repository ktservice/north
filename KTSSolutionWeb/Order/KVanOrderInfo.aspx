﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KVanOrderInfo.aspx.cs" Inherits="KTSSolutionWeb.KVANOrderInfo" %>

<!DOCTYPE html> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>VAN 오더조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
        .loading {
            position: fixed;
            z-index: 99;
            top: 80px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: #f9f9f9 !important;
            opacity: 0.8;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();

            //$(".table-search").css('display', 'none');
        });

        var ktsGrid1;
        var ktsGrid2;
        var grid1;
        var grid2;
        var callGb;
        var Columns;

        var page = {
            init: function () {
                fn_GridInit1();
                fn_GridInit2();
                fn_Load();
            },
            eventbind: function () {
                $("#btnSelect1").click(function (e) {
                    fn_Search();
                });

                $("#btnSelect2").click(function (e) {
                    fn_Search365();
                });

                $("#btnLink").click(function (e) {
                    fn_Linkage();
                });

                $("#btnClose").click(function (e) {
                    window.close();
                }); 

                $("#divModalBackground").click(function (e) {
                    $("#divModal").hide();
                });
                
            }
        };

        var fn_GridInit1 = function () {

            var inputCheck = function (cell) {
                var row = cell.getRow();
                var rowData = row.getData();

                var id = rowData.id;

                element = document.createElement("input");
                element.id = "cb_" + id;
                element.name = "chkbox_Link";
                element.setAttribute("type", "checkbox");

                element.checked = false;

                return element;
            };

            var fn_btnMemo = function (cell) {
                var rowData = cell.getRow().getData();
                var OrderNo = rowData.RECEIPT_PK;
                
                var input = "<input id='btnMemoAdd' type='button' onclick='fn_ModalOpen(" + OrderNo + ");' value='보기' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
                
                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", field: "id", sorter: "number", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", formatter: inputCheck, width: 60, headerSort: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수유형", field: "RECEIPT_DIVISION", width: 100, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "대리점명", field: "AGENCY_NAME", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "가맹점명", field: "FRANCHISEE_NAME", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사업자번호", field: "BUISNESSMAN_NO", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "주소", field: "ADDRESS", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "상세주소", field: "ADDRESS_DETAIL", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "전화번호", field: "TEL", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "휴대번호", field: "CELL", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPT_DATE", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "방문예정일", field: "REQUEST_DATE", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "출동희망일", field: "DISPATCH_HOPE_DATE", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "택배일자", field: "PARCEL_DATE", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "접수내용", field: "RECEIPT_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "특이사항", field: "REQUEST_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "의뢰사항 ", field: "UNIQUENESS", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "회수주소", field: "COLLECT_ADDRESS", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "회수상세주소", field: "COLLECT_ADDRESS_DETAIL", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "엔지니어명", field: "ENGINEER", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "출동여부", field: "DISPATCH_DIVISION", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "BELONG", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "접수상태", field: "STATE", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "관련장비", field: "EQUIPMENT_TYPE", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "장비구분", field: "DEVICE", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "기본견적", field: "POLICY_BASIC_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "추가견적", field: "POLICY_EXTRA_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용SW", field: "SW_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "SW세팅여부", field: "SW_SETTING", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "VAN", field: "van", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "단말기&CAT_ID", field: "PM_NM", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "ASP계정이름", field: "ASP_NAME", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "ASP ID", field: "ASP_ID", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "ASP PW", field: "ASP_PW", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "솔루션계정이름", field: "SOLUTION_NAME", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "솔루션 ID", field: "SOLUTION_ID", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "솔루션 PW", field: "SOLUTION_PW", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "단말기보유여부", field: "TERMINAL_YN", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "SW세팅주체", field: "SW_SETTING_CONTENT", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "단말기발송장소", field: "TERMINAL_ADDR_JIJUM", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점발송주소", field: "TERMINAL_ADDRESS", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "출동종류", field: "DISPATCH_TYPE", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "주말여부", field: "DAYWEEK_TYPE", minWidth: 80, maxWidth: 400, sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "메모", field: "", formatter: fn_btnMemo, width: 70 },
                { headerHozAlign: "center", hozAlign: "center", field: "RECEIPT_PK", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "RECEIPT_TYPE", visible: false }
            ];
           
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid1 = ktsTabulator;
            grid1.gridDefault('ktsGrid1', 'fitData', true, 'local'); // div의 grid id 값

            ktsGrid1 = ktsGrid;
            //ktsGrid1.setHeight("76.5vh");
            ktsGrid1.setHeight("70vh");
        }

        var fn_GridInit2 = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "메모", field: "CONTENT", minWidth: 216, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "ENROLLMENT_DATE", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "글쓴이", field: "WRITER_NAME", minWidth: 80, maxWidth: 400, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", field: "RECEIPT_PK", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid2 = ktsTabulator;
            grid2.gridDefault('ktsGrid2', 'fitData', true, null); // div의 grid id 값

            ktsGrid2 = ktsGrid;
            ktsGrid2.setHeight("280px");
            //ktsGrid2.setHeight("76.5vh");

            $(".tabulator-col").css('height', '');
            $(".tabulator-col").css('padding-top', '');

        }

        var fn_Load = function () {
            //fn_Search();
        }

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;
            
            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                if (ktsGridId == 'ktsGrid1') {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid1.setData(json);
                    ktsGridRowCount(ktsGrid1.getDataCount());
                }
                else {
                    ktsGrid2.setData(json);
                }
            }
        }

        var ajaxCallBack = function (json) {
            if (callGb == 'LinkOk') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        //ktsGridAlert('선택한 KVAN오더를 kts업무지원시스템으로 연동완료했습니다.', 2000);
                        alert('선택한 오더를 kts업무지원시스템으로 연동 완료했습니다.');
                        $("#updateProgress").hide();
                        opener.fn_Search();
                        window.close();
                    }
                    else {
                        $("#updateProgress").hide();
                        ktsGridAlert(item.ERRMSG);
                    }
                });
            }

            callGb = '';
        };

        var fn_Search = function () {

            callGb = '';

            ktsGrid1.showColumn("PARCEL_DATE");
            ktsGrid1.hideColumn("EQUIPMENT_TYPE");
            ktsGrid1.hideColumn("DEVICE");
            ktsGrid1.hideColumn("POLICY_BASIC_CONTENT");
            ktsGrid1.hideColumn("POLICY_EXTRA_CONTENT");
            ktsGrid1.hideColumn("SW_CONTENT");
            ktsGrid1.hideColumn("SW_SETTING");
            ktsGrid1.hideColumn("van");
            ktsGrid1.hideColumn("PM_NM");
            ktsGrid1.hideColumn("ASP_NAME");
            ktsGrid1.hideColumn("ASP_ID");
            ktsGrid1.hideColumn("ASP_PW");
            ktsGrid1.hideColumn("SOLUTION_NAME");
            ktsGrid1.hideColumn("SOLUTION_ID");
            ktsGrid1.hideColumn("SOLUTION_PW");
            ktsGrid1.hideColumn("TERMINAL_YN");
            ktsGrid1.hideColumn("SW_SETTING_CONTENT");
            ktsGrid1.hideColumn("TERMINAL_ADDR_JIJUM");
            ktsGrid1.hideColumn("TERMINAL_ADDRESS");
            ktsGrid1.hideColumn("DISPATCH_TYPE");
            ktsGrid1.hideColumn("DAYWEEK_TYPE");

            var StDate = $("#<%= txbStDt.ClientID %>").val();
            var EnDate = $("#<%= txbEnDt.ClientID %>").val();

            var sdt = new Date(StDate);
            var edt = new Date(EnDate);

            var dateDiff = Math.ceil((edt.getTime() - sdt.getTime()) / (1000 * 3600 * 24));

            if (dateDiff > 7) {
                alert(" 조회기간은 7일을 초과 할 수 없습니다.");
            }
            else {
                //입력 파라미터
                var aData = [];

                aData[0] = StDate;
                aData[1] = EnDate;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid1.getUrl(location.href, 'GetApiVANOrder');
                //grid Bind
                grid1.getDataBind(jsonData, ajaxUrl, 'ktsGrid1');
            }
        }

        var fn_Search365 = function () {

            callGb = '';

            ktsGrid1.hideColumn("PARCEL_DATE");
            ktsGrid1.showColumn("EQUIPMENT_TYPE");
            ktsGrid1.showColumn("DEVICE");
            ktsGrid1.showColumn("POLICY_BASIC_CONTENT");
            ktsGrid1.showColumn("POLICY_EXTRA_CONTENT");
            ktsGrid1.showColumn("SW_CONTENT");
            ktsGrid1.showColumn("SW_SETTING");
            ktsGrid1.showColumn("van");
            ktsGrid1.showColumn("PM_NM");
            ktsGrid1.showColumn("ASP_NAME");
            ktsGrid1.showColumn("ASP_ID");
            ktsGrid1.showColumn("ASP_PW");
            ktsGrid1.showColumn("SOLUTION_NAME");
            ktsGrid1.showColumn("SOLUTION_ID");
            ktsGrid1.showColumn("SOLUTION_PW");
            ktsGrid1.showColumn("TERMINAL_YN");
            ktsGrid1.showColumn("SW_SETTING_CONTENT");
            ktsGrid1.showColumn("TERMINAL_ADDR_JIJUM");
            ktsGrid1.showColumn("TERMINAL_ADDRESS");
            ktsGrid1.showColumn("DISPATCH_TYPE");
            ktsGrid1.showColumn("DAYWEEK_TYPE");

            var StDate = $("#<%= txbStDt.ClientID %>").val();
            var EnDate = $("#<%= txbEnDt.ClientID %>").val();

            var sdt = new Date(StDate);
            var edt = new Date(EnDate);

            var dateDiff = Math.ceil((edt.getTime() - sdt.getTime()) / (1000 * 3600 * 24));

            if (dateDiff > 7) {
                alert(" 조회기간은 7일을 초과 할 수 없습니다.");
            }
            else {
                //입력 파라미터
                var aData = [];

                aData[0] = StDate;
                aData[1] = EnDate;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid1.getUrl(location.href, 'GetApiVAN365Order');
                //grid Bind
                grid1.getDataBind(jsonData, ajaxUrl, 'ktsGrid1');
            }
        }

        var fn_Linkage = function () {
            if (confirm("선택한 VAN오더를 kts업무지원시스템으로 연동 하시겠습니까?")) {
                $("#updateProgress").show();

                var dataCnt = ktsGrid1.getDataCount();
                var aData = [];
                var n = 0;

                callGb = 'LinkOk';

                for (var i = 1; i <= dataCnt; i++) {
                    var row = ktsGrid1.getRow(i);
                    var rowData = row.getData();

                    var id = rowData.id;
                    var chkbox = document.getElementById("cb_" + id);

                    if (chkbox == undefined || chkbox == "undefined" || chkbox == null) {
                        continue;
                    }

                    if (chkbox.checked) {
                        var aOrderData = [];

                        var OrderNo = rowData.RECEIPT_PK;                   //오더(접수)번호
                        var RcvType = rowData.RECEIPT_DIVISION;             //접수유형
                        var AgencyNm = rowData.AGENCY_NAME;                 //대리점명
                        var FranchiseNm = rowData.FRANCHISEE_NAME;          //사업자이름(가맹점명)
                        var CorpNo = rowData.BUISNESSMAN_NO;                //사업자등록번호
                        var Addr = rowData.ADDRESS;                         //주소
                        var DtlAddr = rowData.ADDRESS_DETAIL;               //상세주소
                        var TelNo = rowData.TEL;                            //연락처
                        var PhoneNo = rowData.CELL;                         //핸드폰번호
                        var RcvDt = rowData.RECEIPT_DATE;                   //접수일
                        var RsvDt = rowData.REQUEST_DATE;                   //방문예정일
                        var HopeDt = rowData.DISPATCH_HOPE_DATE             //출동희망일
                        var DlsvDt = rowData.PARCEL_DATE;                   //택배배송일             -- KVAN
                        var RcvNote = rowData.RECEIPT_CONTENT;              //접수내용
                        var ReqNote = rowData.UNIQUENESS;                   //의뢰사항
                        var RtnAddr = rowData.COLLECT_ADDRESS;              //회수배송주소
                        var RtnDtlAddr = rowData.COLLECT_ADDRESS_DETAIL;    //회수배송상세주소
                        //var Worker = rowData.ENGINEER;
                        //var DispYn = rowData.DISPATCH_DIVISION;
                        //var OrderState = rowData.STATE;
                        var Report = rowData.REQUEST_CONTENT;               //특이사항
                        var Belong = rowData.BELONG;                        //소속

                        var Equiptype = rowData.EQUIPMENT_TYPE;             //관련장비              -- VAN365
                        var Device = rowData.DEVICE;                        //장비구분              -- VAN365
                        var Policy_Basic = rowData.POLICY_BASIC_CONTENT;    //기본견적              -- VAN365
                        var Policy_Extra = rowData.POLICY_EXTRA_CONTENT;    //추가견적              -- VAN365
                        var Sw_Content = rowData.SW_CONTENT;                //사용SW                -- VAN365
                        var Sw_Setting = rowData.SW_SETTING;                //SW세팅여부            -- VAN365
                        var Van = rowData.van;                              //VAN                   -- VAN365
                        var Pm_Nm = rowData.PM_NM;                          //단말기&CAT_ID         -- VAN365
                        var Asp_Name = rowData.ASP_NAME;                    //ASP 계정 이름         -- VAN365
                        var Asp_Id = rowData.ASP_ID;                        //ASP ID                -- VAN365
                        var Asp_Pw = rowData.ASP_PW;                        //ASP PW                -- VAN365
                        var Solution_Name = rowData.SOLUTION_NAME;          //솔루션 계정 이름      -- VAN365
                        var Solution_Id = rowData.SOLUTION_ID;              //솔루션 ID             -- VAN365
                        var Solution_Pw = rowData.SOLUTION_PW;              //솔루션 PW             -- VAN365
                        var TerminalYn = rowData.TERMINAL_YN;               //단말기 보유 여부      -- VAN365
                        var Sw_Set_Content = rowData.SW_SETTING_CONTENT;    //SW 세팅 주체          -- VAN365
                        var TerminalAdd_JJ = rowData.TERMINAL_ADDR_JIJUM;   //단말기 발송 장소      -- VAN365
                        var TerminalAdd = rowData.TERMINAL_ADDRESS;         //지점 발송 주소        -- VAN365
                        var Dispatch = rowData.DISPATCH_TYPE;               //출동종류              -- VAN365
                        var HolidayFlag = rowData.DAYWEEK_TYPE;             //주말여부              -- VAN365


                        var ReceiptType = rowData.RECEIPT_TYPE;             //KVAN, VAN365 구분

                        aOrderData[0] = OrderNo;
                        aOrderData[1] = RcvType;
                        aOrderData[2] = AgencyNm;
                        aOrderData[3] = FranchiseNm;
                        aOrderData[4] = CorpNo;
                        aOrderData[5] = Addr;
                        aOrderData[6] = DtlAddr;
                        aOrderData[7] = TelNo;
                        aOrderData[8] = PhoneNo;
                        aOrderData[9] = RcvDt;
                        aOrderData[10] = RsvDt;
                        aOrderData[11] = HopeDt;
                        aOrderData[12] = DlsvDt;
                        aOrderData[13] = RcvNote;
                        aOrderData[14] = ReqNote;
                        aOrderData[15] = RtnAddr;
                        aOrderData[16] = RtnDtlAddr;
                        aOrderData[17] = Report;
                        aOrderData[18] = Belong;

                        aOrderData[19] = Equiptype;
                        aOrderData[20] = Device;
                        aOrderData[21] = Policy_Basic;
                        aOrderData[22] = Policy_Extra;
                        aOrderData[23] = Sw_Content;
                        aOrderData[24] = Sw_Setting;
                        aOrderData[25] = Van;
                        aOrderData[26] = Pm_Nm;
                        aOrderData[27] = Asp_Name;
                        aOrderData[28] = Asp_Id;
                        aOrderData[29] = Asp_Pw;
                        aOrderData[30] = Solution_Name;
                        aOrderData[31] = Solution_Id;
                        aOrderData[32] = Solution_Pw;
                        aOrderData[33] = TerminalYn;
                        aOrderData[34] = Sw_Set_Content;
                        aOrderData[35] = TerminalAdd_JJ;
                        aOrderData[36] = TerminalAdd;
                        aOrderData[37] = Dispatch;
                        aOrderData[38] = HolidayFlag;

                        aOrderData[39] = ReceiptType;

                        aData[n] = aOrderData;

                        n++;
                    }
                    else {
                        continue;
                    }
                }

                if (aData.length > 0) {
                    var pagenum = ktsGrid1.getPage();

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "LinkKVANOrder");

                    ajaxCallEx(jsonData, ajaxUrl, true);

                    var maxPageNum = ktsGrid1.getPageMax();

                    if (maxPageNum < pagenum) {
                        ktsGrid1.setPage(maxPageNum);
                    }
                    else {
                        ktsGrid1.setPage(pagenum);
                    }
                }
                else {
                    ktsGridAlert('선택한 VAN오더가 없습니다.', 2000);
                }
            }
        }

        var fn_ModalOpen = function (OrderNo) {

            //입력 파라미터
            var aData = [];

            aData[0] = OrderNo;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid2.getUrl(location.href, 'GetApiVANNote');

            //grid Bind
            grid2.getDataBind(jsonData, ajaxUrl, 'ktsGrid2');

            $("#divModal").show();
        }

        var fn_ModalClose = function () {
            $("#divModal").hide();
        }
    </script>
</head>
<body style="overflow-x:hidden;overflow-y:hidden;">
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">VAN 오더조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount" style="width: 500px"></label> 
				            <div class="btnset" style="float:right;">
                                <input id="btnSelect1" type="button" class="btn-green" value="KVAN조회" />
                                <input id="btnSelect2" type="button" class="btn-green" value="VAN365조회" />
				            </div>
                            <span class="inpbox" style="float:right;padding-right:20px;">
					            <label>등록일자</label>
                                <asp:TextBox ID="txbStDt" runat="server" class="date" Width="110px" ReadOnly="true"></asp:TextBox>
                                <em style="font-size: 15px; margin: 5px; vertical-align: middle;">~</em>
                                <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="110px" ReadOnly="true"></asp:TextBox>
                            </span> 
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid1"></div>
                        </div>

                        <div style="background-color:white; text-align:center;padding-top:10px;padding-bottom:80px" >
                            <input id="btnLink" type="button" class="btn-green" value="연동" />
                            <input id="btnClose" type="button" class="btn-gray" value="닫기" />
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
			
        <div id="divModal" class="scrollbox" style="display:none;position: fixed; top: 0; width: 100%; height: 100%;">
            <div id="divModalBackground" style="z-index: 98;position: fixed;top: 0;width: 100%;height: 100%;background-color: #ffffff !important;opacity: 0.7;"></div>
			<div class="requestlist" style="position: fixed;z-index: 99;text-align:center;top: 20%;left: 215px;width: 500px;height: 400px;background-color: #d0d0d0 !important;border-radius:10px;">
				<strong style="margin: 10px 5% 10px 5%;float:left";">메모</strong>
                <div class="datalist" style="margin: 10px 5% 0 5%;padding-bottom:10px;">
			        <!-- S:scrollbox -->
			        <div class="scrollbox">
                        <div id="ktsGrid2"></div>
                    </div>
			        <!-- E:scrollbox -->
				</div>

                <input id="btnModalClose" type="button" class="btn-black" style="width:33px;margin:0 !important;border-style:none;" onclick="fn_ModalClose()" value="닫기" />
			</div>
		</div>

        <div id="wrap">
			<!-- S: container -->
			<div id="container">
                <div id="updateProgress" style="display:none;">
                    <div class="loading" />
                    <div class="loadingContent">
                        <img src="../Resource/images/icon_loadingbar.gif" />
                        <h2>Loading...</h2>
                    </div>
                </div>
			</div>
			<!--//E: container -->   
        </div>

        <asp:HiddenField ID="hfCarId" runat="server" />
        <asp:HiddenField ID="hfCardType" runat="server" />
        <asp:HiddenField ID="hfParentType" runat="server" />
        <asp:HiddenField ID="hfMgmtType" runat="server" />

        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
