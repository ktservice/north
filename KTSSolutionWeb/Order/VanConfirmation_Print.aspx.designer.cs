﻿//------------------------------------------------------------------------------
// <자동 생성됨>
//     이 코드는 도구를 사용하여 생성되었습니다.
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다. 
// </자동 생성됨>
//------------------------------------------------------------------------------

namespace KTSSolutionWeb
{


    public partial class VanConfirmation_Print
    {

        /// <summary>
        /// form1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// lblAgencyNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblAgencyNm;

        /// <summary>
        /// lblEmpNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblEmpNm;

        /// <summary>
        /// lblInstallDt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblInstallDt;

        /// <summary>
        /// lblFranchiseNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblFranchiseNm;

        /// <summary>
        /// lblCorpNo 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblCorpNo;

        /// <summary>
        /// lblChk1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk1;

        /// <summary>
        /// lblChk1Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk1Note;

        /// <summary>
        /// lblChk2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk2;

        /// <summary>
        /// lblChk2Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk2Note;

        /// <summary>
        /// lblChk3 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk3;

        /// <summary>
        /// lblChk3Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk3Note;

        /// <summary>
        /// lblChk4 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk4;

        /// <summary>
        /// lblChk4Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk4Note;

        /// <summary>
        /// lblChk5 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk5;

        /// <summary>
        /// lblChk5Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk5Note;

        /// <summary>
        /// lblChk6 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk6;

        /// <summary>
        /// lblChk6Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk6Note;

        /// <summary>
        /// lblChk7 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk7;

        /// <summary>
        /// lblChk7Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk7Note;

        /// <summary>
        /// lblChk8 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk8;

        /// <summary>
        /// lblChk8Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk8Note;

        /// <summary>
        /// lblChk9 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk9;

        /// <summary>
        /// lblChk9Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk9Note;

        /// <summary>
        /// lblChk10 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk10;

        /// <summary>
        /// lblChk10Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblChk10Note;

        /// <summary>
        /// lblPgmChk 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPgmChk;

        /// <summary>
        /// lblPgmNote 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPgmNote;

        /// <summary>
        /// lblPosNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPosNm;

        /// <summary>
        /// lblPosCnt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPosCnt;

        /// <summary>
        /// lblPosChk 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPosChk;

        /// <summary>
        /// lblPosNote 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPosNote;

        /// <summary>
        /// lblTerminalNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTerminalNm;

        /// <summary>
        /// lblTerminalCnt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTerminalCnt;

        /// <summary>
        /// lblTerminalChk 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTerminalChk;

        /// <summary>
        /// lblTerminalNote 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTerminalNote;

        /// <summary>
        /// lblPrintNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPrintNm;

        /// <summary>
        /// lblPrintCnt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPrintCnt;

        /// <summary>
        /// lblPrintChk 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPrintChk;

        /// <summary>
        /// lblPrintNote 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPrintNote;

        /// <summary>
        /// lblTraningChk1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk1;

        /// <summary>
        /// lblTraning1Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning1Note;

        /// <summary>
        /// lblTraningChk2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk2;

        /// <summary>
        /// lblTraning2Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning2Note;

        /// <summary>
        /// lblTraningChk3 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk3;

        /// <summary>
        /// lblTraning3Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning3Note;

        /// <summary>
        /// lblTraningChk4 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk4;

        /// <summary>
        /// lblTraning4Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning4Note;

        /// <summary>
        /// lblTraningChk5 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk5;

        /// <summary>
        /// lblTraning5Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning5Note;

        /// <summary>
        /// lblTraningChk6 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk6;

        /// <summary>
        /// lblTraning6Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning6Note;

        /// <summary>
        /// lblTraningChk7 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk7;

        /// <summary>
        /// lblTraning7Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning7Note;

        /// <summary>
        /// lblTraningChk8 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraningChk8;

        /// <summary>
        /// lblTraning8Note 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTraning8Note;

        /// <summary>
        /// lblFranchiseNm2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblFranchiseNm2;

        /// <summary>
        /// lblCustNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblCustNm;

        /// <summary>
        /// imgSign 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlImage imgSign;

        /// <summary>
        /// lblPhoneNo 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPhoneNo;

        /// <summary>
        /// hdfOrderNo 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfOrderNo;

        /// <summary>
        /// hdfFileNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdfFileNm;
    }
}
