﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegVanOrderInfo.aspx.cs" Inherits="KTSSolutionWeb.RegVanOrderInfo"  EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title id="title" runat="server">VAN오더 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style>
        .scrollbox {overflow-x:hidden; overflow-y:auto; width:100%; height:650px;}
        .scrollbox2 {overflow-x:hidden; overflow-y:auto; width:586px; height:auto; min-height:30px;}
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .div-Screenlock {
            position: absolute;
            top: 137px;
            width: 100%;
            height: 760px;
            background-color: #ffffff !important;
            z-index: 99;
            opacity: 0.7;
            overflow: hidden;
        }

        .td-button {
            width: 20px !important;
            padding:0px !important;
        }
        
        .th-line {
            width: 100px;
            border-right: 1px solid #dfdfdf;
        }

        .div-radio {
            border: 1px solid darkgray;
            width: 80px;
            height: 30px;
            cursor: pointer;
            padding-top: 3px;
            padding-left: 5px;
            margin-right: 4px;
            text-align: left;
            border-radius: 3px;
            background-color: white;
            float: left;
        }

        .label-rbInner {
            cursor: pointer;
            height: 28px;
            padding-top: 3px;
            padding-left: 3px;
            margin-top: -3px;
        }

        .checkbox-extra {
            float: left;
        }

        .label-extra-content {
            width: 480px;
            height: 40px;
            float: left;
            padding-top: 10px;
            padding-left: 5px;
            padding-right: 0px;
            margin-right: 10px;
        }

        .div-extra-minus {
            border: 1px solid darkgray; 
            width: 30px; 
            height: 30px; 
            float: left; 
            border-radius: 5px; 
            margin-top: 5px;
        }

        .div-extra-plus {
            border: 1px solid darkgray;
            width: 30px; 
            height: 30px; 
            float: left; 
            border-radius: 5px; 
            margin-top: 5px; 
            margin-right: 5px;
        }

        .label-extra-calc {
            width: 28px; 
            text-align: center;        
            padding-left: 1px;
            margin-top: -5px;
        }

        .label-extra-cnt {
            width: 20px;
            height: 40px;
            float: left;
            text-align:center;
            padding-top: 10px;
            padding-left: 5px;
            font-size: 15px;
        }      

        .div-extra-del {
            background-color: darkgray;
            width: 30px; 
            height: 30px; 
            float: left; 
            border-radius: 15px; 
            margin-top: 5px;
            cursor:pointer;
        }

        .div-extra-del-inner {
            height: 30px;
            margin-top: -1.5px;
            padding-left: 7.5px;
            display: inline-block;
            float: left;
            cursor: pointer;
        } 

        .div-extra-del-inner:after {            
            display: inline-block;
            content: "\00d7"; 
            font-size:20pt;
            color: white;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                var Status = $("#<%= hdfStatus.ClientID %>").val();

                if (Status == 'R') {
                    fn_GridInit();
                    fn_Load();
                    $("#divDevicePOS").attr('onclick', '').unbind('click');
                    $("#divDeviceCAT").attr('onclick', '').unbind('click');
                    $("#divDeviceKIOSK").attr('onclick', '').unbind('click');
                    $("#divDeviceTablet").attr('onclick', '').unbind('click');
                }
                else {
                    $("#divScreenLock").show();
                    $(".scrollbox").css("overflow-y", "hidden");
                }

                var ispostback = "<%= IsPostBack.ToString() %>";

                if (ispostback == false) {
                    fn_GetPolicyBasicList();
                }
            },
            eventbind: function () {
                $("#<%= ddlReceiptType.ClientID %>").change(function () {
                    var receiptType = $("#<%= ddlReceiptType.ClientID %> option:selected").val();

                    //접수 구분별 컨트롤 show/hide 및 값 초기화
                    if (receiptType == "VAN365") {
                        $("#divScreenLock").hide();
                        $(".scrollbox").css("overflow-y", "auto");

                        $("#<%= trDevice.ClientID %>").show();
                        $("#<%= trPolicyBasic.ClientID %>").show();
                        $("#<%= trPolicyExtra.ClientID %>").show();
                        $("#<%= trTerminalYN.ClientID %>").show();
                        $("#<%= trDispatch.ClientID %>").show();
                        $("#<%= divHolidayChk.ClientID %>").show();

                        $("#<%= trDlvrDt.ClientID %>").hide();

                        $("#<%= trTermialAddr.ClientID %>").show();
                        $("#<%= trSwContent.ClientID %>").show();
                        $("#<%= trSwSetting.ClientID %>").show();
                        $("#<%= trSwSettingHost.ClientID %>").show();
                        $("#<%= trVan.ClientID %>").show();
                        $("#<%= trCatList.ClientID %>").show();
                        $("#<%= trAspName.ClientID %>").show();
                        $("#<%= trAspAccount.ClientID %>").show();
                        $("#<%= trSolutionName.ClientID %>").show();
                        $("#<%= trSolutionAccount.ClientID %>").show();
                    }
                    else {
                        if (receiptType == "KVAN") {
                            $("#divScreenLock").hide();
                            $(".scrollbox").css("overflow-y", "auto");
                        }
                        else {
                            $("#divScreenLock").show();
                            $(".scrollbox").css("overflow-y", "hidden");
                        }

                        $("#<%= trDevice.ClientID %>").hide();
                        $("#<%= trPolicyBasic.ClientID %>").hide();
                        $("#<%= trPolicyExtra.ClientID %>").hide();
                        $("#<%= trTerminalYN.ClientID %>").hide();
                        $("#<%= trDispatch.ClientID %>").hide();
                        $("#<%= divHolidayChk.ClientID %>").hide();

                        $("#<%= trDlvrDt.ClientID %>").show();

                        $("#<%= trTermialAddr.ClientID %>").hide();
                        $("#<%= trSwContent.ClientID %>").hide();
                        $("#<%= trSwSetting.ClientID %>").hide();
                        $("#<%= trSwSettingHost.ClientID %>").hide();
                        $("#<%= trVan.ClientID %>").hide();
                        $("#<%= trCatList.ClientID %>").hide();
                        $("#<%= trAspName.ClientID %>").hide();
                        $("#<%= trAspAccount.ClientID %>").hide();
                        $("#<%= trSolutionName.ClientID %>").hide();
                        $("#<%= trSolutionAccount.ClientID %>").hide();
                    }

                    fn_GetRcvType(receiptType);

                    $("#<%= txbAgencyNm.ClientID %>").val('');
                    $("#<%= txbFranchiseNm.ClientID %> ").val('');
                    $("#<%= txbCorpNo.ClientID %> ").val('');
                    $("#<%= txbAddr.ClientID %> ").val('');
                    $("#<%= txbDtlAddr.ClientID %> ").val('');
                    $("#<%= txbTelNo.ClientID %> ").val('');
                    $("#<%= txbPhoneNo.ClientID %> ").val('');

                    fn_radioBoxChk('POS');
                    $("#<%= ddlPolicyBasic.ClientID %> ").val('');
                    $("#<%= hdfChkPolicyExtra.ClientID %>").val('');
                    $("#<%= hdfChkPolicyExtra2.ClientID %>").val('');
                    fn_ExtraListClose();
                    
                    $("#<%= ddlPartnersYn.ClientID %> ").val('');
                    $("#<%= txbRsvDt.ClientID %> ").val('');
                    $("#<%= ddlRsvTime.ClientID %> ").val('00');

                    fn_radioBoxChk('TerminalY');
                    fn_radioBoxChk('Dispatch_Normal');
                    $("input[id=rbHolidayN]").prop("checked", true);

                    $("#<%= txbHopeDt.ClientID %> ").val('');
                    $("#<%= ddlHopeTime.ClientID %> ").val('00');
                    $("#<%= txbDlvrDt.ClientID %> ").val('');

                    fn_radioBoxChk('AddrKTS');

                    $("#<%= txbRcvNote.ClientID %> ").val('');
                    $("#<%= txbReqNote.ClientID %> ").val('');
                    $("#<%= txbRtnAddr.ClientID %> ").val('');
                    $("#<%= txbRtnDtlAddr.ClientID %> ").val('');
                    $("#<%= txbReport.ClientID %> ").val('');

                    $("#<%= txbSwContent.ClientID %> ").val('');
                    fn_radioBoxChk('SettingC');
                    $("#<%= ddlSWSettingHost.ClientID %> ").val('');
                    $("#<%= txbVan.ClientID %> ").val('');
                    $("#divCatList").empty();
                    $("#divCATList").show();
                    $("#lblCatEmpty").show();
                    $("#").hide();
                    $("#hdfCatCnt").val('0');
                    $("#<%= txbAspName.ClientID %> ").val('');
                    $("#<%= txbAspID.ClientID %> ").val('');
                    $("#<%= txbAspPW.ClientID %> ").val('');
                    $("#<%= txbSolutionName.ClientID %> ").val('');
                    $("#<%= txbSolutionID.ClientID %> ").val('');
                    $("#<%= txbSolutionPW.ClientID %> ").val('');

                    $("#<%= txbProc.ClientID %> ").val('');

                    $("#<%= hdfPreEmpNo.ClientID %> ").val('');
                    $("#<%= hdfEmpNo.ClientID %> ").val('');
                    $("#<%= hdfEmpNm.ClientID %> ").val('');
                    $("#<%= hdfBelong.ClientID %> ").val('');
                    $("#<%= txbEmpUserNm.ClientID %> ").val('');
                    $("#<%= hdfChkSingleCnt.ClientID %>").val(0);
                });

                $("#<%= ddlRcvType.ClientID %>").change(function () {
                    $("#<%= hdfChkPolicyExtra.ClientID %>").val('');
                    $("#<%= hdfChkPolicyExtra2.ClientID %>").val('');
                    $("#<%= hdfChkSingleCnt.ClientID %>").val(0);
                    fn_GetPolicyBasic();
                    fn_ExtraListClose();
                });

                $("input[name=rbDeviceChk]").change(function () {
                    $("#<%= hdfChkPolicyExtra.ClientID %>").val('');
                    $("#<%= hdfChkPolicyExtra2.ClientID %>").val('');
                    $("#<%= hdfChkSingleCnt.ClientID %>").val(0);
                    fn_GetPolicyBasic();
                    fn_ExtraListClose();
                });

                $("#chkExtraAll").click(function () {
                    if ($("#chkExtraAll").is(":checked")) {
                        $("input[name=chkExtra]").prop("checked", true);
                    }
                    else {
                        $("input[name=chkExtra]").prop("checked", false);
                    }

                    fn_SetChkExtraList();
                });

                $("#<%= txbHopeDt.ClientID %>").change(function () {
                    fn_GetHoliDayFlag();
                });

                $("input[name=rbSWChk]").change(function () {
                    if ($("input[name=rbSWChk]:checked").val() == "세팅필요") {
                        $("#<%= ddlSWSettingHost.ClientID %>").prop("disabled", false);
                    }
                    else {
                        $("#<%= ddlSWSettingHost.ClientID %>").val('');
                        $("#<%= ddlSWSettingHost.ClientID %>").prop("disabled", true);
                    }
                });

                $("input[name=rbAddrChk]").change(function () {
                    if ($("input[name=rbAddrChk]:checked").val() == "KTS지점") {
                        $("#<%= txbTerminalAddr.ClientID %>").val('');
                    }
                    else {
                        var addr = $("#<%= txbAddr.ClientID %>").val() + " " + $("#<%= txbDtlAddr.ClientID %>").val();

                        $("#<%= txbTerminalAddr.ClientID %>").val(addr);
                    }
                });                
            } 
        };

        var fn_GridInit = function () {

            var fn_input = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var rowData = cell.getRow().getData();
                var writerNM = rowData.WRITER_NAME;

                if (writerNM.length == 0) {
                    //create and style editor
                    var editor = document.createElement("input");

                    editor.setAttribute("type", "text");

                    //create and style input
                    editor.style.padding = "4px";
                    editor.style.width = "100%";
                    editor.style.boxSizing = "border-box";
                    editor.style.height = "100%";

                    var cellValue = cell.getValue()

                    if (cellValue != undefined) {
                        editor.value = cellValue;
                    }

                    //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                    onRendered(function () {
                        editor.focus();
                        editor.style.css = "100%";
                    });

                    function successFunc() {
                        success(editor.value);
                    }

                    editor.addEventListener("blur", successFunc);

                    //return the editor element
                    return editor;
                }
                else {
                    return cell.getValue();
                }
            };

            var fn_btnMemoAdd = function (cell) {
                var rowData = cell.getRow().getData();
                var rowid = rowData.id;
                var writerNM = rowData.WRITER_NAME;

                var input = "";

                if (writerNM.length == 0) {
                    input = "<input id='btnMemoAdd' type='button' onclick='btnMemoAddCheck(" + rowid + ");' value='등록' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
                }

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", field: "id", visible: false  },
                { headerHozAlign: "center", hozAlign: "center", title: "메모", field: "CONTENT", sorter: "string", editor: fn_input, width: 316 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "ENROLLMENT_DATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록자", field: "WRITER_NAME", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록", field: "", width: 70, formatter: fn_btnMemoAdd },
                { headerHozAlign: "center", hozAlign: "center", title: "오더번호", field: "ORDERNO", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', false, null); // div의 grid id 값
            ktsGrid.setHeight("20vh");
        }

        var fn_Load = function () {
            fn_SetControls();
            fn_GetMemo();
        }

        var fn_SetControls = function () {

            var orderType = $("#<%= hdfOrderNo.ClientID %>").val().substring(0, 1);

            if (orderType == 'M') {
                $("#divExtraSearch").show();
                $("#btnExtraList").show();
                $("#btnCATAdd").show();
            } else {
                $("#divExtraSearch").hide();
                $("#btnExtraList").hide();
                $("#btnCATAdd").hide();
                $("input[name=rbTerminalChk]").prop('disabled', true);
                $("input[name=rbDispatchChk]").prop('disabled', true);
                $("input[name=rbSWChk]").prop('disabled', true);
                $("input[name=rbAddrChk]").prop('disabled', true);
               
            }

            $("input[name=rbDeviceChk]").prop('disabled', true);
            $("#divScreenLock").hide();
            $(".scrollbox").css("overflow-y", "auto");            

            if ($("#<%= ddlReceiptType.ClientID %> option:selected").val() == "VAN365") {
                var regchk = false;

                if ($("#<%= btnReg.ClientID %>").is(":visible")) {
                    regchk = true;
                }
                fn_radioBoxChk($("#<%= hdfDevice.ClientID %>").val());
                fn_radioBoxChk($("#<%= hdfTerminalYn.ClientID %>").val());
                fn_radioBoxChk($("#<%= hdfDispatch.ClientID %>").val());
                fn_radioBoxChk($("#<%= hdfHoliDayFlag.ClientID %>").val());
                fn_radioBoxChk($("#<%= hdfTerminalAddr.ClientID %>").val());
                fn_radioBoxChk($("#<%= hdfSwSetting.ClientID %>").val());

                fn_SetCATList();

                var ExtraList = $("#<%= hdfPolicyExtra.ClientID %>").val();
                fn_ControlBind_PolicyExtra(ExtraList);
            }
        };

        var fn_GetRcvType = function (receiptType) {

            var aData = [];

            aData[0] = receiptType;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetRcvType');

            callGb = 'GetRcvType';

            ajaxCall(jsonData, ajaxUrl, false);
        }

        var fn_GetPolicyBasicList = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPolicyBasicList');

            callGb = 'GetPolicyBasicList';

            ajaxCall(jsonData, ajaxUrl, false);
        }

        var fn_GetPolicyExtraList = function () {

            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPolicyExtraList');

            callGb = 'GetPolicyExtraList';

            ajaxCall(jsonData, ajaxUrl, false);
        }

        var fn_GetPolicyBasic = function () {
            var rcvtype = $("#<%= ddlRcvType.ClientID %> option:selected").text();
            var device = $("input[name=rbDeviceChk]:checked").val();
            var basiclist = $("#<%= hdfPolicyBasic.ClientID %>").val();

            var aData = [];

            aData[0] = rcvtype;
            aData[1] = device;
            aData[2] = basiclist;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPolicyBasic');

            callGb = 'GetPolicyBasic';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetChkPolicyExtra = function () {
            var chkextralist = $("#<%= hdfChkPolicyExtra2.ClientID %>").val();

            var aData = [];

            aData[0] = chkextralist;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetChkPolicyExtra');

            callGb = 'GetChkPolicyExtra';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetPolicyExtra = function () {
            var rcvtype = $("#<%= ddlRcvType.ClientID %> option:selected").text();
            var device = $("input[name=rbDeviceChk]:checked").val();
            var search = $("#txbExtraSearch").val();
            var extralist = $("#<%= hdfPolicyExtra.ClientID %>").val();

            $("#chkExtraAll").show();

            if (rcvtype != "선택하세요" && device != "") {
                $("#divExtraHead").show();
                $("#divExtraList").show();
                $("#divExtraChk").show();
                $("#lblExtraSearch").hide();
                $("#txbExtraSearch").show();
                $("#btnExtraList").hide();
                $("#btnExtraSearh").show();
            }

            var aData = [];

            aData[0] = rcvtype;
            aData[1] = device;
            aData[2] = search;
            aData[3] = extralist;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPolicyExtra');

            callGb = 'GetPolicyExtra';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetHoliDayFlag = function () {
            var hopedt = $("#<%= txbHopeDt.ClientID %>").val();

            var aData = [];

            aData[0] = hopedt;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetHoliDayFlag');

            callGb = 'GetHoliDayFlag';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var successCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetWorkerNm') {
                    var empnm = json;
                    $("#<%= txbEmpUserNm.ClientID %>").val(empnm);

                    if (empnm.length > 0) {
                        $("#<%= lblWorkState.ClientID %>").text("수령대기");

                    }
                    else {
                        $("#<%= lblWorkState.ClientID %>").text("작업자지정");
                    }

                    callGb = '';
                }
                else if (callGb == 'GetRcvType') {
                    fn_ControlBind_RcvType(json);

                    callGb = '';
                }
                else if (callGb == 'GetPolicyBasicList') {
                    $("#<%= hdfPolicyBasic.ClientID %>").val(json);

                    callGb = '';

                    var Status = $("#<%= hdfStatus.ClientID %>").val();

                    //if (Status != 'R') {
                        fn_GetPolicyExtraList();
                    //}
                }
                else if (callGb == 'GetPolicyExtraList') {
                    $("#<%= hdfPolicyExtra.ClientID %>").val(json);

                    callGb = '';
                }
                else if (callGb == 'GetPolicyBasic') {
                    fn_ControlBind_PolicyBasic(json);

                    callGb = '';
                }
                else if (callGb == 'GetPolicyExtra') {
                    fn_ControlBind_ChkPolicyExtra(json);

                    callGb = '';
                }
                else if (callGb == 'GetChkPolicyExtra') {
                    fn_ControlBind_PolicyExtra(json);

                    callGb = '';
                }
                else if (callGb == 'GetHoliDayFlag') {
                    var holydayflag = json;

                    if (holydayflag == "Y") {
                        $("#rbHolidayY").prop("checked", true);
                    }
                    else {
                        $("#rbHolidayN").prop("checked", true);
                    }

                    callGb = '';
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'InsOk') {
                            alert('신규 VAN오더 등록이 완료되었습니다.');
                            fn_ParentSearch();
                        }
                        else if (callGb == 'UpdOk') {
                            alert('VAN오더 수정이 완료되었습니다.');
                            fn_ParentSearch();
                        }
                        else if (callGb == 'DelOk') {
                            alert('VAN오더가 삭제 되었습니다.');
                            fn_ParentSearch();
                        }
                        else if (callGb == 'CancelOk') {
                            alert('VAN오더가 취소처리 되었습니다.');
                            fn_ParentSearch();
                        }
                        else if (callGb == 'InsMemoOk') {
                            ktsGridAlert('메모 정보가 추가되었습니다.', 2000);
                            fn_GetMemo();
                        }
                        $("#<%= txbReason.ClientID %>").val('');
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }

            callGb = '';
        };

        var PopupEmpUser = function () {

            var nWidth = 700;
            var nHeight = 500;

            var param = {};
            var form = "/Common/SearchUser";
            var target = "SearchUser";

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var SetDispEmpNo = function (orgcd, empno, empnm) {
            this.focus();

            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);
            $("#<%=hdfEmpNo.ClientID %>").val(empno);
            $("#<%=hdfEmpNm.ClientID %>").val(empnm);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = empnm;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetWorkerNm');
            callGb = 'GetWorkerNm';

            ajaxCall(jsonData, ajaxUrl);
        };

        var fn_ParentSearch = function () {
            opener.fn_Search();
            window.close();
        };

        var fn_RegVanOrder = function () {
            if (confirm("작업자가 등록된 경우 오더발행 문자가 발송됩니다. VAN오더 정보를 저장 하시겠습니까?")) {
                var Status = $("#<%= hdfStatus.ClientID %>").val();
                var WorkState = $("#<%= hdfWorkState.ClientID %>").val();

                var ReceiptType = $("#<%= ddlReceiptType.ClientID %> option:selected").val();

                var OrderNo = $("#<%= hdfOrderNo.ClientID %>").val();
                var RcvType = $("#<%= ddlRcvType.ClientID %> option:selected").val();
                var AgencyNm = $("#<%= txbAgencyNm.ClientID %>").val();
                var FrenchiseNm = $("#<%= txbFranchiseNm.ClientID %>").val();
                var CorpNo = $("#<%= txbCorpNo.ClientID %>").val();
                var Addr = $("#<%= txbAddr.ClientID %>").val();
                var DtlAddr = $("#<%= txbDtlAddr.ClientID %>").val();
                var TelNo = $("#<%= txbTelNo.ClientID %>").val();
                var PhoneNo = $("#<%= txbPhoneNo.ClientID %>").val();
                var RsvDt = $("#<%= txbRsvDt.ClientID %>").val();
                var RsvTime = $("#<%= ddlRsvTime.ClientID %> option:selected").val();
                var PartnersYn = $("#<%= ddlPartnersYn.ClientID %> option:selected").val();
                var HopeDt = $("#<%= txbHopeDt.ClientID %>").val();
                var HopeTime = $("#<%= ddlHopeTime.ClientID %> option:selected").val();
                var DlvrDt = $("#<%= txbDlvrDt.ClientID %>").val();
                var RcvNote = $("#<%= txbRcvNote.ClientID %>").val();
                var ReqNote = $("#<%= txbReqNote.ClientID %>").val();
                var RtnAddr = $("#<%= txbRtnAddr.ClientID %>").val();
                var RtnDtlAddr = $("#<%= txbRtnDtlAddr.ClientID %>").val();
                var Report = $("#<%= txbReport.ClientID %>").val();
                var Proc = $("#<%= txbProc.ClientID %>").val(); 
                var WorkerOrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
                var WorkerEmpNo = $("#<%= hdfEmpNo.ClientID %>").val();
                var PreWorkerEmpNo = $("#<%= hdfPreEmpNo.ClientID %>").val();
                var PreRsvDt = $("#<%= hdfRsvDt.ClientID %>").val();
                var Belong = $("#<%= hdfBelong.ClientID %>").val();
                var AssignYn = $("#<%= hdfAssignYn.ClientID %>").val();

                var Device = $("input[name=rbDeviceChk]:checked").val();
                var PolicyBasic = $("#<%= ddlPolicyBasic.ClientID %> option:selected").val();
                var PolicyExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val();
                var PolicyExtraText = fn_GetPolicyExtraText();

                var TerminalYN = $("input[name=rbTerminalChk]:checked").val();
                var Dispatch = $("input[name=rbDispatchChk]:checked").val();
                var HolicdayFlag = $("input[name=rbHolidayChk]:checked").val();
                var TerminalAddr_JJ = $("input[name=rbAddrChk]:checked").val();
                var TerminalAddr = $("#<%= txbTerminalAddr.ClientID %>").val();

                var SwContent = $("#<%= txbSwContent.ClientID %> ").val();
                var SwSetting = $("input[name=rbSWChk]:checked").val();
                var SwSettingHost = '';

                if (SwSetting == '세팅필요') {
                    SwSettingHost = $("#<%= ddlSWSettingHost.ClientID %> option:selected").val();
                }

                var Van = $("#<%= txbVan.ClientID %> ").val();
                var CAT = fn_GetCatList();
                var AspName = $("#<%= txbAspName.ClientID %> ").val();
                var AspID = $("#<%= txbAspID.ClientID %> ").val();
                var AspPW = $("#<%= txbAspPW.ClientID %> ").val();
                var SolutionName = $("#<%= txbSolutionName.ClientID %> ").val();
                var SolutionID = $("#<%= txbSolutionID.ClientID %> ").val();
                var SolutionPW = $("#<%= txbSolutionPW.ClientID %> ").val();
                

                var bChk = false;
                var sUrl = "";
                var aData = [];

                if (Status == "N") {
                    if (ReceiptType == "") {
                        message = "접수구분을 선택해주세요.";
                    }
                    else if (RcvType == "") {
                        message = "접수유형을 선택해주세요.";
                    }
                    else if (AgencyNm == "") {
                        message = "대리점명을 입력해주세요.";
                    }
                    else if (FrenchiseNm == "" || CorpNo == "") {
                        message = "사업자이름 및 사업자번호을 입력해주세요.";
                    }
                    else if (Addr == "") {
                        message = "주소를 입력해주세요.";
                    }
                    else if (PartnersYn == "") {
                        message = "파트너사여부를 선택해주세요.";
                    }
                    else if (RsvDt == "") {
                        message = "방문예정일을 입력해주세요.";
                    }
                    else if (HopeDt == "") {
                        message = "출동희망일을 입력해주세요.";
                    }
                    else if (DlvrDt == "" && ReceiptType == "KVAN") {
                        message = "택배배송일을 입력해주세요.";
                    }
                    else if (ReqNote == "") {
                        message = "의뢰사항을 입력해주세요.";
                    }
                    else if (RtnAddr == "") {
                        message = "회수배송주소를 입력해주세요.";
                    }
                    else {
                        bChk = true;

                        callGb = 'InsOk';
                        sUrl = "InsVanOrderInfo";

                        aData[0] = OrderNo;
                        aData[1] = RcvType;
                        aData[2] = AgencyNm;
                        aData[3] = FrenchiseNm;
                        aData[4] = CorpNo;
                        aData[5] = Addr;
                        aData[6] = DtlAddr;
                        aData[7] = TelNo;
                        aData[8] = PhoneNo;
                        aData[9] = PartnersYn;

                        if (RsvDt.length > 0) {
                            aData[10] = RsvDt + " " + RsvTime + ":00:00";
                        }
                        else {
                            aData[10] = "";
                        }

                        if (HopeDt.length > 0) {
                            aData[11] = HopeDt + " " + HopeTime + ":00:00";
                        }
                        else {
                            aData[11] = "";
                        }

                        aData[12] = DlvrDt;
                        aData[13] = RcvNote;
                        aData[14] = ReqNote;
                        aData[15] = RtnAddr;
                        aData[16] = RtnDtlAddr;
                        aData[17] = Report;
                        aData[18] = WorkerOrgCd;
                        aData[19] = WorkerEmpNo;
                        aData[20] = Device;
                        aData[21] = PolicyBasic;
                        aData[22] = PolicyExtraText;
                        aData[23] = TerminalYN;
                        aData[24] = Dispatch;
                        aData[25] = HolicdayFlag;
                        aData[26] = TerminalAddr_JJ;
                        aData[27] = TerminalAddr;
                        aData[28] = SwContent;
                        aData[29] = SwSetting;
                        aData[30] = SwSettingHost;
                        aData[31] = Van;
                        aData[32] = CAT;
                        aData[33] = AspName;
                        aData[34] = AspID;
                        aData[35] = AspPW;
                        aData[36] = SolutionName;
                        aData[37] = SolutionID;
                        aData[38] = SolutionPW;
                        aData[39] = ReceiptType;


                    }
                }
                else {
                    if (OrderNo == "" || RcvType == "") { 
                        message = "잘못된 접근입니다.";
                    }
                    else if (PartnersYn == "") {
                        message = "파트너사지역을 선택해주세요.";
                    }
                    else {
                        bChk = true;

                        callGb = 'UpdOk';
                        sUrl = "UpdVanOrderInfo";

                        aData[0] = OrderNo;
                        aData[1] = PartnersYn;
                        aData[2] = RsvDt + " " + RsvTime + ":00:00";
                        aData[3] = WorkerOrgCd;
                        aData[4] = WorkerEmpNo;
                        aData[5] = PreWorkerEmpNo;
                        aData[6] = Belong;
                        aData[7] = PreRsvDt;
                        aData[8] = AssignYn;

                        if (HopeDt.length > 0) {
                            aData[9] = HopeDt + " " + HopeTime + ":00:00";
                        }
                        else {
                            aData[9] = "";
                        }

                        aData[10] = PolicyExtraText;
                        aData[11] = PolicyExtra;
                        //aData[12] = CAT;
                        //aData[13] = Dispatch;
                        //aData[14] = HolicdayFlag;
                        //aData[15] = SwSetting;
                        //aData[16] = SwSettingHost;
                        //aData[17] = ReceiptType;
                        aData[12] = TerminalYN;
                        aData[13] = Dispatch;
                        aData[14] = HolicdayFlag;
                        aData[15] = TerminalAddr_JJ;
                        aData[16] = TerminalAddr;
                        aData[17] = SwContent;
                        aData[18] = SwSetting;
                        aData[19] = SwSettingHost;
                        aData[20] = Van;
                        aData[21] = CAT;
                        aData[22] = AspName;
                        aData[23] = AspID;
                        aData[24] = AspPW;
                        aData[25] = SolutionName;
                        aData[26] = SolutionID;
                        aData[27] = SolutionPW;
                        aData[28] = ReceiptType;
                        aData[29] = WorkState;
                    }
                }

                if (bChk) {
                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, sUrl);

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
                else {
                    alert(message);
                }
            }
            else {
                return false;
            }
        };

        var fn_DelVanOrder = function () {
            if (confirm("VAN오더 정보를 삭제하시겠습니까?")) {
                var Status = $("#<%= hdfStatus.ClientID %>").val();
                var OrderNo = $("#<%= hdfOrderNo.ClientID %>").val();

                if ((OrderNo == "" || Status != "R")) {
                    alert('잘못된 접근입니다.');
                    window.close();
                }
                else {
                    callGb = 'DelOk'

                    var aData = [];
                    aData[0] = OrderNo;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "DelVanOrderInfo");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
            else {
                return false;
            }
        };

        var fn_CancelVanOrder = function () {
            if (confirm("VAN오더 정보를 취소하시겠습니까?")) {
                var Status = $("#<%= hdfStatus.ClientID %>").val();
                var OrderNo = $("#<%= hdfOrderNo.ClientID %>").val();
                //var OrderNoChk = OrderNo.substring(0, 7);

                if (OrderNo == "" || Status != "R") {
                    alert('잘못된 접근입니다.');
                    window.close();
                }
                else {
                    callGb = 'CancelOk'
                    var Reason = $("#<%= txbReason.ClientID %>").val();

                    if (Reason.length > 0) {
                        var aData = [];
                        aData[0] = OrderNo;
                        aData[1] = Reason;

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = ajaxCallGetUrl(location.href, "CancelVanOrderInfo");

                        ajaxCallEx(jsonData, ajaxUrl, true);
                    }
                    else {
                        alert('사유를 입력해주세요.');
                    }
                }
            }
            else {
                return false;
            }
        };

        var fn_GetMemo = function () {
            callGb = '';
            var OrderNo = $("#<%= hdfOrderNo.ClientID %>").val();

            //입력 파라미터
            var aData = [];

            aData[0] = OrderNo;
            aData[1] = "Y";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetMemoData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);
        }

        var fn_ControlBind_RcvType = function (json) {

            var selector = $("#<%= ddlRcvType.ClientID %>");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.RCVTYPECD;
                options.text = item.RCVTYPENM;

                selector.append(options);
            });
        };

        var fn_ControlBind_PolicyBasic = function (json) {

            var selector = $("#<%= ddlPolicyBasic.ClientID %>");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "기본 견적을 선택해주세요.";

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");
                
                options.className = "options";
                options.value = item.CONTENT;
                options.text = item.CONTENT;

                selector.append(options);
            });
        };

        var fn_ControlBind_ChkPolicyExtra = function (json) {
            var divExtraList = $("#divExtraList");
            var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');
                        
            var chkExtraList = '';

            divExtraList.empty();

            if (JSON.parse(json).length > 0) {
                $.each(JSON.parse(json), function (idx, item) {

                    var divParent = document.createElement("div");

                    divParent.id = "divExtra" + item.POLICY_EXTRA_PK;
                    divParent.className = "divExtra";
                    divParent.style.border = "none";
                    divParent.style.paddingBottom = "5px";
                    divParent.style.backgroundColor = "white";

                    var p = document.createElement("p");

                    p.className = "inpbox";

                    var checkBox = document.createElement("input");

                    checkBox.id = "chkExtra" + item.POLICY_EXTRA_PK;
                    checkBox.name = "chkExtra";
                    checkBox.type = "checkbox";
                    checkBox.className = "checkbox-extra";
                    checkBox.style.width = "20px";
                    checkBox.style.marginLeft = "10px";
                    checkBox.style.marginRight = "10px";
                    checkBox.value = item.POLICY_EXTRA_PK;
                                      
                    for (var i = 0; i < chkExtra.length; i++) {
                        var extraKeyVal = chkExtra[i].split(',');

                        if (extraKeyVal.length > 1) {
                            if (extraKeyVal[0] == item.POLICY_EXTRA_PK) {
                                checkBox.checked = true;

                                if (chkExtraList.length > 0) {
                                    chkExtraList += "|";
                                }

                                chkExtraList += item.POLICY_EXTRA_PK;
                                chkExtraList += "," + extraKeyVal[1];
                            }
                        }
                    }
                    p.append(checkBox);

                    var labelContent = document.createElement("label");

                    labelContent.id = "lblExtraContent" + item.POLICY_EXTRA_PK;
                    labelContent.innerText = item.CONTENT;
                    labelContent.className = "label-extra-content";
                    labelContent.style.fontWeight = "200";

                    p.append(labelContent);

                    divParent.append(p);
                    divExtraList.append(divParent);

                    labelContent.onclick = function () {
                        $("#chkExtra" + item.POLICY_EXTRA_PK).click;
                    }

                    var hdfCountYn = document.createElement("input");
                    hdfCountYn.id = "hdfSingleChk" + item.POLICY_EXTRA_PK;
                    hdfCountYn.type = "hidden";
                    hdfCountYn.className = "extra-singlechk";
                    hdfCountYn.value = item.COUNT_YN;

                    p.append(hdfCountYn);
                });

                $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtraList);

                if (chkExtra.length > 0) {
                    var total = $("input[name=chkExtra]").length;
                    var checked = $("input[name=chkExtra]:checked").length;

                    if (total != checked) {
                        $("#chkExtraAll").prop("checked", false);
                    }
                    else {
                        $("#chkExtraAll").prop("checked", true);
                    }
                }

                $("input[name=chkExtra]").click(function () {
                    var total = $("input[name=chkExtra]").length;
                    var checked = $("input[name=chkExtra]:checked").length;

                    if (total != checked) {
                        $("#chkExtraAll").prop("checked", false);
                    }
                    else {
                        $("#chkExtraAll").prop("checked", true);
                    }

                    fn_SetChkExtraList();
                });
                
                var button = document.createElement("input");

                button.id = "btnExtraAdd";
                button.type = "button";
                button.className = "btn-green";
                button.style.width = "100%";
                button.value = "가져오기";

                button.onclick = function () {
                    var SingleCnt = $("#<%= hdfChkSingleCnt.ClientID %>").val();

                    if (SingleCnt > 1) {
                        alert("LAN 노출 가설은 하나만 입력할 수 있습니다.");
                    }
                    else {
                        $("#<%= hdfChkPolicyExtra2.ClientID %>").val($("#<%= hdfChkPolicyExtra.ClientID %>").val());
                        fn_GetChkPolicyExtra();
                    }
                }

                divExtraList.append(button);
            }
            else {
                alert("불러올 수 있는 추가 견적이 없습니다.");
                fn_ExtraListClose();
            }
        };

        var fn_ControlBind_PolicyExtra = function (json) {
            var divExtraList = $("#divExtraList");
            var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');
            var orderType = $("#<%= hdfOrderNo.ClientID %>").val().substring(0, 1);

            divExtraList.empty();

            if (JSON.parse(json).length > 0) {                
                $("#divExtraList").show();
                $("#chkExtraAll").hide();
                $("#divExtraHead").hide();
                $("#lblExtraSearch").hide();
                $("#txbExtraSearch").hide();

                if (orderType == 'M') {
                    $("#divExtraSearch").show();
                    $("#btnExtraList").show();
                    $("#btnCATAdd").show();
                } else {
                    $("#divExtraSearch").hide();
                    $("#btnExtraList").hide();
                    $("#btnCATAdd").hide();
                }

                $("#btnExtraSearh").hide();                               
                
                $.each(JSON.parse(json), function (idx, item) {
                    var divParent = document.createElement("div");

                    divParent.id = "divExtra" + item.POLICY_EXTRA_PK;
                    divParent.className = "divExtra";
                    divParent.style.border = "none";
                    divParent.style.paddingBottom = "5px";
                    divParent.style.backgroundColor = "white";

                    var hdfPk = document.createElement("input");

                    hdfPk.id = "hdfExtraPk" + item.POLICY_EXTRA_PK;
                    hdfPk.type = "hidden";
                    hdfPk.className = "extra-pk";
                    hdfPk.value = item.POLICY_EXTRA_PK;

                    divParent.append(hdfPk);

                    var p = document.createElement("p");

                    p.className = "inpbox";

                    var labelContent = document.createElement("label");

                    labelContent.id = "lblExtraContent" + item.POLICY_EXTRA_PK;
                    labelContent.innerText = item.CONTENT;
                    labelContent.className = "label-extra-content";
                    labelContent.style.fontWeight = "200"; 

                    p.append(labelContent);

                    var divMinus = document.createElement("div");
                    var divPlus = document.createElement("div");
                    var labelMinus = document.createElement("label");
                    var labelPlus = document.createElement("label");
                    var labelCnt = document.createElement("label");

                    divMinus.className = "div-extra-minus";
                    divPlus.className = "div-extra-plus";

                    labelMinus.className = "label-extra-calc";
                    labelMinus.style.fontWeight = "lighter";
                    labelMinus.style.fontSize = "26px";
                    labelPlus.className = "label-extra-calc";
                    labelPlus.style.fontWeight = "lighter";
                    labelPlus.style.fontSize = "26px";

                    if (orderType != "M") {
                        divMinus.style.backgroundColor = "darkgray";
                        divPlus.style.backgroundColor = "darkgray";
                    }
                    else {
                        if (item.COUNT_YN == "false") {
                            divMinus.style.backgroundColor = "darkgray";
                            divPlus.style.backgroundColor = "darkgray";
                        }
                        else {
                            divMinus.style.cursor = "pointer";
                            divPlus.style.cursor = "pointer";

                            divMinus.onclick = function () {
                                fn_ExtraCnt(item.POLICY_EXTRA_PK, "-");
                            }

                            divPlus.onclick = function () {
                                fn_ExtraCnt(item.POLICY_EXTRA_PK, "+");
                            }
                        }
                    }

                    labelCnt.id = "lblExtra" + item.POLICY_EXTRA_PK;
                    labelCnt.className = "label-extra-cnt";

                    labelCnt.innerText = item.CNT;

                    labelMinus.innerText = "-";
                    labelPlus.innerText = "+";

                    divMinus.append(labelMinus);
                    p.append(divMinus);

                    p.append(labelCnt);

                    divPlus.append(labelPlus);
                    p.append(divPlus);

                    if (orderType != "M") {
                        divMinus.style.backgroundColor = "darkgray";
                        divPlus.style.backgroundColor = "darkgray";
                    }
                    else {
                        var divDel = document.createElement("div");
                        divDel.className = "div-extra-del";

                        divDel.onclick = function () {
                            fn_RemoveDivExtra(item.POLICY_EXTRA_PK);
                        }

                        var divDelInner = document.createElement("div");
                        divDelInner.className = "div-extra-del-inner";

                        divDelInner.onclick = function () {
                            fn_RemoveDivExtra(item.POLICY_EXTRA_PK);
                        }

                        divDel.append(divDelInner);

                        p.append(divDel);
                    }

                    divParent.append(p);
                    divExtraList.append(divParent);
                });
            }
            else {
                fn_ExtraListClose();
            }
        };

        var fn_SetChkExtraList = function () {
            var chkExtra = $("input[name=chkExtra]:checked");
            var chkExtralist = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');

            var chkExtraNew = "";
            var SingleCnt = 0;

            $(chkExtra).each(function () {
                var chkVal = $(this).val();
                var Cnt = "1";

                $(this).parent().find('.extra-singlechk').each(function (index, item) {
                    if ($(item).val() == "false" || $(item).val() == false) {
                        SingleCnt += 1;
                    }
                });

                if (chkExtraNew != '') {
                    chkExtraNew += "|";
                }

                if (chkExtralist.length > 0 && chkExtralist != "") {
                    for (var i = 0; i < chkExtralist.length; i++) {
                        var keyVal = chkExtralist[i].split(',');

                        if (keyVal.length == 0 && keyVal[0] == "") {
                            continue;
                        }

                        var key = keyVal[0];
                        var val = keyVal[1];

                        if (chkVal == key) {
                            Cnt = val;
                            break;
                        }
                    }
                }

                chkExtraNew += chkVal + "," + Cnt;
            });

            $("#<%= hdfChkSingleCnt.ClientID %>").val(SingleCnt);
            $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtraNew);
        };

        var fn_radioBoxChk = function (val) {
            var Status = $("#<%= hdfStatus.ClientID %>").val();

            $("#rb" + val).prop('checked', true);
            $("#rb" + val).change();
        };

        var fn_ExtraCnt = function (key, calc) {

            var label = $("#lblExtra" + key);

            var Cnt = parseInt(label.text());

            if (calc == "-") {
                Cnt = Cnt - 1;
            }
            else {
                Cnt = Cnt + 1;
            }

            label.text(Cnt);

            if (Cnt <= 0) {
                fn_RemoveDivExtra(key);
            } else {
                var divExtraList = $("#divExtraList");

                var chkExtralist = "";

                //divExtra
                divExtraList.find('.extra-pk').each(function (index, item) {
                    if (chkExtralist.length > 0) {
                        chkExtralist += "|";
                    }

                    var key = $(item).val();
                    var cnt = $("#lblExtra" + key).text();

                    chkExtralist += key + "," + cnt;
                });

                $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtralist);
                $("#<%= hdfChkPolicyExtra2.ClientID %>").val(chkExtralist);
            }
        };

        var fn_RemoveDivExtra = function (delkey) {            
            var div = $("#divExtra" + delkey);

            var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');
            var chkExtraNew = "";

            for (var i = 0; i < chkExtra.length; i++) {
                var keyval = chkExtra[i].split(',');

                if (keyval.length > 1) {
                    var key = keyval[0];

                    if (key != delkey) {
                        if (chkExtraNew != '') {
                            chkExtraNew += "|";
                        }

                        chkExtraNew += chkExtra[i];
                    }
                }
            }

            $("#<%= hdfChkPolicyExtra.ClientID %>").val(chkExtraNew);
            $("#<%= hdfChkPolicyExtra2.ClientID %>").val(chkExtraNew);

            div.remove();

            fn_ExtraListClose();
        };

        var fn_ExtraListClose = function () {
            var chkExtraList = $("#<%= hdfChkPolicyExtra2.ClientID %>").val();

            if (chkExtraList.length > 0) {
                fn_GetChkPolicyExtra();
            }
            else {
                $("#chkExtraAll").show();
                $("#divExtraHead").hide();
                $("#divExtraList").hide();
                $("#divExtraChk").hide();
                $("#lblExtraSearch").show();
                $("#txbExtraSearch").hide();

                var orderType = $("#<%= hdfOrderNo.ClientID %>").val().substring(0, 1);

                if (orderType == 'M') {
                    $("#divExtraSearch").show();
                    $("#btnExtraList").show();
                    $("#btnCATAdd").show();
                } else {
                    $("#divExtraSearch").hide();
                    $("#btnExtraList").hide();
                    $("#btnCATAdd").hide();
                }

                $("#btnExtraSearh").hide();
                $("#divExtraList").empty();
                $("#txbExtraSearch").val('');
                $("#<%= hdfChkSingleCnt.ClientID %>").val(0);
            }            
        };

        var fn_GetPolicyExtraText = function () {
            var ExtraList = "";

            if ($("#<%= hdfChkPolicyExtra2.ClientID %>").val() != '') {
                var chkExtra = $("#<%= hdfChkPolicyExtra2.ClientID %>").val().split('|');

                for (var i = 0; i < chkExtra.length; i++) {
                    if (chkExtra[i] != "") {
                        var ExtraText = "";
                        var ExtraCnt = 0;

                        var lblExtraContent = $("#lblExtraContent" + chkExtra[i]);
                        var lblExtra = $("#lblExtra" + chkExtra[i]);

                        ExtraText = lblExtraContent.text();
                        ExtraCnt = lblExtra.text().replace(" ", "");

                        if (ExtraCnt > 0) {
                            if (ExtraList.length > 0) {
                                ExtraList += "| ";
                            }

                            ExtraList += ExtraText + " ^" + ExtraCnt;
                        }
                    }
                }
            }

            return ExtraList;
        };

        var fn_SetCATList = function () {

            var divCatList = $("#divCATList");
            var CatList = $("#<%= hdfCatList.ClientID %>").val().replace(", ", ",").split(",");
            var orderType = $("#<%= hdfOrderNo.ClientID %>").val().substring(0, 1);

            var CatCnt = 0;

            if (CatList.length > 0) {
                for (var i = 0; i < CatList.length; i++) {
                    var item = CatList[i].replace(" : ", ":").split(":");

                    if (item.length > 1) {

                        CatCnt++;

                        let id = CatCnt;

                        var serial = item[0];
                        var catid = item[1];

                        var divParent = document.createElement("div");

                        divParent.id = "divCat" + CatCnt;
                        //divParent.className = "divCat";
                        divParent.style.border = "none";
                        divParent.style.paddingBottom = "5px";

                        var hdfTerminal = document.createElement("input");

                        hdfTerminal.id = "hdfTerminalId" + id;
                        hdfTerminal.type = "hidden";
                        hdfTerminal.className = "terminal-id";
                        hdfTerminal.value = id;

                        divParent.append(hdfTerminal);

                        var p = document.createElement("p");

                        p.className = "inpbox";

                        var labelSerial = document.createElement("label");

                        labelSerial.id = "lblSerial" + CatCnt;
                        labelSerial.innerText = "단말기 시리얼";
                        labelSerial.className = "label-extra-content";
                        labelSerial.style.fontWeight = "300";
                        labelSerial.style.width = "90px";
                        labelSerial.style.float = "left";

                        p.append(labelSerial);

                        var txbSerial = document.createElement("input");

                        txbSerial.id = "txbSerial" + CatCnt;
                        txbSerial.type = "text";
                        txbSerial.value = serial;
                        txbSerial.style.width = "180px";
                        txbSerial.style.float = "left";
                        txbSerial.style.marginRight = "15px";
                        if (orderType != "M") {
                            txbSerial.readOnly = true;
                        }

                        p.append(txbSerial);

                        var labelCatId = document.createElement("label");

                        labelCatId.id = "lblCatId" + CatCnt;
                        labelCatId.innerText = "CAT ID(TID)";
                        labelCatId.className = "label-extra-content";
                        labelCatId.style.fontWeight = "300";
                        labelCatId.style.width = "90px";
                        labelCatId.style.float = "left";

                        p.append(labelCatId);

                        var txbCatId = document.createElement("input");

                        txbCatId.id = "txbCatId" + CatCnt;
                        txbCatId.type = "text";
                        txbCatId.value = catid;
                        txbCatId.style.width = "180px";
                        txbCatId.style.float = "left";
                        txbCatId.style.marginRight = "10px";

                        if (orderType != "M") {
                            txbCatId.readOnly = true;
                        }

                        p.append(txbCatId);

                        if (orderType == "M") {
                            var divDel = document.createElement("div");
                            divDel.className = "div-extra-del";

                            divDel.onclick = function () {
                                fn_RemoveDivCat(id);
                            }

                            var divDelInner = document.createElement("div");
                            divDelInner.className = "div-extra-del-inner";
                            
                            divDel.append(divDelInner);

                            p.append(divDel);
                        }

                        divParent.append(p);

                        divCatList.append(divParent);
                    }
                }
            }


            if (CatCnt > 0) {
                if (orderType == "M") {
                    $("#divCATHead").show();
                }
                else {
                    $("#divCATHead").hide();
                }
                $("#lblCatEmpty").hide(); 
                $("#divCATList").show();
            }
            else {
                $("#lblCatEmpty").show();
                $("#divCATList").hide();
            }

            $("#hdfCatCnt").val(CatCnt);
        };

        var fn_AddCATList = function () {
            var divCatList = $("#divCATList");

            $("#lblCatEmpty").hide();
            $("#divCATList").show();
            var CatCnt = parseInt($("#hdfCatCnt").val());

            CatCnt += 1;

            var divParent = document.createElement("div");

            divParent.id = "divCat" + CatCnt;
            //divParent.className = "divCat";
            divParent.style.border = "none";
            divParent.style.paddingBottom = "5px";

            var hdfTerminal = document.createElement("input");

            hdfTerminal.id = "hdfTerminalId" + CatCnt;
            hdfTerminal.type = "hidden";
            hdfTerminal.className = "terminal-id";
            hdfTerminal.value = CatCnt;

            divParent.append(hdfTerminal);

            var p = document.createElement("p");

            p.className = "inpbox";

            var labelSerial = document.createElement("label");

            labelSerial.id = "lblSerial" + CatCnt;
            labelSerial.innerText = "단말기 시리얼";
            labelSerial.className = "label-extra-content";
            labelSerial.style.fontWeight = "300";
            labelSerial.style.width = "90px";
            labelSerial.style.float = "left";

            p.append(labelSerial);

            var txbSerial = document.createElement("input");

            txbSerial.id = "txbSerial" + CatCnt;
            txbSerial.type = "text";
            txbSerial.style.width = "180px";
            txbSerial.style.float = "left";
            txbSerial.style.marginRight = "15px";
            txbSerial.className = "input-Terminal";

            p.append(txbSerial);

            var labelCatId = document.createElement("label");

            labelCatId.id = "lblCatId" + CatCnt;
            labelCatId.innerText = "CAT ID(TID)";
            labelCatId.className = "label-extra-content";
            labelCatId.style.fontWeight = "300";
            labelCatId.style.width = "90px";
            labelCatId.style.float = "left";

            p.append(labelCatId);

            var txbCatId = document.createElement("input");
            
            txbCatId.id = "txbCatId" + CatCnt;
            txbCatId.type = "text";
            txbCatId.style.width = "180px";
            txbCatId.style.float = "left";
            txbCatId.style.marginRight = "10px";
            txbCatId.className = "input-Terminal";

            p.append(txbCatId);

            var divDel = document.createElement("div");
            divDel.className = "div-extra-del";

            divDel.onclick = function () {
                fn_RemoveDivCat(CatCnt);
            }

            var divDelInner = document.createElement("div");
            divDelInner.className = "div-extra-del-inner";

            divDel.append(divDelInner);

            p.append(divDel);

            divParent.append(p);

            divCatList.append(divParent);

            $("#hdfCatCnt").val(CatCnt);
        };

        var fn_RemoveDivCat = function (key) {

            var div = $("#divCat" +  key);
            div.remove();

            var CatCnt = parseInt($("#hdfCatCnt").val());

            CatCnt -= 1;

            if (CatCnt <= 0) {
                $("#lblCatEmpty").show();
                $("#divCATList").hide();
            }

            $("#hdfCatCnt").val(CatCnt);
        };

        var fn_GetCatList = function () {
            var CatCnt = parseInt($("#hdfCatCnt").val());
            var CatList = '';

            if (CatCnt > 0) {
                var divCatList = $("#divCATList");

                divCatList.find('.terminal-id').each(function (index, item) {
                    var id = $(item).val();

                    if (CatList.length > 0) {
                        CatList += ","
                    }

                    CatList += $("#txbSerial" + id).val() + " : " + $("#txbCatId" + id).val();
                });
            }

            return CatList;
        };

        function btnMemoAddCheck(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var Status = $("#<%= hdfStatus.ClientID %>").val();
            var OrderNo = $("#<%= hdfOrderNo.ClientID %>").val();
            var Content = rowData.CONTENT;

            var bChk = true;

            if (OrderNo == undefined || OrderNo == "") {
                alert("잘못된 접근입니다.");
                bChk = false;
            }
            else if (Content == undefined || Content == "") {
                alert("메모를 입력해주세요.");
                bChk = false;
            }

            if (bChk) {
                if (confirm("메모를 등록 하시겠습니까?")) {
                    InsMemoInfo(OrderNo, Content);
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function InsMemoInfo(OrderNo, Content) {
            var aData = [];

            aData[0] = OrderNo;
            aData[1] = Content;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'InsMemo');

            callGb = 'InsMemoOk';
            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ModalOpen = function () {
            $("#divModal").show();
        }

        var ModalClose = function () {
            $("#divModal").hide();
        }

        var PopupCorpInfo = function () {

            var nWidth = 1000;
            var nHeight = 500;

            var param = {
                pORDERCODE: "1"
            };
            var form = "/Common/SearchCorpPopup";
            var target = "SearchCorp";

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetCorpInfo = function (CorpNm, CorpNo, Addr, AddrDtl, TelNo, PhoneNo) {
            $("#<%= txbFranchiseNm.ClientID %>").val(CorpNm);
            $("#<%= txbCorpNo.ClientID %>").val(CorpNo);
            $("#<%= txbAddr.ClientID %>").val(Addr);
            $("#<%= txbDtlAddr.ClientID %>").val(AddrDtl);
            $("#<%= txbTelNo.ClientID %>").val(TelNo);
            $("#<%= txbPhoneNo.ClientID %>").val(PhoneNo);
        }

        var SaveFileCheck = function () {
            if (confirm("참조 파일들 저장 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        var DelFileCheck = function () {
            if (confirm("선택한 파일을 삭제 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        var AddFileUpload = function () {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:430px;");
            newFileText.style.marginLeft = "0";

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "fileTypeChk('fu" + AddFileUpload.lastAssignedId + "', this.value);");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("accept", "image/*");

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.innerText = "찾아보기";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        }

        var fileTypeChk = function (id, val) {
            var filechk = false;

            var extIndex = val.lastIndexOf('.');

            var extList = ["tiff", "pjp", "jfif", "bmp", "gif", "svg", "png", "xbm", "dib", "jxl", "jpeg", "svgz", "jpg", "webp", "ico", "tif", "pjpeg", "avif"];

            if (extIndex > 1) {

                var fileExt = val.substring(extIndex + 1, val.length).toLowerCase();

                if (extList.indexOf(fileExt) > 0) {
                    filechk = true;
                }
            }
            if (filechk) {
                document.getElementById(id + "-value").value = val;
            }
            else {
                alert("이미지파일만 업로드 가능합니다.");

                document.getElementById(id).value = "";
                document.getElementById(id + "-value").value = "";
            }
        }

        var fn_SetImgTrView = function (bView) {
            if (bView == "Y") {
                $("#<%=tdImgList.ClientID %>").show();
                $("#<%=tdImgAdd.ClientID %>").attr("rowspan", 1);
            }
            else {
                $("#<%=tdImgList.ClientID %>").hide();
                $("#<%=tdImgAdd.ClientID %>").attr("rowspan", 2);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>오더정보 저장</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents" style="padding-top: 10px;">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <div style="text-align:right; margin-bottom:10px;">
                                <input id="btnDel" runat="server" type="button" class="btn-del" style="height:25px;" value="삭제" />
                            </div>
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th class="th-line">접수구분&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlReceiptType" style="width:150px;" runat="server" TabIndex="0">
                                                        <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="KVAN" Value="KVAN"></asp:ListItem>
                                                        <asp:ListItem Text="VAN365" Value="VAN365"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">접수유형&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlRcvType" style="width:150px;" runat="server" TabIndex="1">
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                            <th class="th-line">대리점명&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbAgencyNm" runat="server" Width="200px" MaxLength="20" TabIndex="2"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">사업자이름&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbFranchiseNm" runat="server" Width="180px" MaxLength="150" TabIndex="3"></asp:TextBox>
                                                    <input type="button" id="btnCorpInfo" runat="server" class="td-button" value=".." visible="false" />
                                                </p>
                                            </td>
                                            <th class="th-line" style="width:130px;">사업자등록번호&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCorpNo" runat="server" Width="200px" MaxLength="20" TabIndex="4" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">주소&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbAddr" runat="server" Width="586px" MaxLength="250" TabIndex="5"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">상세주소</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbDtlAddr" runat="server" Width="586px" MaxLength="250" TabIndex="6"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">전화번호</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbTelNo" runat="server" Width="200px" MaxLength="50" TabIndex="7"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th id="thPhone" class="th-line">휴대폰번호</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbPhoneNo" runat="server" Width="200px" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trDevice" runat="server">
                                            <th class="th-line">관련장비</th>
                                            <td colspan="3" style="text-align:left">
                                                <div id="divDevicePOS" class="div-radio" onclick="fn_radioBoxChk('POS');">
                                                    <input type="radio" name="rbDeviceChk" id="rbPOS" value="POS" checked="checked" /><label for="rbPOS" class="label-rbInner">POS</label>
                                                </div>
                                                <div id="divDeviceCAT" class="div-radio" onclick="fn_radioBoxChk('CAT');">
                                                    <input type="radio" name="rbDeviceChk" id="rbCAT" value="CAT" /><label for="rbCAT" class="label-rbInner">CAT</label>
                                                </div>
                                                <div id="divDeviceKIOSK" class="div-radio" onclick="fn_radioBoxChk('KIOSK');">
                                                    <input type="radio" name="rbDeviceChk" id="rbKIOSK" value="KIOSK" /><label for="rbKIOSK" class="label-rbInner">KIOSK</label>
                                                </div>
                                                <div id="divDeviceTablet" class="div-radio" onclick="fn_radioBoxChk('TABLET');">
                                                    <input type="radio" name="rbDeviceChk" id="rbTABLET" value="TABLET" /><label for="rbTABLET" class="label-rbInner">TABLET</label>
                                                </div>     
                                                <asp:HiddenField ID="hdfDevice" runat="server" />                                               
                                            </td>
                                        </tr>
                                        <tr id="trPolicyBasic" runat="server">
                                            <th class="th-line">기본견적</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlPolicyBasic" style="width:586px;" runat="server" TabIndex="9">
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trPolicyExtra" runat="server">
                                            <th class="th-line">추가견적</th>
                                            <td id="tdExtra" colspan="3" style="text-align:left;padding-left:5px">
                                                <div id="divExtraHead" style="width:100%; height: 40px; margin-bottom: 5px; margin-right: 5px; display:none;">
                                                    <p class="inpbox" style="width:518px;height:40px;padding-top:8px;padding-left:10px;">
                                                        <label id="lblExtraHead">추가 견적 조회.</label>
                                                    </p>
                                                    <input id="btnExtraListClose" type="button" class="btn-black" onclick="fn_ExtraListClose();"" value="닫기" />
                                                </div>
                                                <div id="divExtraSearch" style="width:100%; height: 40px; margin-right: 5px;">
                                                    <p id="pExtraSearch" class="inpbox" style="width:518px; height:40px;">
                                                        <label id="lblExtraSearch" style="width:518px; height:40px; padding-top:8px; text-align:center; font-weight:300; font-size: 14px; background-color: #DFDFDF;">추가 요청 사항이 없습니다.</label>
                                                        <input id="txbExtraSearch" style="text-align:left; padding-left: 10px; padding-left: 10px; font-weight:300; font-size: 14px;display:none;" />
                                                    </p>
                                                    <input id="btnExtraList" type="button" class="btn-black" onclick="fn_GetPolicyExtra();" value="불러오기" />
                                                    <input id="btnExtraSearh" type="button" class="btn-black" onclick="fn_GetPolicyExtra();" style="display:none;" value="검색" />
                                                </div>
                                                <div id="divExtraChk" style="width:100%; height: 40px; margin-top: 5px; margin-right: 5px;display:none;">
                                                    <p class="inpbox" style="width:100%; height: 40px; background-color: #EAF7F6;">
                                                        <input type="checkbox" id="chkExtraAll" class="checkbox-extra" style="width: 20px; margin-left: 10px; margin-right: 10px; padding-bottom: 5px;" />
                                                        <label style="text-align: center; font-size: 14px; float: left; padding-top: 8px; width: 470px;">작업 내용</label>
                                                    </p>
                                                </div>
                                                <div id="divExtraList" style="display:none;"">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="trRcvDt" runat="server">
                                            <th class="th-line">접수일</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:Label ID="lblRcvDt" style="font-size:15px; font-weight:bold; padding: 5px 5px 5px; text-align:left;" runat="server" Width="200px"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">파트너사지역&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlPartnersYn" style="width:150px;" runat="server" TabIndex="10">
                                                        <asp:ListItem Text="선택하세요" Value="" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                        <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">방문예정일&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width: 210px">
                                                    <asp:HiddenField ID="hdfRsvDt" runat="server" />
                                                    <asp:TextBox ID="txbRsvDt" runat="server" Width="200px" MaxLength="20" TabIndex="11" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                </p>
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlRsvTime" style="width: 65px;padding: 0 7px;" runat="server" TabIndex="12">
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trTerminalYN" runat="server">
                                            <th class="th-line">단말기보유여부</th>
                                            <td colspan="3" style="text-align:left">
                                                <div id="divTerminalY" runat="server" class="div-radio" style="width:90px;" onclick="fn_radioBoxChk('TerminalY');">
                                                    <input type="radio" name="rbTerminalChk" id="rbTerminalY" value="보유" checked="checked" /><label for="rbTerminalY" class="label-rbInner">보유</label>
                                                </div>
                                                <div id="divTerminalN" runat="server" class="div-radio" style="width:90px;" onclick="fn_radioBoxChk('TerminalN');">
                                                    <input type="radio" name="rbTerminalChk" id="rbTerminalN" value="미보유"/><label for="rbTerminalN" class="label-rbInner">미보유</label>
                                                </div>
                                                <asp:HiddenField ID="hdfTerminalYn" runat="server" Value="TerminalN" />
                                            </td>
                                        </tr>
                                        <tr id="trDispatch" runat="server">
                                            <th class="th-line">출동종류</th>
                                            <td colspan="3" style="text-align:left">
                                                <div id="divDispatchNormal" runat="server" class="div-radio" style="width:90px;" onclick="fn_radioBoxChk('Dispatch_Normal');">
                                                    <input type="radio" name="rbDispatchChk" id="rbDispatch_Normal" value="일반" checked="checked" /><label for="rbDispatch_Normal" class="label-rbInner">일반 출동</label>
                                                </div>
                                                <div id="divDispatchEmergency" runat="server" class="div-radio" style="width:90px;" onclick="fn_radioBoxChk('Dispatch_Emergency');">
                                                    <input type="radio" name="rbDispatchChk" id="rbDispatch_Emergency" value="긴급" /><label for="rbDispatch_Emergency" class="label-rbInner">긴급 출동</label>
                                                </div>
                                                <asp:HiddenField ID="hdfDispatch" runat="server" Value="Dispatch_Normal" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">출동희망일&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <div id="divHolidayChk" runat="server" style="width:100%;height:35px;">
                                                    <div class="div-radio" style="width:140px;">
                                                        <input type="radio" name="rbHolidayChk" id="rbHolidayN" value="평일" checked="checked" disabled="disabled" /><label for="rbHolidayN" class="label-rbInner">평일</label>
                                                    </div>
                                                    <div class="div-radio" style="width:140px;">
                                                        <input type="radio" name="rbHolidayChk" id="rbHolidayY" value="주말" disabled="disabled" /><label for="rbHolidayY" class="label-rbInner">휴일(공휴일포함)</label>
                                                    </div>
                                                </div>
                                                <p class="inpbox" style="width:210px">
                                                    <asp:TextBox ID="txbHopeDt" runat="server" Width="200px" MaxLength="20" TabIndex="13" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                </p>
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlHopeTime" style="width: 65px;padding: 0 7px;" runat="server" TabIndex="14">
                                                    </asp:DropDownList>
                                                </p>
                                                <asp:HiddenField ID="hdfHoliDayFlag" runat="server" Value="rbHolidayN" />
                                            </td>
                                        </tr>
                                        <tr id="trDlvrDt" runat="server">
                                            <th class="th-line">택배배송일&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbDlvrDt" runat="server" Width="200px" MaxLength="20" TabIndex="15" ReadOnly="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trTermialAddr" runat="server">
                                            <th class="th-line">장비발송장소</th>
                                            <td colspan="3" style="text-align:left;" >
                                                <div id="divAddr" runat="server" class="div-radio" style="width:160px" onclick="fn_radioBoxChk('Addr');">
                                                    <input type="radio" name="rbAddrChk" id="rbAddr" value="가맹점" /><label for="rbAddr" class="label-rbInner">가맹점으로 직접 발송</label>
                                                </div>
                                                <div id="divAddrKTS" runat="server" class="div-radio" style="width:160px" onclick="fn_radioBoxChk('AddrKTS');">
                                                    <input type="radio" name="rbAddrChk" id="rbAddrKTS" value="KTS지점" checked="checked" /><label for="rbAddrKTS" class="label-rbInner">KTS 지점을 통해 발송</label>
                                                </div><br /><br />
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbTerminalAddr" runat="server" Width="586px" MaxLength="250" TabIndex="16"></asp:TextBox>
                                                </p>
                                                <asp:HiddenField ID="hdfTerminalAddr" runat="server" Value="AddrKTS" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">접수내용</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbRcvNote" runat="server" Width="586px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="17"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">의뢰사항&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbReqNote" runat="server" Width="200px" MaxLength="250" TabIndex="18"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th class="th-line">상태</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                   <asp:Label style="font-size:15px; font-weight:bold; color:red; padding: 5px 0 5px;" ID="lblWorkState" runat="server" Width="200px"></asp:Label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">회수배송주소&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbRtnAddr" runat="server" Width="586px" MaxLength="250" TabIndex="19"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">회수상세주소</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbRtnDtlAddr" runat="server" Width="586px" MaxLength="250" TabIndex="20"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">특이사항</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbReport" runat="server" Width="586px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="21"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trSwContent" runat="server">
                                            <th class="th-line">사용SW</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbSwContent" runat="server" Width="586px" MaxLength="250" TabIndex="22"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trSwSetting" runat="server">
                                            <th class="th-line">SW세팅여부</th>
                                            <td colspan="3" style="text-align:left">
                                                <div id="divSettingC" runat="server" class="div-radio" style="width:90px;" onclick="fn_radioBoxChk('SettingC');">
                                                    <input type="radio" name="rbSWChk" id="rbSettingC" value="세팅필요" /><label for="rbSettingC" class="label-rbInner">진행</label>
                                                </div>
                                                <div id="divSettingN" runat="server" class="div-radio" style="width:90px;" onclick="fn_radioBoxChk('SettingN');">
                                                    <input type="radio" name="rbSWChk" id="rbSettingN" value="세팅완료" /><label for="rbSettingN" class="label-rbInner">미진행</label>
                                                </div>
                                                <asp:HiddenField ID="hdfSwSetting" runat="server" Value="SettingC" />
                                            </td>
                                        </tr>
                                        <tr id="trSwSettingHost" runat="server">
                                            <th class="th-line">SW세팅주체</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlSWSettingHost" style="width:250px;" runat="server" TabIndex="10" Enabled="false">
                                                        <asp:ListItem Text="선택하세요" Value="" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="엔지니어 자체처리 완결" Value="엔지니어 자체처리 완결"></asp:ListItem>
                                                        <asp:ListItem Text="콜센터 지원 미완결" Value="콜센터 지원 미완결"></asp:ListItem>
                                                        <asp:ListItem Text="대리점 지원 미진행" Value="대리점 지원 미진행"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trVan" runat="server">
                                            <th class="th-line">VAN사</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbVan" runat="server" Width="586px" MaxLength="250" TabIndex="23"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trCatList" runat="server">
                                            <th class="th-line">단말기&CAT_ID</th>
                                            <td colspan="3" style="text-align:left;padding-left:5px">
                                                <div id="divCATHead" style="width:100%; height: 40px; margin-right: 5px;">
                                                    <p id="pCATEmpty" class="inpbox" style="width:518px; height:40px;">
                                                        <label id="lblCatEmpty" style="width:518px; height:40px; padding-top:8px; text-align:center; font-weight:300; font-size: 14px; background-color: #DFDFDF;">CAT/백업단말기를 설치하는경우 단말기 시리얼과 CAT ID를 반드시 입력해야 합니다.</label>
                                                    </p>
                                                    <input id="btnCATAdd" type="button" class="btn-black" onclick="fn_AddCATList();" value="추가" />
                                                </div>
                                                <div id="divCATList" style="display:none;margin-top: 5px; ">
                                                </div>
                                                <input id="hdfCatCnt" type="hidden" value="0" />
                                                <asp:HiddenField ID="hdfCatList" runat="server" Value="" />
                                            </td>
                                        </tr>
                                        <tr id="trAspName" runat="server">
                                            <th class="th-line">ASP이름</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbAspName" runat="server" Width="586px" MaxLength="250" TabIndex="24"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trAspAccount" runat="server">
                                            <th class="th-line">ASP ID</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbAspID" runat="server" Width="200px" MaxLength="100" TabIndex="25"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th class="th-line">ASP PW</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbAspPW" runat="server" Width="200px" MaxLength="100" TabIndex="26"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trSolutionName" runat="server">
                                            <th class="th-line">솔루션이름</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbSolutionName" runat="server" Width="586px" MaxLength="250" TabIndex="27"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trSolutionAccount" runat="server">
                                            <th class="th-line">솔루션 ID</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbSolutionID" runat="server" Width="200px" MaxLength="100" TabIndex="28"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th class="th-line">솔루션 PW</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbSolutionPW" runat="server" Width="200px" MaxLength="100" TabIndex="29"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trProc" runat="server">
                                            <th class="th-line">처리내역</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbProc" runat="server" Width="586px" Height="160px" MaxLength="8000" TextMode="MultiLine" TabIndex="30"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trMemo" runat="server">
                                            <th class="th-line">메모</th>
                                            <td colspan="3" style="text-align:left">
                                                <div class="scrollbox2">
                                                    <div id="ktsGrid"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="th-line">작업자지정</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:HiddenField ID="hdfPreEmpNo" runat="server" />
                                                    <asp:HiddenField ID="hdfEmpNo" runat="server" />
                                                    <asp:HiddenField ID="hdfEmpNm" runat="server" />
                                                    <asp:HiddenField ID="hdfBelong" runat="server" />
                                                    <asp:HiddenField ID="hdfAssignYn" runat="server" />
                                                    <asp:TextBox ID="txbEmpUserNm" runat="server" Width="500px" style="margin-right:10px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                                <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색">+</button>
                                            </td>
                                        </tr>
                                        <tr id="trImg" runat="server">
                                            <th class="th-line" rowspan="2">참조</th>
                                            <td id="tdImgList" runat="server" colspan="3" style="text-align:left" visible="false">
                                                <asp:UpdatePanel ID="updPanelImg" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <%--<span id="spanImg" runat="server">
                                                        </span>--%>
                                                        <asp:Repeater ID="rptImg" runat="server">
                                                            <ItemTemplate>
                                                                <img id="img" runat="server" src='<%# Eval("SRC") %>'  style="max-width:100%;height:auto;" /><br />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr id="trFile" runat="server" visible="false">
                                            <td id="tdImgAdd" runat="server" rowspan="2" colspan="3" style="text-align:left">
                                                <div style="width:100%;max-height:500px;overflow-x:hidden;overflow-y:auto; margin-top:2px; margin-bottom:2px">
                                                    <asp:UpdatePanel ID="updPanelFile" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptFile" runat="server">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSeq" runat="server" Visible="false" Text='<%# Eval("SEQ") %>'></asp:Label>
                                                                    <p class="inpbox" style="width:583px;display:block;">
                                                                        <label style="width:576px;height:40px;padding-top:10px;"><%# Eval("FILENM") %></label>
                                                                        <asp:Button id="btnDelete" runat="server" OnClick="btnDelete_Click" OnClientClick="return DelFileCheck();" style="width: 30px; height: 30px; line-height: 0; color: black; padding: 0; margin: 0;" BorderStyle="None" Text="X" />
                                                                    </p>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div>
                                                    <div id="filebox">
                                                        <p class="filebox" style="margin-right:0;">
                                                            <input type="text" id="fu1-value" style="float:left;width:430px;margin-left:0" />
                                                            <span class="file">
                                                                <input type="file" accept="image/*" id="fu1" name="fu1" onchange="fileTypeChk('fu1', this.value);" /> 
                                                                <label for="fu1">찾아보기</label>
                                                            </span>
                                                            <button type="button" onclick="AddFileUpload();" class="btn-replysave" style="height:40px;width:60px;">추가</button>
                                                        </p>
                                                    </div>
                                                    <asp:Button id="btnImgSave" runat="server" OnClick="btnImgSave_Click" OnClientClick="return SaveFileCheck();" style="height:40px;margin-top:10px;margin-left:220px;border-style:none;float:left" class="btn-green" Text="사진저장" />
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <asp:HiddenField ID="hdfOrderNo" runat="server"/>
                                <asp:HiddenField ID="hdfWorkState" runat="server"/>
                                <asp:HiddenField ID="hdfOrgCd" runat="server"/>
                                <asp:HiddenField ID="hdfStatus" runat="server"/>
                                <asp:HiddenField ID="hdfPolicyBasic" runat="server"/>
                                <asp:HiddenField ID="hdfPolicyExtra" runat="server"/>
                                <asp:HiddenField ID="hdfChkPolicyExtra" runat="server"/>
                                <asp:HiddenField ID="hdfChkPolicyExtra2" runat="server"/>
                                <asp:HiddenField ID="hdfChkSingleCnt" runat="server"/>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력값입니다.
                        <div class="btncenter">
                            <input id="btnReg" runat="server" type="button" class="btn-green" value="저장" />
                            <input id="btnCanCel" runat="server" type="button" class="btn-gray" value="취소" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <div id="divScreenLock" class="div-Screenlock"></div>

        <!-- S: divModal -->
		<div id="divModal" class="scrollbox" style="display:none;position:absolute;width:80%;height:300px;top:30%;left:10%;background:rgb(255, 255, 255);border-radius:10px;border:1px solid gray;">
            <!-- S:popcontents -->
            <div class="requestlist" style="width:90%;text-align:center;margin-left:5%">
                <div class="datalist" style="margin-top:30px;padding-bottom:5px;">
				    <p style="color:black;text-align:left;font-weight:bold;">사유</p>
                    <p class="inpbox">
					    <asp:TextBox ID="txbReason" runat="server" MaxLength="8000" TextMode="MultiLine" placeholder="취소 시 사유를 입력해주세요." style="width:100%;height:140px;margin-bottom:20px"></asp:TextBox> 
				    </p>
                </div>
                <!-- S:btncenter -->
                <div class="btncenter">
                    <input id="btnConfirm" runat="server" type="button" class="btn-green" value="확인" />
                    <input id="btnClose" type="button" class="btn-black" onclick="ModalClose();" value="닫기" />
                </div>
                <!-- //E:btncenter -->
            </div>
		</div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
