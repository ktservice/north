﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.BSL.ExcelImport;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Order;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using OfficeOpenXml;

namespace KTSSolutionWeb
{
    public partial class OrderInfoSts : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPageInit();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }


        private void SetOrgList(string strEmpNo, string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList(strEmpNo, strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetPageInit()
        {
            try
            {
                this.txbStDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");
                string strMonth = this.txbEnDt.Text.Replace("-", "").Substring(0, 6);

                this.hfOrgCd.Value = Session["ORGCD"].ToString();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");

                    SetOrgList("", strMonth);
                }
                else
                {
                    SetOrgList(this.Session["EMPNO"].ToString(), strMonth);

                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;
                }

                this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfSumupDt_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = Request.Form[this.txbEnDt.UniqueID].ToString().Replace("-", "").Substring(0, 6);

                if (!strMonth.Equals(""))
                {
                    if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                    {
                        SetOrgList("", strMonth);
                    }
                    else
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    using (KTSUser user = new KTSUser())
                    {
                        ds = user.GetUserViewOrgMonth2(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strOrgCd = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";

                            strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        }

                        this.hfOrgCd.Value = strOrgCd;
                        this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                    }
                    else
                    {
                        this.hfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            DataSet ds = new DataSet();

            try
            {
                string strOrgCd = aData[0];
                string strStDate = aData[1].Replace("-", "");
                string strEnDate = aData[2].Replace("-", "");
                string strOrderCode = aData[3];
                string strWorkState = aData[4];

                string strAuthID = HttpContext.Current.Session["AUTHID"].ToString();
                string strEmpNo = string.Empty;

                if (!strAuthID.Equals("AUTH0001") && !strAuthID.Equals("AUTH0002") && !strAuthID.Equals("AUTH0003")
                    && !strAuthID.Equals("AUTH0004") && !strAuthID.Equals("AUTH0005"))
                {
                    strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();
                }

                using (OrderMgmt mgmt = new OrderMgmt())
                {
                    ds = mgmt.GetOrderStateInfo(strOrgCd, strStDate, strEnDate, strOrderCode, strWorkState);
                }

                result = Utility.DataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, HttpContext.Current.GetType(), false);
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }

            return result;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                Stream stream = Request.Files["fu"].InputStream;

                if (stream.Length > 0)
                {
                    string strtype = Request.Files["fu"].ContentType;

                    if (strtype.Equals("application/vnd.ms-excel") || strtype.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    {
                        using (ExcelImport excel = new ExcelImport())
                        {
                            using (ExcelPackage excelPackage = new ExcelPackage(stream))
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                if (worksheet.Dimension.End.Column != 12)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업로드 파일 양식이 잘못되었습니다.');", true);
                                }
                                else
                                {
                                    string strEmpNo = Session["EMPNO"].ToString();

                                    excel.ExcelUpLoad("오더정보", worksheet, "tb_order_master_temp", "REGEMPNO", strEmpNo);

                                    using (OrderMgmt mgmt = new OrderMgmt())
                                    {

                                        mgmt.InsertOrderStateInfoExcel(strEmpNo);
                                    }

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExcel", "searchBtnClick();alert('오더정보가 일괄 등록 되었습니다.');", true);
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('엑셀파일만 업로드가 가능합니다.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업로드 할 파일을 선택해주세요.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('엑셀 업로드 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                updPanelOrgCd.Update();
            }
        }
    }
}