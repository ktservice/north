﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SafetyLoseRequest.aspx.cs" Inherits="KTSSolutionWeb.SafetyLoseRequest" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbBaseDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbBaseDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#<%= txbBaseDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#<%= txbBaseDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#selRequest").change(function (e) {
                    if (this.value == '01') {
                        $("#selDt").hide();
                        $("#selMon").hide();
                        $("#lblDate").hide();
                        $("#<%= txbBaseDt.ClientID %>").hide();
                    }
                    else {
                        $("#selDt").show();
                        $("#lblDate").show();
                        if ($("#selDt").val() == 'd') {
                            $("#<%= txbBaseDt.ClientID %>").show();
                            $("#selMon").hide();
                        }
                        else {
                            fn_Month();
                            $("#<%= txbBaseDt.ClientID %>").hide();
                            $("#selMon").show();
                        }
                    }
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "안전용품 교체 신청.xlsx", { sheetName: "안전용품 교체 신청" });
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbBaseDt.ClientID %>").show();
            $("#selMon").hide();

            $("#selDt").hide();
            $("#lblDate").hide();
            $("#<%= txbBaseDt.ClientID %>").hide();
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;
            var cellReqStatusCd = cell.getRow().getData().REQUEST_STATUSCD;
            
            if ($("#selRequest").val() == '01') {
                if (cellReqStatusCd == '00' || cellReqStatusCd == 'AA' || cellReqStatusCd == 'FF') { //00 미신청 //AA 수리진행승인 //FF	승인완료
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='신청' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
                }
                else if (cellReqStatusCd == '11') { //01	신청
                    html = "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='취소' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
                }
            }
            else {
                html = "<input id='btnReg' type='button' onclick='fn_Hist(\"1\", \"" + cellNo + "\");' value='보기' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            }

            return html;
        }

        var fn_CellStatus = function (e, cell) {
            var id = cell.getRow().getData().id;
            var cellReqStatusCd = cell.getRow().getData().REQUEST_STATUSCD;

            if ($("#selRequest").val() == '01') {
                if (!(cellReqStatusCd == '00' || cellReqStatusCd == 'DD')) { //00 미신청 또는 신청취소가 아니면
                    fn_Hist('2', id);
                }
            }

            return '';
        }

        var fn_CellIStatusColor = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            var cellReqStatusCd = cell.getRow().getData().REQUEST_STATUSCD;

            if (!(cellReqStatusCd == '00' || cellReqStatusCd == 'DD')) { //00 미신청 또는 신청취소가 아니면
                cell.getElement().style.color = "#077078";
            }

            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이력일자", field: "HIST_DATE", sorter: "string", width: 140, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "신청자명", field: "CHG_EMPNM", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "신청자사번", field: "CHG_EMPNO", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목순번", field: "ITEMSEQ", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도코드", field: "USECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도", field: "USENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "규격", field: "STANDARD", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태코드", field: "ITEMCD_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태", field: "ITEMNM_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태코드", field: "OPER_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태", field: "OPER_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태코드", field: "HOLD_TYPECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태", field: "HOLD_TYPENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "교체사유", field: "CHG_REASONNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "요청구분", field: "REQ_GBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(신청)", field: "MEMO1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(인수)", field: "MEMO2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(승인)", field: "MEMO3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "신청상태", field: "REQUEST_STATUS", sorter: "string", width: 100, formatter: fn_CellIStatusColor, cellClick: fn_CellStatus },
                { headerHozAlign: "center", hozAlign: "center", title: "이력순번", field: "HIST_SEQ", visible: false },
                { field: "REQUEST_STATUSCD", formatter: fn_CellReg, width: 100, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            aData[5] = '';
            aData[6] = '';
            aData[7] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Hist = function (gubun, id) {
            fn_PopupHist(gubun, id);
        }

        var fn_Reg = function (id) {
            fn_PopupReg(id);
        }

        var fn_Del = function (id) {
            if (confirm("취소 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('D', id);
            }
        }

        var fn_Transaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var giveDate = rowData.GIVE_DATE;
            var itemCd = rowData.ITEMCD;
            var itemSeq = rowData.ITEMSEQ;
            var empno = rowData.EMPNO;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = giveDate;
            aData[2] = itemCd;
            aData[3] = itemSeq;
            aData[4] = '';
            aData[5] = '';
            aData[6] = '';
            aData[7] = '';
            aData[8] = '';
            aData[9] = '';
            aData[10] = empno;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'delTran') {
                ktsGridAlert("취소를 완료했습니다.");
                fn_Search();

                callGb = '';
            }
        }

        var SetDispItem = function (check, cellNo, reqGbNm, chgReasonNm, memo) {

            if (check == 'ok') {

                var row = ktsGrid.getRow(cellNo);
                row.select();

                ktsGrid.updateData([{
                    id: cellNo, REQUEST_STATUSCD: '11', REQUEST_STATUS: '교체신청', CHG_REASONNM: chgReasonNm, REQ_GBNM: reqGbNm, MEMO: memo
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

                callGb = '';
            }
        }

        var fn_PopupHist = function (gubun, id) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vHistSeq = rowData.HIST_SEQ;
            var vGIVE_DATE = rowData.GIVE_DATE;
            var vEMPNO = rowData.EMPNO;
            var vITEMCD = rowData.ITEMCD;
            var vITEMSEQ = rowData.ITEMSEQ;

            
            if (vHistSeq == undefined) {
                vHistSeq = '';
            }

            var form = "/Safety/SafetyRequestUserHistPopup";
            var target = "SafetyRequestUserHistPopup";
            var param = {
                pGubun: gubun,
                pIId: vHistSeq,
                pGiveDate: vGIVE_DATE,
                pEmpNo: vEMPNO,
                pItemCd: vITEMCD,
                pItemSeq: vITEMSEQ
            };

            var nWidth = 720;
            var nHeight = 1100;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupReg = function (id) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vGiveDate = rowData.GIVE_DATE;
            var vOrgNm = rowData.ORGNM;
            var vEmpNm = rowData.EMPNM;
            var vEmpNO = rowData.EMPNO;
            var vEmpJobNm = rowData.JOBNM;
            var vItemNm = rowData.ITEMNM;
            var vItemCd = rowData.ITEMCD;
            var vItemSeq = rowData.ITEMSEQ;

            var form = "/Safety/SafetyLoseRequestPopup";
            var target = "SafetyLoseRequestPopup";
            var param = {
                pIId: id,
                pOrgNm: vOrgNm,
                pEmpNm: vEmpNm,
                pEmpNo: vEmpNO,
                oEmpJobNm: vEmpJobNm,
                pGiveDate: vGiveDate,
                pItemNm: vItemNm,
                pItemCd: vItemCd,
                pItemSeq: vItemSeq,
            };

            var nWidth = 720;
            var nHeight = 680;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Safety";
                var target = "OrgTree_Safety";
                var param = {
                    pORGCD: orgcd,
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            //var BaseMonth = $("#<%= txbBaseDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            /*
            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            */
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate = '';

                if ($("#selDt").val() == 'd') {
                    pDate = $("#<%= txbBaseDt.ClientID %>").val();
                }
                else {
                    pDate = $("#selMon").val();
                }

                if ($("#selRequest").val() == '01') {
                    pDate = '';
                }

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[2] = pDate;
                aData[3] = $("#selRequest").val();
                aData[4] = '';
                aData[5] = '';
                aData[6] = '';
                aData[7] = '';
                

                if ($("#selRequest").val() == '01') {
                    ktsGrid.hideColumn("HIST_DATE");
                    ktsGrid.hideColumn("CHG_EMPNM");
                    ktsGrid.hideColumn("CHG_EMPNO");
                    ktsGrid.hideColumn("MEMO1");
                    ktsGrid.hideColumn("MEMO2");
                    ktsGrid.hideColumn("MEMO3");

                    ktsGrid.showColumn("USECD");
                    ktsGrid.showColumn("USENM");
                    ktsGrid.showColumn("STANDARD");
                    ktsGrid.showColumn("UNIT");
                    ktsGrid.showColumn("SERVICELIFE");
                    ktsGrid.showColumn("ITEMCD_STATUSCD");
                    ktsGrid.showColumn("ITEMNM_STATUSNM");
                    ktsGrid.showColumn("OPER_STATUSCD");
                    ktsGrid.showColumn("OPER_STATUSNM");
                    ktsGrid.showColumn("HOLD_TYPECD");
                    ktsGrid.showColumn("HOLD_TYPENM");
                    ktsGrid.showColumn("MEMO");
                }
                else {
                    ktsGrid.showColumn("HIST_DATE");
                    ktsGrid.showColumn("CHG_EMPNM");
                    ktsGrid.showColumn("CHG_EMPNO");
                    ktsGrid.showColumn("MEMO1");
                    ktsGrid.showColumn("MEMO2");
                    ktsGrid.showColumn("MEMO3");

                    ktsGrid.hideColumn("USECD");
                    ktsGrid.hideColumn("USENM");
                    ktsGrid.hideColumn("STANDARD");
                    ktsGrid.hideColumn("UNIT");
                    ktsGrid.hideColumn("SERVICELIFE");
                    ktsGrid.hideColumn("ITEMCD_STATUSCD");
                    ktsGrid.hideColumn("ITEMNM_STATUSNM");
                    ktsGrid.hideColumn("OPER_STATUSCD");
                    ktsGrid.hideColumn("OPER_STATUSNM");
                    ktsGrid.hideColumn("HOLD_TYPECD");
                    ktsGrid.hideColumn("HOLD_TYPENM");
                    ktsGrid.hideColumn("MEMO");
                }

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }




    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox">
                    <label>구분</label>
                    <select id="selRequest">
                        <option selected="selected" value="01">신청대상</option>
                        <option value="02">이력</option>
                    </select>
                </span>
                <span class="optionbox">
                <label id="lblDate">일자</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="d">일</option>
                    <option value="m">월</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbBaseDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>안전용품 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="GIVE_DATE">지급일자</option>
                            <option value="ORGNM">소속</option>
                            <option value="EMPNM">이름</option>
                            <option value="EMPNO">사번</option>
                            <option value="KTSEMPNO">KTS사번</option>
                            <option value="ITEMNM">품목</option>
                            <option value="SERVICELIFE">내용연수</option>
                            <option value="REQUEST_STATUS">신청상태</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
