﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyApprovalMngPopup.aspx.cs" Inherits="KTSSolutionWeb.SafetyApprovalMngPopup" enableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>안전용품승인</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        /* 모달 창 스타일 */
        .img-modal {
          display: none; /* 초기에는 보이지 않음 */
          position: fixed;
          z-index: 1;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          overflow: auto;
          background-color: rgb(0,0,0);
          background-color: rgba(0,0,0,0.9);
        }

        /* 모달 내부 이미지 스타일 */
        .img-modal-content {
          width: 100%;
          transition: transform 0.3s ease;
        }

        /* 모달 내용 스타일 */
        .img-modal-contentDiv {
          margin: auto;
          width: 50%;
          padding: 20px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoom {
          position: absolute;
          top: 15px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoomIn, .img-modal-zoomOut {
          top: 15px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          padding-left: 10px;
          cursor:pointer;
        }

        /* 닫기 버튼 스타일 */
        .img-modal-close {
          position: absolute;
          top: 15px;
          right: 35px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          cursor:pointer;
        }

        .img-modal-close:hover,
        .img-modal-close:focus {
          color: #bbb;
          text-decoration: none;
          cursor: pointer;
        }


        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type="text"]:read-only, textarea:read-only {
          background : #e3e3e3;
          border: solid 1px #9e9e9e;
        }

        .filebox {
            margin-right: 0;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var init;
        var mngChk = false;
        var check = '';

        var req;
        var requestStatusCd;
        var callGb;

        var currentScale = 1; // 이미지 확대/축소 버튼 클릭 이벤트 핸들러
        var imageCall = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                // 이미지를 클릭했을 때 모달을 보여주는 이벤트 처리
                $("#imgFileView1").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });

                $("#imgFileView2").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });

                $("#imgFileView3").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });

                $(".img-modal-zoomIn").on("click", function () {
                    currentScale += 0.1;
                    $("#imgModal").css("transform", "scale(" + currentScale + ")");
                });

                $(".img-modal-zoomOut").on("click", function () {
                    if (currentScale > 0.1) {
                        currentScale -= 0.1;
                        $(".img-modal-content").css("transform", "scale(" + currentScale + ")");
                    }
                });

                // 모달 닫기 버튼을 클릭했을 때 모달을 닫는 이벤트 처리
                $(".img-modal-close").on("click", function () {
                    $("#divImgViewZoom").css("display", "none");
                    currentScale = 1;
                });


                $("#txtMemo1").change(function () {
                    $("#hdnMemo1").val(this.value);
                });

                $("#txtMemo2").change(function () {
                    $("#hdnMemo2").val(this.value);
                });

                $("#txtMemo3").change(function () {
                    $("#hdnMemo3").val(this.value);
                });

                $("#selReq").change(function (e) {
                    $("#hdnSelReq").val(this.value);
                    if (this.value == '02') {
                        $("#selReq2").val('FF').prop("selected", true);
                        $('#hdnSelReq2').val($("#selReq2").val());

                        $("#thReason").hide();
                        $("#tdReason").hide();

                        if (requestStatusCd == '01' || requestStatusCd == '03' || requestStatusCd == 'FF') {
                            $("#trFile1").show();
                            $("#trFile2").show();

                            var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();

                            if (vFileSeq1 != '') {
                                $("#imgFile1").show(); //삭제1
                                $("#pFilebox1").hide();
                            }
                            else {
                                $("#imgFile1").hide(); //삭제1
                                $("#pFilebox1").show();
                            }
                        }
                        else {
                            $("#trFile1").hide();
                            $("#trFile2").show();
                        }
                    }
                    else {
                        $("#thReason").show();
                        $("#tdReason").show();
                    }
                });

                $("#selChgReason").change(function (e) {
                    $("#hdnChgReason").val(this.value);
                });


                $("#selReq2").change(function (e) {
                    $('#hdnSelReq2').val(this.value);

                    if ($('#hdnSelReq').val() == '02') {
                        if (this.value != 'FF') {
                            $("#selReq2").val('FF').prop("selected", true);

                            ktsGridAlert("요청구분이 불용인 경우 승인구분을 처리완료만 선택가능합니다.");
                            return false;
                        }
                    }
                    else {
                        if (this.value == 'FF' && (requestStatusCd == '01' || requestStatusCd == '03')) {
                            $("#trFile1").show();
                            $("#trFile2").show();

                            var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();

                            if (vFileSeq1 != '') {
                                $("#imgFile1").show(); //삭제1
                                $("#pFilebox1").hide();
                            }
                            else {
                                $("#imgFile1").hide(); //삭제1
                                $("#pFilebox1").show();
                            }
                        }
                        else {
                            $("#trFile1").hide();
                            $("#trFile2").show();
                        }

                    }
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran('U');
                });

                $("#btnMod").click(function (e) {
                    fn_RegTran('M');
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });


                $("#aFile1").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq1.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile2").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq2.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile3").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq3.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#imgFile1").click(function (e) {
                    $("#<%= hdnFileSeq1.ClientID %>").val('');
                    $('#aFile1').text('');
                    $("#pFilebox1").show();
                    $("#pFileA1").hide();
                    $('#txtfu1').val('');
                });

                $("#imgFile2").click(function (e) {
                    $("#<%= hdnFileSeq2.ClientID %>").val('');
                    $('#aFile2').text('');
                    $("#pFilebox2").show();
                    $("#pFileA2").hide();
                    $('#txtfu2').val('');
                });

                $("#imgFile3").click(function (e) {
                    $("#<%= hdnFileSeq3.ClientID %>").val('');
                    $('#aFile3').text('');
                    $("#pFilebox3").show();
                    $("#pFileA3").hide();
                    $('#txtfu3').val('');
                });
                
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';

            $("#imgFileView1").css("width", "0");
            $("#imgFileView1").css("height", "0");
            $("#imgFileView2").css("width", "0");
            $("#imgFileView2").css("height", "0");
            $("#imgFileView3").css("width", "0");
            $("#imgFileView3").css("height", "0");
        }

        var fn_FileAdd = function (seq) {

            if (seq == 1) {
                if ($('#txtfu1').val() == '') {
                    ktsGridAlert('첨부파일이 없습니다.');
                    return false;
                }

                if ($('#aFile2').text() == '') {
                    $("#<%= hdnFileSeq2.ClientID %>").val('');
                }
                if ($('#aFile3').text() == '') {
                    $("#<%= hdnFileSeq3.ClientID %>").val('');
                }
            }
            else if (seq == 2) {
                if ($('#txtfu2').val() == '') {
                    ktsGridAlert('첨부파일이 없습니다.');
                    return false;
                }

                if ($('#aFile1').text() == '') {
                    $("#<%= hdnFileSeq1.ClientID %>").val('');
                }
                if ($('#aFile3').text() == '') {
                    $("#<%= hdnFileSeq3.ClientID %>").val('');
                }
            }
            else if (seq == 3) {
                if ($('#txtfu3').val() == '') {
                    ktsGridAlert('첨부파일이 없습니다.');
                    return false;
                }

                if ($('#aFile1').text() == '') {
                    $("#<%= hdnFileSeq1.ClientID %>").val('');
                }
                if ($('#aFile2').text() == '') {
                    $("#<%= hdnFileSeq2.ClientID %>").val('');
                }
            }

            return true;
        }

        var fn_Load = function () {
            fn_MngChk();

            $("#txtMemo1").val($("#hdnMemo1").val());
            $("#txtMemo2").val($("#hdnMemo2").val());
            $("#txtMemo3").val($("#hdnMemo3").val());

            requestStatusCd = $("#hdnRequestStatusCd").val(); //신청상태코드 : 01(상위부서요청) ,02(인수처리중) ,AA(수리진행승인) ,11(교체신청) ,03(인수완료), FF(처리완료)
            req = $("#hdnReq").val(); //요청코드 : 01(교체) ,02(불용)

            var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
            var vFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
            var vFileSeq3 = $("#<%= hdnFileSeq2.ClientID %>").val();

            //hdnGubun 1(승인), 2(보기), 3(관리자수정)
            if ($('#hdnGubun').val() == '2') { //보기
                $("#trFile1").show(); //인수증파일
                $("#trFile2").show(); //품목파일
                $("#trFile3").show(); //첨부파일
                $("#pFilebox1").hide(); //인수증파일(찾아보기)
                $("#pFilebox2").hide(); //품목파일(찾아보기)
                $("#pFilebox3").hide(); //첨부파일(찾아보기)
                $("#imgFile1").hide(); //삭제1
                $("#imgFile2").hide(); //삭제2
                $("#imgFile3").hide(); //삭제3
            }
            else if ($('#hdnGubun').val() == '1') { //승인
                $("#trFile1").hide(); //인수증파일
                $("#trFile2").show(); //품목파일
                $("#trFile3").show(); //첨부파일
                $("#pFilebox1").hide(); //인수증파일(찾아보기)
                $("#pFilebox2").hide(); //품목파일(찾아보기)
                $("#pFilebox3").hide(); //첨부파일(찾아보기)
                $("#imgFile1").hide(); //삭제1
                $("#imgFile2").hide(); //삭제2
                $("#imgFile3").hide(); //삭제3

                if (vFileSeq1 != '') {
                    $("#trFile1").show(); //인수증파일
                    if (mngChk) $("#imgFile1").show(); //삭제1 관리자일 경우
                    $("#pFilebox1").hide();
                }

                //if (requestStatusCd == '01') { //상위부서요청
                //}
                if (requestStatusCd == '03') { //인수완료
                    if (vFileSeq1 != '') {
                        $("#trFile1").show(); //인수증파일
                        if (mngChk) $("#imgFile1").show(); //삭제1 관리자일 경우
                        $("#pFilebox1").hide();
                    }
                    else {
                        $("#trFile1").show(); //인수증파일
                        if (mngChk) $("#imgFile1").hide(); //삭제1 관리자일 경우
                        $("#pFilebox1").show();
                    }
                }

                if (mngChk) {
                    $("#selReq").show();
                    $("#txtReq").hide();
                    $("#selReq2").show();
                    $("#txtReq2").hide();
                    $("#selChgReason").show();
                    $("#txtChgReason").hide();

                    $("#txtChgReason").removeAttr("readonly");
                    $("#txtMemo1").removeAttr("readonly");
                    $("#txtMemo2").removeAttr("readonly");
                    $("#txtMemo3").removeAttr("readonly");

                    if ($("#hdnReq").val() == '02') {
                        $("#thReason").hide();
                        $("#tdReason").hide();
                    }
                    else {
                        $("#thReason").show();
                        $("#tdReason").show();
                    }
                }
                else {
                    $("#selReq").hide();
                    $("#txtReq").show();
                    $("#selReq2").show();
                    $("#txtReq2").hide();
                    $("#selChgReason").hide();
                    $("#txtChgReason").show();
                }
                
            }
            else if ($('#hdnGubun').val() == '3') { //관리자모드

                $("#trFile1").hide(); //인수증파일
                $("#trFile2").show(); //품목파일
                $("#trFile3").show(); //첨부파일
                $("#pFilebox1").hide(); //인수증파일(찾아보기)
                $("#pFilebox2").hide(); //품목파일(찾아보기)
                $("#pFilebox3").hide(); //첨부파일(찾아보기)
                $("#imgFile1").hide(); //삭제1
                $("#imgFile2").hide(); //삭제2
                $("#imgFile3").hide(); //삭제3

                if (vFileSeq1 != '') {
                    $("#trFile1").show(); //인수증파일
                    $("#imgFile1").show(); //삭제1
                    $("#pFilebox1").hide();
                }

                if (vFileSeq2 != '') {
                    $("#trFile2").show(); //품목파일
                    $("#imgFile2").show(); //삭제2
                    $("#pFilebox2").hide();
                }
                else {
                    $("#trFile2").show(); //품목파일
                    $("#imgFile2").hide(); //삭제2
                    $("#pFilebox2").show();
                }

                if (vFileSeq3 != '') {
                    $("#trFile3").show(); //첨부파일
                    $("#imgFile3").show(); //삭제3
                    $("#pFilebox3").hide();
                }
                else {
                    $("#trFile3").show(); //첨부파일
                    $("#imgFile3").hide(); //삭제3
                    $("#pFilebox3").show();
                }

                if (requestStatusCd == 'FF') { //처리완료
                    if (vFileSeq1 == '') {
                        $("#trFile1").show(); //인수증파일
                        $("#imgFile1").hide(); //삭제1
                        $("#pFilebox1").show();
                    }
                }

                $("#selReq").show();
                $("#txtReq").hide();
                $("#selReq2").show();
                $("#txtReq2").hide();
                $("#selChgReason").show();
                $("#txtChgReason").hide();

                $("#txtChgReason").removeAttr("readonly");
                $("#txtMemo1").removeAttr("readonly");
                $("#txtMemo2").removeAttr("readonly");
                $("#txtMemo3").removeAttr("readonly");

                if ($("#hdnReq").val() == '02') {
                    $("#thReason").hide();
                    $("#tdReason").hide();
                }
                else {
                    $("#thReason").show();
                    $("#tdReason").show();
                }

                $("#trReq22").show();

                $("#selReq2").prop("disabled", true);
                var obj1 = $('#selReq22');
                obj1.empty();
                obj1.append("<option value='01'>상위부서요청</option>");
            }

            //값이 없으면 버튼 없음
            if (vFileSeq1 != '') {
                $("#imgFile1").hide(); //삭제1
            }

            if (vFileSeq2 != '') {
                $("#imgFile2").hide(); //삭제2
            }

            if (vFileSeq2 != '') {
                $("#imgFile2").hide(); //삭제2
            }


            fn_Req();
            
        }

        var fn_MngChk = function () {
            var orgLv = $("#<%= hdnOrgLv.ClientID %>").val();
            var disuseCd = $("#<%= hdnDisuseCd.ClientID %>").val();

            mngChk = false;

            if (disuseCd == undefined) disuseCd = '';
            else if (disuseCd == null) disuseCd = '';

            if (orgLv == 0 || orgLv == 1) {
                if (disuseCd != '') {
                    mngChk = true;
                }
            }
        }

        var fn_RegTran = function (gubun) {
            if (fn_RegChk(gubun)) {
                if (confirm("진행하시겠습니까?")) {

                    if (gubun == 'M') {
                        callGb = 'ModTran';
                    }
                    else {
                        callGb = 'RegTran';
                    }
                    

                    fn_Transaction(gubun);
                }
            }
        }

        var fn_RegChk = function (gubun) {
            if (gubun == 'U') { //등록일 경우
                if ($("#selReq2 option:selected").val() == 'FF') {

                    if (req != '02') {//불용이 아닐때
                        if ($('#aFile1').text() == '') {

                            ktsGridAlert('인수증파일을 첨부해야 합니다.');
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        var fn_GetImage = function (fileNmSeq) {
            callGb = 'getImage';

            //입력 파라미터
            var aData = [];
            aData[0] = fileNmSeq;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl('/Safety/SafetyRequestUserHistPopup', 'GetImage');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ChgReason = function () {
            callGb = 'selChgReason';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S08';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Req = function () {
            callGb = 'selReq';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S09';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Req2 = function () {
            callGb = 'selReq2';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S10';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_FileDownload = function () {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }


        var fn_Transaction = function (gubun) {
            var pGubun = gubun;
            var pOrgLv = $("#hdnOrgLv").val(); //
            var pGiveDate = $("#txtGiveDate").val();
            var pItemCd = $("#hdnItemCd").val();
            var pItemSeq = $("#txtItemSeq").val();
            var pReqStatusCd = $("#selReq2").val();
            var pReqStatusNm = $("#selReq2 option:selected").text(); 
            var pReqGb = $("#selReq").val(); //
            var pReqGbNm = $("#selReq option:selected").text(); //
            var pChgReasonCd = $("#selChgReason").val(); //
            var pChgReasonNm = $("#selChgReason option:selected").text(); //
            var pFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
            var pFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
            var pFileSeq3 = $("#<%= hdnFileSeq3.ClientID %>").val();
            var pMemo = $("#txtMemo3").val();
            var pMemo1 = $("#txtMemo1").val(); //
            var pMemo2 = $("#txtMemo2").val(); //
            var pEmpNo = $("#hdnEmpNo").val();

            //hdnGubun 1(승인), 2(보기), 3(관리자수정)
            if ($('#hdnGubun').val() == '3') { //관리자수정
                pReqStatusCd = $("#selReq22").val();
                pReqStatusNm = $("#selReq22 option:selected").text();
            }

            var aData = [];
            aData[0] = pGubun;
            aData[1] = pOrgLv;
            aData[2] = pGiveDate;
            aData[3] = pItemCd;
            aData[4] = pItemSeq;
            aData[5] = pReqStatusCd;
            aData[6] = pReqStatusNm;
            aData[7] = pReqGb;
            aData[8] = pReqGbNm;
            aData[9] = pChgReasonCd;
            aData[10] = pChgReasonNm;
            aData[11] = pFileSeq1;
            aData[12] = pFileSeq2;
            aData[13] = pFileSeq3;
            aData[14] = pMemo;
            aData[15] = pMemo1;
            aData[16] = pMemo2;
            aData[17] = pEmpNo;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;
            var obj3;

            if (callGb == 'selReq2') {
                obj1 = $('#selReq2');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                var vSelReq2 = $('#hdnSelReq2').val();
                var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
                var vFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
                var vFileSeq3 = $("#<%= hdnFileSeq3.ClientID %>").val();
                var vFileFirstSeq1 = $("#<%= hdnFileFirstSeq1.ClientID %>").val();
                var vFileFirstSeq2 = $("#<%= hdnFileFirstSeq2.ClientID %>").val();
                var vFileFirstSeq3 = $("#<%= hdnFileFirstSeq3.ClientID %>").val();

                var selData = '';

                if (vSelReq2 != undefined) {
                    selData = vSelReq2;
                }

                if (selData != '') {
                    obj1.val(selData).prop("selected", true);
                }


                if ($('#hdnGubun').val() != '2') {

                    if (requestStatusCd == '03' || req == '02') { // 인수완료, 불용
                        obj1.val('FF').prop("selected", true);
                    }
                    else {
                        obj1.val(vSelReq2);

                        // obj1의 선택 여부 확인
                        if (!obj1.val()) {
                            obj1.val($("#selReq2 option:first").val());
                        }

                        obj1.prop("selected", true);
                    }


                    if ($('#hdnLoaded').val() == undefined || $('#hdnLoaded').val() == '') {

                        if (vFileSeq1 != '') {
                            $("#trFile1").show(); //인수증파일
                            $("#pFilebox1").hide();
                        }

                        if (vFileSeq2 != '') {
                            $("#trFile2").show(); //품목파일
                            $("#pFilebox2").hide();
                        }
                        else {
                            $("#trFile2").show(); //품목파일
                            $("#imgFile2").hide(); //삭제2
                            $("#pFilebox2").show();
                        }

                        if (vFileSeq3 != '') {
                            $("#trFile3").show(); //첨부파일
                            $("#pFilebox3").hide();
                        }
                        else {
                            $("#trFile3").show(); //첨부파일
                            $("#imgFile3").hide(); //삭제3
                            $("#pFilebox3").show();
                        }

                        //hdnGubun 1(승인), 2(보기), 3(관리자수정)
                        if ($('#hdnGubun').val() == '1' && mngChk) { //승인

                            if (vFileFirstSeq1 != '') {
                                $("#imgFile1").show(); //삭제1
                            }

                            if (vFileFirstSeq2 != '') {
                                $("#imgFile2").show(); //삭제2
                            }

                            if (vFileFirstSeq3 != '') {
                                $("#imgFile3").show(); //삭제3
                            }
                        }
                        else if ($('#hdnGubun').val() == '3') { //관리자수정

                            if (vFileFirstSeq1 != '') {
                                $("#imgFile1").show(); //삭제1
                            }

                            if (vFileFirstSeq2 != '') {
                                $("#imgFile2").show(); //삭제2
                            }

                            if (vFileFirstSeq3 != '') {
                                $("#imgFile3").show(); //삭제3
                            }
                        }

                        $('#hdnLoaded').val('Y');
                    }
                    else {

                        if (vFileSeq1 != '') {
                            $("#trFile1").show(); //인수증파일
                            $("#pFilebox1").hide();
                        }
                        else {
                            $("#pFileA1").text('');
                            $("#pFileA1").hide();
                        }
                            

                        if (vFileSeq2 != '') {
                            $("#trFile2").show(); //품목파일
                            $("#pFilebox2").hide();
                        }
                        else {
                            $("#trFile2").show(); //품목파일
                            $("#imgFile2").hide(); //삭제2
                            $("#pFilebox2").show();
                            $("#pFileA2").text('');
                            $("#pFileA2").hide();
                        }

                        if (vFileSeq3 != '') {
                            $("#trFile3").show(); //첨부파일
                            $("#pFilebox3").hide();
                        }
                        else {
                            $("#trFile3").show(); //첨부파일
                            $("#imgFile3").hide(); //삭제3
                            $("#pFilebox3").show();
                            $("#pFileA3").text('');
                            $("#pFileA3").hide();
                        }

                        $("#imgFile1").hide();
                        $("#imgFile2").hide();
                        $("#imgFile3").hide();

                        if ($("#selReq").val() == '02') { //불용
                            $("#thReason").hide();
                            $("#tdReason").hide();
                        }
                        else {
                            $("#thReason").show();
                            $("#tdReason").show();
                        }
                        

                        if ($("#selReq2").val() == 'FF' && (requestStatusCd == '01' || requestStatusCd == '03' || requestStatusCd == 'FF')) {
                            $("#trFile1").show();
                            $("#trFile2").show();

                            var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();

                            if (vFileSeq1 != '') {
                                $("#imgFile1").show(); //삭제1
                                $("#pFilebox1").hide();
                            }
                            else {
                                $("#imgFile1").hide(); //삭제1
                                $("#pFilebox1").show();
                            }

                        }

                        if ($("#pFileA1").is(":visible")) {
                            $("#pFilebox1").hide();
                            $("#pFileA1").show();
                            if (mngChk) $("#imgFile1").show();

                        } else {
                            $("#pFilebox1").show();
                            $("#pFileA1").hide();
                        }

                        if ($("#pFileA2").is(":visible")) {
                            $("#pFilebox2").hide();
                            $("#pFileA2").show();
                            if (mngChk) $("#imgFile2").show();

                        } else {
                            $("#pFilebox2").show();
                            $("#pFileA2").hide();
                        }

                        if ($("#pFileA3").is(":visible")) {
                            $("#pFilebox3").hide();
                            $("#pFileA3").show();
                            if (mngChk) $("#imgFile3").show();

                        } else {
                            $("#pFilebox3").show();
                            $("#pFileA3").hide();
                        }

                    }
                    
                }

                if ($('#aFile1').text() != '') {
                    imageCall = 1;
                    fn_GetImage(vFileSeq1);
                }
                else if ($('#aFile2').text() != '') {
                    imageCall = 2;
                    fn_GetImage(vFileSeq2);
                }
                else if ($('#aFile3').text() != '') {
                    imageCall = 3;
                    fn_GetImage(vFileSeq3);
                }
                else {
                    callGb = '';
                }
            }
            else if (callGb == 'getImage')
            {
                var vFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
                var vFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
                var vFileSeq3 = $("#<%= hdnFileSeq3.ClientID %>").val();

                $.each(JSON.parse(json), function (idx, item) {
                    if (imageCall == 1) {
                        $('#imgFileView1').attr('src', item.SRC);
                        $("#imgFileView1").css("width", "20px");
                        $("#imgFileView1").css("height", "20px");
                    }
                    else if (imageCall == 2) {
                        $('#imgFileView2').attr('src', item.SRC);
                        $("#imgFileView2").css("width", "20px");
                        $("#imgFileView2").css("height", "20px")
                    }
                    else if (imageCall == 3) {
                        $('#imgFileView3').attr('src', item.SRC);
                        $("#imgFileView3").css("width", "20px");
                        $("#imgFileView3").css("height", "20px")
                    }
                });

                if (imageCall == 1) {
                    if ($('#aFile2').text() != '') {
                        imageCall = 2;
                        fn_GetImage(vFileSeq2);
                    }
                    else if ($('#aFile3').text() != '') {
                        imageCall = 3;
                        fn_GetImage(vFileSeq3);
                    }
                }
                else if (imageCall == 2) {
                    if ($('#aFile3').text() != '') {
                        imageCall = 3;
                        fn_GetImage(vFileSeq3);
                    }
                    else {
                        imageCall = 0;
                    }
                }
                else {
                    imageCall = 0;
                    callGb = '';
                }
                
            }
            else if (callGb == 'selReq') {
                obj2 = $('#selReq');
                obj2.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj2.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                var selData = '';

                if ($("#hdnSelReq").val() != undefined) {
                    selData = $("#hdnSelReq").val();
                }

                if (selData != '') {
                    obj2.val(selData).prop("selected", true);
                }

                fn_ChgReason();
            }
            else if (callGb == 'selChgReason') {
                obj3 = $('#selChgReason');
                obj3.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj3.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                var selData = '';

                if ($("#hdnChgReason").val() != undefined) {
                    selData = $("#hdnChgReason").val();
                }

                if (selData != '') {
                    obj3.val(selData).prop("selected", true);
                }

                fn_Req2();
            }
            else if (callGb == 'RegTran' || callGb == 'ModTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {

                        if (callGb == 'ModTran') {
                            ktsGridAlert('변경되었습니다.');
                        }
                        else {
                            ktsGridAlert('등록되었습니다.');
                        }
                        

                        setTimeout(function () {
                            SendParentsForm();
                        }, 1000);
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {

            var cellNo = $("#hdnId").val();
            var pChgReasonCd = $("#selChgReason").val(); //
            var pChgReasonNm = $("#selChgReason option:selected").text(); //
            var pReqGb = $("#selReq").val(); //
            var pReqGbNm = $("#selReq option:selected").text();// 
            var pMemo = $("#txtMemo3").val();
            var pMemo1 = $("#txtMemo1").val();
            var pMemo2 = $("#txtMemo2").val();
            var pFilenm1 = $('#aFile1').text();
            var pFileSeq1 = $("#<%= hdnFileSeq1.ClientID %>").val();
            var pFilenm2 = $('#aFile2').text();
            var pFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
            var pFilenm3 = $('#aFile3').text();
            var pFileSeq3 = $("#<%= hdnFileSeq3.ClientID %>").val();
            var pReqStatusCd = $("#selReq2").val(); //
            var pReqStatusNm = $("#selReq2 option:selected").text();//

            //hdnGubun 1(승인), 2(보기), 3(관리자수정)
            if ($('#hdnGubun').val() == '3') { //관리자수정
                pReqStatusCd = $("#selReq22").val();
                pReqStatusNm = $("#selReq22 option:selected").text();
            }

            if ($("#txtReq").is(":visible")) {
                pReqGb = $("#hdnSelReq").val();
                pReqGbNm = $("#txtReq").val();
            }

            if ($("#txtChgReason").is(":visible")) {
                pReqGb = $("#hdnChgReason").val();
                pReqGbNm = $("#txtChgReason").val();
            }

            if ($("#txtChgReason").is(":visible")) {
                pReqGb = $("#hdnChgReason").val();
                pReqGbNm = $("#txtChgReason").val();
            }

            opener.SetDispItem(check, cellNo, pChgReasonCd, pChgReasonNm, pReqGb, pReqGbNm, pMemo, pMemo1, pMemo2, pFilenm1, pFileSeq1, pFilenm2, pFileSeq2, pFilenm3, pFileSeq3, pReqStatusCd, pReqStatusNm);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>안전용품</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">소속</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                    <input type="hidden" id="hdnGubun" runat="server" />
                                                    <input type="hidden" id="hdnRequestStatusCd" runat="server" />
                                                    <input type="hidden" id="hdnReq" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">직무</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpJobNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">품목순번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemSeq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">요청구분</th>
                                            <td class="td-left">
                                                <p class="optionbox">
                                                    <select id="selReq" style="width:112px;" runat="server">
                                                    </select>
                                                    <input type="hidden" id="hdnSelReq" runat="server"/>
                                                    <input type="text" id="txtReq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th id="thReason" style="width: 120px;">교체사유</th>
                                            <td id="tdReason" class="td-left">
                                                <p class="optionbox">
                                                    <select id="selChgReason" style="width:112px;" runat="server">
                                                    </select>
                                                    <input type="hidden" id="hdnChgReason" runat="server" />
                                                    <input type="text" id="txtChgReason" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(신청)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo1" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                    <input type="hidden" id="hdnMemo1" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(인수)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo2" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                    <input type="hidden" id="hdnMemo2" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo3" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine"></asp:TextBox>
                                                    <input type="hidden" id="hdnMemo3" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trReq2">
                                            <th style="width: 120px;">승인구분</th>
                                            <td colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selReq2" style="width:120px;" runat="server">
                                                    </select>
                                                    <input type="hidden" id="hdnSelReq2" runat="server"/>
                                                    <input type="text" id="txtReq2" tabindex="1" style="width:112px;" readonly="true" runat="server" visible="false" />
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="trReq22" style="display:none">
                                            <th style="width: 120px;color: #ff5621;">변경후구분</th>
                                            <td colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selReq22" style="width:120px;color: #ff5621;" runat="server">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="trFile1">
                                            <th style="width: 120px;">인수증파일</th>
                                            <td colspan="3" class="td-left">
                                                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <p id="pFilebox1" class="filebox" runat="server">
                                                        <input type="text" id="txtfu1" style="width: 236px;margin-left:0px" runat="server" />
                                                        <span class="file">
                                                            <input type="file" accept="image/*" id="fu1" name="fu1" onchange="document.getElementById('txtfu1').value=this.value;" />
                                                            <label for="fu1">찾아보기</label>
                                                        </span>
                                                        <asp:Button ID="btnFileAdd1" runat="server" class="btn-black" OnClientClick="return fn_FileAdd('1');" OnClick="btnFileAdd1_Click" Text="첨부" />
                                                    </p>
                                                </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnFileAdd1" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <p id="pFileA1" class="filebox">
                                                    <img id="imgFileView1" style="cursor:pointer" />
                                                    <a id="aFile1" href="#" runat="server"></a>
                                                    <img id="imgFile1" src="/Resource/images/icon_minus_02.png" alt="삭제" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile2">
                                            <th style="width: 120px;">품목파일</th>
                                            <td colspan="3" class="td-left">
                                                <asp:UpdatePanel ID="updPanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <p id="pFilebox2" class="filebox" runat="server">
                                                        <input type="text" id="txtfu2" style="width: 236px;margin-left:0px" runat="server" />
                                                        <span class="file">
                                                            <input type="file" accept="image/*" id="fu2" name="fu2" onchange="document.getElementById('txtfu2').value=this.value;" />
                                                            <label for="fu2">찾아보기</label>
                                                        </span>
                                                        <asp:Button ID="btnFileAdd2" runat="server" class="btn-black" OnClientClick="return fn_FileAdd('2');" OnClick="btnFileAdd2_Click" Text="첨부" />
                                                    </p>
                                                </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnFileAdd2" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <div id="divFileView2">
                                                    
                                                </div>
                                                <p id="pFileA2" class="filebox">
                                                    <img id="imgFileView2" style="cursor:pointer" />
                                                    <a id="aFile2" href="#" runat="server"></a>
                                                    <img id="imgFile2" src="/Resource/images/icon_minus_02.png" alt="삭제" />
                                                </p>
                                            </td>
                                        </tr>
                                         <tr id="trFile3">
                                            <th style="width: 120px;">첨부파일</th>
                                            <td colspan="3" class="td-left">
                                                <asp:UpdatePanel ID="updPanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <p id="pFilebox3" class="filebox" runat="server" style="display:none">
                                                        <input type="text" id="txtfu3" style="width: 236px;margin-left:0px" runat="server" />
                                                        <span class="file">
                                                            <input type="file" accept="image/*" id="fu3" name="fu3" onchange="document.getElementById('txtfu3').value=this.value;" />
                                                            <label for="fu3">찾아보기</label>
                                                        </span>
                                                        <asp:Button ID="btnFileAdd3" runat="server" class="btn-black" OnClientClick="return fn_FileAdd('3');" OnClick="btnFileAdd3_Click" Text="첨부" />
                                                    </p>
                                                </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnFileAdd3" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <p id="pFileA3" class="filebox">
                                                    <img id="imgFileView3" style="cursor:pointer" />
                                                    <a id="aFile3" href="#" runat="server"></a>
                                                    <img id="imgFile3" src="/Resource/images/icon_minus_02.png" alt="삭제" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnMod" type="button" class="btn-save" value="변경" runat="server" />
                                                <input id="btnReg" type="button" class="btn-save" value="등록" runat="server" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                            <asp:Button id="btnFileDownload" OnClick="btnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                            <input type="hidden" id="hdnLoaded" runat="server" />
                            <input type="hidden" id="hdnFileSeq1" runat="server" />
                            <input type="hidden" id="hdnFileSeq2" runat="server" />
                            <input type="hidden" id="hdnFileSeq3" runat="server" />
                            <input type="hidden" id="hdnFileFirstSeq1" runat="server" />
                            <input type="hidden" id="hdnFileFirstSeq2" runat="server" />
                            <input type="hidden" id="hdnFileFirstSeq3" runat="server" />
                            <input type="hidden" id="hdnFileSeq" runat="server" />
                            <input type="hidden" id="hdnOrgLv" runat="server" />
                            <input type="hidden" id="hdnDisuseCd" runat="server" />
                            <input type="hidden" id="hdnDisuseNm" runat="server" />
                        </div>
                        <!-- //E:btncenter -->
                        <div id="divImgViewZoom" class="img-modal">
                            <span class="img-modal-close">&times;</span>
                            <div id="divZoom" class="img-modal-zoom">
                                <span class="img-modal-zoomIn">+</span>
                                <span class="img-modal-zoomOut">-</span>
                            </div>
                            <div id="divModalContent" class="img-modal-contentDiv">
                                <img class="img-modal-content" id="imgModal" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>