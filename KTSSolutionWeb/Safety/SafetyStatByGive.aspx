﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SafetyStatByGive.aspx.cs" Inherits="KTSSolutionWeb.SafetyStatByGive" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbEnDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbEnDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#lblDt").show();
                        $("#<%= txbStDt.ClientID %>").show();
                        $("#<%= txbEnDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "안전용품 지급 현황.xlsx", { sheetName: "안전용품 지급 현황" });
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbStDt.ClientID %>").show();
            $("#<%= txbEnDt.ClientID %>").show();
            $("#lblDt").show();
            $("#selMon").hide();

            obj = $('#selOutGb');
            obj.empty();
            obj.append("<option value='3'>지점별</option>");
            obj.append("<option value='2'>지사별</option>");
            obj.append("<option value='1'>본부별</option>");
        }

        var fn_CellUse1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '1';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }
        var fn_CellUse2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '1';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }
        var fn_CellUse3 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '1';
            var code = '03';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellUse4 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '1';
            var code = '99';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellItemStatus1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '2';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellItemStatus2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '2';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellOperStatus1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '3';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellOperStatus2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '3';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellHoldType1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '4';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellHoldType2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '4';
            var code = '02';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellRepair1 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '5';
            var code = '01';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellRepair2 = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var rptCd = 'SAFETYSTATBYGIVE';
            var gubun = '5';
            var code = 'FF';

            if (value != "0") {
                fn_PopupDetail(id, rptCd, gubun, code);
            }

            return '';
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 200, frozen: true },
                {
                    title: '사용용도', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "업무용", field: "USECD1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellUse1 },
                        { headerHozAlign: "center", hozAlign: "right", title: "공용", field: "USECD2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellUse2 },
                        { headerHozAlign: "center", hozAlign: "right", title: "현장test용", field: "USECD3", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellUse3 },
                        { headerHozAlign: "center", hozAlign: "right", title: "현장기타용", field: "USECD4", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellUse4 },
                    ]
                },
                {
                    title: '제품상태', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "정상", field: "ITEMCD_STATUSCD1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellItemStatus1 },
                        { headerHozAlign: "center", hozAlign: "right", title: "불량", field: "ITEMCD_STATUSCD2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellItemStatus2 },
                    ]
                },
                {
                    title: '운영상태', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "정상", field: "OPER_STATUSCD1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellOperStatus1 },
                        { headerHozAlign: "center", hozAlign: "right", title: "보관중", field: "OPER_STATUSCD2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellOperStatus2 },
                    ]
                },
                {
                    title: '보유형태', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "구매", field: "HOLD_TYPECD1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellHoldType1 },
                        { headerHozAlign: "center", hozAlign: "right", title: "렌탈", field: "HOLD_TYPECD2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellHoldType2 }
                    ]
                },
                {
                    title: '수리상태', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "수리중", field: "REPAIRCD1", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellRepair1 },
                        { headerHozAlign: "center", hozAlign: "right", title: "수리완료", field: "REPAIRCD2", sorter: "number", width: 100, formatter: fn_CellICnt, cellClick: fn_CellRepair2 }
                    ]
                }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
        }


        //조직 찾기 리턴 함수명 정해져 있음
        var SetDispOrgCd = function (orgcd, orgnm) {

            ktsGrid.updateData([{
                id: cellNo, HOLDINGORGCD: orgcd, HOLDINGORGNM: orgnm, GUBUN: "T"
            }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

        }

        var fn_PopupDetail = function (id, rptCd, gubun, code) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var orgCd1 = rowData.ORGCD1;
            var orgCd2 = rowData.ORGCD2;
            var orgCd3 = rowData.ORGCD3;
            var itemCd = rowData.ITEMCD;
            var giveDate = rowData.GIVE_DATE;

            if (orgCd2 == undefined) {
                orgCd2 = '';
            }

            if (orgCd3 == undefined) {
                orgCd3 = '';
            }

            var form = "/Safety/SafetyStatByDetailPopup";
            var target = "SafetyStatByDetailPopup";
            var param = {
                pRPTCD: rptCd,
                pGUBUN: gubun,
                pCODE: code,
                pORGCD1: orgCd1,
                pORGCD2: orgCd2,
                pORGCD3: orgCd3,
                pITEMCD: itemCd,
                pGIVEDATE: giveDate
            };

            var nWidth = 780;
            var nHeight = 780;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var fn_PopupOrgTree = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Safety";
                var target = "OrgTree_Safety";
                var param = {
                    pORGCD: orgcd,
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate1 = '';
                var pDate2 = '';

                if ($("#selDt").val() == 'd') {
                    pDate1 = $("#<%= txbStDt.ClientID %>").val();
                    pDate2 = $("#<%= txbEnDt.ClientID %>").val();
                }
                else {
                    pDate1 = $("#selMon").val();
                }

                var outGb = $('#selOutGb').val();

                if (outGb == '1') {
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                }
                else if (outGb == '2') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                }
                else if (outGb == '3') {
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                }

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = outGb;
                aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[3] = pDate1;
                aData[4] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }




    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="optionbox">
                <label>일자</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="d">일</option>
                    <option value="m">월</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
						<asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox" style="margin-right:20px">
                    <label>출력구분</label>
                    <select id="selOutGb">
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>안전용품 지급 현황 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="GIVE_DATE">지급일자</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                            <option value="ITEMNM">품목</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
