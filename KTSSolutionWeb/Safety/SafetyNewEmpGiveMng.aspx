﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SafetyNewEmpGiveMng.aspx.cs" Inherits="KTSSolutionWeb.SafetyNewEmpGiveMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"] {
            cursor: pointer;
        }

        .tabulator-col-title {
            text-align: center !important;
        }

        .reg-sel {
            display:inline-block;
            margin: 0 0 0 40px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var selUseJson;
        var selStatusJson;
        var selOperJson;
        var selTypeJson;
        var regCnt = 1;
        var arrReg = [];

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbEnDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbEnDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#lblDt").show();
                        $("#<%= txbStDt.ClientID %>").show();
                        $("#<%= txbEnDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    $("input:checkbox[id='selectAllCheckbox']").prop("checked", false);
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "안전용품 신규직원 지급.xlsx", { sheetName: "안전용품 신규직원 지급" });
                });

                $("#btnReg").click(function (e) {
                    fn_Reg();
                });

                $("#selectAllCheckbox").change(function () {
                    var isChecked = this.checked;
                    var checked = 'N';

                    if (isChecked) {
                        checked = 'Y';
                    }

                    $(".cell-check").each(function () {
                        this.checked = isChecked;
                    });


                    var rowCount = ktsGrid.getDataCount();

                    for (var i = 1; i <= rowCount; i++) {
                        ktsGrid.updateData([{
                            id: i, CHECK: checked
                        }]);
                    }

                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbStDt.ClientID %>").show();
            $("#<%= txbEnDt.ClientID %>").show();
            $("#lblDt").show();
            $("#selMon").hide();
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellCheck = function (cell, formatterParams, onRendered) {
            var html = '';
            var cellNo = cell.getRow().getData().id;
            var check = cell.getRow().getData().CHECK;

            html = "<input id='chkEmp' type='checkbox' class='cell-check' onchane='fn_Check(this, \"" + cellNo + "\");' onclick='fn_Check(this, \"" + cellNo + "\");' checked />";

            if (check != 'Y') {
                html = html.replace('checked', '');
            }

            return html;
        }


        var fn_GridInit = function () {

            var Columns = [
                { title: "<input id='selectAllCheckbox' type='checkbox'/>", hozAlign: "center", field: "CHECK", headerSort: false, formatter: fn_CellCheck, headerSort: false, width: 40, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "LVNM1", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "LVNM2", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "LVNM3", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "팀", field: "LVNM4", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "직책", field: "LEVELNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "직급", field: "TITLENM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "JOBNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "입사일자", field: "ENTERDT", sorter: "string" },

            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
            fn_Use();
        }

        var fn_AlertDialog = function (message, gubun) {
            $("#alertDialog").html("<p>" + message + "</p>");

            $("#alertDialog").dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        if (gubun == 'RegTran') {
                            fn_Search();
                        }

                        $(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                    if (gubun == 'RegTran') {
                        fn_Search();
                    }
                }
            });
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Check = function (obj, cellNo) {
            var checked = 'N';

            if (obj.checked) {
                checked = 'Y';
            }

            ktsGrid.updateData([{
                id: cellNo, CHECK: checked
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

        }

        var fn_RegChk = function () {
            var rowCount = ktsGrid.getDataCount();

            for (var i = 1; i <= rowCount; i++) {
                var row = ktsGrid.getRow(i);
                var rowData = row.getData();
                var check = rowData.CHECK;

                if (check == 'Y') {
                    arrReg.push(i);
                }
            }

            if (arrReg.length > 0) return true;

            return false;
        }
        
        var fn_Reg = function () {
            arrReg = [];
            regCnt = 1;

            if (fn_RegChk()) {
                if (confirm("신규직원 일괄지급 하시겠습니까?")) {
                    $("#updateProgress").show();
                    fn_Transaction(arrReg[0]);
                }
            }
            else {
                ktsGridAlert("일괄지급 대상 직원을 체크하세요.");
            }
            
        }


        var fn_Transaction = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var pEmpNo = rowData.EMPNO;

            var pBuyDate = $("#<%= txtBuyDate.ClientID %>").val();
            var pGiveDate = $("#<%= txtGiveDate.ClientID %>").val();
            
            var pUseCd = $('#selUse').val();
            var pUseNm = $("#selUse option:selected").text();
            var pItemCdStatusCd = $('#selStatus').val();
            var pItemNmStatusNm = $("#selStatus option:selected").text();
            var pOperStatusCd = $('#selOper').val();
            var pOperStatusNm = $("#selOper option:selected").text();
            var pHoldTypeCd = $('#selType').val();
            var pHoldTypeNm = $("#selType option:selected").text();

            callGb = 'RegTran';

            //입력 파라미터
            var aData = [];
            aData[0] = pBuyDate;
            aData[1] = pGiveDate;
            aData[2] = pEmpNo;
            aData[3] = pUseCd;
            aData[4] = pUseNm;
            aData[5] = pItemCdStatusCd;
            aData[6] = pItemNmStatusNm;
            aData[7] = pOperStatusCd;
            aData[8] = pOperStatusNm;
            aData[9] = pHoldTypeCd;
            aData[10] = pHoldTypeNm;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;


            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'selUse') {
                selUseJson = json;

                fn_Status();
            }
            else if (callGb == 'selStatus') {
                selStatusJson = json;

                fn_Oper();
            }
            else if (callGb == 'selOper') {
                selOperJson = json;

                fn_Type();
            }
            else if (callGb == 'selType') {
                selTypeJson = json;

                callGb = '';
            }
            else if (callGb == 'RegTran') {
                if (arrReg.length == regCnt) {
                    $("#updateProgress").hide();
                    callGb = '';
                    var msg = '';
                    msg = regCnt + '건이 처리되었습니다.<br>';
                    msg += '신규직원 일괄지급정보는<br>';
                    msg += '물자관리>안전용품관리>안전용품지급<br>';
                    msg += '에서 확인하시기 바랍니다.<br>';

                    fn_AlertDialog(msg, 'RegTran');
                }
                else {
                    var regId = regCnt;
                    regCnt++;
                    fn_Transaction(arrReg[regId]);
                }
            }
        }

        var fn_PopupOrgTree = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Safety";
                var target = "OrgTree_Safety";
                var param = {
                    pORGCD: orgcd,
                    pSHOWORGLV: "3",
                    UPPERYN: "Z"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }


        var fn_SearchChk = function () {
            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Use = function () {
            callGb = 'selUse';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S01';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Status = function () {
            callGb = 'selStatus';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S02';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Oper = function () {
            callGb = 'selOper';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S03';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Type = function () {
            callGb = 'selType';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S04';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_SetSel = function () {

            var obj = $('#selUse');
            obj.empty();

            $.each(JSON.parse(selUseJson), function (idx, item) {
                obj.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
            });

            obj = $('#selStatus');
            obj.empty();

            $.each(JSON.parse(selStatusJson), function (idx, item) {
                obj.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
            });

            obj = $('#selOper');
            obj.empty();

            $.each(JSON.parse(selOperJson), function (idx, item) {
                obj.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
            });

            obj = $('#selType');
            obj.empty();

            $.each(JSON.parse(selTypeJson), function (idx, item) {
                obj.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
            });

        }


        var fn_Search = function () {
            if (fn_SearchChk()) {

                $(".reg-sel").show();
                fn_SetSel();

                var pDate1 = '';
                var pDate2 = '';

                if ($("#selDt").val() == 'd') {
                    pDate1 = $("#<%= txbStDt.ClientID %>").val();
                    pDate2 = $("#<%= txbEnDt.ClientID %>").val();
                }
                else {
                    pDate1 = $("#selMon").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[2] = pDate1;
                aData[3] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }




    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
    <div class="pagenavi">
        <ul>
            <li><a id="aMenuTree1" runat="server"></a></li>
            <li class="last"><a id="aMenuTree2" runat="server"></a></li>
            <!-- 현재 페이지에 last클래스 추가 -->
        </ul>
    </div>
    <!--end subTitle-->

    <!-- S: contentsarea -->
    <div class="contentsarea">
        <div id="alertDialog" class="alert" title="Alert"></div>
        <!-- S:searchbox -->
        <div class="searchbox" style="margin-top: 0;">
            <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="optionbox">
                    <label>일자</label>
                    <select id="selDt" style="width: 70px">
                        <option selected="selected" value="d">일</option>
                        <option value="m">월</option>
                    </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" Style="width: 40% !important;"></asp:TextBox>
                    <label id="lblDt" style="width: 10px">~</label>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" Style="width: 40% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width: 110px"></select>
                    </span>
                </span>
                <span class="inpbox first" style="margin-right: 20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" Style="margin-right: 10px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" Style="margin-right: 10px; width: 40px; height: 40px;" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged" />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
        <!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="reg-sel" style="display:none;">
                    <span class="inpbox">
                        <label>구매일자</label>
                        <input type="text" id="txtBuyDate" class="date" style="width: 110px;cursor:pointer" readonly="readonly" runat="server" />
                    </span>
                    <span class="inpbox">
                        <label>지급일자</label>
                        <input type="text" id="txtGiveDate" class="date" style="width: 110px;cursor:pointer" readonly="readonly" runat="server" />
                    </span>
                    <span class="optionbox">
                        <label>사용용도</label>
                        <select id="selUse" style="width: 110px;cursor:pointer"></select>
                    </span>
                    <span class="optionbox">
                        <label>제품상태</label>
                        <select id="selStatus" style="width: 70px;cursor:pointer"></select>
                    </span>
                    <span class="optionbox">
                        <label>운용상태</label>
                        <select id="selOper" style="width: 82px;cursor:pointer"></select>
                    </span>
                    <span class="optionbox">
                        <label>보유형태</label>
                        <select id="selType" style="width: 70px;cursor:pointer"></select>
                    </span>
                    <input id="btnReg" type="button" class="btn-green last" style="float: right;margin-left: 10px;" value="등록" />
                </div>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" style="float: right;margin-left: 10px;" value="엑셀" />
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display: none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM">소속</option>
                            <option value="EMPNM">이름</option>
                            <option value="EMPNO">사번</option>
                        </select>
                        : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>
