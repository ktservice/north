﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyRequestUserHistPopup.aspx.cs" Inherits="KTSSolutionWeb.SafetyRequestUserHistPopup" EnableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>안전용품이력</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        /* 모달 창 스타일 */
        .img-modal {
          display: none; /* 초기에는 보이지 않음 */
          position: fixed;
          z-index: 1;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          overflow: auto;
          background-color: rgb(0,0,0);
          background-color: rgba(0,0,0,0.9);
        }

        /* 모달 내부 이미지 스타일 */
        .img-modal-content {
          width: 100%;
          transition: transform 0.3s ease;
        }

        /* 모달 내용 스타일 */
        .img-modal-contentDiv {
          margin: auto;
          width: 50%;
          padding: 20px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoom {
          position: absolute;
          top: 15px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoomIn, .img-modal-zoomOut {
          top: 15px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          padding-left: 10px;
          cursor:pointer;
        }

        /* 닫기 버튼 스타일 */
        .img-modal-close {
          position: absolute;
          top: 15px;
          right: 35px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          cursor:pointer;
        }

        .img-modal-close:hover,
        .img-modal-close:focus {
          color: #bbb;
          text-decoration: none;
          cursor: pointer;
        }

        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type="text"]:read-only, textarea:read-only {
          background : #e3e3e3;
          border: solid 1px #9e9e9e;
        }

        .scrollbox {
            overflow-x: hidden;
        }

        .filebox {
            margin-right: 0;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var init;
        var callGb;
        var currentScale = 1; // 이미지 확대/축소 버튼 클릭 이벤트 핸들러
        var imageCall = 0;

        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                // 이미지를 클릭했을 때 모달을 보여주는 이벤트 처리
                $("#imgFileView1").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });

                $("#imgFileView2").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });

                $("#imgFileView3").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });

                $(".img-modal-zoomIn").on("click", function () {
                    currentScale += 0.1;
                    $("#imgModal").css("transform", "scale(" + currentScale + ")");
                });

                $(".img-modal-zoomOut").on("click", function () {
                    if (currentScale > 0.1) {
                        currentScale -= 0.1;
                        $(".img-modal-content").css("transform", "scale(" + currentScale + ")");
                    }
                });

                // 모달 닫기 버튼을 클릭했을 때 모달을 닫는 이벤트 처리
                $(".img-modal-close").on("click", function () {
                    $("#divImgViewZoom").css("display", "none");
                    currentScale = 1;
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });


                $("#aFile1").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq1.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile2").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq2.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#aFile3").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq3.ClientID %>").val());
                    fn_FileDownload();
                });
                
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            fn_Data();
        }

        var fn_GetImage = function (fileNmSeq) {
            callGb = 'getImage';

            //입력 파라미터
            var aData = [];
            aData[0] = fileNmSeq;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetImage');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Req = function () {
            callGb = 'selReq';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S10';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Data = function () {
            callGb = 'Data';

            var gubun = $("#<%= hdnGubun.ClientID %>").val();
            var seq = $("#<%= hdnId.ClientID %>").val();
            var giveDate = $("#<%= hdnGiveDate.ClientID %>").val();
            var empNo = $("#<%= hdnEmpNo.ClientID %>").val();
            var itemCd = $("#<%= hdnItemCd.ClientID %>").val();
            var itemSeq = $("#<%= hdnItemSeq.ClientID %>").val();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = seq;
            aData[2] = giveDate;
            aData[3] = empNo;
            aData[4] = itemCd;
            aData[5] = itemSeq;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_FileDownload = function () {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }


        var ajaxCallBack = function (json) {

            if (callGb == 'Data') {

                $.each(JSON.parse(json), function (idx, item) {
                    $("#txtGiveDate").val(item.GIVE_DATE);
                    $("#txtOrgNm").val(item.ORGNM);
                    $("#txtEmpNm").val(item.EMPNM);
                    $("#txtEmpJobNm").val(item.JOBNM);
                    $("#txtItemNm").val(item.ITEMNM);
                    $("#txtItemSeq").val(item.ITEMSEQ);
                    $("#txtReq").val(item.REQ_GBNM);
                    $("#txtChgReason").val(item.CHG_REASONNM);
                    $("#txtChgDt").val(item.CHG_DATE);
                    $("#txtChgEmp").val(item.CHG_EMPNM);
                    $("#txtReqDt").val(item.REG_DATE);
                    $("#txtReqEmp").val(item.REG_EMPNM);
                    $("#txtAccDt").val(item.ACCEPT_DATE);
                    $("#txtAccEmp").val(item.ACCEPT_EMPNM);
                    $("#txtAppDt").val(item.APPROVAL_DATE);
                    $("#txtAppEmp").val(item.APPROVAL_EMPNM);
                    $("#txtDisuseDt").val(item.DISUSE_DATE);
                    $("#txtDisuseEmp").val(item.DISUSE_EMPNM);
                    $("#txtReq2").val(item.REQ_STATUSNM);
                    $("#hdnFileSeq1").val(item.FILE_SEQ1);
                    $("#hdnFileSeq2").val(item.FILE_SEQ2);
                    $("#hdnFileSeq3").val(item.FILE_SEQ3);
                    $("#aFile1").text(item.FILENM1);
                    $("#aFile2").text(item.FILENM2);
                    $("#aFile3").text(item.FILENM3);
                    $("#txtMemo11").val(item.MEMO11);
                    $("#txtMemo1").val(item.MEMO1);
                    $("#txtMemo2").val(item.MEMO2);
                    $("#txtMemo3").val(item.MEMO3);
                });

                var vFileSeq1 = $("#hdnFileSeq1").val();
                var vFileSeq2 = $("#hdnFileSeq2").val();
                var vFileSeq3 = $("#hdnFileSeq3").val();

                if ($('#aFile1').text() != '') {
                    imageCall = 1;
                    fn_GetImage(vFileSeq1);
                }
                else if ($('#aFile2').text() != '') {
                    imageCall = 2;
                    fn_GetImage(vFileSeq2);
                }
                else if ($('#aFile3').text() != '') {
                    imageCall = 3;
                    fn_GetImage(vFileSeq3);
                }
                else {
                    callGb = '';
                }
            }
            else if (callGb == 'getImage') {
                var vFileSeq1 = $("#hdnFileSeq1").val();
                var vFileSeq2 = $("#hdnFileSeq2").val();
                var vFileSeq3 = $("#hdnFileSeq3").val();

                $.each(JSON.parse(json), function (idx, item) {
                    if (imageCall == 1) {
                        $('#imgFileView1').attr('src', item.SRC);
                        $("#imgFileView1").css("width", "20px");
                        $("#imgFileView1").css("height", "20px");
                    }
                    else if (imageCall == 2) {
                        $('#imgFileView2').attr('src', item.SRC);
                        $("#imgFileView2").css("width", "20px");
                        $("#imgFileView2").css("height", "20px")
                    }
                    else if (imageCall == 3) {
                        $('#imgFileView3').attr('src', item.SRC);
                        $("#imgFileView3").css("width", "20px");
                        $("#imgFileView3").css("height", "20px")
                    }
                });

                if (imageCall == 1) {
                    if ($('#aFile2').text() != '') {
                        imageCall = 2;
                        fn_GetImage(vFileSeq2);
                    }
                    else if ($('#aFile3').text() != '') {
                        imageCall = 3;
                        fn_GetImage(vFileSeq3);
                    }
                }
                else if (imageCall == 2) {
                    if ($('#aFile3').text() != '') {
                        imageCall = 3;
                        fn_GetImage(vFileSeq3);
                    }
                    else {
                        imageCall = 0;
                    }
                }
                else {
                    imageCall = 0;
                    callGb = '';
                }

            }
        }

        function SendParentsForm() {
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>안전용품이력</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">소속</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnGubun" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                    <input type="hidden" id="hdnGiveDate" runat="server" />
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                    <input type="hidden" id="hdnItemSeq" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">직무</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpJobNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">품목순번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemSeq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">요청구분</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtReq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">교체사유</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgReason" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">교체신청일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">교체신청자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">신청일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtReqDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">신청자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtReqEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">인수일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAccDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">인수자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAccEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">불용폐기일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtDisuseDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">불용폐기자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtDisuseEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">승인일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAppDt" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">승인자명</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtAppEmp" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(교체신청)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo11" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(신청)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo1" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(인수)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo2" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(승인)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo3" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine" readonly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">승인구분</th>
                                            <td colspan="3" class="td-left">
                                                <span class="inpbox">
                                                    <input type="text" id="txtReq2" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </span>
                                            </td>
                                        </tr>
                                        <tr id="trFile1">
                                            <th style="width: 120px;">인수증파일</th>
                                            <td colspan="3" class="td-left">
                                                <p id="pFileA1" class="filebox">
                                                    <img id="imgFileView1" style="cursor:pointer" />
                                                    <a id="aFile1" href="#" runat="server"></a>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile2">
                                            <th style="width: 120px;">품목파일</th>
                                            <td colspan="3" class="td-left">
                                                <p id="pFileA2" class="filebox">
                                                    <img id="imgFileView2" style="cursor:pointer" />
                                                    <a id="aFile2" href="#" runat="server"></a>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile3">
                                            <th style="width: 120px;">첨부파일</th>
                                            <td colspan="3" class="td-left">
                                                <p id="pFileA3" class="filebox">
                                                    <img id="imgFileView3" style="cursor:pointer" />
                                                    <a id="aFile3" href="#" runat="server"></a>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                            <asp:Button id="btnFileDownload" OnClick="btnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                            <input type="hidden" id="hdnFileSeq1" runat="server" />
                            <input type="hidden" id="hdnFileSeq2" runat="server" />
                            <input type="hidden" id="hdnFileSeq3" runat="server" />
                            <input type="hidden" id="hdnFileSeq" runat="server" />
                        </div>
                        <!-- //E:btncenter -->
                        <div id="divImgViewZoom" class="img-modal">
                            <span class="img-modal-close">&times;</span>
                            <div id="divZoom" class="img-modal-zoom">
                                <span class="img-modal-zoomIn">+</span>
                                <span class="img-modal-zoomOut">-</span>
                            </div>
                            <div id="divModalContent" class="img-modal-contentDiv">
                                <img class="img-modal-content" id="imgModal" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>