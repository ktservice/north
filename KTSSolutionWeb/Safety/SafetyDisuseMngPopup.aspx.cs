﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class SafetyDisuseMngPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {

                    string pRequestStatus = Request.Form["pRequestStatus"] == null ? "" : Request.Form["pRequestStatus"].ToString();
                    txtReq2.Value = pRequestStatus;

                    string pOrgNm = Request.Form["pOrgNm"] == null ? "" : Request.Form["pOrgNm"].ToString();
                    txtOrgNm.Value = pOrgNm;

                    string pEmpNm = Request.Form["pEmpNm"] == null ? "" : Request.Form["pEmpNm"].ToString();
                    txtEmpNm.Value = pEmpNm;

                    string pEmpNo = Request.Form["pEmpNo"] == null ? "" : Request.Form["pEmpNo"].ToString();
                    hdnEmpNo.Value = pEmpNo;

                    string pEmpJobNm = Request.Form["pEmpJobNm"] == null ? "" : Request.Form["pEmpJobNm"].ToString();
                    txtEmpJobNm.Value = pEmpJobNm;

                    string pGiveDate = Request.Form["pGiveDate"] == null ? "" : Request.Form["pGiveDate"].ToString();
                    txtGiveDate.Value = pGiveDate;

                    string pAppDate = Request.Form["pAppDate"] == null ? "" : Request.Form["pAppDate"].ToString();
                    txtAppDate.Value = pAppDate;

                    string pAcceptDate = Request.Form["pAcceptDate"] == null ? "" : Request.Form["pAcceptDate"].ToString();
                    txtAcceptDate.Value = pAcceptDate;

                    string pDisuseDate = Request.Form["pDisuseDate"] == null ? "" : Request.Form["pDisuseDate"].ToString();
                    txtDisuseDate.Value = pDisuseDate;

                    string pItemNm = Request.Form["pItemNm"] == null ? "" : Request.Form["pItemNm"].ToString();
                    txtItemNm.Value = pItemNm;

                    string pItemCd = Request.Form["pItemCd"] == null ? "" : Request.Form["pItemCd"].ToString();
                    hdnItemCd.Value = pItemCd;

                    string pItemSeq = Request.Form["pItemSeq"] == null ? "" : Request.Form["pItemSeq"].ToString();
                    txtItemSeq.Value = pItemSeq;

                    string pChgReasonNm = Request.Form["pChgReasonNm"] == null ? "" : Request.Form["pChgReasonNm"].ToString();
                    txtChgReason.Value = pChgReasonNm;

                    string pReqGbNm = Request.Form["pReqGbNm"] == null ? "" : Request.Form["pReqGbNm"].ToString();
                    txtReq.Value = pReqGbNm;

                    string pMemo3 = Request.Form["pMemo3"] == null ? "" : Request.Form["pMemo3"].ToString();
                    txtMemo3.Text = pMemo3;

                    string pMemo1 = Request.Form["pMemo1"] == null ? "" : Request.Form["pMemo1"].ToString();
                    txtMemo1.Text = pMemo1;

                    string pMemo11 = Request.Form["pMemo11"] == null ? "" : Request.Form["pMemo11"].ToString();
                    txtMemo11.Text = pMemo11;

                    string pFileNm2 = Request.Form["pFileNm2"] == null ? "" : Request.Form["pFileNm2"].ToString();
                    aFile2.InnerText = pFileNm2.Replace(@"C:\fakepath\", "");

                    string pFileNm3 = Request.Form["pFileNm3"] == null ? "" : Request.Form["pFileNm3"].ToString();
                    aFile3.InnerText = pFileNm3.Replace(@"C:\fakepath\", "");

                    string pFileSeq2 = Request.Form["pFileSeq2"] == null ? "" : Request.Form["pFileSeq2"].ToString();
                    hdnFileSeq2.Value = pFileSeq2;

                    string pFileSeq3 = Request.Form["pFileSeq3"] == null ? "" : Request.Form["pFileSeq3"].ToString();
                    hdnFileSeq3.Value = pFileSeq3;

                }
            }
        }


        protected void btnFileDownload_ServerClick(object sender, EventArgs e)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYIMAGEFILE";
            string jobGb = "GET";
            string errCd = string.Empty;
            string errMsg = string.Empty;
            DataSet ds = new DataSet();

            List<string> aData = new List<string>();

            aData.Add(hdnFileSeq.Value);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                (ds, errCd, errMsg) = dbTransaction.GetTableRow();
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //SELECT FILENM, FILEDATA, FILETYPE, FILESIZE 
                    int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                    byte[] bFile = new byte[nFileLen];
                    bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                    string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                    using (FileUtil fileUtil = new FileUtil())
                    {
                        fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                    }
                }

            }

        }

    }
}