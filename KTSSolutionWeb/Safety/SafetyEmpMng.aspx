﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SafetyEmpMng.aspx.cs" Inherits="KTSSolutionWeb.SafetyEmpMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbEnDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbEnDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd' || this.value == 'ed') {
                        $("#lblDt").show();
                        $("#<%= txbStDt.ClientID %>").show();
                        $("#<%= txbEnDt.ClientID %>").show();
                        $("#selMon").hide();
                        $("#selMon2").hide();
                    }
                    else if (this.value == 'm') {
                        fn_Month('selMon');
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").show();
                        $("#selMon2").hide();
                    }
                    else if (this.value == 'em') {
                        fn_Month('selMon2');
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").hide();
                        $("#selMon2").show();
                    }
                    else if (this.value == 'le') {
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").hide();
                        $("#selMon2").hide();
                    }

                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "안전용품 지급.xlsx", { sheetName: "안전용품 지급" });
                });

                $("#btnGiveReg").click(function (e) {
                    fn_PopupGiveReg();
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnNewUpload").click(function (e) {
                    fn_PopupNewUpload();
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbStDt.ClientID %>").show();
            $("#<%= txbEnDt.ClientID %>").show();
            $("#lblDt").show();
            $("#selMon").hide();
            $("#selMon2").hide();
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var value = cell.getValue();

            if (value == 'Y') {
                cell.getElement().style.color = "#077078";
                cell.getElement().style.fontWeight = "bold";

                html = '지점반환완료';
            }
            else if (value == 'N') {
                var cellExfireYn = cell.getRow().getData().EXFIREYN;
                var cellCheck1 = cell.getRow().getData().CHECK1;
                var cellCheck2 = cell.getRow().getData().CHECK2;

                
                if (cellExfireYn == 'Y' || cellCheck1 == 'Y' || cellCheck2 == 'Y') {
                    cellNo = cell.getRow().getData().id;
                    html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='지점반환' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
                }
            }

            return html;
        }

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "입사일자", field: "ENTERDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "퇴사일자", field: "EXFIREDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "퇴사구분", field: "EXFIREYN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "반환체크1", field: "CHECK1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "반환체크2", field: "CHECK2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점회수", field: "EXFIREREG_YN", sorter: "string", width: 100, formatter: fn_CellReg }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_Month = function (gubun) {
            callGb = gubun;
            var gb = 'DDL1';

            if (gubun == 'selMon2') {
                gb = 'DDL2';
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gb;
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Reg = function (id) {
            if (confirm("소속지점으로 반환 하시겠습니까?")) {
                callGb = 'RegTran';
                cellNo = id;

                fn_Transaction('I', id);
            }
        }


        var fn_Transaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var empno = rowData.EMPNO;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = empno;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'selMon2') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'RegTran') {
                var row = ktsGrid.getRow(cellNo);
                row.select();

                ktsGrid.updateData([{
                    id: cellNo, EXFIREREG_YN: 'Y'
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

                ktsGridAlert("소속 지점으로 반환을 완료했습니다.");

                callGb = '';
            }
        }

        var fn_PopupGiveReg = function () {
            var form = "/Safety/SafetyGivePopup";
            var target = "SafetyGivePopup";
            var param = {
            };
            var nWidth = 720;
            var nHeight = 520;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupUpload = function () {
            var form = "/Safety/SafetyGiveUploadPopup";
            var target = "SafetyGiveUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupNewUpload = function () {
            var form = "/Safety/SafetyGiveNewUploadPopup";
            var target = "SafetyGiveNewUploadPopup";
            var param = {
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Safety";
                var target = "OrgTree_Safety";
                var param = {
                    pORGCD: orgcd,
                    pSHOWORGLV: "3",
                    UPPERYN: "Z"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate1 = '';
                var pDate2 = '';

                if ($("#selDt").val() == 'd' || $("#selDt").val() == 'ed') {
                    pDate1 = $("#<%= txbStDt.ClientID %>").val();
                    pDate2 = $("#<%= txbEnDt.ClientID %>").val();
                }
                else if ($("#selDt").val() == 'm') {
                    pDate1 = $("#selMon").val();
                }
                else if ($("#selDt").val() == 'em') {
                    pDate1 = $("#selMon2").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = $("#selDt").val();
                aData[1] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[2] = pDate1;
                aData[3] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }




    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="optionbox">
                <label>구분</label>
                <select id="selDt" style="width:110px">
                    <option selected="selected" value="d">지급일</option>
                    <option value="m">지급월</option>
                    <option value="em">퇴사월</option>
                    <option value="ed">퇴사일</option>
                    <option value="le">휴직&병가</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                        <select id="selMon2" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnGiveReg" type="button" class="btn-green last" style="float: right;" value="등록" />
                    <input id="btnUpload" type="button" class="btn-green last" style="float: right;" value="지급일괄등록" />
                    <input id="btnNewUpload" type="button" class="btn-green last" style="float: right;" value="신규직원지급일괄등록" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>안전용품 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="GIVE_DATE">지급일자</option>
                            <option value="ORGNM">소속</option>
                            <option value="EMPNM">이름</option>
                            <option value="EMPNO">사번</option>
                            <option value="EXFIREYN">퇴사구분</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
