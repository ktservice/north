﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyGivePopup.aspx.cs" Inherits="KTSSolutionWeb.SafetyGivePopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>안전용품지급등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });


                $("#btnEmpUser").click(function (e) {
                    fn_PopupEmpUser();
                });

                $("#txtEmpNm").click(function (e) {
                    fn_PopupEmpUser();
                });

                $("#btnItem").click(function (e) {
                    fn_PopupItem();
                });

                $("#txtItemNm").click(function (e) {
                    fn_PopupItem();
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            fn_Use();
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("등록하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_RegChk = function () {
            var empno = $("#hdnEmpNo").val();
            var itemCd = $("#hdnItemCd").val();

            if (empno == undefined || empno == '') {
                ktsGridAlert('이름을 입력해야 합니다.');
                return false;
            }

            if (itemCd == undefined || itemCd == '') {
                ktsGridAlert('품목을 입력해야 합니다.');
                return false;
            }


            return true;
        }

        var fn_Use = function () {
            callGb = 'selUse';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S01';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Status = function () {
            callGb = 'selStatus';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S02';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Oper = function () {
            callGb = 'selOper';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S03';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Type = function () {
            callGb = 'selType';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S04';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function fn_PopupItem() {

            var form = "/Oper/SafetyCodePopup";
            var target = "SafetyCodePopup";
            var param = {
                pGUBUN: "1"
            };
            var nWidth = 700;
            var nHeight = 700;

            win = cmPostPopup(form, target, param, nWidth, nHeight);

            win.focus();
        }

        function fn_PopupEmpUser() {

            var form = "/Common/SearchUser";
            var target = "SearchUser";
            var param = {
            };
            var nWidth = 700;
            var nHeight = 500;

            win = cmPostPopup(form, target, param, nWidth, nHeight);

            win.focus();
        }

        function SetDispItemCd(itemCd, itemNm) {
            $("#txtItemNm").val(itemNm);
            $("#hdnItemCd").val(itemCd);
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#txtEmpNm").val(empnm);
            $("#hdnEmpNo").val(empno);
         }

        var fn_Transaction = function (gubun, productCd) {
            var buyDate = $("#txtBuyDate").val();
            var giveDate = $("#txtGiveDate").val();
            var empno = $("#hdnEmpNo").val();
            var itemCd = $("#hdnItemCd").val();
            var itemNm = $("#txtItemNm").val();
            var use = $("#selUse").val();
            var useNm = $("#selUse option:selected").text();
            var status = $("#selStatus").val();
            var statusNm = $("#selStatus option:selected").text();
            var oper = $("#selOper").val();
            var operNm = $("#selOper option:selected").text();
            var type = $("#selType").val();
            var typeNm = $("#selType option:selected").text();

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = buyDate;
            aData[2] = giveDate;
            aData[3] = empno;
            aData[4] = itemCd;
            aData[5] = itemNm;
            aData[6] = use;
            aData[7] = useNm;
            aData[8] = status;
            aData[9] = statusNm;
            aData[10] = oper;
            aData[11] = operNm;
            aData[12] = type;
            aData[13] = typeNm;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;
            var obj3;
            var obj4;

            if (callGb == 'selUse') {
                obj1 = $('#selUse');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                fn_Status();
            }
            else if (callGb == 'selStatus') {
                obj2 = $('#selStatus');
                obj2.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj2.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                fn_Oper();
            }
            else if (callGb == 'selOper') {
                obj3 = $('#selOper');
                obj3.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj3.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                fn_Type();
            }
            else if (callGb == 'selType') {
                obj4 = $('#selType');
                obj4.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj4.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                callGb = '';
            }

            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');

                        setTimeout(function () {
                            SendParentsForm();
                        }, 1000);
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>안전용품</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                    <button type="button" id="btnEmpUser" class="btn-plus" title="검색">+</button>
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">구매일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtBuyDate" runat="server" class="date" Width="110px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtGiveDate" runat="server" class="date" Width="110px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <button type="button" id="btnItem" class="btn-plus" title="검색">+</button>
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">사용용도</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUse" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">제품상태</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selStatus" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">운용상태</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selOper" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                            <th style="width: 120px;">보유상태</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selType" style="width:112px;">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
                <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>