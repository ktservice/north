﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyLoseRequestPopup.aspx.cs" Inherits="KTSSolutionWeb.SafetyLoseRequestPopup" enableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>안전용품등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        /* 모달 창 스타일 */
        .img-modal {
          display: none; /* 초기에는 보이지 않음 */
          position: fixed;
          z-index: 1;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          overflow: auto;
          background-color: rgb(0,0,0);
          background-color: rgba(0,0,0,0.9);
        }

        /* 모달 내부 이미지 스타일 */
        .img-modal-content {
          width: 100%;
          transition: transform 0.3s ease;
        }

        /* 모달 내용 스타일 */
        .img-modal-contentDiv {
          margin: auto;
          width: 50%;
          padding: 20px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoom {
          position: absolute;
          top: 15px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoomIn, .img-modal-zoomOut {
          top: 15px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          padding-left: 10px;
          cursor:pointer;
        }

        /* 닫기 버튼 스타일 */
        .img-modal-close {
          position: absolute;
          top: 15px;
          right: 35px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          cursor:pointer;
        }

        .img-modal-close:hover,
        .img-modal-close:focus {
          color: #bbb;
          text-decoration: none;
          cursor: pointer;
        }

        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type="text"]:read-only, textarea:read-only {
          background : #e3e3e3;
          border: solid 1px #9e9e9e;
        }

        .scrollbox {
            overflow-x: hidden;
        }

        .filebox {
            margin-right: 0;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var giveDate;
        var itemCd;
        var itemSeq;
        var reqGb;
        var reqGbNm;
        var chgReason;
        var chgReasonNm;
        var memo;
        var empno;
        var currentScale = 1; // 이미지 확대/축소 버튼 클릭 이벤트 핸들러
        var imageCall = 0;

        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $("#imgFileView2").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });
                
                $(".img-modal-zoomIn").on("click", function () {
                    currentScale += 0.1;
                    $("#imgModal").css("transform", "scale(" + currentScale + ")");
                });

                $(".img-modal-zoomOut").on("click", function () {
                    if (currentScale > 0.1) {
                        currentScale -= 0.1;
                        $(".img-modal-content").css("transform", "scale(" + currentScale + ")");
                    }
                });

                // 모달 닫기 버튼을 클릭했을 때 모달을 닫는 이벤트 처리
                $(".img-modal-close").on("click", function () {
                    $("#divImgViewZoom").css("display", "none");
                    currentScale = 1;
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#aFile2").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq2.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#imgFile2").click(function (e) {
                    $("#<%= hdnFileSeq2.ClientID %>").val('');
                    $('#aFile2').text('');
                    $("#pFilebox2").show();
                    $("#pFileA2").hide();
                    $('#txtfu2').val('');
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            fn_ChgReason();
            $("#imgFile2").hide();
        }

        var fn_GetImage = function (fileNmSeq) {
            callGb = 'getImage';

            //입력 파라미터
            var aData = [];
            aData[0] = fileNmSeq;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl('/Safety/SafetyRequestUserHistPopup', 'GetImage');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_FileAdd = function (seq) {
            $("#hdnChgReason").val($("#selChgReason").val());

            if (seq == 2) {
                if ($('#txtfu2').val() == '') {
                    ktsGridAlert('첨부파일이 없습니다.');
                    return false;
                }
            }

            return true;
        }

        var fn_FileDownload = function () {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }

        var fn_RegTran = function () {
            if (fn_RegChk()) {
                if (confirm("신청을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_RegChk = function () {
            var selReq = '01';
            var selChgReason = $("#selChgReason").val();
            var txtMemo = $("#txtMemo").val();


            if (txtMemo.length == 0) {
                ktsGridAlert("교체구분에 대한 사유를 비고에 입력해야 합니다.");
                return false;
            }
            else if ($('#aFile2').text() == '') {

                ktsGridAlert('품목파일을 첨부해야 합니다.');
                return false;
            }

            return true;
        }

        var fn_ChgReason = function () {
            callGb = 'selChgReason';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S08';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun, productCd) {
            giveDate = $("#txtGiveDate").val();
            itemCd = $("#hdnItemCd").val();
            itemSeq = $("#txtItemSeq").val();
            reqGb = '01';
            reqGbNm = '교체';
            chgReason = $("#selChgReason").val();
            chgReasonNm = $("#selChgReason option:selected").text();
            fileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
            memo = $("#txtMemo").val();
            empno = $("#hdnEmpNo").val();

            if (reqGb != '01') {
                chgReason = '';
                chgReasonNm = '';
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = giveDate;
            aData[2] = itemCd;
            aData[3] = itemSeq;
            aData[4] = reqGb;
            aData[5] = reqGbNm;
            aData[6] = chgReason;
            aData[7] = chgReasonNm;
            aData[8] = fileSeq2;
            aData[9] = memo;
            aData[10] = empno;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;

            if (callGb == 'selChgReason') {
                obj2 = $('#selChgReason');
                obj2.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj2.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                var vFileSeq2 = $("#<%= hdnFileSeq2.ClientID %>").val();
                if (vFileSeq2 != '') {
                    var val = $('#txtfu2').val();

                    if (val != undefined) {
                        if (val.indexOf('C:\\fakepath\\') > 0) {
                            val = val.replace('C:\\fakepath\\', '')
                        }

                        $("#pFilebox2").hide();
                        $("#imgFile2").show();
                    }
                }

                var selData = '';

                if ($("#hdnChgReason").val() != undefined) {
                    selData = $("#hdnChgReason").val();
                }

                if (selData != '') {
                    obj2.val(selData).prop("selected", true);
                }

                if ($('#aFile2').text() != '') {
                    imageCall = 2;
                    fn_GetImage(vFileSeq2);
                }
                else {
                    callGb = '';
                }
                
            }
            else if (callGb == 'getImage') {
                var vFileSeq2 = $("#hdnFileSeq2").val();

                $.each(JSON.parse(json), function (idx, item) {
                    if (imageCall == 2) {
                        $('#imgFileView2').attr('src', item.SRC);
                        $("#imgFileView2").css("width", "20px");
                        $("#imgFileView2").css("height", "20px")
                    }
                });

                imageCall = 0;
                callGb = '';
            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');

                        setTimeout(function () {
                            SendParentsForm();
                        }, 1000);
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {

            var cellNo = $("#hdnId").val();
                        
            opener.SetDispItem(check, cellNo, reqGbNm, chgReasonNm, memo);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>안전용품</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">소속</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">직무</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpJobNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">품목순번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemSeq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thReason" style="width: 120px;">교체구분</th>
                                            <td colspan="3" id="tdReason" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selChgReason" style="width:112px;">
                                                    </select>
                                                    <input type="hidden" id="hdnChgReason" runat="server" />
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile2">
                                            <th style="width: 120px;">품목파일</th>
                                            <td colspan="3" class="td-left">
                                                <asp:UpdatePanel ID="updPanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <p id="pFilebox2" class="filebox" runat="server">
                                                        <input type="text" id="txtfu2" style="width: 236px;margin-left:0px" runat="server" />
                                                        <span class="file">
                                                            <input type="file" accept="image/*" id="fu2" name="fu2" onchange="document.getElementById('txtfu2').value=this.value;" />
                                                            <label for="fu2">찾아보기</label>
                                                        </span>
                                                        <asp:Button ID="btnFileAdd2" runat="server" class="btn-black" OnClientClick="return fn_FileAdd('2');" OnClick="btnFileAdd2_Click" Text="첨부" />
                                                    </p>
                                                </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnFileAdd2" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <p id="pFileA2" class="filebox">
                                                    <img id="imgFileView2" style="cursor:pointer" />
                                                    <a id="aFile2" href="#" runat="server"></a>
                                                    <img id="imgFile2" src="/Resource/images/icon_minus_02.png" alt="삭제" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="신청" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                            <asp:Button id="btnFileDownload" OnClick="btnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                            <input type="hidden" id="hdnFileSeq2" runat="server" />
                            <input type="hidden" id="hdnFileSeq" runat="server" />
                        </div>
                        <!-- //E:btncenter -->
                        <div id="divImgViewZoom" class="img-modal">
                            <span class="img-modal-close">&times;</span>
                            <div id="divZoom" class="img-modal-zoom">
                                <span class="img-modal-zoomIn">+</span>
                                <span class="img-modal-zoomOut">-</span>
                            </div>
                            <div id="divModalContent" class="img-modal-contentDiv">
                                <img class="img-modal-content" id="imgModal" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>