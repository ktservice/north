﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class SafetyStatByDetailPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    hdnRptCd.Value = Request.Form["pRPTCD"] == null ? "" : Request.Form["pRPTCD"].ToString();
                    hdnGubun.Value = Request.Form["pGUBUN"] == null ? "" : Request.Form["pGUBUN"].ToString();
                    hdnOrgCd1.Value = Request.Form["pORGCD1"] == null ? "" : Request.Form["pORGCD1"].ToString();
                    hdnOrgCd2.Value = Request.Form["pORGCD2"] == null ? "" : Request.Form["pORGCD2"].ToString();
                    hdnOrgCd3.Value = Request.Form["pORGCD3"] == null ? "" : Request.Form["pORGCD3"].ToString();
                    hdnItemCd.Value = Request.Form["pITEMCD"] == null ? "" : Request.Form["pITEMCD"].ToString();
                    hdnCode.Value = Request.Form["pCODE"] == null ? "" : Request.Form["pCODE"].ToString();
                    hdnGiveDate.Value = Request.Form["pGIVEDATE"] == null ? "" : Request.Form["pGIVEDATE"].ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetGridData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYSTATBYDETAIL";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}