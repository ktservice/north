﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class SafetyRequestUserHistPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    string pGubun = Request.Form["pGubun"] == null ? "" : Request.Form["pGubun"].ToString();
                    hdnGubun.Value = pGubun;

                    string pIId = Request.Form["pIId"] == null ? "" : Request.Form["pIId"].ToString();
                    hdnId.Value = pIId;

                    string pGiveDate = Request.Form["pGiveDate"] == null ? "" : Request.Form["pGiveDate"].ToString();
                    hdnGiveDate.Value = pGiveDate;

                    string pEmpNo = Request.Form["pEmpNo"] == null ? "" : Request.Form["pEmpNo"].ToString();
                    hdnEmpNo.Value = pEmpNo;

                    string pItemCd = Request.Form["pItemCd"] == null ? "" : Request.Form["pItemCd"].ToString();
                    hdnItemCd.Value = pItemCd;

                    string pItemSeq = Request.Form["pItemSeq"] == null ? "" : Request.Form["pItemSeq"].ToString();
                    hdnItemSeq.Value = pItemSeq;
                }
            }
        }
        protected void btnFileDownload_ServerClick(object sender, EventArgs e)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYIMAGEFILE";
            string jobGb = "GET";
            string errCd = string.Empty;
            string errMsg = string.Empty;
            DataSet ds = new DataSet();

            List<string> aData = new List<string>();

            aData.Add(hdnFileSeq.Value);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                (ds, errCd, errMsg) = dbTransaction.GetTableRow();
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //SELECT FILENM, FILEDATA, FILETYPE, FILESIZE 
                    int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                    byte[] bFile = new byte[nFileLen];
                    bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                    string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                    using (FileUtil fileUtil = new FileUtil())
                    {
                        fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                    }
                }

            }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetImage(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYIMAGEFILE";
            string jobGb = "GET";
            string errCd = string.Empty;
            string errMsg = string.Empty;
            DataSet ds = new DataSet();

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                (ds, errCd, errMsg) = dbTransaction.GetTableRow();
            }

            DataTable dtImg = new DataTable();
            dtImg.Columns.Add("SERVICE_ERROR_CD", typeof(string));
            dtImg.Columns.Add("SERVICE_ERROR_MSG", typeof(string));
            dtImg.Columns.Add("SERVICE_ERROR_REDIRECT", typeof(string));
            dtImg.Columns.Add("SRC", typeof(string));

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //SELECT FILENM, FILEDATA, FILETYPE, FILESIZE 
                    string strSrc = string.Empty;
                    string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();
                    string strFileType = ds.Tables[0].Rows[0]["FILETYPE"].ToString();
                    int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                    byte[] bFile = new byte[nFileLen];
                    bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                    DataRow dr = dtImg.NewRow();

                    strSrc = "data:" + strFileType + "; base64," + Convert.ToBase64String(bFile);

                    dtImg.Rows.Add(errCd, errMsg, "", strSrc);

                }
            }
            else
            {
                dtImg.Rows.Add(errCd, errMsg, "", "");
            }

            result = Utility.DataTableToJson(dtImg);

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCode(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CODECOLLECTION";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SAFETYREQUESTUSERHIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

    }
}