﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyLoseRequestMngPopup.aspx.cs" Inherits="KTSSolutionWeb.SafetyLoseRequestMngPopup" enableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>안전용품등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
                /* 모달 창 스타일 */
        .img-modal {
          display: none; /* 초기에는 보이지 않음 */
          position: fixed;
          z-index: 1;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          overflow: auto;
          background-color: rgb(0,0,0);
          background-color: rgba(0,0,0,0.9);
        }

        /* 모달 내부 이미지 스타일 */
        .img-modal-content {
          width: 100%;
          transition: transform 0.3s ease;
        }

        /* 모달 내용 스타일 */
        .img-modal-contentDiv {
          margin: auto;
          width: 50%;
          padding: 20px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoom {
          position: absolute;
          top: 15px;
        }

        /* 확대/축소 버튼 스타일 */
        .img-modal-zoomIn, .img-modal-zoomOut {
          top: 15px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          padding-left: 10px;
          cursor:pointer;
        }

        /* 닫기 버튼 스타일 */
        .img-modal-close {
          position: absolute;
          top: 15px;
          right: 35px;
          color: white;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
          cursor:pointer;
        }

        .img-modal-close:hover,
        .img-modal-close:focus {
          color: #bbb;
          text-decoration: none;
          cursor: pointer;
        }

        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type="text"]:read-only, textarea:read-only {
          background : #e3e3e3;
          border: solid 1px #9e9e9e;
        }

        .scrollbox {
            overflow-x: hidden;
        }

        .filebox {
            margin-right: 0;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var tranGb;

        
        var memo = $("#txtMemo").val();
        var empno = $("#hdnEmpNo").val();

        var giveDate;
        var itemCd;
        var itemSeq;
        var reqGb;
        var reqGbNm;
        var fileSeq3;
        var chgReason;
        var chgReasonNm;
        var memo;
        var empno;
        var reqStatusCd = '';
        var reqStatusNm = '';
        var currentScale = 1; // 이미지 확대/축소 버튼 클릭 이벤트 핸들러
        var imageCall = 0;

        var check = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $("#imgFileView3").on("click", function () {
                    $("#imgModal").attr("src", $(this).attr("src"));
                    $("#divImgViewZoom").css("display", "flex");
                });

                $(".img-modal-zoomIn").on("click", function () {
                    currentScale += 0.1;
                    $("#imgModal").css("transform", "scale(" + currentScale + ")");
                });

                $(".img-modal-zoomOut").on("click", function () {
                    if (currentScale > 0.1) {
                        currentScale -= 0.1;
                        $(".img-modal-content").css("transform", "scale(" + currentScale + ")");
                    }
                });

                // 모달 닫기 버튼을 클릭했을 때 모달을 닫는 이벤트 처리
                $(".img-modal-close").on("click", function () {
                    $("#divImgViewZoom").css("display", "none");
                    currentScale = 1;
                });

                $("#selReq").change(function (e) {
                    if (this.value == '01') {
                        $("#thReason").show();
                        $("#tdReason").show();
                    }
                    else {
                        $("#thReason").hide();
                        $("#tdReason").hide();
                    }
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran("1");
                });

                $("#btnReg2").click(function (e) {
                    fn_RegTran("2");
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });

                $("#aFile3").click(function (e) {
                    $("#<%= hdnFileSeq.ClientID %>").val($("#<%= hdnFileSeq3.ClientID %>").val());
                    fn_FileDownload();
                });

                $("#imgFile3").click(function (e) {
                    $("#<%= hdnFileSeq3.ClientID %>").val('');
                    $('#aFile3').text('');
                    $("#pFilebox3").show();
                    $("#pFileA3").hide();
                    $('#txtfu3').val('');
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        }

        var fn_Load = function () {
            fn_Req();
            $("#imgFile3").hide();
        }

        var fn_GetImage = function (fileNmSeq) {
            callGb = 'getImage';

            //입력 파라미터
            var aData = [];
            aData[0] = fileNmSeq;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl('/Safety/SafetyRequestUserHistPopup', 'GetImage');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_FileAdd = function (seq) {
            $("#hdnReq").val($("#selReq").val());

            if (seq == 3) {
                if ($('#txtfu3').val() == '') {
                    ktsGridAlert('첨부파일이 없습니다.');
                    return false;
                }
            }

            return true;
        }

        var fn_SetHiden = function () {
            
        }

        var fn_FileDownload = function () {
            <%=Page.GetPostBackEventReference(btnFileDownload)%>;
        }

        var fn_RegTran = function (gubun) {

            if (gubun == "1") {
                if (fn_RegChk()) {
                    if (confirm("수리진행승인을 진행하시겠습니까?")) {
                        callGb = 'RegTran';

                        fn_Transaction('1', 'U');
                    }
                }
            }
            else {
                if (fn_RegChk()) {
                    if (confirm("상위부서요청을 진행하시겠습니까?")) {
                        callGb = 'RegTran';

                        fn_Transaction('2', 'U');
                    }
                }
            }
            
        }

        var fn_RegChk = function () {
            var selReq = $("#selReq").val();

            /*
            if (selReq == '01' && $('#aFile3').text() == '') {

                ktsGridAlert('첨부파일을 첨부해야 합니다.');
                return false;
            }
            */
            return true;
        }

        var fn_Req = function () {
            callGb = 'selReq';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S09';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (tran, gubun) {
            giveDate = $("#txtGiveDate").val();
            itemCd = $("#hdnItemCd").val();
            itemSeq = $("#txtItemSeq").val();
            reqGb = $("#selReq").val();
            reqGbNm = $("#selReq option:selected").text();
            fileSeq3 = $("#<%= hdnFileSeq3.ClientID %>").val();
            file3 = $('#aFile3').text();
            memo = $("#txtMemo").val();
            empno = $("#hdnEmpNo").val();

            tranGb = tran;

            if (tran == "1") {
                reqStatusCd = 'AA';
                reqStatusNm = '수리진행승인';
            }
            else {
                reqStatusCd = '01';
                reqStatusNm = '상위부서요청';
            }

            if (reqGb != '01') {
                chgReason = '';
                chgReasonNm = '';
            }

            //입력 파라미터
            var aData = [];
            aData[0] = tran;
            aData[1] = gubun;
            aData[2] = giveDate;
            aData[3] = itemCd;
            aData[4] = itemSeq;
            aData[5] = reqStatusCd;
            aData[6] = reqStatusNm;
            aData[7] = reqGb;
            aData[8] = reqGbNm;
            aData[9] = fileSeq3;
            aData[10] = memo;
            aData[11] = empno;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj1;
            var obj2;

            if (callGb == 'selReq') {
                obj1 = $('#selReq');
                obj1.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj1.append("<option value='" + item.CODECD + "'>" + item.CODENM + "</option>");
                });

                var vFileSeq3 = $("#<%= hdnFileSeq3.ClientID %>").val();
                if (vFileSeq3 != '') {
                    var val = $('#txtfu3').val();

                    if (val != undefined) {
                        if (val.indexOf('C:\\fakepath\\') > 0) {
                            val = val.replace('C:\\fakepath\\', '')
                        }

                        $("#pFilebox3").hide();
                        $("#imgFile3").show();
                    }
                }

                var selData = '';

                if ($("#hdnReq").val() != undefined) {
                    selData = $("#hdnReq").val();
                }

                if (selData != '') {
                    obj1.val(selData).prop("selected", true);
                }

                if ($('#aFile3').text() != '') {
                    imageCall = 3;
                    fn_GetImage(vFileSeq3);
                }
                else {
                    callGb = '';
                }
            }
            else if (callGb == 'getImage') {
                var vFileSeq3 = $("#hdnFileSeq3").val();

                $.each(JSON.parse(json), function (idx, item) {
                    if (imageCall == 3) {
                        $('#imgFileView3').attr('src', item.SRC);
                        $("#imgFileView3").css("width", "20px");
                        $("#imgFileView3").css("height", "20px")
                    }
                });

                imageCall = 0;
                callGb = '';
            }
            else if (callGb == 'RegTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('등록되었습니다.');

                        setTimeout(function () {
                            SendParentsForm();
                        }, 1000);
                    }
                });

                check = 'ok';
                callGb = '';
            }
        }

        function SendParentsForm() {

            var cellNo = $("#hdnId").val();
            
            opener.SetDispItem(check, cellNo, reqGbNm, reqStatusCd, reqStatusNm, memo, file3, fileSeq3, tranGb);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>안전용품</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th style="width: 120px;">소속</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtOrgNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnId" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">이름</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnEmpNo" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">직무</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtEmpJobNm" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">지급일자</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtGiveDate" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">품목</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemNm" tabindex="1" style="width:200px;" readonly="true" runat="server" />
                                                    <input type="hidden" id="hdnItemCd" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width: 120px;">품목순번</th>
                                            <td class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtItemSeq" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">요청구분</th>
                                            <td class="td-left">
                                                <span class="optionbox">
                                                    <select id="selReq" style="width:112px;">
                                                    </select>
                                                    <input type="hidden" id="hdnReq" runat="server" />
                                                </span>
                                            </td>
                                            <th id="thReason" style="width: 120px;">교체사유</th>
                                            <td id="tdReason" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtChgReason" tabindex="1" style="width:112px;" readonly="true" runat="server" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고(교체신청)</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo11" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">비고</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txtMemo" runat="server" MaxLength="200" style="min-height:100px;height:100px;width:100%" TextMode="MultiLine"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trFile3">
                                            <th style="width: 120px;">첨부파일</th>
                                            <td colspan="3" class="td-left">
                                                <asp:UpdatePanel ID="updPanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <p id="pFilebox3" class="filebox" runat="server">
                                                        <input type="text" id="txtfu3" style="width: 236px;margin-left:0px" runat="server" />
                                                        <span class="file">
                                                            <input type="file" id="fu3" name="fu3" onchange="document.getElementById('txtfu3').value=this.value;" />
                                                            <label for="fu3">찾아보기</label>
                                                        </span>
                                                        <asp:Button ID="btnFileAdd3" runat="server" class="btn-black" OnClientClick="return fn_FileAdd('3');" OnClick="btnFileAdd3_Click" Text="첨부" />
                                                    </p>
                                                </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnFileAdd3" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <p id="pFileA3" class="filebox">
                                                    <img id="imgFileView3" style="cursor:pointer" />
                                                    <a id="aFile3" href="#" runat="server"></a>
                                                    <img id="imgFile3" src="/Resource/images/icon_minus_02.png" alt="삭제" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" style="width:114px" value="수리진행승인" />
                                                <input id="btnReg2" type="button" class="btn-save" style="width:114px" value="상위부서요청" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                            <asp:Button id="btnFileDownload" OnClick="btnFileDownload_ServerClick" runat="server" Visible="false" Width="0px" class="btn-black" Text="" />
                            <input type="hidden" id="hdnFileSeq3" runat="server" />
                            <input type="hidden" id="hdnFileSeq" runat="server" />
                        </div>
                        <!-- //E:btncenter -->
                        <div id="divImgViewZoom" class="img-modal">
                            <span class="img-modal-close">&times;</span>
                            <div id="divZoom" class="img-modal-zoom">
                                <span class="img-modal-zoomIn">+</span>
                                <span class="img-modal-zoomOut">-</span>
                            </div>
                            <div id="divModalContent" class="img-modal-contentDiv">
                                <img class="img-modal-content" id="imgModal" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>