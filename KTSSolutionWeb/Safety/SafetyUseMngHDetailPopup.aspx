﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SafetyUseMngHDetailPopup.aspx.cs" Inherits="KTSSolutionWeb.SafetyUseMngHDetailPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>품목상세정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
        .selCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }
        .txtCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var chkOrgCd = '';
        var chkOrgNm = '';
        var gubun;
        var rtn;
        var selUseJson;
        var selStatusJson;
        var selOperJson;
        var selTypeJson;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });

                $(".date").datepicker({ dateFormat: 'yy-mm-dd' });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            fn_SetSelField();
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";

            return html;
        }

        var fn_CellLostRequest = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnLostChg' type='button' onclick='fn_LostRequest(\"" + cellNo + "\");' value='교체신청' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";

            return html;
        }

        var fn_UseChg = function (obj, cellNo) {
            var selectedValue = obj.value;
            var selectedText = $(obj).find(':selected').text();

            ktsGrid.updateData([{
                id: cellNo, USECD: selectedValue, USENM: selectedText
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            //obj.style.cssText = 'background-color: #1d68cd1f !important;';

        }

        var fn_CellUse = function (cell, formatterParams, onRendered) { //사용용도
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;
            var value = cell.getRow().getData().USECD;
            var selectedChk = false;

            html = "<select id='selUse' class='selCell' onchange='fn_UseChg(this, \"" + id + "\");'>";

            $.each(JSON.parse(selUseJson), function (idx, item) {
                if (item.CODECD == value) {
                    selectedChk = true;
                }
            });

            $.each(JSON.parse(selUseJson), function (idx, item) {
                if (idx == 0 && selectedChk == false) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else if (item.CODECD == value) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else {
                    html += "<option value='" + item.CODECD + "'>" + item.CODENM + "</option>";
                }
            });

            html += "</select>";

            return html;
        }

        var fn_ItemStatusChg = function (obj, cellNo) {
            var selectedValue = obj.value;
            var selectedText = $(obj).find(':selected').text();

            ktsGrid.updateData([{
                id: cellNo, ITEMCD_STATUSCD: selectedValue, ITEMNM_STATUSNM: selectedText
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            //obj.style.cssText = 'background-color: #1d68cd1f !important;';
        }

        var fn_CellItemStatus = function (cell, formatterParams, onRendered) { //사용용도
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;
            var value = cell.getRow().getData().ITEMCD_STATUSCD;
            var selectedChk = false;

            html = "<select id='selItemStatus' class='selCell' onchange='fn_ItemStatusChg(this, \"" + id + "\");'>";

            $.each(JSON.parse(selStatusJson), function (idx, item) {
                if (item.CODECD == value) {
                    selectedChk = true;

                }
            });

            $.each(JSON.parse(selStatusJson), function (idx, item) {
                if (idx == 0 && selectedChk == false) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else if (item.CODECD == value) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else {
                    html += "<option value='" + item.CODECD + "'>" + item.CODENM + "</option>";
                }
            });

            html += "</select>";


            return html;
        }

        var fn_OperStatusChg = function (obj, cellNo) {
            var selectedValue = obj.value;
            var selectedText = $(obj).find(':selected').text();

            ktsGrid.updateData([{
                id: cellNo, OPER_STATUSCD: selectedValue, OPER_STATUSNM: selectedText
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            //obj.style.cssText = 'background-color: #1d68cd1f !important;';
        }

        var fn_CellOperStatus = function (cell, formatterParams, onRendered) { //제품상태
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;
            var value = cell.getRow().getData().OPER_STATUSCD;
            var selectedChk = false;

            html = "<select id='selOperStatus' class='selCell' onchange='fn_OperStatusChg(this, \"" + id + "\");'>";

            $.each(JSON.parse(selOperJson), function (idx, item) {
                if (item.CODECD == value) {
                    selectedChk = true;
                }
            });

            $.each(JSON.parse(selOperJson), function (idx, item) {
                if (idx == 0 && selectedChk == false) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else if (item.CODECD == value) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else {
                    html += "<option value='" + item.CODECD + "'>" + item.CODENM + "</option>";
                }
            });

            html += "</select>";


            return html;
        }

        var fn_HoldTypeChg = function (obj, cellNo) {
            var selectedValue = obj.value;
            var selectedText = $(obj).find(':selected').text();

            ktsGrid.updateData([{
                id: cellNo, HOLD_TYPECD: selectedValue, HOLD_TYPENM: selectedText
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            //obj.style.cssText = 'background-color: #1d68cd1f !important;';
        }

        var fn_CellHoldType = function (cell, formatterParams, onRendered) { //보유형태
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;
            var value = cell.getRow().getData().HOLD_TYPECD;
            var selectedChk = false;

            html = "<select id='selHoldType' class='selCell' onchange='fn_HoldTypeChg(this, \"" + id + "\");'>";

            $.each(JSON.parse(selTypeJson), function (idx, item) {
                if (item.CODECD == value) {
                    selectedChk = true;
                }
            });

            $.each(JSON.parse(selTypeJson), function (idx, item) {
                if (idx == 0 && selectedChk == false) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else if (item.CODECD == value) {
                    html += "<option value='" + item.CODECD + "' selected='selected'>" + item.CODENM + "</option>";
                }
                else {
                    html += "<option value='" + item.CODECD + "'>" + item.CODENM + "</option>";
                }
            });

            html += "</select>";


            return html;
        }

        var fn_MemoChg = function (obj, cellNo) {
            var inputValue = obj.value;

            ktsGrid.updateData([{
                id: cellNo, MEMOTEXT: inputValue
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            //obj.style.cssText = 'background-color: #1d68cd1f !important;';
        }

        var fn_CellMemo = function (cell, formatterParams, onRendered) { //메모
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;
            var value = cell.getRow().getData().MEMO;

            ktsGrid.updateData([{
                id: cellNo, MEMOTEXT: value
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });


            html = "<textarea id='txtMemo' class='txtCell' rows='1' cols='50' onchange='fn_MemoChg(this, \"" + id + "\");'>";
            html += value;
            html += "</textarea>";

            return html;
        }

        var fn_BuyDateChg = function (obj, cellNo) {
            var inputValue = obj.value;

            ktsGrid.updateData([{
                id: cellNo, BUY_DATETEXT: inputValue
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            //obj.style.cssText = 'background-color: #1d68cd1f !important;';
        }

        var fn_BuyDateClick = function (obj) {
            $(obj).datepicker({ dateFormat: 'yy-mm-dd' });
            $(obj).datepicker().datepicker("show");
        }

        var fn_CellBuyDate = function (cell, formatterParams, onRendered) { //구매일자
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;
            var value = cell.getRow().getData().BUY_DATE;

            ktsGrid.updateData([{
                id: cellNo, BUY_DATETEXT: value
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            //BUY_DATETEXT
            html = "<input type='text' id='txtBuyDate' class='txtCell' readonly='true' onclick='fn_BuyDateClick(this);' onchange='fn_BuyDateChg(this, \"" + id + "\");' value=\"" + value+ "\" />";

            return html;
        }


        var fn_GridInit = function () {

            var Columns = [
                { formatter: fn_CellLostRequest, width: 70, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목순번", field: "ITEMSEQ", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "구매일자", field: "BUY_DATE", sorter: "string", width: 100, formatter: fn_CellBuyDate },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도코드", field: "USECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도", field: "USENM", sorter: "string", width: 100, formatter: fn_CellUse },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태코드", field: "ITEMCD_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태", field: "ITEMNM_STATUSNM", sorter: "string", width: 100, formatter: fn_CellItemStatus },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태코드", field: "OPER_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태", field: "OPER_STATUSNM", sorter: "string", width: 100, formatter: fn_CellOperStatus },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태코드", field: "HOLD_TYPECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태", field: "HOLD_TYPENM", sorter: "string", width: 100, formatter: fn_CellHoldType },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string", width: 240, formatter: fn_CellMemo },
                { headerHozAlign: "center", hozAlign: "center", title: "구매일자입력값", field: "BUY_DATETEXT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "비고입력값", field: "MEMOTEXT", sorter: "string", width: 200, visible: false },
                { formatter: fn_CellReg, width: 150, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("62vh");

        }

        var fn_Load = function () {
            fn_Use();
        }

        var delFlag = false;
        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selUse') {
                selUseJson = json;

                fn_Status();
            }
            else if (callGb == 'selStatus') {
                selStatusJson = json;

                fn_Oper();
            }
            else if (callGb == 'selOper') {
                selOperJson = json;

                fn_Type();
            }
            else if (callGb == 'selType') {
                selTypeJson = json;

                fn_Search();

                callGb = '';
            }
            else if (callGb == 'delTran') {
                ktsGridAlert("삭제를 완료했습니다.");

                delFlag = true;
                fn_Search();

                callGb = '';
            }
            else if (callGb == 'regTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        ktsGridAlert('변경되었습니다.');
                    }
                });

                fn_Search();

                callGb = '';
            }

        }

        var SetDispItem = function (check, cellNo, buyDate, use, useNm, status, statusNm, oper, operNm, type, typeNm, memo) {

            if (check == 'ok') {

                var row = ktsGrid.getRow(cellNo);
                row.select();

                ktsGrid.updateData([{
                    id: cellNo, BUY_DATE: buyDate, USECD: use, USENM: useNm, ITEMCD_STATUSCD: status, ITEMNM_STATUSNM: statusNm, OPER_STATUSCD: oper, OPER_STATUSNM: operNm, HOLD_TYPECD: type, HOLD_TYPENM: typeNm, MEMO: memo
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

                callGb = '';
            }
        }

        var fn_Reg = function (id) {
            if (confirm("정말로 변경 하시겠습니까?")) {
                callGb = 'regTran';
                fn_Transaction('M', id);
            }
        }

        var fn_Del = function (id) {
            if (confirm("정말로 삭제 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('D', id);
            }
        }

        var fn_LostRequest = function (id) {
            if (confirm("교체신청 화면으로 전환 하시겠습니까?")) {
                fn_DirectLost(id);
            }
        }

        var fn_DirectLost = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vGIVE_DATE = rowData.GIVE_DATE;
            var vEMPNO = rowData.EMPNO;
            var vITEMCD = rowData.ITEMCD;
            var vITEMNM = rowData.ITEMNM;
            var vITEMSEQ = rowData.ITEMSEQ;

            var form = "/Safety/SafetyUseLoseRequestPopup";
            var param = {
                pGubun: gubun,
                pGiveDate: vGIVE_DATE,
                pEmpNo: vEMPNO,
                pItemCd: vITEMCD,
                pItemNm: vITEMNM,
                pItemSeq: vITEMSEQ
            };

            var directform = createForm(form, param);

            directform.submit();
        }

        var fn_Use = function () {
            callGb = 'selUse';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S01';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Status = function () {
            callGb = 'selStatus';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S02';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Oper = function () {
            callGb = 'selOper';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S03';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Type = function () {
            callGb = 'selType';

            //입력 파라미터
            var aData = [];
            aData[0] = '1';
            aData[1] = '';
            aData[2] = 'S04';
            aData[3] = 'A01';
            aData[4] = 'Y';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCode');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


        var fn_Transaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var giveDate = rowData.GIVE_DATE;
            var itemCd = rowData.ITEMCD;
            var itemSeq = rowData.ITEMSEQ;
            var empno = rowData.EMPNO;

            var buyDate = rowData.BUY_DATE;
            var buyDateText = rowData.BUY_DATETEXT;
            var use = rowData.USECD;
            var useNm = rowData.USENM;
            var status = rowData.ITEMCD_STATUSCD;
            var statusNm = rowData.ITEMNM_STATUSNM;
            var oper = rowData.OPER_STATUSCD;
            var operNm = rowData.OPER_STATUSNM;
            var type = rowData.HOLD_TYPECD;
            var typeNm = rowData.HOLD_TYPENM;
            var memo = rowData.MEMO;
            var memoText = rowData.MEMOTEXT;

            if (buyDateText != null) {
                buyDate = buyDateText;
            }

            if (memoText != null) {
                memo = memoText;
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = giveDate;
            aData[2] = itemCd;
            aData[3] = itemSeq;
            aData[4] = buyDate;
            aData[5] = use;
            aData[6] = useNm;
            aData[7] = status;
            aData[8] = statusNm;
            aData[9] = oper;
            aData[10] = operNm;
            aData[11] = type;
            aData[12] = typeNm;
            aData[13] = memo;
            aData[14] = empno;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_SetSelField = function () {
            var obj1 = $('#selField');

            obj1.empty();
            obj1.append("<option value=''>선택</option>");
            obj1.append("<option value='GIVE_DATE'>지급일자</option>");
        }

        var fn_Search = function () {
            var empNo = $("#hdnEmpNo").val();
            var itemCd = $("#hdnItemCd").val();

            //입력 파라미터
            var aData = [];
            aData[0] = '';
            aData[1] = 'DETAIL';
            aData[2] = empNo;
            aData[3] = itemCd;
            
            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function SendParentsForm() {
            opener.SetDispItem2(delFlag);
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">품목상세정보</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <input type="hidden" id="hdnEmpNo" runat="server" />
                    <input type="hidden" id="hdnItemCd" runat="server" />
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <strong>조회 결과</strong>
                        <label class="searchrowcount"></label>
			            <div class="pull-right">
				            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				            </div>
			            </div>
                    </div>
                    <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none;">
                                <span>
                                    <select id="selField">
                                        <option value="">선택</option>
                                    </select> : 
                                    <input id="inputValue" disabled="disabled" type="text" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin:20px 30px 0 30px;display:grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>
