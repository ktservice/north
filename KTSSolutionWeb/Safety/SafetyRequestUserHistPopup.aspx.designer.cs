﻿//------------------------------------------------------------------------------
// <자동 생성됨>
//     이 코드는 도구를 사용하여 생성되었습니다.
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다. 
// </자동 생성됨>
//------------------------------------------------------------------------------

namespace KTSSolutionWeb
{


    public partial class SafetyRequestUserHistPopup
    {

        /// <summary>
        /// form1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// txtOrgNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtOrgNm;

        /// <summary>
        /// hdnGubun 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnGubun;

        /// <summary>
        /// hdnId 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnId;

        /// <summary>
        /// hdnGiveDate 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnGiveDate;

        /// <summary>
        /// hdnEmpNo 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnEmpNo;

        /// <summary>
        /// hdnItemCd 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnItemCd;

        /// <summary>
        /// hdnItemSeq 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnItemSeq;

        /// <summary>
        /// txtEmpNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtEmpNm;

        /// <summary>
        /// txtEmpJobNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtEmpJobNm;

        /// <summary>
        /// txtGiveDate 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtGiveDate;

        /// <summary>
        /// txtItemNm 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtItemNm;

        /// <summary>
        /// txtItemSeq 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtItemSeq;

        /// <summary>
        /// txtReq 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtReq;

        /// <summary>
        /// txtChgReason 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtChgReason;

        /// <summary>
        /// txtChgDt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtChgDt;

        /// <summary>
        /// txtChgEmp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtChgEmp;

        /// <summary>
        /// txtReqDt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtReqDt;

        /// <summary>
        /// txtReqEmp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtReqEmp;

        /// <summary>
        /// txtAccDt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtAccDt;

        /// <summary>
        /// txtAccEmp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtAccEmp;

        /// <summary>
        /// txtDisuseDt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtDisuseDt;

        /// <summary>
        /// txtDisuseEmp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtDisuseEmp;

        /// <summary>
        /// txtAppDt 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtAppDt;

        /// <summary>
        /// txtAppEmp 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtAppEmp;

        /// <summary>
        /// txtMemo11 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtMemo11;

        /// <summary>
        /// txtMemo1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtMemo1;

        /// <summary>
        /// txtMemo2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtMemo2;

        /// <summary>
        /// txtMemo3 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtMemo3;

        /// <summary>
        /// txtReq2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtReq2;

        /// <summary>
        /// aFile1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aFile1;

        /// <summary>
        /// aFile2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aFile2;

        /// <summary>
        /// aFile3 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor aFile3;

        /// <summary>
        /// btnFileDownload 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnFileDownload;

        /// <summary>
        /// hdnFileSeq1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnFileSeq1;

        /// <summary>
        /// hdnFileSeq2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnFileSeq2;

        /// <summary>
        /// hdnFileSeq3 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnFileSeq3;

        /// <summary>
        /// hdnFileSeq 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden hdnFileSeq;
    }
}
