﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SafetyDisuseMng.aspx.cs" Inherits="KTSSolutionWeb.SafetyDisuseMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-col-title {
            text-align: center !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var regCnt = 1;
        var arrReg = [];

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= txbEnDt.ClientID %>").change(function () {
                    $("#<%= hfBaseDt.ClientID %>").val($("#<%= txbEnDt.ClientID %>").val());
                    __doPostBack("<%=hfBaseDt.ClientID %>", "");
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'd') {
                        $("#lblDt").show();
                        $("#<%= txbStDt.ClientID %>").show();
                        $("#<%= txbEnDt.ClientID %>").show();
                        $("#selMon").hide();
                    }
                    else {
                        fn_Month();
                        $("#lblDt").hide();
                        $("#<%= txbStDt.ClientID %>").hide();
                        $("#<%= txbEnDt.ClientID %>").hide();
                        $("#selMon").show();
                    }
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    $("input:checkbox[id='selectAllCheckbox']").prop("checked", false);
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "안전용품 불용폐기 관리.xlsx", { sheetName: "안전용품 불용폐기 관리" });
                });

                $("#btnReg").click(function (e) {
                    fn_Reg();
                });

                $("#selectAllCheckbox").change(function () {
                    var isChecked = this.checked;
                    var checked = 'N';

                    if (isChecked) {
                        checked = 'Y';
                    }

                    $(".cell-check").each(function () {
                        this.checked = isChecked;
                    });

                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#<%= txbStDt.ClientID %>").show();
            $("#<%= txbEnDt.ClientID %>").show();
            $("#lblDt").show();
            $("#selMon").hide();
        }


        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellCheck = function (cell, formatterParams, onRendered) {
            var html = '';
            var cellNo = cell.getRow().getData().id;
            var cellDisuseCd = cell.getRow().getData().DISUSECD;

            if ($("#selDisuse").val() == '01') {
                if (cellDisuseCd == '') {
                    var objId = 'chkEmp' + cellNo;

                    html = "<input id='" + objId +"' type='checkbox' class='cell-check' />";
                }
            }
            
            return html;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var cellNo = cell.getRow().getData().id;

            if ($("#selDisuse").val() == '01') {
                html = "<input id='btnView' type='button' onclick='fn_View(\"" + cellNo + "\");' value='보기' class='btn-gray' style='min-width:40px;height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            }
            else {
                html = "<input id='btnView' type='button' onclick='fn_Hist(\"" + cellNo + "\");' value='보기' class='btn-gray' style='min-width:40px;height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            }

            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { title: "<input id='selectAllCheckbox' type='checkbox'/>", hozAlign: "center", field: "CHECK", headerSort: false, formatter: fn_CellCheck, headerSort: false, width: 40, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "신청일자", field: "REG_DATE", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "승인일자", field: "APPROVAL_DATE", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "인수일자", field: "ACCEPT_DATE", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "불용폐기일자", field: "DISUSE_DATE", sorter: "string", width: 110, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "이력일자", field: "HIST_DATE", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "신청자명", field: "CHG_EMPNM", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "신청자사번", field: "CHG_EMPNO", sorter: "string", width: 100, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목순번", field: "ITEMSEQ", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도코드", field: "USECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도", field: "USENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "규격", field: "STANDARD", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "단위", field: "UNIT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태코드", field: "ITEMCD_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태", field: "ITEMNM_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태코드", field: "OPER_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태", field: "OPER_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태코드", field: "HOLD_TYPECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태", field: "HOLD_TYPENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "교체사유", field: "CHG_REASONNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "요청구분코드", field: "REQ_GB", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "요청구분", field: "REQ_GBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "불용처리상태코드", field: "DISUSECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "불용처리상태", field: "DISUSENM", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(교체신청)", field: "MEMO11", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(신청)", field: "MEMO1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(승인)", field: "MEMO3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목파일", field: "FILENM2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목파일순번", field: "FILE_SEQ2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "첨부파일", field: "FILENM3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "첨부파일순번", field: "FILE_SEQ3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "REQUEST_STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "이력순번", field: "HIST_SEQ", visible: false },
                { field: "REQUEST_STATUSCD", formatter: fn_CellReg, width: 100, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_AlertDialog = function (message, gubun) {
            $("#alertDialog").html("<p>" + message + "</p>");

            $("#alertDialog").dialog({
                modal: true,
                buttons: {
                    Ok: function () {
                        if (gubun == 'RegTran') {
                            fn_Search();
                        }

                        $(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                    if (gubun == 'RegTran') {
                        fn_Search();
                    }
                }
            });
        }

        var fn_ConfirmDialog = function (message) {
            $("#confirmDialog").html("<p>" + message + "</p>");

            $("#confirmDialog").dialog({
                modal: true,
                buttons: {
                    "Yes": function () {
                        $("#updateProgress").show();
                        fn_Transaction(arrReg[0]);

                        $(this).dialog("close");
                    },
                    "No": function () {
                        $(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                }
            });
        }


        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_RegChk = function () {
            $(".cell-check").each(function () {
                if (this.checked) {
                    var cId = this.id.replace('chkEmp', '');

                    arrReg.push(cId);
                }
            });

            if (arrReg.length > 0) return true;

            return false;
        }

        var fn_Reg = function () {
            arrReg = [];
            regCnt = 1;

            if (fn_RegChk()) {
                fn_ConfirmDialog('일괄폐기 처리 하시겠습니까?');
            }
            else {
                ktsGridAlert("일괄폐기 대상을 체크하세요.");
            }

        }

        var fn_Hist = function (id) {
            fn_PopupHist('1', id);
        }

        var fn_View = function (id) {
            fn_PopupReg(id);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'RegTran') {
                if (arrReg.length == regCnt) {
                    $("#updateProgress").hide();
                    callGb = '';
                    var msg = '';
                    msg = regCnt + '건이 처리되었습니다.';

                    fn_AlertDialog(msg, 'RegTran');
                }
                else {
                    var regId = regCnt;
                    regCnt++;
                    fn_Transaction(arrReg[regId]);
                }
            }
        }

        var SetDispItem = function () {
        }

        var fn_PopupHist = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vHistSeq = rowData.HIST_SEQ;

            var form = "/Safety/SafetyRequestUserHistPopup";
            var target = "SafetyRequestUserHistPopup";
            var param = {
                pGubun: gubun,
                pIId: vHistSeq
            };

            var nWidth = 720;
            var nHeight = 1100;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupReg = function (id) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vRequestStatusCd = rowData.REQUEST_STATUSCD;
            var vRequestStatus = rowData.REQUEST_STATUS;
            var vGiveDate = rowData.GIVE_DATE;

            var vOrgNm = rowData.ORGNM;
            var vEmpNm = rowData.EMPNM;
            var vEmpNo = rowData.EMPNO;
            var vEmpJobNm = rowData.JOBNM;
            var vItemNm = rowData.ITEMNM;
            var vItemCd = rowData.ITEMCD;
            var vItemSeq = rowData.ITEMSEQ;
            var vChgReasonNm = rowData.CHG_REASONNM;
            var vReqGb = rowData.REQ_GB;
            var vReqGbNm = rowData.REQ_GBNM;
            var vMemo1 = rowData.MEMO1;
            var vMemo3 = rowData.MEMO3;
            var vMemo11 = rowData.MEMO11;
            
            var vFileNm2 = rowData.FILENM2;
            var vFileNm3 = rowData.FILENM3;
            
            var vFileSeq2 = rowData.FILE_SEQ2;
            var vFileSeq3 = rowData.FILE_SEQ3;

            var vAppDate = rowData.APPROVAL_DATE;
            var vAcceptDate = rowData.ACCEPT_DATE;
            
            var vDisuseDate = rowData.DISUSE_DATE;

            var form = "/Safety/SafetyDisuseMngPopup";
            var target = "SafetyDisuseMngPopup";
            var param = {
                pRequestStatus : vRequestStatus,
                pOrgNm         : vOrgNm,
                pEmpNm         : vEmpNm,
                pEmpNo         : vEmpNo,
                pEmpJobNm      : vEmpJobNm,
                pGiveDate      : vGiveDate,
                pAppDate       : vAppDate,
                pAcceptDate    : vAcceptDate,
                pDisuseDate    : vDisuseDate,
                pItemNm        : vItemNm,
                pItemCd        : vItemCd,
                pItemSeq       : vItemSeq,
                pChgReasonNm   : vChgReasonNm,
                pReqGbNm       : vReqGbNm,
                pMemo3         : vMemo3,
                pMemo1         : vMemo1,
                pMemo11        : vMemo11,
                pFileNm2       : vFileNm2,
                pFileNm3       : vFileNm3,
                pFileSeq2      : vFileSeq2,
                pFileSeq3      : vFileSeq3
            };

            var nWidth = 740;
            var nHeight = 1040;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Safety";
                var target = "OrgTree_Safety";
                var param = {
                    pORGCD: orgcd,
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }


        var fn_Transaction = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var pGubun = 'U';
            var pGiveDate = rowData.GIVE_DATE;
            var pItemCd = rowData.ITEMCD;
            var pItemSeq = rowData.ITEMSEQ;
            var pDisuseCd = 'P1';
            var pDisuseNm = '폐기';
            var pEmpNo = rowData.EMPNO;

            callGb = 'RegTran';

            //입력 파라미터
            var aData = [];

            aData[0] = pGubun;
            aData[1] = pGiveDate;
            aData[2] = pItemCd;
            aData[3] = pItemSeq;
            aData[4] = pDisuseCd;
            aData[5] = pDisuseNm;
            aData[6] = pEmpNo;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }



        var fn_SearchChk = function() {
            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate1 = '';
                var pDate2 = '';

                if ($("#selDt").val() == 'd') {
                    pDate1 = $("#<%= txbStDt.ClientID %>").val();
                    pDate2 = $("#<%= txbEnDt.ClientID %>").val();
                }
                else {
                    pDate1 = $("#selMon").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[2] = pDate1;
                aData[3] = pDate2;
                aData[4] = $("#selDisuse").val();

                if ($("#selDisuse").val() == '01') {

                    
                    ktsGrid.hideColumn("HIST_DATE");
                    ktsGrid.hideColumn("CHG_EMPNM");
                    ktsGrid.hideColumn("CHG_EMPNO");
                    ktsGrid.hideColumn("MEMO11");
                    ktsGrid.hideColumn("MEMO1");
                    ktsGrid.hideColumn("MEMO3");

                    ktsGrid.showColumn("CHECK");
                    ktsGrid.showColumn("REG_DATE");
                    ktsGrid.showColumn("DISUSE_DATE");
                    ktsGrid.showColumn("APPROVAL_DATE");
                    ktsGrid.showColumn("ACCEPT_DATE");
                    ktsGrid.showColumn("USECD");
                    ktsGrid.showColumn("USENM");
                    ktsGrid.showColumn("STANDARD");
                    ktsGrid.showColumn("UNIT");
                    ktsGrid.showColumn("SERVICELIFE");
                    ktsGrid.showColumn("ITEMCD_STATUSCD");
                    ktsGrid.showColumn("ITEMNM_STATUSNM");
                    ktsGrid.showColumn("OPER_STATUSCD");
                    ktsGrid.showColumn("OPER_STATUSNM");
                    ktsGrid.showColumn("HOLD_TYPECD");
                    ktsGrid.showColumn("HOLD_TYPENM");
                }
                else {
                    
                    ktsGrid.showColumn("HIST_DATE");
                    ktsGrid.showColumn("CHG_EMPNM");
                    ktsGrid.showColumn("CHG_EMPNO");
                    ktsGrid.showColumn("MEMO11");
                    ktsGrid.showColumn("MEMO1");
                    ktsGrid.showColumn("MEMO3");

                    ktsGrid.hideColumn("CHECK");
                    ktsGrid.hideColumn("REG_DATE");
                    ktsGrid.hideColumn("DISUSE_DATE");
                    ktsGrid.hideColumn("APPROVAL_DATE");
                    ktsGrid.hideColumn("ACCEPT_DATE");
                    ktsGrid.hideColumn("USECD");
                    ktsGrid.hideColumn("USENM");
                    ktsGrid.hideColumn("STANDARD");
                    ktsGrid.hideColumn("UNIT");
                    ktsGrid.hideColumn("SERVICELIFE");
                    ktsGrid.hideColumn("ITEMCD_STATUSCD");
                    ktsGrid.hideColumn("ITEMNM_STATUSNM");
                    ktsGrid.hideColumn("OPER_STATUSCD");
                    ktsGrid.hideColumn("OPER_STATUSNM");
                    ktsGrid.hideColumn("HOLD_TYPECD");
                    ktsGrid.hideColumn("HOLD_TYPENM");
                }

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
        <div id="alertDialog" class="alert" title="Alert"></div>
        <div id="confirmDialog" class="confirm" title="Confirm"></div>
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="optionbox">
                <label>일자</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="d">일</option>
                    <option value="m">월</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="date" ReadOnly="true" style="width:40% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox">
                    <label>구분</label>
                    <select id="selDisuse">
                        <option selected="selected" value="01">폐기대상</option>
                        <option value="02">이력</option>
                    </select>
                </span>
                <div class="btnSearchWrap">
                    <input type="hidden" id="hdnOrgLv"  runat="server" />
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>안전용품 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" style="float: right;margin-left: 10px;" value="엑셀" />
                        <input id="btnReg" type="button" class="btn-green last" style="float: right;margin-left: 10px;" value="일괄폐기" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="GIVE_DATE">지급일자</option>
                            <option value="ORGNM">소속</option>
                            <option value="EMPNM">이름</option>
                            <option value="EMPNO">사번</option>
                            <option value="KTSEMPNO">KTS사번</option>
                            <option value="ITEMNM">품목</option>
                            <option value="SERVICELIFE">내용연수</option>
                            <option value="REQUEST_STATUS">상태</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>