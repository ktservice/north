﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SafetyUseMngH.aspx.cs" Inherits="KTSSolutionWeb.SafetyUseMngH" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var Columns;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#selOut").change(function (e) {
                    if (this.value == 'B') $("#selC").show();
                    else $("#selC").hide();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_SearchChk();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "안전용품 사용현황 관리.xlsx", { sheetName: "안전용품 사용현황 관리" });
                });

                

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellPopup = function (e, cell, itemCd) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var itemCd = itemCd;

            if (itemCd.indexOf('A') >= 0) itemCd = itemCd.substring(1);

            if (value != "0") {
                fn_PopupDetail(id, itemCd);
            }

            return '';
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Reg(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnLostChg' type='button' onclick='fn_LostRequest(\"" + cellNo + "\");' value='교체신청' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";

            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
            ];
        }

        var fn_GridInit2 = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지급일자", field: "GIVE_DATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속코드", field: "ORGCD", sorter: "string", visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목코드", field: "ITEMCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "품목", field: "ITEMNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "품목순번", field: "ITEMSEQ", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구매일자", field: "BUY_DATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "right", title: "내용연수", field: "SERVICELIFE", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도코드", field: "USECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용용도", field: "USENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태코드", field: "ITEMCD_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "제품상태", field: "ITEMNM_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태코드", field: "OPER_STATUSCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "운용상태", field: "OPER_STATUSNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태코드", field: "HOLD_TYPECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "보유형태", field: "HOLD_TYPENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MEMO", sorter: "string" },
                { formatter: fn_CellReg, width: 220, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];
        }

        var fn_GridSet = function () {
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            if ($("#selOut").val() == 'A') grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            else {
                grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
                ktsGrid.setGroupBy(function (data) {
                    return data.EMPNO + " - " + data.EMPNM;
                });

                if ($("#selC").val() == 'A') ktsGrid.setGroupStartOpen(true);
                else ktsGrid.setGroupStartOpen(false);
            }

            fn_SetSelField($("#selOut").val());
        }

        var fn_SetSelField = function (gubun) {
            var obj1 = $('#selField');
            obj1.empty();
            obj1.append("<option value=''>선택</option>");

            if (gubun == 'A') {
                obj1.append("<option value='ORGNM'>소속</option>");
                obj1.append("<option value='EMPNM'>이름</option>");
                obj1.append("<option value='EMPNO'>사번</option>");
                obj1.append("<option value='KTSEMPNO'>KTS사번</option>");
            }
            else {
                obj1.append("<option value='ITEMNM'>품목</option>");
                obj1.append("<option value='GIVE_DATE'>지급일자</option>");
                obj1.append("<option value='ORGNM'>소속</option>");
                obj1.append("<option value='EMPNM'>이름</option>");
                obj1.append("<option value='EMPNO'>사번</option>");
                obj1.append("<option value='KTSEMPNO'>KTS사번</option>");
            }
            
        }

        var fn_Load = function () {

            fn_GridSet();
        }

        var fn_Reg = function (id) {
            fn_PopupReg(id);
        }

        var fn_Del = function (id) {
            if (confirm("정말로 삭제 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('D', id);
            }
        }

        var fn_LostRequest = function (id) {
            fn_PopupLost(id);
        }

        var fn_Transaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var giveDate = rowData.GIVE_DATE;
            var itemCd = rowData.ITEMCD;
            var itemSeq = rowData.ITEMSEQ;
            var empno = rowData.EMPNO;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = giveDate;
            aData[2] = itemCd;
            aData[3] = itemSeq;
            aData[4] = '';
            aData[5] = '';
            aData[6] = '';
            aData[7] = '';
            aData[8] = '';
            aData[9] = '';
            aData[10] = '';
            aData[11] = '';
            aData[12] = '';
            aData[13] = '';
            aData[14] = empno;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_GridItem = function () {
            //입력 파라미터
            var aData = [];
            aData[0] = '';
            aData[1] = 'PRODUCT';
            aData[2] = '';
            aData[3] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            callGb = 'gridItem';
            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'gridItem') {

                fn_GridInit();

                var data = JSON.parse(json);
                var newColumn = { title: '품목', headerHozAlign: "center", columns: [] };

                // 각 객체에서 "ITEMCD"를 키(key)로, "ITEMNM"을 값(value)으로 하는 요소를 출력
                data.forEach(function (item) {
                    newColumn.columns.push({ headerHozAlign: "center", hozAlign: "right", title: item.ITEMNM, field: item.ITEMCD, sorter: "number", formatter: fn_CellICnt, width: 120, cellClick: (e, cell) => { fn_CellPopup(e, cell, item.ITEMCD);} });
                });

                Columns.push(newColumn);

                fn_GridSet();

                callGb = '';
                fn_Search();
            }
            else if (callGb == 'delTran') {
                ktsGridAlert("삭제를 완료했습니다.");
                fn_Search2();

                callGb = '';
            }
            
        }

        var SetDispItem = function (check, cellNo, buyDate, use, useNm, status, statusNm, oper, operNm, type, typeNm, memo) {

            if (check == 'ok') {

                var row = ktsGrid.getRow(cellNo);
                row.select();

                ktsGrid.updateData([{
                    id: cellNo, BUY_DATE: buyDate, USECD: use, USENM: useNm, ITEMCD_STATUSCD: status, ITEMNM_STATUSNM: statusNm, OPER_STATUSCD: oper, OPER_STATUSNM: operNm, HOLD_TYPECD: type, HOLD_TYPENM: typeNm, MEMO: memo
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });

                callGb = '';
            }
        }

        var SetDispItem2 = function (delFlag) {

            if (delFlag) {
                fn_Search();
            }
        }

        var fn_PopupReg = function (id) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vGiveDate = rowData.GIVE_DATE;
            var vBuyDate = rowData.BUY_DATE;
            var vOrgNm = rowData.ORGNM;
            var vEmpNm = rowData.EMPNM;
            var vEmpNO = rowData.EMPNO;
            var vEmpJobNm = rowData.JOBNM;
            var vItemNm = rowData.ITEMNM;
            var vItemCd = rowData.ITEMCD;
            var vItemSeq = rowData.ITEMSEQ;
            var vUse = rowData.USECD;
            var vStatus = rowData.ITEMCD_STATUSCD;
            var vOper = rowData.OPER_STATUSCD;
            var vType = rowData.HOLD_TYPECD;
            var vMemo = rowData.MEMO;

            var form = "/Safety/SafetyUseMngPopup";
            var target = "SafetyUseMngPopup";
            var param = {
                pIId: id,
                pOrgNm: vOrgNm,
                pEmpNm: vEmpNm,
                pEmpNo: vEmpNO,
                pEmpJobNm: vEmpJobNm,
                pGiveDate: vGiveDate,
                pBuyDate: vBuyDate,
                pItemNm: vItemNm,
                pItemCd: vItemCd,
                pItemSeq: vItemSeq,
                pUse: vUse,
                pStatus: vStatus,
                pOper: vOper,
                pType: vType,
                pMemo: vMemo
            };

            var nWidth = 720;
            var nHeight = 680;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupLost = function (id) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var vGiveDate = rowData.GIVE_DATE;
            var vEmpNO = rowData.EMPNO;
            var vItemNm = rowData.ITEMNM;
            var vItemCd = rowData.ITEMCD;
            var vItemSeq = rowData.ITEMSEQ;

            var form = "/Safety/SafetyUseLoseRequestPopup";
            var target = "SafetyUseLoseRequestPopup";
            var param = {
                pEmpNo: vEmpNO,
                pGiveDate: vGiveDate,
                pItemNm: vItemNm,
                pItemCd: vItemCd,
                pItemSeq: vItemSeq
            };

            var nWidth = 1208;
            var nHeight = 680;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupDetail = function (id, itemCd) {

            var row = ktsGrid.getRow(id);
            var rowData = row.getData();
            var empNo = rowData.EMPNO;

            var form = "/Safety/SafetyUseMngHDetailPopup";
            var target = "SafetyUseMngHDetailPopup";
            var param = {
                pEMPNO: empNo,
                pITEMCD: itemCd
            };

            var nWidth = 1208;
            var nHeight = 680;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Safety";
                var target = "OrgTree_Safety";
                var param = {
                    pORGCD: orgcd,
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnBaseDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {

                if ($("#selOut").val() == 'A') fn_GridItem();
                else {
                    fn_Search2();
                }

                
                return true;
            }
        }


        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Search = function () {

            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[1] = 'SEARCH1';
            aData[2] = '';
            aData[3] = '';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        var fn_Search2 = function () {

            fn_GridInit2();
            fn_GridSet();

            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[1] = 'SEARCH2';
            aData[2] = '';
            aData[3] = '';

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <hr class="blank" />
                <span class="optionbox">
                    <label>출력 TYPE</label>
                    <select id="selOut" style="width:60px">
                        <option selected="selected" value="A">A</option>
                        <option value="B">B</option>
                    </select>
                    <span>
                        <select id="selC" style="display:none">
                            <option value="A">펼치기</option>
                            <option value="B">줄이기</option>
                        </select>
                    </span>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>안전용품 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>

