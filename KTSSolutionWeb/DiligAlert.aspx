﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiligAlert.aspx.cs" Inherits="KTSSolutionWeb.DiligAlert" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>공지사항</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">  
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-organization -->
            <div class="windowpop pop-organization">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>알림</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>

                    <!-- S:popcontents -->
                    <div class="popcontents" style="text-align:center;vertical-align:middle;font-size:18px;">
                        금일은 귀하의 휴일 또는 휴가일로<br/>
                        근로제공의무가 없는 날이 오니<br/>
                        업무처리를 삼가하시고 귀가하시기 바랍니다.<br/><br/>

                        만약 부서장의 근무지시가 있었다면<br/>
                        지금 즉시 복무시스템에 복무를<br/>
                        처리하여 주시기 바랍니다.<br/><br/>

                        휴일근무한 경우 휴일근무를 신청하고<br/>
                        휴가일인 경우 휴가 취소를 신청하시어<br/>
                        부서장 승인을 받으시기 바랍니다.                        
                        <!-- S:btncenter -->
                        <div class="btncenter">
                            <input type="button" onclick="window.close();" class="btn-gray" value="닫기" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
                
                <div style="position:absolute; left:10px; bottom:10px;">
                    <asp:UpdatePanel ID="updPanelChk" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:CheckBox ID="chkPopYn" runat="server" AutoPostBack="true" OnCheckedChanged="chkPopYn_CheckedChanged" Text="오늘 하루 자동 팝업을 띄우지 않습니다." />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="chkPopYn" EventName="CheckedChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- E:pop-organization -->
        </div>
    </form>
</body>
</html>
