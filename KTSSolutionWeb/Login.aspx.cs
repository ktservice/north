﻿using System;
using System.Reflection;
using System.Data;
using System.Web.UI;
using System.Web.SessionState;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class Login : PageBase
    {
        #region Fields
        #endregion


        #region Event

        #region Page_load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.Expires = 0;
                Response.Cache.SetNoStore();
                Response.AppendHeader("Pragma", "no-cache");

                //SSL 적용시 처리
                if (!Request.Url.Port.Equals(443))
                {
                    //Response.Redirect(Request.Url.ToString().Replace("http:", "https:"));
                }

                if (!IsPostBack)
                {
                    //세션초기화
                    Session.Abandon();

                    // aes256 으로 포탈에서 받아오는  sso  토큰 값
                    string strPortalSSOCookie = Request.QueryString["sso_token"] == null ? string.Empty : Request.QueryString["sso_token"].ToString();


                    if (strPortalSSOCookie != "")
                    {
                        hdfSSOToken.Value = strPortalSSOCookie;
                        updPanel1.Update();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ssoAuth", "ssoTokenAuth()", true);
                    }
                }
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        #region Method

        #region ValidationCheck
        /// <summary>
        /// ValidationCheck
        /// </summary>
        /// <param name="strLoginChkMsg">ref string</param>
        /// <returns></returns>
        private bool ValidationCheck(ref string strLoginChkMsg)
        {
            string strEmpNo = Request.Form[txbEmpNo.UniqueID];
            string strEmpPwd = Request.Form[txbEmpPwd.UniqueID];
            string strhdfChk1 = Request.Form[hdfChk1.UniqueID];
            string strhdfChk2 = Request.Form[hdfChk2.UniqueID];

            bool bChk = true;

            if (strEmpNo != strhdfChk1 || strEmpPwd != strhdfChk2)
            {
                bChk = false;
            }

            if (!bChk)
            {
                this.txbEmpNo.Text = "";
                this.txbEmpPwd.Text = "";
                this.hdfChk1.Value = "";
                this.hdfChk2.Value = "";

                this.txbEmpNo.Focus();

                updPanel1.Update();

                strLoginChkMsg = "비정상적인 데이터 입력으로 로그인이 거부됩니다.";
            }

            return bChk;
        }
        #endregion

        #region
        private void LoingSuccess()
        {
            try
            {
                DataSet ds = null;
                string strEmpNo = Page.Session["EMPNO"].ToString();

                using (KTSUser ktsUser = new KTSUser())
                {
                    ktsUser.InsertLoginLog(strEmpNo, Page.Request.UserHostAddress, Page.Request.UserAgent);
                    ds = ktsUser.GetDiligInfo(strEmpNo);
                }

                PageUtility pageUtil = new PageUtility(this.Page);

                string strUrl = "/Default";
                bool bDilig = false;

                if (ds != null)
                { 
                    if(ds.Tables[0].Rows.Count > 0)
                    {
                        bDilig = true;
                    }
                }

                string strPopupNm = "DiligAlert_" + strEmpNo;

                HttpCookie Cookie = Request.Cookies[strPopupNm];

                if (Cookie != null)
                {
                    if (Cookie["POPUPYN"] == "Y" && Cookie["DATE"].ToString().Equals(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        bDilig = false;
                    }
                }

                if (bDilig)
                {
                    pageUtil.PageMoveNDiligAlert(strUrl);
                }
                else
                {
                    pageUtil.PageMove(strUrl);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SessionIDManager
        /// <summary>
        /// SessionIDManager
        /// </summary>
        public void SessionIDManager()
        {
            SessionIDManager manager = new SessionIDManager();

            string OldSession = manager.GetSessionID(Context);
            string NewSession = manager.CreateSessionID(Context);

            bool isRedir = false;
            bool isAdd = false;

            manager.SaveSessionID(Context, NewSession, out isRedir, out isAdd);

            HttpApplication ctx = (HttpApplication)HttpContext.Current.ApplicationInstance;
            HttpModuleCollection mods = ctx.Modules;

            SessionStateModule ssm = (SessionStateModule)mods.Get("Session");

            FieldInfo[] fields = ssm.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            SessionStateStoreProviderBase store = null;
            FieldInfo rqidField = null;
            FieldInfo rqLockField = null;
            FieldInfo rqStateNotFountField = null;

            foreach (FieldInfo field in fields)
            {
                if (field.Name.Equals("_store")) store = (SessionStateStoreProviderBase)field.GetValue(ssm);
                if (field.Name.Equals("_rqId")) rqidField = field;
                if (field.Name.Equals("_rqLockId")) rqLockField = field;
                if (field.Name.Equals("_rqSessionStateNotFound")) rqStateNotFountField = field;
            }

            object lockId = rqLockField.GetValue(ssm);

            if ((lockId != null) && (OldSession != null)) store.ReleaseItemExclusive(Context, OldSession, lockId);
            rqStateNotFountField.SetValue(ssm, true);
            rqidField.SetValue(ssm, NewSession);
        }
        #endregion

        #endregion

        #region Event

        #region UserLogin
        /// <summary>
        /// UserLogin
        /// </summary>
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string strLoginChkMsg = "";

                SessionIDManager();

                if (ValidationCheck(ref strLoginChkMsg))
                {
                    string strEmpNo = Request.Form[txbEmpNo.UniqueID];
                    string strEmpPwd = Request.Form[txbEmpPwd.UniqueID];

                    string strSHAEmpPwd = Utility.SHA256Hash(strEmpPwd);

                    UserInfo user = new UserInfo();
                    strLoginChkMsg = user.UserLogOn(strEmpNo, strSHAEmpPwd, this.Page);

                    if (strLoginChkMsg.Equals("Success"))
                    {
                        LoingSuccess();
                    }
                    else if (strLoginChkMsg.Equals("ExfireUser"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('퇴사한 직원은 로그인 할 수 없습니다.');", true);
                    }
                    else if (strLoginChkMsg.Equals("ChangePassword"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ChangePassword", "PopupChangPWD();", true);
                    }
                    else if (strLoginChkMsg.Equals("Fail"))
                    {
                        strLoginChkMsg = user.AdminLogOn(strEmpNo, strEmpPwd, this.Page);

                        if (strLoginChkMsg.Equals("Success"))
                        {
                            LoingSuccess();
                        }
                        else if (strLoginChkMsg.Equals("ExfireUser"))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('퇴사한 직원은 로그인 할 수 없습니다.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('로그인에 실패했습니다. 사번과 패스워드를 확인해주세요.');", true);
                        }
                    }
                    else
                    {
                        strLoginChkMsg = user.AdminLogOn(strEmpNo, strEmpPwd, this.Page);

                        if (strLoginChkMsg.Equals("Success"))
                        {
                            LoingSuccess();
                        }
                        else if (strLoginChkMsg.Equals("ExfireUser"))
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('퇴사한 직원은 로그인 할 수 없습니다.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('로그인에 실패했습니다. 사번과 패스워드를 확인해주세요.');", true);
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationError", "alert('" + strLoginChkMsg + "');", true);
                }
            }
            catch (Exception ex)
            {
                this.txbEmpNo.Text = "";
                this.txbEmpPwd.Text = "";
                this.hdfChk1.Value = "";
                this.hdfChk2.Value = "";

                this.txbEmpNo.Focus();

                updPanel1.Update();

                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #endregion

        protected void btnSsoAuth_Click(object sender, EventArgs e)
        {
            try
            {
                bool bCheckAuth = false;
                string strPortalSSOCookie = hdfSSOToken.Value;

                UserInfo user = new UserInfo();

                bCheckAuth = user.CheckWebAuth(this.Page, strPortalSSOCookie);

                if (bCheckAuth)
                {
                    LoingSuccess();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}