﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search_CarId.aspx.cs" Inherits="KTSSolutionWeb.Search_CarId" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>차대번호 검색</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            var fn_Choicebutton = function (cell) {
                var rowData = cell.getRow().getData();

                var carid = rowData.CARID;

                var input = "<input id='btnCell' type='button' onclick='SendParentsForm(\"" + carid + "\");' value='선택' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "공용/개인", field: "MGMTTYPE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "운용관리상태", field: "MGMTSTATUS", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "차종", field: "CARTYPENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "차명(모델)", field: "MODELNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "임대여부", field: "POSSESSIONTYPENM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량소속", field: "CARORGNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 120, formatter: fn_Choicebutton }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            var grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("368px");

            $("#btnSelect").click(function (e) {
                var CarId = $("#<%= txbCarId.ClientID %>").val();

                //입력 파라미터
                var aData = [];
                aData[0] = "";
                aData[1] = CarId;
                aData[2] = "";

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            });
        });

        function SendParentsForm(carid) {
            var rowid = $("#<%= hfRowId.ClientID %>").val();

            opener.SetGridCarId(rowid, carid);

            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">차대번호검색</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
                                <span class="inpbox" style="padding-right:20px">
                                    <label>차대번호</label>
                                    <asp:TextBox ID="txbCarId" runat="server" MaxLength="50" Width="120px" TabIndex="1"></asp:TextBox>
                                </span>
				                <div class="btnset" style="float:right; padding-right:20px;">
                                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:HiddenField ID="hfRowId" runat="server" />
    </form>
</body>
</html>
