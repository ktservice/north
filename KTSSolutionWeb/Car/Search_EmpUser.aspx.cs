﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using KTS.KTSSolution.Framework.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class Search_EmpUser : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    this.hfCarId.Value = Request.Form["pCARID"] == null ? "" : Request.Form["pCARID"].ToString();
                    this.hfCarOrgCd.Value = Request.Form["pCARORGCD"] == null ? "" : Request.Form["pCARORGCD"].ToString();
                    this.hfRowId.Value = Request.Form["pROWID"] == null ? "" : Request.Form["pROWID"].ToString();
                    this.hfType.Value = Request.Form["pTYPE"] == null ? "" : Request.Form["pTYPE"].ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;

            string rptCd = "";
            string jobGb = "GET";

            if (aData[2].Replace("GRID", "").Equals("ALL"))
            {
                rptCd = "USERINFOBYEMPNM";
                
                aData.RemoveAt(2);
                aData.RemoveAt(1);
            }
            else
            {
                rptCd = "USERINFOBYCARORGCD";
                aData.RemoveAt(2);
            }

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}