﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegCarInfo.aspx.cs" Inherits="KTSSolutionWeb.RegCarInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>차량정보 등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />

    <script type="text/javascript">
        $(document).ready(function () {

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $("input.date").datepicker({
                    dateFormat: "yy-mm-dd",
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    showOtherMonths: false,
                    selectOtherMonths: false,
                    constrainInput: false,
                    prevText: "<",
                    nextText: ">",
                });
            }

            $(".txbCarId").change(function () {
                $("#hdfIdChk").val("");
            });

            $(".txbCarNo").change(function () {
                $("#hdfNoChk").val("");
            });
        });

        function IdCheck(pType) {
            if (IdCheck == "") {
                alert("잘못된 접근입니다.");
                return false;
            }
            else {
                if (pType == "ID" && $("#txbCarId").val() == "") {
                    alert("차대번호를 입력하세요");
                    return false;
                }
                else if (pType == "NO" && $("#txbCarNo").val() == "") {
                    alert("차량번호를 입력하세요");
                    return false;
                }
            }
        }

        function PopupOrgTree() {
            var nWidth = 400;
            var nHeight = 700;

            var form = "/Common/OrgTree_BComp";
            var target = "OrgTree_BComp";

            var param = {
                pKTSYN: "Y"
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function PopupEmpUser() {
            var carorgcd = $("#<%= hdfOrgCd.ClientID %>").val();

            if (carorgcd == "") {
                alert("차량소속 부터 선택해주세요.");
            } else {
                var nWidth = 700;
                var nHeight = 500;

                var param = {};

                var form = "/Car/Search_EmpUser";
                var target = "Search_EmpUser";

                var param = {
                    pCARORGCD: carorgcd
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        function PopupCard(cardtype) {

            var nWidth = 950;
            var nHeight = 600;

            var form;
            var target;

            var param;

            if (cardtype == "H") {

                form = "/Car/HipassCardInfoPopup";
                target = "HipassCardInfoPopup";

                param = {
                    pPARENTTYPE: "FORM"
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            } 
            else {
                var orgcd = $("#<%= hdfOrgCd.ClientID %>").val();

                if (orgcd == "") {
                    alert("주유카드/전기충전카드는 작업자 차량담당자 선택 후 변경가능합니다.")
                }
                else {
                    form = "/Car/FuelCardInfoPopup";
                    target = "FuelCardInfoPopup";

                    param = {
                        pCARDTYPE: cardtype,
                        pPARENTTYPE: "FORM"
                    };

                    cmPostPopup(form, target, param, nWidth, nHeight);
                }
            }

        }

        function SetOrgCode(orgcd) {
            $("#<%= hdfOrgCd.ClientID %>").val(orgcd);
            $("#<%=hdfEmpNo.ClientID %>").val('');
            $("#<%=hdfEmpNm.ClientID %>").val('');
            $("#<%=txbEmpUserNm.ClientID %>").val('');

            __doPostBack("<%=hdfOrgCd.ClientID %>", "");
        }

        function SetGridEmpUser(rowid, ktsempno, empno, empnm, orgnm, empage){
            this.focus();

            $("#<%=hdfEmpNo.ClientID %>").val(empno);
            $("#<%=hdfEmpNm.ClientID %>").val(empnm);
            $("#<%=txbEmpAge.ClientID %>").val(empage);

            __doPostBack("<%=hdfEmpNo.ClientID %>", "");
        }

        function SetGridCardNo(carid, cardno, cardtype) {

            if (cardtype == "F") {
                $("#txbFuel").val(cardno);
            }
            else if (cardtype == "C") {
                $("#txbCharge").val(cardno);
            }
            else if (cardtype == "H") {
                $("#txbHipass").val(cardno);
            }
        }

        function btnRegCheck() {
            var IdChk = $("#hdfIdChk").val();
            var NoChk = $("#hdfNoChk").val();

            if (IdChk != "Y") {
                alert("차대번호 확인 후 등록가능합니다.");
                return false;
            }
            else if (NoChk != "Y") {
                alert("차량번호 확인 후 등록가능합니다.");
                return false;
            }
            else {
                if (confirm("차량정보를 등록 하시겠습니까?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>기동장비정보 저장</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <asp:updatepanel id="updPanel1" runat="server" UpdateMode="Conditional" >
                                    <ContentTemplate>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th style="width:100px">차대번호&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox" style="width:205px;">
                                                            <asp:TextBox ID="txbCarId" runat="server" Width="200px" MaxLength="20" CssClass="txbCarId" TabIndex="1"></asp:TextBox>
                                                        </p>
                                                        <asp:HiddenField ID="hdfIdChk" runat="server" />
                                                        <asp:Button ID="btnCarIDChk" runat="server" OnClientClick="return IdCheck('ID');" OnClick="btnIDChk_Click" class="btn-popupgray" Text="확인" />
                                                    </td>
                                                    <th style="width:130px">차량번호&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox" style="width:205px;">
                                                            <asp:TextBox ID="txbCarNo" runat="server" Width="200px" MaxLength="20" CssClass="txbCarNo" TabIndex="2"></asp:TextBox>
                                                        </p>
                                                        <asp:HiddenField ID="hdfNoChk" runat="server" />
                                                        <asp:Button ID="btnCarNoChk" runat="server" OnClientClick="return IdCheck('NO');" OnClick="btnIDChk_Click" class="btn-popupgray" Text="확인" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">시작일&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbStDate" runat="server" Width="200px" MaxLength="20" TabIndex="3" ReadOnly="true" class="date" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">종료일&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEnDate" runat="server" Width="200px" MaxLength="20" TabIndex="4" ReadOnly="true" class="date" placeholder="YYYY-MM-DD"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">차종&nbsp;<span style="color:red;">*</span></th>
                                                    <td style="text-align:left">
                                                        <p class="optionbox">
                                                            <asp:DropDownList ID="ddlCarType" runat="server" style="width:150px;" TabIndex="5" AutoPostBack="true" OnSelectedIndexChanged="ddlCarType_SelectedIndexChanged">
                                                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">임대여부</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbPosseionType" runat="server" Width="200px" MaxLength="20"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">차명(모델)</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbModelNm" runat="server" Width="200px" MaxLength="50" TabIndex="6"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">차량가</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbCarPrice" runat="server" Width="200px" MaxLength="11" TabIndex="7" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">월임대료</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbRentalPrice" runat="server" Width="200px" MaxLength="11" TabIndex="8" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">차량소속&nbsp;<span style="color:red;">*</span></th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"/>
                                                            <asp:TextBox ID="txbTeam" runat="server" Width="500px" ReadOnly="true" TabIndex="9"></asp:TextBox>
                                                        </p>
                                                        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree();">+</button>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <th style="width:100px">차량담당자</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:HiddenField ID="hdfEmpNo" runat="server" OnValueChanged="hdfEmpNo_ValueChanged" />
                                                            <asp:HiddenField ID="hdfEmpNm" runat="server" />
                                                            <asp:TextBox ID="txbEmpUserNm" runat="server" Width="500px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true" TabIndex="10"></asp:TextBox>
                                                        </p>
                                                        <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">보험나이</th>
                                                    <td style="text-align:left">
                                                        <p class="optionbox">
                                                            <asp:DropDownList ID="ddlInsuranceAge" runat="server" Width="200px" TabIndex="11">
                                                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="만21세" Value="만21세"></asp:ListItem>
                                                                <asp:ListItem Text="만24세" Value="만24세"></asp:ListItem>
                                                                <asp:ListItem Text="만26세" Value="만26세"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </p>
                                                    </td>
                                                    <th style="width:100px">만나이</th>
                                                    <td style="text-align:left">
                                                        <p class="inpbox">
                                                            <asp:TextBox ID="txbEmpAge" runat="server" Width="200px" MaxLength="10"  TabIndex="12" ReadOnly="true"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">과태료유무&nbsp;<span style="color:red;">*</span></th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="optionbox">
                                                            <asp:DropDownList ID="ddlPenalty" runat="server" style="width:100px;" TabIndex="13">
                                                                <asp:ListItem Text="무" Value="무" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="유" Value="유"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">주유카드</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbFuel" runat="server" Width="500px" style="margin-right:10px" onclick="PopupCard('F');" ReadOnly="true" TabIndex="14"></asp:TextBox>
                                                        </p>
                                                        <button id="btnFuel" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupCard('F');">+</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">충전카드</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbCharge" runat="server" Width="500px" style="margin-right:10px" onclick="PopupCard('C');" ReadOnly="true" TabIndex="15"></asp:TextBox>
                                                        </p>
                                                        <button id="btnCharge" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupCard('C');">+</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">Hipass카드</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbHipass" runat="server" Width="500px" style="margin-right:10px" onclick="PopupCard('H');" ReadOnly="true" TabIndex="16"></asp:TextBox>
                                                        </p>
                                                        <button id="btnHipass" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupCard('H');">+</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">공지</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbNotice" runat="server" Width="500px" style="margin-right:10px" TabIndex="17"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">비고</th>
                                                    <td colspan="3" style="text-align:left">
                                                        <p class="inpbox" style="width:510px;">
                                                            <asp:TextBox ID="txbNotice2" runat="server" Width="500px" style="margin-right:10px" TabIndex="18"></asp:TextBox>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnReg" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlCarType" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="hdfEmpNo" EventName="ValueChanged" />
                                    </Triggers>
                                </asp:updatepanel>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력값입니다.
                        <div class="btncenter">
                            <asp:Button id="btnReg" runat="server" OnClientClick="return btnRegCheck()" OnClick="BtnReg_Click" class="btn-green" Text="저장" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
