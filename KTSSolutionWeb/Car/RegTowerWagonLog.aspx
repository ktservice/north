﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegTowerWagonLog.aspx.cs" Inherits="KTSSolutionWeb.RegTowerWagonLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>고소차 작업일보등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= ddlCarNo.ClientID %>").change(function (e) {
                fn_CarNoChange();
            });

            $("#btnRegLog").click(function () {
                btnRegCheck();
            });
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (callGb == 'GetCarInfo') {
                        fn_SetCarInfo(json);
                    }
                    else {
                        if (item.ERRCD == '00') {
                            if (callGb == 'InsOk') {
                                alert('고소차 작업일보 등록이 완료되었습니다.');
                                var workdate = $("#<%= txbWorkDate.ClientID %>").val();
                                var carno = $("#<%= ddlCarNo.ClientID %> option:selected").text();

                                opener.fn_SearchByPopup(workdate, carno);
                                window.close();
                            }
                        }
                        else {
                            alert(item.ERRMSG);
                        }
                    }
                });
            }

            callGb = '';
        };

        function fn_SetCarInfo(json) {
            if (JSON.parse(json).length > 0) {
                $.each(JSON.parse(json)[0], function (key, value) {
                    switch (key) {
                        case 'COMPANY':
                            $("#<%= txbCompany.ClientID %>").val(value);
                            break
                        case 'CARORGCD':
                            $("#<%= hdfCarOrgCd.ClientID %>").val(value);
                            break
                        case 'ORGLV2NM':
                            $("#<%= txbOrgLv2Nm.ClientID %>").val(value);
                            break
                        case 'ORGLV3NM':
                            $("#<%= txbOrgLv3Nm.ClientID %>").val(value);
                            break
                        case 'ORGLV4NM':
                            $("#<%= txbOrgLv4Nm.ClientID %>").val(value);
                            break
                        case 'EMPORGNM':
                            $("#<%= txbEmpUserNm.ClientID %>").val(value);
                            break
                        case 'EMPNO':
                            $("#<%= hdfEmpNo.ClientID %>").val(value);
                            break
                        default:
                            break
                    }
                });
            }
        }

        function fn_ResetCarInfo(clearChk) {
            $("#<%= hdfCarOrgCd.ClientID %>").val('');
            $("#<%= txbCompany.ClientID %>").val('');
            $("#<%= txbOrgLv2Nm.ClientID %>").val('');
            $("#<%= txbOrgLv3Nm.ClientID %>").val('');
            $("#<%= txbOrgLv4Nm.ClientID %>").val('');
            $("#<%= hdfEmpNo.ClientID %>").val('');
            $("#<%= txbEmpUserNm.ClientID %>").val('');

            if (clearChk != '' || clearChk != undefined) {
                $("#<%= hdfReqEmpNo.ClientID %>").val('');
                $("#<%= txbReqEmpUserNm.ClientID %>").val('');
                $("#<%= txbOdrNum.ClientID %>").val('');
                $("#<%= txbWorkDate.ClientID %>").val('');
                $("#<%= ddlWorkType.ClientID %>").val('');
                $("#<%= ddlSvcType.ClientID %>").val('');
                $("#<%= ddlCableType.ClientID %>").val('');
                $("#<%= ddlBldType.ClientID %>").val('');
                $("#<%= ddlPositionType.ClientID %>").val('');
                $("#<%= txbNotice.ClientID %>").val('');
            }
        }

        function fn_CarNoChange() {
            var carId = $("#<%= ddlCarNo.ClientID %> option:selected").val();

            if (carId == '') {
                fn_ResetCarInfo('');
            }
            else {
                var aData = [];
                aData[0] = carId;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarInfo');
                callGb = 'GetCarInfo';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        function PopupEmpUser(colType) {
            var nWidth = 700;
            var nHeight = 520;

            var param = {};

            var form = "/Car/Search_EmpUser";
            var target = "Search_EmpUser";

            var param = {
                pROWID: colType,
                pTYPE: 'ALL'
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetEmpUser(rowid, ktsempno, empno, empnm, orgnm, empage) {
            if (rowid == "REQ") {
                $("#<%=hdfReqEmpNo.ClientID %>").val(empno);
                $("#<%= txbReqEmpUserNm.ClientID %>").val(orgnm + ' ' + empnm);
            }
            else {
                $("#<%=hdfEmpNo.ClientID %>").val(empno);
                $("#<%= txbEmpUserNm.ClientID %>").val(orgnm + ' ' + empnm);
            }
        }

        function btnRegCheck() {
            if (confirm("고소차 작업일보를 등록하시겠습니까?")) {
                fn_RegLogInfo();
            }
        }

        function fn_RegLogInfo() {
            var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();
            var company = $("#<%= txbCompany.ClientID %>").val();
            var reqempno = $("#<%= hdfReqEmpNo.ClientID %>").val();
            var empno = $("#<%= hdfEmpNo.ClientID %>").val();
            var odrnum = $("#<%= txbOdrNum.ClientID %>").val();
            var workdate = $("#<%= txbWorkDate.ClientID %>").val();
            var worktype = $("#<%= ddlWorkType.ClientID %> option:selected").val();
            var svctype = $("#<%= ddlSvcType.ClientID %> option:selected").val();
            var cabletype = $("#<%= ddlCableType.ClientID %> option:selected").val();
            var bldtype = $("#<%= ddlBldType.ClientID %> option:selected").val();
            var positiontype = $("#<%= ddlPositionType.ClientID %> option:selected").val();
            var noice = $("#<%= txbNotice.ClientID %>").val();

            var message = '';
            var bChk = true;

            if (carid == "" || carid == undefined) {
                message = "등록할 차량을 선택해주세요.";
                bChk = false;
            }
            else if (company == "" || company == undefined) {
                message = "사업장을 입력해주세요.";
                bChk = false;
            }
            else if (reqempno == "" || reqempno == undefined) {
                message = "요청자를 입력해주세요";
                bChk = false;
            }
            else if (empno == "" || empno == undefined) {
                message = "작업자가 할당되지 않은 고소차량은 일보를 등록 할 수 없습니다.";
                bChk = false;
            }
            else if (odrnum == "" || odrnum == undefined) {
                message = "명령번호를 입력해주세요.";
                bChk = false;
            }
            else if (workdate == "" || workdate == undefined) {
                message = "작업일자를 입력해주세요";
                bChk = false;
            }


            if (bChk) {
                var aData = [];

                aData[0] = carid;
                aData[1] = company;
                aData[2] = reqempno;
                aData[3] = empno;
                aData[4] = "";
                aData[5] = odrnum;
                aData[6] = workdate;
                aData[7] = worktype;
                aData[8] = svctype;
                aData[9] = cabletype;
                aData[10] = bldtype;
                aData[11] = positiontype;
                aData[12] = noice;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'InsTowerWagonLog');
                callGb = 'InsOk';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            else {
                alert(message);
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>고소차 작업일보 등록</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th style="width:130px">차량번호&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="optionbox" style="width:205px;">
                                                    <asp:DropDownList ID="ddlCarNo" runat="server" AutoPostBack="false" TabIndex="1"></asp:DropDownList>
                                                    <asp:HiddenField ID="hdfLogNo" runat="server" />
                                                    <asp:HiddenField ID="hdfCarOrgCd" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width:130px">사업자&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox" style="width:205px;">
                                                    <asp:TextBox ID="txbCompany" runat="server" Width="200px" MaxLength="20" TabIndex="2"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">본부</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbOrgLv2Nm" runat="server" Width="200px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th style="width:100px">지사</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbOrgLv3Nm" runat="server" Width="200px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">지점</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbOrgLv4Nm" runat="server" Width="200px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">요청자&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:HiddenField ID="hdfReqEmpNo" runat="server" />
                                                    <asp:TextBox ID="txbReqEmpUserNm" runat="server" Width="500px" style="margin-right:10px" onclick="PopupEmpUser('REQ');" ReadOnly="true" TabIndex="3"></asp:TextBox>
                                                    <button id="btnReqEmpUser" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser('REQ');">+</button>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">고소차작업자&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:HiddenField ID="hdfEmpNo" runat="server" />
                                                    <asp:TextBox ID="txbEmpUserNm" runat="server" Width="500px" style="margin-right:10px" onclick="PopupEmpUser('EMP');"  ReadOnly="true"></asp:TextBox>
                                                    <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser('EMP');">+</button>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">명령번호/접속번호/오더번호&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbOdrNum" runat="server" Width="500px" MaxLength="30" TabIndex="5"></asp:TextBox><br />
                                                    <label style="font-weight:normal">개통 : 명령번호(OA-0000 등으로 시작하는 번호로 작성, 접속번호X)</label><br />
                                                    <label style="font-weight:normal">수리 : 서비스계약번호</label><br />
                                                    <label style="font-weight:normal">GigaEyes : 서비스계약ID(SAID)</label><br />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">작업일자&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbWorkDate" runat="server" Width="200px" TabIndex="6" class="date" placeholder="YYYY-MM-DD" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th style="width:100px">작업구분</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlWorkType" runat="server" style="width:200px;" TabIndex="7"></asp:DropDownList>
                                                </p>
                                            </td>   
                                        </tr>
                                        <tr>
                                            <th style="width:100px">상품</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlSvcType" runat="server" style="width:200px;" TabIndex="8"></asp:DropDownList>
                                                </p>
                                            </td>
                                            <th style="width:100px">인입선구분</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlCableType" runat="server" style="width:200px;" TabIndex="9"></asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">건물유형</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlBldType" runat="server" style="width:200px;" TabIndex="10"></asp:DropDownList>
                                                </p>
                                            </td>
                                            <th style="width:100px">작업위치</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlPositionType" runat="server" style="width:200px;" TabIndex="11"></asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">비고</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:TextBox ID="txbNotice" runat="server" Width="500px" style="margin-right:10px" TabIndex="12"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;는 필수 입력 항목입니다.
                        <div class="btncenter">
                            <input id="btnRegLog" type="button" class="btn-green last" style="float:right;" value="등록" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
