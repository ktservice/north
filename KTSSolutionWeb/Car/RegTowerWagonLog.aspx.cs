﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Oper;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class RegTowerWagonLog : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();

                    SetDropdownlist();

                    this.txbWorkDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void PageInit()
        {
            DataSet ds = null;

            try
            {
                string resultSession = string.Empty;
                string rptCd = "USERCARLIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                string empNo = HttpContext.Current.Session["EMPNO"].ToString();

                aData.Add(empNo); //파라미터 추가 시
                aData.Add("TOWERWAGON");
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                ddlCarNo.Items.Clear();
                ddlCarNo.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlCarNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CARNO"].ToString(), ds.Tables[0].Rows[i]["CARID"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetDropdownlist()
        {
            DataSet ds = null;

            try
            {
                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "TOWERWAGONLOGCODE";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    string strWhere = string.Format("CODETYPE = '{0}'", "WORKTYPE");
                    DataRow[] dr = ds.Tables[0].Select(strWhere);

                    ddlWorkType.Items.Clear();
                    ddlWorkType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlWorkType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "SVCTYPE");
                    dr = ds.Tables[0].Select(strWhere);

                    ddlSvcType.Items.Clear();
                    ddlSvcType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlSvcType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "CABLETYPE");
                    dr = ds.Tables[0].Select(strWhere);

                    ddlCableType.Items.Clear();
                    ddlCableType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlCableType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "BLDTYPE");
                    dr = ds.Tables[0].Select(strWhere);

                    ddlBldType.Items.Clear();
                    ddlBldType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlBldType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }

                    strWhere = string.Format("CODETYPE = '{0}'", "POSITIONTYPE");
                    dr = ds.Tables[0].Select(strWhere);

                    ddlPositionType.Items.Clear();
                    ddlPositionType.Items.Add(new ListItem("선택", ""));

                    foreach (DataRow row in dr)
                    {
                        ddlPositionType.Items.Add(new ListItem(row["LOGCODENM"].ToString(), row["LOGCODE"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                {
                    ds.Dispose();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOWERWAGONINFOBYCARID";
            string jobGb = "GET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsTowerWagonLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "TOWERWAGONLOGINSERT";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo);

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

    }
}