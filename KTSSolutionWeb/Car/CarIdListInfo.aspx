﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarIdListInfo.aspx.cs" Inherits="KTSSolutionWeb.CarIdListInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
    
            var fn_selectCartype = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var cartypelist = [];
                cartypelist = $("#<%= hfCarType.ClientID %>").val().split(',');

                cartypelist.forEach(function (str) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = str;
                    options.text = str;

                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);

                        var rowData = cell.getRow().getData();
                        var rowNum = rowData.id;

                        SetCarType(rowNum, selector.value);
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectAge = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var agelist = ["만21세", "만24세", "만26세", "만30세", "만35세"];

                agelist.forEach(function (str) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = str;
                    options.text = options.value = str;;
                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectMgmstatus = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var agelist = ["운행", "유휴", "해지"];

                agelist.forEach(function (str) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = str;
                    options.text = options.value = str;;
                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectMgmtype = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var agelist = ["공용", "개인"];

                agelist.forEach(function (str) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = str;
                    options.text = str;
                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectPenalty = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var penaltylist = ["무", "유"];

                penaltylist.forEach(function (str) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = str;
                    options.text = str;
                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            //Create Date Editor
            var dateEditor = function (cell, onRendered, success, cancel) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell

                //create and style input
                var cellValue = cell.getValue();
                var input = document.createElement("input");

                input.setAttribute("type", "date");

                input.style.padding = "4px";
                input.style.width = "100%";
                input.style.boxSizing = "border-box";

                input.value = cellValue;

                onRendered(function () {
                    input.focus();
                    input.style.height = "100%";
                });

                function onChange() {
                    if (input.value != cellValue) {
                        success(input.value);
                    } else {
                        cancel();
                    }
                }

                //submit new value on blur or change
                input.addEventListener("blur", onChange);

                //submit new value on enter
                input.addEventListener("keydown", function (e) {
                    if (e.keyCode == 13) {
                        onChange();
                    }

                    if (e.keyCode == 27) {
                        cancel();
                    }
                });

                return input;
            };

            var fn_empnmbutton = function (cell) {
                var cellValue = cell.getValue();
                var rowData = cell.getRow().getData();

                var carid = rowData.CARID;

                var input = "";

                if (cellValue == '' || cellValue == undefined) {
                    cellValue = "선택";
                }

                input = "<input id='btnCellEmpUser' type='button' onclick='fn_PopupEmpUser(\"" + carid + "\");' value='" + cellValue + "' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;padding:0 5px;'>";

                return input;
            };

            var fn_orgcdbutton = function (cell) {
                var cellValue = cell.getValue();
                var rowData = cell.getRow().getData();

                var carid = rowData.CARID;
                var carorgcd = rowData.CARORGCD;

                var input = "";

                if (cellValue == '' || cellValue == undefined) {
                    cellValue = "선택";
                }

                input = "<input id='btnCellOrgCd' type='button' onclick='fn_PopupCarOrgCd(\"" + carid + "\", \"" + carorgcd + "\");' value='" + cellValue + "' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;padding:0 5px;'>";

                return input;
            };

            var fn_fuelbutton = function (cell) {
                var cellValue = cell.getValue();
                var rowData = cell.getRow().getData();

                var carid = rowData.CARID;
                var mgmttype = rowData.MGMTTYPE;

                var input = "";

                if (cellValue == '' || cellValue == undefined) {
                    cellValue = "선택";
                }

                input = "<input id='btnCellFuelCard' type='button' onclick='fn_PopupCardInfo(\"" + carid + "\", \"" + mgmttype + "\", \"F\", \"GRID\");' value='" + cellValue + "' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;padding:0 5px;'>";

                return input;
            };

            var fn_chargebutton = function (cell) {
                var cellValue = cell.getValue();
                var rowData = cell.getRow().getData();

                var carid = rowData.CARID;
                var mgmttype = rowData.MGMTTYPE;

                var input = "";

                if (cellValue == '' || cellValue == undefined) {
                    cellValue = "선택";
                }

                input = "<input id='btnCellChargeCard' type='button' onclick='fn_PopupCardInfo(\"" + carid + "\", \"" + mgmttype + "\", \"C\", \"GRID\");' value='" + cellValue + "' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;padding:0 5px;'>";

                return input;
            };

            var fn_hipassbutton = function (cell) {
                var cellValue = cell.getValue();
                var rowData = cell.getRow().getData();

                var carid = rowData.CARID;

                var input = "";

                if (cellValue == '' || cellValue == undefined) {
                    cellValue = "선택";
                }

                input = "<input id='btnCellHipassCard' type='button' onclick='fn_PopupCardInfo(\"" + carid + "\", \"\", \"H\", \"GRID\");' value='" + cellValue + "' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;padding:0 5px;'>";

                return input;
            };

            var fn_regbutton = function (cell) {
                var rowData = cell.getRow().getData();

                var carid = rowData.CARID;

                var input = "<input id='btnCellSave' type='button' onclick='btnSaveCheck(\"" + carid + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDel' type='button' onclick='btnDelCheck(\"" + carid + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>"

                return input;
            };

            var fn_input = function (cell, onRendered, success, cancel) {
                var cellValue = cell.getValue();
                var input;

                input = document.createElement("input");

                input.setAttribute("type", "Text");

                input.style.padding = "4px";
                input.style.width = "100%";
                input.style.boxSizing = "border-box";
                input.setAttribute("maxlength", "11");

                input.value = cellValue;

                onRendered(function () {
                    input.focus();
                    input.style.height = "100%";
                });

                function onChange() {
                    if (input.value != cellValue) {
                        success(input.value);
                    } else {
                        cancel();
                    }
                }

                //submit new value on blur or change
                input.addEventListener("blur", onChange);

                input.addEventListener("keypress", function (e) {
                    if (isNaN(e.key)) {
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }
                    else if (e.key == " ") {
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }
                });

                return input;
            };

            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                value = priceToString(value);

                cell.getElement().style.backgroundColor = "#f1f1f1";
                cell.getElement().style.fontWeight = "500";

                return value;
            };

            var fn_CellInput = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                cell.getElement().style.backgroundColor = "#f1f1f1";
                cell.getElement().style.fontWeight = "500";

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 130, editor: "input", frozen: true, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "공용/개인", field: "MGMTTYPE", sorter: "string", width: 120, editor: fn_selectMgmtype, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "운용관리상태", field: "MGMTSTATUS", sorter: "string", width: 120, editor: fn_selectMgmstatus, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "시작일", field: "STDATE", sorter: "string", width: 140, editor: dateEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "종료일(보험종료일)", field: "ENDATE", sorter: "string", width: 140, editor: dateEditor, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "차종", field: "CARTYPENM", sorter: "string", width: 100, editor: fn_selectCartype, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "차명(모델)", field: "MODELNM", sorter: "string", width: 120, editor: "input", formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "임대여부", field: "POSSESSIONTYPENM", sorter: "string", width: 120, editor: "input", formatter: fn_CellInput  },
                { headerHozAlign: "center", hozAlign: "center", title: "차량가", field: "CARPRICE", sorter: "string", width: 140, editor: fn_input, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "월임대료", field: "RENTALPRICE", sorter: "string", width: 140, editor: fn_input, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량조직코드", field: "CARORGCD", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량소속", field: "CARORGNM", sorter: "string", minWidth: 140, formatter: fn_orgcdbutton },
                { headerHozAlign: "center", hozAlign: "center", title: "보험나이", field: "INSURANCE_AGE", sorter: "string", width: 100, editor: fn_selectAge, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "만나이", field: "EMP_AGE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", minWidth: 140, formatter: fn_empnmbutton },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "과태료유무", field: "PENALTY", sorter: "string", width: 120, editor: fn_selectPenalty, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "주유카드", field: "FUELCARD", sorter: "string", width: 240, formatter: fn_fuelbutton },
                { headerHozAlign: "center", hozAlign: "center", title: "하이패스카드", field: "HIPASSCARD", sorter: "string", width: 240, formatter: fn_hipassbutton },
                { headerHozAlign: "center", hozAlign: "center", title: "전기충전카드", field: "CHARGECARD", sorter: "string", width: 240, formatter: fn_chargebutton },
                { headerHozAlign: "center", hozAlign: "center", title: "공지", field: "NOTICE", sorter: "string", width: 240, editor: "input", formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTICE2", sorter: "string", width: 240, editor: "input", formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "center", title: "수정/삭제", field: "SAVE", width: 160, formatter: fn_regbutton}
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl1 = document.getElementById("inputValue1");
            var ValueEl2 = document.getElementById("inputValue2");
            var ValueEl3 = document.getElementById("inputValue3");

            $("#selField").change(function (e) {
                ValueEl1.value = '';
                ValueEl2.value = '';
                ValueEl3.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl1.setAttribute('style', 'display:inline;');
                    ValueEl1.setAttribute('disabled', 'disabled');

                    ValueEl2.setAttribute('style', 'display:none;');
                    ValueEl3.setAttribute('style', 'display:none;');

                    sField = 'id';
                    sValue = '';
                }
                else {
                    if (sField == "MGMTTYPE") {
                        ValueEl1.setAttribute('disabled', 'disabled');
                        ValueEl1.setAttribute('style', 'display:none;');
                        ValueEl2.setAttribute('style', 'display:inline;');
                        ValueEl3.setAttribute('style', 'display:none;');
                    }
                    else if (sField == "PENALTY") {
                        ValueEl1.setAttribute('disabled', 'disabled');
                        ValueEl1.setAttribute('style', 'display:none;');
                        ValueEl2.setAttribute('style', 'display:none;');
                        ValueEl3.setAttribute('style', 'display:inline;');

                    }
                    else if (sField == "EMPNM") {
                        ValueEl1.removeAttribute('disabled');
                        ValueEl1.setAttribute('style', 'display:inline;');
                        ValueEl2.setAttribute('style', 'display:none;');
                        ValueEl3.setAttribute('style', 'display:none;');
                    }
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue1").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl1.value);
            });

            $("#inputValue2").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl2.value);
            });

            $("#inputValue3").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl3.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridAdd").click(function (e) {
                var form = "/Car/RegCarInfo";
                var target = "RegCarInfo";

                var param = {};

                var nWidth = 940;
                var nHeight = 900;

                cmPostPopup(form, target, param, nWidth, nHeight);
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.hideColumn("SAVE");
                ktsGrid.download("xlsx", "기동장비관리.xlsx", { sheetName: "기동장비관리" });
                ktsGrid.showColumn("SAVE");
            });

            $(".table-search").css('display', 'none');
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'RegOk') {
                            ktsGridAlert('차량정보가 수정되었습니다.', 2000);
                        }
                        else if (callGb == 'DelOk') {
                            ktsGridAlert('차량정보가 삭제되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
            }
        };

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[1] = $("#<%= txbCarNo.ClientID %>").val();
            aData[2] = $("#<%= ddlMgmtStat.ClientID %> option:selected").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;

            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function fn_PopupEmpUser(carid) {
            var rows = ktsGrid.searchRows("CARID", "=", carid);

            var rowData = rows[0].getData();

            var carorgcd = rowData.CARORGCD;

            var form = "/Car/Search_EmpUser";
            var target = "Search_EmpUser";

            var param = {
                pCARID: carid,
                pCARORGCD: carorgcd
            };

            var nWidth = 1050;
            var nHeight = 515;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function fn_PopupCarOrgCd(carid, carorgcd) {

            var form = "/Car/Search_CarOrgCd";
            var target = "Search_CarOrgCd";

            var param = {
                pCARID: carid,
                pCARORGCD: carorgcd
            };

            var nWidth = 950;
            var nHeight = 515;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function fn_PopupCardInfo(carid, mgmttype, cardtype, parenttype) {

            var nWidth = 1080;
            var nHeight = 600;

            var form;
            var target;

            var param;

            if (cardtype == "H") {
                if (parenttype != "") {
                    nWidth = 805;
                    nHeight = 495;
                }

                form = "/Car/HipassCardInfoPopup";
                target = "HipassCardInfoPopup";

                param = {
                    pCARID: carid,
                    pPARENTTYPE: parenttype
                };
            }
            else {
                if (parenttype != "") {
                    nWidth = 985;
                    nHeight = 495;
                }
                form = "/Car/FuelCardInfoPopup";
                target = "FuelCardInfoPopup";

                param = {
                    pCARID: carid,
                    pCARDTYPE: cardtype,
                    pPARENTTYPE: parenttype,
                    pMGMTTYPE: mgmttype
                };
            }

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelSearch.ClientID %>", "");
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

        function DownloadTemplate() {
            window.location.href = '/Template/기동장비 Excel Upload Template.xlsx';
        }

        function btnUploadCheck() {
            if (confirm("차량정보를 일괄 등록 하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function SetCarType(rowNum, cartype) {
            if (cartype == "이륜차") {
                ktsGrid.updateData([{
                    id: rowNum, POSSESSIONTYPENM: "보험계약", RENTALPRICE: "0"
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
            else {
                ktsGrid.updateData([{
                    id: rowNum, POSSESSIONTYPENM: "리스(렌탈)"
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
        }

        function SetGridCarOrg(carid, orgcd, orglv2nm, orglv3nm, orglv4nm, carorgnm) {
            var rows = ktsGrid.searchRows("CARID", "=", carid);

            var rowData = rows[0].getData();
            var rowNum = rowData.id;

            var preCarOrgCd = rowData.CARORGCD;

            ktsGrid.updateData([{
                id: rowNum, CARORGCD: orgcd, ORGLV2NM: orglv2nm, ORGLV3NM: orglv3nm, ORGLV4NM: orglv4nm, CARORGNM: carorgnm
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

            if (preCarOrgCd != orgcd) {

                ktsGrid.updateData([{
                    id: rowNum, EMPNO: "", EMPNM: "", JOBNM: "", EMP_AGE: ""
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
        }

        function SetGridEmpUser(carid, empno, empnm, jobnm, orgnm, empage) {
            var rows = ktsGrid.searchRows("CARID", "=", carid);

            var rowData = rows[0].getData();
            var rowNum = rowData.id;

            ktsGrid.updateData([{
                id: rowNum, EMPNO: empno, EMPNM: empnm, JOBNM: jobnm, EMP_AGE: empage
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });
        }

        function SetGridCardNo(carid, cardno, cardtype) {
            var rows = ktsGrid.searchRows("CARID", "=", carid);

            var rowData = rows[0].getData();
            var rowNum = rowData.id;

            if (cardtype == "F") {
                ktsGrid.updateData([{
                    id: rowNum, FUELCARD: cardno
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
            else if (cardtype == "C") {
                ktsGrid.updateData([{
                    id: rowNum, CHARGECARD: cardno
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
            else if (cardtype == "H") {
                ktsGrid.updateData([{
                    id: rowNum, HIPASSCARD: cardno
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
        }

        function btnSaveCheck(carid) {
            if (confirm(carid + " 차량정보를 수정 하시겠습니까?")) {
                SetCarIdInfo(carid);
            }
            else {
                return false;
            }
        }

        function btnDelCheck(carid) {
            if (confirm(carid + " 차량정보를 삭제 하시겠습니까?")) {

                DelCarIdInfo(carid);
            }
            else {
                return false;
            }
        }

        function SetCarIdInfo(carid) {
            var rows = ktsGrid.searchRows("CARID", "=", carid);

            var rowData = rows[0].getData();

            if (rowData.INSURANCE_AGE == "") {
                ktsGridAlert("보험나이를 선택해주세요.", 2000);
            }
            else if (rowData.EMPNO != "" && rowData.INSURANCE_AGE > rowData.EMP_AGE) {
                ktsGridAlert("보험나이는 만나이보다 작거나 같아야 합니다.", 2000);
            }
            else {
                var pagenum = ktsGrid.getPage();

                var aData = [];

                aData[0] = rowData.CARID;
                aData[1] = rowData.CARNO;
                aData[2] = rowData.MGMTTYPE;
                aData[3] = rowData.MGMTSTATUS;
                aData[4] = rowData.STDATE;
                aData[5] = rowData.ENDATE;
                aData[6] = rowData.CARTYPENM;
                aData[7] = rowData.MODELNM;
                aData[8] = rowData.POSSESSIONTYPENM;
                aData[9] = rowData.CARPRICE;
                aData[10] = rowData.RENTALPRICE;
                aData[11] = rowData.CARORGCD;
                aData[12] = rowData.EMPNO;
                aData[13] = rowData.INSURANCE_AGE;
                aData[14] = rowData.EMP_AGE;
                aData[15] = rowData.PENALTY;
                aData[16] = rowData.FUELCARD;
                aData[17] = rowData.HIPASSCARD;
                aData[18] = rowData.CHARGECARD;
                aData[19] = rowData.NOTICE;
                aData[20] = rowData.NOTICE2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetCarInfo');

                callGb = 'RegOk';
                ajaxCallEx(jsonData, ajaxUrl, true);

                ktsGrid.setPage(pagenum);
            }
        }

        function DelCarIdInfo(carid) {
            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = carid;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'DelCarInfo');

            callGb = 'DelOk';
            ajaxCallEx(jsonData, ajaxUrl, true);

            var maxPageNum = ktsGrid.getPageMax();

            if (maxPageNum < pagenum)
            {
                ktsGrid.setPage(maxPageNum);
            }
            else
            {
                ktsGrid.setPage(pagenum);
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
                    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
				            <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '', '');">+</button>
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfCarType" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="inpbox">
					<label>차량번호</label>
                    <asp:TextBox ID="txbCarNo" runat="server" Width="120px"></asp:TextBox>
                </span> 
                <span class="optionbox">
					<label>운용관리상태</label>
                    <asp:DropDownList ID="ddlMgmtStat" runat="server" AutoPostBack="false">
                        <asp:ListItem Value="" Text="전체" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Y" Text="운행"></asp:ListItem>
                        <asp:ListItem Value="N" Text="유휴"></asp:ListItem>
                        <asp:ListItem Value="E" Text="해지"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>기동장비정보관리</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <button id="btnTemp" type="button" onclick="DownloadTemplate();" class="btn-green">양식다운로드</button>
				    </div>
                    <div class="filebox">
                        <input type="text" id="txtfu" />
                        <span class="file">
                            <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
                            <label for="fu">찾아보기</label>
                        </span>
                        <asp:Button ID="btnUpload" runat="server" OnClientClick="return btnUploadCheck();" OnClick="btnUpload_Click" class="btn-black" Text="일괄등록" />
                    </div>
				    <div class="btnset">
                        <button id="btnFuelCarInfo" type="button" onclick="fn_PopupCardInfo('', '', '', '');" class="btn-green">주유/충전카드</button>
                        <button id="btnHipassCardInfo" type="button" onclick="fn_PopupCardInfo('', '','H', '');" class="btn-green">Hipass카드</button>
                        <input id="gridAdd" type="button" class="btn-green" value="추가" />
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="MGMTTYPE">공용/개인</option>
                            <option value="PENALTY">과태료유무</option>
                            <option value="EMPNM">성명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue1" type="text" value="" disabled="disabled" />
                        <select id="inputValue2" style="display:none;" >
                            <option value="" selected="selected" >전체</option>
                            <option value="공용">공용</option>
                            <option value="개인">개인</option>
                        </select>
                        <select id="inputValue3" style="display:none;" >
                            <option value="" selected="selected" >전체</option>
                            <option value="유">유</option>
                            <option value="무">무</option>
                        </select>
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
