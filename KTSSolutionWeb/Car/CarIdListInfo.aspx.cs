﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.ExcelImport;
using OfficeOpenXml;

namespace KTSSolutionWeb
{
    public partial class CarIdListInfo : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetOrgList();

                GetCarType();

                this.hfOrgCd.Value = this.Session["ORGCD"].ToString();
                this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetCarType()
        {
            DataSet ds = null;

            try
            {
                string resultSession = string.Empty;
                string rptCd = "CARTYPELIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("");
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    string strOption = "";

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (strOption.Length > 0)
                            strOption += ",";

                        strOption += ds.Tables[0].Rows[i]["CARTYPENM"].ToString();
                    }

                    hfCarType.Value = strOption;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PageInit", "alert('오류가 발생했습니다.');", true);
                }

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARINFOLIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetCarInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARINFOLISTUPDATE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCarInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARINFOLISTDELETE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                Stream stream = Request.Files["fu"].InputStream;

                if (stream.Length > 0)
                {
                    string strtype = Request.Files["fu"].ContentType;

                    if (strtype.Equals("application/vnd.ms-excel") || strtype.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    {
                        using (ExcelImport excel = new ExcelImport())
                        {
                            using (ExcelPackage excelPackage = new ExcelPackage(stream))
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                if (worksheet.Dimension.End.Column != 18)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업로드 파일 양식이 잘못되었습니다.');", true);
                                }
                                else
                                {
                                    string strEmpNo = Session["EMPNO"].ToString();

                                    excel.ExcelUpLoad("기동장비정보", worksheet, "tb_carinfolist_temp");

                                    string result = string.Empty;
                                    string resultSession = string.Empty;
                                    string rptCd = "CARINFOLISTEXCELUPLOAD";
                                    string jobGb = "SET";

                                    List<string> aData = new List<string>();

                                    aData.Add(strEmpNo);

                                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                                    resultSession = dbTransaction.SessionCheck();

                                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                                    {
                                        result = dbTransaction.GetJsonData();
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExcel", "searchBtnClick();alert('기동장비정보가 일괄 등록 되었습니다.');", true);
                                    }
                                    else //세션이 끊겼을 경우
                                    {
                                        result = resultSession;
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExcel", "searchBtnClick();alert('기동장비정보 업로드중 오류가 발생했습니다.');", true);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('엑셀파일만 업로드가 가능합니다.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업로드 할 파일을 선택해주세요.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('엑셀 업로드 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                updPanelSearch.Update();
            }
        }
    }
}