﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using Newtonsoft.Json.Linq;

namespace KTSSolutionWeb
{
    public partial class ResvCarLogInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPageInit();
            }

            gridExcel.Attributes.Add("onclick", "ExcelDownload();");
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetPageInit()
        {
            DataSet ds = new DataSet();

            try
            {
                this.txbStDt.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");

                string authId = HttpContext.Current.Session["AUTHID"].ToString();

                if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003"))
                {
                    this.ddlProcType.Enabled = true;

                    this.ddlProcType.Items.Add(new ListItem("삭제", "DELETE"));
                    this.btnDelLog.Visible = true;
                    this.ExCarChkSpan.Visible = true;
                }
                else
                {
                    this.spanProcType.Style.Add("display", "none");

                    this.ddlProcType.Enabled = false;

                    this.btnDelLog.Visible = false;
                    this.ExCarChkSpan.Visible = false;
                }

                if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnEmpUser.Visible = true;

                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");
                    this.btnOrgCd.Attributes.Add("onClick", "PopupOrgTree('', '');");

                    this.txbEmpNm.Attributes.Add("onClick", "PopupEmpUser();");
                    this.btnEmpUser.Attributes.Add("onClick", "PopupEmpUser();");

                    //this.btnAllConf.Visible = true;
                    //this.btnAllConf.Attributes.Add("onClick", "SetLeaveConfirm_All();");

                    this.txbEmpNm.Text = "";
                    this.hdfEmpNo.Value = "";
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnEmpUser.Visible = false;

                    this.txbTeam.Attributes.Remove("onClick");
                    this.btnOrgCd.Attributes.Remove("onClick");

                    this.txbEmpNm.Attributes.Remove("onClick");
                    this.btnEmpUser.Attributes.Remove("onClick");

                    this.txbEmpNm.Text = Session["EMPNM"].ToString();
                    this.hdfEmpNo.Value = Session["EMPNO"].ToString();

                    //this.btnAllConf.Visible = false;
                    //this.btnAllConf.Attributes.Remove("onClick");
                }

                string strMonth = this.txbEnDt.Text.Replace("-", "").Substring(0, 6);

                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }

                this.hdfOrgCd.Value = Session["ORGCD"].ToString();
                this.txbTeam.Text = Utility.GetOrgNm(this.hdfOrgCd.Value, ds.Tables[0]);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void hdfLeaveCnt_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                int nCnt = 0;
                string strLeaveCnt = this.hdfLeaveCnt.Value.ToString();

                int.TryParse(strLeaveCnt, out nCnt);
                string authId = HttpContext.Current.Session["AUTHID"].ToString();

                if ((authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004")) && nCnt > 0)
                {
                    this.btnAllConf.Visible = true;
                    this.btnAllConf.Attributes.Add("onClick", "SetLeaveConfirm_All();");
                }
                else
                {
                    this.btnAllConf.Visible = false;
                    this.btnAllConf.Attributes.Remove("onClick");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private static bool ValidationChk(int nStKm, int nEnKm, int nLeaveKm, string strLeaveYn, out string strMsg)
        {
            bool bChk = false;
            strMsg = "";

            if (nEnKm == 0)
            {
                strMsg = "종료km 를 입력해주세요.";
            }
            else if (nEnKm <= nStKm)
            {
                strMsg = "종료km는 시작km 보다 커야합니다.";
            }
            else
            {
                bChk = true;
            }

            if (bChk && strLeaveYn.Equals("Y"))
            {
                if (nLeaveKm == 0)
                {
                    bChk = false;
                    strMsg = "직퇴km 를 입력해주세요.";
                }
                else if (nLeaveKm <= nEnKm)
                {
                    bChk = false;
                    strMsg = "직퇴km는 종료km 보다 커야합니다.";
                }
                else
                {
                    bChk = true;
                }
            }

            return bChk;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARRESVLISTBYEMPNO";
            string jobGb = "GET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARRESVINFOBYRESVKEY";
            string jobGb = "GET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCarChkInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARCHECKINFO2BYCARID";
            string jobGb = "GET";

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCarChk1(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARCHECKINFO1";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCarChk2(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARCHECKINFO2";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCarLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARLOGINSERT";
            string jobGb = "SET";
            
            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCarLog(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARLOGDELETE_ALL";
            string jobGb = "SET";

            bool bChk = false;

            string authId = HttpContext.Current.Session["AUTHID"].ToString();

            if (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004"))
            {
                bChk = true;
            }

            foreach (string val in aData)
            {
                if (val.Length == 0)
                {
                    bChk = false;

                    break;
                }
                else
                {
                    bChk = true;
                }
            }

            if (bChk)
            {
                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;

            string strMonth = aData[1];

            using (OrgCodeTree org = new OrgCodeTree())
            {
                ds = org.GetOperOrgMonthList("", "N", strMonth);
            }

            result = Utility.GetOrgNm(aData[0], ds.Tables[0]);

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string result2 = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARLOGINFO";
            string jobGb = "GET";

            string authid = HttpContext.Current.Session["AUTHID"].ToString();
            string sessionempno = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(authid); //파라미터 추가 시
            aData.Add(sessionempno); //파라미터 추가 시
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            DataSet dataSet = new DataSet();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                dataSet = dbTransaction.GetDataSet();

                result = Utility.DataSetToJson(dataSet);
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetCarLoginfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARLOGUPDATE";
            string jobGb = "SET";

            //aData[0] = rowData.RESVKEY;
            //aData[1] = rowData.STKM;
            //aData[2] = rowData.ENKM;
            //aData[3] = rowData.LEAVEKM;
            //aData[4] = rowData.FUELAMOUNT;
            //aData[5] = rowData.REASON;
            //aData[6] = rowData.ETC;
            //aData[7] = rowData.LEAVEYN;
            //aData[8] = rowData.MODIYN;
            //aData[9] = rowData.LOGDTTM;
            //aData[10] = rowData.LASTLOGDTTM;
            //aData[11] = rowData.EMPNO;

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            bool bChk = false;
            string strMsg = "";

            string strLeaveYn = aData[7];
            bool bModify = aData[8] == "Y" ? true : false;
            bool bDate = aData[9] == aData[10]  ? true : false;

            int stkm = 0;
            int enkm = 0;
            int leavekm = 0;
            int fuels = 0;

            int.TryParse(aData[1], out stkm);
            int.TryParse(aData[2], out enkm);
            int.TryParse(aData[3], out leavekm);
            int.TryParse(aData[4], out fuels);

            if (strLeaveYn == "Y")
            {
                bChk = ValidationChk(stkm, enkm, leavekm, strLeaveYn, out strMsg);
            }
            else if (strLeaveYn == "R")
            {
                if (aData[11] == empNo)
                {
                    bChk = ValidationChk(stkm, enkm, leavekm, "", out strMsg);
                }
            }
            else if (strLeaveYn == "C")
            {
                if (aData[11] == empNo && bDate)
                {
                    bChk = ValidationChk(stkm, enkm, leavekm, "", out strMsg);
                }
            }
            else if (bModify)
            {
                bChk = ValidationChk(stkm, enkm, leavekm, "", out strMsg);
            }
            else if (fuels  >= 1000)
            {
                bChk = false;
                strMsg = "주유(ℓ)가 잘못되었습니다. ℓ단위로 입력해주세요.";
            }

            if (bChk)
            {
                aData[4] = fuels.ToString();

                aData.RemoveAt(11);
                aData.RemoveAt(10);
                aData.RemoveAt(9);
                aData.RemoveAt(8);

                aData.Add(empNo); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                if (strMsg.Length == 0)
                {
                    strMsg = "잘못된 접근입니다.";
                }

                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", strMsg, "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetLeaveConfirm(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARLOGLEAVECONFIRM";
            string jobGb = "SET";

            bool bChk = false;

            //aData[0] = rowData.RESVKEY;
            //aData[1] = ctype;
            //aData[2] = rowData.LEAVEYN;

            bool bConfirm = aData[2] == "R" ? true : false;
            string authId = HttpContext.Current.Session["AUTHID"].ToString();

            if (bConfirm && (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003") || authId.Equals("AUTH0004")))
            {
                bChk = true;
            }

            if (bChk)
            {
                aData.RemoveAt(2);

                string empNo = HttpContext.Current.Session["EMPNO"].ToString();
                aData.Add(empNo); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCarLoginfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARLOGDELETE";
            string jobGb = "SET";

            bool bChk = false;

            //aData[0] = rowData.RESVKEY;
            //aData[1] = rowData.DELYN;
            //aData[2] = rowData.LOGDTTM;
            //aData[3] = rowData.LASTLOGDTTM;

            bool bDelete = aData[1] == "Y" ? true : false;
            bool bDate = aData[2] == aData[3] ? true : false;
            string authId = HttpContext.Current.Session["AUTHID"].ToString();

            if (bDelete && bDate && (authId.Equals("AUTH0001") || authId.Equals("AUTH0002") || authId.Equals("AUTH0003")))
            {
                bChk = true;
            }

            if (bChk)
            {
                aData.RemoveAt(3);
                aData.RemoveAt(2);
                aData.RemoveAt(1);

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetLeaveConfirm_All(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARLOGLEAVECONFIRM_ALL";
            string jobGb = "SET";

            string authid = HttpContext.Current.Session["AUTHID"].ToString();
            string sessionempno = HttpContext.Current.Session["EMPNO"].ToString();

            if (authid.Equals("AUTH0001") || authid.Equals("AUTH0002") || authid.Equals("AUTH0003") || authid.Equals("AUTH0004") || aData.Count != 6)
            {
                aData.Add(sessionempno); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    result = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    result = resultSession;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                result = Utility.DataTableToJson(dt);
            }

            return result;
        }
    }
}