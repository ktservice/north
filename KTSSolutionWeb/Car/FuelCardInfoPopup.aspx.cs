﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.ExcelImport;
using OfficeOpenXml;


namespace KTSSolutionWeb
{
    public partial class FuelCardInfoPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    this.hfCarId.Value = Request.Form["pCARID"] == null ? "" : Request.Form["pCARID"].ToString();
                    this.hfCardType.Value = Request.Form["pCARDTYPE"] == null ? "" : Request.Form["pCARDTYPE"].ToString();
                    this.hfParentType.Value = Request.Form["pPARENTTYPE"] == null ? "" : Request.Form["pPARENTTYPE"].ToString();
                    this.hfMgmtType.Value = Request.Form["pMGMTTYPE"] == null ? "" : Request.Form["pMGMTTYPE"].ToString();
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARDLISTINFO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string InsCardNo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARDINFOINSERT";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            for (int i = 0; i < aData.Count; i++)
            {
                aData[i] = aData[i] == null ? "" : aData[i];
            }

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string UpdCardInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARDINFOUPDATE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            for (int i = 0; i < aData.Count; i++)
            {
                aData[i] = aData[i] == null ? "" : aData[i];
            }

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCardNo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CARDINFODELETE";
            string jobGb = "SET";

            string empNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(empNo); //파라미터 추가 시

            for (int i = 0; i < aData.Count; i++)
            {
                aData[i] = aData[i] == null ? "" : aData[i];
            }

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                Stream stream = Request.Files["fu"].InputStream;

                if (stream.Length > 0)
                {
                    string strtype = Request.Files["fu"].ContentType;

                    if (strtype.Equals("application/vnd.ms-excel") || strtype.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    {
                        using (ExcelImport excel = new ExcelImport())
                        {
                            using (ExcelPackage excelPackage = new ExcelPackage(stream))
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                                if (worksheet.Dimension.End.Column != 6)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업로드 파일 양식이 잘못되었습니다.');", true);
                                }
                                else
                                {
                                    string strEmpNo = Session["EMPNO"].ToString();

                                    excel.ExcelUpLoad("주유/충전카드정보", worksheet, "tb_fuelcardinfo_temp");

                                    string result = string.Empty;
                                    string resultSession = string.Empty;
                                    string rptCd = "FUELCARDEXCELUPLOAD";
                                    string jobGb = "SET";

                                    List<string> aData = new List<string>();

                                    aData.Add(strEmpNo);

                                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                                    resultSession = dbTransaction.SessionCheck();

                                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                                    {
                                        result = dbTransaction.GetJsonData();
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExcel", "searchBtnClick();alert('주유카드 정보가 일괄 등록 되었습니다.');", true);
                                    }
                                    else //세션이 끊겼을 경우
                                    {
                                        result = resultSession;
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExcel", "searchBtnClick();alert('주유카드 정보 업로드중 오류가 발생했습니다.');", true);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('엑셀파일만 업로드가 가능합니다.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('업로드 할 파일을 선택해주세요.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('엑셀 업로드 중 오류가 발생했습니다.');", true);
            }
        }
    }
}