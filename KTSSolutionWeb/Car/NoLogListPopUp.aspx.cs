﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.ExcelImport;
using OfficeOpenXml;


namespace KTSSolutionWeb
{
    public partial class NoLogListPopUp : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    string strResvKey = Request.Form["pRESVKEY"] == null ? "" : Request.Form["pRESVKEY"].ToString();

                    if (strResvKey.Equals(""))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('잘못된 접근입니다.');window.close();", true);
                    }
                    else
                    {
                        this.hdfResvKey.Value = strResvKey;
                    }
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARNOLOGINFOBYRESVKEY";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}