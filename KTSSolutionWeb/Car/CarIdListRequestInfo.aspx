﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarIdListRequestInfo.aspx.cs" Inherits="KTSSolutionWeb.CarIdListRequestInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {

            var fn_confbutton = function (cell) {
                var value = cell.getValue();
                var rowData = cell.getRow().getData();

                var rowAuth = rowData.USERAUTH;
                var input = "";

                if (value == "R") {
                    if (rowAuth == "AUTH0001" || rowAuth == "AUTH0002" || rowAuth == "AUTH0003") {
                        var rowNo = rowData.id;

                        input = "<input id='btnCellSave' type='button' onclick='btnCheck(\"" + rowNo + "\", \"Y\");' value='승인' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                        input += "&nbsp;<input id='btnCellDel' type='button' onclick='btnCheck(\"" + rowNo + "\", \"R\");' value='반려' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>"
                    }
                    else {
                        input = "승인 대기 중 입니다.";
                    }
                }
                else if (value == "Y") {
                    input = "승인되었습니다.";
                }
                else if (value == "C") {
                    input = "반려되었습니다.";
                }

                return input;
            };

            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                value = priceToString(value);

                return value;
            };

            var fn_Cellcolor1 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_EMPNO;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }              

                return value;
            };

            var fn_Cellcolor2 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_INSURANCE_AGE;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var fn_Cellcolor3 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_EMP_AGE;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var fn_Cellcolor4 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_EMPNM;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var fn_Cellcolor5 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_JOBNM;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var fn_Cellcolor6 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_PENALTY;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };
            var fn_Cellcolor7 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_FUELCARD;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var fn_Cellcolor8 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_HIPASSCARD;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };
            var fn_Cellcolor9 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_CHARGECARD;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var fn_Cellcolor10 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.CHG_NOTICE;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#fffd00";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "변경ID", field: "REQUESTSEQ", sorter: "string", width: 160, visible: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "변경요청일", field: "REQUESTDT", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 130, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "공용/개인", field: "MGMTTYPE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "운용관리상태", field: "MGMTSTATUS", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "차종", field: "CARTYPENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "차명(모델)", field: "MODELNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "임대여부", field: "POSSESSIONTYPENM", sorter: "string", width: 120  },
                { headerHozAlign: "center", hozAlign: "center", title: "차량가", field: "CARPRICE", sorter: "string", width: 140, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "월임대료", field: "RENTALPRICE", sorter: "string", width: 140, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량조직코드", field: "CARORGCD", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경사번", field: "REQ_EMPNO", sorter: "string", width: 120, formatter: fn_Cellcolor1 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번변경여부", field: "CHG_EMPNO", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량소속", field: "CARORGNM", sorter: "string", minWidth: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "보험나이", field: "INSURANCE_AGE", sorter: "string", width: 100 }, 
                { headerHozAlign: "center", hozAlign: "center", title: "변경보험나이", field: "REQ_INSURANCE_AGE", sorter: "string", width: 100, formatter: fn_Cellcolor2 },
                { headerHozAlign: "center", hozAlign: "center", title: "보험나이변경여부", field: "CHG_INSURANCE_AGE", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "만나이", field: "EMP_AGE", sorter: "string", width: 100 }, 
                { headerHozAlign: "center", hozAlign: "center", title: "변경만나이", field: "REQ_EMP_AGE", sorter: "string", width: 100, formatter: fn_Cellcolor3 },
                { headerHozAlign: "center", hozAlign: "center", title: "만나이변경여부", field: "CHG_EMP_AGE", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경성명", field: "REQ_EMPNM", sorter: "string", width: 140, formatter: fn_Cellcolor4 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명변경여부", field: "CHG_EMPNM", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 }, 
                { headerHozAlign: "center", hozAlign: "center", title: "변경직무", field: "REQ_JOBNM", sorter: "string", width: 80, formatter: fn_Cellcolor5 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경직무변경여부", field: "CHG_JOBNM", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "과태료유무", field: "PENALTY", sorter: "string", width: 120 }, 
                { headerHozAlign: "center", hozAlign: "center", title: "변경과태료유무", field: "REQ_PENALTY", sorter: "string", width: 120, formatter: fn_Cellcolor6 },
                { headerHozAlign: "center", hozAlign: "center", title: "과태료유무여부", field: "CHG_PENALTY", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "주유카드", field: "FUELCARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경주유카드", field: "REQ_FUELCARD", sorter: "string", width: 240, formatter: fn_Cellcolor7 },
                { headerHozAlign: "center", hozAlign: "center", title: "주유카드변경여부", field: "CHG_FUELCARD", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "하이패스카드", field: "HIPASSCARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경하이패스카드", field: "REQ_HIPASSCARD", sorter: "string", width: 240, formatter: fn_Cellcolor8 },
                { headerHozAlign: "center", hozAlign: "center", title: "하이패스카드변경여부", field: "CHG_HIPASSCARD", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "전기충전카드", field: "CHARGECARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경전기충전카드", field: "REQ_CHARGECARD", sorter: "string", width: 240, formatter: fn_Cellcolor9 },
                { headerHozAlign: "center", hozAlign: "center", title: "전기충전카드변경여부", field: "CHG_CHARGECARD", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "공지", field: "NOTICE", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경공지", field: "REQ_NOTICE", sorter: "string", width: 240, formatter: fn_Cellcolor10 },
                { headerHozAlign: "center", hozAlign: "center", title: "공지변경여부", field: "CHG_NOTICE", sorter: "string", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "요청자사번", field: "REQEMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "요청자성명", field: "REQEMPNO", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "처리자사번", field: "CONFEMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "처리자성명", field: "CONFEMPNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "처리자일시", field: "CONFDT", sorter: "string", width: 160 },
                { headerHozAlign: "center", hozAlign: "center", title: "사용자권한", field: "USERAUTH", sorter: "string", width: 160, visible: false  },
                { headerHozAlign: "center", hozAlign: "center", title: "승인/반려", field: "REQUESTTYPE", width: 160, formatter: fn_confbutton}
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl1 = document.getElementById("inputValue1");
            var ValueEl2 = document.getElementById("inputValue2");
            var ValueEl3 = document.getElementById("inputValue3");

            $("#selField").change(function (e) {
                ValueEl1.value = '';
                ValueEl2.value = '';
                ValueEl3.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl1.setAttribute('style', 'display:inline;');
                    ValueEl1.setAttribute('disabled', 'disabled');

                    ValueEl2.setAttribute('style', 'display:none;');
                    ValueEl3.setAttribute('style', 'display:none;');

                    sField = 'id';
                    sValue = '';
                }
                else {
                    if (sField == "MGMTTYPE") {
                        ValueEl1.setAttribute('disabled', 'disabled');
                        ValueEl1.setAttribute('style', 'display:none;');
                        ValueEl2.setAttribute('style', 'display:inline;');
                        ValueEl3.setAttribute('style', 'display:none;');
                    }
                    else if (sField == "PENALTY") {
                        ValueEl1.setAttribute('disabled', 'disabled');
                        ValueEl1.setAttribute('style', 'display:none;');
                        ValueEl2.setAttribute('style', 'display:none;');
                        ValueEl3.setAttribute('style', 'display:inline;');

                    }
                    else if (sField == "EMPNM") {
                        ValueEl1.removeAttribute('disabled');
                        ValueEl1.setAttribute('style', 'display:inline;');
                        ValueEl2.setAttribute('style', 'display:none;');
                        ValueEl3.setAttribute('style', 'display:none;');
                    }
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue1").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl1.value);
            });

            $("#inputValue2").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl2.value);
            });

            $("#inputValue3").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl3.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridReqCarInfo").click(function (e) {
                fn_PopupRequest();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.hideColumn("CONF");
                ktsGrid.download("xlsx", "자량정보변경요청관리.xlsx", { sheetName: "자량정보변경요청관리" });
                ktsGrid.showColumn("CONF");
            });

            $(".table-search").css('display', 'none');
        });

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                $("#<%=txbTeam.ClientID %>").val(json);
            }
        }

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'ConfOk') {
                            ktsGridAlert('차량정보가 변경이 승인되었습니다.', 2000);
                        }
                        else if (callGb == 'ReturnOk') {
                            ktsGridAlert('차량정보가 변경이 반려되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
            }
        };

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[1] = $("#<%= txbStDt.ClientID %>").val();
            aData[2] = $("#<%= txbEnDt.ClientID %>").val();
            aData[3] = $("#<%= txbCarNo.ClientID %>").val();
            aData[4] = $("#<%= hfEmpNo.ClientID %>").val();
            aData[5] = $("#<%= ddlConfType.ClientID %> option:selected").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;

            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = $("#<%= txbEnDt.ClientID %>").val().replace("-", "").substring(0, 6);

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;

            var form = "/Common/SearchUser";
            var target = "SearchUser";

            var param = {};

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=hfEmpNo.ClientID %>").val(empno);
            $("#<%=txbEmpNm.ClientID %>").val(empnm);
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }


        function btnCheck(rowNo, btntype) {
            var row = ktsGrid.getRow(rowNo);
            var rowData = row.getData();

            var carid = rowData.CARID;
            var reqseq = rowData.REQUESTSEQ;

            var message = "";

            if (btntype == "Y") {
                message = carid + " 차량에 대한 변경요청을 승인하시겠습니까?";

            }
            else {
                message = carid + "차량에 대한 변경요청을 반려하시겠습니까?";
            }
            

            if (confirm(message)) {
                SetCarInfoConfirm(reqseq, btntype);
            }
            else {
                return false;
            }
        }

        function SetCarInfoConfirm(reqseq, btntype) {

            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = reqseq;
            aData[1] = btntype;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetCarInfoConfirm');

            if (btntype == "Y") {
                callGb = 'ConfOk';
            }
            else {
                callGb = 'ReturnOk';
            }

            ajaxCallEx(jsonData, ajaxUrl, true);

            ktsGrid.setPage(pagenum);            
        }

        function fn_PopupRequest() {

            var nWidth = 940;
            var nHeight = 900;

            var form = "/Car/RequestChangeCarInfo";
            var target = "RequestChangeCarInfo";

            var param = {};

            cmPostPopup(form, target, param, nWidth, nHeight);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
					<label>요청일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
				    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <%--<asp:Button ID="btnOrgCd1" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />--%>
                    <asp:HiddenField ID="hfOrgCd" runat="server"/>
				</span>
                <span class="inpbox" style="margin-right:20px">
					<label>이름</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
				    <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <%--<asp:Button ID="btnEmpUser1" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />--%>
                    <asp:HiddenField ID="hfEmpNo" runat="server" />
                </span>
                <span class="inpbox">
					<label>차량번호</label>
                    <asp:TextBox ID="txbCarNo" runat="server" Width="120px"></asp:TextBox>
                </span> 
                <span class="optionbox">
					<label>요청상태</label>
                    <asp:DropDownList ID="ddlConfType" runat="server" Width="100px" AutoPostBack="false">
                        <asp:ListItem Value="" Text="전체" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="R" Text="요청"></asp:ListItem>
                        <asp:ListItem Value="Y" Text="승인"></asp:ListItem>
                        <asp:ListItem Value="C" Text="반려"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>차량정보 변경요청관리</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridReqCarInfo" type="button" class="btn-green" value="차량정보수정" />
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="MGMTTYPE">공용/개인</option>
                            <option value="PENALTY">과태료유무</option>
                            <option value="EMPNM">성명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue1" type="text" value="" disabled="disabled" />
                        <select id="inputValue2" style="display:none;" >
                            <option value="" selected="selected" >전체</option>
                            <option value="공용">공용</option>
                            <option value="개인">개인</option>
                        </select>
                        <select id="inputValue3" style="display:none;" >
                            <option value="" selected="selected" >전체</option>
                            <option value="유">유</option>
                            <option value="무">무</option>
                        </select>
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
