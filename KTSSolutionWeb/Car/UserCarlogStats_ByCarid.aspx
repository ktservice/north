﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserCarlogStats_ByCarid.aspx.cs" Inherits="KTSSolutionWeb.UserCarlogStats_ByCarid" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                value = priceToString(value);

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "공용/개인", field: "MGMTTYPE", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "운용관리상태", field: "MGMTSTATUS", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "차종", field: "CARTYPENM", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "center", title: "차명(모델)", field: "MODELNM", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "운행km", field: "WORKDISTANCE", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "최초km", field: "STKM", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "최종km", field: "RESULTKM", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "주유량(ℓ)", field: "FUELAMOUNT", sorter: "number", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "최초운행일자", field: "F_LOGDATE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "최종운행일자", field: "L_LOGDATE", sorter: "string", width: 120 },
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                ValueEl.value = '';
                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');

                    sField = 'id';
                    sValue = '';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "기동장비 운행현황.xlsx", { sheetName: "기동장비 운행현황" });
            });

            $(".table-search").css('display', 'none');
        });

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                $("#<%=txbTeam.ClientID %>").val(json);
            }
            callGb = '';
        }

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStDt.ClientID %>").val();
            aData[1] = $("#<%= txbEnDt.ClientID %>").val();
            aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[3] = $("#<%= ddlMgmtType.ClientID %> option:selected").val();
            aData[4] = $("#<%= ddlMgmtStat.ClientID %> option:selected").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;

            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = $("#<%= txbEnDt.ClientID %>").val().replace("-", "").substring(0, 6);

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }

        var ajaxCallBack = function (json) {
            callGb = '';
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
					<label>운행일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfOrgCd" runat="server"/>
				</span>
                <span class="optionbox">
					<label>공용/개인</label>
                    <asp:DropDownList ID="ddlMgmtType" runat="server" AutoPostBack="false">
                        <asp:ListItem Value="" Text="전체" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="개인" Text="개인"></asp:ListItem>
                        <asp:ListItem Value="공용" Text="공용"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="optionbox">
					<label>운용관리상태</label>
                    <asp:DropDownList ID="ddlMgmtStat" runat="server" AutoPostBack="false">
                        <asp:ListItem Value="" Text="전체" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="운행" Text="운행"></asp:ListItem>
                        <asp:ListItem Value="유휴" Text="유휴"></asp:ListItem>
                        <asp:ListItem Value="해지" Text="해지"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>운행현황</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="CARNO">차량번호</option>
                            <option value="EMPNO">사번</option>
                            <option value="EMPNM">성명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
