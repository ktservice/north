﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CardInfoHistPopup.aspx.cs" Inherits="KTSSolutionWeb.CardInfoHistPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>카드정보수정 이력 조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var Columns1;
        var Columns2;

        var page = {
            init: function () {
                fn_GridInit();
            },
            eventbind: function () {
                $("#btnExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", "카드정보수정 이력 조회.xlsx", { sheetName: "카드정보수정이력" });
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                var FieldEl = document.getElementById("selField");
                var ValueEl1 = document.getElementById("inputValue1");
                var ValueEl2 = document.getElementById("inputValue2");

                $("#selField").change(function (e) {
                    ValueEl1.value = '';

                    var sField = FieldEl.value;

                    if (sField == "") {
                        ValueEl1.setAttribute('style', 'display:inline;');
                        ValueEl1.setAttribute('disabled', 'disabled');

                        ValueEl2.setAttribute('style', 'display:none;');

                        sField = 'id';
                    }
                    else {
                        if (sField == "HISTTYPE") {
                            ValueEl1.setAttribute('disabled', 'disabled');
                            ValueEl1.setAttribute('style', 'display:none;');
                            ValueEl2.setAttribute('style', 'display:inline;');
                        }
                        else {
                            ValueEl1.removeAttribute('disabled');
                            ValueEl1.setAttribute('style', 'display:inline;');
                            ValueEl2.setAttribute('style', 'display:none;');
                        }
                    }

                    ktsGrid.setFilter(sField, 'like', '');
                });

                $("#inputValue1").keyup(function (e) {
                    ktsGrid.setFilter(FieldEl.value, 'like', ValueEl1.value);
                });

                $("#inputValue2").change(function (e) {
                    ktsGrid.setFilter(FieldEl.value, 'like', ValueEl2.value);
                });


                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "카드 수정내역.xlsx", { sheetName: "카드 수정내역" });
                });
            }
        };

        var fn_GridInit = function () {

            Columns1 = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업일자", field: "UPDATEDAY", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업구분", field: "HISTTYPE", sorter: "string", width: 100},
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "CARDTYPE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "카드번호", field: "CARDNO", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "카드명", field: "CARDNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사용자명", field: "EMPNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자사번", field: "HISTEMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자명", field: "HISTEMPNM", sorter: "string", width: 120 }
            ];

            Columns2 = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업일자", field: "UPDATEDAY", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업구분", field: "HISTTYPE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "CARDTYPE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "카드번호", field: "CARDNO", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "카드명", field: "CARDNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자사번", field: "HISTEMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업자명", field: "HISTEMPNM", sorter: "string", width: 120 }
            ];

            //컬럼 세팅
            ktsColumns = Columns1;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("61vh");


        }

        var fn_Search = function () {

            var cardtype = $("#<%= ddlCardType.ClientID %> option:selected").val();

            if (cardtype == undefined || cardtype == "") {
                alert("카드구분을 선택해주세요");
                return false;
            }
            else {
                //컬럼 세팅
                if (cardtype == "H") {
                    ktsColumns = Columns2;
                }
                else {
                    ktsColumns = Columns1;
                }

                //그리드 초기 세팅
                grid = ktsTabulator;
                grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
                ktsGrid.setHeight("61vh");

                //입력 파라미터
                var aData = [];

                aData[0] = cardtype;
                aData[1] = $("#<%= txbStDt.ClientID %>").val();
                aData[2] = $("#<%= txbEnDt.ClientID %>").val();

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">카드정보수정 이력 조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                    <!-- S:searchbox -->
                    <div class="searchboxPopUp">
		                <fieldset>
                            <span class="inpbox first">
                                <label>작업일자</label>
                                <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                                <em>~</em>
                                <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                            </span>
                            <span class="optionbox">
                                <label>카드구분</label>
                                <asp:DropDownList ID="ddlCardType" runat="server">
                                    <asp:ListItem Text="선택하세요" Value ="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="주유카드" Value ="F"></asp:ListItem>
                                    <asp:ListItem Text="하이패스카드" Value ="H"></asp:ListItem>
                                    <asp:ListItem Text="전기충전카드" Value ="C"></asp:ListItem>
                                </asp:DropDownList>
                            </span>
                            <input id="btnSelect" type="button" class="btn-green" style="float:right;" value="조회" />
                        </fieldset>
                    </div>
                    <!-- E:searchbox -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
				                <div class="btnset">
                                    <input id="gridExcel" type="button" class="btn-green last" style="float:right;" value="엑셀" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search">
                                <span>
                                    <select id="selField" style="font-weight:bold">
                                        <option value="" selected="selected" >선택</option>
                                        <option value="HISTTYPE">작업구분</option>
                                        <option value="CARDNO">카드번호</option>
                                        <option value="CARDNM">카드명</option>
                                    </select>
                                    <label> : </label>
                                    <input id="inputValue1" type="text" value="" disabled="disabled" />
                                    <select id="inputValue2" style="display:none;" >
                                        <option value="" selected="selected" >전체</option>
                                        <option value="수정">수정</option>
                                        <option value="삭제">삭제</option>
                                    </select>
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
