﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarLogTexRequest.aspx.cs" Inherits="KTSSolutionWeb.CarLogTexRequest" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            /*----검색 조건 ------- */
            $("#btnSelect").click(function (e) {
                var years = $("#<%= ddlReqYears.ClientID %> option:selected").val();
                clearInterval(Timer);

                if (years == '') {
                    alert('연도를 선택해주세요.');
                }
                else {
                    fn_Search();
                    startTimer();
                }
            });

            $("#btnRequest").click(function (e) {
                var years = $("#<%= ddlReqYears.ClientID %> option:selected").val();

                if (years == '') {
                    alert('연도를 선택해주세요.');
                }
                else {
                    btnReqCheck(years);
                }
            });

            $("#btnDelete").click(function (e) {
                var years = $("#<%= ddlReqYears.ClientID %> option:selected").val();

                if (years == '') {
                    alert('연도를 선택해주세요.');
                }
                else {
                    btnDelCheck(years);
                }
            });

            $("#btnDownLoad").click(function (e) {
                var years = $("#<%= ddlReqYears.ClientID %> option:selected").val();

                if (years == '') {
                    alert('연도를 선택해주세요.');
                }
                else {
                    window.location.href = '/TexExcelData/' + years + '년도 국세청제출용 자료.zip';
                }
            });

            $("#<%= ddlReqYears.ClientID %>").change(function (e) {
                startTimer();
                fn_Search();
            });
        });

        var successCallBack = function (json) {
            if (callGb == 'GetStatus') {
                if (JSON.parse(json) == '' || JSON.parse(json) == undefined) {
                    $("#txbMessage").val('자료요청 내역이 없습니다.');
                    $("#btnRequest").val('자료요청');
                    $("#btnRequest").show();
                    $("#btnDownLoad").hide();
                    $("#btnDelete").hide();
                }
                else {
                    if (JSON.parse(json).length > 0) {
                        $.each(JSON.parse(json)[0], function (key, value) {
                            switch (key) {
                                case 'CREATEMESSAGE':
                                    $("#txbMessage").val(value);
                                    break
                                case 'CREATESTATUS':
                                    if (value == 'Y') {
                                        $("#btnRequest").val('자료요청');
                                        $("#btnRequest").hide();
                                        $("#btnDownLoad").hide();
                                        $("#btnDelete").show();
                                    }
                                    else if (value == 'S') {
                                        $("#btnRequest").val('자료요청');
                                        $("#btnRequest").hide();
                                        $("#btnDownLoad").hide();
                                        $("#btnDelete").hide();
                                    }
                                    else if (value == 'E') {
                                        $("#btnRequest").val('재요청');
                                        $("#btnRequest").show();
                                        $("#btnDownLoad").show();
                                        $("#btnDelete").show();
                                    }
                                    else if (value == 'F') {
                                        $("#btnRequest").val('재요청');
                                        $("#btnRequest").show();
                                        $("#btnDownLoad").hide();
                                        $("#btnDelete").show();
                                    }
                                    break
                                default:
                                    break
                            }
                        });
                    }
                }

                callGb = '';
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'ReqOk') {
                            alert('선택한 연도의 국세청자료 작성을 요청했습니다.');
                            callGb = '';
                        }
                        else if (callGb == 'DelOk') {
                            alert('선택한 연도의 국세청자료를 삭제 했습니다.');
                            callGb = '';
                        }

                        fn_Search();
                        startTimer();
                    }
                    else {
                        alert(item.ERRMSG);
                    }
                });
            }
        };

        var Timer;

        function startTimer() {
            var years = $("#<%= ddlReqYears.ClientID %> option:selected").val();
            clearInterval(Timer);
            if (years != "") {
                Timer = setInterval(function () {
                    fn_Search();
                }, 10000);
            }
            else {
                $("#btnRequest").val('자료요청');
                $("#btnRequest").hide();
                $("#btnDownLoad").hide();
                $("#btnDelete").hide();

                clearInterval(Timer);
            }
        }

        function fn_Search() {
            var years = $("#<%= ddlReqYears.ClientID %> option:selected").val();

            if (years == '') {
                $("#txbMessage").val('');
                clearInterval(Timer);
            }
            else {
                //입력 파라미터
                var aData = [];
                aData[0] = years;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetStatus');
                callGb = 'GetStatus';

                ajaxCall(jsonData, ajaxUrl);
            }
        }

        function btnReqCheck(years) {
            if (confirm(years + "년도 운행정보 국세청제출용 자료요청 하시겠습니까?")) {
                ReqTexData(years);
            }
            else {
                return false;
            }
        }

        function btnDelCheck(years) {
            if (confirm(years + " 년도 운행정보 국세청제출용 자료를 삭제 하시겠습니까? 이미 작성 진행 중인 경우 삭제가 안될 수 있습니다.")) {
                DelTexData(years);
            }
            else {
                return false;
            }
        }

        function ReqTexData(years) {
            var aData = [];

            aData[0] = years;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'ReqTexData');

            callGb = 'ReqOk';
            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function DelTexData(years) {
            var aData = [];

            aData[0] = years;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'DelTexData');

            callGb = 'DelOk';
            ajaxCallEx(jsonData, ajaxUrl, true);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:page-upload-management -->
        <div class="page-upload-management">
            <div class="list-top">
                <strong>기동장비 운행정보 국세청자료요청</strong>
            </div>
            <div class="upload-list">
                <ul>
                    <li style="text-align:center;">
			            <label style="padding-left:40px;font-weight:bold;">연도&nbsp;</label>
                        <div class="optionbox">
                            <asp:DropDownList ID="ddlReqYears" runat="server" AutoPostBack="false"></asp:DropDownList>
                        </div>
			            <label style="padding-left:40px;font-weight:bold;">작성현황&nbsp;</label>
                        <div class="inpbox" style="margin-right:40px">
                            <input id="txbMessage" type="text" style="width:500px" />
                        </div>
                        <input id="btnSelect" type="button" class="btn-green" value="조회" />
                        <input id="btnRequest" type="button" class="btn-green" style="display:none;" value="자료요청" />
                        <input id="btnDownLoad" type="button" class="btn-green" style="display:none;" value="다운로드" />
                        <input id="btnDelete" type="button" class="btn-black" style="display:none;" value="자료삭제" />
                    </li>
                </ul>
            </div>
		</div>
		<!-- E:page-upload-management -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
