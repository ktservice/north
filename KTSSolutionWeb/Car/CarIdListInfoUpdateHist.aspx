﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarIdListInfoUpdateHist.aspx.cs" Inherits="KTSSolutionWeb.CarIdListInfoUpdateHist" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;

        $(document).ready(function () {

            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                value = priceToString(value);

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일시", field: "HISTDT", sorter: "string", width: 200, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 130, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "공용/개인", field: "MGMTTYPE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "운용관리상태", field: "MGMTSTATUS", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "시작일", field: "STDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "종료일(보험종료일)", field: "ENDATE", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "차종", field: "CARTYPENM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "차명(모델)", field: "MODELNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "임대여부", field: "POSSESSIONTYPENM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량가", field: "CARPRICE", sorter: "number", width: 140, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "월임대료", field: "RENTALPRICE", sorter: "number", width: 140, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량소속", field: "CARORGNM", sorter: "string", minWidth: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "보험나이", field: "INSURANCE_AGE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "만나이", field: "EMP_AGE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성명", field: "EMPNM", sorter: "string", minWidth: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "과태료유무", field: "PENALTY", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "주유카드", field: "FUELCARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "하이패스카드", field: "HIPASSCARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "전기충전카드", field: "CHARGECARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "공지", field: "NOTICE", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTICE2", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경작업자사번", field: "HISTEMPNO", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경작업자명", field: "HISTEMPNM", sorter: "string", width: 140 }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl1 = document.getElementById("inputValue1");
            var ValueEl2 = document.getElementById("inputValue2");
            var ValueEl3 = document.getElementById("inputValue3");

            $("#selField").change(function (e) {
                ValueEl1.value = '';
                ValueEl2.value = '';
                ValueEl3.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl1.setAttribute('style', 'display:inline;');
                    ValueEl1.setAttribute('disabled', 'disabled');

                    ValueEl2.setAttribute('style', 'display:none;');
                    ValueEl3.setAttribute('style', 'display:none;');

                    sField = 'id';
                    sValue = '';
                }
                else {
                    if (sField == "MGMTTYPE") {
                        ValueEl1.setAttribute('disabled', 'disabled');
                        ValueEl1.setAttribute('style', 'display:none;');
                        ValueEl2.setAttribute('style', 'display:inline;');
                        ValueEl3.setAttribute('style', 'display:none;');
                    }
                    else if (sField == "PENALTY") {
                        ValueEl1.setAttribute('disabled', 'disabled');
                        ValueEl1.setAttribute('style', 'display:none;');
                        ValueEl2.setAttribute('style', 'display:none;');
                        ValueEl3.setAttribute('style', 'display:inline;');

                    }
                    else if (sField == "EMPNM") {
                        ValueEl1.removeAttribute('disabled');
                        ValueEl1.setAttribute('style', 'display:inline;');
                        ValueEl2.setAttribute('style', 'display:none;');
                        ValueEl3.setAttribute('style', 'display:none;');
                    }
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue1").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl1.value);
            });

            $("#inputValue2").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl2.value);
            });

            $("#inputValue3").change(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl3.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "기동장비 수정내역.xlsx", { sheetName: "기동장비 수정내역" });
            });

            $(".table-search").css('display', 'none');
        });

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[1] = $("#<%= txbStDt.ClientID %>").val();
            aData[2] = $("#<%= txbEnDt.ClientID %>").val();
            aData[3] = $("#<%= txbCarNo.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function PopupOrgTree(orgcd, empno) {

            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("만료일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelSearch.ClientID %>", "");
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

        function fn_PopupCardInfoHist() {

            var nWidth = 1080;
            var nHeight = 760;

            var form = "/Car/CardInfoHistPopup";
            var target = "CardInfoHistPopup";

            var param;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
					<label>변경일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ></asp:TextBox>
                </span> 
                <span class="inpbox">
                    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
				            <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '', '');">+</button>
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="inpbox">
					<label>차량번호</label>
                    <asp:TextBox ID="txbCarNo" runat="server" Width="120px"></asp:TextBox>
                </span> 
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>기동장비 수정내역 조회</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <button id="btnFuelCarInfo" type="button" onclick="fn_PopupCardInfoHist();" class="btn-green">카드변경이력</button>
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="MGMTTYPE">공용/개인</option>
                            <option value="PENALTY">과태료유무</option>
                            <option value="EMPNM">성명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue1" type="text" value="" disabled="disabled" />
                        <select id="inputValue2" style="display:none;" >
                            <option value="" selected="selected" >전체</option>
                            <option value="공용">공용</option>
                            <option value="개인">개인</option>
                        </select>
                        <select id="inputValue3" style="display:none;" >
                            <option value="" selected="selected" >전체</option>
                            <option value="Y">Y</option>
                            <option value="N">N</option>
                        </select>
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
