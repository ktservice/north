﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CardUseInfo.aspx.cs" Inherits="KTSSolutionWeb.CardUseInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                value = priceToString(value);

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "승인일자", field: "APPROVEDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "승인시간", field: "APPROVETIME", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "승인번호", field: "APPROVENO", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "카드번호", field: "CARDNO", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "카드명", field: "CARDNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "승인금액", field: "AMOUNT", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "공급가액", field: "SUPPLYVALUE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "부가세", field: "SURTAX", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "가맹점명", field: "STORENAME", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", sorter: "string", width: 220 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "center", title: "주유량", field: "FUELAMOUNT", sorter: "string", width: 140, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "이전주유량", field: "PREFUELAMOUNT", sorter: "string", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "이동거리", field: "DRIVE", sorter: "string", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "연비(㎞/ℓ)", field: "MILEAGE", sorter: "string", width: 100 }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                ValueEl1.value = '';
                ValueEl2.value = '';
                ValueEl3.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');

                    sField = 'id';
                    sValue = '';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            /*----검색 조건 ------- */

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.hideColumn("CONF");
                ktsGrid.download("xlsx", "카드사용내역.xlsx", { sheetName: "카드사용내역" });
                ktsGrid.showColumn("CONF");
            });

            $(".table-search").css('display', 'none');
        });

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                $("#<%=txbTeam.ClientID %>").val(json);
            }
        }

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStDt.ClientID %>").val();
            aData[1] = $("#<%= txbEnDt.ClientID %>").val();
            aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[3] = $("#<%= hfEmpNo.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function PopupOrgTree(orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;

            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = $("#<%= txbEnDt.ClientID %>").val().replace("-", "").substring(0, 6);

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }

        var ajaxCallBack = function (json) {
            callGb = '';
        };

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;

            var form = "/Common/SearchUser";
            var target = "SearchUser";

            var param = {};

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=hfEmpNo.ClientID %>").val(empno);
            $("#<%=txbEmpNm.ClientID %>").val(empnm);
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
					<label>요청일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfOrgCd" runat="server"/>
				</span>
                <span class="inpbox" style="margin-right:20px">
					<label>이름</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
                    <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfEmpNo" runat="server" />
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>카드사용내역</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="APPROVENO">승인번호</option>
                            <option value="CARDNO">카드번호</option>
                            <option value="CARDNM">카드명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
