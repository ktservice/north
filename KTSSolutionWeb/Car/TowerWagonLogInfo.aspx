﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TowerWagonLogInfo.aspx.cs" Inherits="KTSSolutionWeb.TowerWagonLogInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {

            var fn_selectWorktype = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.id = "selWorktype";
                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var worktypelist = $("#<%= hdfWorkType.ClientID %>").val();

                $.each(JSON.parse(worktypelist)[0], function (key, value) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = key;
                    options.text = value;

                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);

                        var rowData = cell.getRow().getData();
                        var rowNum = rowData.id;
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectSvctype = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var svctypelist = $("#<%= hdfSvcType.ClientID %>").val();

                $.each(JSON.parse(svctypelist)[0], function (key, value) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = key;
                    options.text = value;

                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);

                        var rowData = cell.getRow().getData();
                        var rowNum = rowData.id;
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectCabletype = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var cabletypelist = $("#<%= hdfCableType.ClientID %>").val();

                $.each(JSON.parse(cabletypelist)[0], function (key, value) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = key;
                    options.text = value;

                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);

                        var rowData = cell.getRow().getData();
                        var rowNum = rowData.id;
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectBldtype = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var bldtypelist = $("#<%= hdfBldType.ClientID %>").val();

                $.each(JSON.parse(bldtypelist)[0], function (key, value) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = key;
                    options.text = value;

                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);

                        var rowData = cell.getRow().getData();
                        var rowNum = rowData.id;
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_selectPositiontype = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                //create and style editor
                var selector = document.createElement("select");

                selector.style.padding = "3px";
                selector.style.width = "100%";
                selector.style.boxSizing = "border-box";

                var positiontypelist = $("#<%= hdfPositionType.ClientID %>").val();

                $.each(JSON.parse(positiontypelist)[0], function (key, value) {
                    var options = document.createElement("option");

                    options.className = "options";
                    options.value = key;
                    options.text = value;

                    selector.append(options);
                });

                //Set value of editor to the current value of the cell
                selector.value = cell.getValue();

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    selector.focus();
                    selector.style.height = "100%";
                });

                //when the value has been set, trigger the cell to update
                function onChange() {
                    if (selector.value != cellValue) {
                        success(selector.value);

                        var rowData = cell.getRow().getData();
                        var rowNum = rowData.id;
                    } else {
                        cancel();
                    }
                }

                selector.addEventListener("blur", onChange);
                selector.addEventListener("change", onChange);

                //return the editor element
                return selector;
            };

            var fn_CellColor = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                cell.getElement().style.backgroundColor = "#FCE4D6";

                return value;
            };

            var fn_empnmbutton = function (cell) {
                var cellValue = cell.getValue();
                var rowData = cell.getRow().getData();
                
                var rowNo = rowData.id;

                var columnNm = cell.getColumn().getField();

                var input = "";

                if (cellValue == '' || cellValue == undefined) {
                    cellValue = "선택";
                }

                input = "<input id='btnCellEmpUser' type='button' onclick='fn_PopupEmpUser(\"" + rowNo + "\", \"" + columnNm + "\");' value='" + cellValue + "' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;padding:0 5px;'>";

                return input;
            };

            var fn_buttons = function (cell) {
                var value = cell.getValue();
                var rowData = cell.getRow().getData();

                var rowNo = rowData.id;

                var input = "<input id='btnCellSave' type='button' onclick='fn_SaveCheck(\"" + rowNo + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDel' type='button' onclick='fn_DelCheck(\"" + rowNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>"

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", columns: [
                    { headerHozAlign: "center", hozAlign: "center", title: "사업자", field: "COMPANY", sorter: "string", width: 80 },
                    { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGLV2NM", sorter: "string", width: 100 },
                    { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGLV3NM", sorter: "string", width: 160 },
                    { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGLV4NM", sorter: "string", width: 140 },
                    { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 120 },
                    { headerHozAlign: "center", hozAlign: "center", title: "요청자사번", field: "REQEMPNO", sorter: "string", width: 120, formatter: fn_CellColor },
                    { headerHozAlign: "center", hozAlign: "center", title: "요청자명", field: "REQEMPNM", sorter: "string", width: 140, formatter: fn_empnmbutton  },
                    { headerHozAlign: "center", hozAlign: "center", title: "고소차작업자사번", field: "EMPNO", sorter: "string", width: 140, formatter: fn_CellColor},
                    { headerHozAlign: "center", hozAlign: "center", title: "교육이수현황", field: "TRANING", sorter: "string", width: 200, formatter: fn_CellColor },
                    { headerHozAlign: "center", hozAlign: "center", title: "고소차작업자명", field: "EMPNM", sorter: "string", width: 140, formatter: fn_empnmbutton }
                ]},
                { headerHozAlign: "center", hozAlign: "center", title: "작업정보", columns: [
                    { headerHozAlign: "center", hozAlign: "center", title: "작업일", field: "WORKDATE", sorter: "string", width: 120 },
                    { headerHozAlign: "center", hozAlign: "center", title: "작업번호", field: "LOGNO", sorter: "string", width: 120, visible: false },
                    { headerHozAlign: "center", hozAlign: "center", title: "작업구분", field: "WORKTYPE", sorter: "string", width: 100, editor: fn_selectWorktype },
                    { headerHozAlign: "center", hozAlign: "center", title: "상품", field: "SVCTYPE", sorter: "string", width: 100, editor: fn_selectSvctype },
                    { headerHozAlign: "center", hozAlign: "center", title: "명령번호/접속번호/오더번호", field: "ODRNUM", sorter: "string", width: 180, editor: "input", formatter: fn_CellColor },
                    { headerHozAlign: "center", hozAlign: "center", title: "인입선구분", field: "CABLETYPE", sorter: "string", width: 120, editor: fn_selectCabletype },
                    { headerHozAlign: "center", hozAlign: "center", title: "건물유형", field: "BLDTYPE", sorter: "string", width: 150, editor: fn_selectBldtype },
                    { headerHozAlign: "center", hozAlign: "center", title: "작업위치", field: "POSITIONTYPE", sorter: "string", width: 220, editor: fn_selectPositiontype },
                    { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTICE", sorter: "string", width: 240, editor: "input" }
                ]},
                { headerHozAlign: "center", hozAlign: "center", title: "저장", field: "SAVE", width: 160, formatter: fn_buttons }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                ValueEl.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');
                    sField = 'id';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });
            
            $("#btnSelect").click(function (e) {
                fn_Search();
            }); 
            
            $("#gridRegLog").click(function (e) {
                fn_PopupSetLog();
            });


            $("#gridExcel").click(function (e) {
                ktsGrid.hideColumn("SAVE");
                ktsGrid.download("xlsx", "고소작업차 운행일보.xlsx", { sheetName: "고소작업차 운행일보" });
                ktsGrid.showColumn("SAVE");
            });

            $(".table-search").css('display', 'none');
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'RegOk') {
                            ktsGridAlert('작업일보가 수정되었습니다.', 2000);
                        }
                        else if (callGb == 'DelOk') {
                            ktsGridAlert('작업일보가 삭제되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
            }

            callGb = '';
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                $("#<%=txbTeam.ClientID %>").val(json);
            }
        }

        function fn_SearchByPopup(workdate, carno) {
            $("#<%= txbStDt.ClientID %>").val(workdate);
            $("#<%= txbEnDt.ClientID %>").val(workdate);

            $("#<%= txbCarNo.ClientID %>").val(carno);

            fn_Search();
        }

        function fn_Search() {
            $("#<%= hdfColumnNm.ClientID %>").val('');
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStDt.ClientID %>").val();
            aData[1] = $("#<%= txbEnDt.ClientID %>").val();
            aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[3] = $("#<%= hfEmpNo.ClientID %>").val();
            aData[4] = $("#<%= txbCarNo.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

        function fn_SaveCheck(rowNo) {
            if (confirm("작업일보를 수정 하시겠습니까?")) {
                SetWagonLoginfo(rowNo);
            }
            else {
                return false;
            }
        }

        function fn_DelCheck(rowNo) {
            if (confirm("작업일보를 삭제 하시겠습니까?")) {
                DelWagonLogInfo(rowNo);
            }
            else {
                return false;
            }
        }

        function SetWagonLoginfo(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            if (rowData.REQEMPNO == "" || rowData.REQEMPNO == null || rowData.REQEMPNO == undefined || rowData.REQEMPNO == "undefined") {
                ktsGridAlert('요청자를 선택해주세요.', 2000);
            }
            else if (rowData.EMPNO == "" || rowData.EMPNO == null || rowData.EMPNO == undefined || rowData.EMPNO == "undefined") {
                ktsGridAlert('작업자를 선택해주세요.', 2000);

            }
            else {
                var pagenum = ktsGrid.getPage();

                var aData = [];

                aData[0] = rowData.WORKDATE;
                aData[1] = rowData.CARID;
                aData[2] = rowData.LOGNO;
                aData[3] = rowData.TRANING;
                aData[4] = rowData.WORKTYPE;
                aData[5] = rowData.SVCTYPE;
                aData[6] = rowData.ODRNUM;
                aData[7] = rowData.CABLETYPE;
                aData[8] = rowData.BLDTYPE;
                aData[9] = rowData.POSITIONTYPE;
                aData[10] = rowData.NOTICE;
                aData[11] = rowData.REQEMPNO;
                aData[12] = rowData.EMPNO;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTowerWagonLog');

                callGb = 'RegOk';
                ajaxCallEx(jsonData, ajaxUrl, true);

                ktsGrid.setPage(pagenum);
            }
        }

        function DelWagonLogInfo(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = rowData.WORKDATE;
            aData[1] = rowData.CARID;
            aData[2] = rowData.LOGNO;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'DelTowerWagonLog');

            callGb = 'DelOk';
            ajaxCallEx(jsonData, ajaxUrl, true);

            var maxPageNum = ktsGrid.getPageMax();

            if (maxPageNum < pagenum)
            {
                ktsGrid.setPage(maxPageNum);
            }
            else
            {
                ktsGrid.setPage(pagenum);
            }
        }

        function PopupOrgTree(orgcd, empno) {

            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("작업일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: BaseMonth,
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = $("#<%= txbEnDt.ClientID %>").val().replace("-", "").substring(0, 6);

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }

        function fn_PopupEmpUser(id, columnNm) {
            var form = "/Car/Search_EmpUser";
            var target = "Search_EmpUser";

            var param = {
                pROWID: id,
                pTYPE: "ALL"
            };

            var nWidth = 1050;
            var nHeight = 515;

            $("#<%= hdfColumnNm.ClientID %>").val(columnNm);

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetEmpUser(rowNo, ktsempno, empno, empnm, orgnm, empage) {

            var columnNm = $("#<%= hdfColumnNm.ClientID %>").val();

            if (columnNm == "REQEMPNM") {
                ktsGrid.updateData([{
                    id: rowNo, REQEMPNO: empno, REQEMPNM: empnm
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
            else if (columnNm == "EMPNM") {
                ktsGrid.updateData([{
                    id: rowNo, EMPNO: empno, EMPNM: empnm
                }])
                .then(function () {
                    //run code after row
                })
                .catch(function (error) {
                    //handle error
                });
            }
            $("#<%= hdfColumnNm.ClientID %>").val('');
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;

            var form = "/Common/SearchUser";
            var target = "SearchUser";

            var param = {};

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=hfEmpNo.ClientID %>").val(empno);
            $("#<%=txbEmpNm.ClientID %>").val(empnm);
        }

        function fn_PopupSetLog() {

            var nWidth = 940;
            var nHeight = 800;

            var form = "/Car/RegTowerWagonLog";
            var target = "RegTowerWagonLog";

            var param = {};

            cmPostPopup(form, target, param, nWidth, nHeight);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox first" style="margin-right:20px">
					<label>작업일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfOrgCd" runat="server"/>
				</span>
                <span class="inpbox" style="margin-right:20px">
					<label>작업자명</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
                    <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfEmpNo" runat="server" />
                </span>
                <span class="inpbox">
					<label>차량번호</label>
                    <asp:TextBox ID="txbCarNo" runat="server" Width="100px"></asp:TextBox>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>고소작업차 작업일보</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridRegLog" type="button" class="btn-green" value="작업일보등록" />
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="REQEMPNO">요청자사번</option>
                            <option value="REQEMPNM">요청자명</option>
                            <option value="EMPNO">고소차작업자사번</option>
                            <option value="EMPNM">고소차작업자명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
                <asp:HiddenField ID="hdfWorkType" runat="server" />
                <asp:HiddenField ID="hdfSvcType" runat="server" />
                <asp:HiddenField ID="hdfCableType" runat="server" />
                <asp:HiddenField ID="hdfBldType" runat="server" />
                <asp:HiddenField ID="hdfPositionType" runat="server" />         
                <asp:HiddenField ID="hdfColumnNm" runat="server" />       
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
