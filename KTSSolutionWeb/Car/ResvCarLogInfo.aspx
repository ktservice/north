﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResvCarLogInfo.aspx.cs" Inherits="KTSSolutionWeb.ResvCarLogInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }

        .divChkBackground {
            width: 36px;
            height: 20px;
            background: rgb(242 242 242);
            border-width: 2px;
            border-radius: 10px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
            float: right;
        }

        .chkOn {
            width: 18px;
            height: 18px;
            background: rgb(15 255 28);
            border-radius: 9px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
            margin-left: 16px;
        }

        .chkOff {
            width: 18px;
            height: 18px;
            background: rgb(252 31 31);
            border-radius: 9px;
            border: 1px solid transparent;
            border-color: rgb(155 155 155);
            margin-left: 0px;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();

            $(function () {
                $("#txbLogDt").datepicker({
                    dateFormat: "yy-mm-dd",
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    showOtherMonths: false,
                    selectOtherMonths: false,
                    constrainInput: false,
                    prevText: "<",
                    nextText: ">",
                    onSelect: function (dateText) {
                        fn_LogDtChange();
                    }
                });
            });
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                /*----검색 조건 ------- */
                $("#<%= ddlProcType.ClientID %>").change(function (e) {
                    $("#txbLogDt").val('');
                    $("#btnNoLogList").hide();

                    fn_InitCarList();
                    fn_ResetCarInfo();

                    var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();

                    if (proctype == "SAVE") {
                        $("#spanLogTime").show();
                        $("#spanFuels").show();
                        $("#spanStkm").show();
                        $("#spanEnkm").show();
                        $("#spanReqLeave").show();
                        $("#spanCarChk").show();

                        $("#btnRegLog").show();
                        $("#<%= btnDelLog.ClientID %>").hide();
                    }
                    else {
                        $("#spanLogTime").hide();
                        $("#spanFuels").hide();
                        $("#spanStkm").hide();
                        $("#spanEnkm").hide();
                        $("#spanReqLeave").hide();
                        $("#spanCarChk").hide();

                        $("#btnRegLog").hide();
                        $("#<%= btnDelLog.ClientID %>").show();
                    }
                });

                $("#ddlCarList").change(function (e) {
                    fn_CarNoChange();
                });

                $("#<%= chkReqLeave.ClientID %>").change(function (e) {
                    $("#<%= ddlReason.ClientID %> option:selected").val('');

                    if ($("#<%= chkReqLeave.ClientID %>").is(":checked")) {
                        $("#spanReason").show();
                    }
                    else {
                        $("#spanReason").hide();
                    }
                });

                $("#btnNoLogList").click(function (e) {
                    fn_PopupNoLogList();
                });

                $("#btnRegLog").click(function (e) {
                    fn_RegLogCheck();
                });

                $("#btnRegChk1").click(function (e) {
                    fn_RegCarChk('');
                });

                $("#btnRegChk2").click(function (e) {
                    fn_RegCarChk('TOWERWAGON');
                });

                $("#btnCloseChk1").click(function (e) {
                    $("#divRegLog").show();
                    $("#divCarChk1").hide();
                    $("#divCarChk2").hide();
                });

                $("#btnCloseChk2").click(function (e) {
                    $("#divRegLog").show();
                    $("#divCarChk1").hide();
                    $("#divCarChk2").hide();
                });

                $("#<%= btnDelLog.ClientID %>").click(function (e) {
                    fn_DelLogCheck();
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });
            }
        };

        var fn_GridInit = function () {

            var fn_selectReason = function (cell, onRendered, success, cancel, editorParams) {
                //cell - the cell component for the editable cell
                //onRendered - function to call when the editor has been rendered
                //success - function to call to pass thesuccessfully updated value to Tabulator
                //cancel - function to call to abort the edit and return to a normal cell
                //editorParams - params object passed into the editorParams column definition property

                var cellValue = cell.getValue();
                var rowData = cell.getRow().getData();
                var rowLogDttm = rowData.LOGDTTM;
                var rowLastLogDttm = rowData.LASTLOGDTTM;
                var rowEmpno = rowData.EMPNO;
                var rowSEmpno = rowData.SEMPNO;
                var rowLeave = rowData.LEAVEYN;
                //var rowModify = rowData.MODIYN;

                var input = cellValue;

                var bSave = false;

                if (rowLeave == "Y") {
                    bSave = true;
                }
                else if (rowLeave == "R") {
                    if (rowEmpno == rowSEmpno) {
                        bSave = true;
                    }
                }
                else if (rowLeave == "C") {
                    if (rowEmpno == rowSEmpno && rowLogDttm == rowLastLogDttm) {
                        bSave = true;
                    }
                }

                if (bSave) {

                    //create and style editor
                    var selector = document.createElement("select");

                    selector.style.padding = "3px";
                    selector.style.width = "100%";
                    selector.style.boxSizing = "border-box";
                    selector.style.backgroundColor = "#f1f1f1";
                    selector.style.fontWeight = "500";

                    //var penaltylist = ["거주지 원거리", "작업지역 원거리", "주52시간 근로", "주차상면 부족", "코로나 집합 금지", "긴급출동 대기"];
                    var penaltylist = ["거주지 원거리", "작업지역 원거리", "주52시간 근로", "주차상면 부족", "긴급출동 대기"];

                    penaltylist.forEach(function (str) {
                        var options = document.createElement("option");

                        options.className = "options";
                        options.value = str;
                        options.text = options.value = str;;
                        selector.append(options);
                    });

                    //Set value of editor to the current value of the cell
                    selector.value = cell.getValue();

                    //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                    onRendered(function () {
                        selector.focus();
                        selector.style.height = "100%";
                    });

                    //when the value has been set, trigger the cell to update
                    function onChange() {
                        if (selector.value != cellValue) {
                            success(selector.value);
                        } else {
                            cancel();
                        }
                    }

                    selector.addEventListener("blur", onChange);
                    selector.addEventListener("change", onChange);
                    //return the editor element
                    return selector;
                }
                else {
                    //return the editor element
                    return input;
                }
            };

            var fn_buttons = function (cell) {
                var rowData = cell.getRow().getData();

                var rowNo = rowData.id;
                var rowCarNo = rowData.CARNO;
                var rowLogDttm = rowData.LOGDTTM;
                var rowLastLogDttm = rowData.LASTLOGDTTM;
                var rowAuth = rowData.AUTHID;
                var rowEmpno = rowData.EMPNO;
                var rowSEmpno = rowData.SEMPNO;
                var rowLeave = rowData.LEAVEYN;
                var rowModify = rowData.MODIYN;
                var rowDel = rowData.DELYN;

                var input = "";

                var bSave = false;
                var bConfirm = false;
                var bDelete = false;

                if (rowLeave == "Y") {
                    bSave = true;
                }
                else if (rowLeave == "R") {
                    if (rowEmpno == rowSEmpno) {
                        bSave = true;
                    }

                    if (rowAuth == "AUTH0001" || rowAuth == "AUTH0002" || rowAuth == "AUTH0003" || rowAuth == "AUTH0004") {
                        bConfirm = true;
                    }
                }
                else if (rowLeave == "C") {
                    if (rowEmpno == rowSEmpno && rowLogDttm == rowLastLogDttm) {
                        bSave = true;
                    }
                }
                else if (rowModify == "Y") {
                    bSave = true;
                }

                if (rowDel == "Y") {
                    bDelete = true;
                }

                if (bSave) {
                    input += "<input id='btnCellSave' type='button' onclick='fn_SaveCheck(\"" + rowNo + "\", \"" + rowCarNo + "\", \"" + rowLogDttm + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                }

                if (bConfirm) {
                    if (input.length > 0)
                        input += "&nbsp;";

                    input += "<input id='btnCellConfirm' type='button' onclick='fn_ConfirmCheck(\"" + rowNo + "\", \"" + rowCarNo + "\", \"" + rowLogDttm + "\");' value='승인' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                    input += "&nbsp;<input id='btnCellReturn' type='button' onclick='fn_ReturnCheck(\"" + rowNo + "\", \"" + rowCarNo + "\", \"" + rowLogDttm + "\");' value='반려' class='btn-del' style='height:23px;line-height:0px;font-size:15px'>";
                }

                if (bDelete) {
                    if (input.length > 0)
                        input += "&nbsp;";

                    input += "<input id='btnCellDel' type='button' onclick='fn_DelCheck(\"" + rowNo + "\", \"" + rowCarNo + "\", \"" + rowLogDttm + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";
                }

                return input;
            };

            var fn_bEdit = function (cell, cellType) {
                var bEdit = false;

                var rowData = cell.getRow().getData();

                var rowLogDttm = rowData.LOGDTTM;
                var rowLastLogDttm = rowData.LASTLOGDTTM;
                var rowEmpno = rowData.EMPNO;
                var rowSEmpno = rowData.SEMPNO;
                var rowLeave = rowData.LEAVEYN;
                var rowModify = rowData.MODIYN;

                if (cellType == "1") {
                    if (rowLeave == "Y") {
                        bEdit = true;
                    }
                    else if (rowLeave == "R") {
                        if (rowEmpno == rowSEmpno) {
                            bEdit = true;
                        }
                    }
                    else if (rowLeave == "C") {
                        if (rowEmpno == rowSEmpno && rowLogDttm == rowLastLogDttm) {
                            bEdit = true;
                        }
                    }
                    else if (rowModify == "Y") {
                        bEdit = true;
                    }
                }
                else {
                    if (rowLeave == "Y") {
                        bEdit = true;
                    }
                    else if (rowModify == "Y") {
                        if (rowLeave == "E") {
                            bEdit = true;
                        }
                    }
                }

                return bEdit;
            };

            var fn_input1 = function (cell, onRendered, success, cancel) {
                var bEdit = fn_bEdit(cell, "1");

                var cellValue = cell.getValue();
                var input;

                if (bEdit) {
                    input = document.createElement("input");

                    input.setAttribute("type", "Text");

                    input.style.padding = "4px";
                    input.style.width = "100%";
                    input.style.boxSizing = "border-box";
                    input.setAttribute("maxlength", "11");

                    input.value = cellValue;

                    onRendered(function () {
                        input.focus();
                        input.style.height = "100%";
                    });

                    function onChange() {
                        if (input.value != cellValue) {
                            success(input.value);
                        } else {
                            cancel();
                        }
                    }

                    //submit new value on blur or change
                    input.addEventListener("blur", onChange);

                    input.addEventListener("keypress", function (e) {
                        if (isNaN(e.key)) {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        }
                        else if (e.key == " ") {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        }
                    });
                }
                else {
                    input = cellValue;
                }

                return input;
            };

            var fn_input2 = function (cell, onRendered, success, cancel) {
                var bEdit = fn_bEdit(cell, "2");

                var cellValue = cell.getValue();

                var input;

                if (bEdit) {
                    input = document.createElement("input");

                    input.setAttribute("type", "Text");

                    input.style.padding = "4px";
                    input.style.width = "100%";
                    input.style.boxSizing = "border-box";
                    input.setAttribute("maxlength", "11");

                    input.value = cellValue;

                    onRendered(function () {
                        input.focus();
                        input.style.height = "100%";
                    });

                    function onChange() {
                        if (input.value != cellValue) {
                            success(input.value);
                        } else {
                            cancel();
                        }
                    }

                    //submit new value on blur or change
                    input.addEventListener("blur", onChange);

                    input.addEventListener("keypress", function (e) {
                        if (isNaN(e.key)) {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        }
                        else if (e.key == " ") {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        }
                    });
                }
                else {
                    input = cellValue;
                }

                return input;
            };

            var fn_input3 = function (cell, onRendered, success, cancel) {
                var bEdit = fn_bEdit(cell, "1");

                var cellValue = cell.getValue();
                var input;

                if (bEdit) {
                    input = document.createElement("input");

                    input.setAttribute("type", "Text");

                    input.style.padding = "4px";
                    input.style.width = "100%";
                    input.style.boxSizing = "border-box";
                    input.setAttribute("maxlength", "1500");

                    input.value = cellValue;

                    onRendered(function () {
                        input.focus();
                        input.style.height = "100%";
                    });

                    function onChange() {
                        if (input.value != cellValue) {
                            success(input.value);
                        } else {
                            cancel();
                        }
                    }

                    //submit new value on blur or change
                    input.addEventListener("blur", onChange);
                }
                else {
                    input = cellValue;
                }

                return input;
            };

            var fn_CellColor1 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                var bEdit = fn_bEdit(cell, "1");

                if (bEdit) {
                    cell.getElement().style.backgroundColor = "#f1f1f1";
                    cell.getElement().style.fontWeight = "500";
                }

                value = priceToString(value);

                return value;
            };

            var fn_CellColor2 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                var bEdit = fn_bEdit(cell, "2");

                if (bEdit) {
                    cell.getElement().style.backgroundColor = "#f1f1f1";
                    cell.getElement().style.fontWeight = "500";
                }

                value = priceToString(value);

                return value;
            };

            var fn_CellColor3 = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                var bEdit = fn_bEdit(cell, "1");

                if (bEdit) {
                    cell.getElement().style.backgroundColor = "#f1f1f1";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var fn_CellNumber = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();

                value = priceToString(value);

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "운행일자", field: "LOGDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "운행시간", field: "LOGTIME", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", sorter: "string", width: 250, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "시작km", field: "STKM", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "종료km", field: "ENKM", sorter: "number", width: 100, editor: fn_input1, formatter: fn_CellColor1 },
                { headerHozAlign: "center", hozAlign: "center", title: "운행거리", field: "WORKDISTANCE", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴결과", field: "LEAVEKM", sorter: "number", width: 100, editor: fn_input2, formatter: fn_CellColor2 },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴거리", field: "LEAVEDISTANCE", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "최종결과", field: "RESULTKM", sorter: "number", width: 100, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "주유(ℓ)", field: "FUELAMOUNT", sorter: "number", width: 100, editor: fn_input1, formatter: fn_CellColor1 },
                //{ headerHozAlign: "center", hozAlign: "center", title: "전기충전(㎾/h)", field: "FUELAMOUNT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "DELYN", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "LEAVEYN", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "MODIYN", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "AUTHID", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "SEMPNO", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "LOGDTTM", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "LASTLOGDTTM", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "STTIME", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "ENTIME", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "RESVKEY", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "ORGLEVEL", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "", field: "ORGORDER", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴사유", field: "REASON", sorter: "string", width: 150, editor: fn_selectReason, formatter: fn_CellColor3 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "ETC", sorter: "string", width: 200, editor: fn_input3, formatter: fn_CellColor3 },
                { headerHozAlign: "center", hozAlign: "center", title: "저장", field: "SAVE", width: 300, formatter: fn_buttons },
                { headerHozAlign: "center", hozAlign: "center", title: "직퇴요청상태", field: "LEAVESTAT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "주유카드", field: "FUELCARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "하이패스카드", field: "HIPASSCARD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "전기충전카드", field: "CHARGECARD", sorter: "string", width: 240 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //20221102 엑셀SIZE 관련 추가
            ktsExcelSizeYN = "Y";
            ktsExcelHeaders = {
                id: "No.", LOGDATE: "운행일자", LOGTIME: "운행시간", ORGFULLNM: "소속", EMPNO: "사번", KTSEMPNO: "KTS사번", EMPNM: "이름", CARID: "차대번호", CARNO: "차량번호"
                , STKM: "시작KM", ENKM: "종료KM", WORKDISTANCE: "운행거리", LEAVEKM: "직퇴결과", LEAVEDISTANCE: "직퇴거리", RESULTKM: "최종결과", FUELAMOUNT: "주유(ℓ)", REASON: "직퇴사유"
                , SAVE: "저장", LEAVESTAT: "직퇴요청상태", FUELCARD: "주유카드", HIPASSCARD: "하이패스카드", CHARGECARD: "전기충전카드"
            };


            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("40vh");
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                ValueEl.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');
                    sField = 'id';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });
        };

        var fn_Load = function () {
            $("#<%= btnDelLog.ClientID %>").hide();

            $(".table-search").css('display', 'none');
        };

        var ajaxCallEx = function (param, url, gridAlert) {
            $.ajax({
                type: 'post',
                url: url,
                data: param,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    var json = data.d;
                    ajaxCallSuccess(json, gridAlert);
                },
                error: function (request, status, error) {
                    ajaxFail(request, status, error, gridAlert);
                }
            })
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                if (callGb == 'GetCarInfo') {
                    fn_SetCarInfo(json);
                }
                else if (callGb == 'GetCarChkInfo') {
                    fn_GetCarChkInfo(json);
                }
                else {
                    $.each(JSON.parse(json), function (idx, item) {
                        if (item.ERRCD == '00') {
                            if (callGb == 'InsOk') {
                                ktsGridAlert('운행일지가 등록 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'ConfirmOk') {
                                ktsGridAlert('직퇴가 승인 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'ReturnOk') {
                                ktsGridAlert('직퇴가 반려 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'LeaveOk') {
                                ktsGridAlert('직퇴 정보가 등록되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'RegOk') {
                                ktsGridAlert('운행일지가 수정되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'DelOk') {
                                ktsGridAlert('운행일지가 삭제되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'InsChkOk') {
                                ktsGridAlert('검차내용이 저장되었습니다.', 2000);
                                //fn_Search();
                            }
                            else if (callGb == 'ConfAllOk') {
                                ktsGridAlert('직퇴 일괄승인이 완료되었습니다.', 2000);
                                fn_Search();
                            }
                        }
                        else {
                            alert(item.ERRMSG);
                        }
                    });
                }

                callGb = '';
            }
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                //ktsGrid.setData(json);
                //ktsGridRowCount(ktsGrid.getDataCount());
                //ktsGridSearch();

                ktsGrid.setData(JSON.parse(json)["Table"]);
                ktsGridRowCount(ktsGrid.getDataCount());
                ktsGridSearch();

                $("#<%= hdfLeaveCnt.ClientID %>").val(JSON.parse(json)["Table1"][0].CNT);
                __doPostBack("<%= hdfLeaveCnt.ClientID %>", "");
            }
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                callGb = '';

                $("#<%=txbTeam.ClientID %>").val(json);
            }
            else if (callGb == 'GetCarList') {
                callGb = '';

                fn_ControlBind_CarList(json);
            }
        };

        var fn_SaveCheck = function (rowNo, rowCarNo, rowLogDttm) {
            if (confirm(rowCarNo + " 차량 " + rowLogDttm + " 운행정보를 수정 하시겠습니까?")) {
                SetCarLoginfo(rowNo);
            }
            else {
                return false;
            }
        };

        var fn_ConfirmCheck = function (rowNo, rowCarNo, rowLogDttm) {
            if (confirm(rowCarNo + " 차량 " + rowLogDttm + " 직퇴요청을 승인 하시겠습니까?")) {
                SetLeaveConfirm(rowNo, "Y");
            }
            else {
                return false;
            }
        };

        var fn_ReturnCheck = function (rowNo, rowCarNo, rowLogDttm) {
            if (confirm(rowCarNo + " 차량 " + rowLogDttm + " 직퇴요청을 반려 하시겠습니까?")) {
                SetLeaveConfirm(rowNo, "C");
            }
            else {
                return false;
            }
        };

        var fn_DelCheck = function (rowNo, rowCarNo, rowLogDttm) {
            if (confirm(rowCarNo + " 차량 " + rowLogDttm + " 운행정보를 삭제 하시겠습니까?")) {
                DelCarLoginfo(rowNo);
            }
            else {
                return false;
            }
        };

        var fn_LogDtChange = function () {
            var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();
            var logDt = $("#txbLogDt").val();

            $("#txbLogTime").val('');
            fn_ResetCarInfo();

            if (logDt != '') {

                var aData = [];

                //차량목록 조회
                aData[0] = logDt;
                aData[1] = proctype;

                callGb = 'GetCarList';

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarList');

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_CarNoChange = function () {
            var resvKey = $("#ddlCarList option:selected").val();
            var logChk = String($("#ddlCarList option:selected").css('background-color'));

            if (resvKey == '') {
                fn_ResetCarInfo();
            }
            else if (logChk != 'rgba(0, 0, 0, 0)') {
                alert('선택 차량의 이전 운행일지가 등록되지 않았습니다.');
                fn_ResetCarInfo();
                $("#btnNoLogList").show();
            }
            else {
                $("#btnNoLogList").hide();
                var aData = [];
                aData[0] = resvKey;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarInfo');

                callGb = 'GetCarInfo';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        };

        var fn_GetCarChkInfo = function (json) {
            var carcheck = "";

            if (JSON.parse(json).length > 0) {
                carcheck = JSON.parse(json)[0]["CARCHK"];
                $("#lblCarChk").text(carcheck);

                if (carcheck == "검차완료") {
                    $("#lblCarChk").css("color", 'rgb(38 220 28)');
                }
                else {
                    $("#lblCarChk").css("color", 'rgb(224 31 31)');
                }
            }
        };

        var fn_RegLogCheck = function () {
            //검차체크기능 추가 필요

            var resvKey = $("#ddlCarList option:selected").val();
            var logChk = String($("#ddlCarList option:selected").css('background-color'));

            var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();
            var logdt = $("#txbLogDt").val();
            var sttime = $("#hdfStTime").val();
            var logdttm = logdt + " " + sttime;
            var logchk = $("#hdfLogChk").val();
            var stkm = $("#txbStkm").val();
            var enkm = $("#txbEnkm").val();
            var fuels = $("#txbFuels").val();
            var leaveyn = 'N';
            var carcheck = $("#lblCarChk").text();
            var cartype = $("#hdfCarType").val();
            var reason = $("#<%= ddlReason.ClientID %> option:selected").val();
            var lastleavechk = $("#hdfLeaveYn").val();
            var lastlogdttm = $("#hdfLastLogDttm").val();
            var etc = $("#txbEtc").val();

            if ($("#<%= chkReqLeave.ClientID %>").is(":checked")) {
                leaveyn = 'R';
            }

            var chk = true;
            var message = '';

            if (proctype == "SAVE") {
                if (logdt.length == 0) {
                    chk = false;
                    message = '운행일자를 선택하세요.';
                }
                else if (resvKey.length == 0) {
                    chk = false;
                    message = '차량을 선택하세요';
                }
                else if (logChk != 'rgba(0, 0, 0, 0)') {
                    chk = false;
                    message = '선택 차량의 이전 운행일지가 등록되지 않았습니다.';
                }
                else if (lastleavechk == "Y" || lastleavechk == "R") {
                    chk = false;
                    message = '해당 차량은 직퇴승인 대기중이거나 직퇴결과가 입력되지 않았습니다.';
                }
                else if (logchk == 'Y') {
                    chk = false;
                    message = '해당 차량은 운행일시가 같은 운행정보가 이미 등록 되어있습니다.';
                }
                else if (lastlogdttm != "" && lastlogdttm > logdttm) {
                    chk = false;
                    message = '마지막 운행일지 등록일시보다 이전 등록일시는 등록할 수 없습니다.';
                }
                else if (enkm.length == 0) {
                    chk = false;
                    message = '종료km 를 입력해주세요.';
                }
                else if (parseInt(stkm) >= parseInt(enkm)) {
                    chk = false;
                    message = '종료km는 시작km 보다 커야 합니다.';
                }
                else if ((parseInt(enkm) - parseInt(stkm)) > 500) {
                    chk = false;
                    message = '주행거리가 많이 입력되었습니다. 종료km를 확인해주세요';
                }
                else if (fuels != '' && parseInt(fuels) >= 999) {
                    chk = false;
                    message = '주유(ℓ)가 잘못되었습니다. ℓ단위로 입력해주세요.';
                }
                else if (carcheck != "검차완료") {
                    chk = false;
                    message = '선택한 일자에 검차가 저장되지 않았습니다. 검차 저장 후 다시 등록해주세요.';

                    $("#divRegLog").hide();
                    if (cartype == "TOWERWAGON") {
                        $("#divCarChk2").show();
                    }
                    else {
                        $("#divCarChk1").show();
                    }
                }
                else if (leaveyn == 'R' && (reason == '' || reason == undefined)) {
                    chk = false;
                    message = '직퇴사유를 선택해주세요.'
                }
            }
            else {
                chk = false;
                message = '잘못된 접근입니다.';
            }

            if (chk) {
                if (leaveyn == 'R') {
                    message = '운행일지 등록 및 직퇴승인 요청하시겠습니까?';
                }
                else {
                    message = '운행일지를 등록 하시겠습니까?';
                }

                if (confirm(message)) {
                    //입력 파라미터
                    var aData = [];
                    aData[0] = resvKey;
                    aData[1] = enkm;
                    aData[2] = leaveyn;
                    aData[3] = fuels;
                    aData[4] = reason;
                    aData[5] = etc;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'InsCarLog');

                    callGb = 'InsOk';

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
            else {
                ktsGridAlert(message);
            }
        };

        var fn_DelLogCheck = function () {
            //검차체크기능 추가 필요

            var carid = $("#ddlCarList option:selected").val();
            var proctype = $("#<%= ddlProcType.ClientID %> option:selected").val();
            var logdt = $("#txbLogDt").val();

            var chk = true;
            var message = '';

            if (proctype == "DELETE") {
                if (logdt.length == 0) {
                    chk = false;
                    message = '운행일자를 선택하세요.';
                }
                else if (carid.length == 0) {
                    chk = false;
                    message = '차량을 선택하세요';
                }
            }
            else {
                chk = false;
                message = '잘못된 접근입니다.';
            }

            if (chk) {
                message = "선택한 차량의 " + logdt + "부터 등록 된 모든 운행일지가 삭제됩니다. 삭제하시겠습니까?";
                if (confirm(message)) {
                    //입력 파라미터
                    var aData = [];
                    aData[0] = carid;
                    aData[1] = logdt;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, 'DelCarLog');

                    callGb = 'DelOk';

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
            else {
                ktsGridAlert(message);
            }
        };

        var fn_RegCarChk = function (cartype) {
            if (confirm("검차결과를 저장하시겠습니까?")) {
                var resvKey = $("#ddlCarList option:selected").val();

                var method = "";

                var ChkList;
                var Notice = "";

                var bChk = true;

                if (cartype == "TOWERWAGON") {
                    method = 'InsCarChk2';
                    ChkList = ["Crane1", "Remote", "Outrigger", "Slewing", "Crane2", "WorkTable", "OilLv", "Safe", "Oil2", "Pin", "OilPump", "OilChg", "OilCylinder", "BlackBox1", "Etc2"];
                    Notice = $("#txaNotice2").val();
                }
                else {
                    method = 'InsCarChk1';
                    ChkList = ["Damage", "Oil1", "Tire", "Break", "Light", "Luggage", "BlackBox", "Etc1"];
                    Notice = $("#txaNotice1").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = resvKey;

                var i = 1;

                ChkList.forEach(function (str) {
                    if (!$("#chk" + str).is(":checked")) {
                        bChk = false;
                        aData[i] = "불량";
                    }
                    else {
                        aData[i] = "양호";
                    }
                    i++;
                });

                if (!bChk && Notice.length == 0) {
                    ktsGridAlert("불량항목이 있는 경우 특이사항을 반드시 입력해야합니다.");
                }
                else {
                    aData[i] = Notice;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, method);

                    callGb = 'InsChkOk';

                    ajaxCallEx(jsonData, ajaxUrl, true);

                    ChkList.forEach(function (str) {
                        $("#chk" + str).prop("checked", true);
                        $("#divChk" + str).removeClass("chkOff");
                        $("#divChk" + str).addClass("chkOn");
                        $("#lblChk" + str).text("양호");
                        $("#lblChk" + str).css("color", 'rgb(38 220 28)');
                    });

                    $("#lblCarChk").text('검차완료');
                    $("#lblCarChk").css("color", 'rgb(38 220 28)');

                    $("#txaNotice1").val();
                    $("#txaNotice2").val();

                    $("#divRegLog").show();
                    $("#divCarChk1").hide();
                    $("#divCarChk2").hide();
                }
            }
        };

        var fn_Search = function () {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStDt.ClientID %>").val();
            aData[1] = $("#<%= txbEnDt.ClientID %>").val();
            aData[2] = $("#<%= hdfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[3] = $("#<%= hdfEmpNo.ClientID %>").val();
            aData[4] = $("#<%= txbCarNo.ClientID %>").val();
            aData[5] = $("#<%= ExCarChk.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });
            $("#hdfSelect").val(jsonData);

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $("#<%= ddlProcType.ClientID %>").val("SAVE");
            $("#<%= ddlProcType.ClientID %>").change();
            $("#txbLogDt").val('');

            fn_InitCarList();
            fn_ResetCarInfo();

            $(".table-search").css('display', 'block');
        };

        var SetCarLoginfo = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = rowData.RESVKEY;
            aData[1] = rowData.STKM;
            aData[2] = rowData.ENKM;
            aData[3] = rowData.LEAVEKM;
            aData[4] = rowData.FUELAMOUNT;
            aData[5] = rowData.REASON;
            aData[6] = rowData.ETC;
            aData[7] = rowData.LEAVEYN;
            aData[8] = rowData.MODIYN;
            aData[9] = rowData.LOGDTTM;
            aData[10] = rowData.LASTLOGDTTM;
            aData[11] = rowData.EMPNO;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetCarLoginfo');

            callGb = 'RegOk';

            ajaxCallEx(jsonData, ajaxUrl, true);

            ktsGrid.setPage(pagenum);
        };

        var SetLeaveConfirm = function (id, ctype) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = rowData.RESVKEY;
            aData[1] = ctype;
            aData[2] = rowData.LEAVEYN;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetLeaveConfirm');

            if (ctype == "Y") {
                callGb = 'ConfirmOk';
            }
            else {
                callGb = 'ReturnOk';
            }

            ajaxCallEx(jsonData, ajaxUrl, true);

            ktsGrid.setPage(pagenum);
        };

        var DelCarLoginfo = function (id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = rowData.RESVKEY;
            aData[1] = rowData.DELYN;
            aData[2] = rowData.LOGDTTM;
            aData[3] = rowData.LASTLOGDTTM;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'DelCarLoginfo');

            callGb = 'DelOk';

            ajaxCallEx(jsonData, ajaxUrl, true);

            var maxPageNum = ktsGrid.getPageMax();

            if (maxPageNum < pagenum)
            {
                ktsGrid.setPage(maxPageNum);
            }
            else
            {
                ktsGrid.setPage(pagenum);
            }
        }

        var SetLeaveConfirm_All = function () {
            if (confirm("조회된 운행일지 중 모든 직퇴요청을 승인하시겠습니까? ")) {
                var jsonData = $("#hdfSelect").val();

                if (jsonData == undefined || jsonData == "" || jsonData == null) {
                    ktsGridAlert('조회가 되지않았습니다. 일괄 승인을 위해서는 조회를 먼저해야합니다.', 2000);
                }
                else {
                    var DataCnt = ktsGrid.getDataCount();

                    if (DataCnt > 0) {
                        //WebMethod Url
                        var ajaxUrl = ajaxCallGetUrl(location.href, 'SetLeaveConfirm_All');

                        callGb = 'ConfAllOk';

                        ajaxCallEx(jsonData, ajaxUrl, true);
                    }
                    else {
                        ktsGridAlert('조회된 데이터가 없습니다.', 2000);
                    }
                }
            }
        };

        var SetOrgCd = function (orgcd, chk) {
            this.focus();
            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = $("#<%= txbEnDt.ClientID %>").val().replace("-", "").substring(0, 6);

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        };

        var ExcelDownload = function () {
            ktsGrid.hideColumn("SAVE");

            ktsExcelFileNm = "공용차량 기동장비 운행일지"; //20221102 엑셀SIZE 관련 추가
            ktsGrid.download("json", ktsExcelFileNm + ".xlsx", { sheetName: ktsExcelFileNm }); //20221102 엑셀SIZE 관련 추가
            //ktsGrid.download("xlsx", "기동장비 운행일지.xlsx", { sheetName: "운행일지" });

            ktsGrid.showColumn("SAVE");
        };

        var fn_InitCarList = function () {
            $("#ddlCarList").empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택하세요";

            $("#ddlCarList").append(options);

            $("#ddlCarList").removeAttr('disable');
            $('#ddlCarList').attr('disabled', 'disabled');
        };

        var fn_ResetCarInfo = function () {
            $("#txbLogTime").val('');
            $("#txbFuels").val('');
            $("#txbStkm").removeAttr('readonly');
            $("#txbStkm").val('');
            $("#txbEnkm").val('');
            $("#txbNoti").val('');
            $("#txbEtc").val('');
            $("#<%= chkReqLeave.ClientID %>").prop("checked", false);
            $("#spanReason").hide();
            $("#<%= ddlReason.ClientID %>").val("");
            $("#lblCarChk").text("");
            $("#hdfLeaveYn").val("");
            $("#hdfLastLogDttm").val("");
            $("#hdfStTime").val("");
            $("#hdfLogChk").val("");
            $("#btnNoLogList").hide();
        };

        var fn_CarChk = function (chktype) {
            if ($("#chk" + chktype).is(":checked")) {
                $("#chk" + chktype).prop("checked", false);
                $("#divChk" + chktype).removeClass("chkOn");
                $("#divChk" + chktype).addClass("chkOff");
                $("#lblChk" + chktype).text("불량");
                $("#lblChk" + chktype).css("color", 'rgb(224 31 31)');
            }
            else {
                $("#chk" + chktype).prop("checked", true);
                $("#divChk" + chktype).removeClass("chkOff");
                $("#divChk" + chktype).addClass("chkOn");
                $("#lblChk" + chktype).text("양호");
                $("#lblChk" + chktype).css("color", 'rgb(38 220 28)');
            }
        };

        var fn_ControlBind_CarList = function (json) {
            var selector = $("#ddlCarList");

            selector.empty();
            var options = document.createElement("option");

            options.className = "options";
            options.value = "";

            if (JSON.parse(json).length > 0) {
                options.text = "선택하세요";

                selector.removeAttr('disabled');
            }
            else {
                options.text = "예약차량없음";

                selector.removeAttr('disabled');
                selector.attr('disabled', 'disabled');
            }

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.RESVKEY;
                options.text = item.CARNO;

                if (item.LOGYN == "N") {
                    options.style.backgroundColor = "lightcoral";
                }

                selector.append(options);
            });
        };

        var fn_SetCarInfo = function (json) {
            if (JSON.parse(json).length > 0) {
                $.each(JSON.parse(json)[0], function (key, value) {
                    switch (key) {
                        case 'LOGTIME':
                            $("#txbLogTime").val(value);
                            break
                        case 'STKM':
                            if (value == 0) {
                                $("#txbStkm").removeAttr('readonly');
                            }
                            else {
                                $("#txbStkm").attr('readonly', 'true');
                            }
                            $("#txbStkm").val(value);
                            break
                        case 'NOTICE':
                            $("#txbNoti").val(value);
                            break
                        case 'CARTYPE':
                            $("#hdfCarType").val(value);
                            break
                        case 'CARCHK':
                            $("#lblCarChk").text(value);

                            if (value == "검차완료") {
                                $("#lblCarChk").css("color", 'rgb(38 220 28)');
                            }
                            else {
                                $("#lblCarChk").css("color", 'rgb(224 31 31)');
                            }
                            break
                        case 'LEAVEYN':
                            $("#hdfLeaveYn").val(value);
                            break
                        case 'LASTLOGDTTM':
                            $("#hdfLastLogDttm").val(value);
                            break
                        case 'STTIME':
                            $("#hdfStTime").val(value); 
                            break
                        case 'LOGCHK':
                            $("#hdfLogChk").val(value);
                            break
                        default:
                            break
                    }
                });
            }
        };

        var fn_PopupNoLogList = function () {
            var resvKey = $("#ddlCarList option:selected").val();

            if (resvKey == "" || resvKey == undefined) {
                alert("잘못된 접근입니다.");
                $("#btnNoLogList").hide();
            }
            else {
                var nWidth = 872;
                var nHeight = 700;

                var form = "/Car/NoLogListPopUp";
                var target = "NoLogList";

                var param = {
                    pRESVKEY: resvKey,
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        };

        var PopupOrgTree = function (orgcd, empno) {

            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("운행일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: BaseMonth,
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        };

        var PopupEmpUser = function () {

            var nWidth = 700;
            var nHeight = 500;

            var form = "/Common/SearchUser";
            var target = "SearchUser";

            var param = {};

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        var SetDispEmpNo = function (orgcd, empno, empnm) {
            $("#<%=hdfEmpNo.ClientID %>").val(empno);
            $("#<%=txbEmpNm.ClientID %>").val(empnm);
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div id="divRegLog" class="searchbox" style="margin-top:0;">
		    <fieldset>
                <span class="optionbox first" id="spanProcType" runat="server">
					<label>등록/삭제</label>
                    <asp:DropDownList ID="ddlProcType" runat="server" AutoPostBack="false" style="width:70px;">
                        <asp:ListItem Text="등록" Value="SAVE" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
				</span>
                <span class="inpbox">
					<label>운행일자</label>
                    <input type="text" id="txbLogDt" style="width:110px" readonly="true" />
                </span>
                <span class="optionbox">
					<label>차량</label>
                    <select id="ddlCarList" style="width:120px;" disabled="disabled">
                        <option value="">선택하세요</option>
                    </select>
                    <input type="hidden" id="hdfCarType" value="" />
                    <input type="hidden" id="hdfLeaveYn" value="" />
                    <input type="hidden" id="hdfLastLogDttm" value="" />
                    <input type="hidden" id="hdfStTime" value="" />
                    <input type="hidden" id="hdfLogChk" value="" />
				</span>
                <span class="inpbox" id="spanLogTime">
					<label>운행시간</label>
                    <input type="text" id="txbLogTime" style="width:140px" readonly="true" />
                </span>
                <span class="inpbox" id="spanFuels">
					<label>주유(ℓ)</label>
                    <input type="text" id="txbFuels" style="width:60px" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="3" value="0" />
                </span>
                <span class="inpbox" id="spanStkm">
					<label>시작km</label>
                    <input type="text" id="txbStkm" style="width:90px" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11" readonly="readonly" />
                </span>
                <span class="inpbox" id="spanEnkm">
					<label>종료km</label>
                    <input type="text" id="txbEnkm" style="width:90px" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);" maxlength="11" value="0" />
                </span>
                <span class="ickbox" id="spanReqLeave">
                    <label>직퇴요청</label>
                    <input id="chkReqLeave" runat="server" type="checkbox" />
                </span>
                <span class="optionbox" id="spanReason" style="display:none;">
					<label>직퇴사유</label>
                    <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="false" style="width:150px">
                        <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="거주지 원거리" Value="거주지 원거리"></asp:ListItem>
                        <asp:ListItem Text="작업지역 원거리" Value="작업지역 원거리"></asp:ListItem>
                        <asp:ListItem Text="주52시간 근로" Value="주52시간 근로"></asp:ListItem>
                        <asp:ListItem Text="주차상면 부족" Value="주차상면 부족"></asp:ListItem>
                        <asp:ListItem Text="긴급출동 대기" Value="긴급출동 대기"></asp:ListItem>
                    </asp:DropDownList>
				</span>
                <span class="inpbox" id="spanCarChk">
                    <label id="lblCarChk" style="font: bold 12px;color:rgb(38 220 28);"></label>
                </span>
                <div style="float:right;">
                    <input id="btnRegLog" type="button" class="btn-green last" style="float:right;"  value="등록" />
                    <input id="btnDelLog" type="button" class="btn-green last" style="float:right;" value="삭제" runat="server" />
                </div>
                <hr class="blank" />
                <div>                         
                    <span class="inpbox">
					    <label>비고</label>
                        <input type="text" id="txbEtc" style="width:300px; margin-right:0;" maxlength="1500" />
                    </span>
                    <span style="margin-left:31px;">
                        <input id="btnNoLogList" type="button" class="btn-green" style="display:none;" value="일지미등록정보" />
                    </span>
                    <span class="inpbox" style="float:right;" >
					    <label>공지</label>
                        <input type="text" id="txbNoti" style="width:500px; margin:0" maxlength="1500" readonly="true" />
                    </span>
                </div>
            </fieldset>
        </div>
        <div id="divCarChk1" class="searchbox" style="display:none;">
		    <fieldset>    
                <span class="ickbox first" style="padding-top:9px;">
					<label>차량파손 : 차량 외관 상태 여부 확인</label>
                    <label id="lblChkDamage" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Damage');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Damage');">
                        <div id="divChkDamage" class="chkOn"></div>
                    </div>
                    <input id="chkDamage" type="checkbox" style="display:none;" checked="checked"/>
				</span>
                <span class="ickbox" style="padding-top:9px;">
					<label>오일누수 : 엔진오일, 미션오일, 브레이크오일등 차량 하부 누수 여부 확인</label>
                    <label id="lblChkOil11" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Oil1');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Oil1');">
                        <div id="divChkOil1" class="chkOn"></div>
                    </div>
                    <input id="chkOil1" type="checkbox" style="display:none;" checked="checked"/>
				</span>
                <span class="ickbox" style="padding-top:9px;">
					<label>타이어 : 타이어 마모 상태, 펑크 여부 확인</label>
                    <label id="lblChkTire" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Tire');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Tire');">
                        <div id="divChkTire" class="chkOn"></div>
                    </div>
                    <input id="chkTire" type="checkbox" style="display:none;" checked="checked"/>
                </span> </br>
                <span class="ickbox" style="padding-top:9px;">
					<label>브레이크</label>
                    <label id="lblChkBreak" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Break');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Break');">
                        <div id="divChkBreak" class="chkOn"></div>
                    </div>
                    <input id="chkBreak" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>라이트/계기판</label>
                    <label id="lblChkLight" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Light');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Light');">
                        <div id="divChkLight" class="chkOn"></div>
                    </div>
                    <input id="chkLight" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>적재물안전</label>
                    <label id="lblChkLuggage" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Luggage');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Luggage');">
                        <div id="divChkLuggage" class="chkOn"></div>
                    </div>
                    <input id="chkLuggage" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>블랙박스작동</label>
                    <label id="lblChkBlackBox" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('BlackBox');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('BlackBox');">
                        <div id="divChkBlackBox" class="chkOn"></div>
                    </div>
                    <input id="chkBlackBox" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>기타(직접기재)</label>
                    <label id="lblChkEtc1" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Etc1');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Etc1');">
                        <div id="divChkEtc1" class="chkOn"></div>
                    </div>
                    <input id="chkEtc1" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <div style="float:right;">
                    <input id="btnRegChk1" type="button" class="btn-green" style="margin-right:20px;" value="검차저장" />
                    <input id="btnCloseChk1" type="button" class="btn-green last" style="float:right;" value="닫기" />
                </div>
                <span class="inpbox">
                    <textarea id="txaNotice1" rows="5" maxlength="1500" style="width:1400px; height:100px;" placeholder="특이사항(불량항목이 있는 경우 반드시 입력)" ></textarea>
                </span>
            </fieldset>
        </div>
        <div id="divCarChk2" class="searchbox" style="display:none;">
		    <fieldset>    
                <span class="ickbox first" style="padding-top:9px;">
					<label>붐대외관</label>
                    <label id="lblChkCrane1" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Crane1');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Crane1');">
                        <div id="divChkCrane1" class="chkOn"></div>
                    </div>
                    <input id="chkCrane1" type="checkbox" style="display:none;" checked="checked"/>
				</span>
                <span class="ickbox" style="padding-top:9px;">
					<label>리모콘작동</label>
                    <label id="lblChkRemote" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Remote');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Remote');">
                        <div id="divChkRemote" class="chkOn"></div>
                    </div>
                    <input id="chkRemote" type="checkbox" style="display:none;" checked="checked"/>
				</span>
                <span class="ickbox" style="padding-top:9px;">
					<label>아우트리거작동</label>
                    <label id="lblChkOutrigger" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Outrigger');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Outrigger');">
                        <div id="divChkOutrigger" class="chkOn"></div>
                    </div> 
                    <input id="chkOutrigger" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>선회장치작동</label>
                    <label id="lblChkSlewing" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Slewing');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Slewing');">
                        <div id="divChkSlewing" class="chkOn"></div>
                    </div>
                    <input id="chkSlewing" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>붐대작동</label>
                    <label id="lblChkCrane2" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Crane2');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Crane2');">
                        <div id="divChkCrane2" class="chkOn"></div>
                    </div>
                    <input id="chkCrane2" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>작업대작동</label>
                    <label id="lblChkWorkTable" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('WorkTable');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('WorkTable');">
                        <div id="divChkWorkTable" class="chkOn"></div>
                    </div>
                    <input id="chkWorkTable" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>오일양점검</label>
                    <label id="lblChkOilLv" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('OilLv');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('OilLv');">
                        <div id="divChkOilLv" class="chkOn"></div>
                    </div>
                    <input id="chkOilLv" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>안전장치작동</label>
                    <label id="lblChkSafe" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Safe');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Safe');">
                        <div id="divChkSafe" class="chkOn"></div>
                    </div>
                    <input id="chkSafe" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <div style="float:right;">
                    <input id="btnRegChk2" type="button" class="btn-green" style="margin-right:20px;" value="검차저장" />
                    <input id="btnCloseChk2" type="button" class="btn-green last" style="float:right;" value="닫기" />
                </div>
                <span class="ickbox" style="padding-top:9px;">
					<label>오일누수</label>
                    <label id="lblChkOil2" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Oil2');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Oil2');">
                        <div id="divChkOil2" class="chkOn"></div>
                    </div>
                    <input id="chkOil2" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>핀,너트풀림</label>
                    <label id="lblChkPin" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Pin');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Pin');">
                        <div id="divChPin" class="chkOn"></div>
                    </div>
                    <input id="chkPin" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>유압펌프점검</label>
                    <label id="lblChkOilPump" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('OilPump');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('OilPump');">
                        <div id="divChkOilPump" class="chkOn"></div>
                    </div>
                    <input id="chkOilPump" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>유압오일교체</label>
                    <label id="lblChkOilChg" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('OilChg');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('OilChg');">
                        <div id="divChkOilChg" class="chkOn"></div>
                    </div>
                    <input id="chkOilChg" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>유압실린더점검</label>
                    <label id="lblChkOilCylinder" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('OilCylinder');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('OilCylinder');">
                        <div id="divChkOilCylinder" class="chkOn"></div>
                    </div>
                    <input id="chkOilCylinder" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label> 블랙박스작동</label>
                    <label id="lblChkBlackBox1" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('BlackBox1');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('BlackBox1');">
                        <div id="divChkBlackBox1" class="chkOn"></div>
                    </div>
                    <input id="chkBlackBox1" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="ickbox" style="padding-top:9px;">
					<label>기타(직접기재)</label>
                    <label id="lblChkEtc2" style="font: bold 12px;color:rgb(38 220 28); margin-left:5px;float: right;" onclick="fn_CarChk('Etc2');">양호</label>
                    <div class="divChkBackground" onclick="fn_CarChk('Etc2');">
                        <div id="divChkEtc2" class="chkOn"></div>
                    </div>
                    <input id="chkEtc2" type="checkbox" style="display:none;" checked="checked"/>
                </span>
                <span class="inpbox">
                    <textarea id="txaNotice2" rows="5" maxlength="1500" style="width:1400px; height:100px;" placeholder="특이사항(불량항목이 있는 경우 반드시 입력)" ></textarea>
                </span>
            </fieldset>
        </div>
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox first" style="margin-right:20px">
					<label>운행일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hdfOrgCd" runat="server"/>
				</span>
                <span class="inpbox" style="margin-right:20px">
					<label>이름</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
                    <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hdfEmpNo" runat="server" />
                </span>
                <span class="inpbox">
					<label>차량번호</label>
                    <asp:TextBox ID="txbCarNo" runat="server" Width="100px"></asp:TextBox>
                </span>
                 <span class="optionbox" id="ExCarChkSpan" runat="server">
					<label>차량구분</label>
                    <asp:DropDownList ID="ExCarChk" runat="server" AutoPostBack="false" style="width:150px">
                        <asp:ListItem Text="운행차량" Value="TRUE" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="해지차량" Value="FALSE"></asp:ListItem>
                    </asp:DropDownList>
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
                <input id="hdfSelect" type="hidden" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>운행일지관리</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <asp:UpdatePanel ID="udpBtn" runat="server">
                            <ContentTemplate>
                                <asp:HiddenField ID="hdfLeaveCnt" runat="server" Value="0" OnValueChanged="hdfLeaveCnt_ValueChanged" />
                                <input id="btnAllConf" runat="server" type="button" class="btn-green" style="margin-right:5px;" visible="false" value="일괄승인" />
                                <input id="gridExcel" runat="server" type="button" class="btn-green last"  value="엑셀" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger  ControlID="hdfLeaveCnt" EventName="ValueChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="ORGFULLNM">소속</option>
                            <option value="EMPNO">사번</option>
                            <option value="KTSEMPNO">KTS사번</option>
                            <option value="EMPNM">성명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
