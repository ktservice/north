﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserCarlogStats_ByMgmtStatus.aspx.cs" Inherits="KTSSolutionWeb.UserCarlogStats_ByMgmtStatus" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            options = {
                pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
            };

            $(".stMonth").monthpicker(options);
            $(".etMonth").monthpicker(options);

            $("#<%= txbEnMonth.ClientID %>").change(function () {
            });

            $(".stMonth").change(function (e) {
                if (!CompareMonToday(this.value)) {
                    $(".stMonth").val($("#hdnStMonth").val());
                }
                else {
                    $("#hdnStMonth").val($(".stMonth").val());
                }

                return false;
            });

            $(".etMonth").change(function (e) {
                if (!CompareMonToday(this.value)) {
                    $(".etMonth").val($("#hdnEtMonth").val());
                }
                else {
                    $("#hdnEtMonth").val($(".etMonth").val());
                }

                return false;
            });

            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            var fn_Cellcolor = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var chgyn = rowData.COLORCHGYN;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#bbffff";
                    cell.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "월", field: "MDATE", sorter: "string", width: 120, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140, visible: false, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140, visible: false, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "팀", field: "ORGNM4", sorter: "string", width: 140, visible: false, formatter: fn_Cellcolor },
                {
                    title: '차종', headerHozAlign: "center",
                    columns: [
                        {
                            title: '운행', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "이륜차", field: "R_AUTOBICYCLE", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "경차", field: "R_SMALL", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "전기차", field: "R_ELECTRIC", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "중형차", field: "R_MIDSIZE", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "SUV", field: "R_SUV", sorter: "number", width: 120, formatter: fn_Cellcolor }
                            ]
                        },
                        {
                            title: '유휴', headerHozAlign: "center",
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "이륜차", field: "U_AUTOBICYCLE", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "경차", field: "U_SMALL", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "전기차", field: "U_ELECTRIC", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "중형차", field: "U_MIDSIZE", sorter: "number", width: 120, formatter: fn_Cellcolor },
                                { headerHozAlign: "center", hozAlign: "center", title: "SUV", field: "U_SUV", sorter: "number", width: 120, formatter: fn_Cellcolor }
                            ]
                        },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", field: "COLORCHGYN", sorter: "string", visible: false }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "기동장비 운용현황.xlsx", { sheetName: "기동장비 운용현황" });
            });
        });

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                $("#<%=txbTeam.ClientID %>").val(json);
            }
            callGb = '';
        }

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                $.each(JSON.parse(json), function (key, value) {
                    if (key == 'Table') {
                        ktsGrid.setData(value);
                        ktsGridRowCount(ktsGrid.getDataCount());
                        ktsGridSearch();
                    }
                    else {
                        var orglv = value[0].MINORGLV;

                        if (orglv == "0") {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.hideColumn("ORGNM2");
                            ktsGrid.hideColumn("ORGNM3");
                            ktsGrid.hideColumn("ORGNM4");
                        }
                        else if(orglv == "1") {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.showColumn("ORGNM2");
                            ktsGrid.hideColumn("ORGNM3");
                            ktsGrid.hideColumn("ORGNM4");
                        }
                        else if (orglv == "2") {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.showColumn("ORGNM2");
                            ktsGrid.showColumn("ORGNM3");
                            ktsGrid.hideColumn("ORGNM4");

                        }
                        else {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.showColumn("ORGNM2");
                            ktsGrid.showColumn("ORGNM3");
                            ktsGrid.showColumn("ORGNM4");
                        }
                    }
                });

            }
        }
        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStMonth.ClientID %>").val().replace("-", "");
            aData[1] = $("#<%= txbEnMonth.ClientID %>").val().replace("-", "");
            aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');
            callGb = 'GetData';
            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

        }

        function PopupOrgTree(orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;

            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }

        var ajaxCallBack = function (json) {
            callGb = '';
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
					<label>기간</label>
                    <asp:TextBox ID="txbStMonth" runat="server" class="stMonth" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnMonth" runat="server" class="enMonth" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfOrgCd" runat="server"/>
				</span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
                <input id="hdnStMonth" type="hidden" />
                <input id="hdnEtMonth" type="hidden" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>운행현황</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
