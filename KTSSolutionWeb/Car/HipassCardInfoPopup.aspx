﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HipassCardInfoPopup.aspx.cs" Inherits="KTSSolutionWeb.HipassCardInfoPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Hipass카드 정보조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();

            $(".table-search").css('display', 'none');
        });

        var grid;
        var callGb;

        var page = {
            init: function () {
                fn_PageInit();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#btnExcel").click(function (e) {
                    ktsGrid.hideColumn("SAVE");
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                    ktsGrid.showColumn("SAVE");
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnAdd").click(function (e) {
                    var row = ktsGrid.getRow(0);

                    if (!row) {
                        ktsGrid.addRow({ id: 0 }, true, 0);
                    }
                });

                var FieldEl = document.getElementById("selField");
                var ValueEl = document.getElementById("inputValue");

                $("#selField").change(function (e) {
                    ValueEl.value = '';

                    var sField = FieldEl.value;

                    if (sField == "") {
                        ValueEl.setAttribute('style', 'display:inline;');
                        ValueEl.setAttribute('disabled', 'disabled');

                        sField = 'id';
                    }
                    else {
                        ValueEl.removeAttribute('disabled');
                        ValueEl.setAttribute('style', 'display:inline;');
                    }

                    ktsGrid.setFilter(sField, 'like', '');
                });

                $("#inputValue").keyup(function (e) {
                    ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
                });
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        if (callGb == 'AddOk') {
                            ktsGridAlert('카드정보가 추가되었습니다.', 2000);
                        }
                        else if (callGb == 'RegOk') {
                            ktsGridAlert('카드정보가 수정되었습니다.', 2000);
                        }
                        else if (callGb == 'DelOk') {
                            ktsGridAlert('카드정보가 삭제되었습니다.', 2000);
                        }

                        fn_Search();
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });
            }
        };

        var fn_PageInit = function () {
            var parenttype = $("#<%= hfParentType.ClientID %>").val();

            if (parenttype != "") {
                $("#divTemp").hide();
                $("#divFile").hide();
                $("#btnExcel").hide();
                $("#btnAdd").hide();
                $("#btnSelect").removeClass();
                $("#btnSelect").addClass("btn-green last");
                $("#btnSelect").css("float", "right;");
            }
        };

        var fn_input = function (cell, onRendered, success, cancel, editorParams) {
            //cell - the cell component for the editable cell
            //onRendered - function to call when the editor has been rendered
            //success - function to call to pass thesuccessfully updated value to Tabulator
            //cancel - function to call to abort the edit and return to a normal cell
            //editorParams - params object passed into the editorParams column definition property

            var parenttype = $("#<%= hfParentType.ClientID %>").val();

            var rowData = cell.getRow().getData();
            var rowNum = rowData.id;

            if (rowNum == 0) {
                //create and style editor
                var editor = document.createElement("input");

                editor.setAttribute("type", "text");

                //create and style input
                editor.style.padding = "4px";
                editor.style.width = "100%";
                editor.style.boxSizing = "border-box";
                editor.style.height = "100%";

                var cellValue = cell.getValue()

                if (cellValue != undefined) {
                    editor.value = cellValue;
                }

                //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
                onRendered(function () {
                    editor.focus();
                    editor.style.css = "100%";
                });

                function successFunc() {
                    success(editor.value);
                }

                editor.addEventListener("blur", successFunc);

                //return the editor element
                return editor;
            }
            else {
                return cell.getValue();
            }
        };

        var fn_caridbutton = function (cell) {
            var cellValue = cell.getValue();
            var rowData = cell.getRow().getData();

            var id = rowData.id;
            var input = "";

            if (cellValue == '' || cellValue == undefined) {
                cellValue = "선택";
            }

            input = "<input id='btnCellCarInfo' type='button' onclick='fn_PopupCarInfo(\"" + id + "\");' value='" + cellValue + "' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;padding:0 5px;'>";

            return input;
        };

        var fn_Choicebutton = function (cell) {
            var rowData = cell.getRow().getData();

            var cardno = rowData.CARDNO;

            var input = "<input id='btnCellCard' type='button' onclick='SendParentsForm(\"" + cardno + "\");' value='선택' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";

            return input;
        };

        var fn_Regbutton = function (cell) {

            var rowData = cell.getRow().getData();

            var id = rowData.id;

            var input = "";

            if (id == 0) {
                input = "<input id='btnCellAdd' type='button' onclick='btnAddCheck();' value='추가' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDelRow' type='button' onclick='btnDelRow();' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px'>";
            }
            else {
                input = "<input id='btnCellReg' type='button' onclick='btnRegCheck(\"" + id + "\");' value='수정' class='btn-save' style='height:23px;line-height:0px;font-size:15px'>";
                input += "&nbsp;<input id='btnCellDel' type='button' onclick='btnDelCheck(\"" + id + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>"
            }
            return input;
        };

        var fn_GridInit = function () {

            var parenttype = $("#<%= hfParentType.ClientID %>").val();

            var Columns;

            if (parenttype != "") {
                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                    { headerHozAlign: "center", hozAlign: "center", title: "카드번호", field: "CARDNO", sorter: "string", width: 240 },
                    { headerHozAlign: "center", hozAlign: "center", title: "카드명", field: "CARDNM", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 200 },
                    { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 80, formatter: fn_Choicebutton }
                ];
            }
            else {
                Columns = [
                    { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                    { headerHozAlign: "center", hozAlign: "center", title: "카드번호", field: "CARDNO", sorter: "string", width: 240, editor: fn_input },
                    { headerHozAlign: "center", hozAlign: "center", title: "카드명", field: "CARDNM", sorter: "string", width: 200, editor: "input" },
                    { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 200, formatter: fn_caridbutton },
                    { headerHozAlign: "center", hozAlign: "center", title: "저장/삭제", field: "SAVE", width: 160, formatter: fn_Regbutton }
                ];
            }

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("60vh");
        }

        var fn_Load = function () {
            fn_Search();
        }

        var fn_Search = function () {
            callGb = '';

            //입력 파라미터
            var aData = [];

            aData[0] = "H";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

        function DownloadTemplate() {
            window.location.href = '/Template/하이패스카드 Excel Upload Template.xlsx';
        }

        function btnUploadCheck() {
            if (confirm("Hipass카드 정보를 일괄 등록 하시겠습니까? 엑셀에 중복으로 저장된 데이터는 등록되지 않습니다.")) {
                return true;
            }
            else {
                return false;
            }
        }

        function SendParentsForm(cardno) {
            if (confirm("사용중인 카드인 경우 소유자가 변경됩니다.")) {
                var carid = $("#<%= hfCarId.ClientID %>").val();

                opener.SetGridCardNo(carid, cardno, "H");
                window.close();
            }
            else {
                return false;
            }
        }

        function fn_PopupCarInfo(id) {
            var form = "/Car/Search_CarId";
            var target = "Search_CarId";

            var param = {
                pROWID: id
            };

            var nWidth = 1050;
            var nHeight = 515;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        function SetGridCarId(rowNum, carid) {
            ktsGrid.updateData([{
                id: rowNum, CARID: carid
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });
        }

        function btnAddCheck() {
            var row = ktsGrid.getRow(0);
            var rowData = row.getData();

            var cardno = rowData.CARDNO;
            var cardnm = rowData.CARDNM;
            var carid = rowData.CARID;

            var bChk = true;

            if (cardno == undefined || cardno == "") {
                alert("카드번호를 입력해주세요");
                bChk = false;
            }
            else if (cardnm == undefined || cardnm == "") {
                alert("카드카드명을 입력해주세요");
                bChk = false;
            }

            if (bChk) {
                if (confirm(cardno + " 정보를 추가 하시겠습니까?")) {
                    InsCardNoInfo(cardno, cardnm, carid);
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function btnDelRow() {
            var row = ktsGrid.getRow(0);

            if (row) {
                ktsGrid.deleteRow(0);
            }
        }

        function btnRegCheck(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var cardno = rowData.CARDNO;
            var cardnm = rowData.CARDNM;
            var carid = rowData.CARID;

            if (confirm(cardno + " 정보를 수정 하시겠습니까?")) {
                UpdCardNoInfo(cardno, cardnm, carid);
            }
            else {
                return false;
            }
        }

        function btnDelCheck(id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var cardno = rowData.CARDNO;

            if (confirm(cardno + " 정보를 삭제 하시겠습니까?")) {

                DelCardNoInfo(cardno);
            }
            else {
                return false;
            }
        }

        function InsCardNoInfo(cardno, cardnm, carid) {
            var pagenum = ktsGrid.getPage();
            var aData = [];

            aData[0] = cardno;
            aData[1] = cardnm;
            aData[2] = "";
            aData[3] = carid;
            aData[4] = "";
            aData[5] = "H";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'InsCardNo');

            callGb = 'AddOk';
            ajaxCallEx(jsonData, ajaxUrl, true);

            ktsGrid.setPage(pagenum);
        }

        function UpdCardNoInfo(cardno, cardnm, carid) {
            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = cardno;
            aData[1] = cardnm;
            aData[2] = "";
            aData[3] = carid;
            aData[4] = "";
            aData[5] = "H";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'UpdCardInfo');

            callGb = 'RegOk';
            ajaxCallEx(jsonData, ajaxUrl, true);

            ktsGrid.setPage(pagenum);
        }

        function DelCardNoInfo(cardno) {
            var pagenum = ktsGrid.getPage();

            var aData = [];

            aData[0] = cardno;
            aData[1] = "H";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'DelCardNo');

            callGb = 'DelOk';
            ajaxCallEx(jsonData, ajaxUrl, true);

            var maxPageNum = ktsGrid.getPageMax();

            if (maxPageNum < pagenum) {
                ktsGrid.setPage(maxPageNum);
            }
            else {
                ktsGrid.setPage(pagenum);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">Hipass카드 정보조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
				                <div id="divTemp"  class="btnset">
                                    <button id="btnTemp" type="button" onclick="DownloadTemplate();" class="btn-green">양식다운로드</button>
				                </div>
                                <div id="divFile" class="filebox">
                                    <input type="text" id="txtfu" />
                                    <span class="file">
                                        <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
                                        <label for="fu">찾아보기</label>
                                    </span>
                                    <asp:Button ID="btnUpload" runat="server" OnClientClick="return btnUploadCheck();" OnClick="btnUpload_Click" class="btn-black" Text="일괄등록" />
                                </div>
				                <div class="btnset">
                                    <input id="btnSelect" type="button" class="btn-green" value="조회" />
                                    <input id="btnAdd" type="button" class="btn-green" value="추가" />
                                    <input id="gridExcel" type="button" class="btn-green last" style="float:right;" value="엑셀" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search" style="display:none">
                                <span>
                                    <select id="selField" style="font-weight:bold">
                                        <option value="" selected="selected" >선택</option>
                                        <option value="CARDNO">카드번호</option>
                                        <option value="CARDNM">카드명</option>
                                        <option value="CARID">차대번호</option>
                                    </select>
                                    <label> : </label>
                                    <input id="inputValue" type="text" value="" disabled="disabled" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:HiddenField ID="hfCarId" runat="server" />
        <asp:HiddenField ID="hfCardType" runat="server" />
        <asp:HiddenField ID="hfParentType" runat="server" />
    </form>
</body>
</html>
