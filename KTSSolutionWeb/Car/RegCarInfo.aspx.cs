﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Oper;
using System.Collections.Generic;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class RegCarInfo : PageBase
    {
        private DataTable DtCarType
        {
            get
            {
                if (ViewState["DtCarType"] != null)
                    return (DataTable)ViewState["DtCarType"];
                else
                    return null;
            }
            set
            {
                ViewState["DtCarType"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    PageInit();

                    updPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void PageInit()
        {
            DataSet ds = null;

            try
            {
                string resultSession = string.Empty;
                string rptCd = "CARTYPELIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("");
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    DtCarType = ds.Tables[0];

                    for (int i = 0; i < DtCarType.Rows.Count; i++)
                    {
                        ddlCarType.Items.Add(new ListItem(DtCarType.Rows[i]["CARTYPENM"].ToString(), DtCarType.Rows[i]["CARTYPE"].ToString()));
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PageInit", "alert('오류가 발생했습니다.');", true);
                }

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetEmpAge()
        {

            DataSet ds = new DataSet();

            try
            {
                string strCarType = this.ddlCarType.SelectedValue;
                string strEmpNo = Request.Form[hdfEmpNo.UniqueID];

                if (strCarType.Length > 0 && strEmpNo.Length > 0)
                {
                    string resultSession = string.Empty;
                    string rptCd = "GETUSERAGE";
                    string jobGb = "GET";

                    List<string> aData = new List<string>();

                    aData.Add(strCarType);
                    aData.Add(strEmpNo);
                    aData.Add("F");

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        ds = dbTransaction.GetDataSet();

                        if (ds.Tables.Count == 1)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                this.txbEmpAge.Text = ds.Tables[0].Rows[0]["EMPAGE"].ToString();
                                this.ddlInsuranceAge.SelectedValue = ds.Tables[0].Rows[0]["INSURANCEAGE"].ToString();

                                if (ds.Tables[0].Rows[0]["EMPAGE"].ToString().Equals(""))
                                {
                                    this.txbEmpAge.ReadOnly = false;

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 나이를 조회할 수 없습니다. 직접 입력해주세요.');", true);
                                }
                            }
                            else
                            {
                                this.txbEmpAge.ReadOnly = false;

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 나이를 조회할 수 없습니다. 직접 입력해주세요.');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('사용자 나이 조회 중 오류가 발생했습니다.');", true);
                        }
                    }
                    else //세션이 끊겼을 경우
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private bool ValidationCheck(ref string strMsg)
        {
            bool bCheck = true;

            string strCarId = Request.Form[this.txbCarId.UniqueID];
            string strCarIdChk = Request.Form[this.hdfIdChk.UniqueID];
            string strCarNo = Request.Form[this.txbCarNo.UniqueID];
            string strCarNoChk = Request.Form[this.hdfNoChk.UniqueID];
            string strStDate = Request.Form[this.txbStDate.UniqueID];
            string strEnDate = Request.Form[this.txbEnDate.UniqueID];
            string strCarType = this.ddlCarType.SelectedValue;
            string strCarOrgCd = Request.Form[this.hdfOrgCd.UniqueID];
            string strEmpNo = Request.Form[this.hdfEmpNo.UniqueID];
            string strEmpAge = Request.Form[this.txbEmpAge.UniqueID];
            string strInsuranceAge = this.ddlInsuranceAge.SelectedValue;

            int nEmpAge = 0;
            int nInsAge = 0;

            int.TryParse(strEmpAge.Replace("만", "").Replace("세",""), out nEmpAge);
            int.TryParse(strInsuranceAge.Replace("만", "").Replace("세", ""), out nInsAge);

            if (!strCarIdChk.Equals("Y") || !strCarNoChk.Equals("Y") || strCarId.Equals("") || strCarNo.Equals(""))
            {
                strMsg = "잘못된 접근입니다.";
                return false;
            }

            if (strStDate.Equals("") || strEnDate.Equals(""))
            {
                strMsg = "시작일과 종료일(보험종료일)을 입력해주세요.";
                return false;
            }

            if (strCarType.Equals(""))
            {
                strMsg = "차종을 선택해주세요.";
                return false;
            }

            if (strCarOrgCd.Equals(""))
            {
                strMsg = "차량소속을 입력해주세요.";
                return false;
            }

            if (strInsuranceAge.Equals(""))
            {
                strMsg = "보험나이를 선택해 주세요.";
                return false;
            }

            if (!strEmpNo.Equals(""))
            {
                if (nEmpAge < nInsAge)
                {
                    strMsg = "보험나이는 만나이보다 작거나 같아야 합니다.";
                    return false;
                }
            }

            return bCheck;
        }

        protected void btnIDChk_Click(object sender, EventArgs e)
        {
            try
            {
                string strStDate = Request.Form[this.txbStDate.UniqueID];
                string strEnDate = Request.Form[this.txbEnDate.UniqueID];
                string strFuel = Request.Form[this.txbFuel.UniqueID];
                string strCharge = Request.Form[this.txbCharge.UniqueID];
                string strHipass = Request.Form[this.txbHipass.UniqueID];
                string strNotice = Request.Form[this.txbNotice.UniqueID];
                string strNotice2 = Request.Form[this.txbNotice2.UniqueID];

                this.txbStDate.Text = strStDate;
                this.txbEnDate.Text = strEnDate;
                this.txbFuel.Text = strFuel;
                this.txbCharge.Text = strCharge;
                this.txbHipass.Text = strHipass;
                this.txbNotice.Text = strNotice;
                this.txbNotice2.Text = strNotice2;

                Button btn = (Button)sender;

                string strCheckType = "CARID";
                string strCheckTypeNm = "차대번호";
                string strCarInfo = "";

                if (btn.ClientID == "btnCarIDChk")
                {
                    strCheckType = "CARID";
                    strCheckTypeNm = "차대번호";
                    strCarInfo = txbCarId.Text;
                }
                else
                {
                    strCheckType = "CARNO";
                    strCheckTypeNm = "차량번호";
                    strCarInfo = txbCarNo.Text;
                }

                DataSet ds = null;

                string resultSession = string.Empty;
                string rptCd = "CARIDNOCHECK";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add(strCarInfo);
                aData.Add(strCheckType);
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables.Count == 1)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "RegCarCheck", "alert('이미 등록 된 " + strCheckTypeNm + " 입니다.');", true);
                        }
                        else
                        {
                            if (btn.ClientID == "btnCarIDChk")
                            {
                                hdfIdChk.Value = "Y";
                            }
                            else
                            {
                                hdfNoChk.Value = "Y";
                            }

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "RegCarCheck", "alert('등록 가능한 " + strCheckTypeNm + " 입니다.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "RegCarCheck", "alert('" + strCheckTypeNm + " 체크 중 오류가 발생했습니다.');", true);
                    }
                }
                else //세션이 끊겼을 경우
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RegCarCheck", "alert('" + strCheckTypeNm + " 체크 중 오류가 발생했습니다.');", true);
                }

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlCarType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strStDate = Request.Form[this.txbStDate.UniqueID];
                string strEnDate = Request.Form[this.txbEnDate.UniqueID];
                string strFuel = Request.Form[this.txbFuel.UniqueID];
                string strCharge = Request.Form[this.txbCharge.UniqueID];
                string strHipass = Request.Form[this.txbHipass.UniqueID];
                string strNotice = Request.Form[this.txbNotice.UniqueID];
                string strNotice2 = Request.Form[this.txbNotice2.UniqueID];

                this.txbStDate.Text = strStDate;
                this.txbEnDate.Text = strEnDate;
                this.txbFuel.Text = strFuel;
                this.txbCharge.Text = strCharge;
                this.txbHipass.Text = strHipass;
                this.txbNotice.Text = strNotice;
                this.txbNotice2.Text = strNotice2;

                string strCarType = ddlCarType.SelectedValue;

                if (strCarType.Length > 0)
                {
                    string strWhere = string.Format("CARTYPE = '{0}'", strCarType);
                    DataRow[] dr = DtCarType.Select(strWhere);

                    if (dr.Length > 0)
                    {
                        if (dr[0]["INSURANCE"].ToString().Equals("보험계약"))
                        {
                            this.txbRentalPrice.Text = "";
                        }
                        else
                        {
                            this.txbRentalPrice.Text = "";
                        }

                        this.txbPosseionType.Text = dr[0]["POSSESSIONTYPENM"].ToString();
                    }
                    else
                    {
                        this.txbPosseionType.Text = "";
                        this.txbRentalPrice.Text = "";
                    }
                }
                else
                {
                    this.txbPosseionType.Text = "";
                    this.txbRentalPrice.Text = "";
                }

                GetEmpAge();

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strStDate = Request.Form[this.txbStDate.UniqueID];
                string strEnDate = Request.Form[this.txbEnDate.UniqueID];
                string strFuel = Request.Form[this.txbFuel.UniqueID];
                string strCharge = Request.Form[this.txbCharge.UniqueID];
                string strHipass = Request.Form[this.txbHipass.UniqueID];
                string strNotice = Request.Form[this.txbNotice.UniqueID];
                string strNotice2 = Request.Form[this.txbNotice2.UniqueID];

                this.txbStDate.Text = strStDate;
                this.txbEnDate.Text = strEnDate;
                this.txbFuel.Text = strFuel;
                this.txbCharge.Text = strCharge;
                this.txbHipass.Text = strHipass;
                this.txbNotice.Text = strNotice;
                this.txbNotice2.Text = strNotice2;

                string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
                bool bValCheck = true;

                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetOrgInfo(strOrgCd);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.txbTeam.Text = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString();

                            this.updPanel1.Update();
                        }
                        else
                        {
                            bValCheck = false;
                        }
                    }
                    else
                    {
                        bValCheck = false;
                    }

                    if (!bValCheck)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void hdfEmpNo_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strStDate = Request.Form[this.txbStDate.UniqueID];
                string strEnDate = Request.Form[this.txbEnDate.UniqueID];
                string strFuel = Request.Form[this.txbFuel.UniqueID];
                string strCharge = Request.Form[this.txbCharge.UniqueID];
                string strHipass = Request.Form[this.txbHipass.UniqueID];
                string strNotice = Request.Form[this.txbNotice.UniqueID];
                string strNotice2 = Request.Form[this.txbNotice2.UniqueID];

                this.txbStDate.Text = strStDate;
                this.txbEnDate.Text = strEnDate;
                this.txbFuel.Text = strFuel;
                this.txbCharge.Text = strCharge;
                this.txbHipass.Text = strHipass;
                this.txbNotice.Text = strNotice;
                this.txbNotice2.Text = strNotice2;

                string strOrgCd = Request.Form[hdfOrgCd.UniqueID];
                string strEmpNm = Request.Form[hdfEmpNm.UniqueID];

                bool bValCheck = true;

                using (UserMgmt userReg = new UserMgmt())
                {
                    ds = userReg.GetOrgInfo(strOrgCd);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            this.txbEmpUserNm.Text = ds.Tables[0].Rows[0]["ORGFULLNM"].ToString() + " " + strEmpNm;
                        }
                        else
                        {
                            bValCheck = false;
                        }
                    }
                    else
                    {
                        bValCheck = false;
                    }

                    if (!bValCheck)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('잘못된 접근입니다.');", true);
                    }
                }

                GetEmpAge();

                this.updPanel1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void BtnReg_Click(object sender, EventArgs e)
        {
            try
            {
                string strMsg = "";

                if (ValidationCheck(ref strMsg))
                {
                    string strRegEmpNo = this.Session["EMPNO"].ToString();

                    string strCarId = Request.Form[this.txbCarId.UniqueID];
                    string strCarNo = Request.Form[this.txbCarNo.UniqueID];
                    string strStDate = Request.Form[this.txbStDate.UniqueID];
                    string strEnDate = Request.Form[this.txbEnDate.UniqueID];
                    string strCarType = this.ddlCarType.SelectedValue;
                    string strCarTypeNm = this.ddlCarType.SelectedItem.Text;
                    string strModelNm = Request.Form[this.txbModelNm.UniqueID];
                    string strPosseionType = Request.Form[this.txbPosseionType.UniqueID];
                    string strCarPrice = Request.Form[this.txbCarPrice.UniqueID];
                    string strRentalPrice = Request.Form[this.txbRentalPrice.UniqueID];
                    string strCarOrgCd = Request.Form[this.hdfOrgCd.UniqueID];
                    string strEmpNo = Request.Form[this.hdfEmpNo.UniqueID];
                    string strInsuranceAge = this.ddlInsuranceAge.SelectedValue;
                    string strEmpAge = Request.Form[this.txbEmpAge.UniqueID];
                    string strPenalty = this.ddlPenalty.SelectedValue;
                    string strFuel = Request.Form[this.txbFuel.UniqueID];
                    string strCharge = Request.Form[this.txbCharge.UniqueID];
                    string strHipass = Request.Form[this.txbHipass.UniqueID];
                    string strNotice = Request.Form[this.txbNotice.UniqueID];
                    string strNotice2 = Request.Form[this.txbNotice2.UniqueID];

                    string result = string.Empty;
                    string resultSession = string.Empty;
                    string rptCd = "CARIDINFOINSERT";
                    string jobGb = "SET";

                    List<string> aData = new List<string>();

                    aData.Add(strCarId);
                    aData.Add(strCarNo);
                    aData.Add(strStDate);
                    aData.Add(strEnDate);
                    aData.Add(strCarType);
                    aData.Add(strCarTypeNm);
                    aData.Add(strModelNm);
                    aData.Add(strPosseionType);
                    aData.Add(strCarPrice);
                    aData.Add(strRentalPrice);
                    aData.Add(strCarOrgCd);
                    aData.Add(strEmpNo);
                    aData.Add(strInsuranceAge);
                    aData.Add(strEmpAge);
                    aData.Add(strPenalty);
                    aData.Add(strFuel);
                    aData.Add(strCharge);
                    aData.Add(strHipass);
                    aData.Add(strNotice);
                    aData.Add(strNotice2);
                    aData.Add(strRegEmpNo);

                    DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                    resultSession = dbTransaction.SessionCheck();

                    if (resultSession.Equals("")) //세션 유지일 경우 정상
                    {
                        result = dbTransaction.GetJsonData();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOK", "alert('차량등록이 완료되었습니다.');window.close();", true);
                    }
                    else //세션이 끊겼을 경우
                    {
                        result = resultSession;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('차량등록중 오류가 발생했습니다.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('" + strMsg + "');", true);
                }
            }
            catch(Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}