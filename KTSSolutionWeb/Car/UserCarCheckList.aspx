﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserCarCheckList.aspx.cs" Inherits="KTSSolutionWeb.UserCarCheckList" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;
        var Columns1;
        var Columns2;

        $(document).ready(function () {
            Columns1 = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "검차일자", field: "LOGDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량파손", field: "DAMAGE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "오일누수", field: "OIL1", sorter: "string", width: 100},
                { headerHozAlign: "center", hozAlign: "center", title: "타이어", field: "TIRE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "브레이크", field: "BREAK", sorter: "string", width: 100},
                { headerHozAlign: "center", hozAlign: "center", title: "라이트/계기판", field: "LIGHT", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "적재물안전", field: "LUGGAGE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "블랙박스", field: "BlackBox", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "기타(직접기재)", field: "ETC", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "특이사항", field: "NOTICE", sorter: "string", width: 300 }
            ];

            Columns2 = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "검차일자", field: "LOGDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차대번호", field: "CARID", sorter: "string", width: 160, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "차량번호", field: "CARNO", sorter: "string", width: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "붐대외관", field: "CRANE1", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "리모콘작동", field: "REMOTE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "아우트리거작동", field: "OUTRIGGER", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "선회장치작동", field: "SLEWING", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "붐대작동", field: "CRANE2", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "작업대작동", field: "WORKTABLE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "오일양점검", field: "OILLV", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "안전장치작동", field: "SAFE", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "오일누수", field: "OIL2", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "핀,너트풀림", field: "PIN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "유압펌프점검", field: "OILPUMP", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "유압오일교체", field: "OILCHG", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "유압실린더점검", field: "OILCYLINDER", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "블랙박스", field: "BlackBox", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "기타(직접기재)", field: "ETC", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "특이사항", field: "NOTICE", sorter: "string", width: 400 }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns1;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            /*----검색 조건 ------- */

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                ValueEl.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');
                    sField = 'id';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });
            
            $("#btnSelect").click(function (e) {
                fn_Search();
            }); 

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "기간별검차내역.xlsx", { sheetName: "기간별검차내역" });
            });

            $(".table-search").css('display', 'none');
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (callGb == 'GetCarInfo') {
                        fn_SetCarInfo(json);                        
                    }
                    else if (callGb == 'GetCarChkInfo') {
                        fn_GetCarChkInfo(json);
                    }
                    else {
                        if (item.ERRCD == '00') {
                            if (callGb == 'InsOk') {
                                ktsGridAlert('운행일지가 등록 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'ConfirmOk') {
                                ktsGridAlert('직퇴가 승인 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'ConfirmNo') {
                                ktsGridAlert('직퇴가 반려 되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'LeaveOk') {
                                ktsGridAlert('직퇴 정보가 등록되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'RegOk') {
                                ktsGridAlert('운행일지가 수정되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'DelOk') {
                                ktsGridAlert('운행일지가 삭제되었습니다.', 2000);
                                fn_Search();
                            }
                            else if (callGb == 'InsChkOk') {
                                ktsGridAlert('검차내용이 저장되었습니다.', 2000);
                            }
                        }
                        else {
                            ktsGridAlert(item.ERRMSG);
                        }
                    }
                });
            }

            callGb = '';
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                $("#<%=txbTeam.ClientID %>").val(json);
            }
        }

        function fn_Search() {
            var cargbn = $("#<%= ddlCarGbn.ClientID %> option:selected").val();

            //컬럼 세팅
            if (cargbn == "") {
                ktsColumns = Columns1;
            }
            else {
                ktsColumns = Columns2;
            }

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값

            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStDt.ClientID %>").val();
            aData[1] = $("#<%= txbEnDt.ClientID %>").val();
            aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
            aData[3] = $("#<%= hfEmpNo.ClientID %>").val();
            aData[4] = $("#<%= ddlCarGbn.ClientID %> option:selected").val();
            aData[5] = $("#<%= txbCarNo.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function searchBtnClick() {
            $("#btnSelect").click();
        }

        function PopupOrgTree(orgcd, empno) {

            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                alert("검차일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: BaseMonth,
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        function SetOrgCd(orgcd, chk) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;
            aData[1] = $("#<%= txbEnDt.ClientID %>").val().replace("-", "").substring(0, 6);

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%=hfEmpNo.ClientID %>").val(empno);
            $("#<%=txbEmpNm.ClientID %>").val(empnm);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>    
                <span class="inpbox first" style="margin-right:20px">
					<label>검차일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfOrgCd" runat="server"/>
				</span>
                <span class="inpbox" style="margin-right:20px">
					<label>이름</label>
                    <asp:TextBox ID="txbEmpNm" runat="server" Width="100px" onclick="PopupEmpUser();" ReadOnly="true"></asp:TextBox>
                    <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfEmpNo" runat="server" />
                </span>
                <span class="optionbox">
					<label>차량유형</label>
                    <asp:DropDownList ID="ddlCarGbn" runat="server" AutoPostBack="false" style="width:120px">
                        <asp:ListItem Value="" Text="일반" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="TOWERWAGON" Text="고소차"></asp:ListItem>
                    </asp:DropDownList>
                    <input type="hidden" id="hfCarType" value="" />
				</span>
                <span class="inpbox">
					<label>차량번호</label>
                    <asp:TextBox ID="txbCarNo" runat="server" Width="100px"></asp:TextBox>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>검차이력조회</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search" style="display:none">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="ORGFULLNM">소속</option>
                            <option value="EMPNO">사번</option>
                            <option value="KTSEMPNO">KTS사번</option>
                            <option value="EMPNM">성명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
