﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestChangeCarInfo.aspx.cs" Inherits="KTSSolutionWeb.RequestChangeCarInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>차량정보 변경요청</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= ddlCarNo.ClientID %>").change(function (e) {
                fn_CarNoChange();
            });

            $("#btnRequest").click(function () {
                btnRequestCheck();
            });
        });

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (callGb == 'GetCarInfo') {
                        fn_SetCarInfo(json);
                    }
                    else if (callGb == 'GetCardInfo') {
                        fn_GetCardInfo(json);
                    }
                    else {
                        if (item.ERRCD == '00') {
                            if (callGb == 'ReqOk') {
                                alert('차량정보 변경 요청이 완료되었습니다.');
                                $("#<%= ddlCarNo.ClientID %>").val('');
                                fn_CarNoChange();
                            }
                        }
                        else {
                            alert(item.ERRMSG);
                        }
                    }
                });
            }

            callGb = '';
        };

        function fn_SetCarInfo(json) {
            if (JSON.parse(json).length > 0) {
                $.each(JSON.parse(json)[0], function (key, value) {
                    switch (key) {
                        case 'MGMTTYPE':
                            $("#<%= txbMgmtType.ClientID %>").val(value);
                            break
                        case 'MGMTSTATUS':
                            $("#<%= txbMgmtStatus.ClientID %>").val(value);
                            break
                        case 'CARTYPE':
                            $("#<%= hdfCarType.ClientID %>").val(value);
                            break
                        case 'CARTYPENM':
                            $("#<%= txbCarTypeNm.ClientID %>").val(value);
                            break
                        case 'POSSESSIONTYPENM':
                            $("#<%= txbPosseionType .ClientID %>").val(value);
                            break
                        case 'MODELNM':
                            $("#<%= txbModelNm.ClientID %>").val(value);
                            break
                        case 'CARPRICE':
                            $("#<%= txbCarPrice.ClientID %>").val(value);
                            break
                        case 'RENTALPRICE':
                            $("#<%= txbRentalPrice.ClientID %>").val(value);
                            break
                        case 'CARORGCD':
                            $("#<%= hdfCarOrgCd.ClientID %>").val(value);
                            break
                        case 'CARORGNM':
                            $("#<%= txbTeam.ClientID %>").val(value);
                            break
                        case 'ORGCD':
                            $("#<%= hdfOrgCd.ClientID %>").val(value);
                            break
                        case 'EMPNO':
                            $("#<%= hdfEmpNo.ClientID %>").val(value);
                            break
                        case 'EMPNM':
                            $("#<%= txbEmpUserNm.ClientID %>").val(value);
                            break
                        case 'EMP_AGE':
                            $("#<%= txbEmpAge.ClientID %>").val(value);
                            break
                        case 'INSURANCE_AGE':
                            $("#<%= ddlInsuranceAge.ClientID %>").val(value);
                            break
                        case 'PENALTY':
                            $("#<%= ddlPenalty.ClientID %>").val(value);
                            break
                        case 'FUELCARD':
                            $("#<%= txbFuel.ClientID %>").val(value);
                            break
                        case 'CHARGECARD':
                            $("#<%= txbCharge.ClientID %>").val(value);
                            break
                        case 'HIPASSCARD':
                            $("#<%= txbHipass.ClientID %>").val(value);
                            break
                        case 'NOTICE':
                            $("#<%= txbNotice.ClientID %>").val(value);
                            break
                        default:
                            break
                    }
                });
            }
        }

        function fn_GetCardInfo(json) {
            if (JSON.parse(json).length > 0) {
                $.each(JSON.parse(json)[0], function (key, value) {
                    switch (key) {
                        case 'ORGCD':
                            $("#<%= hdfOrgCd.UniqueID %>").val(value);
                            break
                        case 'EMPNM':
                            $("#<%= txbEmpUserNm.UniqueID %>").val(value);
                            break
                        case 'FUELCARD':
                            $("#<%= txbFuel.UniqueID %>").val(value);
                            break
                        case 'CHARGECARD':
                            $("#<%= txbCharge.UniqueID %>").val(value);
                            break
                        case 'EMP_AGE':
                            $("#<%= txbEmpAge.UniqueID %>").val(value);
                            if (value == "") {
                                $("#<%= txbEmpAge.UniqueID %>").removeAttr('readonly');
                            }
                            else {

                                $("#<%= txbEmpAge.UniqueID %>").attr('readonly', 'true');
                            }
                            break
                        default:
                            break
                    }
                });
            }
        }

        function fn_ResetCarInfo() {
            $("#<%= txbMgmtType.ClientID %>").val('');
            $("#<%= txbMgmtStatus.ClientID %>").val('');
            $("#<%= hdfCarType.ClientID %>").val('');
            $("#<%= txbCarTypeNm.ClientID %>").val('');
            $("#<%= txbPosseionType .ClientID %>").val('');
            $("#<%= txbModelNm.ClientID %>").val('');
            $("#<%= txbCarPrice.ClientID %>").val('');
            $("#<%= txbRentalPrice.ClientID %>").val('');            
            $("#<%= hdfCarOrgCd.ClientID %>").val('');
            $("#<%= hdfOrgCd.ClientID %>").val('');
            $("#<%= hdfEmpNo.ClientID %>").val('');
            $("#<%= txbTeam.ClientID %>").val('');
            $("#<%= txbEmpUserNm.ClientID %>").val('');
            $("#<%= txbEmpAge.ClientID %>").val('');
            $("#<%= txbEmpAge.UniqueID %>").removeAttr('readonly');
            $("#<%= ddlInsuranceAge.ClientID %>").val('');
            $("#<%= ddlPenalty.ClientID %>").val("무");
            $("#<%= txbFuel.ClientID %>").val('');
            $("#<%= txbCharge.ClientID %>").val('');
            $("#<%= txbHipass.ClientID %>").val('');
            $("#<%= txbNotice.ClientID %>").val('');
        }

        function fn_CarNoChange() {
            var carId = $("#<%= ddlCarNo.ClientID %> option:selected").val();

            if (carId == '') {
                fn_ResetCarInfo();
            }
            else {
                var aData = [];
                aData[0] = carId;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCarInfo');
                callGb = 'GetCarInfo';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        function PopupEmpUser() {
            var carorgcd = $("#<%= hdfCarOrgCd.ClientID %>").val();

            if (carorgcd == "") {
                alert("차량소속 부터 선택해주세요.");
            } else {
                var nWidth = 700;
                var nHeight = 520;

                var param = {};

                var form = "/Car/Search_EmpUser";
                var target = "Search_EmpUser";

                var param = {
                    pCARORGCD: carorgcd
                };

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        function PopupCard(cardtype) {

            var nWidth = 950;
            var nHeight = 600;

            var form;
            var target;

            var param;

            var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();

            if (carid == "" || carid == undefined) {
                alert("수정할 차량을 먼저 선택해 주세요.");
            }
            else {
                if (cardtype == "H") {
                    form = "/Car/HipassCardInfoPopup";
                    target = "HipassCardInfoPopup";

                    param = {
                        pPARENTTYPE: "FORM"
                    };

                    cmPostPopup(form, target, param, nWidth, nHeight);

                }
                else {
                    var empno = $("#<%= hdfEmpNo.ClientID %>").val();
                    var mgmttype = $("#<%= txbMgmtType.ClientID %>").val();

                    form = "/Car/FuelCardInfoPopup";
                    target = "FuelCardInfoPopup";

                    param = {
                        pCARDTYPE: cardtype,
                        pPARENTTYPE: "FORM",
                        pMGMTTYPE: mgmttype
                    };

                    cmPostPopup(form, target, param, nWidth, nHeight);
                }
            }
        }

        function SetGridCardNo(carid, cardno, cardtype) {

            if (cardtype == "F") {
                $("#txbFuel").val(cardno);
            }
            else if (cardtype == "C") {
                $("#txbCharge").val(cardno);
            }
            else if (cardtype == "H") {
                $("#txbHipass").val(cardno);
            }
        }

        function SetGridEmpUser(rowid, ktsempno, empno, empnm, orgnm, empage){
            $("#<%=hdfEmpNo.ClientID %>").val(empno);
            $("#<%=txbFuel.ClientID %>").val("");
            $("#<%=txbCharge.ClientID %>").val("");
            
            if (empno == "") {
                $("#<%=hdfOrgCd.ClientID %>").val("");
                $("#<%=txbEmpUserNm.ClientID %>").val("");
                $("#<%=txbEmpAge.ClientID %>").val("");
                $("#<%= txbEmpAge.UniqueID %>").removeAttr('readonly');
            }
            else {
                var cartype = $("#<%= hdfCarType.ClientID %>").val();

                var aData = [];
            
                aData[0] = cartype;
                aData[1] = empno;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCardInfo');
                callGb = 'GetCardInfo';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        function btnRequestCheck() {
            if (confirm("차량정보 변경 요청 하시겠습니까?")) {
                fn_Requset();
            }
        }

        function fn_Requset() {

            var chk = true;

            var carid = $("#<%= ddlCarNo.ClientID %> option:selected").val();
            var empno = $("#<%= hdfEmpNo.ClientID %>").val();

            var empage = $("#<%= txbEmpAge.ClientID %>").val().replace('만', '').replace('세', '');
            var penalty = $("#<%= ddlPenalty.ClientID %> option:selected").val();
            var fuelcard = $("#<%= txbFuel.ClientID %>").val();
            var chargecard = $("#<%= txbCharge.ClientID %>").val();
            var hipasscard = $("#<%= txbHipass.ClientID %>").val();
            var noice = $("#<%= txbNotice.ClientID %>").val();

            var insage = $("#<%= ddlInsuranceAge.ClientID %> option:selected").val().replace('만', '').replace('세', '');

            if (carid == "" || carid == undefined) {
                alert("변경 요청할 차량을 선택해주세요.");
                chk = false;
            }
            else if (insage == '' || insage == undefined) {
                alert("보험나이를 선택해 주세요.");
                chk = false;
            }
            else if (empno != '' && empno != undefined) {
                if (empage == '' || empage == undefined) {
                    alert("만나이를 입력해 주세요.");
                    chk = false;
                }
                else if (empage < insage) {
                    alert("보험나이는 만나이보다 작거나 같아야 합니다.");
                    chk = false;
                }
            }


            if(chk) {
                var aData = [];

                aData[0] = carid;
                aData[1] = empno;
                aData[2] = empage;
                aData[3] = insage;
                aData[4] = penalty;
                aData[5] = fuelcard;
                aData[6] = chargecard;
                aData[7] = hipasscard;
                aData[8] = noice;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'ReqCarInfo');
                callGb = 'ReqOk';

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>차량정보 변경</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
            
                    <!-- S:popcontents -->
                    <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr>
                                            <th style="width:130px">차량번호</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="optionbox" style="width:205px;">
                                                    <asp:DropDownList ID="ddlCarNo" runat="server" AutoPostBack="false" TabIndex="1"></asp:DropDownList>
                                                </p>
                                                <asp:HiddenField ID="hdfCarId" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">공용/개인</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbMgmtType" runat="server" Width="200px" MaxLength="20" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th style="width:100px">운용관리상태</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbMgmtStatus" runat="server" Width="200px" MaxLength="20" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">차종</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCarTypeNm" runat="server" Width="200px" MaxLength="20" ReadOnly="true"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfCarType" runat="server" />
                                                </p>
                                            </td>
                                            <th style="width:100px">임대여부</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbPosseionType" runat="server" Width="200px" MaxLength="20" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">차명(모델)</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbModelNm" runat="server" Width="200px" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">차량가</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbCarPrice" runat="server" Width="200px" MaxLength="11" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                            <th style="width:100px">월임대료</th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbRentalPrice" runat="server" Width="200px" MaxLength="11" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">차량소속</th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:HiddenField ID="hdfCarOrgCd" runat="server"/>
                                                    <asp:TextBox ID="txbTeam" runat="server" Width="500px" ReadOnly="true"></asp:TextBox>
                                                </p>
                                            </td>

                                        </tr>
                                        <tr>
                                            <th style="width:100px">차량담당자&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:HiddenField ID="hdfOrgCd" runat="server"/>
                                                    <asp:HiddenField ID="hdfEmpNo" runat="server" />
                                                    <asp:TextBox ID="txbEmpUserNm" runat="server" Width="500px" style="margin-right:10px" onclick="PopupEmpUser();" ReadOnly="true" TabIndex="2"></asp:TextBox>
                                                </p>
                                                <button id="btnEmpUser" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">보험나이</th>
                                            <td style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlInsuranceAge" runat="server" Width="200px" TabIndex="2">
                                                        <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="만21세" Value="만21세"></asp:ListItem>
                                                        <asp:ListItem Text="만24세" Value="만24세"></asp:ListItem>
                                                        <asp:ListItem Text="만26세" Value="만26세"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                            <th style="width:100px">만나이&nbsp;<span style="color:red;">*</span></th>
                                            <td style="text-align:left">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbEmpAge" runat="server" Width="200px" MaxLength="10"  TabIndex="3"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">과태료유무&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="optionbox">
                                                    <asp:DropDownList ID="ddlPenalty" runat="server" style="width:100px;" TabIndex="4">
                                                        <asp:ListItem Text="무" Value="무" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="유" Value="유"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">주유카드&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:TextBox ID="txbFuel" runat="server" Width="500px" style="margin-right:10px" onclick="PopupCard('F');" ReadOnly="true" TabIndex="5"></asp:TextBox>
                                                </p>
                                                <button id="btnFuel" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupCard('F');">+</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">충전카드&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:TextBox ID="txbCharge" runat="server" Width="500px" style="margin-right:10px" onclick="PopupCard('C');" ReadOnly="true" TabIndex="6"></asp:TextBox>
                                                </p>
                                                <button id="btnCharge" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupCard('C');">+</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">Hipass카드&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:TextBox ID="txbHipass" runat="server" Width="500px" style="margin-right:10px" onclick="PopupCard('H');" ReadOnly="true" TabIndex="7"></asp:TextBox>
                                                </p>
                                                <button id="btnHipass" runat="server" type="button" class="btn-plus" title="검색" onclick="javascript:PopupCard('H');">+</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:100px">공지&nbsp;<span style="color:red;">*</span></th>
                                            <td colspan="3" style="text-align:left">
                                                <p class="inpbox" style="width:510px;">
                                                    <asp:TextBox ID="txbNotice" runat="server" Width="500px" style="margin-right:10px" TabIndex="8"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->

                        <!-- S:btncenter -->
                        <span style="color:red;text-align:center;">*</span>&nbsp;항목만 변경요청 가능합니다.
                        <div class="btncenter">
                            <input id="btnRequest" type="button" class="btn-green last" style="float:right;" value="변경요청" />
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
