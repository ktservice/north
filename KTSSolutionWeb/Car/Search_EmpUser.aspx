﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search_EmpUser.aspx.cs" Inherits="KTSSolutionWeb.Search_EmpUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>사용자검색</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            var fn_Choicebutton = function (cell) {
                var rowData = cell.getRow().getData();

                var ktsempno = rowData.KTSEMPNO;
                var empno = rowData.EMPNO;
                var empnm = rowData.EMPNM;
                var jobnm = rowData.JOBNM;
                var orgnm = rowData.ORGNM;
                var empage = rowData.EMPAGE;

                var input = "<input id='btnCell' type='button' onclick='SendParentsForm(\"" + ktsempno + "\", \"" + empno + "\", \"" + empnm + "\", \"" + jobnm + "\", \"" + orgnm + "\", \"" + empage + "\");' value='선택' class='btn-gray' style='height:23px;line-height:0px;font-size:15px;'>";

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", sorter: "string", width: 400},
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "나이", field: "EMPAGE", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "선택", field: "CHOICE", width: 120, formatter: fn_Choicebutton }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            var grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("368px");

            $("#btnSelect").click(function (e) {
                var EmpNm = $("#<%= txbEmpNm.ClientID %>").val();

                if (EmpNm.length == 0) {
                    alert("이름을 입력해주세요.");
                    return false;
                } else {
                //입력 파라미터
                    var aData = [];
                    aData[0] = EmpNm;
                    aData[1] = $("#<%= hfCarOrgCd.ClientID %>").val();
                    aData[2] = $("#<%= hfType.ClientID %>").val();

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetData');

                    //grid Bind
                    grid.getDataBind(jsonData, ajaxUrl);
                }
            });

            $("#btnClear").click(function (e) {
                SendParentsForm("", "", "", "", "", "");
            });

        });

        function SendParentsForm(ktsempno, empno, empnm, jobnm, orgnm, empage) {
            var carid = $("#<%= hfCarId.ClientID %>").val();
            var rowid = $("#<%= hfRowId.ClientID %>").val();
            var typechk = $("#<%= hfType.ClientID %>").val();

            if (carid.length != 0) {
                opener.SetGridEmpUser(carid, empno, empnm, jobnm, orgnm, empage);
            }
            else if (typechk == 'ALL') {
                opener.SetEmpUser(rowid, ktsempno, empno, empnm, orgnm, empage);
            }
            else if (typechk == 'ALLGRID') {
                opener.SetGridEmpUser(rowid, ktsempno, empno, empnm, orgnm, empage);
            }
            else {
                opener.SetGridEmpUser(rowid, ktsempno, empno, empnm, orgnm, empage);
            }

            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">사원검색</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
                                <span class="inpbox" style="padding-right:20px">
                                    <label>이름</label>
                                    <asp:TextBox ID="txbEmpNm" runat="server" MaxLength="50" Width="120px" TabIndex="1"></asp:TextBox>
                                </span>
				                <div class="btnset" style="float:right; padding-right:20px;">
                                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                                    <input id="btnClear" type="button" class="btn-black last" value="선택해제" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:HiddenField ID="hfCarId" runat="server" />
        <asp:HiddenField ID="hfCarOrgCd" runat="server" />
        <asp:HiddenField ID="hfRowId" runat="server" />
        <asp:HiddenField ID="hfType" runat="server" />
    </form>
</body>
</html>
