﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Web.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class ResvCarCheckList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPageInit();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetPageInit()
        {
            DataSet ds = new DataSet();

            try
            {
                this.txbStDt.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM-dd");

                if (this.Session["AUTHID"].ToString().Equals("AUTH0001") || this.Session["AUTHID"].ToString().Equals("AUTH0002")
                    || this.Session["AUTHID"].ToString().Equals("AUTH0003") || this.Session["AUTHID"].ToString().Equals("AUTH0004"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnEmpUser.Visible = true;

                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");
                    this.btnOrgCd.Attributes.Add("onClick", "PopupOrgTree('', '');");

                    this.txbEmpNm.Attributes.Add("onClick", "PopupEmpUser();");
                    this.btnEmpUser.Attributes.Add("onClick", "PopupEmpUser();");

                    this.txbEmpNm.Text = "";
                    this.hfEmpNo.Value = "";
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnEmpUser.Visible = false;

                    this.txbTeam.Attributes.Remove("onClick");
                    this.btnOrgCd.Attributes.Remove("onClick");

                    this.txbEmpNm.Attributes.Remove("onClick");
                    this.btnEmpUser.Attributes.Remove("onClick");

                    this.txbEmpNm.Text = Session["EMPNM"].ToString();
                    this.hfEmpNo.Value = Session["EMPNO"].ToString();
                }

                string strMonth = this.txbEnDt.Text.Replace("-", "").Substring(0, 6);

                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }

                this.hfOrgCd.Value = Session["ORGCD"].ToString();
                this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, ds.Tables[0]);
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;

            string strMonth = aData[1];

            using (OrgCodeTree org = new OrgCodeTree())
            {
                ds = org.GetOperOrgMonthList("", "N", strMonth);
            }

            result = Utility.GetOrgNm(aData[0], ds.Tables[0]);

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "RESVCARCHECKINFOHIST";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}