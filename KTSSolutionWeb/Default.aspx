﻿<%@ Page Language="C#" MasterPageFile="~/SiteMain.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="KTSSolutionWeb.Default" EnableEventValidation="false" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabnav li a{position:relative; display:block; margin-left:10px; color: #9a9fb2; font-size: 16px; text-decoration:none;}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_SearchNoti();
            },
            eventbind: function () {
                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    var boardType = $(this).attr("id").replace("tab", "");

                    $("#<%= hdfBoardType.ClientID %>").val(boardType);
                    fn_SearchBoard();

                }).filter(':eq(0)').click();
            }
        };

        var fn_SearchNoti = function () {
            $("#<%= btnSelectNoti.ClientID %>").click();
        };

        var fn_SearchBoard = function () {
            $("#<%= btnSelectBoard.ClientID %>").click();
        };

        function NoticeView(noticeid, writerid) {
            var nWidth = 600;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pNOTICEID: noticeid,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/NoticeView", param);

            Popupform.target = "NoticeView";
            var win = window.open("", "NoticeView", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function BoardView(boardid, writerid) {
            var nWidth = 600;
            var nHeight = 900;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pBOARDID: boardid,
                pWRITERID: writerid
            };

            var Popupform = createForm("/Common/BoardView", param);

            Popupform.target = "BoardView";
            var win = window.open("", "BoardView", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PageRefresh() {
            fn_SearchNoti();
            fn_SearchBoard();
        }
    </script>
    <!-- S: contentsarea -->
    <div class="contentsarea main">
        <div class="main-banner"></div>
        <div class="table-wrapper">
            <asp:Button id="btnSelectNoti" runat="server" OnClick="btnSelectNoti_Click" class="btn-green" style="display:none" Text="조회" />
            <asp:Button id="btnSelectBoard" runat="server" OnClick="btnSelectBoard_Click" class="btn-green" style="display:none" Text="조회" />
            <asp:HiddenField ID="hdfBoardType" runat="server" Value="1" />
            <div class="table-notice">
                <div style="height:60px">
                    <div style="width:100px;padding: 16px 0 8px;float:left">
                        <div style="font-size: 18px;font-weight:bold">공지사항</div>
                    </div>
                </div>
                <asp:UpdatePanel ID="updPanelNoti" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="boardlist" style="min-height:252px">
                            <table>
                                <colgroup>
                                    <col style="width:70px">
                                    <col style="width:auto">
                                    <col style="width:140px">
                                </colgroup>
                                <tbody>
                                    <asp:Repeater ID="rptResultNoti" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="number"><%# Eval("NUM") %></td>
                                                <td class="list-title">
                                                    <asp:Label ID="lblNoticeID" runat="server" Visible="false" Text='<%# Eval("NOTICEID") %>'></asp:Label>
                                                    <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                                    <a id="aTitle" runat="server" style="cursor:pointer"><%# Eval("TITLE") %></a>
                                                </td>
                                                <td class="date"><%# Eval("WRITEDT") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>                
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="pagingNoti" />
                        <asp:AsyncPostBackTrigger ControlID="btnSelectNoti" EventName="Click"/>
                    </Triggers>
                </asp:UpdatePanel>
                <uc:paging ID="pagingNoti" runat="server" OnPreRender="pagingNoti_PreRender" />
            </div> 
            <div class="table-tech">
                <div style="height:60px">
                    <div style="width:100px;padding: 16px 0 8px;float:left">
                        <div style="font-size: 18px;font-weight:bold">기술공유</div>
                    </div>
                    <div style="padding: 10px 0 8px;float: left">
                        <ul id="tabnav" runat="server" class="tabnav">
                        </ul>
                    </div>
                </div>
                <asp:UpdatePanel ID="updPanelBoard" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="boardlist" style="min-height:252px">
                            <table>
                                <colgroup>
                                    <col style="width:70px">
                                    <col style="width:auto">
                                    <col style="width:140px">
                                </colgroup>
                                <tbody>
                                    <asp:Repeater ID="rptResultBoard" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="number"><%# Eval("NUM") %></td>
                                                <td class="list-title">
                                                    <asp:Label ID="lblBoardID" runat="server" Visible="false" Text='<%# Eval("BOARDID") %>'></asp:Label>
                                                    <asp:Label ID="lblBoardType" runat="server" Visible="false" Text='<%# Eval("BOARDTYPE") %>'></asp:Label>
                                                    <asp:Label ID="lblWriterID" runat="server" Visible="false" Text='<%# Eval("WRITERID") %>'></asp:Label>
                                                    <a id="aTitle" runat="server" style="cursor:pointer"><%# Eval("TITLE") %></a>
                                                </td>
                                                <td class="date"><%# Eval("WRITEDT") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="pagingBoard" />
                        <asp:AsyncPostBackTrigger ControlID="btnSelectBoard" EventName="Click"/>
                    </Triggers>
                </asp:UpdatePanel>
                <uc:paging ID="pagingBoard" runat="server" OnPreRender="pagingBoard_PreRender" />
            </div>    
        </div>
    </div>
</asp:Content>
