﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BCompanyState.aspx.cs" Inherits="KTSSolutionWeb.BCompanyState" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        window.onload = setChartWidth;

        function setChartWidth() {
            var width = document.documentElement.clientWidth - 180;
            $("#<%= hfChartWidth.ClientID %>").val(width);
        }

        function PopupOrgTree(orgcd, empno) {

            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pCHKNODELV: "0",
                    pMULTICHK: "Y",
                    pOPER: "N",
                    pMDATE: SumupMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_BComp", param);

                Popupform.target = "OrgTree_BComp";
                var win = window.open("", "OrgTree_BComp", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function PopupBCompanyDetail(orgcd, orgnm, sumupmonth, cnt, worktype) {

            if (cnt != '0' && cnt != '0.00') {
                var param = {
                    pORGCD: orgcd,
                    pORGNM: orgnm,
                    pSUMUPMONTH: sumupmonth,
                    pWORKTYPE: worktype
                };

                var nWidth = 1200;
                var nHeight = 760;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var Popupform = createForm("/Pay/BCompanyDetail", param);

                Popupform.target = "PopupDetail";
                var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
            else {
                return;
            }
        }

        function SearchChk() {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();
            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SetOrgCode(orgcd) {

            this.focus();

            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelSearch)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>
                <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <span class="optionbox" style="margin-right:20px;">
                            <label>조회일자</label>
                            <asp:DropDownList ID="ddlSumupMonth" runat="server" Width="120px" OnSelectedIndexChanged="ddlSumupMonth_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
			            </span>
				        <span class="inpbox" style="margin-right:10px;">
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None"/>
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
				        </span>
                        <span class="optionbox">
                            <label>조건</label>
                            <asp:DropDownList ID="ddlValType" runat="server" Width="100px">
                                <asp:ListItem Text="전체" Value="ALL" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="개통" Value="SALES_OPEN"></asp:ListItem>
                                <asp:ListItem Text="AS" Value="SALES_AS"></asp:ListItem>
                                <asp:ListItem Text="보정" Value="SALES_REVICE"></asp:ListItem>
                                <asp:ListItem Text="단말" Value="SALES_TERMINAL"></asp:ListItem>
                                <asp:ListItem Text="공사" Value="SALES_CONST"></asp:ListItem>
                                <asp:ListItem Text="도서" Value="SALES_ISLAND"></asp:ListItem>
                                <asp:ListItem Text="기가아이즈" Value="SALES_GEYE"></asp:ListItem>
                            </asp:DropDownList>
			            </span>
                        <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlSumupMonth" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
		    </fieldset>
        </div>
        <br />
		<!-- //E:searchbox -->        
                
		<!-- S:datalist -->
        <div class="chartbox">
            <div style="width:100%;height:420px;overflow-x:hidden;overflow-y:hidden;"> <!-- 차트 사이즈 조정 -->
                <asp:HiddenField ID="hfChartWidth" runat="server" />
                <asp:UpdatePanel ID="updPanelChart1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:chart id="chart1" runat="server" Height="420px">
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                    <AxisX>
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineColor="#000" LineDashStyle="Solid" />
                                    </AxisX>
                                    <AxisY IsLabelAutoFit="False" IsMarginVisible="False" LabelAutoFitStyle="None">
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineDashStyle="NotSet"/>
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <Legends>
                                <asp:Legend Name="Legend1"></asp:Legend>
                            </Legends>
                        </asp:chart>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
		<!-- //E:datalist -->

		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>B협력사 정산 결과 조회</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
                </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th rowspan="2">No.</th>
                                    <th rowspan="2">조직명</th>
                                    <th rowspan="2" style="border-right:1px solid #dfdfdf;">일자</th>
                                    <th colspan="2" style="border-right:1px solid #dfdfdf;">합계</th>
                                    <th colspan="2" style="border-right:1px solid #dfdfdf;">개통</th>
                                    <th colspan="2" style="border-right:1px solid #dfdfdf;">AS</th>
                                    <th colspan="2" style="border-right:1px solid #dfdfdf;">보정</th>
                                    <th colspan="2" style="border-right:1px solid #dfdfdf;">단말</th>
                                    <th colspan="2" style="border-right:1px solid #dfdfdf;">공사</th>
                                    <th colspan="2" style="border-right:1px solid #dfdfdf;">도서</th>
                                    <th colspan="2">기가아이즈</th>
                                </tr>
                                <tr>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                    <th>공사</th>
                                    <th style="border-right:1px solid #dfdfdf;">정산</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr id="tr" runat="server">
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td><%# Eval("SUMUPMONTH") %></td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_TOTAL")%>', '');"><%# Eval("SALES_TOTAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_TOTAL")%>', '');"><%# Eval("ALLOWANCE_TOTAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_OPEN")%>', '개통');"><%# Eval("SALES_OPEN") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_OPEN")%>', '개통');"><%# Eval("ALLOWANCE_OPEN") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_AS")%>', 'AS');"><%# Eval("SALES_AS") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_AS")%>', 'AS');"><%# Eval("ALLOWANCE_AS") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_REVICE")%>', '보정');"><%# Eval("SALES_REVICE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_REVICE")%>', '보정');"><%# Eval("ALLOWANCE_REVICE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_TERMINAL")%>', '단말');"><%# Eval("SALES_TERMINAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_TERMINAL")%>', '단말');"><%# Eval("ALLOWANCE_TERMINAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_CONST")%>', '공사');"><%# Eval("SALES_CONST") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_CONST")%>', '공사');"><%# Eval("ALLOWANCE_CONST") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_ISLAND")%>', '도서');"><%# Eval("SALES_ISLAND") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_ISLAND")%>', '도서');"><%# Eval("ALLOWANCE_ISLAND") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("SALES_GEYE")%>', 'GiGAeyes');"><%# Eval("SALES_GEYE") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupBCompanyDetail('<%# Eval("ORGCD")%>', '<%# Eval("ORGNM")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ALLOWANCE_GEYE")%>', 'GiGAeyes');"><%# Eval("ALLOWANCE_GEYE") %></a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
