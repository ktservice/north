﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;
using KTS.KTSSolution.BSL.Pay;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Security;

namespace KTSSolutionWeb
{
    public partial class MaritPayTotalListByEmpState : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetSumupMonth();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");


                    this.btnEmpUser.Visible = true;
                    this.btnEmpUser.Enabled = true;

                    this.btnEmpUser.OnClientClick += "PopupEmpUser();";
                    this.txbEmpNm.Attributes.Add("onClick", "PopupEmpUser();");

                    this.txbEmpNm.Text = "";
                    this.hfEmpNo.Value = "";

                    this.txbTeam.Text = "";
                    this.hfOrgCd.Value = "";

                    this.btnReset.Visible = true;
                    this.btnReset.Enabled = true;
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;

                    this.btnEmpUser.Visible = false;
                    this.btnEmpUser.Enabled = false;

                    this.txbEmpNm.Text = Session["EMPNM"].ToString();
                    this.hfEmpNo.Value = Session["EMPNO"].ToString();

                    this.btnReset.Visible = false;
                    this.btnReset.Enabled = false;
                }

                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetSumupMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (MeritPayMgmt mgmt = new MeritPayMgmt())
                {
                    ds = mgmt.GetSumupMonth();
                }

                ddlSumupMonth.Items.Clear();
                ddlSumupMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlSumupMonth.Items.Add(new ListItem(ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString(), ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hfOrgCd.Value;
                string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");
                string strEmpNo = hfEmpNo.Value;

                if (strSumMonth.Length > 0 && (strOrgCd.Length > 0 || strEmpNo.Length > 0))
                {
                    using (MeritPayMgmt mgmt = new MeritPayMgmt())
                    {
                        if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                        {
                            ds = mgmt.GetMaritPayTotalListByEmp(strOrgCd, strSumMonth, strEmpNo);
                        }
                        else
                        {
                            ds = mgmt.GetMaritPayTotalListByEmp("", strSumMonth, strEmpNo);
                        }
                    }

                    paging.PageNumber = 0;
                    paging.PageSize = 30;

                    paging.Dt = null;

                    if (ds.Tables.Count > 0)
                    {
                        paging.TotalRows = ds.Tables[0].Rows.Count;
                        paging.Dt = ds.Tables[0];
                    }

                    paging.SetPagingDataList();

                    SetChart();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetChart()
        {
            try
            {
                try
                {
                    chart1.Width = Unit.Parse(hfChartWidth.Value + "px");
                }
                catch
                {
                    chart1.Width = Unit.Parse("2000px");
                }

                Color[] colors = { ColorTranslator.FromHtml("#6ad7c4")
                                     , ColorTranslator.FromHtml("#029f88")
                                     , ColorTranslator.FromHtml("#00c0a9")
                                     , ColorTranslator.FromHtml("#ed8a1d")// ee1c25
                                     , ColorTranslator.FromHtml("#d5b911") }; //ed8a1d

            string[] legendText = { "개통", "AS", "수작업", "기가아이즈", "파견" };

                chart1.Legends[0].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;

                for (int j = 0; j < 5; j++)
                {
                    Series series = new Series("series" + j.ToString());
                    series.ChartType = SeriesChartType.StackedColumn;
                    series.BorderWidth = 1;
                    series.Color = colors[j];
                    series.LegendText = legendText[j];
                    series.SmartLabelStyle.Enabled = false;

                    chart1.Series.Add(series);
                }

                for (int i = 1; i < paging.Dt.Rows.Count; i++)
                {

                    double[] data = { double.Parse(paging.Dt.Rows[i]["OPENPOINT"].ToString())
                                    , double.Parse(paging.Dt.Rows[i]["ASPOINT"].ToString())
                                    , double.Parse(paging.Dt.Rows[i]["MANUAL"].ToString())
                                    , double.Parse(paging.Dt.Rows[i]["GEYES"].ToString())
                                    , double.Parse(paging.Dt.Rows[i]["DISPATCH"].ToString()) };

                    string[] tooltip = { "개통 : " + paging.Dt.Rows[i]["OPENPOINT"].ToString()
                                       , "AS : " + paging.Dt.Rows[i]["ASPOINT"].ToString()
                                       , "수작업 : " + paging.Dt.Rows[i]["MANUAL"].ToString()
                                       , "기가아이즈 : " + paging.Dt.Rows[i]["GEYES"].ToString()
                                       , "파견 : " + paging.Dt.Rows[i]["DISPATCH"].ToString() };

                    for (int j = 0; j < 5; j++)
                    {
                        chart1.Series[j].Points.AddY(data[j]);

                        if (data[j] != 0)
                        {
                            chart1.Series[j].Points[i - 1].ToolTip = tooltip[j];
                            chart1.Series[j].Points[i - 1].Label = tooltip[j];
                            chart1.Series[j].Points[i - 1].LabelForeColor = Color.Black;//Color.White;
                            chart1.Series[j].SmartLabelStyle.Enabled = true;
                        }
                    }
                    chart1.Series[0].Points[i - 1].AxisLabel = paging.Dt.Rows[i]["EMPNM"].ToString();
                }

                chart1.ChartAreas[0].AxisX.Interval = 1;
                chart1.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;


                updPanelChart1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[21];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGNM";
            ArrHeader[3] = "EMPNO";
            ArrHeader[4] = "KTSEMPNO";
            ArrHeader[5] = "EMPNM";
            ArrHeader[6] = "SUMUPMONTH";
            ArrHeader[7] = "OPENCNT";
            ArrHeader[8] = "ASCNT";
            ArrHeader[9] = "OPENPOINT";
            ArrHeader[10] = "ASPOINT";
            ArrHeader[11] = "MANUAL";
            ArrHeader[12] = "GEYES";
            ArrHeader[13] = "TERMINAL";
            ArrHeader[14] = "NONMOVE";
            ArrHeader[15] = "DISPATCH";
            ArrHeader[16] = "EVENT1";
            ArrHeader[17] = "EVENT2";
            ArrHeader[18] = "EVENT3";
            ArrHeader[19] = "SUMPOINT";
            ArrHeader[20] = "SUMPRICE";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[21];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "조직코드";
            ArrHeader[2] = "조직명";
            ArrHeader[3] = "사번";
            ArrHeader[4] = "KTS사번";
            ArrHeader[5] = "이름";
            ArrHeader[6] = "일자";
            ArrHeader[7] = "개통건수";
            ArrHeader[8] = "AS건수";
            ArrHeader[9] = "개통 포인트";
            ArrHeader[10] = "AS 포인트";
            ArrHeader[11] = "수작업";
            ArrHeader[12] = "기가아이즈";
            ArrHeader[13] = "단말";
            ArrHeader[14] = "무출동";
            ArrHeader[15] = "파견";
            ArrHeader[16] = "EVENT1";
            ArrHeader[17] = "EVENT2";
            ArrHeader[18] = "EVENT3";
            ArrHeader[19] = "합계";
            ArrHeader[20] = "매출액";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        if (rptResult.Items.Count > 0)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult.Items[0].FindControl("tr");

                            if (paging.PageNumber == 0)
                            {
                                tr.Style.Add("background-color", "#e6e6e6");
                            }
                        }
                    }

                    updPanel1.Update();

                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }


        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {

                    string strOrgCd = this.hfOrgCd.Value;
                    string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");
                    string strEmpNo = hfEmpNo.Value;

                    if (strSumMonth.Length > 0 && (strOrgCd.Length > 0 || strEmpNo.Length > 0))
                    {
                        using (MeritPayMgmt mgmt = new MeritPayMgmt())
                        {
                            if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                            {
                                ds = mgmt.GetMaritPayTotalListByEmp(strOrgCd, strSumMonth, strEmpNo);
                            }
                            else
                            {
                                ds = mgmt.GetMaritPayTotalListByEmp("", strSumMonth, strEmpNo);
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                        return;
                    }
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "실적급 월별 조회 결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    this.txbEmpNm.Text = "";
                    this.hfEmpNo.Value = "";

                    this.txbTeam.Text = "";
                    this.hfOrgCd.Value = "";

                    this.ddlSumupMonth.SelectedIndex = 0;

                    updPanelSearch.Update();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfEmpNo_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    this.txbEmpNm.Text = Request.Form[this.txbEmpNm.UniqueID].ToString();

                    updPanelSearch.Update();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        protected void ddlSumupMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strMonth = ddlSumupMonth.SelectedValue.Replace("-", "");

                this.hfOrgCd.Value = "";
                this.txbTeam.Text = "";

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    if (!strMonth.Equals(""))
                    {
                        SetOrgList(strMonth);
                    }
                }
                else
                {
                    this.hfOrgCd.Value = this.Session["ORGCD"].ToString();
                    this.txbTeam.Text = this.Session["ORGFULLNM"].ToString();
                }
                
                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}