﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TurnKeyDetail.aspx.cs" Inherits="KTSSolutionWeb.TurnKeyDetail" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>턴키 월별 상세조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        
		<!-- S: windowpop-wrap -->
        <div id="windowpop-wrap">    
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">        
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>턴키 월별 상세</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                
                <!-- S:datalist -->
                <div class="datalist">
                    <!-- S:list-top -->
                    <div class="list-top">
                        <div class="pull-right">
                            <div class="btnset" style="margin-right:10px;">
                                <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_ServerClick" class="btn-green" Text="엑셀" /> 
                            </div>'
                        </div>
                    </div>
                    <!-- E:list-top -->
			        <!-- S:scrollbox -->
			        <div class="scrollbox" style="height:500px;overflow-y:auto;">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th rowspan="2">No.</th>
                                            <th rowspan="2">일자</th>
                                            <th rowspan="2">본부</th>
                                            <th rowspan="2">업체명</th>
                                            <th rowspan="2">업체코드</th>
                                            <th rowspan="2">국사명</th>
                                            <th rowspan="2" style="border-right:1px solid #dfdfdf;">국사코드</th>
                                            <th colspan="16" style="border-right:1px solid #dfdfdf;">턴키금액</th>
                                            <th colspan="22" style="border-right:1px solid #dfdfdf;">추가금액</th>
                                            <th rowspan="2">총계</th>
                                        </tr>
                                        <tr>
                                            <th>개통</th>
                                            <th>AS</th>
                                            <th>FM인건비</th>
                                            <th>10G노트북</th>
                                            <th>자재인건비</th>
                                            <th>고소차렌탈비</th>
                                            <th>다온플랜</th>
                                            <th>사무실</th>
                                            <th>자재창고</th>
                                            <th>샤워장</th>
                                            <th>주차비</th>
                                            <th>고객회선<br />관리실</th>
                                            <th>자가개통<br />운영비</th>
                                            <th>특수지역<br />정산</th>
                                            <th>품질관리평가</th>
                                            <th style="border-right:1px solid #dfdfdf;">소계</th>

                                            <th>C-TYPE<br />이너텔</th>
                                            <th>자가망</th>
                                            <th>기가아이즈<br />(개통)</th>
                                            <th>기가아이즈<br />(수리)</th>
                                            <th>수작업정산<br />(개통)</th>
                                            <th>수작업정산<br />(수리)</th>
                                            <th>초고속품질개선비</th>
                                            <th>기가지니<br />앱페어링차감</th>
                                            <th>IOT신상품</th>
                                            <th>핵심고객care서비스</th>
                                            <th>개통</th>
                                            <th>수리</th>
                                            <th>개통/AS</th>
                                            <th>단말관리</th>
                                            <th>기타</th>
                                            <th>SLA개통</th>
                                            <th>SLA수리</th>
                                            <th>개통/AS물자비</th>
                                            <th>모뎀물자비</th>
                                            <th>이벤트</th>
                                            <th>단말회수</th>
                                            <th style="border-right:1px solid #dfdfdf;">소계</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptResult" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("NUM") %></td>
                                                    <td><%# Eval("SUMUPMONTH") %></td>
                                                    <td><%# Eval("ORGLV2NM") %></td>
                                                    <td><%# Eval("COMPANYNM") %></td>
                                                    <td><%# Eval("COMPANYCD") %></td>
                                                    <td><%# Eval("OFFICENM") %></td>
                                                    <td><%# Eval("OFFICECD") %></td>

                                                    <td><%# Eval("OPENPRICE") %></td>
                                                    <td><%# Eval("ASPRICE") %></td>
                                                    <td><%# Eval("FM") %></td>
                                                    <td><%# Eval("LAPTOP10G") %></td>
                                                    <td><%# Eval("MATERIAL") %></td>
                                                    <td><%# Eval("RENTAL") %></td>
                                                    <td><%# Eval("DAONPLAN") %></td>
                                                    <td><%# Eval("OFFICE") %></td>
                                                    <td><%# Eval("MATERIALSTORED") %></td>
                                                    <td><%# Eval("SHOWERROOM") %></td>
                                                    <td><%# Eval("PARKING") %></td>
                                                    <td><%# Eval("CUSTLINE") %></td>
                                                    <td><%# Eval("SELFOPEN") %></td>
                                                    <td><%# Eval("SPECIALPLACECOST") %></td>
                                                    <td><%# Eval("QUALITYMGMT") %></td>
                                                    <td><%# Eval("SUBTOTAL1") %></td>
                                            
                                                    <td><%# Eval("CTYPE") %></td>
                                                    <td><%# Eval("SELFLINE") %></td>
                                                    <td><%# Eval("GEYEOPEN") %></td>
                                                    <td><%# Eval("GEYEAS") %></td>
                                                    <td><%# Eval("MANUALOPEN") %></td>
                                                    <td><%# Eval("MANUALAS") %></td>
                                                    <td><%# Eval("IMPROVE") %></td>
                                                    <td><%# Eval("APP_PAIRRING") %></td>
                                                    <td><%# Eval("IOT") %></td>
                                                    <td><%# Eval("VIPCARESVC") %></td>
                                                    <td><%# Eval("OPENCOST") %></td>
                                                    <td><%# Eval("ASCOST") %></td>
                                                    <td><%# Eval("OPENASCOST") %></td>
                                                    <td><%# Eval("TERMINAL") %></td>
                                                    <td><%# Eval("ETC") %></td>
                                                    <td><%# Eval("SLAOPEN") %></td>
                                                    <td><%# Eval("SLAAS") %></td>
                                                    <td><%# Eval("OPENASMATERIAL") %></td>
                                                    <td><%# Eval("MODEM") %></td>
                                                    <td><%# Eval("EVENTSUM") %></td>
                                                    <td><%# Eval("RETURNTERMINAL") %></td>
                                                    <td><%# Eval("SUBTOTAL2") %></td>
                                                    <td><%# Eval("TOTALPRICE") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="paging" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
			        <!-- E:scrollbox -->
                    <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
                </div>
                <!-- E:datalist -->
            </div>
        </div>
    </form>
</body>
</html>
