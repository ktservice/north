﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserPayCoefficientInfo.aspx.cs" Inherits="KTSSolutionWeb.UserPayCoefficientInfo" %>

<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $("#<%= ddlSumupMonth.ClientID %>").change(function (e) {
                    fn_GetTeam();
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#btnExcel").click(function (e) {
                    ktsGrid.download("xlsx", "구역계수관리.xlsx", { sheetName: "구역계수관리" });
                });
            }
        };

        var fn_GridInit = function () {

            var fn_Input = function (cell) {
                var cellValue = cell.getValue();
                var field = cell.getField();
                var row = cell.getRow();
                var rowData = row.getData();
                var id = rowData.id;
                var orgnm = rowData.ORGFULLNM;

                var UpdateYn = $("#<%= hfUpdateYn.ClientID %>").val();

                var input;

                if (orgnm == "합계" || orgnm == "" || UpdateYn != "Y") {
                    input = cellValue;
                }
                else {
                    var minVal = parseFloat($("#<%= hfMinVal.ClientID %>").val().replace(',', ''));
                    var maxVal = parseFloat($("#<%= hfMaxVal.ClientID %>").val().replace(',', ''));

                    input = document.createElement("input");

                    input.setAttribute("type", "Text");

                    input.style.padding = "4px";
                    input.style.width = "100px";
                    input.style.boxSizing = "border-box";
                    input.setAttribute("maxlength", "7");
                    input.style.height = "100%";

                    input.value = cellValue;

                    var onChange = function () {
                        var val = parseFloat(input.value);

                        if (input.value != cellValue) {
                            if (val > 0) {
                                if ((val < minVal) || val > maxVal) {
                                    alert("구역계수는 최소 " + minVal + " 이상, 최대 " + maxVal + " 이하 여야 합니다.");
                                    input.value = cellValue;
                                }
                                else {
                                    $("#<%= hfCalcResult.ClientID %>").val('N');

                                    if (field == 'OPEN_CONFFICIENT') {
                                        ktsGrid.updateData([{
                                            id: id, OPEN_CONFFICIENT: input.value
                                        }])
                                            .then(function () {
                                                //run code after row
                                            })
                                            .catch(function (error) {
                                                //handle error
                                            });
                                    }
                                    else {
                                        ktsGrid.updateData([{
                                            id: id, AS_CONFFICIENT: input.value
                                        }])
                                            .then(function () {
                                                //run code after row
                                            })
                                            .catch(function (error) {
                                                //handle error
                                            });
                                    }
                                }
                            }
                            else {
                                input.value = cellValue;
                            }
                        } else {
                            input.value = cellValue;
                        }
                    };

                    //submit new value on blur or change
                    input.addEventListener("keypress", function (event) {
                        var val = input.value;
                        const regex = /^[ㄱ-ㅣ가-힣a-zA-Z]+$/;

                        event = event || window.event;
                        var keyid = (event.which) ? event.which : event.keyCode;
                        if ((keyid >= 48 && keyid <= 57) || keyid == 46) {
                            if (keyid == 46) {
                                if (val.indexOf(".") < 1) {
                                    if (regex.test(val)) {
                                        return false;
                                    }
                                    else {
                                        return true;
                                    }
                                }
                                else {
                                    return false;
                                }
                            }
                            else {
                                if (regex.test(val)) {
                                    return false;
                                }
                                else {
                                    return true;
                                }
                            }
                        }
                        else {
                            return false;
                        }
                    });

                    input.addEventListener("blur", onChange);
                }

                return input;
            };

            var fn_Color = function (cell) {
                var value = cell.getValue();

                var row = cell.getRow();
                var rowData = row.getData();

                var orgnm = rowData.ORGFULLNM;

                if (orgnm == "합계") {
                    row.getElement().style.backgroundColor = "#e6e6e6";
                    row.getElement().style.fontWeight = "500";
                }

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", minWidth: 10, maxWidth: 100, sorter: "number", formatter: fn_Color },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGFULLNM", minWidth: 100, maxWidth: 500, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "SUMUPMONTH", minWidth: 60, maxWidth: 200, headerSort: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", minWidth: 60, maxWidth: 200, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", minWidth: 60, maxWidth: 200, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", minWidth: 60, maxWidth: 200, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "조정전 개통", field: "OPENPOINT", minWidth: 100, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "조정전 AS", field: "ASPOINT", minWidth: 100, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "개통 구역개수", field: "OPEN_CONFFICIENT", minWidth: 100, maxWidth: 200, headerSort: false, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "center", title: "AS 구역개수", field: "AS_CONFFICIENT", minWidth: 100, maxWidth: 200, headerSort: false, formatter: fn_Input },
                { headerHozAlign: "center", hozAlign: "center", title: "조정후 개통", field: "OPENPOINT_C", minWidth: 120, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "조정후 AS", field: "ASPOINT_C", minWidth: 120, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "수작업", field: "MANUAL", minWidth: 30, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "파견", field: "DISPATCH", minWidth: 30, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "EVENT1", field: "EVENT1", minWidth: 90, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "EVENT2", field: "EVENT2", minWidth: 90, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "EVENT3", field: "EVENT3", minWidth: 90, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "합계", field: "SUMPOINT", minWidth: 30, maxWidth: 200, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "매출액", field: "SUMPRICE", minWidth: 100, maxWidth: 250, sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", field: "IDMSEMPNO", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        };

        var fn_Load = function () {
        };

        var ajaxCallBack = function (json) {
            if (callGb != "") {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'RegOk') {
                            alert('최종저장을 완료했습니다.');

                            fn_Search();
                        }
                    }
                    else {
                        ktsGridAlert(item.ERRMSG);
                    }
                });

                callGb = '';
            }
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                ktsGrid.setData(json);
                ktsGridRowCount(ktsGrid.getDataCount());

                var UpdateYn = $("#<%= hfUpdateYn.ClientID %>").val();

                if (UpdateYn == "Y") {
                    if (ktsGrid.getDataCount() > 0) {
                        var row = ktsGrid.getRow(ktsGrid.getDataCount());
                        var rowData = row.getData();

                        //var orgnm = rowData.ORGFULLNM;
                        var Open_SUM = 0;
                        var As_SUM = 0;

                        for (var i = 1; i <= ktsGrid.getDataCount(); i++) {
                            var row = ktsGrid.getRow(i);
                            var rowData = row.getData();
                            var Open_P = 0;
                            var As_P = 0;

                            try {
                                Open_P = parseInt(rowData.OPENPOINT.replace(',', ''));
                                As_P = parseInt(rowData.ASPOINT.replace(',', ''));
                            }
                            catch {
                                Open_P = 0;
                                As_P = 0;
                            }

                            var Open_P = parseInt(rowData.OPENPOINT.replace(',', ''));
                            var As_P = parseInt(rowData.ASPOINT.replace(',', ''));

                            Open_SUM += Open_P;
                            As_SUM += As_P;
                        }

                        //if (orgnm == "합계") {
                        $("#<%= txbOpenPoint.ClientID %>").val(Open_SUM);
                        $("#<%= txbAsPoint.ClientID %>").val(As_SUM);
                        //}
                    }
                }
            }
        }

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                var org = json.split('|');

                if (org.length > 5) {
                    $("#<%= hdfOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);

                    if (org[2].length > 0) {
                        $("#<%= hfMinVal.ClientID %>").val(org[2]);
                    }
                    if (org[3].length > 0) {
                        $("#<%= hfMaxVal.ClientID %>").val(org[3]);
                    }
                    if (org[4].length > 0) {
                        $("#<%= hfLimitVal.ClientID %>").val(org[4]);
                    }
                    if (org[5].length > 0) {
                        $("#<%= hfUpdateYn.ClientID %>").val(org[5]);
                    }
                }
            }

            callGb = '';
        }

        var fn_GetTeam = function () {
            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val().replace("-", "");

            if (SumupMonth != '') {
                var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();

                var aData = [];
                aData[0] = OrgCd;
                aData[1] = SumupMonth;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                callGb = 'GetTeam';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hdfOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');
            }
        }

        var fn_Search = function () {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val().replace("-", "");
            var UpdateYn = $("#<%= hfUpdateYn.ClientID %>").val();

            $("#<%= txbOpenPoint.ClientID %>").val('');
            $("#<%= txbAsPoint.ClientID %>").val('');
            $("#<%= txbOpenPointC.ClientID %>").val('');
            $("#<%= txbAsPointC.ClientID %>").val('');
            $("#<%= txbOpenPointC.ClientID %>").css('background-color', '');
            $("#<%= txbAsPointC.ClientID %>").css('background-color', '');

            if (SumupMonth.length == 0) {
                alert("정산일을 선택해주세요.");
                return false;
            }
            else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                //입력 파라미터
                var aData = [];
                aData[0] = OrgCd;
                aData[1] = SumupMonth;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                if (UpdateYn == "Y") {
                    $("#divOpen").show();
                    $("#divAs").show();
                    $("#divBtn").show();
                    $("#divCalYn").hide();

                    $("#<%= hfCalcResult.ClientID %>").val('N');

                    $("#btnCalc").click(function (e) {
                        fn_Calc();
                    });

                    $("#btnReg").click(function (e) {
                        fn_RegChk();
                    });

                    grid.gridDefault('ktsGrid', 'fitData', true, null); // div의 grid id 값
                }
                else {
                    $("#divOpen").hide();
                    $("#divAs").hide();
                    $("#divBtn").hide();

                    $("#btnCalc").off("click");
                    $("#btnReg").off("click");

                    $("#<%= hfCalcResult.ClientID %>").val('');

                    if (UpdateYn == "N") {
                        $("#divCalYn").show();
                    }
                    else {
                        $("#divCalYn").hide();
                    }

                    grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
                }

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
        }

        var fn_Calc = function () {
            var UpdateYn = $("#<%= hfUpdateYn.ClientID %>").val();

            var OpenPoint = $("#<%= txbOpenPoint.ClientID %>").val().replace(',', '');
            var AsPoint = $("#<%= txbAsPoint.ClientID %>").val().replace(',', '');
            var MinVal = $("#<%= hfMinVal.ClientID %>").val().replace(',', '');
            var MaxVal = $("#<%= hfMaxVal.ClientID %>").val().replace(',', '');
            var LimitVal = $("#<%= hfLimitVal.ClientID %>").val().replace(',', '');

            var dOpenPoint = parseFloat(OpenPoint);
            var dAsPoint = parseFloat(AsPoint);
            var dMinVal = parseFloat(MinVal);
            var dMaxVal = parseFloat(MaxVal);
            var dLimitVal = parseFloat(LimitVal);

            if (UpdateYn != 'Y' || dOpenPoint == NaN || dAsPoint == NaN) {
                $("#<%= hfCalcResult.ClientID %>").val('N');
                alert("잘못된 접근입니다.");
            }
            else {
                if (dMinVal == NaN || dMaxVal == NaN || dLimitVal == NaN) {
                    dMinVal = 0;
                    dMaxVal = 0;
                    dLimitVal = 0.5;
                }

                var bCal = true;
                var dOpenMinChk = dOpenPoint - (dOpenPoint * (dLimitVal * 0.01));
                var dOpenMaxChk = dOpenPoint + (dOpenPoint * (dLimitVal * 0.01));

                var dAsMinChk = dAsPoint - (dAsPoint * (dLimitVal * 0.01));
                var dAsMaxChk = dAsPoint + (dAsPoint * (dLimitVal * 0.01));

                var dOpenPoint_C = 0;
                var dAsPoint_C = 0;

                var dataCnt = ktsGrid.getDataCount();

                if (dataCnt > 1) {
                    for (var i = 1; i <= dataCnt; i++) {
                        var row = ktsGrid.getRow(i);
                        var rowData = row.getData();

                        var Open_P = parseFloat(rowData.OPENPOINT.replace(',', ''));
                        var As_P = parseFloat(rowData.ASPOINT.replace(',', ''));
                        var Open_C = parseFloat(rowData.OPEN_CONFFICIENT.replace(',', ''));
                        var As_C = parseFloat(rowData.AS_CONFFICIENT.replace(',', ''));

                        if (Open_C == NaN || As_C == NaN) {
                            bCal = false;
                            $("#<%= hfCalcResult.ClientID %>").val('N');
                            alert("잘못 입력된 구역계수가 있습니다. 확인 후 다시 시도해주세요.");
                            break;
                        }
                        else if (Open_P == NaN || As_P == NaN) {
                            bCal = false;
                            $("#<%= hfCalcResult.ClientID %>").val('N');
                            alert("계산 할수 없는 포인트 값이 있습니다. 확인 후 다시 시도해주세요.");
                            break;
                        }
                        else {
                            if (dMinVal > 0 && dMaxVal > 0) {
                                if ((dMinVal > Open_C) || (dMaxVal < Open_C) || (dMinVal > As_C) || (dMaxVal < As_C)) {
                                    bCal = false;
                                    $("#<%= hfCalcResult.ClientID %>").val('N');
                                    alert('구역계수는 최소 ' + MinVal + ' 이상, 최대 ' + MaxVal + ' 이하 여야 합니다.');
                                    break;
                                }
                            }

                            dOpenPoint_C += Open_P * Open_C;
                            dAsPoint_C += As_P * As_C;
                        }
                    }
                }
                else {
                    alert("조회된 데이터가 없습니다.");
                    $("#<%= hfCalcResult.ClientID %>").val('X');
                }

                if (bCal) {
                    $("#<%= txbOpenPointC.ClientID %>").val(dOpenPoint_C);
                    $("#<%= txbAsPointC.ClientID %>").val(dAsPoint_C);

                    var bOpen = false;
                    var bAs = false;

                    if (dOpenPoint_C >= dOpenMinChk && dOpenPoint_C <= dOpenMaxChk) {
                        bOpen = true;
                        $("#<%= txbOpenPointC.ClientID %>").css('background-color', '#00b3ff');
                    }
                    else {
                        bOpen = false;
                        $("#<%= txbOpenPointC.ClientID %>").css('background-color', 'red');
                    }

                    if (dAsPoint_C >= dAsMinChk && dAsPoint_C <= dAsMaxChk) {
                        bAs = true;
                        $("#<%= txbAsPointC.ClientID %>").css('background-color', '#00b3ff');
                    }
                    else {
                        bAs = false;
                        $("#<%= txbAsPointC.ClientID %>").css('background-color', 'red');
                    }

                    if (bOpen && bAs) {
                        $("#<%= hfCalcResult.ClientID %>").val('T');
                    }
                    else {
                        $("#<%= hfCalcResult.ClientID %>").val('F');
                    }
                }
            }
        }

        var fn_RegChk = function () {
            var CalcResult = $("#<%= hfCalcResult.ClientID %>").val();

            if (CalcResult == "T") {
                fn_RegProc();
            } else if (CalcResult == "N") {
                alert("계산이 되지 않았습니다. 계산을 먼저 해주세요");
            } else if (CalcResult == "F") {
                alert("계산된 포인트가 조정 범위에 포함되지 않습니다.");
            } else if (CalcResult == "X") {
                alert("조회된 데이터가 없습니다.");
            } else {
                alert("잘못된 접근입니다.");
            }
        }

        var fn_RegProc = function () {
            var dataCnt = ktsGrid.getDataCount();
            var aData = [];
            var n = 0;

            callGb = 'RegOk';

            for (var i = 1; i <= dataCnt; i++) {
                var row = ktsGrid.getRow(i);
                var rowData = row.getData();

                var SumupMonth = rowData.SUMUPMONTH;
                var EmpNo = rowData.IDMSEMPNO;

                if (SumupMonth.length > 0 && EmpNo.length > 0) {
                    var aRegData = [];

                    var Open_P = parseFloat(rowData.OPENPOINT.replace(',', ''));
                    var As_P = parseFloat(rowData.ASPOINT.replace(',', ''));
                    var Open_C = parseFloat(rowData.OPEN_CONFFICIENT.replace(',', ''));
                    var As_C = parseFloat(rowData.AS_CONFFICIENT.replace(',', ''));

                    var Open_R = Open_P * Open_C;
                    var As_R = As_P * As_C;

                    aRegData[0] = SumupMonth;
                    aRegData[1] = EmpNo;
                    aRegData[2] = Open_C;
                    aRegData[3] = As_C;
                    aRegData[4] = Open_R;
                    aRegData[5] = As_R;

                    aData[n] = aRegData;

                    n++;
                }
                else {
                    continue;
                }
            }

            if (aData.length > 0) {
                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, "RegData");

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
            else {
                ktsGridAlert('잘못된 접근입니다.', 2000);
            }
        }

        var PopupOrgTree = function (orgcd, empno) {

            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val().replace("-", "");

            if (SumupMonth.length == 0) {
                alert("정산일을 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMDATE: SumupMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();

            $("#<%=hdfOrgCd.ClientID %>").val(orgcd);
            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val().replace("-", "");

            var aData = [];
            aData[0] = orgcd;
            aData[1] = SumupMonth;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
    <div class="pagenavi">
        <ul>
            <li><a id="aMenuTree1" runat="server"></a></li>
            <li class="last"><a id="aMenuTree2" runat="server"></a></li>
            <!-- 현재 페이지에 last클래스 추가 -->
        </ul>
    </div>
    <!--end subTitle-->

    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
            <fieldset>
                <span class="optionbox first">
                    <label>정산일</label>
                    <asp:DropDownList ID="ddlSumupMonth" runat="server" Width="120px" AutoPostBack="false">
                        <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="inpbox">
                    <label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                    <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
                    <asp:HiddenField ID="hdfOrgCd" runat="server" />
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
            </fieldset>
        </div>
        <!-- //E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>구역계수 조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="btnExcel" type="button" class="btn-green" style="float: right;" value="엑셀" />
                    </div>
                </div>
                <div class="pull-right">
                    <div id="divOpen" class="inpbox" style="display: none;">
                        <label>개통포인트</label>
                        <asp:TextBox ID="txbOpenPoint" runat="server" Width="100px" ReadOnly="true" Style="margin-right: 0px"></asp:TextBox>
                        <asp:TextBox ID="txbOpenPointC" runat="server" Width="100px" ReadOnly="true" Style="margin-right: 10px; background-color: white;"></asp:TextBox>
                    </div>
                    <div id="divAs" class="inpbox" style="display: none;">
                        <label>AS포인트</label>
                        <asp:TextBox ID="txbAsPoint" runat="server" Width="100px" ReadOnly="true" Style="margin-right: 0px"></asp:TextBox>
                        <asp:TextBox ID="txbAsPointC" runat="server" Width="100px" ReadOnly="true" Style="background-color: white;"></asp:TextBox>
                        <asp:HiddenField ID="hfCalcResult" runat="server" Value="X" />
                        <asp:HiddenField ID="hfMinVal" runat="server" />
                        <asp:HiddenField ID="hfMaxVal" runat="server" />
                        <asp:HiddenField ID="hfLimitVal" runat="server" />
                        <asp:HiddenField ID="hfUpdateYn" runat="server" />
                    </div>
                    <div id="divBtn" class="btnset" style="display: none; float: right; margin-left: 10px; margin-right: 10px;">
                        <input id="btnCalc" type="button" class="btn-green" value="계산" />
                        <input id="btnReg" type="button" class="btn-green" value="최종저장" />
                    </div>
                    <div id="divCalYn" class="inpbox" style="display: none; margin-top: 8px; margin-right: 20px;">
                        <label>구역계수 수정 마감일이 지났습니다.</label>
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->

            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div id="ktsGrid"></div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
    <!--//E: contentsarea -->
</asp:Content>
