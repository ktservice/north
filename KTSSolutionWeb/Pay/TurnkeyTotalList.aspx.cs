﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;
using KTS.KTSSolution.BSL.Pay;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Security;

namespace KTSSolutionWeb
{
    public partial class TurnkeyTotalList : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private string ORGLV
        {
            get
            {
                if (ViewState["ORGLV"] != null)
                    return ViewState["ORGLV"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGLV"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetSumupMonth();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;
                }

                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strEmpNo, string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperBCompanyOrgMonthList(strEmpNo, strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetSumupMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                {
                    ds = mgmt.GetSumupMonth();
                }

                ddlSumupMonth.Items.Clear();
                ddlSumupMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlSumupMonth.Items.Add(new ListItem(ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString(), ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hfOrgCd.Value;
                string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");

                if (strSumMonth.Length > 0)
                {
                    using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                    {
                        ds = mgmt.GetTurnkeyTotalList(strOrgCd, strSumMonth);
                    }

                    paging.PageNumber = 0;
                    paging.PageSize = 30;

                    paging.Dt = null;

                    if (ds.Tables.Count > 0)
                    {
                        paging.TotalRows = ds.Tables[0].Rows.Count;
                        paging.Dt = ds.Tables[0];

                        ORGLV = ds.Tables[1].Rows[0]["ORGLV"].ToString();
                    }

                    paging.SetPagingDataList();

                    SetChart();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetChart()
        {
            try
            {
                try
                {
                    chart1.Width = Unit.Parse(hfChartWidth.Value + "px");
                }
                catch
                {
                    chart1.Width = Unit.Parse("2000px");
                }

                string strValType = this.ddlValType.SelectedValue;
                string strValName = this.ddlValType.SelectedItem.Text;

                chart1.Legends.Remove(chart1.Legends[0]);

                Series series = new Series("series");
                series.ChartType = SeriesChartType.StackedColumn;
                series.YValueType = ChartValueType.Double;
                series.BorderWidth = 1;
                series.SmartLabelStyle.Enabled = false;

                chart1.Series.Add(series);

                DataRow[] dr = paging.Dt.Select(string.Format("ORGCD = '{0}'", "300000"));

                for (int i = 1; i < paging.Dt.Rows.Count; i++)
                {
                    DataPoint dp = new DataPoint();

                    dp.SetValueY(double.Parse(paging.Dt.Rows[i][strValType].ToString().Replace(", ", "")));
                    dp.AxisLabel = paging.Dt.Rows[i]["ORGNM"].ToString();

                    if (!paging.Dt.Rows[i][strValType].ToString().Equals("0") && !paging.Dt.Rows[i][strValType].ToString().Equals("0.00"))
                    {
                        dp.ToolTip = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                        dp.Label = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                        dp.LabelForeColor = Color.Black;//Color.White;
                        dp.Color = double.Parse(paging.Dt.Rows[i][strValType].ToString()) > 0 ? ColorTranslator.FromHtml("#00c0a9") : ColorTranslator.FromHtml("#ed8a1d");
                    }

                    chart1.Series[0].Points.Add(dp);
                    //if (paging.Dt.Rows[i]["ORGCD"].ToString().Equals("300000"))
                    //{
                    //    string strVal = paging.Dt.Rows[i][strValType].ToString().Replace(", ", "");
                    //    string strCommVal = paging.Dt.Rows[i]["COMM_" + strValType].ToString().Replace(", ", "");

                    //    dp.SetValueY(double.Parse(strVal) - double.Parse(strCommVal));

                    //    dp.AxisLabel = paging.Dt.Rows[i]["ORGNM"].ToString();

                    //    if (!strVal.Equals("0") && !strVal.Equals("0.00"))
                    //    {
                    //        dp.ToolTip = strValName + " : " + string.Format("{0:#,###}", (double.Parse(strVal) - double.Parse(strCommVal)));
                    //        dp.Label = strValName + " : " + string.Format("{0:#,###}", (double.Parse(strVal) - double.Parse(strCommVal)));
                    //        dp.LabelForeColor = Color.Black;//Color.White;
                    //        dp.Color = double.Parse(strVal) > 0 ? ColorTranslator.FromHtml("#00c0a9") : ColorTranslator.FromHtml("#ed8a1d");
                    //    }

                    //    chart1.Series[0].Points.Add(dp);

                    //    dp = new DataPoint();

                    //    dp.SetValueY(double.Parse(strCommVal));

                    //    dp.AxisLabel = "공통비";

                    //    if (!strCommVal.Equals("0") && !strCommVal.Equals("0.00"))
                    //    {
                    //        dp.ToolTip = "공통비" + " : " + string.Format("{0:#,###}", double.Parse(strCommVal));
                    //        dp.Label = "공통비" + " : " + string.Format("{0:#,###}", double.Parse(strCommVal));
                    //        dp.LabelForeColor = Color.Black;//Color.White;
                    //        dp.Color = double.Parse(strCommVal) > 0 ? ColorTranslator.FromHtml("#00c0a9") : ColorTranslator.FromHtml("#ed8a1d");
                    //    }

                    //    chart1.Series[0].Points.Insert(0, dp);
                    //}
                    //else
                    //{
                    //    dp.SetValueY(double.Parse(paging.Dt.Rows[i][strValType].ToString().Replace(", ", "")));
                    //    dp.AxisLabel = paging.Dt.Rows[i]["ORGNM"].ToString();

                    //    if (!paging.Dt.Rows[i][strValType].ToString().Equals("0") && !paging.Dt.Rows[i][strValType].ToString().Equals("0.00"))
                    //    {
                    //        dp.ToolTip = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                    //        dp.Label = strValName + " : " + paging.Dt.Rows[i][strValType].ToString();
                    //        dp.LabelForeColor = Color.Black;//Color.White;
                    //        dp.Color = double.Parse(paging.Dt.Rows[i][strValType].ToString()) > 0 ? ColorTranslator.FromHtml("#00c0a9") : ColorTranslator.FromHtml("#ed8a1d");
                    //    }

                    //    chart1.Series[0].Points.Add(dp);
                    //}
                }

                chart1.ChartAreas[0].AxisX.Interval = 1;
                chart1.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;

                chart1.ChartAreas[0].AxisY.Crossing = 0;


                updPanelChart1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[43];
            
            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGFULLNM";
            ArrHeader[3] = "SUMUPMONTH";
            ArrHeader[4] = "OPENPRICE";
            ArrHeader[5] = "ASPRICE";
            ArrHeader[6] = "FM";
            ArrHeader[7] = "LAPTOP10G";
            ArrHeader[8] = "MATERIAL";
            ArrHeader[9] = "RENTAL";
            ArrHeader[10] = "DAONPLAN";
            ArrHeader[11] = "OFFICE";
            ArrHeader[12] = "MATERIALSTORED";
            ArrHeader[13] = "SHOWERROOM";
            ArrHeader[14] = "PARKING";
            ArrHeader[15] = "CUSTLINE";
            ArrHeader[16] = "SELFOPEN";
            ArrHeader[17] = "SPECIALPLACECOST";
            ArrHeader[18] = "QUALITYMGMT";
            ArrHeader[19] = "SUBTOTAL1";
            
            ArrHeader[20] = "CTYPE";
            ArrHeader[21] = "SELFLINE";
            ArrHeader[22] = "GEYEOPEN";
            ArrHeader[23] = "GEYEAS";
            ArrHeader[24] = "MANUALOPEN";
            ArrHeader[25] = "MANUALAS";
            ArrHeader[26] = "IMPROVE";
            ArrHeader[27] = "APP_PAIRRING";
            ArrHeader[28] = "IOT";
            ArrHeader[29] = "VIPCARESVC";
            ArrHeader[30] = "OPENCOST";
            ArrHeader[31] = "ASCOST";
            ArrHeader[32] = "OPENASCOST";
            ArrHeader[33] = "TERMINAL";
            ArrHeader[34] = "ETC";
            ArrHeader[35] = "SLAOPEN";
            ArrHeader[36] = "SLAAS";
            ArrHeader[37] = "OPENASMATERIAL";
            ArrHeader[38] = "MODEM";
            ArrHeader[39] = "EVENTSUM";
            ArrHeader[40] = "RETURNTERMINAL";
            ArrHeader[41] = "SUBTOTAL2";
            ArrHeader[42] = "TOTALPRICE";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[7];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "조직코드";
            ArrHeader[2] = "조직명";
            ArrHeader[3] = "일자";
            ArrHeader[4] = "턴키금액";
            ArrHeader[5] = "추가금액";
            ArrHeader[6] = "총계";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {
            string[] ArrHeader = new string[43];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "조직코드";
            ArrHeader[2] = "조직명";
            ArrHeader[3] = "일자";

            ArrHeader[4] = "개통";
            ArrHeader[5] = "AS";
            ArrHeader[6] = "FM인건비";
            ArrHeader[7] = "10G노트북";
            ArrHeader[8] = "자재인건비";
            ArrHeader[9] = "고소차렌탈비";
            ArrHeader[10] = "다온플랜";
            ArrHeader[11] = "사무실";
            ArrHeader[12] = "자재창고";
            ArrHeader[13] = "샤워장";
            ArrHeader[14] = "주차비";
            ArrHeader[15] = "고객회선 관리실";
            ArrHeader[16] = "자가개통 운영비";
            ArrHeader[17] = "특수지역정산";
            ArrHeader[18] = "품질관리평가";
            ArrHeader[19] = "소계";

            ArrHeader[20] = "C-TYPE 이너텔";
            ArrHeader[21] = "자가망";
            ArrHeader[22] = "기가아이즈(개통)";
            ArrHeader[23] = "기가아이즈(수리)";
            ArrHeader[24] = "수작업정산(개통)";
            ArrHeader[25] = "수작업정산(수리)";
            ArrHeader[26] = "초고속품질개선비";
            ArrHeader[27] = "기가지니앱페어링차감";
            ArrHeader[28] = "IOT신상품";
            ArrHeader[29] = "핵심고객care서비스";
            ArrHeader[30] = "개통";
            ArrHeader[31] = "수리";
            ArrHeader[32] = "개통/AS";
            ArrHeader[33] = "단말관리";
            ArrHeader[34] = "기타";
            ArrHeader[35] = "SLA개통";
            ArrHeader[36] = "SLA수리";
            ArrHeader[37] = "개통/AS물자비";
            ArrHeader[38] = "모뎀물자비";
            ArrHeader[39] = "이벤트";
            ArrHeader[40] = "단말회수";
            ArrHeader[41] = "소계";

            ArrHeader[42] = "총계";

            return ArrHeader;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        if (rptResult.Items.Count > 0)
                        {
                            HtmlTableRow tr = (HtmlTableRow)rptResult.Items[0].FindControl("tr");

                            if (paging.PageNumber == 0)
                            {
                                tr.Style.Add("background-color", "#e6e6e6");
                            }
                        }
                    }
                    
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }


        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {

                    string strOrgCd = this.hfOrgCd.Value;
                    string strSumMonth = this.ddlSumupMonth.SelectedValue.Replace("-", "");

                    if (strSumMonth.Length > 0)
                    {
                        using (TurnKeyMgmt mgmt = new TurnKeyMgmt())
                        {
                            ds = mgmt.GetTurnkeyTotalList(strOrgCd, strSumMonth);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                        return;
                    }
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();
                    excel.MergeCell = "4/16,5/22";

                    excel.ExcelDownLoad(this.Page, dt, "턴키 월별 조회 결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlSumupMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = ddlSumupMonth.SelectedValue.Replace("-", "");

                if (!strMonth.Equals(""))
                {
                    if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                    {
                        SetOrgList("", strMonth);
                    }
                    else
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    using (KTSUser user = new KTSUser())
                    {
                        ds = user.GetUserViewOrgMonth(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strOrgCd = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";

                            strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        }

                        this.hfOrgCd.Value = strOrgCd;
                        this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                    }
                    else
                    {
                        this.hfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}