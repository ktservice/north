﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DispatchInfo.aspx.cs" Inherits="KTSSolutionWeb.DispatchInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgCd() {

            var nWidth = 600;
            var nHeight = 400;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGLV: "3"
            };

            var Popupform = createForm("/Common/SearchOrgCd", param);

            Popupform.target = "SearchOrgCd";
            var win = window.open("", "SearchOrgCd", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function PopupOrgTree(orgcd, empno) {

            var SumupMonth = $("#<%= ddlRegMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("등록일를 선택해주세요.");
                return false;
            } else {

                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMDATE: SumupMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function btnDelCheck() {
            if (confirm("파견내역을 삭제하시겠습니까?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function btnRegChk() {
            var DispEmpNm = $("#<%= txbDispEmpNm.ClientID %>").val();
            var DispEmpNo = $("#<%= hfDispEmpNo.ClientID %>").val();
            var DispOrgNm = $("#<%= txbDispOrgNm.ClientID %>").val();
            var DispStDt = $("#<%= txbStDt.ClientID %>").val();
            var DispEnDt = $("#<%= txbEnDt.ClientID %>").val();

            if (DispEmpNm.length == 0 || DispEmpNo.length == 0) {
                alert("파견자를 입력해주세요.");
                return false;
            } else if (DispOrgNm.length == 0) {
                alert("파견지점을 입력해주세요.");
                return false;
            } else if (DispStDt.length == 0 || DispEnDt.length == 0) {
                alert("파견일자를 입력해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function SearchChk() {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var SumupMonth = $("#<%= ddlRegMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("등록일을 선택해주세요.");
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
                return false;
            } else {
                return true;
            }
        }

        function InitValue() {
            $("#<%= hfDispEmpOrg.ClientID %>").val('');
            $("#<%= hfDispEmpNo.ClientID %>").val('');
            $("#<%= txbDispEmpNm.ClientID %>").val('');
            $("#<%= hfDispOrgCd.ClientID %>").val('');
            $("#<%= txbDispOrgNm.ClientID %>").val('');
            $("#<%= txbStDt.ClientID %>").val('');
            $("#<%= txbEnDt.ClientID %>").val('');
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#<%= hfDispEmpOrg.ClientID %>").val(orgcd);
            $("#<%= hfDispEmpNo.ClientID %>").val(empno);
            $("#<%= txbDispEmpNm.ClientID %>").val(empnm);
        }

        function SetDispOrgCd(orgcd, orgnm) {
            $("#<%= hfDispOrgCd.ClientID %>").val(orgcd);
            $("#<%= txbDispOrgNm.ClientID %>").val(orgnm);
        }

        function SetOrgCd(orgcd, valtype) {
            document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelOrg)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>       
				<span class="inpbox" style="padding-bottom:10px;">
					<label>파견자</label>
                    <asp:TextBox ID="txbDispEmpNm" runat="server" Width="100px" ReadOnly="true"></asp:TextBox>
					<button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                    <asp:HiddenField ID="hfDispEmpOrg" runat="server" />
                    <asp:HiddenField ID="hfDispEmpNo" runat="server" />
				</span>
				<span class="inpbox" style="padding-bottom:10px;">
					<label>파견지점</label>
                    <asp:TextBox ID="txbDispOrgNm" runat="server" Width="120px" ReadOnly="true"></asp:TextBox>
					<button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgCd();">+</button>
                    <asp:HiddenField ID="hfDispOrgCd" runat="server" />
                </span>
				<span class="inpbox" style="padding-bottom:10px;">
					<label>파견일자</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="date" Width="120px" MaxLength="10"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEnDt" runat="server" class="date" Width="120px" MaxLength="10"></asp:TextBox>
                </span>
                <asp:Button id="btnDispReg" runat="server" OnClientClick="return btnRegChk();" OnClick="btnDispReg_Click" class="btn-green last" style="float:right;" Text="등록" />
		    </fieldset>
        </div>
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>  
                <span class="optionbox">
                    <asp:UpdatePanel ID="updPanelRegDt" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>파견시작일</label>
                            <asp:DropDownList ID="ddlRegMonth" runat="server" Width="120px" OnSelectedIndexChanged="ddlRegMonth_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
			    </span>     
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
                            <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlRegMonth" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_ServerClick" class="btn-green last" style="float:right;" Text="조회" />
		    </fieldset>
        </div>
		<!-- //E:searchbox -->
        
		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>파견내역 조회 결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" style="float:right;" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
			    </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>소속</th>
                                    <th>사번</th>
                                    <th>KTS사번</th>
                                    <th>이름</th>
                                    <th>파견지점</th>
                                    <th>파견일자</th>
                                    <th>삭제</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGFULLNM") %></td>
                                            <td>
                                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EMPNO") %>'></asp:Label>
                                            </td>
                                            <td><%# Eval("KTSEMPNO") %></td>
                                            <td><%# Eval("EMPNM") %></td>
                                            <td><%# Eval("DISPATCHORGNM") %></td>
                                            <td>
                                                <asp:Label ID="lblDisPatchDate" runat="server" Text='<%# Eval("DISPATCHDATE") %>'></asp:Label>
                                            </td>
                                            <td> 
                                                <asp:Button id="btnDelete" runat="server" OnClientClick="return btnDelCheck();" OnClick="btnDelete_Click" class="btn-del" Text="삭제" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
