﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using System.Data;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.BSL.Pay;

namespace KTSSolutionWeb
{
    public partial class TerminalDetail : PageBase
    {
        private string ORGCD
        {
            get
            {
                if (ViewState["ORGCD"] != null)
                    return ViewState["ORGCD"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["ORGCD"] = value;
            }
        }
        private string SUMUPMONTH
        {
            get
            {
                if (ViewState["SUMUPMONTH"] != null)
                    return ViewState["SUMUPMONTH"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SUMUPMONTH"] = value;
            }
        }

        private string SELECTLV
        {
            get
            {
                if (ViewState["SELECTLV"] != null)
                    return ViewState["SELECTLV"].ToString();
                else
                    return "";
            }
            set
            {
                ViewState["SELECTLV"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ORGCD = Request.Form["pORGCD"] == null ? "" : Request.Form["pORGCD"].ToString();
                    SUMUPMONTH = Request.Form["pSUMUPMONTH"] == null ? "" : Request.Form["pSUMUPMONTH"].ToString().Replace("-", "");

                    GetDataList();
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                using (MeritPayMgmt mgmt = new MeritPayMgmt())
                {
                    ds = mgmt.GetMeritPayTerminalDetailList(ORGCD, SUMUPMONTH, SELECTLV);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn(DataTable dt)
        {
            string[] ArrHeader = new string[dt.Columns.Count];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ArrHeader[i] = dt.Columns[i].ColumnName;
            }
            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {

            string[] ArrHeader = new string[22];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "현황월";
            ArrHeader[2] = "국사코드";
            ArrHeader[3] = "지점";
            ArrHeader[4] = "국사";
            ArrHeader[5] = "법인코드";
            ArrHeader[6] = "협력사코드";
            ArrHeader[7] = "법인명";
            ArrHeader[8] = "구분";
            ArrHeader[9] = "kt사번";
            ArrHeader[10] = "이름";
            ArrHeader[11] = "임대단말 제외건";
            ArrHeader[12] = "임대단말 정산건";
            ArrHeader[13] = "임대단말 정산액";
            ArrHeader[14] = "안테나 제외건";
            ArrHeader[15] = "안테나 회수건";
            ArrHeader[16] = "안테나 회수금액(정산액 아님)";
            ArrHeader[17] = "판매단말 제외건";
            ArrHeader[18] = "판매단말 정산건";
            ArrHeader[19] = "판매단말 정산액";
            ArrHeader[20] = "정산건수";
            ArrHeader[21] = "정산금액";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    using (MeritPayMgmt mgmt = new MeritPayMgmt())
                    {
                        ds = mgmt.GetMeritPayTerminalDetailList(ORGCD, SUMUPMONTH, SELECTLV);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn(dt);
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "실적급 월별 상세조회-단말");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }

        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}