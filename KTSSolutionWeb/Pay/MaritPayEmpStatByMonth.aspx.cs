﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Pay;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;

namespace KTSSolutionWeb
{
    public partial class MaritPayEmpStatByMonth : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        private DataTable DtSumupMonth
        {
            get
            {
                if (ViewState["DtSumupMonth"] != null)
                    return (DataTable)ViewState["DtSumupMonth"];
                else
                    return null;
            }
            set
            {
                ViewState["DtSumupMonth"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetSumupMonth();

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    this.btnOrgCd.Visible = true;
                    this.btnOrgCd.Enabled = true;

                    this.btnOrgCd.OnClientClick += "PopupOrgTree('', '');";
                    this.txbTeam.Attributes.Add("onClick", "PopupOrgTree('', '');");

                    this.btnEmpUser.Visible = true;
                    this.btnEmpUser.Enabled = true;

                    this.btnEmpUser.OnClientClick += "PopupEmpUser();";
                    this.txbEmpNm.Attributes.Add("onClick", "PopupEmpUser();");

                    this.txbEmpNm.Text = "";
                    this.hfEmpNo.Value = "";

                    this.txbTeam.Text = "";
                    this.hfOrgCd.Value = "";

                    this.btnReset.Visible = true;
                    this.btnReset.Enabled = true;
                }
                else
                {
                    this.btnOrgCd.Visible = false;
                    this.btnOrgCd.Enabled = false;

                    this.btnEmpUser.Visible = false;
                    this.btnEmpUser.Enabled = false;

                    this.txbEmpNm.Text = Session["EMPNM"].ToString();
                    this.hfEmpNo.Value = Session["EMPNO"].ToString();

                    this.btnReset.Visible = false;
                    this.btnReset.Enabled = false;
                }

                updPanelSearch.Update();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetSumupMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (MeritPayMgmt mgmt = new MeritPayMgmt())
                {
                    ds = mgmt.GetSumupMonth();
                }

                ddlStMonth.Items.Clear();
                ddlStMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    DtSumupMonth = ds.Tables[0];

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlStMonth.Items.Add(new ListItem(DtSumupMonth.Rows[i]["SUMUPMONTH"].ToString(), DtSumupMonth.Rows[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hfOrgCd.Value;
                string strStMonth = this.ddlStMonth.SelectedValue.Replace("-", "");
                string strEnMonth = this.ddlEnMonth.SelectedValue.Replace("-", "");
                string strEmpNo = this.hfEmpNo.Value;
                string strValType = this.ddlValType.SelectedValue;

                if ((strStMonth.Length > 0 && strEnMonth.Length > 0 && strValType.Length > 0) && (strOrgCd.Length > 0 || strEmpNo.Length > 0))
                {
                    using (MeritPayMgmt mgmt = new MeritPayMgmt())
                    {
                        if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                        {
                            ds = mgmt.GetMaritPayEmpStatByMonth(strOrgCd, strStMonth, strEnMonth, strEmpNo, strValType);
                        }
                        else
                        {
                            ds = mgmt.GetMaritPayEmpStatByMonth("", strStMonth, strEnMonth, strEmpNo, strValType);
                        }
                    }

                    paging.PageNumber = 0;
                    paging.PageSize = 30;

                    paging.Dt = null;

                    if (ds.Tables.Count > 0)
                    {
                        paging.TotalRows = ds.Tables[0].Rows.Count;
                        paging.Dt = ds.Tables[0];
                    }

                    DateTime StDate = DateTime.Parse(this.ddlStMonth.SelectedValue);
                    DateTime EnDate = DateTime.Parse(this.ddlEnMonth.SelectedValue);

                    int nDiffMonth = 12 * ((EnDate.Year - StDate.Year)) + (EnDate.Month - StDate.Month);

                    TableCell thNum = new TableCell();
                    thNum.Style.Add("font-weight", "800");
                    thNum.Text = "No.";
                    thead.Controls.Add(thNum);

                    TableCell thOrgNm = new TableCell();
                    thOrgNm.Style.Add("font-weight", "800");
                    thOrgNm.Text = "조직명";
                    thead.Controls.Add(thOrgNm);

                    TableCell thEmpNo = new TableCell();
                    thEmpNo.Style.Add("font-weight", "800");
                    thEmpNo.Text = "사번";
                    thead.Controls.Add(thEmpNo);

                    TableCell thKtsEmpNo = new TableCell();
                    thKtsEmpNo.Style.Add("font-weight", "800");
                    thKtsEmpNo.Text = "KTS사번";
                    thead.Controls.Add(thKtsEmpNo);

                    TableCell thEmpNm = new TableCell();
                    thEmpNm.Style.Add("font-weight", "800");
                    thEmpNm.Text = "이름";
                    thead.Controls.Add(thEmpNm);

                    for (int i = 0; i <= nDiffMonth; i++)
                    {
                        TableCell th = new TableCell();
                        th.Style.Add("font-weight", "800");
                        th.Text += StDate.AddMonths(i).ToString("yyyy년 MM월");
                        thead.Controls.Add(th);
                    }

                    TableCell thSum = new TableCell();
                    thSum.Style.Add("font-weight", "800");
                    thSum.Text = "합계";
                    thead.Controls.Add(thSum);

                    updPanel1.Update();
                    paging.SetPagingDataList();

                    SetChart(StDate, nDiffMonth);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetChart(DateTime StDate, int nDiffMonth)
        {
            try
            {
                try
                {
                    chart1.Width = Unit.Parse(hfChartWidth.Value + "px");
                }
                catch
                {
                    chart1.Width = Unit.Parse("2000px");
                }

                Color[] colors = { ColorTranslator.FromHtml("#00c0a9")
                                     , ColorTranslator.FromHtml("#029f88")
                                     , ColorTranslator.FromHtml("#d5b911")
                                     , ColorTranslator.FromHtml("#ed8a1d")
                                     , ColorTranslator.FromHtml("#985105")
                                     , ColorTranslator.FromHtml("#8ac8fc")
                                     , ColorTranslator.FromHtml("#4069fa")
                                     , ColorTranslator.FromHtml("#ab52fb")
                                     , ColorTranslator.FromHtml("#6ad7c4")};


                for (int i = 1; i < paging.Dt.Rows.Count; i++)
                {
                    Series series = new Series("series" + i.ToString());
                    series.ChartType = SeriesChartType.Line;
                    series.MarkerStyle = MarkerStyle.Circle;
                    series.MarkerSize = 6;
                    series.IsValueShownAsLabel = false;
                    series.BorderWidth = 2;
                    series.Color = colors[(i - 1) % 9];
                    series.LegendText = paging.Dt.Rows[i]["EMPNM"].ToString();
                    series.SmartLabelStyle.Enabled = true;

                    chart1.Series.Add(series);

                    for (int j = 0; j <= nDiffMonth; j++)
                    {
                        chart1.Series[i - 1].Points.AddY(paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString());
                        chart1.Series[i - 1].Points[j].ToolTip = paging.Dt.Rows[i]["EMPNM"].ToString() + " : " + paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString();

                        if (!paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString().Equals("0")
                            && !paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString().Equals("0.00"))
                        {
                            chart1.Series[i - 1].Points[j].IsValueShownAsLabel = true;
                            chart1.Series[i - 1].Points[j].Label = paging.Dt.Rows[i]["EMPNM"].ToString() + " : " + paging.Dt.Rows[i]["VALDATA" + j.ToString()].ToString();
                            chart1.Series[i - 1].Points[j].LabelForeColor = colors[(i - 1) % 9];
                        }

                        if (i - 1 == 0)
                        {
                            chart1.Series[0].Points[j].AxisLabel = StDate.AddMonths(j).ToString("yyyy년 MM월");
                        }
                    }
                }

                chart1.Legends[0].LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
                chart1.ChartAreas[0].AxisX.Interval = 1;
                chart1.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;

                updPanelChart1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private string[] GetHeaderColumn(int nDiffMonth)
        {
            string[] ArrHeader = new string[nDiffMonth + 8];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGNM";
            ArrHeader[3] = "EMPNO";
            ArrHeader[4] = "KTSEMPNO";
            ArrHeader[5] = "EMPNM";


            for (int i = 0; i <= nDiffMonth; i++)
            {
                ArrHeader[i + 6] = "VALDATA" + i.ToString();
            }

            ArrHeader[nDiffMonth + 7] = "SUMVALDATA";

            return ArrHeader;
        }

        private string[] GetHeaderTop(int nDiffMonth)
        {
            string[] ArrHeader = new string[nDiffMonth + 8];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "조직코드";
            ArrHeader[2] = "조직명";
            ArrHeader[3] = "사번";
            ArrHeader[4] = "KTS사번";
            ArrHeader[5] = "이름";

            DateTime StDate = DateTime.Parse(ddlStMonth.SelectedValue);

            for (int i = 0; i <= nDiffMonth; i++)
            {
                ArrHeader[i + 6] = StDate.AddMonths(i).ToString("yyyy년 MM월");
            }

            ArrHeader[nDiffMonth + 7] = "합계";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {
            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        if (rptResult.Items.Count > 0)
                        {
                            TableRow tr = (TableRow)rptResult.Items[0].FindControl("tr");

                            if (paging.PageNumber == 0)
                            {
                                tr.Style.Add("background-color", "#e6e6e6");
                            }
                        }
                    }

                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }


        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {

                    string strOrgCd = this.hfOrgCd.Value;
                    string strStMonth = this.ddlStMonth.SelectedValue.Replace("-", "");
                    string strEnMonth = this.ddlEnMonth.SelectedValue.Replace("-", "");
                    string strEmpNo = this.hfEmpNo.Value;
                    string strValType = this.ddlValType.SelectedValue;

                    if ((strStMonth.Length > 0 && strEnMonth.Length > 0 && strValType.Length > 0) && (strOrgCd.Length > 0 || strEmpNo.Length > 0))
                    {
                        using (MeritPayMgmt mgmt = new MeritPayMgmt())
                        {
                            if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                            {
                                ds = mgmt.GetMaritPayEmpStatByMonth(strOrgCd, strStMonth, strEnMonth, strEmpNo, strValType);
                            }
                            else
                            {
                                ds = mgmt.GetMaritPayEmpStatByMonth("", strStMonth, strEnMonth, strEmpNo, strValType);
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                        return;
                    }
                }

                DateTime StDate = DateTime.Parse(this.ddlStMonth.SelectedValue);
                DateTime EnDate = DateTime.Parse(this.ddlEnMonth.SelectedValue);

                int nDiffMonth = 12 * ((EnDate.Year - StDate.Year)) + (EnDate.Month - StDate.Month);

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn(nDiffMonth);
                    excel.HeaderTop = GetHeaderTop(nDiffMonth);
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "실적급 개인별 추이 조회 결과_" + this.ddlValType.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    this.txbEmpNm.Text = "";
                    this.hfEmpNo.Value = "";

                    this.txbTeam.Text = "";
                    this.hfOrgCd.Value = "";

                    this.ddlStMonth.SelectedIndex = 0;

                    ddlEnMonth.Items.Clear();
                    ddlEnMonth.Items.Add(new ListItem("선택", ""));

                    updPanelSearch.Update();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfEmpNo_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    this.txbEmpNm.Text = Request.Form[this.txbEmpNm.UniqueID].ToString();

                    updPanelSearch.Update();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void ddlStMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strStMonth = ddlStMonth.SelectedValue;

                ddlEnMonth.Items.Clear();
                ddlEnMonth.Items.Add(new ListItem("선택", ""));

                if (strStMonth.Length > 0)
                {
                    DataRow[] dr = DtSumupMonth.Select(string.Format("SUMUPMONTH >= '{0}'", strStMonth));

                    for (int i = 0; i < dr.Length; i++)
                    {
                        ddlEnMonth.Items.Add(new ListItem(dr[i]["SUMUPMONTH"].ToString(), dr[i]["SUMUPMONTH"].ToString()));
                    }
                }

                updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void tr_DataBinding(object sender, EventArgs e)
        {
            try
            {
                TableRow tr = (TableRow)sender;

                string strStMonth = this.ddlStMonth.SelectedValue.Replace("-", "");
                string strEnMonth = this.ddlEnMonth.SelectedValue.Replace("-", "");

                DateTime StDate = DateTime.Parse(this.ddlStMonth.SelectedValue);
                DateTime EnDate = DateTime.Parse(this.ddlEnMonth.SelectedValue);

                int nDiffMonth = 12 * ((EnDate.Year - StDate.Year)) + (EnDate.Month - StDate.Month);

                TableCell tdNum = new TableCell();
                tdNum.Text = string.Format("{0}", Eval("NUM").ToString());
                tr.Controls.Add(tdNum);

                TableCell tdOrgNm = new TableCell();
                tdOrgNm.Text = string.Format("{0}", Eval("ORGNM").ToString());
                tr.Controls.Add(tdOrgNm);

                TableCell tdEmpNo = new TableCell();
                tdEmpNo.Text = string.Format("{0}", Eval("EMPNO").ToString());
                tr.Controls.Add(tdEmpNo);

                TableCell tdKtsEmpNo = new TableCell();
                tdKtsEmpNo.Text = string.Format("{0}", Eval("KTSEMPNO").ToString());
                tr.Controls.Add(tdKtsEmpNo);

                TableCell tdEmpNm = new TableCell();
                tdEmpNm.Text = string.Format("{0}", Eval("EMPNM").ToString());
                tr.Controls.Add(tdEmpNm);

                for (int i = 0; i <= nDiffMonth; i++)
                {
                    TableCell td = new TableCell();
                    td.Text = string.Format("{0}", Eval("VALDATA" + i.ToString()).ToString());
                    tr.Controls.Add(td);
                }

                TableCell tdSum = new TableCell();
                tdSum.Text = string.Format("{0}", Eval("SUMVALDATA").ToString());
                tr.Controls.Add(tdSum);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void ddlEnMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string strMonth = ddlEnMonth.SelectedValue.Replace("-", "");

                this.hfOrgCd.Value = "";
                this.txbTeam.Text = "";

                if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0017") || Session["AUTHID"].Equals("AUTH0004"))
                {
                    if (!strMonth.Equals(""))
                    {
                        SetOrgList(strMonth);
                    }
                }
                else
                {
                    this.hfOrgCd.Value = this.Session["ORGCD"].ToString();
                    this.txbTeam.Text = this.Session["ORGFULLNM"].ToString();
                }

                this.updPanelSearch.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}