﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaritPayTotalListByOrgState.aspx.cs" Inherits="KTSSolutionWeb.MaritPayTotalListByOrgState" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        window.onload = setChartWidth;

        function setChartWidth() {
            var width = document.documentElement.clientWidth - 180;
            $("#<%= hfChartWidth.ClientID %>").val(width);
        }

        function PopupOrgTree(orgcd, empno) {

            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hdfOrgCd.ClientID %>").value;
                }

                var nWidth = 400;
                var nHeight = 700;
                var nTop = (window.screen.height / 2) - (nHeight / 2);
                var nLeft = (window.screen.width / 2) - (nWidth / 2);

                if (nTop < 0) nTop = 0;
                if (nLeft < 0) nLeft = 0;

                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pMdate: SumupMonth,
                    pTYPE: "pay"
                };

                var Popupform = createForm("/Common/OrgTree_Oper", param);

                Popupform.target = "OrgTree_Oper";
                var win = window.open("", "OrgTree_Oper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                Popupform.submit();

                win.focus();
            }
        }

        function PopupMaritPayDetail(orgcd, selectlv, sumupmonth, cnt, valtype) {

            if (cnt != '0' && cnt != '0.00') {
                var param = {
                    pORGCD: orgcd,
                    pSUMUPMONTH: sumupmonth,
                    pSELECTLV: selectlv,
                    pVALTYPE: valtype
                };

                if (valtype == "OPENCNT" || valtype == "OPENPOINT") {

                    var nWidth = 1200;
                    var nHeight = 760;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Pay/OpenCntDetail", param);

                    Popupform.target = "PopupDetail";
                    var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                }
                else if (valtype == "ASCNT" || valtype == "ASPOINT") {

                    var nWidth = 1200;
                    var nHeight = 760;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Pay/AsCntDetail", param);

                    Popupform.target = "PopupDetail";
                    var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                }
                else if (valtype == "MANUAL") {

                    var nWidth = 1200;
                    var nHeight = 760;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Pay/ManualDetail", param);

                    Popupform.target = "PopupDetail";
                    var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                }
                else if (valtype == "TERMINAL") {

                    var nWidth = 1200;
                    var nHeight = 760;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Pay/TerminalDetail", param);

                    Popupform.target = "PopupDetail";
                    var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                }
                else if (valtype == "NONMOVE") {

                    var nWidth = 1200;
                    var nHeight = 760;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Pay/NonMoveDetail", param);

                    Popupform.target = "PopupDetail";
                    var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                }
                else if (valtype == "DISPATCH") {

                    var nWidth = 1200;
                    var nHeight = 760;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Pay/DispatchDetail", param);

                    Popupform.target = "PopupDetail";
                    var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                }
                else if (valtype == "EVENT1" || valtype == "EVENT2" || valtype == "EVENT3") {

                    var nWidth = 1200;
                    var nHeight = 760;
                    var nTop = (window.screen.height / 2) - (nHeight / 2);
                    var nLeft = (window.screen.width / 2) - (nWidth / 2);

                    if (nTop < 0) nTop = 0;
                    if (nLeft < 0) nLeft = 0;

                    var Popupform = createForm("/Pay/EventDetail", param);

                    Popupform.target = "PopupDetail";
                    var win = window.open("", "PopupDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
                    Popupform.submit();

                    win.focus();
                }
                else {
                    return;
                }
            }
            else {
                return;
            }
        }

        function SearchChk() {
            var OrgCd = $("#<%= hdfOrgCd.ClientID %>").val();
            var SumupMonth = $("#<%= ddlSumupMonth.ClientID %> option:selected").val();

            if (SumupMonth.length == 0) {
                alert("조회일자를 선택해주세요.");
                return false;
                return false;
            } else if (OrgCd.length == 0) {
                alert("조직을 선택해주세요.");
            } else {
                return true;
            }
        }

        function SetOrgCd(orgcd, valtype) {

            this.focus();

            document.getElementById("<%=hdfOrgCd.ClientID %>").value = orgcd;

            <%=Page.GetPostBackEventReference(updPanelOrg)%>;
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>
                <span class="optionbox">
                    <asp:UpdatePanel ID="updPanelSumupMonth" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>조회일자</label>
                            <asp:DropDownList ID="ddlSumupMonth" runat="server" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="ddlSumupMonth_SelectedIndexChanged">
                                <asp:ListItem Text="선택" Value="" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
			    </span>
				<span class="inpbox">
                    <asp:UpdatePanel ID="updPanelOrg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
					        <button type="button" class="btn-plus" title="검색" onclick="javascript:PopupOrgTree('', '');">+</button>
                            <asp:HiddenField ID="hdfOrgCd" runat="server" OnValueChanged="hdfOrgCd_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hdfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlSumupMonth" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <asp:Button id="btnSelect" runat="server" OnClientClick="return SearchChk();" OnClick="btnSelect_Click" class="btn-green last" style="float:right;" Text="조회" />
		    </fieldset>
        </div>
        <br />
		<!-- //E:searchbox -->        
                
		<!-- S:datalist -->
        <div class="chartbox">
            <div style="width:100%;height:420px;overflow-x:hidden;overflow-y:hidden;text-align:left;"> <!-- 차트 사이즈 조정 -->
                <asp:HiddenField ID="hfChartWidth" runat="server" />
                <asp:UpdatePanel ID="updPanelChart1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:chart id="chart1" runat="server" Height="420px">
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                    <AxisX>
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineColor="#000" LineDashStyle="Solid" />
                                    </AxisX>
                                    <AxisY IsLabelAutoFit="False" IsMarginVisible="False" LabelAutoFitStyle="None">
                                        <LabelStyle Font="돋움" />
                                        <MajorGrid LineDashStyle="NotSet"/>
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <Legends>
                                <asp:Legend Name="Legend1"></asp:Legend>
                            </Legends>
                        </asp:chart>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
		<!-- //E:datalist -->

		<!-- S:datalist -->
		<div class="datalist">
		    <!-- S:list-top -->
		    <div class="list-top">
			    <strong>실적급 월별 조회결과</strong>
			    <div class="pull-right">
				    <div class="btnset">
			            <asp:Button id="btnExcel" runat="server" class="btn-green" onClick="btnExcel_ServerClick" Text="엑셀" />
				    </div>
                </div>
		    </div>
		    <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>조직명</th>
                                    <th>일자</th>
                                    <th>개통건수</th>
                                    <th>AS건수</th>
                                    <th>개통 포인트</th>
                                    <th>AS 포인트</th>
                                    <th>수작업</th>
                                    <th>기가아이즈</th>
                                    <th>단말</th>
                                    <th>무출동</th>
                                    <th>파견</th>
                                    <th>EVENT1</th>
                                    <th>EVENT2</th>
                                    <th>EVENT3</th>
                                    <th>합계</th>
                                    <th>매출액</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptResult" runat="server">
                                    <ItemTemplate>
                                        <tr id="tr" runat="server">
                                            <td><%# Eval("NUM") %></td>
                                            <td><%# Eval("ORGNM") %>
                                                <asp:Label ID="lblOrgCd" runat="server" Visible="false" Text='<%# Eval("ORGCD") %>'></asp:Label>
                                                <asp:Label ID="lblSelectLv" runat="server" Visible="false" Text='<%# Eval("SELECTLV") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSumupMonth" runat="server" Text='<%# Eval("SUMUPMONTH") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("OPENCNT") %>', 'OPENCNT')"><%# Eval("OPENCNT") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ASCNT") %>', 'ASCNT');"><%# Eval("ASCNT") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("OPENPOINT") %>', 'OPENPOINT');"><%# Eval("OPENPOINT") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("ASPOINT") %>', 'ASPOINT');"><%# Eval("ASPOINT") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("MANUAL") %>', 'MANUAL');"><%# Eval("MANUAL") %></a>
                                            </td>
                                            <td><%# Eval("GEYES") %></td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("TERMINAL") %>', 'TERMINAL');"><%# Eval("TERMINAL") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("NONMOVE") %>', 'NONMOVE');"><%# Eval("NONMOVE") %></a>
                                            </td>
                                            <td>
                                                <a  style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("DISPATCH") %>', 'DISPATCH');"><%# Eval("DISPATCH") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("EVENT1") %>', 'EVENT1');"><%# Eval("EVENT1") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("EVENT2") %>', 'EVENT2');"><%# Eval("EVENT2") %></a>
                                            </td>
                                            <td>
                                                <a style="color:#000;" onclick="javascript:PopupMaritPayDetail('<%# Eval("ORGCD")%>', '<%# Eval("SELECTLV")%>', '<%# Eval("SUMUPMONTH")%>', '<%# Eval("EVENT3") %>', 'EVENT3');"><%# Eval("EVENT3") %></a>
                                            </td>
                                            <td><%# Eval("SUMPOINT") %></td>
                                            <td><%# Eval("SUMPRICE") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSelect" EventName="click"/>
                        <asp:AsyncPostBackTrigger ControlID="paging" />
                    </Triggers>
                </asp:UpdatePanel>      
            </div>
            <uc:paging ID="paging" runat="server" OnPreRender="paging_PreRender" />
        </div>
		<!-- E:datalist -->
    </div>
	<!--//E: contentsarea -->
</asp:Content>
