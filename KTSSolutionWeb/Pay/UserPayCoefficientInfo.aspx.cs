﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;
using KTS.KTSSolution.BSL.Pay;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Globalization;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web;

namespace KTSSolutionWeb
{
    public partial class UserPayCoefficientInfo : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                GetSumupMonth();

                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void GetSumupMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                {
                    ds = mgmt.GetSumupMonth();
                }

                ddlSumupMonth.Items.Clear();
                ddlSumupMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlSumupMonth.Items.Add(new ListItem(ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString(), ds.Tables[0].Rows[i]["SUMUPMONTH"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            try
            {
                string strResult = "";

                //if (bCalc)
                //{
                //    if (rptResult.Items.Count > 0)
                //    {
                //        double dOpenPoint = double.Parse(txbOpenPoint.Text);
                //        double dAsPoint = double.Parse(txbAsPoint.Text);

                //        double dOpenMinChk = dOpenPoint - (dOpenPoint * (LimitVal * 0.01));
                //        double dOpenMaxChk = dOpenPoint + (dOpenPoint * (LimitVal * 0.01));

                //        double dAsMinChk = dAsPoint - (dAsPoint * (LimitVal * 0.01));
                //        double dAsMaxChk = dAsPoint + (dAsPoint * (LimitVal * 0.01));

                //        double dOpenPoint_C = 0;
                //        double dAsPoint_C = 0;

                //        for (int i = 0; i < rptResult.Items.Count - 1; i++)
                //        {
                //            Label lblOpenPoint = (Label)rptResult.Items[i].FindControl("lblOpenPoint");
                //            Label lblASPoint = (Label)rptResult.Items[i].FindControl("lblASPoint");
                //            TextBox txbOpenConfficient = (TextBox)rptResult.Items[i].FindControl("txbOpenConfficient");
                //            TextBox txbAsConfficient = (TextBox)rptResult.Items[i].FindControl("txbAsConfficient");

                //            double OPEN = 0;
                //            double AS = 0;

                //            double.TryParse(txbOpenConfficient.Text, out OPEN);
                //            double.TryParse(txbAsConfficient.Text, out AS);

                //            if (bValChk && MinVal > 0 && MaxVal > 0)
                //            {
                //                if ((MinVal > OPEN) || (MaxVal < OPEN) || (MinVal > AS) || (MaxVal < AS))
                //                {
                //                    strResult = "V";
                //                    hfCalcResult.Value = strResult;

                //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('구역계수는 최소 " + MinVal + " 이상, 최대 " + MaxVal + " 이하 여야 합니다.');", true);

                //                    return;
                //                }
                //            }

                //            dOpenPoint_C += double.Parse(lblOpenPoint.Text) * OPEN;
                //            dAsPoint_C += double.Parse(lblASPoint.Text) * AS;
                //        }

                //        txbOpenPointC.Text = dOpenPoint_C.ToString();
                //        txbAsPointC.Text = dAsPoint_C.ToString();

                //        if ((dOpenPoint_C >= dOpenMinChk && dOpenPoint_C <= dOpenMaxChk) && (dAsPoint_C >= dAsMinChk && dAsPoint_C <= dAsMaxChk))
                //        {
                //            strResult = "T";
                //        }
                //        else
                //        {
                //            strResult = "F";
                //        }

                //        if (dOpenPoint_C >= dOpenMinChk && dOpenPoint_C <= dOpenMaxChk)
                //        {
                //            txbOpenPointC.Style.Remove("background-color");
                //            txbOpenPointC.Style.Add("background-color", "#00b3ff");
                //        }
                //        else
                //        {
                //            txbOpenPointC.Style.Remove("background-color");
                //            txbOpenPointC.Style.Add("background-color", "red");
                //        }

                //        if (dAsPoint_C >= dAsMinChk && dAsPoint_C <= dAsMaxChk)
                //        {
                //            txbAsPointC.Style.Remove("background-color");
                //            txbAsPointC.Style.Add("background-color", "#00b3ff");
                //        }
                //        else
                //        {
                //            txbAsPointC.Style.Remove("background-color");
                //            txbAsPointC.Style.Add("background-color", "red");
                //        }
                //    }
                //    else
                //    {
                //        strResult = "X";
                //    }
                //}
                //else
                //{
                //    strResult = "E";
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                //}

                //hfCalcResult.Value = strResult;
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnReg_Click(object sender, EventArgs e)
        {
            try
            {
                //if (hfCalcResult.Value.Equals("T"))
                //{
                //    List<string> lstData = new List<string>();

                //    for (int i = 0; i < rptResult.Items.Count; i++)
                //    {
                //        Label lblSumupMonth = (Label)rptResult.Items[i].FindControl("lblSumupMonth");
                //        Label lblIdmsEmpNo = (Label)rptResult.Items[i].FindControl("lblIdmsEmpNo");

                //        string strSumupMonth = lblSumupMonth.Text;
                //        string strEmpNo = lblIdmsEmpNo.Text;


                //        if (strSumupMonth.Length > 0 && strEmpNo.Length > 0)
                //        {
                //            Label lblOpenPoint = (Label)rptResult.Items[i].FindControl("lblOpenPoint");
                //            Label lblASPoint = (Label)rptResult.Items[i].FindControl("lblASPoint");
                //            TextBox txbOpenConfficient = (TextBox)rptResult.Items[i].FindControl("txbOpenConfficient");
                //            TextBox txbAsConfficient = (TextBox)rptResult.Items[i].FindControl("txbAsConfficient");

                //            double dOpenPoint = 0;
                //            double dOpenConff = 0;

                //            double.TryParse(lblOpenPoint.Text, out dOpenPoint);
                //            double.TryParse(txbOpenConfficient.Text, out dOpenConff);

                //            string strOpenConfficient = string.Format("{0:0.00000}", dOpenConff);
                //            string strOpenPoint_C = string.Format("{0:0.00000}", dOpenPoint * dOpenConff);

                //            double dAsPoint = 0;
                //            double dAsConff = 0;

                //            double.TryParse(lblASPoint.Text, out dAsPoint);
                //            double.TryParse(txbAsConfficient.Text, out dAsConff);
                
                //            string strOpenConfficient = string.Format("{0:0.00000}", dOpenConff);
                //            string strOpenPoint_C = string.Format("{0:0.00000}", dOpenPoint * dOpenConff);
                //            string strAsConfficient = string.Format("{0:0.00000}", dAsConff);
                //            string strAsPoint_C = string.Format("{0:0.00000}", dAsPoint * dAsConff);

                //            string strData = strSumupMonth + "|" + strEmpNo + "|" + strOpenConfficient + "|" + strAsConfficient + "|" + strOpenPoint_C + "|" + strAsPoint_C;

                //            lstData.Add(strData);
                //        }
                //    }

                //    using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                //    {
                //        mgmt.UpdUserPayCofficient(lstData);
                //    }

                //    GetDataList();
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "RegOk", "alert('최종저장을 완료했습니다.');", true);

                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('잘못된 접근입니다.');", true);
                //}
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();
            DataSet dsOrg = new DataSet();

            string result = string.Empty;

            string strOrgCd = "";
            string strMonth = aData[1];

            using (OrgCodeTree org = new OrgCodeTree())
            {
                DateTime chkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime month = new DateTime(int.Parse(strMonth.Substring(0, 4)), int.Parse(strMonth.Substring(4, 2)), 1);

                if (chkDate > month)
                {
                    ds = org.GetOperOrgMonthList("", "N", strMonth);
                }
                else
                {
                    ds = org.GetOperOrgList("", "N");
                }

                if (aData[0].Length == 0)
                {
                    using (KTSUser user = new KTSUser())
                    {
                        dsOrg = user.GetUserViewOrgMonth(HttpContext.Current.Session["EMPNO"].ToString(), strMonth);
                    }

                    for (int i = 0; i < dsOrg.Tables[0].Rows.Count; i++)
                    {
                        if (strOrgCd.Length > 0)
                            strOrgCd += ",";

                        strOrgCd += dsOrg.Tables[0].Rows[i]["ORGCD"].ToString();
                    }
                }
                else
                {
                    strOrgCd = aData[0];
                }
            }

            result = Utility.GetOrgNm(strOrgCd, ds.Tables[0]);

            string strMinVal = "";
            string strMaxVal = "";
            string strLimitVal = "";
            string strUpdateYn = "";
            bool bSum = false;

            DataRow[] dr = ds.Tables[0].Select(string.Format("ORGCD = '{0}'", strOrgCd));

            if (dr.Length > 0)
            {
                if (dr[0]["ORGLEVEL"].ToString().Equals("3") && strOrgCd.Length == 6)
                {
                    bSum = true;
                }
            }

            string strAuthid = HttpContext.Current.Session["AUTHID"].ToString();

            if (bSum && (strAuthid.Equals("AUTH0001") || strAuthid.Equals("AUTH0017") || strAuthid.Equals("AUTH0003") || strAuthid.Equals("AUTH0004")))
            {
                using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                {
                    ds = mgmt.GetCoefficientLimitVal(strMonth);
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    strMinVal = ds.Tables[0].Rows[0]["MINVAL"].ToString();
                    strMaxVal = ds.Tables[0].Rows[0]["MAXVAL"].ToString();
                    strLimitVal = ds.Tables[0].Rows[0]["SUMVAL"].ToString();
                    strUpdateYn = ds.Tables[0].Rows[0]["UPDATEYN"].ToString();
                }
            }

            return strOrgCd + "|" + result + "|" + strMinVal + "|" + strMaxVal + "|" + strLimitVal + "|" + strUpdateYn;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = null;

                string strOrgCd = aData[0];
                string strSumMonth = aData[1];

                using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                {
                    ds = mgmt.GetUserPayConficientList(strOrgCd, strSumMonth);
                }

                if (ds != null)
                {
                    result = Utility.DataTableToJson(ds.Tables[0]);
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string RegData(List<List<string>> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, null);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                List<string> lstData = new List<string>();

                DataTable dt = new DataTable();

                try
                {
                    foreach (List<string> list in aData)
                    {
                        string strSumupMonth = list[0];
                        string strEmpNo = list[1];
                        string strOpen_C = list[2];
                        string strAs_C = list[3];
                        string strOpen_R = list[4];
                        string strAs_R = list[5];

                        double dOpen_C = 0;
                        double dAs_C = 0;
                        double dOpen_R = 0;
                        double dAs_R = 0;

                        double.TryParse(strOpen_C, out dOpen_C);
                        double.TryParse(strAs_C, out dAs_C);
                        double.TryParse(strOpen_R, out dOpen_R);
                        double.TryParse(strAs_R, out dAs_R);

                        string strOpenConfficient = string.Format("{0:0.00000}", dOpen_C);
                        string strAsConfficient = string.Format("{0:0.00000}", dAs_C);
                        string strOpenPoint_C = string.Format("{0:0.00000}", dOpen_R);
                        string strAsPoint_C = string.Format("{0:0.00000}", dAs_R);

                        string strData = strSumupMonth + "|" + strEmpNo + "|" + strOpenConfficient + "|" + strAsConfficient + "|" + strOpenPoint_C + "|" + strAsPoint_C;

                        lstData.Add(strData);
                    }

                    using (UserConfficientMgmt mgmt = new UserConfficientMgmt())
                    {
                        mgmt.UpdUserPayCofficient(lstData);
                        dt = Utility.DataTableError("00", "", "");
                    }
                }
                catch
                {
                    dt = Utility.DataTableError("01", "최종저장 중 오류가 발생했습니다.", "");
                }

                result = Utility.DataTableToJson(dt);
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
    }
}