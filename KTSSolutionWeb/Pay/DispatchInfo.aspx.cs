﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;
using KTS.KTSSolution.BSL.Pay;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Globalization;

namespace KTSSolutionWeb
{
    public partial class DispatchInfo : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetDDLRegMonth();

                this.txbDispOrgNm.Attributes.Add("onclick", "PopupOrgCd();");
                this.txbDispEmpNm.Attributes.Add("onclick", "PopupEmpUser();");
                this.txbTeam.Attributes.Add("onclick", "PopupOrgTree('', '');");

                updPanelOrg.Update();

                //GetDataList();
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void SetOrgList(string strMonth)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    DateTime chkDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateTime month = new DateTime(int.Parse(strMonth.Substring(0, 4)), int.Parse(strMonth.Substring(4, 2)), 1);

                    if (chkDate > month)
                    {
                        ds = org.GetOperOrgMonthList("", "N", strMonth);
                    }
                    else
                    {
                        ds = org.GetOperOrgList("", "N");
                    }                    
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void SetDDLRegMonth()
        {
            DataSet ds = new DataSet();

            try
            {
                using (DispatchMgmt mgmt = new DispatchMgmt())
                {
                    ds = mgmt.GetDispatchRegMonth();
                }

                ddlRegMonth.Items.Clear();
                ddlRegMonth.Items.Add(new ListItem("선택", ""));

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ddlRegMonth.Items.Add(new ListItem(ds.Tables[0].Rows[i]["REGDT"].ToString(), ds.Tables[0].Rows[i]["REGDT"].ToString()));
                    }
                }

                updPanelRegDt.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private void GetDataList()
        {
            DataSet ds = new DataSet();

            try
            {
                paging.bPreRender = true;

                string strOrgCd = this.hdfOrgCd.Value;
                string strRegMonth = this.ddlRegMonth.SelectedValue.Replace("-", "");

                using (DispatchMgmt mgmt = new DispatchMgmt())
                {
                    ds = mgmt.GetDispatchList(strOrgCd, strRegMonth);
                }

                paging.PageNumber = 0;
                paging.PageSize = 20;

                paging.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    paging.TotalRows = ds.Tables[0].Rows.Count;
                    paging.Dt = ds.Tables[0];
                }

                paging.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }

        private string[] GetHeaderColumn()
        {
            string[] ArrHeader = new string[9];

            ArrHeader[0] = "NUM";
            ArrHeader[1] = "ORGCD";
            ArrHeader[2] = "ORGFULLNM";
            ArrHeader[3] = "EMPNO";
            ArrHeader[4] = "KTSEMPNO";
            ArrHeader[5] = "EMPNM";
            ArrHeader[6] = "DISPATCHORGCD";
            ArrHeader[7] = "DISPATCHORGNM";
            ArrHeader[8] = "DISPATCHDATE";

            return ArrHeader;
        }

        private string[] GetHeaderTop()
        {
            string[] ArrHeader = new string[9];

            ArrHeader[0] = "No.";
            ArrHeader[1] = "소속코드";
            ArrHeader[2] = "소속";
            ArrHeader[3] = "사번";
            ArrHeader[4] = "KTS사번";
            ArrHeader[5] = "이름";
            ArrHeader[6] = "파견지점코드";
            ArrHeader[7] = "파견지점";
            ArrHeader[8] = "파견일자";

            return ArrHeader;
        }

        private string[] GetHeaderBottom()
        {

            string[] ArrHeader = new string[0];

            return ArrHeader;
        }

        protected void btnSelect_ServerClick(object sender, EventArgs e)
        {
            try
            {
                GetDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        protected void paging_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (paging.bPreRender)
                {
                    if (paging.Dt != null)
                    {
                        paging.RepeaterDataBind(rptResult);

                        for (int i = 0; i < rptResult.Items.Count; i++)
                        {
                            Button btnDelete = (Button)rptResult.Items[i].FindControl("btnDelete");

                            AsyncPostBackTrigger asyncBtnDelete = new AsyncPostBackTrigger();
                            asyncBtnDelete.ControlID = btnDelete.UniqueID;
                            asyncBtnDelete.EventName = "Click";

                            updPanel1.Triggers.Add(asyncBtnDelete);
                        }
                    }
                    
                    updPanel1.Update();
                    paging.bPreRender = false;
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnExcel_ServerClick(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = null;

            try
            {
                if (paging.Dt != null)
                    dt = paging.Dt.Copy();
                else
                {
                    string strOrgCd = this.hdfOrgCd.Value;
                    string strRegMonth = this.ddlRegMonth.SelectedValue.Replace("-", "");

                    using (DispatchMgmt mgmt = new DispatchMgmt())
                    {
                        ds = mgmt.GetDispatchList(strOrgCd, strRegMonth);
                    }

                    dt = ds.Tables[0];
                }

                using (ExcelUtil excel = new ExcelUtil())
                {
                    excel.HeaderColumn = GetHeaderColumn();
                    excel.HeaderTop = GetHeaderTop();
                    excel.HeaderBom = GetHeaderBottom();

                    excel.ExcelDownLoad(this.Page, dt, "파견내역 조회 결과");
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();

                if (dt != null)
                    dt.Dispose();
            }
        }

        protected void hdfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;

                this.updPanelOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnDelete = (Button)sender;
                Label lblEmpNo = (Label)btnDelete.Parent.FindControl("lblEmpNo");
                Label lblDisPatchDate = (Label)btnDelete.Parent.FindControl("lblDisPatchDate");

                if (lblEmpNo.Text.Length == 0 || lblDisPatchDate.Text.Length == 0 || lblDisPatchDate.Text.Split('~').Length < 2)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('잘못된 접근입니다.');", true);
                }
                else
                {
                    string strStDt = lblDisPatchDate.Text.Split('~')[0].Replace("-", "");
                    string strEnDt = lblDisPatchDate.Text.Split('~')[1].Replace("-", "");

                    using (DispatchMgmt mgmt = new DispatchMgmt())
                    {
                        mgmt.DelDispatchInfo(lblEmpNo.Text, strStDt, strEnDt);
                    }

                    SetDDLRegMonth();
                    GetDataList();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Delete", "alert('파견내역이 삭제되었습니다.');", true);
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('파견내역 삭제 중 오류가 발생했습니다.');", true);
            }
        }

        protected void btnDispReg_Click(object sender, EventArgs e)
        {
            DataSet dsOrg = new DataSet();
            DataSet dsList = new DataSet();

            try
            {
                string strOrgCd = this.hfDispEmpOrg.Value;
                string strEmpNo = this.hfDispEmpNo.Value;
                string strEmpNm = Request.Form[txbDispEmpNm.UniqueID].ToString();

                string strDispOrgNm = Request.Form[txbDispOrgNm.UniqueID].ToString(); 
                string strDispOrgCd = this.hfDispOrgCd.Value;

                string strStdt = this.txbStDt.Text.Replace("-", "");
                string strEndt = this.txbEnDt.Text.Replace("-", "");

                string strRegEmpNo = Page.Session["EMPNO"].ToString();


                if (strOrgCd.Length == 0 || strEmpNm.Length == 0 || strEmpNo.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('파견자를 입력하세요.');", true);
                }
                else if (strDispOrgNm.Length == 0 || strDispOrgCd.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('파견지점을 입력하세요.');", true);
                }
                else if (strStdt.Length == 0 || strEndt.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('파견일자를 입력하세요.');", true);
                }
                else
                {
                    int nStdt = 0;
                    int nEndt = 0;

                    int.TryParse(strStdt, out nStdt);
                    int.TryParse(strEndt, out nEndt);

                    if (nStdt == 0 || nEndt == 0 || (nStdt > nEndt))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('파견일자가 잘못되었습니다. 확인해주세요.');", true);
                    }
                    else
                    {
                        using (DispatchMgmt mgmt = new DispatchMgmt())
                        {
                            dsList = mgmt.GetDispatchListCheck(strEmpNo, strDispOrgCd);

                            bool bDateChk = true;
                            bool bOrgChk = true;

                            for (int i = 0; i < dsList.Tables[0].Rows.Count; i++)
                            {
                                string strSavedStDt = dsList.Tables[0].Rows[i]["STDT"].ToString();
                                string strSavedEnDt = dsList.Tables[0].Rows[i]["ENDT"].ToString();

                                if (int.Parse(strStdt) >= int.Parse(strSavedStDt) && int.Parse(strStdt) <= int.Parse(strSavedEnDt))
                                {
                                    bDateChk = false;
                                }
                                else if (int.Parse(strEndt) >= int.Parse(strSavedStDt) && int.Parse(strEndt) <= int.Parse(strSavedEnDt))
                                {
                                    bDateChk = false;
                                }
                            }

                            if (!dsList.Tables[1].Rows[0]["DISPORGCHK"].ToString().Equals("Y"))
                            {
                                bOrgChk = false;
                            }

                            if (!bDateChk)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('기간이 겹치는 파견목록이 있습니다.');", true);
                            }
                            //else if (!bOrgChk)
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationChk", "alert('지사가 같은 지점으로는 파견등록을 할 수 없습니다.');", true);
                            //}
                            else
                            {
                                mgmt.SetDispatchInfo(strOrgCd, strEmpNo, strEmpNm, strDispOrgCd, strStdt, strEndt, strRegEmpNo);

                                SetDDLRegMonth();
                                GetDataList();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Save", "alert('파견내역이 등록되었습니다.');InitValue();", true);
                            }
                        }
                    }
                }

                updPanel1.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('파견내역 등록 중 오류가 발생했습니다.');", true);
            }
            finally
            {
                if (dsOrg != null)
                    dsOrg.Dispose();

                if (dsList != null)
                    dsList.Dispose();
            }

        }

        protected void ddlRegMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = ddlRegMonth.SelectedValue.Replace("-", "");

                if (!strMonth.Equals(""))
                {
                    SetOrgList(strMonth);

                    using (KTSUser user = new KTSUser())
                    {
                        ds = user.GetUserViewOrgMonth(this.Session["EMPNO"].ToString(), strMonth);
                    }

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strOrgCd = "";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";

                            strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                        }

                        this.hdfOrgCd.Value = strOrgCd;
                        this.txbTeam.Text = Utility.GetOrgNm(this.hdfOrgCd.Value, DtOrgList);
                    }
                    else
                    {
                        this.hdfOrgCd.Value = "";
                        this.txbTeam.Text = "";
                    }
                }
                else
                {
                    this.hdfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelOrg.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
    }
}