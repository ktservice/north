﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PassInfoSts.aspx.cs" Inherits="KTSSolutionWeb.PassInfoSts" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <script type="text/javascript">
        var grid;
        var callGb;
        var Columns1;
        var Columns2;
        var Columns3;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlPassType.ClientID %>").change(function (e) {
                    fn_GetTeam();
                });

                $("#<%= txbTeam.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#btnOrg").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#btnSelect").click(function (e) {
                    var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

                    if (PassType == "") {
                        alert("구분을 선택해주세요.");
                    } else {
                        fn_Search();
                    }
                });

                $("#gridExcel").click(function (e) {
                    var name = "PASS현황통계";
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Cellvalue = function (cell) {
            var rowData = cell.getRow().getData();
            var fieldNm = cell.getColumn().getField();
            var passDt = rowData.PASSDT;
            var passType = rowData.PASSTYPENM;

            var value = cell.getValue();

            if (fieldNm == "NUM" || fieldNm == "PASSDT") {
                if (passDt == "9999-12" || passType == "") {
                    value = "";
                }
            }
            else if (fieldNm == "ORGNM") {
                if (passDt == "9999-12") {
                    value = "계";
                }
            }

            return value;
        };

        var fn_CellDt = function (cell) {
            var rowData = cell.getRow().getData();
            var passDt = cell.getValue();
            var passType = rowData.PASSTYPENM;

            var value;

            if (passDt == "9999-12" || passType == "") {
                value = "";
            } else {
                value = passDt;
            }

            return value;
        };

        var fn_CellOrg = function (cell) {
            var rowData = cell.getRow().getData();
            var passDt = rowData.PASSDT;

            var value;

            if (passDt == "9999-12") {
                value = "계";
            } else {
                value = cell.getValue();
            }

            return value;
        };

        var fn_CellPopup = function (cell) {
            var cellValue = cell.getValue();
            var fieldNm = cell.getColumn().getField();
            var rowData = cell.getRow().getData();

            var passdt = rowData.PASSDT;
            var empno = rowData.EMPNO;
            var orgcd = rowData.ORGCD;
            var passtype = rowData.PASSTYPE;
            var svctype = rowData.SVCTYPE;
            var stdt = rowData.STDT.substring(0, 10);
            var endt = rowData.ENDT.substring(0, 10);
            if (passdt != "" && passdt != "9999-12") {
                if (passdt > stdt.substring(0, 7)) {
                    stdt = passdt + "-01";
                }

                if (passdt < endt.substring(0, 7)) {
                    var en = [];
                    en = passdt.split('-');

                    var endate = new Date(parseInt(en[0]), parseInt(en[1]), 0);

                    var year = endate.getFullYear(); // 년도
                    var month = ("0" + (endate.getMonth() + 1)).slice(-2);  // 월
                    var date = ("0" + endate.getDate()).slice(-2);  // 날짜

                    endt = year + "-" + month + "-" + date;
                }
            }

            stdt = stdt + " 00:00:00";
            endt = endt + " 23:59:59";

            var valType = "";

            if (fieldNm == "PASSCNT") {
                valType = "P";
            }
            else if (fieldNm == "SHOOTINGCNT") {
                valType = "T";
            } else if (fieldNm == "SUCCCNT") {
                valType = "S";
            } else if (fieldNm == "FAILCNT") {
                valType = "F";
            }

            var selType = "";

            if ($("#chkLv1").is(":checked")) {
                selType = "1LV";
            }
            else if ($("#chkLv2").is(":checked")) {
                selType = "2LV";
            }
            else if ($("#chkLv3").is(":checked")) {
                selType = "3LV";
            }
            else if ($("#chkLv4").is(":checked")) {
                selType = "TRTEMPNO";
            }
            else {
                selType = "EMPNO";
            }

            var value;
            if (cellValue == "0" || cellValue == "-" || valType == "") {
                value = cellValue;
            } else {
                var param = empno + "|" + orgcd + "|" + passtype + "|" + svctype + "|" + stdt + "|" + endt + "|" + valType + "|" + selType;

                value = "<label onclick='fn_DetailPopup(\"" + param + "\");' style='cursor:pointer;color:blue;'>" + cellValue + "</label>";
            }

            return value;
        };

        var fn_GridInit = function () {

            Columns1 = [
                { headerHozAlign: "center", hozAlign: "center", title: "순번", field: "NUM", formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "년월", field: "PASSDT", formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", width: 400, formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO" },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM" },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분", field: "PASSTYPENM" },
                { headerHozAlign: "center", hozAlign: "center", title: "상품", field: "SVCTYPE" },
                { headerHozAlign: "center", hozAlign: "center", title: "PASS건", field: "PASSCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅건", field: "SHOOTINGCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "성공건", field: "SUCCCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "실패건", field: "FAILCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", field: "ORGCD", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSTYPE", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "STDT", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "ENDT", visible: false }
            ];

            Columns2 = [
                { headerHozAlign: "center", hozAlign: "center", title: "순번", field: "NUM", formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "년월", field: "PASSDT", formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "소속", field: "ORGNM", width: 400, formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자사번", field: "EMPNO" },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자이름", field: "EMPNM" },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분", field: "PASSTYPENM" },
                { headerHozAlign: "center", hozAlign: "center", title: "상품", field: "SVCTYPE" },
                { headerHozAlign: "center", hozAlign: "center", title: "PASS건", field: "PASSCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅건", field: "SHOOTINGCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "성공건", field: "SUCCCNT", formatter: fn_CellPopup},
                { headerHozAlign: "center", hozAlign: "center", title: "실패건", field: "FAILCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", field: "ORGCD", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSTYPE", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "STDT", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "ENDT", visible: false }
            ];

            Columns3 = [
                { headerHozAlign: "center", hozAlign: "center", title: "순번", field: "NUM", formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "년월", field: "PASSDT", formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "조직명", field: "ORGNM", width: 400, formatter: fn_Cellvalue },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분", field: "PASSTYPENM" },
                { headerHozAlign: "center", hozAlign: "center", title: "상품", field: "SVCTYPE" },
                { headerHozAlign: "center", hozAlign: "center", title: "PASS건", field: "PASSCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅건", field: "SHOOTINGCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "성공건", field: "SUCCCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "실패건", field: "FAILCNT", formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", field: "ORGCD", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSTYPE", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "STDT", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "ENDT", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns1;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값

            //var ValueEl = document.getElementById("sValue");

            //$("#sValue").change(function (e) {
            //    ktsGrid.setFilter('ADDREQWAITCNT', ValueEl.value, 0);
            //});
        }

        var fn_Load = function () {
            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
            var date = ("0" + today.getDate()).slice(-2);  // 날짜

            var todayDate = year + '-' + month + '-' + date;
            
            $("#txbStDt").val(todayDate);
            $("#txbEnDt").val(todayDate);

            //fn_Search();
        }

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                var org = json.split('|');
                var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

                if (org.length == 4) {
                    $("#<%= hfAuthOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);

                    $("#<%= txbTeam.ClientID %>").off("click");
                    $("#btnOrg").off("click");

                    $("#<%= txbTeam.ClientID %>").click(function (e) {
                        PopupPassOrgTree(PassType, org[2], org[3]);
                    });

                    $("#btnOrg").click(function (e) {
                        PopupPassOrgTree(PassType, org[2], org[3]);
                    });
                }
            }
            callGb = '';
        }

        var fn_GetTeam = function () {
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

            if (PassType != '') {
                var OrgCd = $("#<%= hfAuthOrgCd.ClientID %>").val();

                var aData = [];
                aData[0] = PassType;
                aData[1] = OrgCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                callGb = 'GetTeam';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hfAuthOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');

                $("#<%= txbTeam.ClientID %>").off("click");
                $("#btnOrg").off("click");

                $("#<%= txbTeam.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#btnOrg").click(function (e) {
                    alert("구분을 선택해주세요.");
                });
            }
        }

        var fn_Search = function () {
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();
            var OrgCd = $("#<%= hfAuthOrgCd.ClientID %>").val();
            var EmpNo = $("#txbEmpNo").val();
            var StDt = $("#txbStDt").val() + " 00:00:00";
            var EnDt = $("#txbEnDt").val() + " 23:59:59";
            var rbVal = $('input[name=rbDateChk]:checked').val();
            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
            var chkMonth = year + '-' + month;

            if (PassType == "") {
                alert("구분을 선택해주세요.");
            } else if (rbVal == "month" && chkMonth != EnDt.substring(0, 7)) {
                alert('당월 현재 체크 조회 시 이번 달만 조회 가능합니다.');
            } else {
                var selType;

                if ($("#chkLv1").is(":checked")) {
                    selType = "1LV";
                    ktsGrid.setColumns(Columns3);
                }
                else if ($("#chkLv2").is(":checked")) {
                    selType = "2LV";
                    ktsGrid.setColumns(Columns3);
                }
                else if ($("#chkLv3").is(":checked")) {
                    selType = "3LV";
                    ktsGrid.setColumns(Columns3);
                }
                else if ($("#chkLv4").is(":checked")) {
                    selType = "TRTEMPNO";
                    ktsGrid.setColumns(Columns2);
                }
                else {
                    selType = "EMPNO";
                    ktsGrid.setColumns(Columns1);
                }

                //입력 파라미터
                var aData = [];

                aData[0] = PassType;
                aData[1]= OrgCd.replace(/,/gi, "^");
                aData[2] = EmpNo;
                aData[3] = StDt;
                aData[4] = EnDt;
                aData[5] = selType;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

        var fn_DetailPopup = function (val) {

            var nWidth = 1200;
            var nHeight = 628;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pVal: val
            };

            var Popupform = createForm("/Pass/PassInfoDetailPopup", param);
            
            Popupform.target = "PassDetail";
            var win = window.open("", "PassDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }

        function SetPassOrgCd(orgcd) {
            this.focus();
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

            var aData = [];
            aData[0] = PassType;
            aData[1] = orgcd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);

            this.focus();
        }

        function SetDispEmpNo(orgcd, empno, empnm) {
            $("#txbEmpNo").val(empno);
            $("#txbEmpNm").val(empnm);
        }

        function rbDateCheck(obj) {
            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
            var date = ("0" + today.getDate()).slice(-2);  // 날짜

            var todayDate = year + '-' + month + '-' + date;

            if (obj.value == "date") {
                $("#txbStDt").show();

                $("#txbStDt").val(todayDate);
                $("#txbEnDt").val(todayDate);
            }
            else {
                $("#txbStDt").hide();

                $("#txbStDt").val(todayDate.substring(0, 8) + "01");
                $("#txbEnDt").val(todayDate);
            }
        }

        function chkLvCheck(obj) {
            if (obj.value == "lv1") {
                $("#chkLv2").prop("checked", false);
                $("#chkLv3").prop("checked", false);
                $("#chkLv4").prop("checked", false);
            }
            else if (obj.value == "lv2") {
                $("#chkLv1").prop("checked", false);
                $("#chkLv3").prop("checked", false);
                $("#chkLv4").prop("checked", false);
            }
            else if (obj.value == "lv3") {
                $("#chkLv1").prop("checked", false);
                $("#chkLv2").prop("checked", false);
                $("#chkLv4").prop("checked", false);
            }
            else {
                $("#chkLv1").prop("checked", false);
                $("#chkLv2").prop("checked", false);
                $("#chkLv3").prop("checked", false);
            }
        }

        function PopupPassOrgTree(passtype, operator, upperyn) {

            var orgcd = $("#<%= hfAuthOrgCd.ClientID %>").val();

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pPASSTYPE: passtype,
                pOPERATOR: operator,
                pUPPERYN: upperyn
            };

            var Popupform = createForm("/Common/OrgTree_Pass", param);

            Popupform.target = "OrgTree_PassAuth";
            pop3 = window.open("", "OrgTree_PassAuth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop3.focus();
        }

        function PopupEmpUser() {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>   
				<span class="optionbox">
                    <asp:DropDownList ID="ddlPassType" style="width:150px;" AutoPostBack="false" runat="server"></asp:DropDownList>
                </span>    
                <span class="inpbox first">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="hfAuthOrgCd" runat="server" />
                    <asp:HiddenField ID="hfUpperYn" runat="server" />
				    <button id="btnOrg" runat="server" type="button" class="btn-plus" title="검색">+</button>
				</span>
				<span class="inpbox">
					<label>사번</label>
                    <input type="text" id="txbEmpNo" style="width:100px;margin-right:10px" onclick="PopupEmpUser();" readonly="readonly" />
					<label>이름</label>
                    <input type="text" id="txbEmpNm" style="width:100px;" onclick="PopupEmpUser();" readonly="readonly" />
					<button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                </span>
                <hr class="blank" />
                <span class="inpbox">
					<input type="radio" name="rbDateChk" id="rbDate" value="date" runat="server" checked="true" onclick="rbDateCheck(this);" /><label>일자</label>
					<input type="radio" name="rbDateChk" id="rbMonth" value="month" runat="server" onclick="rbDateCheck(this);" /><label>당월 현재</label>
                    <input type="text" id="txbStDt" class="date" style="width:120px" readonly="readonly" />
                    <em>~</em>
                    <input type="text" id="txbEnDt" class="date" style="width:120px" readonly="readonly" />
                </span>
                <span class="inpbox">
                    <input type="checkbox" name="chkLvChk" id="chkLv1" value="lv1" onclick="chkLvCheck(this);" /><label>1레벨(본부)</label>
                    <input type="checkbox" name="chkLvChk" id="chkLv2" value="lv2" onclick="chkLvCheck(this);" /><label>2레벨(지사)</label>
                    <input type="checkbox" name="chkLvChk" id="chkLv3" value="lv3" onclick="chkLvCheck(this);" /><label>3레벨(지점)</label>
                    <input type="checkbox" name="chkLvChk" id="chkLv4" value="trtEmp" onclick="chkLvCheck(this);" /><label>담당자별</label>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조직별 조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
