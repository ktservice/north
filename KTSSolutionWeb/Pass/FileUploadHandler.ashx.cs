﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using KTS.KTSSolution.BSL.Pass;
using KTS.KTSSolution.Framework.Web;

namespace KTSSolutionWeb.Pass
{
    /// <summary>
    /// ReqFIleUploadHandler의 요약 설명입니다.
    /// </summary>
    public class FileUploadHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Stream stream = null;

            try
            {
                HttpFileCollection uploadFiles = context.Request.Files;


                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];
                    string[] strKeys = uploadFiles.AllKeys[i].Split('_');

                    if (strKeys.Length != 2)
                    {
                        throw new Exception("잘못된 접근입니다.");
                    }
                    string strProcId = strKeys[0];
                    string strEmpNo = strKeys[1];

                    stream = postedFIle.InputStream;

                    if (stream.Length == 0)
                        continue;

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    //type체크 필요 
                    int nFileLen = Convert.ToInt32(stream.Length);

                    byte[] byteFile = new byte[nFileLen];

                    stream.Read(byteFile, 0, nFileLen);

                    using (PassMgmt mgmt = new PassMgmt())
                    {
                        mgmt.InsPassNShootingFile(strProcId, strFileNm, byteFile, strFileType, nFileLen, strEmpNo);
                    }

                    stream.Close();
                    stream.Dispose();
                }

                context.Response.Write(returnValue("00", ""));
            }
            catch(Exception ex)
            {
                context.Response.Write(returnValue("01", ex.Message));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string returnValue(string strResultCd, string strResultMsg)
        {
            string result = string.Empty;

            DataTable dt = new DataTable();

            dt.Columns.Add("SERVICE_ERROR_CD");
            dt.Columns.Add("SERVICE_ERROR_MSG");

            DataRow dr = dt.NewRow();

            dr["SERVICE_ERROR_CD"] = strResultCd;
            dr["SERVICE_ERROR_MSG"] = strResultMsg;

            dt.Rows.Add(dr);

            result = Utility.DataTableToJson(dt);

            return result;
        }
    }
}