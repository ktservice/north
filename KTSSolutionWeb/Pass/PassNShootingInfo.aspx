﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PassNShootingInfo.aspx.cs" Inherits="KTSSolutionWeb.PassNShootingInfo" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Scripts/Pass/PassControls.js?20250307"></script>
    <link href="/Resource/css/tabulator.min.css" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{cursor:pointer;}
        .tooltip {position: relative;}
        .tooltip .tooltiptxt {visibility: hidden;position: absolute;top: -25px;left: 40px;padding-left:3px;padding-right:3px;border: 0.5px solid black;background-color:white;}
        .tooltip:hover .tooltiptxt {visibility:visible;}
        .tooltip .tooltiptxt2 {visibility: hidden;position: absolute;top: -10px;left: 90px;padding-left:3px;padding-right:3px;border: 0.5px solid black;background-color:white;z-index:999;}
        .tooltip:hover .tooltiptxt2 {visibility:visible;}
        .div-Screenlock {position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;background-color: #ffffff !important;z-index: 99;opacity: 0.7;overflow: hidden;display:none;}
        .div-modal {position: absolute;width: 90%;height: 730px;top: 15%;left: 5%;background: rgb(255, 255, 255);border-radius: 10px;border: 1px solid gray;overflow: hidden;z-index:100;}
        .div-modal-inner {width:90%;margin-left:5%;padding-top:30px;text-align:left;}
        .div-modal-title {margin-top:10px;margin-bottom: 5px;height:35px;overflow:hidden;text-align:left;}
        .div-modal-content {margin-top:10px;margin-bottom: 30px;padding-bottom:15px;height:300px;overflow:auto;text-align:center;}
        .table-modal > thead > tr {border-bottom:1px solid #999;height:24px;table-layout: fixed;}
        .table-modal > thead > tr > th {border-left:1px solid #999;padding-top:2px;text-align:center;}
        .table-modal > tbody > tr {border-bottom:1px solid #999;height:24px;}
        .table-modal > tbody > tr > td {border-left:1px solid #999;padding:2px 5px;text-align:center;}       
        .table-modal > tbody > tr > td > select {display: inline-block;height: 22px;padding: 0 10px 0 5px;border: 1px solid #dfdfdf;border-radius: 2px;background: url('/Resource/images/icon_select_arr.png') #fff no-repeat 100% 50%;line-height: 22px;vertical-align: middle;color: #000;font-size: 14px;}
        .table-modal > tbody > tr > td > input[type="text"] {display: inline-block;height: 22px;padding: 0 10px 0 5px;border: 1px solid #dfdfdf;border-radius: 2px;line-height: 22px;vertical-align: middle;color: #000;font-size: 14px;}
    </style>

    <script type="text/javascript">
        var ktsGrid1;
        var ktsGrid2;
        var grid;
        var callGb;
        var Json_scvGbn;
        var Json_scvList;
        var Json_mobileList;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_GridInit2();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlPassType.ClientID %>").change(function (e) {
                    $("#<%= hdfAuthOrgCd.ClientID %>").val('');
                    fn_GetTeam();
                });

                $("#<%= txbTeam.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#<%= btnOrg.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#btnSelect").click(function (e) {
                    var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

                    if (PassType == "") {
                        alert("구분을 선택해주세요.");
                    } else {
                        fn_Search();
                    }
                });

                $("#gridExcel").click(function (e) {
                    var name = "PASS현황조회";
                    ktsGrid1.download("xlsx", name + ".xlsx", { sheetName: name });
                });

                $("#gridExcel2").click(function (e) {
                    fn_ExcelDetail();
                });
            }
        };

        var fn_GridInit = function () {
            var fn_SignOver = function (e, cell) {
                var rowData = cell.getRow().getData();

                var signData = rowData.IMGDATA;

                if (signData.length > 0) {
                    var img = $("#imgSign");

                    img.attr("src", signData);

                    var curX = e.pageX;
                    var curY = e.pageY - 200;

                    $("#divModal").css({
                        top: curY,
                        left: curX
                    });

                    $("#divModal").show();
                }
            };

            var fn_SignOut = function (e, cell) {
                var img = $("#imgSign");

                img.src = "";
                $("#divModal").hide();
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                //작업자
                { headerHozAlign: "center", hozAlign: "center", title: "접수일자", field: "RCTDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수유형", field: "RCTTYPENM", sorter: "string", minWidth: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "희망상품", field: "HOPESVC", sorter: "string", minWidth: 120, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "PASSSTATNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "접속번호", field: "CONNO", sorter: "string", minWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string", minWidth: 120, cellMouseOver: fn_SignOver, cellMouseOut: fn_SignOut },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처", field: "CUSTTELNO", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "관계", field: "RELATIONS", sorter: "string", minWidth: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "PASSTYPENM", sorter: "string", minWidth: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수상품", field: "SVCTYPE", sorter: "string", minWidth: 80 },
                //SM
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGFULLNM", sorter: "string", minWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원명", field: "EMPNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", minWidth: 90 },
                { headerHozAlign: "center", hozAlign: "center", title: "전화번호", field: "MOBILE", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", minWidth: 100, visible: false },
                //작업자
                { headerHozAlign: "center", hozAlign: "center", title: "요청사항", field: "NOTE1", sorter: "string", minWidth: 200, width: 400 },
                //SM
                { headerHozAlign: "center", hozAlign: "center", title: "접수자소속", field: "TRTORGNM", sorter: "string", minWidth: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자사번", field: "TRTEMPNO", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "TRTEMPNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅상품", field: "SHTSVC", sorter: "string", minWidth: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅일", field: "SHOOTINGDT", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성공여부", field: "SUCCYNNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실패사유", field: "FAILNOTE", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTE2", sorter: "string", minWidth: 200, width: 400 },
                { headerHozAlign: "center", hozAlign: "center", field: "PROCID", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSTYPE", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "PASSSTAT", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "RCTTYPE", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "SUCCYN", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "CUSTSIGNYN", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "IMGDATA", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "UPLOADYN", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "MONTHFIN", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "CHOICEYN", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "FILEYN", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            var rowDoubleClick = function (e, row) {
                var rowData = row.getData();
                var procId = rowData.PROCID;

                fn_ModalShow(procId);
            };

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDblClick('ktsGrid1', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            ktsGrid1 = ktsGrid;
        };

        var fn_GridInit2 = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                //작업자
                { headerHozAlign: "center", hozAlign: "center", title: "접수일자", field: "RCTDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수유형", field: "RCTTYPENM", sorter: "string", minWidth: 120, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "희망상품", field: "HOPESVC", sorter: "string", minWidth: 120, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "PASSSTATNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "접속번호", field: "CONNO", sorter: "string", minWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string", minWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "연락처", field: "CUSTTELNO", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "관계", field: "RELATIONS", sorter: "string", minWidth: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "PASSTYPENM", sorter: "string", minWidth: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수상품", field: "SVCTYPE", sorter: "string", minWidth: 80 },
                //SM
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGFULLNM", sorter: "string", minWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원명", field: "EMPNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", minWidth: 90 },
                { headerHozAlign: "center", hozAlign: "center", title: "전화번호", field: "MOBILE", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", minWidth: 100, visible: false },
                //작업자
                { headerHozAlign: "center", hozAlign: "center", title: "요청사항", field: "NOTE1", sorter: "string", minWidth: 200, width: 400 },
                //SM
                { headerHozAlign: "center", hozAlign: "center", title: "접수자소속", field: "TRTORGNM", sorter: "string", minWidth: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자사번", field: "TRTEMPNO", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "TRTEMPNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅일", field: "SHOOTINGDT", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성공여부", field: "SUCCYNNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실패사유", field: "FAILNOTE", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTE2", sorter: "string", minWidth: 200, width: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅상품", field: "SHTSVC", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분", field: "SVCGBN", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품명", field: "SVCLIST", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "모델", field: "MODEL", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통전화번호", field: "TELNO", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "수량", field: "CNT", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "결합여부", field: "COMBINEYN", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "반값초이스", field: "HALFPRICEYN", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "회선구분", field: "LINEGBN", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "약정갱신", field: "REBEWYN", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "업셀링", field: "UPCELLINGYN", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스계약번호", field: "SCN", sorter: "string", minWidth: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTE", sorter: "string", minWidth: 150 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;

            grid.gridDefault('ktsGrid2', 'fitColumns', true, 'local');; // div의 grid id 값

            ktsGrid2 = ktsGrid;
        };

        var fn_Load = function () {
            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
            var date = ("0" + today.getDate()).slice(-2);  // 날짜

            var todayDate = year + '-' + month + '-' + date;

            $("#txbStDt").val(todayDate);
            $("#txbEnDt").val(todayDate);

            Json_scvGbn = $("#<%= hdfSvcGbn.ClientID %>").val();
            Json_scvList = $("#<%= hdfSvcList.ClientID %>").val();
            Json_mobileList = $("#<%= hdfMobileList.ClientID %>").val();
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                if (ktsGridId == 'ktsGrid1') {
                    var pagenum = ktsGrid1.getPage();

                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid1.setData(json);
                    ktsGridRowCount(ktsGrid1.getDataCount());

                    var maxPageNum = ktsGrid1.getPageMax();

                    if (maxPageNum < pagenum) {
                        ktsGrid1.setPage(maxPageNum);
                    }
                    else {
                        ktsGrid1.setPage(pagenum);
                    }
                }
                else {
                    ktsRowCountClass = 'searchrowcount';
                    ktsGrid2.setData(json);
                    ktsGridRowCount(ktsGrid2.getDataCount());
                    
                    var name = "PASS현황 상세조회";
                    ktsGrid2.download("xlsx", name + ".xlsx", { sheetName: name });
                }
            }
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                callGb = '';

                var org = json.split('|');
                var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

                if (org.length == 4) {
                    $("#<%= hdfAuthOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);
                    $("#<%= hdfUpperYn.ClientID %>").val(org[3]);

                    $("#<%= txbTeam.ClientID %>").off("click");
                    $("#<%= btnOrg.ClientID %>").off("click");

                    if (org[3] == 'Y') {
                        var now = new Date();
                        var year = now.getFullYear();
                        var month = String(now.getMonth() + 1).padStart(2, '0');

                        var today = year + "-" + month;

                        $("#txbMonthFin").val(today);
                        $("#btnMonthFinY").removeAttr("disabled");
                        $("#btnMonthFinN").removeAttr("disabled");
                        $("#spanMonthFin").show();
                    }
                    else {
                        $("#txbMonthFin").val("");
                        $("#btnMonthFinY").attr("disabled", "disalbed");
                        $("#btnMonthFinN").attr("disabled", "disalbed");
                        $("#spanMonthFin").hide();
                    }

                    $("#<%= txbTeam.ClientID %>").click(function (e) {
                        PopupPassOrgTree(PassType, org[2], org[3]);
                    });

                    $("#<%= btnOrg.ClientID %>").click(function (e) {
                        PopupPassOrgTree(PassType, org[2], org[3]);
                    });
                }
            }
            else if (callGb == 'SetPassInfo') {
                callGb = '';
                var passType = $("#lblPassType").text();
                var trtEmpChk = $("#lblTrtEmpChk").text();

                if (passType != "SPACE" && passType != "BIZ") {
                    if ($("#tbody").children('tr').length > 0 && trtEmpChk == "Y") {
                        var procid = $("#<%= hdfProcId.ClientID %>").val();

                        fn_SetShtSvc(procid);
                    }
                    else {
                        alert('PASS 정보가 저장되었습니다.');
                        fn_ModalClose();
                    }
                }
                else {
                    alert('PASS 정보가 저장되었습니다.');
                    fn_ModalClose();
                }
            }
            else if (callGb == 'DelPassInfo') {
                callGb = '';

                alert('PASS 정보가 삭제되었습니다.');
                fn_ModalClose();
            }
            else if (callGb == 'GetShtSvc') {
                callGb = '';
                var procid = $("#<%= hdfProcId.ClientID %>").val();

                fn_ControlBind_ShtSvc(json, procid);
            }
            else if (callGb == 'SetShtSvc') {
                callGb = '';

                alert('PASS 정보가 저장되었습니다.');
                fn_ModalClose();
            }
            else if (callGb == 'DelShtSvc') {
                callGb = '';

                //상품 목록에서 해당상품 삭제
                alert('슈팅상품이 삭제되었습니다.');
                fn_GetShtSvc();
            }
            else if (callGb == 'SetPassTrtEmpNo') {
                callGb = '';

                //담당자 부분 컨트롤 조정
                var empNo = $("#<%= hdfTrtEmpNo.ClientID %>").val();

                $("#btnTrtEmpNo").off("click");
                $("#btnChoice").off("click");

                $("#lblChoiceYn").html("선택완료");
                $("#lblTrtEmpNo").html(empNo);
                $("#lblTrtEmpChk").html('Y');

                $("#spanChoice").show();
                $("#lblChoiceYn").show();
                $("#btnChoice").hide();
                $("#spanTrtEmpNo").hide();
                $("#ddlSuccYn").removeAttr("disabled");

                fn_ShowRegSvc();
            }
            else if (callGb == 'MonthFinOk_Y') {
                callGb = '';

                alert('월마감 되었습니다.');
                fn_Search();
            }
            else if (callGb == 'MonthFinOk_N') {
                callGb = '';

                alert('월마감이 취소 되었습니다.');
                fn_Search();
            }

        };

        var fn_GetTeam = function () {
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

            if (PassType != '') {
                var OrgCd = $("#<%= hdfAuthOrgCd.ClientID %>").val();

                var aData = [];
                aData[0] = PassType;
                aData[1] = OrgCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                callGb = 'GetTeam';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hdfAuthOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');

                $("#<%= txbTeam.ClientID %>").off("click");
                $("#<%= btnOrg.ClientID %>").off("click");

                $("#<%= txbTeam.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#<%= btnOrg.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });
            }
        };

        var fn_Search = function () {
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();
            var OrgCd = $("#<%= hdfAuthOrgCd.ClientID %>").val();
            var EmpNo = $("#txbEmpNo").val();
            var StDt = $("#txbStDt").val();// + " 00:00:00";
            var EnDt = $("#txbEnDt").val();// + " 23:59:59";
            var PassStat = $("#<%= ddlPassStat.ClientID %> option:selected").val();
            var SuccYn = $("#<%= ddlShootingYn.ClientID %> option:selected").val();

            if (PassType == "") {
                alert("구분을 선택해주세요.");
            } else {

                if (PassType == "MOT") {
                    ktsGrid1.hideColumn("HOPESVC");
                    ktsGrid1.showColumn("RCTTYPENM");
                    ktsGrid1.showColumn("CONNO");
                    ktsGrid1.showColumn("SVCTYPE");              
                }
                else {
                    ktsGrid1.hideColumn("CONNO");
                    ktsGrid1.showColumn("HOPESVC");
                    ktsGrid1.hideColumn("RCTTYPENM");
                    ktsGrid1.hideColumn("SVCTYPE");
                }

                //입력 파라미터
                var aData = [];

                aData[0] = PassType;
                aData[1] = OrgCd.replace(/,/gi, "^");
                aData[2] = EmpNo;
                aData[3] = StDt;
                aData[4] = EnDt;
                aData[5] = PassStat;
                aData[6] = SuccYn;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid1', false);

                $(".table-search").css('display', 'block');
            }
        };

        var fn_ExcelDetail = function () {
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();
            var OrgCd = $("#<%= hdfAuthOrgCd.ClientID %>").val();
            var EmpNo = $("#txbEmpNo").val();
            var StDt = $("#txbStDt").val();// + " 00:00:00";
            var EnDt = $("#txbEnDt").val();// + " 23:59:59";
            var PassStat = $("#<%= ddlPassStat.ClientID %> option:selected").val();
            var SuccYn = $("#<%= ddlShootingYn.ClientID %> option:selected").val();

            if (PassType == "") {
                alert("구분을 선택해주세요.");
            } else {

                if (PassType == "MOT") {
                    ktsGrid2.hideColumn("HOPESVC");
                    ktsGrid2.showColumn("RCTTYPENM");
                    ktsGrid2.showColumn("CONNO");
                    ktsGrid2.showColumn("SVCTYPE");
                }
                else {
                    ktsGrid2.hideColumn("CONNO");
                    ktsGrid2.showColumn("HOPESVC");
                    ktsGrid2.hideColumn("RCTTYPENM");
                    ktsGrid2.hideColumn("SVCTYPE");
                }

                //입력 파라미터
                var aData = [];

                aData[0] = PassType;
                aData[1] = OrgCd.replace(/,/gi, "^");
                aData[2] = EmpNo;
                aData[3] = StDt;
                aData[4] = EnDt;
                aData[5] = PassStat;
                aData[6] = SuccYn;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetDetailData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid2', false);

                $(".table-search").css('display', 'block');
            }
        };

        var fn_PassChoice = function (procid) {
            if (confirm("PASS를 선택 하시겠습니까?")) {
                if (procid == "") {
                    alert("잘못된 접근입니다.");
                }
                else {
                    var aData = [];

                    aData[0] = procid;

                    callGb = 'SetPassTrtEmpNo';

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetPassTrtEmpNo");

                    ajaxCall(jsonData, ajaxUrl, false);
                }

            }
            else {
                return false;
            }
        };

        var fn_SetPassInfo = function (procid) {
            if (confirm("PASS를 저장 하시겠습니까?")) {
                if (procid == "") {
                    alert("잘못된 접근입니다.");
                }
                else {
                    var passType = $("#lblPassType").text();
                    var conNo = $("#txbConNo").val();
                    var custTel = $("#txbCustTelNo").val();
                    var succYn = $("#ddlSuccYn option:selected").val();
                    var passStat = $("#lblPassStat").text();
                    var trtEmpNo = $("#lblTrtEmpNo").text();
                    var note2 = $("#txbNote2").val();
                    var failNote = $("#txbFailNote").val();
                    var bReg = true;

                    if (succYn == "Y") {
                        if (trtEmpNo == "") {
                            alert("담당자가 지정되지 않아 성공 처리를 할 수 없습니다.");
                            bReg = false;
                        }
                        else if (passType != "SPACE" && passType != "BIZ") {
                            if ($("#tbody").children('tr').length == 0) {
                                bReg = false;
                            }
                            else {
                                $.each($("#tbody").children('tr'), function () {
                                    var lblNo = $(this).find("label[for='lblNo']");
                                    var id = lblNo.text();
                                    var svcType = $("#ddlSvcType_" + id + " option:selected").val();

                                    if (svcType != "") {
                                        var svcGbn = $("#ddlSvcGbn_" + id + " option:selected").val();
                                        var svcList = $("#ddlSvcList_" + id + " option:selected").val();
                                        var mobile = $("#ddlMobile_" + id + " option:selected").val();
                                        var tel = $("#txbTel_" + id).val();

                                        if (svcGbn == "" || svcList == "") {
                                            alert("슈팅 상품을 추가해야 성공 처리가 가능합니다.");
                                            bReg = false;
                                            return false;
                                        }

                                        if (svcType == "모바일") {
                                            if (mobile == "" || tel == "") {
                                                alert("상품구분/상품명이 누락되었습니다.(모바일은 모델명/개통전화번호까지 필수 입력 항목입니다.)");
                                                bReg = false;
                                                return false;
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                    else if (succYn == "N") {
                        if (failNote == "") {
                            bReg = false;
                            alert("실패 처리 시 실패사유를 입력해야 합니다.");
                        }
                    }

                    if (bReg) {
                        var aData = [];

                        aData[0] = procid;
                        aData[1] = conNo;
                        aData[2] = custTel;
                        aData[3] = succYn;
                        aData[4] = passStat;
                        aData[5] = trtEmpNo;
                        aData[6] = note2;
                        aData[7] = failNote;

                        callGb = "SetPassInfo";

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = ajaxCallGetUrl(location.href, "SetPassInfo");

                        ajaxCall(jsonData, ajaxUrl, false);
                    }
                }
            }
            else {
                return false; 
            }
        };

        var fn_GetShtSvc = function (procid) {
            var procid = $("#<%= hdfProcId.ClientID %>").val();

            var aData = [];

            aData[0] = procid;

            callGb = "GetShtSvc";

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, "GetShtSvc");

            ajaxCall(jsonData, ajaxUrl, false);
        }

        var fn_SetShtSvc = function (procid) {
            if (procid == "") {
                alert("잘못된 접근입니다.");
            }
            else {
                var tbody = $("#tbody");
                var aData = [];
                let i = 0;

                $.each(tbody.children('tr'), function () {
                    var aDataList = [];

                    var lblNo = $(this).find("label[for='lblNo']");

                    var id = lblNo.text();

                    var seq = $("#lblSeq_" + id).text();
                    var svcType = $("#ddlSvcType_" + id + " option:selected").val();

                    if (svcType != "") {
                        var svcGbn = $("#ddlSvcGbn_" + id + " option:selected").val();
                        var svcList = $("#ddlSvcList_" + id + " option:selected").val();
                        var mobile = $("#ddlMobile_" + id + " option:selected").val();
                        var tel = $("#txbTel_" + id).val();
                        var cnt = $("#txbCnt_" + id).val();

                        var combineYn = "N";
                        if ($("#chbCombine_" + id).is(":checked")) {
                            combineYn = "Y";
                        }

                        var halfPriceYn = "N";
                        if ($("#chbHalfPrice_" + id).is(":checked")) {
                            halfPriceYn = "Y";
                        }

                        var line = $("#ddlLine_" + id + " option:selected").val();

                        var rebewYn = "N";
                        if ($("#chbRebew_" + id).is(":checked")) {
                            rebewYn = "Y";
                        }

                        var upCellingYn = "N";
                        if ($("#chbUpCelling_" + id).is(":checked")) {
                            upCellingYn = "Y";
                        }

                        var scn = $("#txbScn_" + id).val();
                        var note = $("#txbNote_" + id).val();

                        aDataList[0] = procid;
                        aDataList[1] = seq;
                        aDataList[2] = svcType;
                        aDataList[3] = svcGbn;
                        aDataList[4] = svcList;
                        aDataList[5] = mobile;
                        aDataList[6] = tel;
                        aDataList[7] = cnt;
                        aDataList[8] = combineYn;
                        aDataList[9] = halfPriceYn;
                        aDataList[10] = line;
                        aDataList[11] = rebewYn;
                        aDataList[12] = upCellingYn;
                        aDataList[13] = scn;
                        aDataList[14] = note;

                        aData[i] = aDataList;

                        i++;
                    }
                });

                if (aData.length > 0) {
                    callGb = "SetShtSvc";

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetShtSvc");

                    ajaxCall(jsonData, ajaxUrl, false);
                }
            }
        };

        var fn_DelShtSvc = function (id) {
            var procid = $("#<%= hdfProcId.ClientID %>").val();
            var svcid = $("#lblSeq_" + id).text();

            if (procid != "" && svcid == "") {
                $("#tr_" + id).remove();
            }
            else {
                if (confirm("슈팅상품 정보를 삭제 하시겠습니까?")) {
                    if (procid == "" || svcid == "") {
                        alert("잘못된 접근입니다.");
                    }
                    else {
                        var aData = [];

                        aData[0] = procid;
                        aData[1] = svcid;

                        callGb = "DelShtSvc";

                        var jsonData = JSON.stringify({ aData: aData });

                        //WebMethod Url
                        var ajaxUrl = ajaxCallGetUrl(location.href, "DelShtSvc");

                        ajaxCall(jsonData, ajaxUrl, false);
                    }
                }
                else {
                    return false;
                }
            }
        };

        var fn_DelPassInfo = function (procid, passType) {
            if (confirm("PASS를 삭제 하시겠습니까?")) {
                if (procid == "") {
                    alert("잘못된 접근입니다.");
                }
                else {
                    var aData = [];

                    aData[0] = procid;
                    aData[1] = passType;

                    callGb = "DelPassInfo";

                    var jsonData = JSON.stringify({ aData: aData });
                     
                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "DelPassInfo");

                    ajaxCall(jsonData, ajaxUrl, false);
                }
            }
            else {
                return false;
            }
        };

        var fn_RegMonthFin = function (finYn) {
            var confMsg = "";

            if (finYn == 'Y') {
                confMsg = "월마감 하시겠습니까?";
            }
            else {
                confMsg = "당일 마감한 PASS 건 만 취소 됩니다.월마감을 취소 하시겠습니까?";
            }            
                
            if (confirm(confMsg)) {
                var passType = $("#<%= ddlPassType.ClientID %> option:selected").val();
                var empNo = $("#<%= hdfTrtEmpNo.ClientID %>").val();
                var stDt = $("#txbMonthFin").val();

                if (passType == "" && empNo == "" && stDt == "") {
                    alert("잘못된 접근입니다.");
                }
                else {
                    var aData = [];

                    aData[0] = passType;
                    aData[1] = empNo;
                    aData[2] = stDt + "-01";
                    aData[3] = finYn;

                    callGb = "MonthFinOk_" + finYn;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "SetMonthFin");

                    ajaxCall(jsonData, ajaxUrl, false);
                }
            }
            else {
                return false;
            }
        }

        var PopupPassOrgTree = function (passtype, operator, upperyn) {

            var orgcd = $("#<%= hdfAuthOrgCd.ClientID %>").val();

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pPASSTYPE: passtype,
                pOPERATOR: operator,
                pUPPERYN: upperyn
            };

            var Popupform = createForm("/Common/OrgTree_Pass", param);

            Popupform.target = "OrgTree_PassAuth";
            pop3 = window.open("", "OrgTree_PassAuth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop3.focus();
        };

        var PopupEmpUser = function () {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var fn_PopupPassUser = function(passtype, empno, procid) {

            var sPtype = "EMPNO";

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pPASSTYPE: passtype,
                pEMPNO: empno,
                pPTYPE: sPtype,
                pPROCID: procid
            };

            var Popupform = createForm("/Common/SearchPassOper", param);

            Popupform.target = "SearchPassOper";
            var win = window.open("", "SearchPassOper", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var fn_RegPassInfo = function () {
            var nWidth = 700;
            var nHeight = 750;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Pass/RegPassNShooting", null);

            Popupform.target = "RegPassNShooting";
            var win = window.open("", "RegPassNShooting", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var fn_PopupFiles = function (procid) {
            var nWidth = 700;
            var nHeight = 750;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pPROCID: procid
            };

            var Popupform = createForm("/Pass/RegPassFiles", param);

            Popupform.target = "RegPassFiles";
            var win = window.open("", "RegPassFiles", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();

        };

        var fn_ShowRegSvc = function () {
            var passType = $("#lblPassType").text();

            if (passType != "SPACE" && passType != "BIZ") {
                $('#btnAdd').show();
                $("#thDel").show();

                $.each($("#tbody").children('tr'), function () {
                    var lblNo = $(this).find("label[for='lblNo']");

                    var id = lblNo.text();
                    var svcType = $("#ddlSvcType_" + id + " option:selected").val();

                    $("#ddlSvcType_" + id).removeAttr("disabled");
                    $("#txbCnt_" + id).removeAttr("disabled");
                    $("#chbCombine_" + id).removeAttr("disabled");
                    $("#ddlLine_" + id).removeAttr("disabled");
                    $("#txbScn_" + id).removeAttr("disabled");
                    $("#txbNote_" + id).removeAttr("disabled");

                    $("#tdDel_" + id).show();
                    $("#btnDel_" + id).click(function (e) {
                        fn_DelShtSvc(id);
                    });

                    if (svcType != "") {
                        $("#ddlSvcGbn_" + id).removeAttr("disabled");
                        fn_ControlBind_SvcGbn(id, svcType);
                        $("#txbCnt_" + id).removeAttr("disabled");

                        if (svcType == "모바일") {
                            $("#ddlMobile_" + id).removeAttr("disabled");
                            $("#txbTel_" + id).removeAttr("disabled");
                        }
                        else {
                            $("#chbRebew_" + id).removeAttr("disabled");
                            $("#chbUpCelling_" + id).removeAttr("disabled");
                        }
                    }
                });
            }
        };

        var SetDispEmpNo = function (orgcd, empno, empnm) {
            $("#txbEmpNo").val(empno);
            $("#txbEmpNm").val(empnm);
        };

        var SetPassOrgCd = function (orgcd) {
            this.focus();

            $("#<%= hdfAuthOrgCd.ClientID %>").val(orgcd);
            fn_GetTeam();
        };

        var SetOperCd = function (opercd, opernm, procid) {
            this.focus();

            var nm = opernm.split(' ');
            var orgnm = '';
            var empnm = '';

            if (nm.length > 1) {
                empnm = nm[nm.length - 1];
                orgnm = opernm.replace(" " + empnm, "");
            }

            $("#lblTrtEmpNo").html(opercd);
            $("#lblTrtEmpNm").html(opernm);

            if (opercd.length > 0) {
                fn_ShowRegSvc();
            }
        }

        var fn_ModalShow = function (procid) {
            if (procid == "") {
                alert("잘못된 접근입니다.")
            }
            else {
                $("#<%= hdfProcId.ClientID %>").val(procid);
                var upperYn = $("#<%= hdfUpperYn.ClientID %>").val();
                var empNo = $("#<%= hdfTrtEmpNo.ClientID %>").val();

                $("body").css("overflow", "hidden");

                $("#divScreenLock").show();

                fn_ControlSet_Modal(procid, upperYn, empNo);
            }
        };

        var fn_ModalClose = function () {
            $("#<%= hdfProcId.ClientID %>").val('');
            $("body").css("overflow", "auto");

            $("#divScreenLock").hide();
            $("#divModal1").hide();
            $("#btnTrtEmpNo").off("click");

            $("#lblTrtEmpNo").html('');
            $("#lblTrtEmpNm").html('');
            $("#lblTrtEmpChk").html('');

            fn_ReSearch();
        };

        var fn_ReSearch = function () {
            fn_Search();
        };

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>   
				<span class="optionbox">
					<label>구분</label>
                    <asp:DropDownList ID="ddlPassType" style="width:150px;" AutoPostBack="false" runat="server"></asp:DropDownList>
                </span>    
                <span class="inpbox first">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="hdfAuthOrgCd" runat="server" />
                    <asp:HiddenField ID="hdfUpperYn" runat="server" />
				    <button id="btnOrg" runat="server" type="button" class="btn-plus" title="검색">+</button>
				</span>
				<span class="inpbox">
					<label>사번</label>
                    <input type="text" id="txbEmpNo" style="width:100px;margin-right:10px" onclick="PopupEmpUser();" readonly="readonly" />
					<label>이름</label>
                    <input type="text" id="txbEmpNm" style="width:100px;" onclick="PopupEmpUser();" readonly="readonly" />
					<button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                </span>
				<span id="spanMonthFin" class="inpbox" style="float:right; text-align:right;display:none;" >
					<label>마감 년월</label>
                    <input type="text" id="txbMonthFin" class="month" readonly="readonly" styl="width:120px" />
                    <input type="button" id="btnMonthFinY" class="btn-black" onclick="fn_RegMonthFin('Y');" style="float:right;color:white;width:100px;margin-left:10px" value="월마감" />
                    <input type="button" id="btnMonthFinN" class="btn-black last" onclick="fn_RegMonthFin('N');" style="float:right;color:white;width:100px;margin-left:10px" value="마감취소" />
                </span>
                <hr class="blank" />
                <span class="inpbox">
					<label>접수일</label>
                    <input type="text" id="txbStDt" class="date" style="width:120px" readonly="readonly" />
                    <em>~</em>
                    <input type="text" id="txbEnDt" class="date" style="width:120px" readonly="readonly" />
                </span>
				<span class="optionbox">
					<label>상태</label>
                    <asp:DropDownList ID="ddlPassStat" style="width:250px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pass 대기중" Value="P"></asp:ListItem>
                        <asp:ListItem Text="Shooting 진행중" Value="S"></asp:ListItem>
                        <asp:ListItem Text="Shooting 완료" Value="F"></asp:ListItem>
                    </asp:DropDownList>
                </span>
				<span class="optionbox">
					<label>성공여부</label>
                    <asp:DropDownList ID="ddlShootingYn" style="width:100px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="성공" Value="Y"></asp:ListItem>
                        <asp:ListItem Text="실패" Value="N"></asp:ListItem>
                        <asp:ListItem Text="보류" Value="H"></asp:ListItem>
                        <asp:ListItem Text="신청서미비" Value="R"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조직별 조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="btnRegPass" type="button" class="btn-green" onclick="fn_RegPassInfo();" value="등록" />
                        <input id="gridExcel" type="button" class="btn-green" value="엑셀" />
                        <input id="gridExcel2" type="button" class="btn-green last" value="상세내역 엑셀" />
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div id="ktsGrid1"></div>
            </div>
			<div class="scrollbox" style="display:none;">
                <div id="ktsGrid2"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
        <div id="divModal" style="display:none;position:absolute;width:300px;height:200px;background:rgb(255, 255, 255); border-width:2px;border-color:black;border-radius:10px;">
            <img id="imgSign" src="" style="border:1px solid black; width:99%; height:99%;" />
        </div>
        <!-- S: divModal1 -->
        <div id="divScreenLock" class="div-Screenlock"></div>
	    <div id="divModal1" class="div-modal" style="display:none;">
            <!-- S:popcontents -->
            <div class="div-modal-inner">
                <div class="div-modal-title">
				    <span class="inpbox">
					    <label id="lblRctType">접수유형 :&nbsp;</label>
                        <input type="text" id="txbRctType" style="width:250px;margin-right:10px;background-color:#f5f5f5;" readonly="readonly" />
                    </span>
				    <span class="inpbox">
					    <label>접수일자 :&nbsp;</label>
                        <input type="text" id="txbRctDate" style="width:120px;margin-right:10px;background-color:#f5f5f5;" readonly="readonly" />
                    </span>
				    <span class="inpbox">
					    <label>고객명 :&nbsp;</label>
                        <input type="text" id="txbCusNm" style="width:100px;margin-right:10px;background-color:#f5f5f5;" readonly="readonly" />
                    </span>
				    <span class="inpbox">
					    <label>관계 :&nbsp;</label>
                        <input type="text" id="txbRelations" style="width:100px;margin-right:10px;background-color:#f5f5f5;" readonly="readonly" />
                    </span>
				    <span class="inpbox">
					    <label>PASS시 선택한 상품 :&nbsp;</label>
                        <input type="text" id="txbPassType" style="width:250px;margin-right:10px;background-color:#f5f5f5;" readonly="readonly" />
					    <label id="lblPassType" style="display:none;"></label>   
                    </span>
				    <span class="inpbox">
					    <label>상태 :&nbsp;</label>
                        <input type="text" id="txbPassStat" style="width:100px;margin-right:10px;background-color:#f5f5f5;" readonly="readonly" />
					    <label id="lblPassStat" style="display:none;"></label>                        
                    </span>
                </div>
                <div class="div-modal-title" style="height:100px;">
				    <span class="inpbox">
					    <label>요청사항 :&nbsp;</label>
                        <textarea id="txaNote1" style="width:800px;height:90px;margin-right:10px;padding:5px;border:1px solid #dfdfdf;background-color:#f5f5f5;cursor:default;" readonly="readonly"></textarea>
                    </span>
                </div>
                <div class="div-modal-title" style="margin-bottom:20px;">
				    <span id="spanConNo" class="inpbox" >
					    <label>접속번호 :&nbsp;</label>
                        <input type="text" id="txbConNo" style="width:140px;margin-right:10px" />
                    </span>
				    <span class="inpbox">
					    <label>연락처 :&nbsp;</label>
                        <input type="text" id="txbCustTelNo" style="width:140px;margin-right:10px" />
                    </span>
				    <span class="optionbox">
					    <label>성공여부 :&nbsp;</label>
                        <select id="ddlSuccYn" style="width:100px;margin-right:10px">
                            <option value="" selected="selected">선택하세요</option>
                            <option value="Y">성공</option>
                            <option value="N">실패</option>
                            <option value="H">보류</option>
                            <option value="R">신청서미비</option>
                        </select>
                    </span>
				    <span id="spanChoice" class="inpbox" style="display:none;"><!-- 부서장권한이 아닌 경우 -->
					    <label>PASS건 선택 :&nbsp;</label>
                        <label id="lblChoiceYn"></label>
                        <input id="btnChoice" type="button" class="btn-save" style="width:40px;padding:0;border: 1px solid #2CBBB7;background: #fff;border-radius: 2px;color: #2CBBB7;" value="선택" />
                    </span>
				    <span id="spanTrtEmpNo" class="inpbox" style="display:none;width:700px;"><!-- 부서장권한자일 경우 -->
					    <label>담당자지정 :&nbsp;</label>   
					    <label id="lblTrtEmpNm"></label>     
					    <label id="lblTrtEmpNo" style="display:none;"></label>   
					    <label id="lblTrtEmpChk" style="display:none;"></label>   
                        <input id="btnTrtEmpNo" type="button" class="btn-plus" style="padding-left:5px;"/>
                    </span>
                </div>
                <div class="div-modal-title" style="margin-bottom:20px;">
				    <span class="inpbox">
					    <label>비&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;고 :&nbsp;</label>
                        <input type="text" id="txbNote2" maxlength="500" style="width:400px;margin-right:10px" />
                    </span>
				    <span class="inpbox">
					    <label>실패사유 :&nbsp;</label>
                        <input type="text" id="txbFailNote" maxlength="500" style="width:400px;margin-right:10px" />
                    </span>
                    <input id="btnFiles" type="button" class="btn-save" style="width:100px;height:30px;font-size:15px;display:none;" value="첨부파일보기" />
                </div>
                <input id="btnAdd" type="button" class="btn-save" onclick="fn_SvcAdd();" style="width:40px;height:25px;font-size:15px;display:none;" value="추가" /><br />
                <div id="divDetailSvc" class="div-modal-content">
                    <table class="table-modal">
                        <thead>
                            <tr>
                                <th style="border-left:0px;width:50px;min-width:50px;">순번</th>
                                <th style="width:120px;min-width:120px;">상품분류</th>
                                <th style="width:280px;min-width:280px;">상품구분</th>
                                <th style="width:300px;min-width:300px;">상품명</th>
                                <th style="width:190px;min-width:190px;">모델</th>
                                <th style="width:140px;min-width:120px;">개통전화번호</th>
                                <th style="width:60px;min-width:60px;">수량</th>
                                <th style="width:60px;min-width:60px;">결합</th>
                                <th style="width:80px;min-width:80px;">반값초이스</th>
                                <th style="width:120px;min-width:120px;">회선구분</th>
                                <th style="width:60px;min-width:60px;">약정갱신</th>
                                <th style="width:60px;min-width:60px;">업셀링</th>
                                <th style="width:120px;min-width:120px;">서비스계약번호</th>
                                <th style="width:200px;min-width:200px;">비고</th>
                                <th id="thDel" style="width:80px;min-width:80px;display:none;">삭제</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                    <asp:HiddenField ID="hdfProcId" runat="server" />
                </div>
                <!-- S:btncenter -->
                <div class="btncenter" style="width:100%;text-align:center;">
                    <input id="btnReg" type="button" class="btn-green" value="저장" />
                    <input id="btnDel" type="button" class="btn-red" value="삭제" />
                    <input id="btnClose" type="button" class="btn-black" onclick="fn_ModalClose();" value="닫기" />
                </div>
                <!-- //E:btncenter -->
            </div>
	    </div>
        <!-- E: divModal1 -->
    </div>
    
    <asp:HiddenField ID="hdfTrtEmpNo" runat="server" />
    <asp:HiddenField ID="hdfSvcGbn" runat="server" />
    <asp:HiddenField ID="hdfSvcList" runat="server" />
    <asp:HiddenField ID="hdfMobileList" runat="server" />
	<!-- E: contentsarea -->
</asp:Content>
