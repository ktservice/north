﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.Script.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.BSL.Pass;
using KTS.KTSSolution.BSL.Common;

namespace KTSSolutionWeb
{
    public partial class RegPassNShooting : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('잘못된 접근입니다.');window.close();", true);
            }
            else
            {
                this.hdfDeptCd.Value = Session["DEPTCD"].ToString();
                this.hdfEmpNo.Value = Session["EMPNO"].ToString();
                this.hdfEmpNm.Value = Session["EMPNM"].ToString();
            }
        }
        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPassType(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "GETPASSTYPE";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetRctType(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "GETRCTTYPE";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();

            string result = string.Empty;

            string strOrgCd = aData[0];
            string strEmpNm = aData[1];

            using (OrgCodeTree org = new OrgCodeTree())
            {
                ds = org.GetOperOrgList("", "N");
            }

            result = Utility.GetOrgNm(strOrgCd, ds.Tables[0]);

            return result + strEmpNm;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetConsult(List<string> aData)
        {
            DataSet ds = null;

            string result = string.Empty;

            string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            using (PassMgmt pass = new PassMgmt())
            {
                ds = pass.GetPassBizAuthist(strEmpNo);
            }

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    result = Utility.DataTableToJson(ds.Tables[0]);
                }
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetPassNShootingInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PASSNSHOOTINGINFOINSERT";
            string jobGb = "SET";

            string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(""); //모바일 서명
            aData.Add("R");
            aData.Add(strEmpNo);
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        #endregion
    }
}