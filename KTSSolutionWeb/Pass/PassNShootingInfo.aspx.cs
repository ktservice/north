﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Pass;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json.Linq;

namespace KTSSolutionWeb
{
    public partial class PassNShootingInfo : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPassAuthList();

                SetPassSvcInfo();

                this.hdfTrtEmpNo.Value = Session["EMPNO"].ToString();
            }
        }
        #endregion

        #endregion

        #region Method

        #region MenuTree_Load
        /// <summary>
        /// MenuTree_Load
        /// </summary>
        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region SetPassAuthList
        /// <summary>
        /// SetPassAuthList
        /// </summary>
        private void SetPassAuthList()
        {
            DataSet ds = new DataSet();

            try
            {
                string strEmpNo = Session["EMPNO"].ToString();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "GETPASSAUTHINFOBYUSER";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add(strEmpNo);
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlPassType.Items.Add(new ListItem("선택하세요", ""));

                    DataTable dt = ds.Tables[0].AsEnumerable()
                        .GroupBy(
                            row => new
                            {
                                PASSTYPE = row.Field<string>("PASSTYPE"),
                                PASSTYPENM = row.Field<string>("PASSTYPENM")
                            })
                        .Select(g =>
                        {
                            DataRow row = ds.Tables[0].NewRow();

                            row["PASSTYPE"] = g.Key.PASSTYPE;
                            row["PASSTYPENM"] = g.Key.PASSTYPENM;

                            return row;
                        }).CopyToDataTable();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ddlPassType.Items.Add(new ListItem(dt.Rows[i]["PASSTYPENM"].ToString(), dt.Rows[i]["PASSTYPE"].ToString()));
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ValidationCheck", "alert('PASS권한이 없습니다. 관리자에게 문의하세요.');window.location.href = '/Default';", true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion


        #region SetPassSvcInfo
        /// <summary>
        /// SetPassSvcInfo
        /// </summary>
        private void SetPassSvcInfo()
        {
            try
            {
                string resultSvcGbn = string.Empty;
                string resultSvcList = string.Empty;
                string resultMobileList = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "GETPASSSVCGBNINFO";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    resultSvcGbn = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    resultSvcGbn = resultSession;
                }

                resultSession = string.Empty;
                rptCd = "GETPASSSVCLISTINFO";
                jobGb = "GET";

                aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    resultSvcList = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    resultSvcList = resultSession;
                }

                resultSession = string.Empty;
                rptCd = "GETPASSMOBILELISTINFO";
                jobGb = "GET";

                aData = new List<string>();

                aData.Add("F"); //파라미터 추가 시

                dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    resultMobileList = dbTransaction.GetJsonData();
                }
                else //세션이 끊겼을 경우
                {
                    resultMobileList = resultSession;
                }

                this.hdfSvcGbn.Value = resultSvcGbn;
                this.hdfSvcList.Value = resultSvcList;
                this.hdfMobileList.Value = resultMobileList;
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetTeamNm(List<string> aData)
        {
            DataSet ds = new DataSet();
            DataSet dsPass = new DataSet();

            string result = string.Empty;

            string strPassType = aData[0];
            string strOrgCd = aData[1];

            string strAuthOper = "";
            string strUpperYn = "N";

            string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            using (PassMgmt pass = new PassMgmt())
            {
                dsPass = pass.GetPassAuthList(strEmpNo);
            }

            DataRow[] drUpper = dsPass.Tables[0].Select(string.Format("PASSTYPE = '{0}' AND UPPERYN = '{1}'", strPassType, "Y"));

            if (strPassType.Equals("BIZ") || drUpper.Length > 0)
            {
                strAuthOper = strEmpNo;
            }
            else
            {
                strAuthOper = HttpContext.Current.Session["DEPTCD"].ToString();
            }

            DataRow[] dr = dsPass.Tables[0].Select(string.Format("PASSTYPE = '{0}' AND AUTHOPERATOR = '{1}'", strPassType, strAuthOper));

            for (int i = 0; i < dr.Length; i++)
            {
                if (i == 0)
                    strUpperYn = dr[i]["UPPERYN"].ToString();

                if (strOrgCd.Length == 0)
                {
                    if (strOrgCd.Length > 0)
                    {
                        strOrgCd += ",";
                    }

                    strOrgCd += dr[i]["ORGCD"].ToString();
                }
            }

            using (OrgCodeTree org = new OrgCodeTree())
            {
                ds = org.GetOperOrgList("", "N");
            }

            result = Utility.GetOrgNm(strOrgCd, ds.Tables[0]);

            return strOrgCd + "|" + result + "|" + strAuthOper + "|" + strUpperYn;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "GETPASSNSHOOTINGINFO";
            string jobGb = "GET";

            string strTrtEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(strTrtEmpNo);
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetDetailData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "GETPASSNSHOOTINGDETAILINFO";
            string jobGb = "GET";

            string strTrtEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(strTrtEmpNo);
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetPassInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PASSNSHOOTINGINFOUPDATE";
            string jobGb = "SET";

            string strEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(strEmpNo); //파라미터 추가 시
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelPassInfo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PASSNSHOOTINGINFODELETE";
            string jobGb = "SET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetPassTrtEmpNo(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PASSNSHOOTINGCHOICE";
            string jobGb = "SET";

            string strTrtEmpNo = HttpContext.Current.Session["EMPNO"].ToString();

            aData.Add(strTrtEmpNo);
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetShtSvc(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SHOOTINGSVCINFO";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetShtSvc(List<List<string>> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SHOOTINGSVCINFOINSERT";
            string jobGb = "SET";

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                bool bErr = false;
                string procId = "";

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, null);
                resultSession = dbTransaction.SessionCheck();

                foreach (List<string> list in aData)
                {
                    procId = list[0];

                    dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, list);
                    resultSession = dbTransaction.SessionCheck();

                    result = dbTransaction.GetJsonData();

                    JObject jsonObj = JObject.Parse(result.Substring(1, result.Length - 2));
                    string errcd = jsonObj["SERVICE_ERROR_CD"].ToString();

                    if (errcd.Equals("01"))
                    {
                        bErr = true;
                        break;
                    }
                }

                if (bErr)
                {
                    DataTable dt = Utility.DataTableError("01", "슈팅상품 정보 저장 중 오류가 발생했습니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelShtSvc(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "SHOOTINGSVCINFODELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string SetMonthFin(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PASSNSHOOTINGMONTHFINUPDATE";
            string jobGb = "SET";

            string strStDt = aData[2];

            DateTime EnDt = DateTime.ParseExact(strStDt, "yyyy-MM-dd", null);

            string strEnDt = EnDt.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd");

            aData.Add(strEnDt);
            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        
        #endregion
    }
}