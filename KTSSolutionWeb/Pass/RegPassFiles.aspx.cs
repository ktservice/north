﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.Script.Services;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.BSL.Pass;

namespace KTSSolutionWeb
{
    public partial class RegPassFiles : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('잘못된 접근입니다.');window.close();", true);
            }
            else
            {
                this.hdfProcId.Value = Request.Form["pPROCID"] == null ? "" : Request.Form["pPROCID"].ToString();
                this.hdfEmpNo.Value = Session["EMPNO"].ToString();
            }

            if (this.hdfProcId.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('잘못된 접근입니다.');window.close();", true);
            }
        }
        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetPassFile(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "GETPASSNSHOOTINGFILE";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelPassFile(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PASSNSHOOTINGFILEDELETE";
            string jobGb = "SET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string FileDownload(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "GETPASSNSHOOTINGFILEDOWNLOAD";
            string jobGb = "GET";
            string errCd = string.Empty;
            string errMsg = string.Empty;
            HttpContext context = HttpContext.Current;
            Page page = context.CurrentHandler as Page;

            DataSet ds = new DataSet();

            aData.Add("F");

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                (ds, errCd, errMsg) = dbTransaction.GetTableRow();
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //SELECT FILENM, FILEDATA, FILETYPE, FILESIZE 
                    int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                    byte[] bFile = new byte[nFileLen];
                    bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];
                    string fileData = Convert.ToBase64String(bFile);
                    string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                    DataTable dt = new DataTable();
                    dt.Columns.Add("FILESIZE", typeof(int));
                    dt.Columns.Add("FILEDATA", typeof(string));
                    dt.Columns.Add("FILENM", typeof(string));
                    dt.Rows.Add(nFileLen, fileData, strFileNm);

                    result = Utility.DataTableToJson(dt);
                }

            }

            return result;
        }

        #endregion
    }
}