﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PassInfoDetailPopup.aspx.cs" Inherits="KTSSolutionWeb.PassInfoDetailPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>PASS상세 조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;

        var page = {
            init: function () {
                fn_GridInit();
                fn_Search();
            },
            eventbind: function () {
                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });

                var FieldEl = document.getElementById("selField");
                var ValueEl = document.getElementById("inputValue");

                $("#selField").change(function (e) {
                    ValueEl.value = '';

                    var sField = FieldEl.value;

                    if (sField == "") {
                        ValueEl.setAttribute('disabled', 'disabled');

                        sField = 'NUM';
                    }
                    else {
                        ValueEl.removeAttribute('disabled');
                    }

                    ktsGrid.setFilter(sField, 'like', '');
                });

                $("#inputValue").keyup(function (e) {
                    ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
                });
            }
        };

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "NUM", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "PASS일", field: "PASSDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "TEL", field: "CUSTTELNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "관계", field: "RELATIONTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGFULLNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사원명", field: "EMPNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "PASSTYPENM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "희망상품", field: "SVCTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "희망일자", field: "HOPEDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "타입", field: "MOTTYPE", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "PASSSTATNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "참고사항", field: "NOTI1", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "PASS접수일", field: "CREATEDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "슈팅일", field: "SHOOTINGDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "결과일", field: "RESULTDT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "접속번호", field: "CONNO", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "성공여부", field: "SUCCYN", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "NOTI2", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자소속", field: "TRTORGNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자명", field: "TRTNAME", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자사번", field: "TRTEMPNO", sorter: "string" }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("69vh");
        }

        var fn_Search = function () {
            var param = "<%= Request.Form["pVal"] %>";

            if (param.split("|").length == 8) {
                var passtype = param.split("|")[2];

                if (passtype == "BIZ" || passtype == "SPACE") {
                    ktsGrid.showColumn("SVCTYPE");
                    ktsGrid.hideColumn("CONNO");
                }
                else if (passtype == "MOBILE") {
                    ktsGrid.hideColumn("SVCTYPE");
                    ktsGrid.hideColumn("CONNO");
                }
                else {
                    ktsGrid.showColumn("SVCTYPE");
                    ktsGrid.showColumn("CONNO");
                }

                //입력 파라미터
                var aData = [];
                aData[0] = param;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
            else {
                alert("잘못된 접근입니다.");
                window.close();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">PASS상세 조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
				                <div class="btnset">
                                    <input id="gridExcel" type="button" class="btn-green last" style="float:right;" value="엑셀" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search">
                                <span>
                                    <select id="selField" style="font-weight:bold">
                                        <option value="" selected="selected" >선택</option>
                                        <option value="CUSTNM">고객명</option>
                                        <option value="ORGFULLNM">부서</option>
                                        <option value="EMPNM">사원명</option>
                                        <option value="IDMSEMPNO">IDMS사번</option>
                                        <option value="EMPNO">사번</option>
                                        <option value="TRTNAME">담당자명</option>
                                        <option value="TRTEMPNO">담당자사번</option>
                                    </select>
                                    <label> : </label>
                                    <input id="inputValue" type="text" value="" disabled="disabled" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
