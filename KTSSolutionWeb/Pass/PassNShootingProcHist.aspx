﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PassNShootingProcHist.aspx.cs" Inherits="KTSSolutionWeb.PassNShootingProcHist" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Scripts/Pass/PassControls.js"></script>
    <link href="/Resource/css/tabulator.min.css" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{cursor:pointer;}
        .tooltip {position: relative;}
        .tooltip .tooltiptxt {visibility: hidden;position: absolute;top: -25px;left: 40px;padding-left:3px;padding-right:3px;border: 0.5px solid black;background-color:white;}
        .tooltip:hover .tooltiptxt {visibility:visible;}
        .tooltip .tooltiptxt2 {visibility: hidden;position: absolute;top: -10px;left: 90px;padding-left:3px;padding-right:3px;border: 0.5px solid black;background-color:white;z-index:999;}
        .tooltip:hover .tooltiptxt2 {visibility:visible;}
        .div-Screenlock {position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;background-color: #ffffff !important;z-index: 99;opacity: 0.7;overflow: hidden;display:none;}
        .div-modal {position: absolute;width: 90%;height: 730px;top: 15%;left: 5%;background: rgb(255, 255, 255);border-radius: 10px;border: 1px solid gray;overflow: hidden;z-index:100;}
        .div-modal-inner {width:90%;margin-left:5%;padding-top:30px;text-align:left;}
        .div-modal-title {margin-top:10px;margin-bottom: 5px;height:35px;overflow:hidden;text-align:left;}
        .div-modal-content {margin-top:10px;margin-bottom: 30px;padding-bottom:15px;height:300px;overflow:auto;text-align:center;}
        .table-modal > thead > tr {border-bottom:1px solid #999;height:24px;table-layout: fixed;}
        .table-modal > thead > tr > th {border-left:1px solid #999;padding-top:2px;text-align:center;}
        .table-modal > tbody > tr {border-bottom:1px solid #999;height:24px;}
        .table-modal > tbody > tr > td {border-left:1px solid #999;padding:2px 5px;text-align:center;}       
        .table-modal > tbody > tr > td > select {display: inline-block;height: 22px;padding: 0 10px 0 5px;border: 1px solid #dfdfdf;border-radius: 2px;background: url('/Resource/images/icon_select_arr.png') #fff no-repeat 100% 50%;line-height: 22px;vertical-align: middle;color: #000;font-size: 14px;}
        .table-modal > tbody > tr > td > input[type="text"] {display: inline-block;height: 22px;padding: 0 10px 0 5px;border: 1px solid #dfdfdf;border-radius: 2px;line-height: 22px;vertical-align: middle;color: #000;font-size: 14px;}
    </style>

    <script type="text/javascript">
        var callGb;
        var Json_scvGbn;
        var Json_scvList;
        var Json_mobileList;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#<%= ddlPassType.ClientID %>").change(function (e) {
                    $("#<%= hdfAuthOrgCd.ClientID %>").val('');
                    fn_GetTeam();
                });

                $("#<%= txbTeam.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#<%= btnOrg.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#btnSelect").click(function (e) {
                    var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

                    if (PassType == "") {
                        alert("구분을 선택해주세요.");
                    } else {
                        fn_Search();
                    }
                });

                $("#gridExcel").click(function (e) {
                    var name = "PASS처리이력조회";
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_GridInit = function () {
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수유형", field: "RCTTYPENM", sorter: "string", minWidth: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "희망상품", field: "HOPESVC", sorter: "string", minWidth: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일자", field: "RCTDATE", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "PASSTYPENM", sorter: "string", minWidth: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTNM", sorter: "string", minWidth: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서", field: "ORGFULLNM", sorter: "string", minWidth: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원명", field: "EMPNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", minWidth: 90 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", minWidth: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "상태", field: "PASSSTATNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자소속", field: "TRTORGNM", sorter: "string", minWidth: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자사번", field: "TRTEMPNO", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "TRTEMPNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "성공여부", field: "SUCCYNNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "처리자소속", field: "REGORGNM", sorter: "string", minWidth: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "처리자사번", field: "REGEMPNO", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "처리자명", field: "REGEMPNM", sorter: "string", minWidth: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "처리일시", field: "REGDATE", sorter: "string", minWidth: 140 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        };

        var fn_Load = function () {
            var today = new Date();
            var year = today.getFullYear(); // 년도
            var month = ("0" + (today.getMonth() + 1)).slice(-2);  // 월
            var date = ("0" + today.getDate()).slice(-2);  // 날짜

            var todayDate = year + '-' + month + '-' + date;

            $("#txbStDt").val(todayDate);
            $("#txbEnDt").val(todayDate);
        };

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                callGb = '';

                var org = json.split('|');
                var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

                if (org.length == 4) {
                    $("#<%= hdfAuthOrgCd.ClientID %>").val(org[0]);
                    $("#<%= txbTeam.ClientID %>").val(org[1]);
                    $("#<%= hdfUpperYn.ClientID %>").val(org[3]);

                    $("#<%= txbTeam.ClientID %>").off("click");
                    $("#<%= btnOrg.ClientID %>").off("click");

                    if (org[3] == 'Y') {
                        var now = new Date();
                        var year = now.getFullYear();
                        var month = String(now.getMonth() + 1).padStart(2, '0');

                        var today = year + "-" + month;

                        $("#txbMonthFin").val(today);
                        $("#btnMonthFinY").removeAttr("disabled");
                        $("#btnMonthFinN").removeAttr("disabled");
                        $("#spanMonthFin").show();
                    }
                    else {
                        $("#txbMonthFin").val("");
                        $("#btnMonthFinY").attr("disabled", "disalbed");
                        $("#btnMonthFinN").attr("disabled", "disalbed");
                        $("#spanMonthFin").hide();
                    }

                    $("#<%= txbTeam.ClientID %>").click(function (e) {
                        PopupPassOrgTree(PassType, org[2], org[3]);
                    });

                    $("#<%= btnOrg.ClientID %>").click(function (e) {
                        PopupPassOrgTree(PassType, org[2], org[3]);
                    });
                }
            }
        };

        var fn_GetTeam = function () {
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();

            if (PassType != '') {
                var OrgCd = $("#<%= hdfAuthOrgCd.ClientID %>").val();

                var aData = [];
                aData[0] = PassType;
                aData[1] = OrgCd;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

                callGb = 'GetTeam';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#<%= hdfAuthOrgCd.ClientID %>").val('');
                $("#<%= txbTeam.ClientID %>").val('');

                $("#<%= txbTeam.ClientID %>").off("click");
                $("#<%= btnOrg.ClientID %>").off("click");

                $("#<%= txbTeam.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });

                $("#<%= btnOrg.ClientID %>").click(function (e) {
                    alert("구분을 선택해주세요.");
                });
            }
        };

        var fn_Search = function () {
            var PassType = $("#<%= ddlPassType.ClientID %> option:selected").val();
            var OrgCd = $("#<%= hdfAuthOrgCd.ClientID %>").val();
            var EmpNo = $("#txbEmpNo").val();
            var StDt = $("#txbStDt").val();// + " 00:00:00";
            var EnDt = $("#txbEnDt").val();// + " 23:59:59";
            var PassStat = $("#<%= ddlPassStat.ClientID %> option:selected").val();
            var SuccYn = $("#<%= ddlShootingYn.ClientID %> option:selected").val();

            if (PassType == "") {
                alert("구분을 선택해주세요.");
            } else {
                if (PassType == "MOT") {
                    ktsGrid.hideColumn("HOPESVC");
                    ktsGrid.showColumn("RCTTYPENM");
                }
                else {
                    ktsGrid.showColumn("HOPESVC");
                    ktsGrid.hideColumn("RCTTYPENM");
                }

                //입력 파라미터
                var aData = [];

                aData[0] = PassType;
                aData[1] = OrgCd.replace(/,/gi, "^");
                aData[2] = EmpNo;
                aData[3] = StDt;
                aData[4] = EnDt;
                aData[5] = PassStat;
                aData[6] = SuccYn;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        };

        var PopupPassOrgTree = function (passtype, operator, upperyn) {

            var orgcd = $("#<%= hdfAuthOrgCd.ClientID %>").val();

            var nWidth = 400;
            var nHeight = 700;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pPASSTYPE: passtype,
                pOPERATOR: operator,
                pUPPERYN: upperyn
            };

            var Popupform = createForm("/Common/OrgTree_Pass", param);

            Popupform.target = "OrgTree_PassAuth";
            pop3 = window.open("", "OrgTree_PassAuth", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            pop3.focus();
        };

        var PopupEmpUser = function () {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var SetDispEmpNo = function (orgcd, empno, empnm) {
            $("#txbEmpNo").val(empno);
            $("#txbEmpNm").val(empnm);
        };

        var SetPassOrgCd = function (orgcd) {
            this.focus();

            $("#<%= hdfAuthOrgCd.ClientID %>").val(orgcd);
            fn_GetTeam();
        };

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">
		    <fieldset>   
				<span class="optionbox">
					<label>구분</label>
                    <asp:DropDownList ID="ddlPassType" style="width:150px;" AutoPostBack="false" runat="server"></asp:DropDownList>
                </span>    
                <span class="inpbox first">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="hdfAuthOrgCd" runat="server" />
                    <asp:HiddenField ID="hdfUpperYn" runat="server" />
				    <button id="btnOrg" runat="server" type="button" class="btn-plus" title="검색">+</button>
				</span>
				<span class="inpbox">
					<label>사번</label>
                    <input type="text" id="txbEmpNo" style="width:100px;margin-right:10px" onclick="PopupEmpUser();" readonly="readonly" />
					<label>이름</label>
                    <input type="text" id="txbEmpNm" style="width:100px;" onclick="PopupEmpUser();" readonly="readonly" />
					<button type="button" class="btn-plus" title="검색" onclick="javascript:PopupEmpUser();">+</button>
                </span>
                <hr class="blank" />
                <span class="inpbox">
					<label>접수일</label>
                    <input type="text" id="txbStDt" class="date" style="width:120px" readonly="readonly" />
                    <em>~</em>
                    <input type="text" id="txbEnDt" class="date" style="width:120px" readonly="readonly" />
                </span>
				<span class="optionbox">
					<label>상태</label>
                    <asp:DropDownList ID="ddlPassStat" style="width:250px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pass 대기중" Value="P"></asp:ListItem>
                        <asp:ListItem Text="Shooting 진행중" Value="S"></asp:ListItem>
                        <asp:ListItem Text="Shooting 완료" Value="F"></asp:ListItem>
                    </asp:DropDownList>
                </span>
				<span class="optionbox">
					<label>슈팅여부</label>
                    <asp:DropDownList ID="ddlShootingYn" style="width:100px;" runat="server">
                        <asp:ListItem Text="전체" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="성공" Value="Y"></asp:ListItem>
                        <asp:ListItem Text="실패" Value="N"></asp:ListItem>
                        <asp:ListItem Text="보류" Value="H"></asp:ListItem>
                        <asp:ListItem Text="반려" Value="R"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>
