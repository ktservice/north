﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class PassInfoDetailPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "PASSPROCDETAILPOPUP";
            string jobGb = "GET";

            //aData[0] = empno;
            //aData[1] = orgcd;
            //aData[2] = passtype;
            //aData[3] = svctype;
            //aData[4] = stdt;
            //aData[5] = endt;
            //aData[6] = valType;
            //aData[7] = selType;

            List<string> aParam = new List<string>();
                        
            for (int i = 0; i < aData[0].Split('|').Length; i++)
            {
                aParam.Add(aData[0].Split('|')[i].ToString());
            }

            aParam.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aParam);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}