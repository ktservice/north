﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegPassFiles.aspx.cs" Inherits="KTSSolutionWeb.RegPassFiles" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>첨부파일 관리</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <style>  
        .filebox .file {float: left;width:30px !important}
    </style>

    <script type="text/javascript">     
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
            },
            eventbind: function () {
            }
        };

        var fn_Load = function () {
            fn_GetPassFile();
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://bss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var successCallBack = function (json) {
            if (callGb == 'GetPassFile') {
                callGb = '';

                fn_ControlBind(json);
            }
            else if (callGb == 'FileDelOk') {
                callGb = '';

                alert('첨부파일이 삭제되었습니다.');
                fn_GetPassFile();
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb == 'FileInsOk') {
                callGb = '';

                alert('첨부파일 등록이 완료되었습니다.');
                $("#divFileUpload").empty();
                AddFileUpload();

                fn_GetPassFile();
            }
            else if (callGb == 'FileDownOk') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    var link = document.createElement('a');
                    link.href = 'data:application/octet-stream;base64,' + item.FILEDATA;
                    link.download = item.FILENM;
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                });
            }
        }

        var fn_GetPassFile = function () {
            $("#divFileList").empty();

            var procId = $("#<%= hdfProcId.ClientID %>").val();

            if (procId == "") {
                alert("잘못된 접근입니다.");
                window.close();
            }
            else {

                var aData = [];

                aData[0] = procId;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPassFile');

                callGb = 'GetPassFile';

                ajaxCall(jsonData, ajaxUrl, false);
            };
        };

        var fn_DelPassFile = function (seq) {
            var procId = $("#<%= hdfProcId.ClientID %>").val();

            if (confirm("첨부파일을 삭제하시겠습니까?")) {
                var aData = [];

                aData[0] = procId;
                aData[1] = seq;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'DelPassFile');

                callGb = 'FileDelOk';

                ajaxCall(jsonData, ajaxUrl, false);
            }
        };

        var fn_RegFileData = function () {
            if (confirm("첨부파일을 저장하시겠습니까?")) {
                var procId = $("#<%= hdfProcId.ClientID %>").val();

                if (procId == "") {
                    alert("잘못된 접근입니다.");
                    window.close();
                }
                else {
                    var Files = $("#divFileUpload").find("input[type='file']");
                    var empNo = $("#<%= hdfEmpNo.ClientID %>").val();

                    var formData = new FormData();

                    var Keys = procId + "_" + empNo;

                    for (var i = 0; i < Files.length; i++) {
                        formData.append(Keys, Files[i].files[0]);
                    }

                    var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/Pass/FileUploadHandler.ashx");

                    callGb = "FileInsOk";

                    ajaxCallFileUpload(formData, ajaxUrl);
                }
            }
        };

        var fn_ControlBind = function (json) {
            var divFileList = $("#divFileList");

            divFileList.empty();

            $.each(JSON.parse(json), function (idx, item) {
                var p = document.createElement("p");
                p.className = "inpbox";
                p.style.display = "block";
                p.style.textAlign = "left";

                var link = document.createElement('a');
                link.setAttribute("onclick", "javascript:fn_FileDownLoad(" + item.SEQ + ");");

                var label = document.createElement("label");
                label.setAttribute("id", "lblFile" + item.SEQ);
                label.setAttribute("name", "lblFileNm");
                label.setAttribute("style", "width: 90%;height: 35px;padding-top: 10px;text-overflow: ellipsis;white-space: nowrap;");
                
                label.innerText = item.id + ". " + item.FILENM;

                link.append(label);

                p.append(link);

                var button = document.createElement("button");
                button.type = "button";
                button.setAttribute("id", "btnDelFile" + item.SEQ);
                button.setAttribute("onclick", "javascript:fn_DelPassFile(" + item.SEQ + ");");
                button.style.width = "30px";
                button.style.height = "30px";
                button.style.lineHeight = "0";
                button.style.color = "black";
                button.style.padding = "0";
                button.style.margin = "0";
                button.style.border = "none";
                button.innerText = "X";

                p.append(button);

                divFileList.append(p);
            });
        };

        var AddFileUpload = function () {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("divFileUpload");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-right:0;");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:400px;");

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "document.getElementById('fu" + AddFileUpload.lastAssignedId + "-value').value=this.value;");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.setAttribute("style", "width:30px;float:left; margin-right:5px;");
            newFileLabel.innerText = "...";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fn_FileDownLoad = function (seq) {
            var procId = $("#<%= hdfProcId.ClientID %>").val();

            //입력 파라미터
            var aData = [];

            aData[0] = procId;
            aData[1] = seq;

            callGb = 'FileDownOk';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'FileDownload');

            ajaxCallEx(jsonData, ajaxUrl, true);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-organization -->
            <div class="windowpop pop-clothes-request">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>첨부파일 관리</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self);">닫기</button>

                    <!-- S:popcontents -->
                    <div class="popcontents" style="margin-bottom:20px;">

                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <tbody>
                                        <tr id="trUpload">
                                            <th style="border-right:1px solid #dfdfdf; border-bottom:1px solid #dfdfdf;">첨부파일<br />
                                                <button type="button" onclick="AddFileUpload();" class="btn-save" style="width:40px;height:25px;font-size:13px">추가</button>
                                            </th>
                                            <td style="text-align:left;border-bottom:1px solid #dfdfdf;">
                                                <div id="divFileUpload" style="height:300px;overflow-y:auto;">
                                                    <p class="filebox">
                                                        <input type="text" id="fu1-value" style="width:400px" />
                                                        <span class="file">
                                                            <input type="file" id="fu1" name="fu1" onchange="document.getElementById('fu1-value').value=this.value;" /> 
                                                            <label for="fu1" style="width:30px; float:left;">...</label>
                                                        </span>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="trList">
                                            <th style="border-right:1px solid #dfdfdf;">파일목록</th>
                                            <td style="text-align:left;padding:5px;">
                                                <div id="divFileList" style="height:200px;overflow-y:auto;">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>	
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
			<div class="list-top">
				<div style="text-align:center;margin-bottom:20px;">
                    <input id="btnReg" type="button" class="btn-green" onclick="fn_RegFileData();" value="등록" />
                    <input id="btnClose" type="button" class="btn-black" onclick="window.close();" value="닫기" />
                </div>
            </div>
        <asp:HiddenField ID="hdfProcId" runat="server" />
        <asp:HiddenField ID="hdfEmpNo" runat="server" />
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
