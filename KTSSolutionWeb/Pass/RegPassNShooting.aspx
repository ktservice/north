﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegPassNShooting.aspx.cs" Inherits="KTSSolutionWeb.RegPassNShooting" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Pass등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <script type="text/javascript" src="/Scripts/Pass/PassControls.js"></script>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <style>  
        .filebox .file {float: left;width:30px !important}
    </style>

    <script type="text/javascript">     
        var callGb;
        var rctTypeBind;
        var consultBind;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Load();
            },
            eventbind: function () {
                $("#ddlPassType").change(function (e) {
                    fn_ViewControls();
                    fn_GetRctType();
                });

                $("#ddlRctType").change(function (e) {
                    fn_ViewControls();
                });
            }
        };

        var fn_Load = function () {
            var now = new Date();
            var year = now.getFullYear();
            var month = String(now.getMonth() + 1).padStart(2, '0');
            var day = String(now.getDate()).padStart(2, '0');
            var today = year + "-" + month + "-" + day;

            $("#txbRctDt").val(today);
            fn_GetPassType();
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://bss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var successCallBack = function (json) {
            if (callGb == 'GetPassType') {
                callGb = '';

                fn_ControlBind(json, 'PASS');

                var orgcd = $("#<%= hdfDeptCd.ClientID %>").val();
                var empnm = $("#<%= hdfEmpNm.ClientID %>").val();

                fn_GetTeam(orgcd, empnm);
            }
            else if (callGb == 'GetRctType') {
                callGb = '';

                fn_ControlBind(json, 'RCT');
            }
            else if (callGb == 'GetTeam') {
                callGb = '';

                $("#txbTrtEmpNm").val(json);
            }
            else if (callGb == 'GetConsult') {
                callGb = '';

                fn_ControlBind(json, 'CON');
            }
            else if (callGb == 'SetPassInfo') {
                callGb = '';

                if ($("#trUpload").is(":visible")) {
                    fn_RegFileData(json);
                }
                else {
                    alert('PASS등록이 완료되었습니다.');

                    opener.fn_ReSearch();
                    window.close();
                }
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb == 'FileInsOk') {
                callGb = '';

                alert('PASS등록이 완료되었습니다.');

                opener.fn_ReSearch();
                window.close();
            }
        }

        var fn_ViewControls = function () {
            var PassType = $("#ddlPassType option:selected").val();
            var RctType = $("#ddlRctType option:selected").val().split('_');
            
            if (PassType == "MOT") {
                $("#thRctType").text("접수유형");
                $("#trConNo").show();

                $("#trConsult").hide();
                $("#trSvcList").show();

                var UploadYn = "";

                if (RctType.length == 2) {
                    UploadYn = RctType[1];
                }

                if (UploadYn == "Y") {
                    $("#trUploadTitle").show();
                    $("#trUpload").show();
                }
                else {
                    $("#trUploadTitle").hide();
                    $("#trUpload").hide();
                }
            }
            else {
                $("#thRctType").text("희망상품");
                $("#trConNo").hide();

                if (PassType == "BIZ") {
                    $("#trConsult").show();                    
                }
                else {
                    $("#trConsult").hide();
                }

                $("#trSvcList").hide();

                $("#trUploadTitle").hide();
                $("#trUpload").hide();
            }
        };

        var fn_GetPassType = function () {
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetPassType');

            callGb = 'GetPassType';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetRctType = function () {
            var PassType = $("#ddlPassType option:selected").val();

            var aData = [];

            aData[0] = PassType;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetRctType');

            callGb = 'GetRctType';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_GetConsult = function () {
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetConsult');

            callGb = 'GetConsult';

            ajaxCall(jsonData, ajaxUrl, false);
        };

        var fn_RegChk = function () {
            var Passtype = $("#ddlPassType option:selected").val();
            var RctType = $("#ddlRctType option:selected").val();

            if (Passtype == "") {
                alert("구분을 선택해주세요.");
                return false;
            }
            else if (RctType == "") {
                if (Passtype == "MOT") {
                    alert("접수유형을 선택해주세요.");
                }
                else {
                    alert("희망상품을 선택해주세요.");
                }
            }
            else {
                if (confirm("PASS를 저장 하시겠습니까?")) {
                    fn_RegPassInfo();
                }
                else {
                    return false;
                }
            }
        };

        var fn_RegPassInfo = function () {
            var Passtype = $("#ddlPassType option:selected").val();
            var RctType = $("#ddlRctType option:selected").val().split('_')[0];
            var CustNm = $("#txbCustNm").val();
            var TelNo = $("#txbTelNo").val();
            var RctDt = $("#txbRctDt").val();
            var Relations = $("#txbRelations").val();
            var ConNo = $("#txbConNo").val();
            var Consult = $("#ddlConsult option:selected").val();
            var Svc = "";

            if (Passtype == "MOT") {
                var chkSvc = $("input[name=chbSvc]:checked");


                $(chkSvc).each(function () {
                    if (Svc != "") {
                        Svc += "^";
                    }

                    Svc += $(this).val();
                });
            }

            var Noti = $("#<%= txbNoti.ClientID %>").val();

            var aData = [];

            aData[0] = Passtype;
            aData[1] = RctType;
            aData[2] = CustNm;
            aData[3] = TelNo;
            aData[4] = RctDt;
            aData[5] = Relations;
            aData[6] = ConNo;
            aData[7] = Consult;
            aData[8] = Svc;
            aData[9] = Noti;
            
            callGb = "SetPassInfo";
            
            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, "SetPassNShootingInfo");

            ajaxCall(jsonData, ajaxUrl, false);

        };

        var fn_RegFileData = function (json) {
            var ProcId = "";
            var Files = $("#divFileUpload").find("input[type='file']");
            var EmpNo = $("#<%= hdfEmpNo.ClientID %>").val();

            $.each(JSON.parse(json), function (idx, item) {
                ProcId = item.PROCID;
                return false;
            });

            var formData = new FormData();

            var Keys = ProcId + "_" + EmpNo;

            for (var i = 0; i < Files.length; i++) {
                formData.append(Keys, Files[i].files[0]);
            }

            var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/Pass/FileUploadHandler.ashx");

            callGb = "FileInsOk";

            ajaxCallFileUpload(formData, ajaxUrl);
        };

        var AddFileUpload = function () {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("divFileUpload");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-right:0;");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:530px;");

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";
            newFile.setAttribute("onchange", "document.getElementById('fu" + AddFileUpload.lastAssignedId + "-value').value=this.value;");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.setAttribute("style", "width:30px;float:left; margin-right:5px;");
            newFileLabel.innerText = "...";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        };

        var fn_PopupEmpUser = function () {

            var nWidth = 700;
            var nHeight = 500;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm("/Common/SearchUser", null);

            Popupform.target = "SearchUser";
            var win = window.open("", "SearchUser", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var SetDispEmpNo = function (orgcd, empno, empnm) {
            $("#<%= hdfEmpNo.ClientID %>").val(empno);
            fn_GetTeam(orgcd, empnm);
        };

        var fn_GetTeam = function (orgcd, empnm) {
            var aData = [];
            aData[0] = orgcd;
            aData[1] = empnm;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');

            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl, false);
        };
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-organization -->
            <div class="windowpop pop-clothes-request">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong>PASS 등록</strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self);">닫기</button>

                    <!-- S:popcontents -->
                    <div class="popcontents" style="margin-bottom:20px;">

                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table>
                                    <colgroup>
                                        <col style="width:10%"/>
                                        <col style="width:30%"/>
                                        <col style="width:10%"/>
                                        <col style="width:50%"/>
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th>구분</th>
                                            <td style="text-align:left;">
                                                <p class="optionbox">
                                                    <select id="ddlPassType" style="width:92% !important;">
                                                        <option value="">선택하세요</option>
                                                    </select>
                                                </p>
                                            </td>
                                            <th id="thRctType">접수유형</th>
                                            <td id="tdRctType" style="text-align:left;">
                                                <p class="optionbox">
                                                    <select id="ddlRctType" style="width:92% !important;">
                                                        <option value="">선택하세요</option>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>판매자</th>
                                            <td colspan="3" style="text-align:left;">
                                                <p class="inpbox">
                                                    <input type="text" id="txbTrtEmpNm" onclick="fn_PopupEmpUser();" readonly="readonly" style="width:88.5% !important" />
					                                <button type="button" class="btn-plus" title="검색" onclick="javascript:fn_PopupEmpUser();">+</button>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thCustNm">고객명</th>
                                            <td id="tdCustNm" style="text-align:left;">
                                                <p class="inpbox">
                                                    <input type="text" id="txbCustNm" maxlength="50" style="width:92% !important" />
                                                </p>
                                            </td>
                                            <th id="thCustTel">연락처</th>
                                            <td id="tdCustTel" style="text-align:left;">
                                                <p class="inpbox">
                                                    <input type="text" id="txbTelNo" maxlength="11" style="width:92% !important" onkeypress="return OnlyNumber();" onkeydown="ReplaceKorean(this);"/>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thRctDt">접수일자</th>
                                            <td id="tdRctDt" style="text-align:left;">
                                                <p class="inpbox">
                                                    <input type="text" id="txbRctDt" class="date" readonly="readonly" style="width:120px !important" />
                                                </p>
                                            </td>
                                            <th id="thRel">관계</th>
                                            <td id="tdRel" style="text-align:left;">
                                                <p class="inpbox">
                                                    <input type="text" id="txbRelations" maxlength="50" style="width:92% !important" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trConNo" style="display:none;">
                                            <th>접속번호</th>
                                            <td colspan="3" style="text-align:left;">
                                                <p class="inpbox">
                                                    <input type="text" id="txbConNo" maxlength="50" style="width:96.5% !important" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trConsult" style="display:none;">
                                            <th>컨설팅</th>
                                            <td colspan="3" style="text-align:left;">
                                                <p class="optionbox">
                                                    <select id="ddlConsult" style="width:96% !important;">
                                                        <option value="">선택하세요</option>
                                                    </select>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trSvcList" style="display:none;"">
                                            <th>상품선택</th>
                                            <td colspan="3" style="text-align:left;padding-left:10px;padding-right:30px;">
                                                <table>
                                                    <tr style="border-bottom:1px solid #cccccc">
                                                        <td style="height:30px;width:50%;">인터넷</td>
                                                        <td style="height:30px;width:50%;"><input id="chbInternet" name="chbSvc" type="checkbox" value="인터넷" /></td>
                                                    </tr>
                                                    <tr style="border-bottom:1px solid #cccccc">
                                                        <td style="height:30px;width:50%;">TV</td>
                                                        <td style="height:30px;width:50%;"><input id="chbTv" name="chbSvc" type="checkbox" value="TV" /></td>
                                                    </tr>
                                                    <tr style="border-bottom:1px solid #cccccc">
                                                        <td style="height:30px;width:50%;">모바일</td>
                                                        <td style="height:30px;width:50%;"><input id="chbMobile" name="chbSvc" type="checkbox" value="모바일" /></td>
                                                    </tr>
                                                    <tr style="border-bottom:1px solid #cccccc">
                                                        <td style="height:30px;width:50%;">전화</td>
                                                        <td style="height:30px;width:50%;"><input id="chbTel" name="chbSvc" type="checkbox" value="전화" /></td>
                                                    </tr>
                                                    <tr style="border-bottom:1px solid #cccccc">
                                                        <td style="height:30px;width:50%;">AP</td>
                                                        <td style="height:30px;width:50%;"><input id="chbAp" name="chbSvc" type="checkbox" value="AP" /></td>
                                                    </tr>
                                                    <tr style="border-bottom:1px solid #cccccc">
                                                        <td style="height:30px;width:50%;">업셀링</td>
                                                        <td style="height:30px;width:50%;"><input id="chbUpcelling" name="chbSvc" type="checkbox" value="업셀링" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:30px;width:50%;">기타</td>
                                                        <td style="height:30px;width:50%;"><input id="chbEtc" name="chbSvc" type="checkbox" value="기타" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>요청사항</th>
                                            <td colspan="3" style="text-align:left;">
                                                <p class="inpbox">
                                                    <asp:TextBox ID="txbNoti" runat="server" Width="96.5%" Height="240px" MaxLength="8000" TextMode="MultiLine" style="padding:5px;"></asp:TextBox>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr id="trUploadTitle" style="display:none;border-top:1px solid #000">
                                            <th colspan="4" style="text-align:center;">첨부파일 <button type="button" onclick="AddFileUpload();" class="btn-save" style="width:40px;height:25px;font-size:13px;margin-top:-1px;margin-right:10px;float:right;">추가</button>
                                            </th>
                                        </tr>
                                        <tr id="trUpload" style="display:none;">
                                            <td colspan="4" style="text-align:left;">
                                                <div id="divFileUpload" style="height:140px;overflow-y:auto;padding-left:10px">
                                                    <p class="filebox" style="margin-right:0;">
                                                        <input type="text" id="fu1-value" style="width:530px" />
                                                        <span class="file">
                                                            <input type="file" id="fu1" name="fu1" onchange="document.getElementById('fu1-value').value=this.value;" /> 
                                                            <label for="fu1" style="width:30px; float:left;">...</label>
                                                        </span>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>	
                                </table>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:popcontents -->
                </div>
                <!-- E:popupwrap -->
            </div>
			<div class="list-top">
				<div style="text-align:center;margin-bottom:20px;">
                    <input id="btnReg" type="button" class="btn-green" onclick="return fn_RegChk();" value="등록" />
                    <input id="btnClose" type="button" class="btn-black" onclick="window.close();" value="닫기" />
                </div>
            </div>
        </div> 
        <asp:HiddenField ID="hdfDeptCd" runat="server" />
        <asp:HiddenField ID="hdfEmpNo" runat="server" />
        <asp:HiddenField ID="hdfEmpNm" runat="server" />
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
