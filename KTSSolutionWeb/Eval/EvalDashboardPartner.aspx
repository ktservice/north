﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EvalDashboardPartner.aspx.cs" Inherits="KTSSolutionWeb.EvalDashboardPartner" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $("#<%= txbEnDt.ClientID %>").change(function () {
                });

                $("#selTeam").change(function (e) {
                    $("#<%= hfOrgCd.ClientID %>").val(this.value);
                });

                $("#btnInUpload").click(function (e) {
                    fn_PopupUpload("I");
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

            }
        };

        var fn_Init = function () {
            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            if ($("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'y' || $("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'Y') {
                $('#selTeam').hide();
            }
            else {
                fn_Team();
            }
        }

        var fn_Load = function () {
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selTeam') {
                var dataCnt = 0;
                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.OFFICECD + "'>" + item.OFFICENM + "</option>");
                    dataCnt++;
                });

                if (dataCnt > 1) {
                    $("#" + callGb + " option:eq(0)").prop("selected", true);
                    $("#<%= hfOrgCd.ClientID %>").val(obj.val());
                    $("#<%= hfOrgCdFix.ClientID %>").val(obj.val());

                    obj.show();
                    $("#<%=txbTeam.ClientID %>").hide();
                }
                else {
                    obj.hide();
                }

                callGb = '';
            }
        }

        var fn_PopupUpload = function (gubun) {
            var form = "/Eval/EvalProductUploadPopup";
            var target = "EvalProductUploadPopup";
            var param = {
                pGubun: gubun
            };
            var nWidth = 800;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupDash = function (date1, date2, orgCd) {
            var form = "/Eval/EvalDashboardPartnerPopup";
            var target = "EvalDashboardPartnerPopup";
            var nWidth = 1400;
            var nHeight = 1024;
            var param = {
                pHeight: nHeight,
                pWidth: nWidth,
                pDate1: date1,
                pDate2: date2,
                pOrgCd: orgCd
            };

            cmPostPopup2(form, target, param, nWidth, nHeight, 'no', 'no', 'no', 'no', '');
        }

        var fn_OrgPartner = function (orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCdFix.ClientID %>").value;
            }

            var form = "/Common/OrgPartners";
            var target = "OrgPartners";
            var param = {
                pCheckType: 'R',
                pType: 'TEAM',
                pUpperOrgCd: orgcd,
                pOfficeCd: '',
                pTeamCd: '',
                pKtsYn: ''
            };
            var nWidth = 1180;
            var nHeight = 580;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetOrgPartner = function (officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(officeCd);
            $("#<%=txbTeam.ClientID %>").val(officeNm);
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function() {
            var BaseMonth = $("#<%= txbEnDt.ClientID %>").val();

            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                alert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                alert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        var fn_Team = function () {
            callGb = 'selTeam';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUserOrg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function () {
            if (fn_SearchChk()) {
                var pDate1 = $("#<%= txbStDt.ClientID %>").val().replace(/-/gi, "");
                var pDate2 = $("#<%= txbEnDt.ClientID %>").val().replace(/-/gi, "");
                var pOrgCd = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                fn_PopupDash(pDate1, pDate2, pOrgCd);
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="inpbox">
                    <label>기간</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <span class="optionbox">
                                <select id="selTeam" style="width:160px;display:none"></select>
                            </span>
                            <asp:TextBox ID="txbTeam" runat="server" Width="300px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCdFix" runat="server"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnInUpload" type="button" class="btn-green last" value="생산성일괄등록" />
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                </div>
                <input id="hdnStMonth" type="hidden" />
                <input id="hdnEtMonth" type="hidden" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
