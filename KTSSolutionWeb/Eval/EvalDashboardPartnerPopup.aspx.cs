﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class EvalDashboardPartnerPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                string orgCd = this.Page.Session["ORGCD"].ToString();
                this.Page.Session["ORGCD"] = orgCd;

                if (!IsPostBack)
                {
                    string pHeight = Request.Form["pHeight"] == null ? "" : Request.Form["pHeight"].ToString();
                    string pWidth = Request.Form["pWidth"] == null ? "" : Request.Form["pWidth"].ToString();
                    string pDate1 = Request.Form["pDate1"] == null ? "" : Request.Form["pDate1"].ToString();
                    string pDate2 = Request.Form["pDate2"] == null ? "" : Request.Form["pDate2"].ToString();
                    string pOrgCd = Request.Form["pOrgCd"] == null ? "" : Request.Form["pOrgCd"].ToString();

                    hdnHeight.Value = pHeight;
                    hdnWidth.Value = pWidth;
                    hdnDate1.Value = pDate1;
                    hdnDate2.Value = pDate2;
                    hdnOrgCd.Value = pOrgCd;
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "EVALDASHBOARD_PARTNER";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}