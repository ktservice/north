﻿using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class EvalKpiStatByTransPartner : PageBase
    {
        private DataTable DtOrgList
        {
            get
            {
                if (ViewState["DtOrgList"] != null)
                    return (DataTable)ViewState["DtOrgList"];
                else
                    return null;
            }
            set
            {
                ViewState["DtOrgList"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크  
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();

                SetPageInit();

                //TestModeCheck(this.Page);
            }
        }

        private void SetOrgList(string strEmpNo, string strMonth, string strLimitLv)
        {
            DataSet ds = new DataSet();

            try
            {
                using (OrgCodeTree org = new OrgCodeTree())
                {
                    ds = org.GetLimitOrgMonthList(strEmpNo, strMonth, strLimitLv);
                }

                if (ds.Tables.Count > 0)
                {
                    DtOrgList = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }


        private void SetPageInit()
        {
            try
            {
                string ktsYn = GetKtsYn();
                string orgCd = Session["ORGCD"].ToString();
                string orgNm = "";
                string fnTree = "fn_OrgPartner";
                this.txbStDt.Text = DateTime.Now.AddMonths(-3).ToString("yyyy-MM");
                this.txbEnDt.Text = DateTime.Now.ToString("yyyy-MM");
                string strMonth = this.txbEnDt.Text.Replace("-", "").Substring(0, 6);

                hdnToMonth.Value = this.txbEnDt.Text;

                this.hdnKtsYn.Value = ktsYn;

                if (ktsYn.ToUpper().Equals("N"))
                {
                    this.hfOrgCd.Value = orgCd;

                    fnTree = "fn_OrgPartner";
                }

                if (ktsYn.ToUpper().Equals("Y"))
                {
                    if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                    {
                        this.btnOrgCd.Visible = true;
                        this.btnOrgCd.Enabled = true;

                        this.btnOrgCd.OnClientClick += fnTree + "('', '');";
                        this.txbTeam.Attributes.Add("onClick", fnTree + "('', '');");

                        SetOrgList("", strMonth, "5");
                    }
                    else
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth, "5");

                        this.btnOrgCd.Visible = false;
                        this.btnOrgCd.Enabled = false;
                    }

                    this.hfOrgCd.Value = orgCd;

                    this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                }
                else
                {
                    if (Session["AUTHID"].Equals("AUTH0007"))
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth, "5");

                        this.btnOrgCd.Visible = false;
                        this.btnOrgCd.Enabled = false;
                    }
                    else
                    {
                        this.btnOrgCd.Visible = true;
                        this.btnOrgCd.Enabled = true;

                        this.btnOrgCd.OnClientClick += fnTree + "('', '');";
                        this.txbTeam.Attributes.Add("onClick", fnTree + "('', '');");

                        SetOrgList("", strMonth, "5");
                    }

                    this.txbTeam.Text = Utility.GetBCompOrgNm(orgCd, DtOrgList);

                    this.hfOrgCd.Value = orgCd;
                }

                this.hfOrgCdFix.Value = this.hfOrgCd.Value;

                updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);
                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        string GetKtsYn()
        {
            string ktsYn = "Y";
            if (Session["KTSYN"] != null)
            {
                ktsYn = Session["KTSYN"].ToString();
            }

            return ktsYn;
        }

        string GetOrgCd(string officeCd)
        {
            string orgCd = string.Empty;

            orgCd = Utility.GetBCompTeamCd(officeCd, DtOrgList);

            return orgCd;
        }

        protected void hfOfficeCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = string.Empty;
                string strOrgCd = string.Empty;
                string ktsYn = GetKtsYn();

                strOrgNm = Utility.GetBCompOrgNm(this.hfOfficeCd.Value, DtOrgList);
                this.hfOrgCd.Value = this.hfOfficeCd.Value;

                this.txbTeam.Text = strOrgNm;
                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfOrgCd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strOrgNm = string.Empty;
                string ktsYn = GetKtsYn();

                if (ktsYn.Equals("Y"))
                {
                    strOrgNm = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList, false);

                    if (strOrgNm.IndexOf(",") > 0)
                    {
                        strOrgNm = Utility.GetBCompOrgNm(this.hfOrgCd.Value, DtOrgList);
                    }
                }
                else strOrgNm = Utility.GetBCompOrgNm(this.hfOfficeCd.Value, DtOrgList);

                this.txbTeam.Text = strOrgNm;
                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        protected void hfBaseDt_ValueChanged(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string strMonth = Request.Form[this.txbEnDt.UniqueID].ToString().Replace("-", "").Substring(0, 6);

                if (!strMonth.Equals(""))
                {
                    if (Session["AUTHID"].Equals("AUTH0001") || Session["AUTHID"].Equals("AUTH0002") || Session["AUTHID"].Equals("AUTH0003") || Session["AUTHID"].Equals("AUTH0004") || Session["AUTHID"].Equals("AUTH0005"))
                    {
                        SetOrgList("", strMonth, "4");
                    }
                    else
                    {
                        SetOrgList(this.Session["EMPNO"].ToString(), strMonth, "4");
                    }

                    if (DtOrgList.Rows[0]["ORGLEVEL"].ToString() != "4")
                    {
                        using (KTSUser user = new KTSUser())
                        {
                            ds = user.GetUserViewOrgMonth2(this.Session["EMPNO"].ToString(), strMonth);
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string strOrgCd = "";
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                if (strOrgCd.Length > 0)
                                    strOrgCd += ",";

                                strOrgCd += ds.Tables[0].Rows[i]["ORGCD"].ToString();
                            }

                            this.hfOrgCd.Value = strOrgCd;
                            this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                        }
                        else
                        {
                            this.hfOrgCd.Value = "";
                            this.txbTeam.Text = "";
                        }
                    }
                    else
                    {
                        this.txbTeam.Text = Utility.GetOrgNm(this.hfOrgCd.Value, DtOrgList);
                    }
                }
                else
                {
                    this.hfOrgCd.Value = "";
                    this.txbTeam.Text = "";
                }


                this.updPanelOrgCd.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetDdlMonth(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "EvalKpiStatByTrans_Partner";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetLabel(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "EvalKpiStatByTrans_Partner";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetGridData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "EvalKpiStatByTrans_Partner";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetUserOrg(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "ORGPARTNERUSERINFO";
            string jobGb = "GET";
            string empNo = string.Empty;

            try
            {
                empNo = HttpContext.Current.Session["EMPNO"].ToString();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            aData.Add(empNo);
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}