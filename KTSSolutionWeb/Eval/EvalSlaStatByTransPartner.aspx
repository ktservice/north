﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EvalSlaStatByTransPartner.aspx.cs" Inherits="KTSSolutionWeb.EvalSlaStatByTransPartner" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script src='/Resource/js/chartjs/chart.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-datalabels.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/hammer.min.js' ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-zoom.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/utils.js' ></script>
    <script src='/Resource/js/chartjs/kts_chartjs-1.0.js' ></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/chartjs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"], .containMove, .containMoveText{
            cursor:pointer;
        }

        .datalistmove{
            cursor:default;
        }

        .searchbox {
            border-radius: 6px 6px 0 0;
            border: 1px solid #eee;
            border-bottom: none;
        }

        .list-top {
            padding: 20px 45px 0;
        }

        .chartSpanB {
            margin: 0 0 0 0px !important;
        }

    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var getJsonData;
        var getJsonLabelData;
        var odr = 'ODR';
        var orgNm = 'ORGNM';
        var mDate = 'MDATE';

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_ChartInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#selTeam").change(function (e) {
                    $("#<%= hfOrgCd.ClientID %>").val(this.value);
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "평가 SLA 월별추이.xlsx", { sheetName: "평가 SLA 월별추이" });
                });

                $(".containMoveText").click(function (e) {
                    $(".containMove").trigger("click");
                });

                $(".containMove").click(function (e) {

                    if (this.title === '내리기') {
                        fn_LayerUp();
                    }
                    else {
                        fn_LayerDown();
                    }
                });
            }
        };

        var fn_Init = function () {
            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            $(".table-search").css('display', 'none');

            obj = $('#selModule');
            obj.empty();
            obj.append("<option value='1'>고객중심 개통</option>");
            obj.append("<option value='2'>고객중심 AS</option>");

            //fn_Month();
            if ($("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'y' || $("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'Y') {
                $('#selTeam').hide();
            }
            else {
                fn_Team();
            }
        }

        var fn_ChartInit = function () {
            fn_ChartCombo();
            fn_LayerInit();

            objAxesX = 'selChartAxesX'; //X축 콤보박스 ID값 설정
            objAxesY = 'selChartAxesY';//Y축 콤보박스 ID값 설정
        }

        var fn_ChartCombo = function (val) {
        }

        var fn_LayerInit = function () {
            fn_LayerDown();
        }

        var fn_LayerUp = function () {
            $(".chartWrapper").show();
            $(".chartWrapper").addClass("opendChart");
            $(".containMove").attr("src", "/Resource/images/icon_arr_move_up.png");
            $(".containMove").attr("title", "올리기");
            $(".containMoveText").html('<span>차트</span>닫기');
        }

        var fn_LayerDown = function () {
            $(".chartWrapper").hide();
            $(".chartWrapper").removeClass("opendChart");
            $(".containMove").attr("src", "/Resource/images/icon_arr_move_down.png");
            $(".containMove").attr("title", "내리기");
            $(".containMoveText").html('<span>차트</span>보기');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var gridCellShow = false;
        var fn_CellShowHide = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (gridCellShow) {
                if (value == '0') {
                    ktsGrid.showColumn("ORGNM1");
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                    ktsGrid.hideColumn("OFFICENM");
                }
                else if (value == '1') {
                    ktsGrid.showColumn("ORGNM1");
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                    ktsGrid.hideColumn("OFFICENM");
                }
                else if (value == '2') {
                    ktsGrid.hideColumn("ORGNM1");
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                    ktsGrid.hideColumn("OFFICENM");
                }
                else if (value == '3') {
                    ktsGrid.hideColumn("ORGNM1");
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                    ktsGrid.showColumn("OFFICENM");
                }

                gridCellShow = false;
            }

            return value;
        }

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "월", field: "MDATE", sorter: "number", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "국소코드", field: "OFFICECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국소", field: "OFFICENM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조직레벨", field: "ORGLV", sorter: "string", formatter: fn_CellShowHide, visible: false },
                {
                    title: '고객중심 개통', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "GL", field: "CSGT_GL", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "대상건", field: "CSGT_TARGET_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "2일이내처리건", field: "CSGT_FIX_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "실적", field: "CSGT_RESULT_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "달성율(%)", field: "CSGT_REACH_RATE", sorter: "number", width: 160, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '고객중심 AS', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "GL", field: "CSAS_GL", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "대상건", field: "CSAS_TARGET_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "처리건", field: "CSAS_FIX_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "실적", field: "CSAS_RESULT_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "달성율(%)", field: "CSAS_REACH_RATE", sorter: "number", width: 160, formatter: fn_CellNumber },
                    ]
                },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });

                if ($("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'y' || $("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'Y') {
                    $('#selTeam').hide();
                }
                else {
                    fn_Team();
                }
            }
            else if (callGb == 'selTeam') {
                var dataCnt = 0;
                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.OFFICECD + "'>" + item.OFFICENM + "</option>");
                    dataCnt++;
                });

                if (dataCnt > 1) {
                    $("#" + callGb + " option:eq(0)").prop("selected", true);
                    $("#<%= hfOrgCd.ClientID %>").val(obj.val());
                    $("#<%= hfOrgCdFix.ClientID %>").val(obj.val());

                    obj.show();
                    $("#<%=txbTeam.ClientID %>").hide();
                }
                else {
                    obj.hide();
                }

                callGb = '';
            }
            else if (callGb == 'label') {
                callGb = '';
                getJsonLabelData = json;

                fn_Search();
            }
            else if (callGb == 'search') {
                callGb = '';
                getJsonLabelData = json;

                fn_Search();
            }
        }

        var fn_OrgPartner = function (orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCdFix.ClientID %>").value;
            }

            var form = "/Common/OrgPartners";
            var target = "OrgPartners";
            var param = {
                pCheckType: 'R',
                pType: 'TEAM',
                pUpperOrgCd: orgcd,
                pOfficeCd: '',
                pTeamCd: '',
                pKtsYn: ''
            };
            var nWidth = 1180;
            var nHeight = 580;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetOrgPartner = function (officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(officeCd);
            $("#<%=txbTeam.ClientID %>").val(officeNm);
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var fn_SearchChk = function () {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        var fn_ChartDataSet = function (datasetNm, chartType, checkKey, datakey, labelKey) {
            var dataset;

            chartDataCnt = 0;

            var label = datasetNm;

            chartData = dataFilter3(labelKey, datakey, checkKey);

            if (chartDataCnt > 0) {
                dataset = setDataSetCustom(chartType, label, chartData, datakey, false);
                chartConfig.data.datasets.push(dataset);
            }
        }

        var fn_Chart = function (ajaxJsonData) {

            setChartZoom();
            setChartOption();
            setChartConfig();

            $(".chartCombobox").css('display', 'block');
            $(".chartTitle").text($("#selModule option:checked").text() + ' Chart');
            setChartTitle('', '', '');

            parseJsonData(ajaxJsonData);

            //annotation 값 넣기
            annotationLabel.content = getJsonOneValue(JSON.parse(ajaxJsonData), orgNm);

            //차트 라벨컬럼들 세팅
            setChartDataLabels(mDate, JSON.parse(ajaxJsonData));

            chartDataColorIdx = 0; //초기화
            chartLineDataColorIdx = 0;
            chartBarDataColorIdx = 0;
            chartConfig.data.datasets = []; //초기화

            var datakey1 = '';
            var datakey2 = '';

            if ($('#selModule').val() == '1') {
                datakey1 = 'CSGT_RESULT_CNT';
                datakey2 = 'CSGT_GL';
            }
            else if ($('#selModule').val() == '2') {
                datakey1 = 'CSAS_RESULT_CNT';
                datakey2 = 'CSAS_GL';

            }

            //차트 데이터 세팅 입력 "bar", "line"
            var datasetNm = '실적';
            fn_ChartDataSet(datasetNm, 'bar', mDate, datakey1, mDate);

            datasetNm = 'GL';
            fn_ChartDataSet(datasetNm, 'line', mDate, datakey2, mDate);


            //선형 차트채우기 false
            for (var i = 0; i < chartConfig.data.datasets.length; i++) {
                chartConfig.data.datasets[i].fill = false;
                //chartConfig.data.datasets[i].borderColor = dataSetBdColor;
                //chartConfig.data.datasets[i].backgroundColor = dataSetBgColor;
            }

            //바형 두께
            chartConfig.options.maxBarThickness = 100;

            //데이터표시 false
            chartConfig.options.plugins.datalabels.display = false;

            //차트x축 false
            chartConfig.options.scales.x.grid.display = false;

            //라벨 플로그인 추가
            chartPlugins.push(ChartDataLabels);
            chartPlugins.push(ChartZoom);

            chartObjNm = 'ktsChart'; //차트 div id 지정
            chartParentObjNm = 'chartContainer'; //차트 div 부모 class 지정

            //차트크기
            setChartWidthHeight(true, 8);

            ctx = document.getElementById(chartObjNm).getContext('2d');

            //차트 그리기
            chartObj = new Chart(ctx, chartConfig);

            //차트y축
            setChartAxesY('y');
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Team = function () {
            callGb = 'selTeam';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUserOrg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Label = function () {
            if (fn_SearchChk()) {

                callGb = 'label';

                var pDate1 = '';
                var pDate2 = '';

                pDate1 = $(".stMonth").val();
                pDate2 = $(".etMonth").val();

                //입력 파라미터
                var aData = [];
                aData[0] = 'LABEL';
                aData[1] = pDate1;
                aData[2] = pDate2;
                aData[3] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                var jsonData = JSON.stringify({ aData: aData });
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetLabel');

                ajaxCallEx(jsonData, ajaxUrl, true);
            }
        }

        var gridColShow = function () {
            ktsGrid.hideColumn("CSGT_GL");
            ktsGrid.hideColumn("CSGT_TARGET_CNT");
            ktsGrid.hideColumn("CSGT_FIX_CNT");
            ktsGrid.hideColumn("CSGT_RESULT_CNT");
            ktsGrid.hideColumn("CSGT_REACH_RATE");
            ktsGrid.hideColumn("CSAS_GL");
            ktsGrid.hideColumn("CSAS_TARGET_CNT");
            ktsGrid.hideColumn("CSAS_FIX_CNT");
            ktsGrid.hideColumn("CSAS_RESULT_CNT");
            ktsGrid.hideColumn("CSAS_REACH_RATE");

            if ($('#selModule').val() == '1') {
                ktsGrid.showColumn("CSGT_GL");
                ktsGrid.showColumn("CSGT_TARGET_CNT");
                ktsGrid.showColumn("CSGT_FIX_CNT");
                ktsGrid.showColumn("CSGT_RESULT_CNT");
                ktsGrid.showColumn("CSGT_REACH_RATE");
            }
            else if ($('#selModule').val() == '2') {
                ktsGrid.showColumn("CSAS_GL");
                ktsGrid.showColumn("CSAS_TARGET_CNT");
                ktsGrid.showColumn("CSAS_FIX_CNT");
                ktsGrid.showColumn("CSAS_RESULT_CNT");
                ktsGrid.showColumn("CSAS_REACH_RATE");
            }

        }

        var fn_Search = function () {
            if (fn_SearchChk()) {

                var pDate1 = '';
                var pDate2 = '';

                callGb = 'search';

                gridCellShow = true;

                pDate1 = $(".stMonth").val();
                pDate2 = $(".etMonth").val();

                gridColShow();

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = pDate1;
                aData[2] = pDate2;
                aData[3] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox">
                    <label>기간</label>
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <span class="optionbox">
                                <select id="selTeam" style="width:160px;display:none"></select>
                            </span>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCdFix" runat="server"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="optionbox" style="margin-right:20px">
                    <label>지표</label>
                    <select id="selModule">
                    </select>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                <input id="hdnStMonth" type="hidden" />
                <input id="hdnEtMonth" type="hidden" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:chartWrapper -->
        <div class="chartWrapper">
            <div class="chartCombobox" style="display: none;">
                <span class="chartTitle"></span>
            </div>
            <div class="chartContainer">
                <canvas id="ktsChart"></canvas>
            </div>
        </div>
        <!-- E:chartWrapper -->
        <div class="datalistmove">
            <label class="containMoveText"><span>차트</span>보기</label>
            <img class="containMove" src="/Resource/images/icon_arr_move_up.png" title="올리기" />
        </div>
        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>SLA 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>