﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EvalDashboardPopup.aspx.cs" Inherits="KTSSolutionWeb.EvalDashboardPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>직영지표조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script src='/Resource/js/chartjs/chart.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-datalabels.min.js' type="module" ></script>
    <!--<script src='/Resource/js/chartjs/chartjs-plugin-colorschemes.js' type="module" ></script>-->
    <script src='/Resource/js/chartjs/chartjs-plugin-annotation.js' type="module" ></script>
    <script src='/Resource/js/chartjs/hammer.min.js' ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-zoom.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/utils.js' ></script>
    <script src='/Resource/js/chartjs/kts_chartjs-1.0.js' ></script>
    <link href="/Resource/css/dashboard.css?20240621" rel="stylesheet" />
    <link href="/Resource/css/chartjs.css?20240624" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 2px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var PRODUCTDATACOL = ["MDATE", "ORGNM", "CNT_OPEN", "CNT_AS", "SALES_OPEN", "SALES_AS", "POINT_OPEN", "POINT_AS", "POINT_MAN", "POINT_4PAGW", "POINT_BUCKET", "POINT_DISPATCH", "POINT_VAN", "POINT_WIFI", "POINT_2PEOPLE1", "POINT_CANCEL", "POINT_SINHOSU", "POINT_DANJA", "SALES_PAIRING", "SALES_TERMINAL", "SALES_CSRESULT"];
        var KPIDATACOL = ["MDATE", "ORGNM", "VOC_GL", "VOC_OCCUR_ECNT", "VOC_REACH_ERATE", "GTAS_GL", "GTAS_OCCUR_RATE", "GTAS_REACH_RATE", "CSI_GL", "CSI_RESULT_CNT", "CSI_REACH_RATE", "NVOC_GL", "NVOC_RESULT_CNT", "NVOC_REACH_RATE", "ASNO_GL", "ASNO_RESULT_CNT", "ASNO_REACH_RATE"];
        var SLADATACOL = ["MDATE", "ORGNM", "CSGT_GL", "CSGT_RESULT_CNT", "CSGT_REACH_RATE", "CSAS_GL", "CSAS_RESULT_CNT", "CSAS_REACH_RATE"];
        var CARDATACOL = ["MDATE", "ORGNM", "CARTYPENM", "HOLD_EMP_RACE", "HOLD_EMP_IDLE", "HOLD_PUBLIC_RACE", "HOLD_PUBLIC_IDLE", "RACE_EMP_RACE", "RACE_EMP_IDLE", "RACE_PUBLIC_RACE", "RACE_PUBLIC_IDLE", "RACEDISTANCE_EMP_RACE", "RACEDISTANCE_EMP_IDLE", "RACEDISTANCE_PUBLIC_RACE", "RACEDISTANCE_PUBLIC_IDLE", "FUELUSE_EMP_RACE", "FUELUSE_EMP_IDLE", "FUELUSE_PUBLIC_RACE", "FUELUSE_PUBLIC_IDLE"];
        

        var PRODUCTDATASETNM = ["개통건수", "AS건수"];
        var PRODUCTDATAKEY = ["CNT_OPEN", "CNT_AS"];
        var PRODUCTCHARTTYPE = ["bar", "bar"];

        var KPIDATASETNM = ["품질VOC 달성율", "작업후고장 달성율", "대외CSI 달성율", "N-VOC 달성율", "AS무출동 달성율"];
        var KPIDATAKEY = ["VOC_REACH_ERATE", "GTAS_REACH_RATE", "CSI_REACH_RATE", "NVOC_REACH_RATE", "ASNO_REACH_RATE"];
        var KPICHARTTYPE = ["bar", "bar", "bar", "bar", "bar"];

        var SLADATASETNM = ["고객중심개통 달성율", "고객중심AS 달성율"];
        var SLADATAKEY = ["CSGT_REACH_RATE", "CSAS_REACH_RATE"];
        var SLACHARTTYPE = ["bar", "bar"];

        var CARDATASETNM = ["운행거리", "유류사용량"];
        var CARDATAKEY = ["MON_RACEDISTANCE", "MON_FUELUSE"];
        var CARCHARTTYPE = ["line", "bar"];
        var CARCHARTOBJNM = ["ktsChartCar1", "ktsChartCar2"];

        var carButtonClick = 'N';

        var mDate = 'MDATE';
        var odr = 'ODR';
        var orgNm = 'ORGNM';
        var orgSeq = 0;

        var endDate = '';

        var atSectionXMax = 100;
        var atSectionContent = '';

        var getPRODUCTJsonData;
        var getKPIJsonData;
        var getSLAJsonData;
        var getCARJsonData;
        

        var chartAjaxJsonData;

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#gridExcel").click(function (e) {
                    if ($('#tblProductListData').is(':visible')) {
                        fn_excelDown('tblProductListData', 'CS생산성 추이', 'CS생산성 추이', 'xlsx');
                    }
                    else if ($('#tblKpiListData').is(':visible')) {
                        fn_excelDown('tblKpiListData', 'KPI 추이', 'KPI 추이', 'xlsx');
                    }
                    else if ($('#tblSlaListData').is(':visible')) {
                        fn_excelDown('tblSlaListData', 'SLA 추이', 'SLA 추이', 'xlsx');
                    }
                    else if ($('#tblCarListData').is(':visible')) {
                        fn_excelDown('tblCarListData', '기동장비 추이', '기동장비 추이', 'xlsx');
                    }
                });

                $('.tabnav a').click(function (e) {
                    $('.graph-area > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    if (this.text === 'CS생산성 추이') {
                        $('#tblProductListData').show();
                        $('#tblKpiListData').hide();
                        $('#tblSlaListData').hide();
                        $('#tblCarListData').hide();
                    }
                    else if (this.text === 'KPI 추이') {
                        $('#tblProductListData').hide();
                        $('#tblKpiListData').show();
                        $('#tblSlaListData').hide();
                        $('#tblCarListData').hide();
                    }
                    else if (this.text === 'SLA 추이') {
                        $('#tblProductListData').hide();
                        $('#tblKpiListData').hide();
                        $('#tblSlaListData').show();
                        $('#tblCarListData').hide();
                    }
                    else if (this.text === '기동장비 추이') {
                        $('#tblProductListData').hide();
                        $('#tblKpiListData').hide();
                        $('#tblSlaListData').hide();
                        $('#tblCarListData').show();
                    }

                    return false;
                }).filter(':eq(0)').click();

            }
        };

        var fn_Init = function () {
            var vHeight = '100%';
            var vWidth = '100%';

            endDate = $("#<%=hdnDate2.ClientID %>").val();

            fn_Title();

            $('#tblKpiListData').hide();
            $('#tblSlaListData').hide();
            $('#tblCarListData').hide();
            
            $(window).resize(function () {
                $('.pop').css('width', $(window).width());
                $('.pop').css('height', $(window).height());
            });
        }

        var fn_Title = function () {
            var val = endDate;
            var year = '';
            var month = '';
            var htmlTxt = '';

            if (val.length == 6) {
                year = val.substr(0, 4);
                month = val.substr(4, 2);
            }

            htmlTxt = ": " + year + '.' + month;

            $('#lblTitleMon').text(htmlTxt);
        }

        var fn_Load = function () {
            callGb = 'PRODUCT';
            fn_Search(callGb);
        }

        var fn_excelDown = function (id, sheetNm, fileNm, type, fn, ) {
            var elt = document.getElementById(id);
            var wb = XLSX.utils.table_to_book(elt, { sheet: sheetNm });
            XLSX.writeFile(wb, fileNm + '.' + type);
        }

        var fn_TopData = function (data, danwi) {

            if (data != undefined) {
                if (data != '') {
                    return priceToString(data) + danwi;
                }
            }

            return '';
        }

        var ajaxCallBack = function (json) {

            if (callGb == 'PRODUCT') {
                getPRODUCTJsonData = json;

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ODR == '1' && item.MDATE == endDate) {
                        $("#lblTotGtCnt").text(fn_TopData(item.TOT_GT_CNT, '건'));
                        $("#lblTotAsCnt").text(fn_TopData(item.TOT_AS_CNT, '건'));
                    }
                    
                });

                chartBarDataColorIdx = 0;
                chartLineDataColorIdx = 0;

                mDate = 'MDATE';
                fn_ChartStack(json, 'ktsChart1', PRODUCTDATASETNM, PRODUCTDATAKEY, 'chart1', PRODUCTCHARTTYPE, 'x', true);
                fn_ListBind(callGb);

                callGb = 'KPI';
                fn_Search(callGb);
            }
            else if (callGb == 'KPI') {
                getKPIJsonData = json;

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ODR == '1' && item.MDATE == endDate) {
                        $("#lblTotVocReachRate").text(fn_TopData(item.TOT_VOC_REACH_ERATE, '%'));
                        $("#lblTotGtasReachRate").text(fn_TopData(item.TOT_GTAS_REACH_RATE, '%'));
                        $("#lblTotCsiReachRate").text(fn_TopData(item.TOT_CSI_REACH_RATE, '%'));
                        $("#lblTotNvocReachRate").text(fn_TopData(item.TOT_NVOC_REACH_RATE, '%'));
                        $("#lblTotAsnoReachRate").text(fn_TopData(item.TOT_ASNO_REACH_RATE, '%'));
                    }
                });

                chartBarDataColorIdx = 0;
                chartLineDataColorIdx = 0;

                mDate = 'MDATE';
                fn_Chart(json, 'ktsChart2', KPIDATASETNM, KPIDATAKEY, 'chart2', KPICHARTTYPE, 'y', false);
                fn_ListBind(callGb);

                callGb = 'SLA';
                fn_Search(callGb);
            }
            else if (callGb == 'SLA') {
                getSLAJsonData = json;

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ODR == '1' && item.MDATE == endDate) {
                        $("#lblTotCsgtReachRate").text(fn_TopData(item.TOT_CSGT_REACH_RATE, '%'));
                        $("#lblTotCsasReachRate").text(fn_TopData(item.TOT_CSAS_REACH_RATE, '%'));
                    }
                });

                chartBarDataColorIdx = 0;
                chartLineDataColorIdx = 0;

                mDate = 'MDATE';
                fn_Chart(json, 'ktsChart3', SLADATASETNM, SLADATAKEY, 'chart3', SLACHARTTYPE, 'y', false);
                fn_ListBind(callGb);

                callGb = 'CAR';
                fn_Search(callGb);
            }
            else if (callGb == 'CAR') {
                getCARJsonData = json;

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ODR == '1' && item.MDATE == endDate) {
                        $("#lblTotCarCnt1").text(fn_TopData(item.TOT_RACE, '대'));
                        $("#lblTotCarCnt2").text(fn_TopData(item.TOT_RACEDISTANCE, '㎞'));
                        $("#lblTotCarCnt3").text(fn_TopData(item.TOT_FUELUSE, 'ℓ'));
                    }
                });

                chartBarDataColorIdx = 0;
                chartLineDataColorIdx = 0;

                mDate = 'MDATE';
                orgSeq = 0;

                fn_ChartCar(json, CARCHARTOBJNM, CARDATASETNM, CARDATAKEY, '', CARCHARTTYPE, 'x', true, 1);
                fn_ListBind(callGb);

                callGb = '';

                $(window).resize(function () {
                    $('.pop').css('width', $(window).width());
                    $('.pop').css('height', $(window).height());
                });
            }

        }


        var fn_ChartButtonLeft = function (buttonGubun) {
            if (buttonGubun == '2') {
                orgSeq--;

                if (orgSeq < 0) {
                    orgSeq = ajaxOrg.length - 1;
                }

                chartBarDataColorIdx = 0;
                chartLineDataColorIdx = 0;

                mDate = 'MDATE';

                carButtonClick = 'Y';
                fn_ChartCar(getCARJsonData, CARCHARTOBJNM, CARDATASETNM, CARDATAKEY, 'chartRight', CARCHARTTYPE, 'x', true, 2);
            }
        }

        var fn_ChartButtonRight = function (buttonGubun) {
            if (buttonGubun == '2') {
                orgSeq++;

                if (ajaxOrg.length <= orgSeq) {
                    orgSeq = 0;
                }

                chartBarDataColorIdx = 0;
                chartLineDataColorIdx = 0;

                mDate = 'MDATE';

                carButtonClick = 'Y';
                fn_ChartCar(getCARJsonData, CARCHARTOBJNM, CARDATASETNM, CARDATAKEY, 'chartRight', CARCHARTTYPE, 'x', true, 2);
            }
        }


        var fn_ChartDataSet = function (datasetNm, chartType, orgKey, orgValue, datakey, labelKey, groupKey, groupValue) {
            var dataset;

            chartDataCnt = 0;

            var label = datasetNm;

            chartData = dataFilter4(labelKey, datakey, orgKey, orgValue, groupKey, groupValue);

            if (chartDataCnt > 0) {
                dataset = setDataSetCustom(chartType, label, chartData, datakey, false);
                chartConfig.data.datasets.push(dataset);
            }
        }

        var fn_ChartDataSet2 = function (ajaxJsonData, datasetNm, chartType, valueKeys, monthKey, orgKey, groupKey, groupValue) {
            var dataset;
            var ajaxMonth = [];
            var json = JSON.parse(ajaxJsonData);

            ajaxMonth = getChartMonthValue(json, mDate);

            for (var z = 0; z < valueKeys.length; z++) {
                var valueKey = valueKeys[z];

                for (var i = 0; i < ajaxMonth.length; i++) {
                    var checkMonth = ajaxMonth[i];

                    chartData = [];
                    chartDataCnt = 0;

                    for (var j = 0; j < ajaxOrg.length; j++) {
                        var orgValue = ajaxOrg[j];

                        for (var k = 0; k < json.length; k++) {
                            var orgData = json[k][orgKey];
                            var groupData = json[k][groupKey];
                            var monthData = json[k][monthKey];
                            var inputData = json[k][valueKey];

                            if (groupData == groupValue) {
                                if (orgData == orgValue) {
                                    if (checkMonth == monthData) {
                                        if (inputData == undefined) {
                                            inputData = '';
                                        }
                                        else {
                                            if (inputData == 'null' || inputData == 'NULL') {
                                                inputData = '';
                                            }
                                        }
                                        chartDataCnt++;
                                        chartData.push(inputData);
                                    }

                                }
                            }

                        }
                    }

                    if (chartDataCnt > 0) {
                        var label = datasetNm[z] + '[' + checkMonth + ']';

                        dataset = setDataSetCustom(chartType, label, chartData, valueKey, false, valueKey);
                        chartConfig.data.datasets.push(dataset);
                    }
                }
            }

        }

        var fn_ChartCar = function (ajaxJsonData, chartObjNm, datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked, gubun) {
            chartAjaxJsonData = ajaxJsonData;

            if (gubun == '1') {
                //상위조직 세팅
                setChartDataOrgs(JSON.parse(ajaxJsonData), orgNm, odr, '1');
                fn_ChartBind(ajaxJsonData, chartObjNm[0], '1', '', ajaxParentOrg, ajaxOrg, datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked);

                //하위조직 세팅
                setChartDataOrgs(JSON.parse(ajaxJsonData), orgNm, odr, '2');
            }

            chartBarDataColorIdx = 0;
            chartLineDataColorIdx = 0;

            fn_ChartBind(ajaxJsonData, chartObjNm[1], '2', '', ajaxOrg, ajaxOrg, datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked);
        }

        var fn_ChartStack = function (ajaxJsonData, chartObjNm, datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked) {
            chartAjaxJsonData = ajaxJsonData;

            //상위조직 세팅
            setChartDataOrgs(JSON.parse(ajaxJsonData), orgNm, odr, '1');
            //하위조직 세팅
            setChartDataOrgs(JSON.parse(ajaxJsonData), orgNm, odr, '2');

            fn_ChartBind(ajaxJsonData, chartObjNm, '1', '', ajaxParentOrg, ajaxOrg, datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked);
        }

        var fn_Chart = function (ajaxJsonData, chartObjNm, datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked) {
            chartAjaxJsonData = ajaxJsonData;

            //상위조직 세팅
            setChartDataOrgs(JSON.parse(ajaxJsonData), orgNm, odr, '1');

            fn_ChartBind(ajaxJsonData, chartObjNm, '1', '', ajaxParentOrg, '', datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked);
        }

        var evtOnResize = function (obj) {
            if (obj.canvas.id == 'ktsChart1') {
                obj.options.plugins.annotation = '';
            }
        }
        
        var fn_ChartBind = function (ajaxJsonData, chartObjNm, gubun, buttonGubun, orgNmData, orgNmData2, datasetNm, datakey, parentObjNm, chartType, chartAxis, stacked) {

            setChartObjNm(chartObjNm);

            setChartZoom();
            setChartOption();
            setChartConfig();
            
            parseJsonData(ajaxJsonData);

            if (callGb == 'CAR' || carButtonClick == 'Y') {
                setChartTitle(orgNmData[orgSeq], '', '');
            }
            else {
                setChartTitle(orgNmData[0], '', '');
            }

            chartDataColorIdx = 0; //초기화
            chartConfig.data.datasets = []; //초기화

            chartResponsiveHeight = $('.graph-area').height();

            if (callGb == 'PRODUCT') {
                //차트 라벨컬럼들 세팅
                setChartDataLabels(orgNm, JSON.parse(ajaxJsonData), odr, '2');
                fn_ChartDataSet2(ajaxJsonData, datasetNm, chartType[0], datakey, mDate, orgNm, odr, '2');
            }
            else if (callGb == 'CAR' || carButtonClick == 'Y') {
                //차트 라벨컬럼들 세팅
                setChartDataLabels(mDate, JSON.parse(ajaxJsonData), odr, gubun);

                if (datasetNm.length > 0) {
                    for (var i = 0; i < datasetNm.length; i++) {
                        fn_ChartDataSet(datasetNm[i], chartType[i], orgNm, orgNmData[orgSeq], datakey[i], mDate, odr, gubun);
                    }
                }
            }
            else {
                //차트 라벨컬럼들 세팅
                setChartDataLabels(mDate, JSON.parse(ajaxJsonData), odr, '1');

                if (datasetNm.length > 0) {
                    for (var i = 0; i < datasetNm.length; i++) {
                        fn_ChartDataSet(datasetNm[i], chartType[i], orgNm, orgNmData, datakey[i], mDate, odr, gubun);
                    }
                }
            }

            //선형 차트채우기 false
            for (var i = 0; i < chartConfig.data.datasets.length; i++) {
                chartConfig.data.datasets[i].fill = false;
                //chartConfig.data.datasets[i].borderColor = dataSetBdColor;
                //chartConfig.data.datasets[i].backgroundColor = dataSetBgColor;
            }

            //바형 두께
            chartConfig.options.maxBarThickness = 100;

            //데이터표시 false
            chartConfig.options.plugins.datalabels.display = false;

            //차트x축 false
            chartConfig.options.scales.x.grid.display = false;

            chartConfig.options.indexAxis = chartAxis;

            //차트 누젹 유무
            chartConfig.options.scales.x.stacked = stacked;
            chartConfig.options.scales.y.stacked = stacked;

            chartPlugins = [];
            chartAnnotation = [];

            //라벨 플로그인 추가
            chartPlugins.push(ChartDataLabels);
            chartPlugins.push(ChartZoom);
            //chartPlugins.push(ChartColorSchemes);

            annotationSection.xMax = atSectionXMax;
            annotationSection.label.content = atSectionContent;

            chartAnnotation.push(annotationSection);
            chartPlugins.push(ChartAnnotation);
            
            if (callGb == 'CAR' || callGb == 'PRODUCT') {
                chartConfig.options.plugins.annotation = {};
            }

            if (callGb == 'CAR' || carButtonClick == 'Y') {

                chartConfig.options.plugins.annotation = {};

                if (gubun == '1') {
                    parentObjNm = 'chartLeft'; //차트 div 부모 class 지정
                    //차트크기
                    setChartBasicWidthHeight(parentObjNm, '');
                }
                else if (gubun == '2') {
                    parentObjNm = 'chartRight'; //차트 div 부모 class 지정
                    //차트크기
                    setChartBasicWidthHeight(parentObjNm, '2');
                }

                carButtonClick == 'N';
            }
            
            
            //차트크기
            setChartBasicWidthHeight(parentObjNm, buttonGubun);

            var chartCtx = document.getElementById(chartObjNm).getContext('2d');
            setChartCtx(chartCtx);

            //차트 그리기
            var chart = new Chart(chartCtx, chartConfig);
            setChartObj(chart);

            //줌모드
            //setChartZoomMode('y');

            //차트y축
            setChartAxesY('y');

        }

        var fn_ListHtml = function (json, col, odr, module) {
            var html = '';
            var orgData = '';
            var colOrgData = '';
            var endLine = false;

            if (module == 'CAR') {

            }

            if (json != undefined) {
                if (json.length > 0) {
                    for (var i = 0; i < json.length; i++) {
                        var colOdr = json[i][odr];

                        if (colOdr != undefined) {
                            if (colOdr == '1') {
                                html += '<tr class="fc-black">';
                            }
                            else {
                                html += '<tr>';
                            }
                        }
                        else {
                            if (i == 0) {
                                html += '<tr class="fc-black">';
                            }
                            else {
                                html += '<tr>';
                            }
                        }

                        if (i == 0) {
                            colOrgData = json[i]["ORGNM"];
                        }

                        if (json.length > i+1) {
                            colOrgData = json[i + 1]["ORGNM"];
                        }

                        if (orgData == '') {
                            orgData = colOrgData;
                        }

                        if (orgData != colOrgData) {
                            orgData = colOrgData;
                            endLine = true;
                        }

                        for (var j = 0; j < col.length; j++) {
                            var htmlTd = '';
                            var colData = json[i][col[j]];

                            if (colData != undefined) {

                                if (col[j] == 'MDATE') {
                                    htmlTd = '<td>' + colData + '</td>';
                                }
                                else {
                                    htmlTd = '<td>' + priceToString(colData) + '</td>';
                                }
                            }
                            else {
                                htmlTd = '<td></td>';
                            }

                            if (colOdr == '1' && j == 1) {
                                htmlTd = htmlTd.replace('<td>', '<td><span class="border-span green">');
                                htmlTd = htmlTd.replace('</td>', '</span></td>');
                            }
                            else if (colOdr != '1' && j == 1) {
                                htmlTd = htmlTd.replace('<td>', '<td><span class="border-span gray">');
                                htmlTd = htmlTd.replace('</td>', '</span></td>');
                            }

                            if (module == 'CAR') {
                                if (endLine) {
                                    htmlTd = htmlTd.replace('<td', '<td style="border-bottom: 1px solid #6b6b6b"');
                                }
                            }

                            html += htmlTd;
                        }

                        if (endLine) {
                            endLine = false;
                        }

                        html += '</tr>';
                    }
                }
            }

            return html;
        }

        var fn_ListBind = function (module) {
            var html = '';
            var odrCol = 'ODR';

            if (module == "PRODUCT") {
                var json = JSON.parse(getPRODUCTJsonData);
                html = fn_ListHtml(json, PRODUCTDATACOL, odrCol, module);
                $('#productListData').css('text-align', 'center');
                $('#productListData').html(html);
            }
            else if (module == "KPI") {
                var json = JSON.parse(getKPIJsonData);
                html = fn_ListHtml(json, KPIDATACOL, odrCol, module);
                $('#kpiListData').css('text-align', 'center');
                $('#kpiListData').html(html);
            }
            else if (module == "SLA") {
                var json = JSON.parse(getSLAJsonData);
                html = fn_ListHtml(json, SLADATACOL, odrCol, module);
                $('#slaListData').css('text-align', 'center');
                $('#slaListData').html(html);
            }
            else if (module == "CAR") {
                var json = JSON.parse(getCARJsonData);
                html = fn_ListHtml(json, CARDATACOL, odrCol, module);
                $('#carListData').css('text-align', 'center');
                $('#carListData').html(html);
            }
        }

        var fn_Search = function (module) {

            var sDate = $("#<%= hdnDate1.ClientID %>").val();
            var eDate = $("#<%= hdnDate2.ClientID %>").val();
            var orgCd = $("#<%= hdnOrgCd.ClientID %>").val();

            //입력 파라미터
            var aData = [];
            aData[0] = module;
            aData[1] = sDate;
            aData[2] = eDate;
            aData[3] = orgCd;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        function handleClick() {
            let popmodal = document.querySelector(".pop-modal");
            let clickedClass = "on";
            if (popmodal.classList.contains(clickedClass)) {
                popmodal.classList.remove(clickedClass);
            } else {
                popmodal.classList.add(clickedClass);
            };
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wpop-wrap">
            <div class="pop">
                <div class="pop-top">
                    <div style="display:flex"><h3>직영 지표 조회</h3><span id="lblTitleMon" style="font-size: 15px;top: 2px;left:6px;position: relative;"></span></div>
                    <button class="btn-close-pop" title="닫기" onclick="SendParentsForm()">닫기</button>
                    <input id="hdnHeight" type="hidden" runat="server" />
                    <input id="hdnWidth" type="hidden" runat="server" />
                    <input id="hdnDate1" type="hidden" runat="server" />
                    <input id="hdnDate2" type="hidden" runat="server" />
                    <input id="hdnOrgCd" type="hidden" runat="server" />
                </div>
                <div class="pop-body">
                    <div class="flex-area">
                        <div class="line-box wid1">
                            <div class="lb-tit">
                                <p>CS생산성</p>
                                <img src="/Resource/images/icon-pop01.png" alt="CS생산성 아이콘" />
                            </div>
                            <ul>
                                <li>
                                    <p class="small-tit">개통건수</p>
                                    <p><span id="lblTotGtCnt"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">AS건수</p>
                                    <p><span id="lblTotAsCnt"></span></p>
                                </li>
                            </ul>
                        </div>
                        <div class="line-box wid3">
                            <div class="lb-tit">
                                <p>KPI/SLA 달성율</p>
                                <img src="/Resource/images/icon-pop02.png" alt="KPI/SLA 달성률 아이콘" />
                            </div>
                            <ul>
                                <li>
                                    <p class="small-tit">품질VOC</p>
                                    <p><span id="lblTotVocReachRate"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">작업후고장</p>
                                    <p><span id="lblTotGtasReachRate"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">CSI</p>
                                    <p><span id="lblTotCsiReachRate"></span></p>
                                </li>
                                    
                                <li>
                                    <p class="small-tit">VOC</p>
                                    <p><span id="lblTotNvocReachRate"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">무출동</p>
                                    <p><span id="lblTotAsnoReachRate"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">개통</p>
                                    <p><span id="lblTotCsgtReachRate"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">AS</p>
                                    <p><span id="lblTotCsasReachRate"></span></p>
                                </li>
                            </ul>
                        </div>
                        <div class="line-box wid2">
                            <div class="lb-tit">
                                <p>기동장비 운용 현황</p>
                                <img src="/Resource/images/icon-pop03.png" alt="기동장비 운용 현황 아이콘" />
                            </div>
                            <ul>
                                <li>
                                    <p class="small-tit">운행차량</p>
                                    <p><span id="lblTotCarCnt1"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">운행거리</p>
                                    <p><span id="lblTotCarCnt2"></span></p>
                                </li>
                                <li>
                                    <p class="small-tit">유류 사용량</p>
                                    <p><span id="lblTotCarCnt3"></span></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="pt-30">
                        <div class="tab-area">
                            <ul class="tabnav">
                                <li><a href="#tab01">CS생산성 추이</a></li>
                                <li><a href="#tab02">KPI 추이</a></li>
                                <li><a href="#tab03">SLA 추이</a></li>
                                <li><a href="#tab04">기동장비 추이</a></li>
                            </ul>
                        </div>
                        <div class="graph-area">
                            <div id="tab01">
                                <div class="chartContainer">
                                    <div class="chart1"></div>
                                </div>
                            </div>
                            <div id="tab02">
                                <div class="chartContainer">
                                    <div class="chart2"></div>
                                </div>
                            </div>
                            <div id="tab03">
                                <div class="chartContainer">
                                    <div class="chart3"></div>
                                </div>
                            </div>
                            <div id="tab04">
                                <div class="chartContainer">
                                    <div class="chartLeft"></div>
                                    <div class="chartRight"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pt-10">
                        <div class="pull-right">
				            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" style="float:right;" value="엑셀" />
				            </div>
                            <div style="display: block;padding:10px;"><span>&nbsp;</span></div>
			            </div>
                        <div class="poptable-st" style="bottom:0;overflow:auto">
                            <table id="tblProductListData">
                                <colgroup>
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                    <col style="width: 4.76%;">
                                </colgroup>
                                <thead>
                                    <tr class="bg-light">
                                        <th class="no-bor left">월</th>
                                        <th>구분</th>
                                        <th>개통건수</th>
                                        <th>AS건수</th>
                                        <th>개통 매출</th>
                                        <th>AS 매출</th>
                                        <th>개통 포인트</th>
                                        <th>AS 포인트</th>
                                        <th>수작업정산</th>
                                        <th>4P-AGW</th>
                                        <th>버킷</th>
                                        <th>파견</th>
                                        <th>VAN</th>
                                        <th>공공WiFi</th>
                                        <th>2인1조</th>
                                        <th>무효화</th>
                                        <th>신호수</th>
                                        <th>단자이설</th>
                                        <th>페어링 차감</th>
                                        <th>단말회수</th>
                                        <th class="no-bor right">CS실적급</th>
                                    </tr>
                                </thead>
                                <tbody id="productListData">
                                </tbody>
                            </table>
                            <table id="tblKpiListData">
                                <colgroup>
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                    <col style="width: 5.88%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th rowspan="3" class="no-bor left">월</th>
                                        <th rowspan="3">구분</th>
                                        <th colspan="6">고객품질개선</th>
                                        <th colspan="6">고객만족도</th>
                                        <th colspan="3" rowspan="2" class="no-bor right">AS 무출동</th>
                                    </tr>
                                    <tr>
                                        <th colspan="3">(품질 VOC)</th>
                                        <th colspan="3">(작업후고장)</th>
                                        <th colspan="3">(대외CSI)</th>
                                        <th colspan="3">(N-VOC)</th>
                                    </tr>
                                    <tr class="bg-light">
                                        <th>G/L</th>
                                        <th>실적</th>
                                        <th>달성율(%)</th>
                                        <th>G/L</th>
                                        <th>실적</th>
                                        <th>달성율(%)</th>
                                        <th>G/L</th>
                                        <th>실적</th>
                                        <th>달성율(%)</th>
                                        <th>G/L</th>
                                        <th>실적</th>
                                        <th>달성율(%)</th>
                                        <th>G/L</th>
                                        <th>실적</th>
                                        <th class="no-bor right">달성율(%)</th>
                                    </tr>
                                </thead>
                                <tbody id="kpiListData">
                                </tbody>
                            </table>
                            <table id="tblSlaListData">
                                <colgroup>
                                    <col style="width: 12.5%;">
                                    <col style="width: 12.5%;">
                                    <col style="width: 12.5%;">
                                    <col style="width: 12.5%;">
                                    <col style="width: 12.5%;">
                                    <col style="width: 12.5%;">
                                    <col style="width: 12.5%;">
                                    <col style="width: 12.5%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th rowspan="3" class="no-bor left">월</th>
                                        <th rowspan="3">구분</th>
                                        <th colspan="6">SLA 고객만족도</th>
                                    </tr>
                                    <tr>
                                        <th colspan="3">(고객중심 개통)</th>
                                        <th colspan="3">(고객중심 AS)</th>
                                    </tr>
                                    <tr class="bg-light">
                                        <th>G/L</th>
                                        <th>실적</th>
                                        <th>달성율(%)</th>
                                        <th>G/L</th>
                                        <th>실적</th>
                                        <th class="no-bor right">달성율(%)</th>
                                    </tr>
                                </thead>
                                <tbody id="slaListData">
                                </tbody>
                            </table>
                            <table id="tblCarListData">
                                <colgroup>
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                    <col style="width: 5.263%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th rowspan="3" class="no-bor left">월</th>
                                        <th rowspan="3">조직</th>
                                        <th rowspan="3">구분</th>
                                        <th colspan="4">보유차량</th>
                                        <th colspan="4">운행차량</th>
                                        <th colspan="4">운행거리</th>
                                        <th colspan="4">유류 사용량</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">개인</th>
                                        <th colspan="2">공용</th>
                                        <th colspan="2">개인</th>
                                        <th colspan="2">공용</th>
                                        <th colspan="2">개인</th>
                                        <th colspan="2">공용</th>
                                        <th colspan="2">개인</th>
                                        <th colspan="2">공용</th>
                                    </tr>
                                    <tr class="bg-light">
                                        <th>운행</th>
                                        <th>유휴</th>
                                        <th>운행</th>
                                        <th>유휴</th>
                                        <th>운행</th>
                                        <th>유휴</th>
                                        <th>운행</th>
                                        <th>유휴</th>
                                        <th>운행</th>
                                        <th>유휴</th>
                                        <th>운행</th>
                                        <th>유휴</th>
                                        <th>운행</th>
                                        <th>유휴</th>
                                        <th>운행</th>
                                        <th class="no-bor right">유휴</th>
                                    </tr>
                                </thead>
                                <tbody id="carListData">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>