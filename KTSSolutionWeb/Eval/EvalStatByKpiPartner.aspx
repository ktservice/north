﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" CodeBehind="EvalStatByKpiPartner.aspx.cs" Inherits="KTSSolutionWeb.EvalStatByKpiPartner" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script src='/Resource/js/chartjs/chart.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-datalabels.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/hammer.min.js' ></script>
    <script src='/Resource/js/chartjs/chartjs-plugin-zoom.min.js' type="module" ></script>
    <script src='/Resource/js/chartjs/utils.js' ></script>
    <script src='/Resource/js/chartjs/kts_chartjs-1.0.js' ></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/chartjs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"], .containMove, .containMoveText{
            cursor:pointer;
        }

        .datalistmove{
            cursor:default;
        }

        .searchbox {
            border-radius: 6px 6px 0 0;
            border: 1px solid #eee;
            border-bottom: none;
        }

        .list-top {
            padding: 20px 45px 0;
        }

        .chartSpanB {
            margin: 0 0 0 0px !important;
        }

        .optionbox select {
            width: 170px;
        }

    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        
        var callGb;
        var evalDate = 'EVAL_DATE';

        var dateGubun = 'Day';
        var odr = 'ODR';
        var orgNm = 'ORGNM';
        var orgSeq = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_ChartInit();
                fn_Load();
            },
            eventbind: function () {

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#selTeam").change(function (e) {
                    $("#<%= hfOrgCd.ClientID %>").val(this.value);
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "평가 KPI 지표현황.xlsx", { sheetName: "평가 KPI 지표현황" });
                });

                $(".containMoveText").click(function (e) {
                    $(".containMove").trigger("click");
                });

                $(".containMove").click(function (e) {

                    if (this.title === '내리기') {
                        fn_LayerUp();
                    }
                    else {
                        fn_LayerDown();
                    }
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');

            obj = $('#selModule');
            obj.empty();
            obj.append("<option value='1'>대외CSI</option>");
            obj.append("<option value='2'>N-VOC감소율(잠정)</option>");
            obj.append("<option value='3'>AS무출동</option>");

            fn_Month();
        }

        var fn_ChartInit = function () {
            fn_ChartCombo();
            fn_LayerInit();

            objAxesX = 'selChartAxesX'; //X축 콤보박스 ID값 설정
            objAxesY = 'selChartAxesY';//Y축 콤보박스 ID값 설정
        }

        var fn_ChartCombo = function (val) {
        }

        var fn_LayerInit = function () {
            fn_LayerDown();
        }

        var fn_LayerUp = function () {
            $(".chartWrapper").show();
            $(".chartWrapper").addClass("opendChart");
            $(".containMove").attr("src", "/Resource/images/icon_arr_move_up.png");
            $(".containMove").attr("title", "올리기");
            $(".containMoveText").html('<span>차트</span>닫기');
        }

        var fn_LayerDown = function () {
            $(".chartWrapper").hide();
            $(".chartWrapper").removeClass("opendChart");
            $(".containMove").attr("src", "/Resource/images/icon_arr_move_down.png");
            $(".containMove").attr("title", "내리기");
            $(".containMoveText").html('<span>차트</span>보기');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var gridCellShow = false;
        var fn_CellShowHide = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (gridCellShow) {
                if (value == '0') {
                    ktsGrid.showColumn("ORGNM1");
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                    ktsGrid.hideColumn("OFFICENM");
                }
                else if (value == '1') {
                    ktsGrid.showColumn("ORGNM1");
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.hideColumn("ORGNM3");
                    ktsGrid.hideColumn("OFFICENM");
                }
                else if (value == '2') {
                    ktsGrid.hideColumn("ORGNM1");
                    ktsGrid.showColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                    ktsGrid.hideColumn("OFFICENM");
                }
                else if (value == '3') {
                    ktsGrid.hideColumn("ORGNM1");
                    ktsGrid.hideColumn("ORGNM2");
                    ktsGrid.showColumn("ORGNM3");
                    ktsGrid.showColumn("OFFICENM");
                }

                gridCellShow = false;
            }

            return value;
        }

        var fn_GridInit = function () {

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "일", field: "EVAL_DATE", sorter: "number", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "국소코드", field: "OFFICECD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국소", field: "OFFICENM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "조직코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조직", field: "ORGNM", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "조직레벨", field: "ORGLV", sorter: "string", formatter: fn_CellShowHide, visible: false },
                {
                    title: '대외CSI', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "GL", field: "CSI_GL", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "조사건", field: "CSI_CHECK_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "총점수", field: "CSI_TOT_POINT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "실적", field: "CSI_RESULT_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "달성율(%)", field: "CSI_REACH_RATE", sorter: "number", width: 160, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: 'N-VOC감소율(잠정)', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "GL", field: "NVOC_GL", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "발생건", field: "NVOC_OCCUR_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "발생건(e)", field: "NVOC_RESULT_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "달성율(%)", field: "NVOC_REACH_RATE", sorter: "number", width: 160, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: 'AS무출동', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "GL", field: "ASNO_GL", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "무출동", field: "ASNO_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "전체건", field: "ASNO_TOT_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "실적", field: "ASNO_RESULT_CNT", sorter: "number", width: 160, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "달성율(%)", field: "ASNO_REACH_RATE", sorter: "number", width: 160, formatter: fn_CellNumber },
                    ]
                },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var ajaxCallBack = function (json) {
            var obj;

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });

                if ($("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'y' || $("#<%= hdnKtsYn.ClientID %>").val().toLowerCase() == 'Y') {
                    $('#selTeam').hide();
                }
                else {
                    fn_Team();
                }
            }
            else if (callGb == 'selTeam') {
                var dataCnt = 0;
                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.OFFICECD + "'>" + item.OFFICENM + "</option>");
                    dataCnt++;
                });

                if (dataCnt > 1) {
                    $("#" + callGb + " option:eq(0)").prop("selected", true);
                    $("#<%= hfOrgCd.ClientID %>").val(obj.val());
                    $("#<%= hfOrgCdFix.ClientID %>").val(obj.val());

                    obj.show();
                    $("#<%=txbTeam.ClientID %>").hide();
                }
                else {
                    obj.hide();
                }

                callGb = '';
            }
        }

        var fn_OrgPartner = function (orgcd, empno) {

            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCdFix.ClientID %>").value;
            }

            var form = "/Common/OrgPartners";
            var target = "OrgPartners";
            var param = {
                pCheckType:'R',
                pType:'TEAM',
                pUpperOrgCd: orgcd,
                pOfficeCd:'',
                pTeamCd:'',
                pKtsYn:''
            };
            var nWidth = 1180;
            var nHeight = 580;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetOrgPartner = function (officeNm, officeCd, teamNm, teamCd, upperOrgNm, upperOrgCd, orgFullNm) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(officeCd);
            $("#<%=txbTeam.ClientID %>").val(officeNm);
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
            
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var fn_SearchChk = function () {
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        var fn_ChartDataSet = function (datasetNm, chartType, checkKey, datakey, labelKey) {
            var dataset;

            chartDataCnt = 0;

            var label = datasetNm;

            chartData = dataFilter3(labelKey, datakey, checkKey, dateGubun);

            if (chartDataCnt > 0) {
                dataset = setDataSetCustom(chartType, label, chartData, datakey, false);
                chartConfig.data.datasets.push(dataset);
            }
        }

        var fn_Chart = function (ajaxJsonData) {

            setChartZoom();
            setChartOption();
            setChartConfig();

            $(".chartCombobox").css('display', 'block');
            $(".chartTitle").text($("#selModule option:checked").text() + ' Chart');
            setChartTitle('', '', '');

            parseJsonData(ajaxJsonData);

            //annotation 값 넣기
            annotationLabel.content = getJsonOneValue(JSON.parse(ajaxJsonData), orgNm);

            //차트 라벨컬럼들 세팅
            setChartDataLabels(evalDate, JSON.parse(ajaxJsonData), '', '', dateGubun);

            chartDataColorIdx = 0; //초기화
            chartLineDataColorIdx = 0;
            chartBarDataColorIdx = 0;
            chartConfig.data.datasets = []; //초기화

            var datakey1 = '';
            var datakey2 = '';
            var datasetNm = '실적';

            if ($('#selModule').val() == '1') {
                datasetNm = '실적';
                datakey1 = 'CSI_RESULT_CNT';
                datakey2 = 'CSI_GL';
            }
            else if ($('#selModule').val() == '2') {
                datasetNm = '발생건(e)';
                datakey1 = 'NVOC_RESULT_CNT';
                datakey2 = 'NVOC_GL';
            }
            else {
                datasetNm = '실적';
                datakey1 = 'ASNO_RESULT_CNT';
                datakey2 = 'ASNO_GL';
            }

            //차트 데이터 세팅 입력 "bar", "line"
            fn_ChartDataSet(datasetNm, 'bar', evalDate, datakey1, evalDate);

            datasetNm = 'GL';
            //fn_ChartDataSet(datasetNm, 'line', evalDate, datakey2, evalDate);


            //선형 차트채우기 false
            for (var i = 0; i < chartConfig.data.datasets.length; i++) {
                chartConfig.data.datasets[i].fill = false;
                //chartConfig.data.datasets[i].borderColor = dataSetBdColor;
                //chartConfig.data.datasets[i].backgroundColor = dataSetBgColor;
            }

            //바형 두께
            chartConfig.options.maxBarThickness = 100;

            //데이터표시 false
            chartConfig.options.plugins.datalabels.display = false;

            //차트x축 false
            chartConfig.options.scales.x.grid.display = false;

            //라벨 플로그인 추가
            chartPlugins.push(ChartDataLabels);
            chartPlugins.push(ChartZoom);

            chartObjNm = 'ktsChart'; //차트 div id 지정
            chartParentObjNm = 'chartContainer'; //차트 div 부모 class 지정

            //차트크기
            setChartWidthHeight(true, 8);

            ctx = document.getElementById(chartObjNm).getContext('2d');

            //차트 그리기
            chartObj = new Chart(ctx, chartConfig);

            //차트y축
            setChartAxesY('y');

        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Team = function () {
            callGb = 'selTeam';

            //입력 파라미터
            var aData = [];

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUserOrg');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DDL';
            aData[1] = '';
            aData[2] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var gridColShow = function () {
            ktsGrid.hideColumn("CSI_GL");
            ktsGrid.hideColumn("CSI_CHECK_CNT");
            ktsGrid.hideColumn("CSI_TOT_POINT");
            ktsGrid.hideColumn("CSI_RESULT_CNT");
            ktsGrid.hideColumn("CSI_REACH_RATE");
            ktsGrid.hideColumn("NVOC_GL");
            ktsGrid.hideColumn("NVOC_OCCUR_CNT");
            ktsGrid.hideColumn("NVOC_RESULT_CNT");
            ktsGrid.hideColumn("NVOC_REACH_RATE");
            ktsGrid.hideColumn("ASNO_GL");
            ktsGrid.hideColumn("ASNO_CNT");
            ktsGrid.hideColumn("ASNO_TOT_CNT");
            ktsGrid.hideColumn("ASNO_RESULT_CNT");
            ktsGrid.hideColumn("ASNO_REACH_RATE");

            if ($('#selModule').val() == '1') {
                ktsGrid.showColumn("CSI_GL");
                ktsGrid.showColumn("CSI_CHECK_CNT");
                ktsGrid.showColumn("CSI_TOT_POINT");
                ktsGrid.showColumn("CSI_RESULT_CNT");
                ktsGrid.showColumn("CSI_REACH_RATE");

            }
            else if ($('#selModule').val() == '2') {
                ktsGrid.showColumn("NVOC_GL");
                ktsGrid.showColumn("NVOC_OCCUR_CNT");
                ktsGrid.showColumn("NVOC_RESULT_CNT");
                ktsGrid.showColumn("NVOC_REACH_RATE");
            }
            else {
                ktsGrid.showColumn("ASNO_GL");
                ktsGrid.showColumn("ASNO_CNT");
                ktsGrid.showColumn("ASNO_TOT_CNT");
                ktsGrid.showColumn("ASNO_RESULT_CNT");
                ktsGrid.showColumn("ASNO_REACH_RATE");
            }
        }

        var fn_Search = function () {

            if (fn_SearchChk()) {

                var pDate = '';

                callGb = 'search';

                gridCellShow = true;

                pDate = $("#selMon").val();

                gridColShow();

                //입력 파라미터
                var aData = [];
                aData[0] = '';
                aData[1] = pDate;
                aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }

        }
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <input type="hidden" id="hdnBaseDt" runat="server" />
                <span class="inpbox">
                    <label>월</label>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <span class="optionbox">
                                <select id="selTeam" style="width:160px;display:none"></select>
                            </span>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCdFix" runat="server"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <span class="optionbox" style="margin-right:20px">
                    <label>지표</label>
                    <select id="selModule">
                    </select>
                </span>
                 <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:chartWrapper -->
        <div class="chartWrapper">
            <div class="chartCombobox" style="display: none;">
                <span class="chartTitle"></span>
            </div>
            <div class="chartContainer">
            </div>
        </div>
        <!-- E:chartWrapper -->
        <div class="datalistmove">
            <label class="containMoveText"><span>차트</span>보기</label>
            <img class="containMove" src="/Resource/images/icon_arr_move_up.png" title="올리기" />
        </div>
        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
