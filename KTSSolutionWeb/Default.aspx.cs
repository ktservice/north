﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using Microsoft.AspNet.FriendlyUrls;

namespace KTSSolutionWeb
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string host = Request.Url.Host.ToLower();

            if (host.Equals("mbss.ktservice.co.kr"))
            {
                string url = "";
                url = "https";
                url += "://";
                url += host;
                url += "/MobileLogin";

                Session.Abandon();
                Response.Redirect(url);
            }

            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                GetNotiPopup();

                GetBoardType();
            }
        }

        #region GetNotiPopup
        /// <summary>
        /// GetNotiPopup
        /// </summary>
        private void GetNotiPopup()
        {
            DataSet ds = new DataSet();

            try
            {
                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetNotiPopUpList();
                }

                string strScript = "";

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string pid = ds.Tables[0].Rows[i]["NOTICEID"].ToString();
                    string strWriterId = ds.Tables[0].Rows[i]["WRITERID"].ToString();
                    string strPopupNm = "Notice_" + pid;

                    bool bPop = true;

                    HttpCookie Cookie = Request.Cookies[strPopupNm];

                    if (Cookie != null)
                    {
                        if (Cookie["POPUPYN"] == "Y" && Cookie["DATE"].ToString().Equals(DateTime.Now.ToString("yyyyMMdd")))
                        {
                            bPop = false;
                        }
                    }

                    if (bPop)
                    {
                        strScript += "NoticeView('" + pid + "', '" + strWriterId + "');";
                    }
                }

                if (strScript.Length > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "NoticePopup", strScript, true);
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region GetBoardType
        /// <summary>
        /// GetBoardType
        /// </summary>
        private void GetBoardType()
        {
            try
            {
                DataSet ds = new DataSet();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "BOARDTYPELIST";
                string jobGb = "GET";

                List<string> aData = new List<string>();
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                hdfBoardType.Value = dt.Rows[i]["BOARDTYPE"].ToString();
                            }

                            string strInner = "<li><a id=\"tab" + dt.Rows[i]["BOARDTYPE"].ToString() + "\" href=\"#tab" + dt.Rows[i]["BOARDTYPE"].ToString() + "\">" + dt.Rows[i]["CATEGORY"].ToString() + "</a></li>";
                            tabnav.InnerHtml += strInner;
                        }
                    }
                }
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PageError", "alert('페이지 로드 중 오류가 발생했습니다.');window.close();", true);
            }
        }
        #endregion

        #region GetDataList_Noti
        /// <summary>
        /// GetDataList_Noti
        /// </summary>
        private void GetDataList_Noti()
        {
            DataSet ds = new DataSet();

            try
            {
                pagingNoti.bPreRender = true;

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetNotiList("", "", "", "");
                }

                pagingNoti.PageNumber = 0;
                pagingNoti.PageSize = 6;

                pagingNoti.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    pagingNoti.TotalRows = ds.Tables[0].Rows.Count;
                    pagingNoti.Dt = ds.Tables[0];
                }

                pagingNoti.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region GetDataList_Board
        /// <summary>
        /// GetDataList_Board
        /// </summary>
        private void GetDataList_Board()
        {
            DataSet ds = new DataSet();

            try
            {
                pagingBoard.bPreRender = true;

                string strBoardType = hdfBoardType.Value;

                using (Notice Noti = new Notice())
                {
                    ds = Noti.GetBoardList("", strBoardType, "", "");
                }

                pagingBoard.PageNumber = 0;
                pagingBoard.PageSize = 6;

                pagingBoard.Dt = null;

                if (ds.Tables.Count > 0)
                {
                    pagingBoard.TotalRows = ds.Tables[0].Rows.Count;
                    pagingBoard.Dt = ds.Tables[0];
                }

                pagingBoard.SetPagingDataList();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #region pagingNoti_PreRender
        /// <summary>
        /// pagingNoti_PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pagingNoti_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (pagingNoti.bPreRender)
                {
                    if (pagingNoti.Dt != null)
                    {
                        pagingNoti.RepeaterDataBind(rptResultNoti);

                        for (int i = 0; i < rptResultNoti.Items.Count; i++)
                        {
                            Label lblNoticeID = (Label)rptResultNoti.Items[i].FindControl("lblNoticeID");
                            Label lblWriterID = (Label)rptResultNoti.Items[i].FindControl("lblWriterID");
                            HtmlAnchor aTitle = (HtmlAnchor)rptResultNoti.Items[i].FindControl("aTitle");

                            aTitle.Attributes.Add("onclick", "NoticeView('" + lblNoticeID.Text + "', '" + lblWriterID.Text + "')");
                        }
                    }

                    pagingNoti.bPreRender = false;
                }

                updPanelNoti.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region pagingBoard_PreRender
        /// <summary>
        /// pagingBoard_PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pagingBoard_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (pagingBoard.bPreRender)
                {
                    if (pagingBoard.Dt != null)
                    {
                        pagingBoard.RepeaterDataBind(rptResultBoard);

                        for (int i = 0; i < rptResultBoard.Items.Count; i++)
                        {
                            Label lblBoardID = (Label)rptResultBoard.Items[i].FindControl("lblBoardID");
                            Label lblWriterID = (Label)rptResultBoard.Items[i].FindControl("lblWriterID");
                            HtmlAnchor aTitle = (HtmlAnchor)rptResultBoard.Items[i].FindControl("aTitle");

                            aTitle.Attributes.Add("onclick", "BoardView('" + lblBoardID.Text + "', '" + lblWriterID.Text + "')");
                        }
                    }
                    pagingBoard.bPreRender = false;
                }

                updPanelBoard.Update();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }

        }
        #endregion

        #region btnSelectNoti_Click
        /// <summary>
        /// btnSelectNoti_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSelectNoti_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList_Noti();

                GetDataList_Board();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        #region btnSelectBoard_Click
        /// <summary>
        /// btnSelectBoard_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSelectBoard_Click(object sender, EventArgs e)
        {
            try
            {
                GetDataList_Noti();

                GetDataList_Board();
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion
    }
}