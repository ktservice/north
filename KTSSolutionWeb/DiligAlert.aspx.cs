﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

namespace KTSSolutionWeb
{
    public partial class DiligAlert : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void chkPopYn_CheckedChanged(object sender, EventArgs e)
        {
            string strEmpNo = Page.Session["EMPNO"].ToString();
            string strCookie = "DiligAlert_" + strEmpNo;

            if (chkPopYn.Checked)
            {
                HttpCookie Cookie = null;

                if (Request.Cookies[strCookie] == null)
                {
                    Cookie = new HttpCookie(strCookie);
                }
                else
                {
                    Cookie = Request.Cookies[strCookie];
                }

                Cookie["POPUPYN"] = "Y";
                Cookie["DATE"] = DateTime.Now.ToString("yyyyMMdd");
                Cookie.Expires = DateTime.Now.AddDays(1);
                Cookie.HttpOnly = true;
                Response.Cookies.Add(Cookie);
            }
            else
            {
                if (Request.Cookies[strCookie] != null)
                {
                    Request.Cookies[strCookie].Value = null;
                    Request.Cookies[strCookie].Expires = new DateTime(1999, 1, 1);
                    Request.Cookies.Remove(strCookie);
                }
            }

            updPanelChk.Update();
        }
    }
}