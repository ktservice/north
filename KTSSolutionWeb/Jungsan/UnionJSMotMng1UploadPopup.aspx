﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnionJSMotMng1UploadPopup.aspx.cs" Inherits="KTSSolutionWeb.UnionJSMotMng1UploadPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>MOT기타상품관리일괄등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-error {
          border: 4px solid #D00;
          color: #590000;
        }

        .btn-red {
            display: inline-block;
            min-width: 80px;
            height: 34px;
            padding: 0 15px;
            background: #ed1c24;
            text-align: center;
            line-height: 34px;
            border-radius: 2px;
            color: #fff;
            font-size: 14px;
            font-weight: 800;
            border-style: none;
        }

        #progressBarSet {
            padding:25px;
        }
        
        #progressBarContainer {
            position: fixed;
            display: flex;
            z-index: 99;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: #f9f9f9 !important;
            opacity: 0.8;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        #progressText {
            margin-top: 10px;
            font-weight: bold;
            text-align: center;
            color: #9c340a;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var moncd;
            var rm;

            var page = {
                init: function () {
                    fn_Init();
                },
                eventbind: function () {
                    $("#btnNewTemp").click(function (e) {
                        fn_ExcelSearch('newExcel');
                    });

                    $("#btnNotTemp").click(function (e) {
                        fn_NotExcelDown();
                    });

                    $("#btnDel").click(function (e) {
                        fn_Del();
                        
                    });

                    $("#btnClose").click(function (e) {
                        SendParentsForm();
                    });
                }
            };

        });

        var fn_Init = function () {
            today = new Date();
            moncd = $("#<%= txtMon.ClientID %>").val();

            rm = $("#<%= hdnRm.ClientID %>").val();

            ktsGridId = 'windowpop-wrap';

            fn_AuthGroupList();
        }

        var fn_NotExcelDown = function () {
            var msg = '';

            moncd = $("#<%= txtMon.ClientID %>").val();
            rm = $("#<%= hdnRm.ClientID %>").val();

            if (rm === "Y") {
                msg += $("#hdnAuthNm").val() + ' 권한입니다.\n';
                msg += moncd + '월 전체를 내려받습니다.\n';
                msg += $("#hdnAuthNm").val() + ' 권한 이외는 등록하신분 자료만 내려받습니다.\n';
            }

            msg += '진행하시겠습니까?';

            if (confirm(msg)) {
                fn_Hist();
            }
        }

        var fn_AuthGroupList = function () {
            callGb = 'authGroupList';

            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hdnAuthId.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetAuthGroupList');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Del = function () {
            var target = 'MOT1';
            var msg = '';

            if (rm === "Y") {
                msg += '시스템관리 권한입니다.\n';
                msg += moncd + '월 전체 Data를 삭제합니다.\n';
                msg += '시스템관리 권한 이외는 등록하신분 자료만 삭제합니다.\n';
            }

            msg += '진행하시겠습니까?';

            if (confirm(msg)) {
                callGb = 'delTran';
                fn_Transaction(target, moncd);
            }
        }

        var fn_Transaction = function (target, moncd) {

            //입력 파라미터
            var aData = [];
            aData[0] = target;
            aData[1] = moncd;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ExcelSearch = function (gubun) {
            callGb = gubun;
            var pDate = moncd;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pDate;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Hist = function () {
            callGb = 'notHIST';
            var gubun = 'RESULT';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = moncd;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetHistData');

            ajaxCallEx(jsonData, ajaxUrl, true);

        }

        var fn_excelDown = function (gubun, json, sheetNm1, sheetNm2, fileNm, type) {
            var headers;

            // Custom headers mapping
            if (gubun === 'not') {
                headers = ['사원번호', '실판매자', '정산총액', '기본장려금', 'MOT 실적급', '기타추가', '비고(적용정책)', '정산코드', '상품코드', '접수일', '상품명/지급명', '서비스번호', '고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', '계약번호(SAID)', '접수자명', '담당자', 'KT제공사은품', '상품명 상세내역', '약정', '오류메시지', '발생일'];
            }
            else {
                headers = ['사원번호', '실판매자', '정산총액', '기본장려금', 'MOT 실적급', '기타추가', '비고(적용정책)', '정산코드', '상품코드', '접수일', '상품명/지급명', '서비스번호', '고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', '계약번호(SAID)', '접수자명', '담당자', 'KT제공사은품', '상품명 상세내역', '약정'];
            }

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            if (gubun === 'not') {
                data = jsonData.map(item => [item.KTSEMPNO, item.EMPNM, item.TOT_JS_AMT, item.BASE_INCENTIVE_AMT, item.MOT_PERFOR_AMT, item.ETC_ADD_AMT, item.POLICYNM, item.JSCD, item.PRODUCTCD, item.RECEIPTDT, item.PRODUCTNM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTDESC, item.PRODUCT_PERIOD, item.ERRMSG, item.DBCREATEDT]);
            }
            else {
                data = jsonData.map(item => [item.KTSEMPNO, item.EMPNM, item.TOT_JS_AMT, item.BASE_INCENTIVE_AMT, item.MOT_PERFOR_AMT, item.ETC_ADD_AMT, item.POLICYNM, item.JSCD, item.PRODUCTCD, item.RECEIPTDT, item.PRODUCTNM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTDESC, item.PRODUCT_PERIOD]);
            }

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws1 = XLSX.utils.aoa_to_sheet(data);

            if (gubun === 'not') {
                // Add styles (example: set column width)
                ws1['!cols'] = [
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 22.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 20.89 },
                    { wch: 14.89 },
                    { wch: 16.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 28.89 },
                    { wch: 22.89 },
                    { wch: 12.89 },
                    { wch: 32.89 },
                    { wch: 18.89 }
                ];

            }
            else {
                // Add styles (example: set column width)
                ws1['!cols'] = [
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 22.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 20.89 },
                    { wch: 14.89 },
                    { wch: 16.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 28.89 },
                    { wch: 22.89 },
                    { wch: 12.89 }
                ];
            }

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            for (var C = 0; C < headers.length; ++C) {
                var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                if (!ws1[cell_address]) continue;
                ws1[cell_address].s = headerStyle;
            }

            //0~6  7~20   21_ 26
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws1['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws1[cell_ref]) continue;

                    //if (R > 1)
                    if (R > 0) {
                        if (C >= 2 && C <= 5) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 24) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();

            if (gubun === 'not') {
                XLSX.utils.book_append_sheet(wb, ws1, sheetNm1);
                XLSX.utils.book_append_sheet(wb, ws2, sheetNm2);

                ws2 = null;
            }
            else {
                XLSX.utils.book_append_sheet(wb, ws1, sheetNm1);
            }

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

        }

        var fn_excelSheet2 = function (json) {
            var headers;

            headers = ['업로드월', '업로드사번', '업로드이름', '업로드명', '업로드일', '결과', '총건수', '오류메시지'];

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.UPLOAD_MON, item.EMPNO, item.EMPNM, item.UPLOADNM, item.UPLOADDT, item.RESULT, item.TOT_CNT, item.MESSAGE]);

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 19.89 },
                { wch: 18.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 32.89 }
            ];

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            for (var C = 0; C < headers.length; ++C) {
                var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                if (!ws[cell_address]) continue;
                ws[cell_address].s = headerStyle;
            }

            //6
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    //if (R > 1)
                    if (R > 0) {
                        if (C == 6) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            return ws;

        }


        var excelTitle = function (pTitle) {
            var title = '';
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2) +
                ('0' + today.getDate()).slice(-2);

            title += formattedDate + '_';
            title += pTitle;

            return title;
        }

        var ws2;
        var ajaxCallBack = function (json) {
            var errCd = '';
            var errMsg = '';

            if (callGb == 'newExcel') {
                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                }
                else {
                    var title = excelTitle('통합정산 MOT기타상품관리1 신규양식 일괄등록 Template');
                    fn_excelDown('new', json, 'Sheet1', '', title, 'xlsx');
                }
            }
            else if (callGb == 'notExcel') {
                callGb = '';

                var SheetNm1 = '';
                var SheetNm2 = '';

                SheetNm1 = 'DB검증 오류(' + moncd.replace(/-/gi, "") + ')';
                SheetNm2 = '응용프로그램 오류(' + moncd.replace(/-/gi, "") + ')';

                var title = excelTitle('통합정산 MOT기타상품관리1 엑셀오류확인');
                fn_excelDown('not', json, SheetNm1, SheetNm2, title, 'xlsx');
            }
            else if (callGb == 'notHIST') {

                callGb = '';

                fn_ExcelSearch('notExcel');
                ws2 = fn_excelSheet2(json);
            }
            else if (callGb == 'delTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('삭제되었습니다.');
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'authGroupList') {
                $.each(JSON.parse(json), function (idx, item) {
                    $("#hdnAuthNm").val(item.AUTHNM);
                });
            }
        }

        function btnUploadChk() {
            if (confirm("등록 하시겠습니까?")) {
                startProgress();

                return true;
            }
            else {
                return false;
            }
        }

        function endProgress(msg) {
            $("#progressBarSet").hide();

            if (msg != '') {
                alert(msg);
            }
        }

        function startProgress() {
            $("#progressBarSet").show();
            $("#progressText").html("등록 건수가 많으면 5분이상 소요됩니다.<br>닫지마시고<br>잠시만 기다려주세요.");
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <div id="msg" class="tabulator"></div>
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">통합정산 일괄등록</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />
            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <!-- S:list-top -->
                        <div class="list-top">
                            <label class="searchrowcount"></label>
                            <span class="inpbox" style="margin-left: 32px">
                                <label>정산월</label>
                                <span class="inpbox">
                                    <asp:TextBox ID="txtMon" runat="server" ReadOnly="true" style="width:100px !important;"></asp:TextBox>
                                </span>
                            </span>
                            <div class="pull-right" style="margin-right: 30px">
                                <div class="btnset">
                                    <input id="btnDel" type="button" class="btn-red last" style="float: right; margin: 0px 10px" value="월Data삭제" />
                                    <input id="btnNotTemp" type="button" class="btn-green last" style="float: right; margin: 0px 10px" value="엑셀오류확인내려받기" />
                                    <input id="btnNewTemp" type="button" class="btn-green last" style="float: right; margin: 0px 10px" value="신규양식다운로드" />
                                </div>
                            </div>
                        </div>
                        <div class="upload-list" style="vertical-align:text-bottom;margin: 50px 0 0 0;">
                            <ul>
                                <li>
                                    <strong style="padding:10px 0px;float:left;margin-left:32px">파일</strong>
                                    <div class="file-field">
                                        <p class="filebox">
                                            <input type="text" id="txtfu" style="width:595px" />
                                            <span class="file">
                                                <input type="file" id="fu" name="fu" onchange="document.getElementById('txtfu').value=this.value;" /> 
                                                <label for="fu">찾아보기</label>
                                            </span>
                                            <asp:Button ID="btnUpload" runat="server" OnClientClick="return btnUploadChk();" OnClick="btnUpload_Click" style="float: right;margin: 0px 10px" class="btn-black" Text="자료등록" />
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="progressBarSet" style="display:none">
                            <div id="progressBarContainer">
                            <img src="../Resource/images/icon_loadingbar.gif" />
                            <h2>등록중...</h2>
                                <div id="progressText"></div>
                            </div>
                        </div>
                         </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUpload" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress runat="server" ID="updProgress1" AssociatedUpdatePanelID="updPanel1">
                            <ProgressTemplate>
                                <div>Loading...</div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin: 120px 30px 0 30px; display: grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                        <input id="hdnRm" type="hidden" runat="server" />
                        <input id="hdnAuthId" type="hidden" runat="server" />
                        <input id="hdnAuthNm" type="hidden" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>