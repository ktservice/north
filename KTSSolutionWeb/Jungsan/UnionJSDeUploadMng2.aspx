﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnionJSDeUploadMng2.aspx.cs" Inherits="KTSSolutionWeb.UnionJSDeUploadMng2" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .headerPay {
            background: #d7d4f073 !important;
        }

        .headerPolicy {
            background: #f0d4d473 !important;
        }
        
        .txtCell {
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
            text-align:right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        #progressBarContainer {
            position: fixed;
            display: flex;
            z-index: 99;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: #f9f9f9 !important;
            opacity: 0.8;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        #progressText {
            margin-top: 10px;
            font-weight: bold;
            text-align: center;
            color: #9c340a;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var closeDt1 = '';
        var closeDt2 = '';
        var dateNow;
        var today;
        var todayMon;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();

                        var etMonthValue = $('.etMonth').val();  // etMonth 텍스트박스 값 가져오기
                        var etParts = etMonthValue.split('-');   // etMonth 값을 '-'로 분할
                        var etYear = parseInt(etParts[0], 10);   // etMonth 연도 부분 정수로 변환
                        var etMonth = parseInt(etParts[1], 10);  // etMonth 월 부분 정수로 변환

                        if (etMonth === 1) {
                            etYear -= 1;   // etMonth의 월이 1이면 연도 1 감소
                            etMonth = 12;  // etMonth의 월을 12로 설정
                        } else {
                            etMonth -= 1;  // etMonth의 월을 1 감소
                        }

                        // 새로운 stMonth 값을 'YYYY-MM' 형식으로 설정
                        var newStMonthValue = etYear + '-' + (etMonth < 10 ? '0' + etMonth : etMonth);

                        $('.stMonth').val(newStMonthValue);  // stMonth 텍스트박스에 새로운 값 설정
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {
                    fn_Search('CHECK');
                });

                $("#btnExcel").click(function (e) {
                    fn_BatchExcelDown();
                });
                
                $("#gridExcel").click(function (e) {
                    fn_GridExcel('통합정산 대사우업로드2차관리', '통합정산 대사우업로드2차관리', 'xlsx');
                });

            }
        };

        var fn_Init = function () {
            today = new Date();
            dateNow = today.getDate();
            todayMon = today.getFullYear().toString() + ('0' + (today.getMonth() + 1)).slice(-2);


            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());


            $(".table-search").css('display', 'none');

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
            $("#tab02").hide();


            var formattedDate = today.getFullYear().toString() + '-' +
                ('0' + (today.getMonth() + 1)).slice(-2) + '월 ';

            $("#btnExcel").val(formattedDate + $("#btnExcel").val());

        }

        var fn_BatchExcelDown = function () {
            startProgress();
            fn_ExcelSearch('checkExcel');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "MDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원번호", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자(계좌주)", field: "EMPNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "right", title: "지급총액", field: "JS_TOT_RESULT_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_SUM_AMT", sorter: "number", width: 160, formatter: fn_CellNumber },
                {
                    title: '고성과인센티브', headerHozAlign: "center",
                    columns: [
                        
                        { headerHozAlign: "center", hozAlign: "right", title: "Pnt", field: "PNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "인센티브", field: "INCENTIVE", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "Pnt_B", field: "PNT_B", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "인센티브_B", field: "INCENTIVE_B", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "Pnt_C", field: "PNT_C", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "인센티브_C", field: "INCENTIVE_C", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '홈기본상품', headerHozAlign: "center",
                    columns: [
                        {
                            title: '인터넷', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "INTERNET_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "INTERNET_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                            ]
                        },
                        {
                            title: 'TV', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "TV_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "TV_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                            ]
                        },
                        {
                            title: '음성', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "VOICE_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "VOICE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                            ]
                        },
                    ]
                },
                {
                    title: '홈부가상품', headerHozAlign: "center",
                    columns: [
                        {
                            title: '기가전환', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "CHANGE_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "CHANGE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                            ]
                        },
                        {
                            title: '재약정', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "UPDATE_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "UPDATE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                            ]
                        },
                        {
                            title: '기타상품', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "ETC_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "ETC_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                            ]
                        },
                    ]
                },
                {
                    title: '무선상품', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "WIRELESS_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "WIRELESS_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '유지수수료', headerHozAlign: "center",
                    columns: [
                        {
                            title: '50%지급', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "MOTETC_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "MOTETC_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                            ]
                        },
                    ]
                },
                {
                    title: '재게', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "실적급총액", field: "PERFOR_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "홈실적급", field: "HOME_PERFOR_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '추가분', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "SM실적급", field: "SM_PERFOR_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber },
                    ]
                },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

    
        var fn_Load = function () {
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pType: "jungsan"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function () {
            return true;
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var excelTitle = function (pTitle) {
            var title = '';
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2) +
                ('0' + today.getDate()).slice(-2);

            title += formattedDate + '_';
            title += pTitle;

            return title;
        }

        function getLastDayOfMonth(year, month) {
            return new Date(year, month, 0).getDate();
        }

        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });

            }
            else if (callGb == 'checkExcel') {
                callGb = '';

                var SheetNm1 = '';
                var SheetNm2 = '';

                SheetNm1 = '① 홈기본상품';
                SheetNm2 = '② 홈부가상품';
                SheetNm3 = '③ 무선상품';
                SheetNm4 = '④ 유지수수료';
                SheetNm5 = '⑤ 추가인센티브';

                var title = excelTitle('통합정산 대사우업로드2차');
                fn_excelDown('', json, SheetNm1, SheetNm2, SheetNm3, SheetNm4, SheetNm5, title, 'xlsx');
            }

        }

        var fn_GridExcel = function (sheetNm, fileNm, type) {
            var headers;

            // Custom headers mapping   
            headers = [
                ['No.', '본부', '지사', '지점', '직무', '사원번호', 'KTS사번', '판매자\n(계좌주)', '지급총액', '정산총액', '고성과인센티브', '고성과인센티브', '고성과인센티브', '고성과인센티브', '고성과인센티브', '고성과인센티브', '홈기본상품', '홈기본상품', '홈기본상품', '홈기본상품', '홈기본상품', '홈기본상품', '홈부가상품', '홈부가상품', '홈부가상품', '홈부가상품', '홈부가상품', '홈부가상품', '무선상품', '무선상품', '유지수수료', '유지수수료', '재게', '재게', '추가분'],
                ['', '', '', '', '', '', '', '', '', '', 'Pnt', '인센티브', 'Pnt_B', '인센티브_B', 'Pnt_C', '인센티브_C', '인터넷', '인터넷', 'TV', 'TV', '음성', '음성', '기가전환', '기가전환', 'GiGA_WiFi', 'GiGA_WiFi', '기타상품', '기타상품', '', '', '50%지급', '50%지급', '실적급총액', '홈실적급', 'SM실적급']
            ];

            var data = ktsGrid.getData();

            // 그리드 데이터 변환
            var excelData = [
                headers[0],  // First row of headers
                headers[1], // Second row of headers
                ...data.map(row => [row.id, row.ORGNM1, row.ORGNM2, row.ORGNM3, row.JOBJM, row.EMPNO, row.KTSEMPNO, row.EMPNM, row.JS_TOT_RESULT_AMT, row.TOT_SUM_AMT, row.PNT, row.INCENTIVE, row.PNT_B, row.INCENTIVE_B, row.PNT_C, row.INCENTIVE_C, row.INTERNET_CNT, row.INTERNET_AMT, row.TV_CNT, row.TV_AMT, row.VOICE_CNT, row.VOICE_AMT, row.CHANGE_CNT, row.CHANGE_AMT, row.UPDATE_CNT, row.UPDATE_AMT, row.ETC_CNT, row.ETC_AMT, row.WIRELESS_CNT, row.WIRELESS_AMT, row.MOTETC_CNT, row.MOTETC_AMT, row.PERFOR_AMT, row.HOME_PERFOR_AMT, row.SM_PERFOR_AMT])  // 나머지 데이터
            ];

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(excelData);

            ws['!merges'] = [
                { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "No"
                { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "본부"
                { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } }, // Merge "지사"
                { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } }, // Merge "지점"
                { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } }, // Merge "직무"
                { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } }, // Merge "사원번호"
                { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } }, // Merge "KTS사번"
                { s: { r: 0, c: 7 }, e: { r: 1, c: 7 } }, // Merge "판매자"
                { s: { r: 0, c: 8 }, e: { r: 1, c: 8 } }, // Merge "지급총액"
                { s: { r: 0, c: 9 }, e: { r: 1, c: 9 } }, // Merge "정산총액"
                { s: { r: 0, c: 10 }, e: { r: 0, c: 15 } }, // Merge "고성과인센티브"
                { s: { r: 0, c: 16 }, e: { r: 0, c: 21 } }, // Merge "홈기본상품"
                { s: { r: 1, c: 16 }, e: { r: 1, c: 17 } }, // Merge "인터넷"
                { s: { r: 1, c: 18 }, e: { r: 1, c: 19 } }, // Merge "TV"
                { s: { r: 1, c: 20 }, e: { r: 1, c: 21 } }, // Merge "음성"
                { s: { r: 0, c: 22 }, e: { r: 0, c: 27 } }, // Merge "홈부가상품"
                { s: { r: 1, c: 22 }, e: { r: 1, c: 23 } }, // Merge "기가전환"
                { s: { r: 1, c: 24 }, e: { r: 1, c: 25 } }, // Merge "GiGA_WiFi"
                { s: { r: 1, c: 26 }, e: { r: 1, c: 27 } }, // Merge "기타상품"
                { s: { r: 0, c: 28 }, e: { r: 1, c: 29 } }, // Merge "무선상품"
                { s: { r: 0, c: 30 }, e: { r: 0, c: 31 } }, // Merge "유지수수료"
                { s: { r: 1, c: 30 }, e: { r: 1, c: 31 } }, // Merge "50%지급"
                { s: { r: 0, c: 32 }, e: { r: 0, c: 33 } } // Merge "재게"
            ];

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 10.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];

            ws = fn_ExcelHeaderStyle(2, ws, headers);

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 1) {
                        if (C == 8 || C == 9 || C == 11 || C == 13 || C == 15 || C == 17 || C == 19 || C == 21 || C == 23 || C == 25 || C == 27 || C == 29 || (C >= 31 && C <= 34)) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 10 || C == 12 || C == 14 || C == 16 || C == 18 || C == 20 || C == 22 || C == 24 || C == 26 || C == 28 || C == 30) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            // 엑셀 파일 생성 및 다운로드
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);
            XLSX.writeFile(wb, fileNm + '.' + type);
        }

        var fn_excelDown = function (gubun, json, sheetNm1, sheetNm2, sheetNm3, sheetNm4, sheetNm5, fileNm, type) {
            var headers;
            var data1;
            var data2;
            var data3;
            var data4;
            var data5;
            var ws1;
            var ws2;
            var ws3;
            var ws4;
            var ws5;

            headers = ['참조KEY', '본부', '지사', '지점', '사원구분', '사원번호', '실판매자', 'MOT Point', '정산총액', '기본정책', 'MOT실적급', 'APT5만\nOff_IP팩3만\n대학전략동', 'IF단지4만\nBulk활성화\n법인Bulk', '결합', 'SM패스\n이미징\n차감', '사은품초과등\n기타차감', '기본장려금\n기지급', '지급/미지급사유', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국\n(회선수)', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간'];

            var jsonData = JSON.parse(json);

            // Convert JSON data to array of arrays
            data1 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

            // Add headers as the first row
            data1.unshift(headers);

            // Convert array of arrays to worksheet
            ws1 = XLSX.utils.aoa_to_sheet(data1);

            // Add styles (example: set column width)
            ws1['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 26.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 14.89 },
                { wch: 18.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 26.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            ws1 = fn_ExcelHeaderStyle(1, ws1, headers);

            headers = ['참조KEY', '본부', '지사', '지점', '사원구분', '사원번호', '실판매자', 'MOT\npoint', '정산총액', '기본장려금', '프로모션\n(기전특별)', 'SM패스\n이미징\n차감', 'MOT실적급', '기지급\n장려금', '비  고', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간'];
            data2 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
            data2.unshift(headers);

            ws2 = XLSX.utils.aoa_to_sheet(data2);

            ws2['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            ws2 = fn_ExcelHeaderStyle(1, ws2, headers);

            headers = ['참조KEY', '본부', '지사', '지점', '직원구분', '사원번호', '실판매자', '지급액', '획득\nPoint', 'MOT실적급', '기본정책', '무선MOT\n인센티브\n(패드제외)', '추가장려금', 'ARPU UP\n주력', '특화모델', '주말MNP', '주말MNP\n추가', 'LG판매활성화1', 'LG판매활성화2', 'LG판매활성화3', 'LG판매활성화4', '재고소진', '전월무선결합\n수수료', '무선차감', '특이사항', '상품코드', '개통일', '가입자명', '서비스번호', '모델명', '가입유형', '요금제명', '인터넷계약ID', '유선고객명', '결합유형', '결합구성', '인터넷현상품', '인터넷접수일'];
            data3 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
            data3.unshift(headers);
            ws3 = XLSX.utils.aoa_to_sheet(data3);

            ws3['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];


            ws3 = fn_ExcelHeaderStyle(1, ws3, headers);

            headers = ['참조KEY', '본부', '지사', '지점', '사원구분', '사원번호', '실판매자', '지급액\n(유지수료*50%)', '유지수수료\n(수납액*지급율)', '지급율', '당월\n사용액', '수납액', '수납월', '비고', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국\n(회선수)', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간'];
            data4 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
            data4.unshift(headers);
            ws4 = XLSX.utils.aoa_to_sheet(data4);

            ws4['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 18.89 },
                { wch: 18.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            ws4 = fn_ExcelHeaderStyle(1, ws4, headers);

            headers = [
                ['지사지점', '사원번호', '판매자\n(계좌주)', '지급총액', '정산총액', '고성과인센티브', '', '', '', '', '', '홈기본상품', '', '', '', '', '', '홈부가상품', '', '', '', '', '', '무선상품', '', '유지수수료', '유지수수료', '재게', '', '추가분'],
                ['', '', '', '', '', 'Pnt', '인센티브', 'Pnt_B', '인센티브_B', 'Pnt_C', '인센티브_C', '인터넷', '인터넷', 'TV', 'TV', '음성', '음성', '기가전환', '기가전환', 'GiGA_WiFi', 'GiGA_WiFi', '기타상품', '기타상품', '', '', '50%지급', '50%지급', '실적급총액', '홈실적급', 'SM실적급']
            ];

            data5 = jsonData.map(item => [
                item.ORGNM3 || null,
                item.KTSEMPNO || null,
                item.EMPNM || null,
                item.JS_TOT_RESULT_AMT || null,
                item.TOT_SUM_AMT || null,
                item.PNT || null,
                item.INCENTIVE || null,
                item.PNT_B || null,
                item.INCENTIVE_B || null,
                item.PNT_C || null,
                item.INCENTIVE_C || null,
                item.INTERNET_CNT || null,
                item.INTERNET_AMT || null,
                item.TV_CNT || null,
                item.TV_AMT || null,
                item.VOICE_CNT || null,
                item.VOICE_AMT || null,
                item.CHANGE_CNT || null,
                item.CHANGE_AMT || null,
                item.UPDATE_CNT || null,
                item.UPDATE_AMT || null,
                item.ETC_CNT || null,
                item.ETC_AMT || null,
                item.WIRELESS_CNT || null,
                item.WIRELESS_AMT || null,
                item.MOTETC_CNT || null,
                item.MOTETC_AMT || null,
                item.PERFOR_AMT || null,
                item.HOME_PERFOR_AMT || null,
                item.SM_PERFOR_AMT || null
            ]);

            data5.unshift(headers[1]); // Add the second row of headers
            data5.unshift(headers[0]); // Add the first row of headers

            ws5 = XLSX.utils.aoa_to_sheet(data5);

            /* ws5['!merges'] = [
                { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "본부"
                { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "지사"
                { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } }, // Merge "지점"
                { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } }, // Merge "사원번호"
                { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } }, // Merge "판매자"
                { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } }, // Merge "지급총액"
                { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } }, // Merge "정산총액"
                { s: { r: 0, c: 7 }, e: { r: 0, c: 12 } }, // Merge "고성과인센티브"
                { s: { r: 0, c: 13 }, e: { r: 0, c: 18 } }, // Merge "홈기본상품"
                { s: { r: 1, c: 13 }, e: { r: 1, c: 14 } }, // Merge "인터넷"
                { s: { r: 1, c: 15 }, e: { r: 1, c: 16 } }, // Merge "TV"
                { s: { r: 1, c: 17 }, e: { r: 1, c: 18 } }, // Merge "음성"
                { s: { r: 0, c: 19 }, e: { r: 0, c: 24 } }, // Merge "홈부가상품"
                { s: { r: 1, c: 19 }, e: { r: 1, c: 20 } }, // Merge "기가전환"
                { s: { r: 1, c: 21 }, e: { r: 1, c: 22 } }, // Merge "GiGA_WiFi"
                { s: { r: 1, c: 23 }, e: { r: 1, c: 24 } }, // Merge "기타상품"
                { s: { r: 0, c: 25 }, e: { r: 1, c: 26 } }, // Merge "무선상품"
                { s: { r: 0, c: 27 }, e: { r: 0, c: 28 } }, // Merge "유지수수료"
                { s: { r: 1, c: 27 }, e: { r: 1, c: 28 } }, // Merge "50%지급"
                { s: { r: 0, c: 29 }, e: { r: 0, c: 30 } } // Merge "재게"
            ];
            */

            const mergeRanges = [
                { start: [0, 0], end: [1, 0] }, // "지사지점"
                { start: [0, 1], end: [1, 1] }, // "사원번호"
                { start: [0, 2], end: [1, 2] }, // "판매자"
                { start: [0, 3], end: [1, 3] }, // "지급총액"
                { start: [0, 4], end: [1, 4] }, // "정산총액"
                { start: [0, 5], end: [0, 10] }, // "고성과인센티브"
                { start: [0, 11], end: [0, 16] }, // "홈기본상품"
                { start: [1, 11], end: [1, 12] }, // "인터넷"
                { start: [1, 13], end: [1, 14] }, // "TV"
                { start: [1, 15], end: [1, 16] }, // "음성"
                { start: [0, 17], end: [0, 22] }, // "홈부가상품"
                { start: [1, 17], end: [1, 18] }, // "기가전환"
                { start: [1, 19], end: [1, 20] }, // "GiGA_WiFi"
                { start: [1, 21], end: [1, 22] }, // "기타상품"
                { start: [0, 23], end: [1, 24] }, // "무선상품"
                { start: [0, 25], end: [0, 26] }, // "유지수수료"
                { start: [1, 25], end: [1, 26] }, // "50%지급"
                { start: [0, 27], end: [0, 28] } // "재게"
            ];

            // 병합 범위를 엑셀 워크시트에 추가
            ws5['!merges'] = mergeRanges.map(range => ({
                s: { r: range.start[0], c: range.start[1] },
                e: { r: range.end[0], c: range.end[1] }
            }));

            // 병합 범위 내부 셀 비우기
            mergeRanges.forEach(range => {
                for (let r = range.start[0]; r <= range.end[0]; r++) {
                    for (let c = range.start[1]; c <= range.end[1]; c++) {
                        // 병합 범위의 첫 번째 셀은 유지
                        if (r === range.start[0] && c === range.start[1]) continue;

                        const cellAddress = XLSX.utils.encode_cell({ r, c });
                        delete ws5[cellAddress]; // 셀 삭제
                    }
                }
            });

            // Add styles (example: set column width)
            ws5['!cols'] = [
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];

            ws5 = fn_ExcelHeaderStyle(2, ws5, headers);


            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws5['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws5[cell_ref]) continue;

                    if (R > 1) {
                        if (C == 5 || C == 6 || C == 8 || C == 10 || C == 12 || C == 14 || C == 16 || C == 18 || C == 20 || C == 22 || C == 24 || C == 26 || (C >= 28 && C <= 31)) {
                            ws5[cell_ref].z = '0'; // 숫자 형식

                            ws5[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 7 || C == 9 || C == 11 || C == 13 || C == 15 || C == 17 || C == 19 || C == 21 || C == 23 || C == 25 || C == 27) {
                            ws5[cell_ref].z = '0'; // 숫자 형식

                            ws5[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws5[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws1, sheetNm1);
            XLSX.utils.book_append_sheet(wb, ws2, sheetNm2);
            XLSX.utils.book_append_sheet(wb, ws3, sheetNm3);
            XLSX.utils.book_append_sheet(wb, ws4, sheetNm4);
            XLSX.utils.book_append_sheet(wb, ws5, sheetNm5);

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

            endProgress('');
        }

        var fn_ExcelHeaderStyle = function (hRow, ws, headers)
        {
            // Style headers
            var headerStyle = {
                font: { name: '맑은 고딕', bold: true, sz: 10, color: { rgb: "FFFFFF" } },
                fill: { fgColor: { rgb: "3366cc" } },
                alignment: { horizontal: "center", vertical: "center", wrapText: true },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                },
            };

            if (hRow === 1) {
                // Apply styles to headers
                for (var C = 0; C < headers.length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }
            else {
                // Apply styles to headers
                for (var R = 0; R < headers.length; ++R) {
                    for (var C = 0; C < headers[R].length; ++C) {
                        var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                        if (!ws[cell_address]) continue;
                        ws[cell_address].s = headerStyle;
                    }
                }
            }

            return ws;
        }


        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DEUPLOADMNG2';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ExcelSearch = function (gubun) {
            callGb = gubun;
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2);

            var pDate = formattedDate;
            var pJsResult = '완료';

            if (gubun === 'checkExcel') {
                gubun = 'CHECK';
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pDate;
            aData[2] = pJsResult;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function (gubun) {
            if (fn_SearchChk()) {

                var pDate1 = $("#selMon").val().replace(/-/gi, "");
                var pJsResult = '완료';

                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = pDate1;
                aData[2] = pJsResult;

                var jsonData = JSON.stringify({ aData: aData });

                if (gubun === 'CHECK') {
                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                    fn_GridInit();
                    ktsRowCountClass = 'searchrowcount';

                }
                
                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
                $(".table-option").css('display', 'block');
                $(".table-search").css('display', 'block');
            }
        }

        function endProgress(msg) {
            $("#progressBarSet").hide();

            if (msg != '') {
                alert(msg);
            }
        }

        function startProgress() {
            $("#progressBarSet").show();
            $("#progressText").html("잠시만 기다려주세요.");
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->

	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>정산월</label>
                <select id="selDt" style="width:80px;display:none;">
                    <option selected="selected" value="m">정산월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnExcel" type="button" class="btn-black last" style="float: right;margin-right:30px" value="대사우업로드2차내려받기" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                    <input id="hdnRm" type="hidden" runat="server" />
                    <input id="hdnAuthId" type="hidden" runat="server" />
                    <input id="hdnAuthNm" type="hidden" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <div class="list-top">
                <strong>대사우업로드2차 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                    </div>
                </div>
            </div>
            <div class="scrollbox">
                <div class="table-search" style="display: none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                            <option value="EMPNM">판매자(계좌주)</option>
                        </select>
                        : 
                       
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
        </div>
        <!-- E:datalist -->
        <div id="progressBarSet" style="display:none">
            <div id="progressBarContainer">
            <img src="../Resource/images/icon_loadingbar.gif" />
            <h2>다운로드중...</h2>
                <div id="progressText"></div>
            </div>
        </div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>
