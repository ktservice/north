﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnionJSEmpSupportChkMng.aspx.cs" Inherits="KTSSolutionWeb.UnionJSEmpSupportChkMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .headerPay {
            background: #d7d4f073 !important;
        }

        .headerPolicy {
            background: #f0d4d473 !important;
        }
        
        .txtCell {
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
            text-align:right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        #divEnd{
            color: #be1a1a;
            display: flex;
            position: absolute;
            justify-content: flex-end;
            padding-right: 20px;
        }

        #progressBarSet {
            padding:25px;
        }
        
        #progressBarContainer {
            position: fixed;
            display: flex;
            z-index: 99;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: #f9f9f9 !important;
            opacity: 0.8;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        #progressText {
            margin-top: 10px;
            font-weight: bold;
            text-align: center;
            color: #9c340a;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var idChk = false;
        var dateNow;
        var today;
        var todayMon;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();

                        var etMonthValue = $('.etMonth').val();  // etMonth 텍스트박스 값 가져오기
                        var etParts = etMonthValue.split('-');   // etMonth 값을 '-'로 분할
                        var etYear = parseInt(etParts[0], 10);   // etMonth 연도 부분 정수로 변환
                        var etMonth = parseInt(etParts[1], 10);  // etMonth 월 부분 정수로 변환

                        if (etMonth === 1) {
                            etYear -= 1;   // etMonth의 월이 1이면 연도 1 감소
                            etMonth = 12;  // etMonth의 월을 12로 설정
                        } else {
                            etMonth -= 1;  // etMonth의 월을 1 감소
                        }

                        // 새로운 stMonth 값을 'YYYY-MM' 형식으로 설정
                        var newStMonthValue = etYear + '-' + (etMonth < 10 ? '0' + etMonth : etMonth);

                        $('.stMonth').val(newStMonthValue);  // stMonth 텍스트박스에 새로운 값 설정
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnSelect").click(function (e) {

                    if ($('.tabnav a.active').text() === '이력 조회') {
                        $('.tabnav a').eq(1).click();
                    }
                    else {
                        $('.tabnav a').eq(0).click();
                    }
                });

                $("#btnExcel").click(function (e) {
                    fn_BatchExcelDown();
                });
                
                $("#gridExcel").click(function (e) {
                    fn_GridExcel('통합정산 고객지원조사', '고객지원조사', 'xlsx');
                    //ktsGrid.download("xlsx", "통합정산 고객지원조사.xlsx", { sheetName: "통합정산 고객지원조사" });
                });

                $("#gridExcel2").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 고객지원조사 이력.xlsx", { sheetName: "이력" });
                });

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    fn_TabSelect(this.text);

                    return false;
                });
            }
        };

        var fn_Init = function () {
            today = new Date();
            dateNow = today.getDate();
            todayMon = today.getFullYear().toString() + ('0' + (today.getMonth() + 1)).slice(-2);


            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            idChk = checkId($("#<%=hfOrgCd.ClientID %>"));

            $(".table-search").css('display', 'none');
            $(".stMonth").show();
            $(".etMonth").show();
            $("#lblDt").show();

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
            $("#tab02").hide();


            var formattedDate = today.getFullYear().toString() + '-' +
                ('0' + (today.getMonth() + 1)).slice(-2) + '월 ';

            $("#btnExcel").val(formattedDate + $("#btnExcel").val());


            $("#divEnd").width($(".contentsarea").width());
            $("#divEnd").offset({ top: $(".last").offset().top });

        }

        var fn_TabSelect = function (txt) {
            if (txt === '고객지원조사 조회') {
                var msg = '';
                var rm = $("#<%= hdnRm.ClientID %>").val();

                if (rm === "Y") {
                    msg += $("#hdnAuthNm").val() + ' 권한입니다.\n';
                    msg += $("#hdnAuthNm").val() + ' 권한은 조직조건 포함입니다.\n';
                    msg += '이외는 등록하신분 자료만 조회됩니다.\n';
                    msg += '진행하시겠습니까?';

                    if (confirm(msg)) {
                        fn_Search('SEARCH');
                    }
                }
                else {
                    fn_Search('SEARCH');
                }
            }
            else {
                var msg = '';
                var rm = $("#<%= hdnRm.ClientID %>").val();

                if (rm === "Y") {
                    msg += $("#hdnAuthNm").val() + ' 권한입니다.\n';
                    msg += $("#hdnAuthNm").val() + ' 권한은 전체 자료가 조회됩니다.\n';
                    msg += '이외는 등록하신분 자료만 조회됩니다.\n';
                    msg += '진행하시겠습니까?';

                    if (confirm(msg)) {
                        fn_Hist();
                    }
                }
                else {
                    fn_Hist();
                }
            }
        }

        var fn_BatchExcelDown = function () {
            var msg = '';
            var rm = $("#<%= hdnRm.ClientID %>").val();

            if (rm === "Y") {
                msg += $("#hdnAuthNm").val() + ' 권한입니다.\n';
                msg += todayMon + '월 전체를 내려받습니다.\n';
                msg += $("#hdnAuthNm").val() + ' 권한 이외는 등록하신분 자료만 내려받습니다.\n';
            }

            msg += '진행하시겠습니까?';

            if (confirm(msg)) {
                startProgress();
                fn_ExcelSearch('modExcel');
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var jsMon = cell.getRow().getData().JS_MON;

            cellNo = cell.getRow().getData().id;
            jsMon = jsMon.replace(/-/g, "");

            if (todayMon == jsMon) {
                html += "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            }

            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수월", field: "RECEIPT_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원구분", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "right", title: "고객지원가능금액<br>(정산총액-MOT실적급)", field: "TOT_JS_AMT", sorter: "number", width: 180, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "본인지급분", field: "MYPAY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "송금 적용액", field: "SENDAPPLY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "고객선지급분<br>적용액", field: "PREPAY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                {
                    title: '선지급분', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "은행", field: "BANKCD1", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "은행명", field: "BANKNM1", sorter: "string", width: 120 },
                        { headerHozAlign: "center", hozAlign: "center", title: "계좌", field: "ACCOUNT_NUM1", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "예금주명", field: "DEPOSITOR1", sorter: "string", width: 150 },
                        { headerHozAlign: "center", hozAlign: "right", title: "고객선지급분", field: "CUST_PAY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "center", title: "관계", field: "RELATIONSHIP", sorter: "string", width: 100 },
                    ]
                },
                {
                    title: '후지급분', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "은행", field: "BANKCD2", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "은행명", field: "BANKNM2", sorter: "string", width: 120 },
                        { headerHozAlign: "center", hozAlign: "center", title: "계좌", field: "ACCOUNT_NUM2", sorter: "string", width: 140 },
                        { headerHozAlign: "center", hozAlign: "center", title: "예금주명", field: "DEPOSITOR2", sorter: "string", width: 150 },
                        { headerHozAlign: "center", hozAlign: "right", title: "고객송금액", field: "CUST_TRANSFER_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "left", title: "첨부파일비고", field: "ADDFILE_NOTE", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT<br>point", field: "MOT_POINT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본정책", field: "POLICY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "APT5만<br>Off_IP팩3만<br>대학전략동", field: "DE_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "IF단지4만<br>Bulk활성화<br>법인Bulk", field: "DE_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "결합", field: "DE_AMT3", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "SM패스이미징차감", field: "DE_AMT4", sorter: "number", width: 140, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "사은품초과등<br>기타차감", field: "DE_AMT5", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본장려금<br>기지급", field: "DE_AMT6", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "지급/미지급사유", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICEGBCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자명", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "SA_ID", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자ID", field: "RECEIPTID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자명", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품순번", field: "PRODUCTSEQ", sorter: "string", width: 80, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "메가패스/<br>메가티비<br>약정기간", field: "PRODUCT_PERIOD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "제출여부", field: "SUBMIT_YN", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "승인여부", field: "APPROVAL_YN", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200 },
                { formatter: fn_CellReg, width: 80, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Grid2Init = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "업로드월", field: "UPLOAD_MON", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "이름", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "명칭", field: "UPLOADNM", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "일시", field: "UPLOADDT", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "결과", field: "RESULT", sorter: "string" },
                { headerHozAlign: "center", hozAlign: "right", title: "총건수", field: "TOT_CNT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "비고", field: "MESSAGE", sorter: "string" }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid2', 'fitColumns', true, 'local'); // div의 grid id 값
        }

    
        var fn_Load = function () {
        }

        var fn_PopupUpload = function () {
            var form = "/Jungsan/UnionJSEmpSupportChkMngUploadPopup";
            var target = "UnionJSEmpSupportChkMngUploadPopup";
            var param = {
            };
            var nWidth = 892;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pType: "jungsan",
                    pUPPERYNCHKLV: "3",
                    pSHOWORGLV: "3"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function () {
            var BaseMonth = '';
            var OrgCd = '';

            if (idChk) {
                BaseMonth = $("#<%= hdnToMonth.ClientID %>").val();
                OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

                if (BaseMonth.length == 0) {
                    ktsGridAlert("기준일자를 선택해주세요.");
                    return false;
                }
                if (OrgCd.length == 0) {
                    ktsGridAlert("조회할 조직을 선택해 주세요.");
                    return false;
                } else {
                    return true;
                }
            }

            return true;
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var checkId = function (element) {
            var id = $(element).attr('id');
            if (id) {
                return id; // id가 있으면 해당 id 반환
            } else {
                return false; // id가 없으면 false 반환
            }
        }

        var fn_excelDown = function (gubun, json, sheetNm, fileNm, type) {
            var headers;

            // Custom headers mapping   
            if (gubun === 'not') {
                headers = [
                    ['참조KEY', '본부', '지사', '지점', '사원구분', '사원번호', '실판매자', '고객명', '고객지원가능금액\n(정산총액-MOT실적급)', '본인지급분', '송금 적용액', '고객선지급분\n적용액', '선지급분', '선지급분', '선지급분', '선지급분', '선지급분', '선지급분', '후지급분', '후지급분', '후지급분', '후지급분', '후지급분', '첨부파일비고', 'MOT Point', '정산총액', '기본정책', 'MOT실적급', 'APT5만\nOff_IP팩3만\n대학전략동', 'IF단지4만\nBulk활성화\n법인Bulk', '결합', 'SM패스\n이미징\n차감', '사은품초과등\n기타차감', '기본장려금\n기지급', '지급/미지급사유', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '진행상태', '구분', '수용국\n(회선수)', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간', '제출여부', '승인여부', '오류메시지', '발생일'],
                    ['', '', '', '', '', '', '', '', '', '', '', '', '은행', '은행명', '계좌', '예금주명', '고객선지급분', '관계', '은행', '은행명', '계좌', '예금주명', '고객송금액', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
                ];
            }
            else {
                headers = [
                    ['참조KEY', '본부', '지사', '지점', '사원구분', '사원번호', '실판매자', '고객명', '고객지원가능금액\n(정산총액-MOT실적급)', '본인지급분', '송금 적용액', '고객선지급분\n적용액', '선지급분', '선지급분', '선지급분', '선지급분', '선지급분', '선지급분', '후지급분', '후지급분', '후지급분', '후지급분', '후지급분', '첨부파일비고', 'MOT Point', '정산총액', '기본정책', 'MOT실적급', 'APT5만\nOff_IP팩3만\n대학전략동', 'IF단지4만\nBulk활성화\n법인Bulk', '결합', 'SM패스\n이미징\n차감', '사은품초과등\n기타차감', '기본장려금\n기지급', '지급/미지급사유', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '진행상태', '구분', '수용국\n(회선수)', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간', '제출여부', '승인여부'],
                    ['', '', '', '', '', '', '', '', '', '', '', '', '은행', '은행명', '계좌', '예금주명', '고객선지급분', '관계', '은행', '은행명', '계좌', '예금주명', '고객송금액', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
                ];
            }

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            if (gubun === 'not') {
                data = jsonData.map(item => [item.REFKEY, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.CUSTOMERNM, item.CUSTSUPPORT_AMT, item.MYPAY_AMT, item.SENDAPPLY_AMT, item.PREPAY_AMT, item.BANKCD1, item.BANKNM1, item.ACCOUNT_NUM1, item.DEPOSITOR1, item.CUST_PAY_AMT, item.RELATIONSHIP, item.BANKCD2, item.BANKNM2, item.ACCOUNT_NUM2, item.DEPOSITOR2, item.CUST_TRANSFER_AMT, item.ADDFILE_NOTE, item.MOT_POINT, item.TOT_JS_AMT, item.POLICY_AMT, item.MOT_PERFOR_AMT, item.DE_AMT1, item.DE_AMT2, item.DE_AMT3, item.DE_AMT4, item.DE_AMT5, item.DE_AMT6, item.POLICYNM, item.JSCD, item.SERVICEGBCD, item.RECEIPTDT, item.SERVICENM, item.SERVICENO, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPTID, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTNM, item.PRODUCT_PERIOD, item.SUBMIT_YN, item.APPROVAL_YN, item.EMSG, item.DBCREATEDT]);
            }
            else {
                data = jsonData.map(item => [item.REFKEY, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.CUSTOMERNM, item.CUSTSUPPORT_AMT, item.MYPAY_AMT, item.SENDAPPLY_AMT, item.PREPAY_AMT, item.BANKCD1, item.BANKNM1, item.ACCOUNT_NUM1, item.DEPOSITOR1, item.CUST_PAY_AMT, item.RELATIONSHIP, item.BANKCD2, item.BANKNM2, item.ACCOUNT_NUM2, item.DEPOSITOR2, item.CUST_TRANSFER_AMT, item.ADDFILE_NOTE, item.MOT_POINT, item.TOT_JS_AMT, item.POLICY_AMT, item.MOT_PERFOR_AMT, item.DE_AMT1, item.DE_AMT2, item.DE_AMT3, item.DE_AMT4, item.DE_AMT5, item.DE_AMT6, item.POLICYNM, item.JSCD, item.SERVICEGBCD, item.RECEIPTDT, item.SERVICENM, item.SERVICENO, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPTID, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTNM, item.PRODUCT_PERIOD, item.SUBMIT_YN, item.APPROVAL_YN]);
            }

            data.unshift(headers[1]); // Add the second row of headers
            data.unshift(headers[0]); // Add the first row of headers

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            if (gubun === 'not') {
                ws['!merges'] = [
                    { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
                    { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } },
                    { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } },
                    { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } },
                    { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } },
                    { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } },
                    { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } },
                    { s: { r: 0, c: 7 }, e: { r: 1, c: 7 } },
                    { s: { r: 0, c: 8 }, e: { r: 1, c: 8 } },
                    { s: { r: 0, c: 9 }, e: { r: 1, c: 9 } },
                    { s: { r: 0, c: 10 }, e: { r: 1, c: 10 } },
                    { s: { r: 0, c: 11 }, e: { r: 1, c: 11 } },
                    { s: { r: 0, c: 12 }, e: { r: 0, c: 17 } },
                    { s: { r: 0, c: 18 }, e: { r: 0, c: 22 } },
                    { s: { r: 0, c: 23 }, e: { r: 1, c: 23 } },
                    { s: { r: 0, c: 24 }, e: { r: 1, c: 24 } },
                    { s: { r: 0, c: 25 }, e: { r: 1, c: 25 } },
                    { s: { r: 0, c: 26 }, e: { r: 1, c: 26 } },
                    { s: { r: 0, c: 27 }, e: { r: 1, c: 27 } },
                    { s: { r: 0, c: 28 }, e: { r: 1, c: 28 } },
                    { s: { r: 0, c: 29 }, e: { r: 1, c: 29 } },
                    { s: { r: 0, c: 30 }, e: { r: 1, c: 30 } },
                    { s: { r: 0, c: 31 }, e: { r: 1, c: 31 } },
                    { s: { r: 0, c: 32 }, e: { r: 1, c: 32 } },
                    { s: { r: 0, c: 33 }, e: { r: 1, c: 33 } },
                    { s: { r: 0, c: 34 }, e: { r: 1, c: 34 } },
                    { s: { r: 0, c: 35 }, e: { r: 1, c: 35 } },
                    { s: { r: 0, c: 36 }, e: { r: 1, c: 36 } },
                    { s: { r: 0, c: 37 }, e: { r: 1, c: 37 } },
                    { s: { r: 0, c: 38 }, e: { r: 1, c: 38 } },
                    { s: { r: 0, c: 39 }, e: { r: 1, c: 39 } },
                    { s: { r: 0, c: 40 }, e: { r: 1, c: 40 } },
                    { s: { r: 0, c: 41 }, e: { r: 1, c: 41 } },
                    { s: { r: 0, c: 42 }, e: { r: 1, c: 42 } },
                    { s: { r: 0, c: 43 }, e: { r: 1, c: 43 } },
                    { s: { r: 0, c: 44 }, e: { r: 1, c: 44 } },
                    { s: { r: 0, c: 45 }, e: { r: 1, c: 45 } },
                    { s: { r: 0, c: 46 }, e: { r: 1, c: 46 } },
                    { s: { r: 0, c: 47 }, e: { r: 1, c: 47 } },
                    { s: { r: 0, c: 48 }, e: { r: 1, c: 48 } },
                    { s: { r: 0, c: 49 }, e: { r: 1, c: 49 } },
                    { s: { r: 0, c: 50 }, e: { r: 1, c: 50 } },
                    { s: { r: 0, c: 51 }, e: { r: 1, c: 51 } },
                    { s: { r: 0, c: 52 }, e: { r: 1, c: 52 } },
                    { s: { r: 0, c: 53 }, e: { r: 1, c: 53 } },
                    { s: { r: 0, c: 54 }, e: { r: 1, c: 54 } },
                    { s: { r: 0, c: 55 }, e: { r: 1, c: 55 } }
                ];

                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 18.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 20.89 },//고객지원가능금액
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 26.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 22.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 26.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 28.89 },
                    { wch: 22.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 32.89 },
                    { wch: 18.89 }
                ];

            }
            else {
                ws['!merges'] = [
                    { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
                    { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } },
                    { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } },
                    { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } },
                    { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } },
                    { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } },
                    { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } },
                    { s: { r: 0, c: 7 }, e: { r: 1, c: 7 } },
                    { s: { r: 0, c: 8 }, e: { r: 1, c: 8 } },
                    { s: { r: 0, c: 9 }, e: { r: 1, c: 9 } },
                    { s: { r: 0, c: 10 }, e: { r: 1, c: 10 } },
                    { s: { r: 0, c: 11 }, e: { r: 1, c: 11 } },
                    { s: { r: 0, c: 12 }, e: { r: 0, c: 17 } },
                    { s: { r: 0, c: 18 }, e: { r: 0, c: 22 } },
                    { s: { r: 0, c: 23 }, e: { r: 1, c: 23 } },
                    { s: { r: 0, c: 24 }, e: { r: 1, c: 24 } },
                    { s: { r: 0, c: 25 }, e: { r: 1, c: 25 } },
                    { s: { r: 0, c: 26 }, e: { r: 1, c: 26 } },
                    { s: { r: 0, c: 27 }, e: { r: 1, c: 27 } },
                    { s: { r: 0, c: 28 }, e: { r: 1, c: 28 } },
                    { s: { r: 0, c: 29 }, e: { r: 1, c: 29 } },
                    { s: { r: 0, c: 30 }, e: { r: 1, c: 30 } },
                    { s: { r: 0, c: 31 }, e: { r: 1, c: 31 } },
                    { s: { r: 0, c: 32 }, e: { r: 1, c: 32 } },
                    { s: { r: 0, c: 33 }, e: { r: 1, c: 33 } },
                    { s: { r: 0, c: 34 }, e: { r: 1, c: 34 } },
                    { s: { r: 0, c: 35 }, e: { r: 1, c: 35 } },
                    { s: { r: 0, c: 36 }, e: { r: 1, c: 36 } },
                    { s: { r: 0, c: 37 }, e: { r: 1, c: 37 } },
                    { s: { r: 0, c: 38 }, e: { r: 1, c: 38 } },
                    { s: { r: 0, c: 39 }, e: { r: 1, c: 39 } },
                    { s: { r: 0, c: 40 }, e: { r: 1, c: 40 } },
                    { s: { r: 0, c: 41 }, e: { r: 1, c: 41 } },
                    { s: { r: 0, c: 42 }, e: { r: 1, c: 42 } },
                    { s: { r: 0, c: 43 }, e: { r: 1, c: 43 } },
                    { s: { r: 0, c: 44 }, e: { r: 1, c: 44 } },
                    { s: { r: 0, c: 45 }, e: { r: 1, c: 45 } },
                    { s: { r: 0, c: 46 }, e: { r: 1, c: 46 } },
                    { s: { r: 0, c: 47 }, e: { r: 1, c: 47 } },
                    { s: { r: 0, c: 48 }, e: { r: 1, c: 48 } },
                    { s: { r: 0, c: 49 }, e: { r: 1, c: 49 } },
                    { s: { r: 0, c: 50 }, e: { r: 1, c: 50 } },
                    { s: { r: 0, c: 51 }, e: { r: 1, c: 51 } },
                    { s: { r: 0, c: 52 }, e: { r: 1, c: 52 } },
                    { s: { r: 0, c: 53 }, e: { r: 1, c: 53 } }
                ];

                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 18.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 20.89 },//고객지원가능금액
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 26.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 22.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 14.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 26.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 28.89 },
                    { wch: 22.89 },
                    { wch: 12.89 },
                    { wch: 12.89 },
                    { wch: 12.89 }
                ];
            }

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center", wrapText: true },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            // Apply styles to headers
            for (var R = 0; R < headers.length; ++R) {
                for (var C = 0; C < headers[R].length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 1) {
                        if (C == 24) {
                            ws[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else if ((C >= 8 && C <= 11) || C == 16 || C == 22 || (C >= 25 && C <= 33)) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 51) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

            endProgress('');
        }

        var excelTitle = function (pTitle) {
            var title = '';
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2) +
                ('0' + today.getDate()).slice(-2);

            title += formattedDate + '_';
            title += pTitle;

            return title;
        }

        function getLastDayOfMonth(year, month) {
            return new Date(year, month, 0).getDate();
        }

        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });

                fn_AuthGroupList();
            }
            else if (callGb == 'authGroupList') {
                $.each(JSON.parse(json), function (idx, item) {
                    $("#hdnAuthNm").val(item.AUTHNM);
                });
            }
            else if (callGb == 'delTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridId = 'ktsGrid';
                    ktsGridAlert("삭제를 완료했습니다.", 1000);
                    setTimeout(function () {
                        $('.tabnav a').eq(0).click();
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'modExcel') {

                callGb = '';

                if (JSON.parse(json).length === -1) {
                    if ($('.tabnav a.active').text() === '이력 조회') {
                        ktsGridId = 'ktsGrid2';
                        ktsGridAlert("Data가 없습니다.");
                    }
                    else {
                        ktsGridId = 'ktsGrid';
                        ktsGridAlert("Data가 없습니다.");
                    }
                    endProgress('');
                }
                else {
                    var title = excelTitle('통합정산 고객지원조사');
                    var sheet = '';
                    var pDate1 = '';
                    var pDate2 = '';
                    var pMon = '';

                    if ($("#selDt").val() == 'm') {
                        pDate1 = $("#selMon").val().replace(/-/gi, "");
                        pDate2 = $("#selMon").val().replace(/-/gi, "");
                    }
                    else {
                        pDate1 = $(".stMonth").val().replace(/-/gi, "");
                        pDate2 = $(".etMonth").val().replace(/-/gi, "");
                    }

                    if (pDate1 > pDate2) {
                        pMon = pDate1;
                    }
                    else {
                        pMon = pDate2;
                    }

                    sheet = '고객지원조사';
                    sheet += '(' + pMon + ")";

                    fn_excelDown('mod', json, sheet, title, 'xlsx');
                }
            }
        }

        var fn_GridExcel = function (sheetNm, fileNm, type) {
            var headers;

            // Custom headers mapping   
            headers = [
                ['No.', '정산월', '접수월', '본부', '지사', '지점', '사원구분', '사번', 'KTS사번', 'IDMS사번', '실판매자', '고객명', '고객지원가능금액\n(정산총액-MOT실적급)', '본인지급분', '송금 적용액', '고객선지급분\n적용액', '선지급분', '선지급분', '선지급분', '선지급분', '선지급분', '선지급분', '후지급분', '후지급분', '후지급분', '후지급분', '후지급분', '첨부파일비고', 'MOT Point', '정산총액', '기본정책', 'MOT실적급', 'APT5만\nOff_IP팩3만\n대학전략동', 'IF단지4만\nBulk활성화\n법인Bulk', '결합', 'SM패스\n이미징\n차감', '사은품초과등\n기타차감', '기본장려금\n기지급', '지급/미지급사유', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '진행상태', '구분', '수용국\n(회선수)', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간', '제출여부', '승인여부', '등록일', '변경일'],
                ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '은행', '은행명', '계좌', '예금주명', '고객선지급분', '관계', '은행', '은행명', '계좌', '예금주명', '고객송금액', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
            ];

            var data = ktsGrid.getData();

            // 그리드 데이터 변환
            var excelData = [
                headers[0],  // First row of headers
                headers[1], // Second row of headers
                ...data.map(row => [row.id, row.JS_MON, row.RECEIPT_MON, row.ORGNM1, row.ORGNM2, row.ORGNM3, row.JOBNM, row.EMPNO, row.KTSEMPNO, row.IDMSEMPNO, row.EMPNM, row.CUSTOMERNM, row.CUSTSUPPORT_AMT, row.MYPAY_AMT, row.SENDAPPLY_AMT, row.PREPAY_AMT, row.BANKCD1, row.BANKNM1, row.ACCOUNT_NUM1, row.DEPOSITOR1, row.CUST_PAY_AMT, row.RELATIONSHIP, row.BANKCD2, row.BANKNM2, row.ACCOUNT_NUM2, row.DEPOSITOR2, row.CUST_TRANSFER_AMT, row.ADDFILE_NOTE, row.MOT_POINT, row.TOT_JS_AMT, row.POLICY_AMT, row.MOT_PERFOR_AMT, row.DE_AMT1, row.DE_AMT2, row.DE_AMT3, row.DE_AMT4, row.DE_AMT5, row.DE_AMT6, row.POLICYNM, row.JSCD, row.SERVICEGBCD, row.RECEIPTDT, row.SERVICENM, row.SERVICENO, row.STATUS, row.GUBUN, row.ACCEPT_ORGNM, row.OPENDT, row.CANCELDT, row.BUY_ORGNM, row.SAID, row.RECEIPTID, row.BUY_EMPNM, row.KT_GIFT, row.PRODUCTNM, row.PRODUCT_PERIOD, row.SUBMIT_YN, row.APPROVAL_YN, row.DBCREATEDT, row.DBLASTUPDATEDT])  // 나머지 데이터
            ];

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(excelData);

            ws['!merges'] = [
                { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
                { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } },
                { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } },
                { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } },
                { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } },
                { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } },
                { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } },
                { s: { r: 0, c: 7 }, e: { r: 1, c: 7 } },
                { s: { r: 0, c: 8 }, e: { r: 1, c: 8 } },
                { s: { r: 0, c: 9 }, e: { r: 1, c: 9 } },
                { s: { r: 0, c: 10 }, e: { r: 1, c: 10 } },
                { s: { r: 0, c: 11 }, e: { r: 1, c: 11 } },
                { s: { r: 0, c: 12 }, e: { r: 1, c: 12 } },
                { s: { r: 0, c: 13 }, e: { r: 1, c: 13 } },
                { s: { r: 0, c: 14 }, e: { r: 1, c: 14 } },
                { s: { r: 0, c: 15 }, e: { r: 1, c: 15 } },
                { s: { r: 0, c: 16 }, e: { r: 0, c: 21 } },
                { s: { r: 0, c: 22 }, e: { r: 0, c: 26 } },
                { s: { r: 0, c: 27 }, e: { r: 1, c: 27 } },
                { s: { r: 0, c: 28 }, e: { r: 1, c: 28 } },
                { s: { r: 0, c: 29 }, e: { r: 1, c: 29 } },
                { s: { r: 0, c: 30 }, e: { r: 1, c: 30 } },
                { s: { r: 0, c: 31 }, e: { r: 1, c: 31 } },
                { s: { r: 0, c: 32 }, e: { r: 1, c: 32 } },
                { s: { r: 0, c: 33 }, e: { r: 1, c: 33 } },
                { s: { r: 0, c: 34 }, e: { r: 1, c: 34 } },
                { s: { r: 0, c: 35 }, e: { r: 1, c: 35 } },
                { s: { r: 0, c: 36 }, e: { r: 1, c: 36 } },
                { s: { r: 0, c: 37 }, e: { r: 1, c: 37 } },
                { s: { r: 0, c: 38 }, e: { r: 1, c: 38 } },
                { s: { r: 0, c: 39 }, e: { r: 1, c: 39 } },
                { s: { r: 0, c: 40 }, e: { r: 1, c: 40 } },
                { s: { r: 0, c: 41 }, e: { r: 1, c: 41 } },
                { s: { r: 0, c: 42 }, e: { r: 1, c: 42 } },
                { s: { r: 0, c: 43 }, e: { r: 1, c: 43 } },
                { s: { r: 0, c: 44 }, e: { r: 1, c: 44 } },
                { s: { r: 0, c: 45 }, e: { r: 1, c: 45 } },
                { s: { r: 0, c: 46 }, e: { r: 1, c: 46 } },
                { s: { r: 0, c: 47 }, e: { r: 1, c: 47 } },
                { s: { r: 0, c: 48 }, e: { r: 1, c: 48 } },
                { s: { r: 0, c: 49 }, e: { r: 1, c: 49 } },
                { s: { r: 0, c: 50 }, e: { r: 1, c: 50 } },
                { s: { r: 0, c: 51 }, e: { r: 1, c: 51 } },
                { s: { r: 0, c: 52 }, e: { r: 1, c: 52 } },
                { s: { r: 0, c: 53 }, e: { r: 1, c: 53 } },
                { s: { r: 0, c: 54 }, e: { r: 1, c: 54 } },
                { s: { r: 0, c: 55 }, e: { r: 1, c: 55 } },
                { s: { r: 0, c: 56 }, e: { r: 1, c: 56 } },
                { s: { r: 0, c: 57 }, e: { r: 1, c: 57 } },
                { s: { r: 0, c: 58 }, e: { r: 1, c: 58 } },
                { s: { r: 0, c: 59 }, e: { r: 1, c: 59 } },
                { s: { r: 0, c: 60 }, e: { r: 1, c: 60 } }
            ];

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center", wrapText: true },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            // Apply styles to headers
            for (var R = 0; R < headers.length; ++R) {
                for (var C = 0; C < headers[R].length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }

            // 엑셀 파일 생성 및 다운로드
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);
            XLSX.writeFile(wb, fileNm + '.' + type);
        }

        var fn_Del = function (id) {
            if (confirm("삭제 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('EMPSUPPORTCHK', 'D', id);
            }
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'EMPSUPPORTCHK';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_AuthGroupList = function () {
            callGb = 'authGroupList';

            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= hdnAuthId.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetAuthGroupList');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (target, jobType, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var jsMon = rowData.JS_MON;
            var said = rowData.SAID;
            var status = rowData.STATUS;
            var gubun = rowData.GUBUN;
            var serviceNm = rowData.SERVICENM;
            var productNm = rowData.PRODUCTNM;
            var productSeq = rowData.PRODUCTSEQ;

            //입력 파라미터
            var aData = [];
            aData[0] = target;
            aData[1] = jobType;
            aData[2] = jsMon;
            aData[3] = said;
            aData[4] = status;
            aData[5] = gubun;
            aData[6] = serviceNm;
            aData[7] = productNm;
            aData[8] = productSeq;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ExcelSearch = function (gubun) {
            callGb = gubun;
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2);

            var pDate = formattedDate;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pDate;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function (gubun) {
            if (fn_SearchChk()) {

                var pDate1 = '';
                var pDate2 = '';
                var pOrgCd = '';

                if (idChk) pOrgCd = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

                if ($("#selDt").val() == 'm') {
                    pDate1 = $("#selMon").val();
                    pDate2 = $("#selMon").val();
                }
                else {
                    pDate1 = $(".stMonth").val();
                    pDate2 = $(".etMonth").val();
                }
          
                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = pOrgCd;
                aData[2] = pDate1;
                aData[3] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                fn_GridInit();
                ktsRowCountClass = 'searchrowcount';

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
                $(".table-option").css('display', 'block');
                $(".table-search").css('display', 'block');
            }
        }

        var fn_Hist = function () {
            var pUploadMon = '';
            var gubun = 'RESULT';
            var pDate1 = '';
            var pDate2 = '';

            if ($("#selDt").val() == 'm') {
                pDate1 = $("#selMon").val().replace(/-/gi, "");
                pDate2 = $("#selMon").val().replace(/-/gi, "");
            }
            else {
                pDate1 = $(".stMonth").val().replace(/-/gi, "");
                pDate2 = $(".etMonth").val().replace(/-/gi, "");
            }

            if (pDate1 > pDate2) {
                pUploadMon = pDate1;
            }
            else {
                pUploadMon = pDate2;
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pUploadMon;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetHistGridData');

            fn_Grid2Init();
            ktsRowCountClass = 'searchrowcount2';
            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        function endProgress(msg) {
            $("#progressBarSet").hide();

            if (msg != '') {
                alert(msg);
            }
        }

        function startProgress() {
            $("#progressBarSet").show();
            $("#progressText").html("잠시만 기다려주세요.");
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <div id="divEnd"></div>
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>정산월</label>
                <select id="selDt" style="width:80px;display:none;">
                    <option selected="selected" value="m">정산월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnUpload" type="button" class="btn-green last" style="float: right;" value="일괄등록" />
                    <input id="btnExcel" type="button" class="btn-black last" style="float: right;margin-right:30px" value="고객지원조사내려받기" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                    <input id="hdnRm" type="hidden" runat="server" />
                    <input id="hdnAuthId" type="hidden" runat="server" />
                    <input id="hdnAuthNm" type="hidden" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <ul class="tabnav">
                <li><a href="#tab01">고객지원조사 조회</a></li>
                <li><a href="#tab02">이력 조회</a></li>
            </ul>
            <div class="tabcontent">
                <div id="tab01">
                    <div class="list-top">
                        <strong>고객지원조사 결과</strong>
                        <label class="searchrowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField">
                                    <option value="">선택</option>
                                    <option value="RECEIPT_MON">접수월</option>
                                    <option value="ORGNM1">본부</option>
                                    <option value="ORGNM2">지사</option>
                                    <option value="ORGNM3">지점</option>
                                    <option value="EMPNM">실판매자명</option>
                                </select>
                                : 
                        <input id="inputValue" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid"></div>
                    </div>
                </div>
                <div id="tab02">
                    <div class="list-top">
                        <strong>이력 결과</strong>
                        <label class="searchrowcount2"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel2" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div id="ktsGrid2"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- E:datalist -->
        <div id="progressBarSet" style="display:none">
            <div id="progressBarContainer">
            <img src="../Resource/images/icon_loadingbar.gif" />
            <h2>다운로드중...</h2>
                <div id="progressText"></div>
            </div>
        </div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>