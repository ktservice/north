﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalesJSPolicyMng.aspx.cs" Inherits="KTSSolutionWeb.SalesJSPolicyMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .selCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        .txtCell {
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
            text-align:right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $("#selField").change(function (e) {
                    fn_Field('selField', 'inputValue');
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnSelect").click(function (e) {
                    fn_Search('SEARCH');
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "영업정산 정책관리 정보.xlsx", { sheetName: "영업정산 정책관리 정보" });
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnPolicyReg").click(function (e) {
                    fn_DirectPolicyReg();
                });

                $("#btnPolicyCode").click(function (e) {
                    fn_PolicyCode();
                });

                $("#selMon").change(function (e) {
                    fn_Class('selPolicy', this.value);
                });
            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            fn_Class('selMon', '');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellInput = function (cell, formatterParams, onRendered) { //텍스트박스
            var cellValue = cell.getValue();
            var cellNo = cell.getRow().getData().id;
            var fieldNm = cell.getField();

            return fn_CellInputBind(cellNo, fieldNm, cellValue);
        }

        var fn_CellAddDdl = function (cell, formatterParams, onRendered) { //텍스트박스
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;

            if (ddlChanged) {
                html = "<select id='selPolicyAddYn' class='selCell' onchange='fn_PolicyAddYnChg(this, \"" + id + "\");' style='background-color:#cfd2d7 !important'>";
            }
            else {
                html = "<select id='selPolicyAddYn' class='selCell' onchange='fn_PolicyAddYnChg(this, \"" + id + "\");'>";
            }
            
            if (cellValue === 'Y') {
                html += "<option value='Y' selected='selected'>Y</option>";
                html += "<option value='N'>N</option>";
            }
            else {
                html += "<option value='Y'>Y</option>";
                html += "<option value='N' selected='selected'>N</option>";
            }

            html += "</select>";

            ddlChanged = false;

            return html;
        }

        var ddlChanged = false;
        var fn_PolicyAddYnChg = function (obj, cellNo) {
            var selectedValue = obj.value;
            ddlChanged = true;

            //obj.style.setProperty('background-color', '#cfd2d7 ', 'important');

            ktsGrid.updateData([{
                id: cellNo, POLICYADDYN: selectedValue
            }])
            .then(function () {
                //run code after row
            })
            .catch(function (error) {
                //handle error
            });

        }
       

        var fn_CellInputBind = function (cellNo, fieldNm, value) { //텍스트박스
            var html = '';

            if (fieldNm == 'TECH_AMT0') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, TECH_AMT0TEXT: value }]);
                html = "<input type='text' id='txtInput2' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'TECH_AMT1') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, TECH_AMT1TEXT: value }]);
                html = "<input type='text' id='txtInput3' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'TECH_AMT2') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, TECH_AMT2TEXT: value }]);
                html = "<input type='text' id='txtInput4' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'TECH_AMT3') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, TECH_AMT3TEXT: value }]);
                html = "<input type='text' id='txtInput5' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'TECH_AMT5') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, TECH_AMT5TEXT: value }]);
                html = "<input type='text' id='txtInput6' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'GEN_AMT0') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, GEN_AMT0TEXT: value }]);
                html = "<input type='text' id='txtInput7' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'GEN_AMT1') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, GEN_AMT1TEXT: value }]);
                html = "<input type='text' id='txtInput8' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'GEN_AMT2') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, GEN_AMT2TEXT: value }]);
                html = "<input type='text' id='txtInput9' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'GEN_AMT3') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, GEN_AMT3TEXT: value }]);
                html = "<input type='text' id='txtInput10' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'GEN_AMT5') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, GEN_AMT5TEXT: value }]);
                html = "<input type='text' id='txtInput11' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'ADD_AMT0') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, ADD_AMT0TEXT: value }]);
                html = "<input type='text' id='txtInput12' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'ADD_AMT1') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, ADD_AMT1TEXT: value }]);
                html = "<input type='text' id='txtInput13' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'ADD_AMT2') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, ADD_AMT2TEXT: value }]);
                html = "<input type='text' id='txtInput14' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'ADD_AMT3') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, ADD_AMT3TEXT: value }]);
                html = "<input type='text' id='txtInput15' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'ADD_AMT5') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, ADD_AMT5TEXT: value }]);
                html = "<input type='text' id='txtInput16' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'POINT0') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, POINT0TEXT: value }]);
                html = "<input type='text' id='txtInput17' class='txtPCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'POINT1') {
                value = priceToString2(value);
                ktsGrid.updateData([{ id: cellNo, POINT1TEXT: value }]);
                html = "<input type='text' id='txtInput18' class='txtPCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'POINT2') {
                value = priceToString2(value);
                ktsGrid.updateData([{ id: cellNo, POINT2TEXT: value }]);
                html = "<input type='text' id='txtInput19' class='txtPCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'POINT3') {
                value = priceToString2(value);
                ktsGrid.updateData([{ id: cellNo, POINT3TEXT: value }]);
                html = "<input type='text' id='txtInput20' class='txtPCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'POINT5') {
                value = priceToString2(value);
                ktsGrid.updateData([{ id: cellNo, POINT5TEXT: value }]);
                html = "<input type='text' id='txtInput21' class='txtPCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'CAL_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, CAL_AMTTEXT: value }]);
                html = "<input type='text' id='txtInput22' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }

            return html;
        }

        var fn_MemoChg = function (obj, cellNo, fieldNm) {
            var value = obj.value.replace(/[,]/g, '');
            obj.style.setProperty('background-color', '#cfd2d7', 'important');

            value = getNumberOrZero(value);

            // Remove all non-digit characters except for dot
            value = value.replace(/[^\d.]/g, '');

            // Restrict to two decimal places
            var parts = value.split('.');
            if (parts.length > 2) {
                parts = [parts[0], parts[1].substring(0, 2)];
            }
            if (parts[1] && parts[1].length > 2) {
                parts[1] = parts[1].substring(0, 2);
            }
            value = parts.join('.');

            // Add commas for every three digits before the decimal point
            if (parts[0].length > 3) {
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
            value = parts.join('.');

            // Set maximum length (e.g., 15 characters including commas and decimal point)
            var maxLength = 12;
            if (value.length > maxLength) {
                value = value.slice(0, maxLength);
            }

            // Update the input value
            obj.value = value;

            if (fieldNm == 'TECH_AMT0') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, TECH_AMT0TEXT: obj.value }]);
            }
            else if (fieldNm == 'TECH_AMT1') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, TECH_AMT1TEXT: obj.value }]);
            }
            else if (fieldNm == 'TECH_AMT2') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, TECH_AMT2TEXT: obj.value }]);
            }
            else if (fieldNm == 'TECH_AMT3') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, TECH_AMT3TEXT: obj.value }]);
            }
            else if (fieldNm == 'TECH_AMT5') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, TECH_AMT5TEXT: obj.value }]);
            }
            else if (fieldNm == 'GEN_AMT0') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, GEN_AMT0TEXT: obj.value }]);
            }
            else if (fieldNm == 'GEN_AMT1') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, GEN_AMT1TEXT: obj.value }]);
            }
            else if (fieldNm == 'GEN_AMT2') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, GEN_AMT2TEXT: obj.value }]);
            }
            else if (fieldNm == 'GEN_AMT3') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, GEN_AMT3TEXT: obj.value }]);
            }
            else if (fieldNm == 'GEN_AMT5') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, GEN_AMT5TEXT: obj.value }]);
            }
            else if (fieldNm == 'ADD_AMT0') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, ADD_AMT0TEXT: obj.value }]);
            }
            else if (fieldNm == 'ADD_AMT1') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, ADD_AMT1TEXT: obj.value }]);
            }
            else if (fieldNm == 'ADD_AMT2') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, ADD_AMT2TEXT: obj.value }]);
            }
            else if (fieldNm == 'ADD_AMT3') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, ADD_AMT3TEXT: obj.value }]);
            }
            else if (fieldNm == 'ADD_AMT5') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, ADD_AMT5TEXT: obj.value }]);
            }
            else if (fieldNm == 'POINT0') {
                obj.value = getNumberOrZero(value.replace(/[,]/g, ''));
                ktsGrid.updateData([{ id: cellNo, POINT0TEXT: obj.value }]);
            }
            else if (fieldNm == 'POINT1') {
                obj.value = getNumberOrZero(value.replace(/[,]/g, ''));
                ktsGrid.updateData([{ id: cellNo, POINT1TEXT: obj.value }]);
            }
            else if (fieldNm == 'POINT2') {
                obj.value = getNumberOrZero(value.replace(/[,]/g, ''));
                ktsGrid.updateData([{ id: cellNo, POINT2TEXT: obj.value }]);
            }
            else if (fieldNm == 'POINT3') {
                obj.value = getNumberOrZero(value.replace(/[,]/g, ''));
                ktsGrid.updateData([{ id: cellNo, POINT3TEXT: obj.value }]);
            }
            else if (fieldNm == 'POINT5') {
                obj.value = getNumberOrZero(value.replace(/[,]/g, ''));
                ktsGrid.updateData([{ id: cellNo, POINT5TEXT: obj.value }]);
            }
            else if (fieldNm == 'CAL_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, CAL_AMTTEXT: obj.value }]);
            }

        }


        //숫자 체크
        function getNumberOrZero(value) {
            // 입력 값이 숫자인지 확인합니다.
            if (typeof value === 'number' && !isNaN(value)) {
                return String(value);
            }
            // 입력 값이 문자열로 되어 있는지 확인하고, 숫자로 변환합니다.
            if (typeof value === 'string') {
                // 첫 번째 소수점만 남기고 이후의 값은 삭제합니다.
                var firstDecimalIndex = value.indexOf('.');
                if (firstDecimalIndex !== -1) {
                    var secondDecimalIndex = value.indexOf('.', firstDecimalIndex + 1);
                    if (secondDecimalIndex !== -1) {
                        value = value.substring(0, secondDecimalIndex);
                    }
                }

                var number = Number(value);
                if (!isNaN(number)) {
                    return String(number);
                }
            }
            // 위 조건을 모두 만족하지 않으면 0을 반환합니다.
            return '0';
        }


        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }


        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Mod(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";

            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "기준월", field: "MDATE", sorter: "string", width: 140, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정책코드", field: "POLICYCD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "정책명", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스구분코드", field: "SERVICEGBCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스코드", field: "SERVICECD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "서비스명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "PRODUCTCD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분코드", field: "PRODUCTGBCD", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분명", field: "PRODUCTGBNM", sorter: "string", width: 100 },
                {
                    title: '기술직', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "무약정", field: "TECH_AMT0", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "1년", field: "TECH_AMT1", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "2년", field: "TECH_AMT2", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "3년", field: "TECH_AMT3", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "5년", field: "TECH_AMT5", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "무약정입력값", field: "TECH_AMT0TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "1년입력값", field: "TECH_AMT1TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "2년입력값", field: "TECH_AMT2TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "3년입력값", field: "TECH_AMT3TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "5년입력값", field: "TECH_AMT5TEXT", sorter: "number", width: 120, visible: false },
                    ]
                },
                {
                    title: '일반직', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "무약정", field: "GEN_AMT0", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "1년", field: "GEN_AMT1", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "2년", field: "GEN_AMT2", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "3년", field: "GEN_AMT3", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "5년", field: "GEN_AMT5", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "무약정입력값", field: "GEN_AMT0TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "1년입력값", field: "GEN_AMT1TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "2년입력값", field: "GEN_AMT2TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "3년입력값", field: "GEN_AMT3TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "5년입력값", field: "GEN_AMT5TEXT", sorter: "number", width: 120, visible: false },
                    ]
                },
                {
                    title: '추가지급', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "무약정", field: "ADD_AMT0", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "1년", field: "ADD_AMT1", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "2년", field: "ADD_AMT2", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "3년", field: "ADD_AMT3", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "5년", field: "ADD_AMT5", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "무약정입력값", field: "ADD_AMT0TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "1년입력값", field: "ADD_AMT1TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "2년입력값", field: "ADD_AMT2TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "3년입력값", field: "ADD_AMT3TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "5년입력값", field: "ADD_AMT5TEXT", sorter: "number", width: 120, visible: false },
                    ]
                },
                {
                    title: '포인트', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "무약정", field: "POINT0", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "center", title: "1년", field: "POINT1", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "center", title: "2년", field: "POINT2", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "center", title: "3년", field: "POINT3", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "center", title: "5년", field: "POINT5", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "무약정입력값", field: "POINT0TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "1년입력값", field: "POINT1TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "2년입력값", field: "POINT2TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "3년입력값", field: "POINT3TEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "5년입력값", field: "POINT5TEXT", sorter: "number", width: 120, visible: false },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "right", title: "계산기준금액", field: "CAL_AMT", sorter: "number", width: 130, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "계산기준금액입력값", field: "CAL_AMTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200, visible: false },
                { formatter: fn_CellReg, width: 150, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];
         
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

  
        var fn_Load = function () {
        }

        var delFlag = false;
        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon' || callGb == 'selPolicy') {
                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    if (callGb == 'selMon') {
                        obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                    }
                    else {
                        obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                    }
                });

                if (callGb == 'selMon') {
                    $('#selMon option:first').prop('selected', true);
                }
                else if (callGb == 'selPolicy') {
                    $('#selPolicy option:first').prop('selected', true);
                }

                obj.val(obj.val()).trigger('change');
            }
            else if (callGb == 'delTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert("삭제를 완료했습니다.", 1000);
                    delFlag = true;
                    setTimeout(function () {
                        fn_Search('SEARCH');
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'modTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('변경되었습니다.', 1000);
                    setTimeout(function () {
                        fn_Search('SEARCH');
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
        }

        var fn_DirectPolicyReg = function () {
            var form = "/Jungsan/SalesJSProductMapping";
            var param = {
            };

            var directform = createForm(form, param);

            directform.submit();
        }

        var fn_PopupUpload = function () {
            var form = "/Jungsan/SalesJSPolicyMngUploadPopup";
            var target = "SalesJSPolicyMngUploadPopup";
            var param = {
            };
            var nWidth = 812;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PolicyCode = function () {
            var form = "/Jungsan/SalesJSPolicyCodeMappingPopup";
            var target = "SalesJSPolicyCodeMappingPopup";
            var param = {
            };

            var nWidth = 900;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PolicyReg = function (gubun) {
            var policyCd = $("#selPolicy").val();

            var form = "/Jungsan/SalesJSPolicyInfoRegPopup";
            var target = "SalesJSPolicyInfoRegPopup";

            var param = {
                pStatus: gubun,
                pPolicyCd: policyCd
            };

            var nWidth = 720;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }


        var fn_Field = function (id1, id2) {
            var fieldEl = document.getElementById(id1);
            var valueEl = document.getElementById(id2);
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_Mod = function (id) {
            if (confirm("변경 하시겠습니까?")) {
                callGb = 'modTran';
                fn_Transaction('M', id);
            }
        }

        var fn_Del = function (id) {
            if (confirm("삭제 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('D', id);
            }
        }

        var fn_Transaction = function (gubun, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var mdate = rowData.MDATE;
            var policyCd = rowData.POLICYCD;
            var serviceCd = rowData.SERVICECD;
            var productCd = rowData.PRODUCTCD;

            var techAmt0 = rowData.TECH_AMT0TEXT;
            var techAmt1 = rowData.TECH_AMT1TEXT;
            var techAmt2 = rowData.TECH_AMT2TEXT;
            var techAmt3 = rowData.TECH_AMT3TEXT;
            var techAmt5 = rowData.TECH_AMT5TEXT;

            var genAmt0 = rowData.GEN_AMT0TEXT;
            var genAmt1 = rowData.GEN_AMT1TEXT;
            var genAmt2 = rowData.GEN_AMT2TEXT;
            var genAmt3 = rowData.GEN_AMT3TEXT;
            var genAmt5 = rowData.GEN_AMT5TEXT;

            var addAmt0 = rowData.ADD_AMT0TEXT;
            var addAmt1 = rowData.ADD_AMT1TEXT;
            var addAmt2 = rowData.ADD_AMT2TEXT;
            var addAmt3 = rowData.ADD_AMT3TEXT;
            var addAmt5 = rowData.ADD_AMT5TEXT;

            var point0 = rowData.POINT0TEXT;
            var point1 = rowData.POINT1TEXT;
            var point2 = rowData.POINT2TEXT;
            var point3 = rowData.POINT3TEXT;
            var point5 = rowData.POINT5TEXT;

            var calAmt = rowData.CAL_AMTTEXT;


            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = mdate;
            aData[2] = policyCd;
            aData[3] = serviceCd;
            aData[4] = productCd;
            aData[5] = techAmt0.replace(/,/g, "");
            aData[6] = techAmt1.replace(/,/g, "");
            aData[7] = techAmt2.replace(/,/g, "");
            aData[8] = techAmt3.replace(/,/g, "");
            aData[9] = techAmt5.replace(/,/g, "");
            aData[10] = genAmt0.replace(/,/g, "");
            aData[11] = genAmt1.replace(/,/g, "");
            aData[12] = genAmt2.replace(/,/g, "");
            aData[13] = genAmt3.replace(/,/g, "");
            aData[14] = genAmt5.replace(/,/g, "");
            aData[15] = addAmt0.replace(/,/g, "");
            aData[16] = addAmt1.replace(/,/g, "");
            aData[17] = addAmt2.replace(/,/g, "");
            aData[18] = addAmt3.replace(/,/g, "");
            aData[19] = addAmt5.replace(/,/g, "");
            aData[20] = point0.replace(/,/g, "");
            aData[21] = point1.replace(/,/g, "");
            aData[22] = point2.replace(/,/g, "");
            aData[23] = point3.replace(/,/g, "");
            aData[24] = point5.replace(/,/g, "");
            aData[25] = calAmt.replace(/,/g, "");

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Class = function (gb, code) {
            callGb = gb;

            var gubun = '';

            if (gb == 'selMon') gubun = 'POLICYMNG';
            else if (gb == 'selPolicy') gubun = 'POLICYMNGCD';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = code;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetBizJsDll');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function (gubun) {
            var pDate = $("#selMon").val();
            var pPolicy = $("#selPolicy").val();
            var pGubun = gubun;

            //입력 파라미터
            var aData = [];
            aData[0] = pGubun;
            aData[1] = pDate;
            aData[2] = pDate;
            aData[3] = pPolicy;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="inpbox">
                    <label>구분</label>
                    <span class="optionbox">
                        <select id="selMon" style="width: 110px"></select>
                        <select id="selPolicy" style="width:246px"></select>
                    </span>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnUpload" type="button" class="btn-green last" style="float: right;" value="일괄등록" />
                    <input id="btnPolicyCode" type="button" class="btn-green last" style="float: right;" value="정책매핑정보" />
                    <input id="btnPolicyReg" type="button" class="btn-green last" style="float: right;" value="정책매핑등록" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>정책 조회 결과</strong>
                <label class="searchrowcount"></label>
			    <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
			    </div>
            </div>
            <!-- //E:list-top -->
            
            <!-- S:scrollbox -->
            <div class="scrollbox">
                <div class="table-search" style="display:none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="POLICYCD">정책코드</option>
                            <option value="POLICYNM">정책명</option>
                            <option value="SERVICEGBCD">서비스구분코드</option>
                            <option value="SERVICECD">서비스코드</option>
                            <option value="SERVICENM">서비스명</option>
                            <option value="PRODUCTCD">상품코드</option>
                            <option value="PRODUCTNM">상품명</option>
                            <option value="PRODUCTGBCD">상품구분코드</option>
                            <option value="PRODUCTGBNM">상품구분명</option>
                        </select> : 
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
            <!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>