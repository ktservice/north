﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnionJSProductRegPopup.aspx.cs" Inherits="KTSSolutionWeb.UnionJSProductRegPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>코드등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        input[type=radio] + * {
            margin-left: 0;
        }
        .radiobox input {
            position: relative;
            width: 20px;
            height: 12px;
            opacity: 1;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';
        var productgbCd = '';
        var productgbNm = '';
        var serviceGbCd = '';
        var serviceCd = '';
        var serviceNm = '';
        var productCd = '';
        var productNm = '';
        var memo = '';
        var useYn = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#selProductgb").change(function (e) {
                    fn_Class('selService', this.value);
                });

                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });

                $('input[name="gubun"]').change(function () {
                    //$("#selService").hide();
                    if ($(this).val() === 'new') {
                        $("#thProductgb").hide();
                        $("#tdProductgb").hide();
                        $("#thProductgbNm").show();
                        $("#tdProductgbNm").show();
                        $("#thProductgbCd").show();
                        $("#tdProductgbCd").show();

                        $("#thService").hide();
                        $("#tdService").hide();
                        $("#thServiceNm").show();
                        $("#tdServiceNm").show();
                        $("#thServiceCd").show();
                        $("#tdServiceCd").show();

                        $('input[name="gubun2"]').prop('disabled', true);
                        $('input[name="gubun2"][value="new"]').prop('checked', true);
                    }
                    else {
                        $("#thProductgb").show();
                        $("#tdProductgb").show();
                        $("#thProductgbNm").hide();
                        $("#tdProductgbNm").hide();
                        $("#thProductgbCd").hide();
                        $("#tdProductgbCd").hide();

                        $("#thService").show();
                        $("#tdService").show();
                        $("#thServiceNm").hide();
                        $("#tdServiceNm").hide();
                        $("#thServiceCd").hide();
                        $("#tdServiceCd").hide();

                        $('input[name="gubun2"]').prop('disabled', false);
                        $('input[name="gubun2"][value="ext"]').prop('checked', true);
                    }
                });

                $('input[name="gubun2"]').change(function () {
                    //$("#selService").hide();
                    if ($(this).val() === 'new') {
                        $("#thService").hide();
                        $("#tdService").hide();
                        $("#thServiceNm").show();
                        $("#tdServiceNm").show();
                        $("#thServiceCd").show();
                        $("#tdServiceCd").show();
                    }
                    else {
                        $("#thService").show();
                        $("#tdService").show();
                        $("#thServiceNm").hide();
                        $("#tdServiceNm").hide();
                        $("#thServiceCd").hide();
                        $("#tdServiceCd").hide();

                    }
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';

            var obj1 = $('#selUse');
            obj1.empty();

            obj1.append("<option value='Y'>Y</option>");
            obj1.append("<option value='N'>N</option>");

            $("#txtProductCd").attr('readonly', true);
            $("#txtProductCd").attr('style', 'background: #d1d1d1 !important;width: 400px');
            $("#txtServiceCd").attr('readonly', true);
            $("#txtServiceCd").attr('style', 'background: #d1d1d1 !important;width: 400px');

            $("#thProductgb").show();
            $("#tdProductgb").show();

            $("#thProductgbNm").hide();
            $("#tdProductgbNm").hide();
            $("#thProductgbCd").hide();
            $("#tdProductgbCd").hide();

            $("#thServiceNm").hide();
            $("#tdServiceNm").hide();
            $("#thServiceCd").hide();
            $("#tdServiceCd").hide();

            //$("#selService").hide();

            if ($("#hdnStatus").val() === 'M') {
                $("#thGubun").hide();
                $("#tdGubun").hide();
                $("#thGubun2").show();
                $("#tdGubun2").show();

                $("#btnReg").hide();
                $("#btnMod").show();

                $("#lblGubun22").text('변경');
            }
            else {
                $("#thGubun").show();
                $("#tdGubun").show();
                $("#thGubun2").show();
                $("#tdGubun2").show();

                $("#btnReg").show();
                $("#btnMod").hide();
            }
            
        }

        var fn_Load = function () {
            fn_Class('selProductgb', '');
        }

        var fn_RegTran = function () {
            if (fn_RegChk('I')) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_ModTran = function () {
            if (fn_RegChk('M')) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegChk = function (gubun) {
            productCd = $("#txtProductCd").val();
            productNm = $("#txtProductNm").val();
            serviceGbCd = $("#txtServiceGbCd").val();
            memo = $("#txtMemo").val();
            useYn = $("#selUse").val();
            
            if (gubun === 'M') {
                productgbCd = $("#selProductgb").val();
                productgbNm = $("#selProductgb option:selected").text();

                var selectedValue2 = $('input[name="gubun2"]:checked').val();

                if (selectedValue2 === 'new') {
                    serviceCd = $("#txtServiceCd").val();
                    serviceNm = $("#txtServiceNm").val();
                }
                else {
                    serviceCd = $("#selService").val();
                    serviceNm = $("#selService option:selected").text();
                }
            }
            else {
                var selectedValue = $('input[name="gubun"]:checked').val();
                var selectedValue2 = $('input[name="gubun2"]:checked').val();

                if (selectedValue === 'new') {
                    productgbCd = $("#txtProductgbCd").val();
                    productgbNm = $("#txtProductgbNm").val();
                }
                else {
                    productgbCd = $("#selProductgb").val();
                    productgbNm = $("#selProductgb option:selected").text();
                }

                if (selectedValue2 === 'new') {
                    serviceCd = $("#txtServiceCd").val();
                    serviceNm = $("#txtServiceNm").val();
                }
                else {
                    serviceCd = $("#selService").val();
                    serviceNm = $("#selService option:selected").text();
                }
            }

            if (productgbNm.length == 0) {
                ktsGridAlert("상품구분명을 입력하세요.");
                return false;
            }
            if (productgbCd.length == 0) {
                ktsGridAlert("상품구분코드를 입력하세요.");
                return false;
            }
            if (serviceNm.length == 0) {
                ktsGridAlert("서비스명을 입력하세요.");
                return false;
            }
            //if (serviceCd.length == 0) {
            //    ktsGridAlert("서비스코드를 입력하세요.");
            //    return false;
            //}
            if (productNm.length == 0) {
                ktsGridAlert("상품명을 입력하세요.");
                return false;
            }
            //if (productCd.length == 0) {
            //    ktsGridAlert("상품코드를 입력하세요.");
            //    return false;
            //}
            if (selUse.length == 0) {
                ktsGridAlert("변경하려는 사용을 선택하세요.");
                return false;
            }

            return true;
        }

        var fn_Class = function (gb, code) {
            callGb = gb;

            var gubun = '';

            if (gb == 'selProductgb') gubun = 'PRODUCTGB';
            else if (gb == 'selService') gubun = 'SERVICE';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = code;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUnionJsDll');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }


        var fn_Transaction = function (gubun) {
            //입력 파라미터
            var aData = [];
            aData[0] = 'PRODUCT';
            aData[1] = gubun;
            aData[2] = productgbCd;
            aData[3] = productgbNm;
            aData[4] = serviceGbCd;
            aData[5] = serviceCd;
            aData[6] = serviceNm;
            aData[7] = productCd;
            aData[8] = productNm;
            aData[9] = memo;
            aData[10] = useYn;
            aData[11] = '';
            aData[12] = '';
            aData[13] = '';
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selProductgb' || callGb == 'selService') {
                obj = $('#' + callGb);
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });

                if ($("#hdnStatus").val().length != 0) {

                    if ($("#hdnStatus").val() === 'M') {
                        $(".title strong").text("코드 변경");
                        fn_SetData();
                        if (callGb == 'selProductgb') obj.val($("#txtProductgbCd").val()).prop("selected", true);
                        else if (callGb == 'selService') obj.val($("#txtServiceCd").val()).prop("selected", true);
                    }
                }

                if (callGb == 'selProductgb') obj.val(obj.val()).trigger('change');
                else if (callGb == 'selService') {
                    if (obj.val() == '' || obj.val() == null) {
                        $('#selService option:first').prop('selected', true);
                    }
                }

            }
            else if (callGb == 'RegTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('등록되었습니다.');
                    check = 'ok';
                    setTimeout(function () {
                        SendParentsForm();
                    }, 1000);
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'ModTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('변경되었습니다.');
                    check = 'ok';
                    setTimeout(function () {
                        SendParentsForm();
                    }, 1000);
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
        }

        function fn_SetData() {
            $("#txtProductgbNm").val($("#hdnProductgbNm").val());
            $("#txtProductgbCd").val($("#hdnProductgbCd").val());
            $("#txtServiceGbCd").val($("#hdnServiceGbCd").val());
            $("#txtServiceNm").val($("#hdnServiceNm").val());
            $("#txtServiceCd").val($("#hdnServiceCd").val());
            $("#txtProductNm").val($("#hdnProductNm").val());
            $("#txtProductCd").val($("#hdnProductCd").val());
            $("#txtMemo").val($("#hdnMemo").val());
            $("#selUse").val($("#hdnUse").val()).prop("selected", true);
        }

        function SendParentsForm() {
            var cellNo = $("#hdnCellNo").val();

            opener.SetDispClass(check, cellNo, productgbCd, productgbNm, serviceCd, serviceNm, productCd, productNm, memo, useYn);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>코드 등록</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th id="thGubun" style="width: 120px;">상품구분선택</th>
                                            <td id="tdGubun" colspan="3" class="td-left">
                                                <p class="radiobox">
                                                    <label>
                                                        <input type="radio" name="gubun" value="ext" checked />기존
                                                    </label>
                                                     <label>
                                                        <input type="radio" name="gubun" value="new" />신규
                                                    </label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thProductgb" style="width: 120px;">상품구분</th>
                                            <td id="tdProductgb" colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selProductgb"></select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thProductgbNm" style="width: 120px;">상품구분명</th>
                                            <td id="tdProductgbNm" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductgbNm" maxlength="60" tabindex="1" style="width:400px;" placeholder="상품구분명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thProductgbCd" style="width: 120px;">상품구분코드</th>
                                            <td id="tdProductgbCd" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductgbCd" maxlength="7" tabindex="1" style="width:400px;" placeholder="상품구분코드 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thServiceGbCd" style="width: 120px;">서비스구분코드</th>
                                            <td id="tdServiceGbCd" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtServiceGbCd" maxlength="7" tabindex="1" style="width:400px;" placeholder="서비스구분코드 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thGubun2" style="width: 120px;">서비스선택</th>
                                            <td id="tdGubun2" colspan="3" class="td-left">
                                                <p class="radiobox">
                                                    <label>
                                                        <input type="radio" name="gubun2" value="ext" checked />기존
                                                    </label>
                                                     <label>
                                                        <input type="radio" name="gubun2" value="new" /><span id="lblGubun22">신규</span>
                                                    </label>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thService" style="width: 120px;">서비스</th>
                                            <td id="tdService" colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selService" style="width:246px"></select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thServiceNm" style="width: 120px;">서비스명</th>
                                            <td id="tdServiceNm" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtServiceNm" maxlength="60" tabindex="1" style="width:400px;" placeholder="서비스명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thServiceCd" style="width: 120px;">서비스코드</th>
                                            <td id="tdServiceCd" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtServiceCd" maxlength="7" tabindex="1" style="width:400px;" placeholder="서비스코드 자동채번" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductNm" maxlength="60" tabindex="1" style="width:400px;" placeholder="상품명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품코드</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtProductCd" maxlength="10" tabindex="1" style="width:400px;" placeholder="상품코드 자동채번" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 120px;">상품설명</th>
                                            <td colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtMemo" maxlength="132" tabindex="1" style="width:400px;" placeholder="상품설명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                       <tr>
                                            <th style="width: 120px;">사용</th>
                                            <td colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUse">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <input type="hidden" id="hdnCellNo" runat="server" />
                            <input type="hidden" id="hdnStatus" runat="server" />
                            <input type="hidden" id="hdnProductgbCd" runat="server" />
                            <input type="hidden" id="hdnProductgbNm" runat="server" />
                            <input type="hidden" id="hdnServiceGbCd" runat="server" />
                            <input type="hidden" id="hdnServiceNm" runat="server" />
                            <input type="hidden" id="hdnServiceCd" runat="server" />
                            <input type="hidden" id="hdnProductNm" runat="server" />
                            <input type="hidden" id="hdnProductCd" runat="server" />
                            <input type="hidden" id="hdnMemo" runat="server" />
                            <input type="hidden" id="hdnUse" runat="server" />
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			            
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>