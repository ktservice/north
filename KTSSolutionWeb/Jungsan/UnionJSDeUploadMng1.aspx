﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnionJSDeUploadMng1.aspx.cs" Inherits="KTSSolutionWeb.UnionJSDeUploadMng1" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .headerPay {
            background: #d7d4f073 !important;
        }

        .headerPolicy {
            background: #f0d4d473 !important;
        }
        
        .txtCell {
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
            text-align:right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        #progressBarContainer {
            position: fixed;
            display: flex;
            z-index: 99;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: #f9f9f9 !important;
            opacity: 0.8;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        #progressText {
            margin-top: 10px;
            font-weight: bold;
            text-align: center;
            color: #9c340a;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var closeDt1 = '';
        var closeDt2 = '';
        var dateNow;
        var today;
        var todayMon;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();

                        var etMonthValue = $('.etMonth').val();  // etMonth 텍스트박스 값 가져오기
                        var etParts = etMonthValue.split('-');   // etMonth 값을 '-'로 분할
                        var etYear = parseInt(etParts[0], 10);   // etMonth 연도 부분 정수로 변환
                        var etMonth = parseInt(etParts[1], 10);  // etMonth 월 부분 정수로 변환

                        if (etMonth === 1) {
                            etYear -= 1;   // etMonth의 월이 1이면 연도 1 감소
                            etMonth = 12;  // etMonth의 월을 12로 설정
                        } else {
                            etMonth -= 1;  // etMonth의 월을 1 감소
                        }

                        // 새로운 stMonth 값을 'YYYY-MM' 형식으로 설정
                        var newStMonthValue = etYear + '-' + (etMonth < 10 ? '0' + etMonth : etMonth);

                        $('.stMonth').val(newStMonthValue);  // stMonth 텍스트박스에 새로운 값 설정
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {
                    fn_Search('CHECK');
                });

                $("#btnExcel").click(function (e) {
                    fn_BatchExcelDown();
                });
                
                $("#gridExcel").click(function (e) {
                    fn_GridExcel('통합정산 대사우업로드1차관리', '통합정산 대사우업로드1차관리', 'xlsx');
                });

            }
        };

        var fn_Init = function () {
            today = new Date();
            dateNow = today.getDate();
            todayMon = today.getFullYear().toString() + ('0' + (today.getMonth() + 1)).slice(-2);


            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());


            $(".table-search").css('display', 'none');

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
            $("#tab02").hide();


            var formattedDate = today.getFullYear().toString() + '-' +
                ('0' + (today.getMonth() + 1)).slice(-2) + '월 ';

            $("#btnExcel").val(formattedDate + $("#btnExcel").val());

        }

        var fn_BatchExcelDown = function () {
            startProgress();
            fn_ExcelSearch('checkExcel');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원구분", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원번호", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "MOT<br>point", field: "MOT_POINT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본정책", field: "POLICY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "APT5만<br>Off_IP팩3만<br>대학전략동", field: "DE_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "IF단지4만<br>Bulk활성화<br>법인Bulk", field: "DE_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "결합", field: "DE_AMT3", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "SM패스이미징차감", field: "DE_AMT4", sorter: "number", width: 140, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "사은품초과등<br>기타차감", field: "DE_AMT5", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본장려금<br>기지급", field: "DE_AMT6", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "지급/미지급사유", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICEGBCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자명", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "SA_ID", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자ID", field: "RECEIPTID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자명", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "메가패스/<br>메가티비<br>약정기간", field: "PRODUCT_PERIOD", sorter: "string", width: 140 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

    
        var fn_Load = function () {
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pType: "jungsan"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function () {
            return true;
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var excelTitle = function (pTitle) {
            var title = '';
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2) +
                ('0' + today.getDate()).slice(-2);

            title += formattedDate + '_';
            title += pTitle;

            return title;
        }

        function getLastDayOfMonth(year, month) {
            return new Date(year, month, 0).getDate();
        }

        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });

            }
            else if (callGb == 'checkExcel') {
                callGb = '';

                var SheetNm1 = '';
                var SheetNm2 = '';

                SheetNm1 = '① 홈기본상품';
                SheetNm2 = '② 홈부가상품';
                SheetNm3 = '③ 무선상품';
                SheetNm4 = '④ 유지수수료';
                SheetNm5 = '⑤ 추가인센티브';

                var title = excelTitle('통합정산 대사우업로드1차');
                fn_excelDown('', json, SheetNm1, SheetNm2, SheetNm3, SheetNm4, SheetNm5, title, 'xlsx');
            }

        }

        var fn_GridExcel = function (sheetNm, fileNm, type) {

            // 그리드 데이터를 배열 형식으로 가져옴 (헤더 포함)
            var headers = ktsGrid.getColumns().map(col => col.getDefinition().title.replace(/<br>/g, "\n"));
            var data = ktsGrid.getData();

            // 그리드 데이터 변환
            var excelData = [
                headers,  // 첫 번째 행은 헤더
                ...data.map(row => [row.id, row.JS_MON, row.ORGNM1, row.ORGNM3, row.JOBNM, row.EMPNO, row.KTSEMPNO, row.EMPNM, row.MOT_POINT, row.TOT_JS_AMT, row.POLICY_AMT, row.MOT_PERFOR_AMT, row.DE_AMT1, row.DE_AMT2, row.DE_AMT3, row.DE_AMT4, row.DE_AMT5, row.DE_AMT6, row.POLICYNM, row.JSCD, row.SERVICEGBCD, row.RECEIPTDT, row.SERVICENM, row.SERVICENO, row.CUSTOMERNM, row.STATUS, row.GUBUN, row.ACCEPT_ORGNM, row.OPENDT, row.CANCELDT, row.BUY_ORGNM, row.SAID, row.RECEIPTID, row.BUY_EMPNM, row.KT_GIFT, row.PRODUCTNM, row.PRODUCT_PERIOD])  // 나머지 데이터
            ];

            // 엑셀 워크시트 생성
            var ws = XLSX.utils.aoa_to_sheet(excelData);

            // 헤더에 줄바꿈 스타일 적용
            headers.forEach((header, index) => {
                let cell = ws[XLSX.utils.encode_cell({ r: 0, c: index })]; // 헤더는 첫 번째 행(0번째)
                cell.s = {
                    font: { name: '맑은 고딕', bold: true, sz: 10, color: { rgb: "FFFFFF" } },
                    fill: { fgColor: { rgb: "3366cc" } },
                    alignment: { horizontal: "center", vertical: "center", wrapText: true },
                    border: {
                        top: { style: "thin", color: { rgb: "000000" } },
                        bottom: { style: "thin", color: { rgb: "000000" } },
                        left: { style: "thin", color: { rgb: "000000" } },
                        right: { style: "thin", color: { rgb: "000000" } }
                    } };
            });

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 8.89 },
                { wch: 9.89 },
                { wch: 13.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 10.89 },
                { wch: 10.89 },
                { wch: 10.89 },
                { wch: 10.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 24.89 },
                { wch: 14.89 },
                { wch: 24.89 },//
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 24.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 20.89 },
                { wch: 20.89 },
                { wch: 12.89 }
            ];

            // 엑셀 파일 생성 및 다운로드
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);
            XLSX.writeFile(wb, fileNm + '.' + type);
        }

        var fn_excelDown = function (gubun, json, sheetNm1, sheetNm2, sheetNm3, sheetNm4, sheetNm5, fileNm, type) {
            var headers;
            var data1;
            var data2;
            var data3;
            var data4;
            var data5;
            var ws1;
            var ws2;
            var ws3;
            var ws4;
            var ws5;

            headers = ['참조KEY', '본부', '지점', '사원구분', '사원번호', '실판매자', 'MOT Point', '정산총액', '기본정책', 'MOT실적급', 'APT5만\nOff_IP팩3만\n대학전략동', 'IF단지4만\nBulk활성화\n법인Bulk', '결합', 'SM패스\n이미징\n차감', '사은품초과등\n기타차감', '기본장려금\n기지급', '지급/미지급사유', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국\n(회선수)', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간'];

            var jsonData = JSON.parse(json);

            // Convert JSON data to array of arrays
            // data1 = jsonData.map(item => [item.REFKEY, item.ORGNM1, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.MOT_POINT, item.TOT_JS_AMT, item.POLICY_AMT, item.MOT_PERFOR_AMT, item.DE_AMT1, item.DE_AMT2, item.DE_AMT3, item.DE_AMT4, item.DE_AMT5, item.DE_AMT6, item.POLICYNM, item.JSCD, item.SERVICEGBCD, item.RECEIPTDT, item.SERVICENM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPTID, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTNM, item.PRODUCT_PERIOD]);
            data1 = jsonData.map(item => [
                item.REFKEY || null,  
                item.ORGNM1 || null,
                item.ORGNM3 || item.ORGNM1,
                item.JOBNM || null,
                item.KTSEMPNO || null,
                item.EMPNM || null,
                item.MOT_POINT || 0,
                item.TOT_JS_AMT || 0,
                item.POLICY_AMT || 0,
                item.MOT_PERFOR_AMT || 0,
                item.DE_AMT1 || null,
                item.DE_AMT2 || null,
                item.DE_AMT3 || null,
                item.DE_AMT4 || null,
                item.DE_AMT5 || null,
                item.DE_AMT6 || null,
                item.POLICYNM || null,
                item.JSCD || null,
                item.SERVICEGBCD || null,
                item.RECEIPTDT || null,
                item.SERVICENM || null,
                item.SERVICENO || null,
                item.CUSTOMERNM || null,
                item.STATUS || null,
                item.GUBUN || null,
                item.ACCEPT_ORGNM || null,
                item.OPENDT || null,
                item.CANCELDT || null,
                item.BUY_ORGNM || null,
                item.SAID || null,
                item.RECEIPTID || null,
                item.BUY_EMPNM || null,
                item.KT_GIFT || null,
                item.PRODUCTNM || null,
                item.PRODUCT_PERIOD || null
            ]);

            // Add headers as the first row
            data1.unshift(headers);

            // Convert array of arrays to worksheet
            ws1 = XLSX.utils.aoa_to_sheet(data1);

            // Add styles (example: set column width)
            ws1['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 26.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 14.89 },
                { wch: 18.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 26.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];


            ws1 = fn_ExcelHeaderStyle(1, ws1, headers);

            //0~6  7~20   21_ 26
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws1['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws1[cell_ref]) continue;

                    //if (R > 1)
                    if (R > 0) {
                        if (C == 7) {
                            ws1[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else if (C >= 8 && C <= 16) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 35) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }
            //, '본부', '지사', '지점'

            headers = ['참조KEY', '본부', '지점', '사원구분', '사원번호', '실판매자', 'MOT\npoint', '정산총액', '기본장려금', '프로모션\n(기전특별)', 'SM패스\n이미징\n차감', 'MOT실적급', '기지급\n장려금', '비  고', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간'];
            data2 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
            data2.unshift(headers);

            ws2 = XLSX.utils.aoa_to_sheet(data2);

            ws2['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            ws2 = fn_ExcelHeaderStyle(1, ws2, headers);

            headers = ['참조KEY', '본부', '지점', '직원구분', '사원번호', '실판매자', '지급액', '획득\nPoint', 'MOT실적급', '기본정책', '무선MOT\n인센티브\n(패드제외)', '추가장려금', 'ARPU UP\n주력', '특화모델', '주말MNP', '주말MNP\n추가', 'LG판매활성화1', 'LG판매활성화2', 'LG판매활성화3', 'LG판매활성화4', '재고소진', '전월무선결합\n수수료', '무선차감', '특이사항', '상품코드', '개통일', '가입자명', '서비스번호', '모델명', '가입유형', '요금제명', '인터넷계약ID', '유선고객명', '결합유형', '결합구성', '인터넷현상품', '인터넷접수일'];
            data3 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
            data3.unshift(headers);
            ws3 = XLSX.utils.aoa_to_sheet(data3);

            ws3['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];


            ws3 = fn_ExcelHeaderStyle(1, ws3, headers);

            headers = ['참조KEY', '본부', '지점', '사원구분', '사원번호', '실판매자', '지급액\n(유지수료*50%)', '유지수수료\n(수납액*지급율)', '지급율', '당월\n사용액', '수납액', '수납월', '비고', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국\n(회선수)', '개통일자', '해지일자', '판매자명', 'SA_ID', '접수자ID', '실판매자명', 'KT제공사은품', '상품명 상세내역', '메가패스/\n메가티비\n약정기간'];
            data4 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
            data4.unshift(headers);
            ws4 = XLSX.utils.aoa_to_sheet(data4);

            ws4['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 18.89 },
                { wch: 18.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            ws4 = fn_ExcelHeaderStyle(1, ws4, headers);

            headers = [
                ['지사지점', '사원번호', '판매자\n(계좌주)', '지급총액', '정산총액', '고성과인센티브', '', '', '', '', '', '홈기본상품', '', '', '', '', '', '홈부가상품', '', '', '', '', '', '무선상품', '', '유지수수료', '유지수수료', '재게', '', '추가분'],
                ['', '', '', '', '','Pnt', '인센티브', 'Pnt_B', '인센티브_B', 'Pnt_C', '인센티브_C', '인터넷', '인터넷', 'TV', 'TV', '음성', '음성', '기가전환', '기가전환', 'GiGA_WiFi', 'GiGA_WiFi', '기타상품', '기타상품', '', '', '50%지급', '50%지급', '실적급총액', '홈실적급', 'SM실적급']
            ];

            data5 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

            data5.unshift(headers[1]); // Add the second row of headers
            data5.unshift(headers[0]); // Add the first row of headers
            
            ws5 = XLSX.utils.aoa_to_sheet(data5);

            
          /*  ws5['!merges'] = [
                { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "지사지점"
                { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "사원번호"
                { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } }, // Merge "판매자"
                { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } }, // Merge "지급총액"
                { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } }, // Merge "정산총액"
                { s: { r: 0, c: 5 }, e: { r: 0, c: 10 } }, // Merge "고성과인센티브"
                { s: { r: 0, c: 11 }, e: { r: 0, c: 16 } }, // Merge "홈기본상품"
                { s: { r: 1, c: 11 }, e: { r: 1, c: 12 } }, // Merge "인터넷"
                { s: { r: 1, c: 13 }, e: { r: 1, c: 14 } }, // Merge "TV"
                { s: { r: 1, c: 15 }, e: { r: 1, c: 16 } }, // Merge "음성"
                { s: { r: 0, c: 17 }, e: { r: 0, c: 22 } }, // Merge "홈부가상품"
                { s: { r: 1, c: 17 }, e: { r: 1, c: 18 } }, // Merge "기가전환"
                { s: { r: 1, c: 19 }, e: { r: 1, c: 20 } }, // Merge "GiGA_WiFi"
                { s: { r: 1, c: 21 }, e: { r: 1, c: 22 } }, // Merge "기타상품"
                { s: { r: 0, c: 23 }, e: { r: 1, c: 24 } }, // Merge "무선상품"
                { s: { r: 0, c: 25 }, e: { r: 0, c: 26 } }, // Merge "유지수수료"
                { s: { r: 1, c: 25 }, e: { r: 1, c: 26 } }, // Merge "50%지급"
                { s: { r: 0, c: 27 }, e: { r: 0, c: 28 } } // Merge "재게"
            ]; 

            for (let c = 6; c <= 10; c++) { // c: 6 ~ c: 10 (병합 범위 내부 셀)
                cellAddress = XLSX.utils.encode_cell({ r: 0, c });
                delete ws5[cellAddress]; // 완전히 빈 값으로 설정
            }; */

            const mergeRanges = [
                { start: [0, 0], end: [1, 0] }, // "지사지점"
                { start: [0, 1], end: [1, 1] }, // "사원번호"
                { start: [0, 2], end: [1, 2] }, // "판매자"
                { start: [0, 3], end: [1, 3] }, // "지급총액"
                { start: [0, 4], end: [1, 4] }, // "정산총액"
                { start: [0, 5], end: [0, 10] }, // "고성과인센티브"
                { start: [0, 11], end: [0, 16] }, // "홈기본상품"
                { start: [1, 11], end: [1, 12] }, // "인터넷"
                { start: [1, 13], end: [1, 14] }, // "TV"
                { start: [1, 15], end: [1, 16] }, // "음성"
                { start: [0, 17], end: [0, 22] }, // "홈부가상품"
                { start: [1, 17], end: [1, 18] }, // "기가전환"
                { start: [1, 19], end: [1, 20] }, // "GiGA_WiFi"
                { start: [1, 21], end: [1, 22] }, // "기타상품"
                { start: [0, 23], end: [1, 24] }, // "무선상품"
                { start: [0, 25], end: [0, 26] }, // "유지수수료"
                { start: [1, 25], end: [1, 26] }, // "50%지급"
                { start: [0, 27], end: [0, 28] } // "재게"
            ];

            // 병합 범위를 엑셀 워크시트에 추가
            ws5['!merges'] = mergeRanges.map(range => ({
                s: { r: range.start[0], c: range.start[1] },
                e: { r: range.end[0], c: range.end[1] }
            }));

            // 병합 범위 내부 셀 비우기
            mergeRanges.forEach(range => {
                for (let r = range.start[0]; r <= range.end[0]; r++) {
                    for (let c = range.start[1]; c <= range.end[1]; c++) {
                        // 병합 범위의 첫 번째 셀은 유지
                        if (r === range.start[0] && c === range.start[1]) continue;

                        const cellAddress = XLSX.utils.encode_cell({ r, c });
                        delete ws5[cellAddress]; // 셀 삭제
                    }
                }
            });

            // Add styles (example: set column width)
            ws5['!cols'] = [
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];

            ws5 = fn_ExcelHeaderStyle(2, ws5, headers);


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws1, sheetNm1);
            XLSX.utils.book_append_sheet(wb, ws2, sheetNm2);
            XLSX.utils.book_append_sheet(wb, ws3, sheetNm3);
            XLSX.utils.book_append_sheet(wb, ws4, sheetNm4);
            XLSX.utils.book_append_sheet(wb, ws5, sheetNm5);

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

            endProgress('');
        }

        var fn_ExcelHeaderStyle = function (hRow, ws, headers)
        {
            // Style headers
            var headerStyle = {
                font: { name: '맑은 고딕', bold: true, sz: 10, color: { rgb: "FFFFFF" } },
                fill: { fgColor: { rgb: "3366cc" } },
                alignment: { horizontal: "center", vertical: "center", wrapText: true },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                },
            };

            if (hRow === 1) {
                // Apply styles to headers
                for (var C = 0; C < headers.length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }
            else {
                // Apply styles to headers
                for (var R = 0; R < headers.length; ++R) {
                    for (var C = 0; C < headers[R].length; ++C) {
                        var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                        if (!ws[cell_address]) continue;
                        ws[cell_address].s = headerStyle;
                    }
                }
            }

            return ws;
        }


        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'DEUPLOADMNG1';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ExcelSearch = function (gubun) {
            callGb = gubun;
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2);

            var pDate = formattedDate;

            if (gubun === 'checkExcel') {
                gubun = 'CHECK';
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pDate;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function (gubun) {
            if (fn_SearchChk()) {

                var pDate1 = $("#selMon").val().replace(/-/gi, "");

                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = pDate1;

                var jsonData = JSON.stringify({ aData: aData });

                if (gubun === 'CHECK') {
                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                    fn_GridInit();
                    ktsRowCountClass = 'searchrowcount';

                }
                
                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
                $(".table-option").css('display', 'block');
                $(".table-search").css('display', 'block');
            }
        }

        function endProgress(msg) {
            $("#progressBarSet").hide();

            if (msg != '') {
                alert(msg);
            }
        }

        function startProgress() {
            $("#progressBarSet").show();
            $("#progressText").html("잠시만 기다려주세요.");
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->

	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>정산월</label>
                <select id="selDt" style="width:80px;display:none;">
                    <option selected="selected" value="m">정산월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnExcel" type="button" class="btn-black last" style="float: right;margin-right:30px" value="대사우업로드1차내려받기" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                    <input id="hdnRm" type="hidden" runat="server" />
                    <input id="hdnAuthId" type="hidden" runat="server" />
                    <input id="hdnAuthNm" type="hidden" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <div class="list-top">
                <strong>대사우업로드1차 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                    </div>
                </div>
            </div>
            <div class="scrollbox">
                <div class="table-search" style="display: none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                            <option value="EMPNM">실판매자</option>
                        </select>
                        : 
                       
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
        </div>
        <!-- E:datalist -->
        <div id="progressBarSet" style="display:none">
            <div id="progressBarContainer">
            <img src="../Resource/images/icon_loadingbar.gif" />
            <h2>다운로드중...</h2>
                <div id="progressText"></div>
            </div>
        </div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>
