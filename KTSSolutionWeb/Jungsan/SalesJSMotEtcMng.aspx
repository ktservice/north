﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalesJSMotEtcMng.aspx.cs" Inherits="KTSSolutionWeb.SalesJSMotEtcMng" %>

<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"] {
            cursor: pointer;
        }

        .txtCell {
            height: 100%;
            text-align: center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor: pointer;
            text-align: right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align: center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();

                        var etMonthValue = $('.etMonth').val();  // etMonth 텍스트박스 값 가져오기
                        var etParts = etMonthValue.split('-');   // etMonth 값을 '-'로 분할
                        var etYear = parseInt(etParts[0], 10);   // etMonth 연도 부분 정수로 변환
                        var etMonth = parseInt(etParts[1], 10);  // etMonth 월 부분 정수로 변환

                        if (etMonth === 1) {
                            etYear -= 1;   // etMonth의 월이 1이면 연도 1 감소
                            etMonth = 12;  // etMonth의 월을 12로 설정
                        } else {
                            etMonth -= 1;  // etMonth의 월을 1 감소
                        }

                        // 새로운 stMonth 값을 'YYYY-MM' 형식으로 설정
                        var newStMonthValue = etYear + '-' + (etMonth < 10 ? '0' + etMonth : etMonth);

                        $('.stMonth').val(newStMonthValue);  // stMonth 텍스트박스에 새로운 값 설정
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnSelect").click(function (e) {
                    fn_Search('SEARCH');
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "영업정산 MOT기타상품관리.xlsx", { sheetName: "영업정산 MOT기타상품관리" });
                });

            }
        };

        var fn_Init = function () {
            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            $(".table-search").css('display', 'none');
            $(".stMonth").show();
            $(".etMonth").show();
            $("#lblDt").show();

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
            $("#tab02").hide();
        }

        var fn_CellInput = function (cell, formatterParams, onRendered) { //텍스트박스
            var cellValue = cell.getValue();
            var cellNo = cell.getRow().getData().id;
            var fieldNm = cell.getField();

            return fn_CellInputBind(cellNo, fieldNm, cellValue);
        }

        var fn_CellInputBind = function (cellNo, fieldNm, value) { //텍스트박스
            var html = '';

            if (fieldNm == 'SAID') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, SAIDTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='18' onchange='fn_MemoChg2(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'TOT_JS_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, TOT_JS_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'BASE_INCENTIVE_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, BASE_INCENTIVE_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'MOT_PERFOR_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, MOT_PERFOR_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'ETC_ADD_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, ETC_ADD_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }

            return html;
        }

        var fn_MemoChg = function (obj, cellNo, fieldNm) {
            var value = obj.value.replace(/[,]/g, '');
            obj.style.setProperty('background-color', '#cfd2d7', 'important');

            value = getNumberOrZero(value);

            // Remove all non-digit characters except for dot
            value = value.replace(/[^\d.]/g, '');

            // Restrict to two decimal places
            var parts = value.split('.');
            if (parts.length > 2) {
                parts = [parts[0], parts[1].substring(0, 2)];
            }
            if (parts[1] && parts[1].length > 2) {
                parts[1] = parts[1].substring(0, 2);
            }
            value = parts.join('.');

            // Add commas for every three digits before the decimal point
            if (parts[0].length > 3) {
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
            value = parts.join('.');

            // Set maximum length (e.g., 15 characters including commas and decimal point)
            var maxLength = 12;
            if (value.length > maxLength) {
                value = value.slice(0, maxLength);
            }

            // Update the input value
            obj.value = value;

            if (fieldNm == 'TOT_JS_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, TOT_JS_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'BASE_INCENTIVE_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, BASE_INCENTIVE_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'MOT_PERFOR_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, MOT_PERFOR_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'ETC_ADD_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, ETC_ADD_AMTTEXT: obj.value }]);
            }
        }

        var fn_MemoChg2 = function (obj, cellNo, fieldNm) {
            var value = obj.value.replace(/[,]/g, '');
            obj.style.setProperty('background-color', '#cfd2d7', 'important');

            // Update the input value
            obj.value = value;

            if (fieldNm == 'SAID') {
                ktsGrid.updateData([{ id: cellNo, SAIDTEXT: obj.value }]);
            }
        }

        //숫자 체크
        function getNumberOrZero(value) {
            // 입력 값이 숫자인지 확인합니다.
            if (typeof value === 'number' && !isNaN(value)) {
                return String(value);
            }
            // 입력 값이 문자열로 되어 있는지 확인하고, 숫자로 변환합니다.
            if (typeof value === 'string') {
                // 첫 번째 소수점만 남기고 이후의 값은 삭제합니다.
                var firstDecimalIndex = value.indexOf('.');
                if (firstDecimalIndex !== -1) {
                    var secondDecimalIndex = value.indexOf('.', firstDecimalIndex + 1);
                    if (secondDecimalIndex !== -1) {
                        value = value.substring(0, secondDecimalIndex);
                    }
                }

                var number = Number(value);
                if (!isNaN(number)) {
                    return String(number);
                }
            }
            // 위 조건을 모두 만족하지 않으면 0을 반환합니다.
            return '0';
        }

        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Mod(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";

            return html;
        }


        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수월", field: "RECEIPT_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "국사코드", field: "ORGCD4", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "국사", field: "ORGNM4", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "부서명", field: "ORGNM", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM2", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                {
                    title: '정산내역', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "정산총액입력값", field: "TOT_JS_AMTTEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "기본장려금", field: "BASE_INCENTIVE_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "기본장려금입력값", field: "BASE_INCENTIVE_AMTTEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "MOT실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "MOT실적급입력값", field: "MOT_PERFOR_AMTTEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "right", title: "기타추가", field: "ETC_ADD_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "right", title: "기타추가입력값", field: "ETC_ADD_AMTTEXT", sorter: "number", width: 120, visible: false },
                    ]
                },
                {
                    title: '정책', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "비고(적용정책)", field: "POLICYNM", sorter: "string", width: 200 },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                {
                    title: '계약내역', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)", field: "SAID", sorter: "string", width: 180, formatter: fn_CellInput },
                        { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)입력값", field: "SAIDTEXT", sorter: "number", width: 120, visible: false },
                        { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 150 },
                        { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                        { headerHozAlign: "center", hozAlign: "center", title: "담당자", field: "BUY_EMPNM", sorter: "string", width: 150 },
                        { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                        { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "PRODUCTCD", sorter: "string", width: 110 },
                        { headerHozAlign: "center", hozAlign: "left", title: "상품명/지급명", field: "PRODUCTNM", sorter: "string", width: 240 },
                        { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTDESC", sorter: "string", width: 240 },
                        { headerHozAlign: "center", hozAlign: "center", title: "약정", field: "PRODUCT_PERIOD", sorter: "string", width: 80 },
                        { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200, visible: false },
                { formatter: fn_CellReg, width: 150, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_PopupUpload = function () {
            var form = "/Jungsan/SalesJSMotEtcMngUploadPopup";
            var target = "SalesJSMotEtcMngUploadPopup";
            var param = {
            };
            var nWidth = 892;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pType: "jungsan"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }


        var fn_SearchChk = function () {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val();
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }
            if (OrgCd.length == 0) {
                ktsGridAlert("조회할 조직을 선택해 주세요.");
                return false;
            } else {
                return true;
            }
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var ajaxCallBack = function (json) {
            var obj;
            var callGbTemp = '';
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'delTran' || callGb == 'delTran2') {
                callGbTemp = callGb;
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    if (callGbTemp == 'delTran') {
                        ktsGridId = 'ktsGrid';
                        ktsGridAlert("삭제를 완료했습니다.", 1000);
                        setTimeout(function () {
                            fn_Search('SEARCH');
                        }, 1000); // 1000 밀리초(1초) 대기
                    }
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'modTran') {
                callGbTemp = callGb;
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    if (callGbTemp == 'modTran') {
                        ktsGridId = 'ktsGrid';
                        ktsGridAlert('변경되었습니다.', 1000);
                        setTimeout(function () {
                            fn_Search('SEARCH');
                        }, 1000); // 1000 밀리초(1초) 대기
                    }
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'MOTMNG';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Mod = function (id) {
            if (confirm("변경 하시겠습니까?")) {
                callGb = 'modTran';
                fn_Transaction('MOT', 'M', id);
            }
        }

        var fn_Del = function (id) {
            if (confirm("삭제 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('MOT', 'D', id);
            }
        }

        var fn_Transaction = function (target, jobType, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var said = rowData.SAID;
            var gubun = rowData.GUBUN;
            var productNm = rowData.PRODUCTNM;
            var productDesc = rowData.PRODUCTDESC;
            var saidChg = rowData.SAIDTEXT;

            var totJsAmt = rowData.TOT_JS_AMTTEXT;
            var baseIncentiveAmt = rowData.BASE_INCENTIVE_AMTTEXT;
            var motPerforAmt = rowData.MOT_PERFOR_AMTTEXT;
            var etcAddAmt = rowData.ETC_ADD_AMTTEXT;


            //입력 파라미터
            var aData = [];
            aData[0] = target;
            aData[1] = jobType;
            aData[2] = said;
            aData[3] = gubun;
            aData[4] = productNm;
            aData[5] = productDesc;
            aData[6] = saidChg;
            aData[7] = totJsAmt.replace(/,/g, "");
            aData[8] = baseIncentiveAmt.replace(/,/g, "");
            aData[9] = motPerforAmt.replace(/,/g, "");
            aData[10] = etcAddAmt.replace(/,/g, "");

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function (gubun) {
            if (fn_SearchChk()) {
                var pDate1 = '';
                var pDate2 = '';
                var pDateGb = $("#selGb").val();

                if ($("#selDt").val() == 'm') {
                    pDate1 = $("#selMon").val();
                    pDate2 = $("#selMon").val();
                }
                else {
                    pDate1 = $(".stMonth").val();
                    pDate2 = $(".etMonth").val();
                }

                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = pDateGb;
                aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");
                aData[3] = pDate1;
                aData[4] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
                $(".table-option").css('display', 'block');
                $(".table-search").css('display', 'block');
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
    <div class="pagenavi">
        <ul>
            <li><a id="aMenuTree1" runat="server"></a></li>
            <li class="last"><a id="aMenuTree2" runat="server"></a></li>
            <!-- 현재 페이지에 last클래스 추가 -->
        </ul>
    </div>
    <!--end subTitle-->

    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S:searchbox -->
        <div class="searchbox" style="margin-top: 0;">
            <fieldset>
                <span class="optionbox">
                    <label>구분</label>
                    <select id="selGb" style="width: 70px">
                        <option selected="selected" value="J">정산</option>
                        <option value="R">접수</option>
                    </select>
                    <select id="selDt" style="width: 70px">
                        <option selected="selected" value="m">월</option>
                        <option value="b">범위</option>
                    </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" Style="width: 30% !important;"></asp:TextBox>
                    <label id="lblDt" style="width: 10px">~</label>
                    <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" Style="width: 30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width: 110px"></select>
                    </span>
                </span>
                <span class="inpbox first" style="margin-right: 20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged" />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged" />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnUpload" type="button" class="btn-green last" style="float: right;" value="일괄등록" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                </div>
            </fieldset>
        </div>
        <!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <div class="list-top">
                <strong>MOT기타상품 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                    </div>
                </div>
            </div>
            <div class="scrollbox">
                <div class="table-search" style="display: none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="RECEIPT_MON">접수월</option>
                            <option value="ORGNM1">본부</option>
                            <option value="ORGNM2">지사</option>
                            <option value="ORGNM3">지점</option>
                            <option value="ORGNM4">국사</option>
                            <option value="EMPNM">실판매자명</option>
                        </select>
                        : 
                       
                        <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>
