﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnionJSCloseInfoMng.aspx.cs" Inherits="KTSSolutionWeb.UnionJSCloseInfoMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .line {
            border-bottom: 2px solid #00000082;
         }

        .searchbox {
            margin-top:0px !important;
            height:92px !important;
        }
        .tabcontent {
          display: inline;
        }

        .inpbox input {
            display: inline-block;
            height: 40px;
            width: 100%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 38px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }


        .inpbox textarea {
            display: inline-block;
            height: 100%;
            width: 100%;
            padding: 0 14px;
            border: 1px solid #dfdfdf;
            border-radius: 2px;
            line-height: 38px;
            vertical-align: middle;
            color: #000;
            font-size: 15px;
        }

        .searchbox {
            height:150px;
        }

        .plusicon {
            background:#fff url('/Resource/Mobile_images/icon_plus_01.png')no-repeat 50%;
            width:11px;
            height:11px;
        }
        .btn-gray
        {
            min-width:60px;
            padding:0px;
        }

        .thisEndDt{
            color: #be1a1a !important;
            margin-right: 40px !important;
        }

        .thisEndBack{
            background-color: #8b8b8b52;
        }
        
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var title = '';
        var closeDt1 = '';
        var closeDt2 = '';

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {

                $('#selCloseDt1').on('change', function () {
                    fn_thisCloseDt('1', $(this).val());
                });

                $('#selCloseDt2').on('change', function () {
                    fn_thisCloseDt('2', $(this).val());
                });

                $("#btnSave1").click(function (e) {
                    fn_ModTran();
                });
            }
        };

        var fn_Load = function () {
            fn_CloseDtBind('selCloseDt1');
            fn_CloseDtBind('selCloseDt2');

            fn_CloseInfo();
        }

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
        }

        function getLastDayOfMonth(year, month) {
            return new Date(year, month, 0).getDate();
        }

        var fn_thisCloseDt = function (gubun, closeDt) {
            var endMsg = '';
            var today = new Date();
            var endDay = ('0' + closeDt).slice(-2);
            var lastDay = getLastDayOfMonth(today.getFullYear(), today.getMonth() + 1);
            var endMon = today.getFullYear().toString() + '-' + ('0' + (today.getMonth() + 1)).slice(-2);

            if (closeDt > lastDay) {
                endDay = lastDay;
            }

            //endMsg = (today.getMonth() + 1) + '월 마감일';
            //$("#thCloseDt").html(endMsg);

            if (gubun === '1') {
                endMsg = endMon + '-' + endDay;
                $("#lblCloseDt1").html(endMsg);
            }
            else if (gubun === '2') {
                endMsg = endMon + '-' + endDay;
                $("#lblCloseDt2").html(endMsg);
            }
        }

        var fn_CloseDtBind = function (id) {
            obj = $('#' + id); //selCloseDt1
            obj.empty();

            for (var i = 1; i <= 31; i++) {
                obj.append("<option value='" + i + "'>" + i + "</option>");
            }
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "제목", field: "TITLE", sorter: "string", width: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "1차마감일(매월)", field: "CLOSEDT1", sorter: "number", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "2차마감일(매월)", field: "CLOSEDT2", sorter: "number", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "REGDT", sorter: "string", width: 200},
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
        }


        var fn_ModTran = function () {
            if (fn_RegChk()) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegChk = function (gubun) {
            title = '';
            closeDt1 = '';
            closeDt2 = '';

            closeDt1 = $('#selCloseDt1').val();
            closeDt2 = $('#selCloseDt2').val();

            title = $("#txtCloseTitle").val();

            if (title.length == 0) {
                ktsGridAlert("제목을 입력하세요.");
                return false;
            }

            return true;
        }


        var fn_Transaction = function (gubun) {
            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = title;
            aData[2] = closeDt1;
            aData[3] = closeDt2;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetCloseInfoTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_CloseInfo = function () {
            callGb = 'SEARCH';

            //입력 파라미터
            var aData = [];
            aData[0] = callGb;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCloseInfo');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Hist = function () {
            callGb = 'HIST';

            //입력 파라미터
            var aData = [];
            aData[0] = callGb;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = grid.getUrl(location.href, 'GetCloseInfo');

            ktsRowCountClass = 'searchrowcount';
            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        }

        var ajaxCallBack = function (json) {

            if (callGb == 'SEARCH') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    title = item.TITLE;
                    closeDt1 = item.CLOSEDT1;
                    closeDt2 = item.CLOSEDT2;
                });

                $('#txtCloseTitle').val(title);
                $('#selCloseDt1').val(closeDt1);
                $('#selCloseDt2').val(closeDt2);

                fn_thisCloseDt('1', closeDt1);
                fn_thisCloseDt('2', closeDt2);

                fn_Hist();
            }
            else if (callGb == 'ModTran') {

                $.each(JSON.parse(json), function (idx, item) {
                    if (item.ERRCD == '00') {
                        alert('변경되었습니다');
                    }
                });

                callGb = '';

                fn_Hist();
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
    <div class="contentsarea">
    </div>
    <div class="datalist">
        <div class="list-top">
            <strong>마감관리</strong>
        </div>
        <div class="scrollbox">
            <table>
                <tbody>
                    <tr class="thisEndBack">
                        <th id="thCloseDt" style="width: 120px;">이번달 마감일</th>
                        <td class="td-left">
                            <p class="inpbox">
                                <label id="lblClose1">1차 마감일 : </label>
                                <label id="lblCloseDt1" class="thisEndDt"></label>
                                <label id="lblClose2">2차 마감일 : </label>
                                <label id="lblCloseDt2" class="thisEndDt"></label>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 120px;">마감일 제목</th>
                        <td class="td-left">
                            <p class="inpbox">
                                <input id="txtCloseTitle" style="width:600px;" type="text" maxlength="50" placeholder="마감일 제목 입력" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 120px;">1차 마감일</th>
                        <td class="td-left">
                            <p class="optionbox">매월
                                <select id="selCloseDt1" style="width:60px"></select>
                                일 까지
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 120px;">2차 마감일</th>
                        <td class="td-left">
                            <p class="optionbox">매월
                                <select id="selCloseDt2" style="width:60px"></select>
                                일 까지
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input id="btnSave1" type="button" class="btn-save" value="저장" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="margin-bottom: 20px;"></div>
        <!-- S:datalist -->
        <div class="datalist">
            <div class="list-top">
                <strong>변경이력</strong>
                <label class="searchrowcount"></label>
            </div>
            <div class="scrollbox">
                <div id="ktsGrid"></div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
