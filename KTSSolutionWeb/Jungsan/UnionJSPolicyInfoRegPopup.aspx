﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnionJSPolicyInfoRegPopup.aspx.cs" Inherits="KTSSolutionWeb.UnionJSPolicyInfoRegPopup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>코드등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .td-left {
          text-align: left !important;
        }

        .text-number {
          text-align: right !important;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var callGb;
        var check = '';
        var policyCd = '';
        var policyNm = '';
        var useYn = '';

        var page = {
            init: function () {
                fn_Init();
                fn_Load();
            },
            eventbind: function () {
                $("#btnReg").click(function (e) {
                    fn_RegTran();
                });

                $("#btnMod").click(function (e) {
                    fn_ModTran();
                });

                $("#btnDel").click(function (e) {
                    fn_DelTran();
                });

                $("#btnClose").click(function (e) {
                    window.close();
                });
            }
        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';

            var obj1 = $('#selUse');
            var obj2 = $('#selUseM');
            obj1.empty();
            obj1.append("<option value='Y'>Y</option>");
            obj1.append("<option value='N'>N</option>");

            obj2.empty();
            obj2.append("<option value='Y'>Y</option>");
            obj2.append("<option value='N'>N</option>");

            $("#txtPolicyCd").attr('readonly', true);
            $("#txtPolicyCd").attr('style', 'background: #d1d1d1 !important;width: 400px');

            if ($("#hdnStatus").val() === 'M') {
                $("#txtPolicyCdM").attr('readonly', true);
                $("#txtPolicyCdM").attr('style', 'background: #d1d1d1 !important;width: 400px');
            }
        }

        var fn_Load = function () {
            if ($("#hdnStatus").val() === 'M') {
                fn_SetData();
                fn_Class($("#hdnPolicyCd").val());
            }
        }

        var fn_DelTran = function () {
            if (confirm("삭제를 진행하시겠습니까?")) {
                callGb = 'DelTran';

                fn_Transaction('D');
            }
        }

        var fn_RegTran = function () {
            if (fn_RegChk('I')) {
                if (confirm("등록을 진행하시겠습니까?")) {
                    callGb = 'RegTran';

                    fn_Transaction('I');
                }
            }
        }

        var fn_ModTran = function () {
            if (fn_RegChk('M')) {
                if (confirm("변경을 진행하시겠습니까?")) {
                    callGb = 'ModTran';

                    fn_Transaction('M');
                }
            }
        }

        var fn_RegChk = function (gubun) {

            if (gubun == 'I') {
                policyCd = $("#txtPolicyCd").val();
                policyNm = $("#txtPolicyNm").val();
                useYn = $("#selUse").val();
            }
            else {
                policyCd = $("#txtPolicyCdM").val();
                policyNm = $("#txtPolicyNmM").val();
                useYn = $("#selUseM").val();
            }

            if (policyNm.length == 0) {
                ktsGridAlert("정책명을 입력하세요.");
                return false;
            }
            //if (policyCd.length == 0) {
            //    ktsGridAlert("정책코드를 입력하세요.");
            //    return false;
            //}
            
            if (selUse.length == 0) {
                ktsGridAlert("변경하려는 사용을 선택하세요.");
                return false;
            }

            return true;
        }

        var fn_Class = function (code) {
            callGb = 'GetPolicy';

            var gubun = 'POLICY';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = code;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetUnionJsDll');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (gubun) {

            if (gubun == 'D') {

                policyCd = $("#hdnPolicyCd").val();
                policyNm = '';
                useYn = '';
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = policyCd;
            aData[2] = policyNm;
            aData[3] = useYn;
            
            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'GetPolicy') {
                var useYn = '';

                $.each(JSON.parse(json), function (idx, item) {
                    $("#txtPolicyNmM").val(item.NM);
                    $("#txtPolicyCdM").val(item.CD);
                    useYn = item.USEYN;
                });

                $('#selUseM').val(useYn).prop("selected", true);

            }
            else if (callGb == 'RegTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('등록되었습니다.');
                    check = 'ok';
                    setTimeout(function () {
                        SendParentsForm();
                    }, 1000);
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'ModTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('변경되었습니다.');
                    check = 'ok';
                    setTimeout(function () {
                        SendParentsForm();
                    }, 1000);
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'DelTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('삭제되었습니다.');
                    check = 'ok';
                    setTimeout(function () {
                        SendParentsForm();
                    }, 1000);
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
        }

        function fn_SetData() {
        }

        function SendParentsForm() {
            opener.SetDispClass2(check, policyCd);
            window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="windowpop-wrap">   
            <!-- S:pop-user-registration -->
            <div class="windowpop pop-user-registration">      
		        <div class="popupwrap">
			        <div class="title"><strong>코드 등록</strong></div>			        
			        <button type="button" class="btn-popclose" onclick="window.close(self)"></button>

			        <div class="popcontents">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <table id="tblDel">
                                    <tbody>
                                        <tr>
                                            <th id="thPolicyNmM" style="width: 120px;">정책명</th>
                                            <td id="tdPolicyNmM" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtPolicyNmM" maxlength="60" tabindex="1" style="width:400px;" placeholder="정책명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thPolicyCdM" style="width: 120px;">정책코드</th>
                                            <td id="tdPolicyCdM" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtPolicyCdM" maxlength="7" tabindex="1" style="width:400px;" placeholder="정책코드 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                       <tr>
                                            <th style="width: 120px;">사용</th>
                                            <td colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUseM">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnMod" type="button" class="btn-save" value="변경" />
                                                <input id="btnDel" type="button" class="btn-del" value="삭제" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="tblReg">
                                    <tbody>
                                        <tr>
                                            <th id="thPolicyNm" style="width: 120px;">정책명</th>
                                            <td id="tdPolicyNm" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtPolicyNm" maxlength="60" tabindex="1" style="width:400px;" placeholder="정책명 입력" />
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th id="thPolicyCd" style="width: 120px;">정책코드</th>
                                            <td id="tdPolicyCd" colspan="3" class="td-left">
                                                <p class="inpbox">
                                                    <input type="text" id="txtPolicyCd" maxlength="7" tabindex="1" style="width:400px;" placeholder="정책코드 자동채번" />
                                                </p>
                                            </td>
                                        </tr>
                                       <tr>
                                            <th style="width: 120px;">사용</th>
                                            <td colspan="3" class="td-left">
                                                <span class="optionbox">
                                                    <select id="selUse">
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <input id="btnReg" type="button" class="btn-save" value="등록" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- S:btncenter -->
                        <div style="margin: 20px 30px 0 30px; display: grid;">
                            <input type="hidden" id="hdnStatus" runat="server" />
                            <input type="hidden" id="hdnPolicyCd" runat="server" />
                            <button id="btnClose" type="button" class="btn-green">닫기</button>
                        </div>
                        <!-- //E:btncenter -->
                    </div>
                </div>
            </div>
			<div class="list-top">
				<div style="padding-left:210px">
			        <div class="btnset">
			        </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>