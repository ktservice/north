﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BizJSCablePayUploadPopup.aspx.cs" Inherits="KTSSolutionWeb.BizJSCablePayUploadPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>유선상품기지급내역일괄등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-error {
          border: 4px solid #D00;
          color: #590000;
        }

        .btn-red {
            display: inline-block;
            min-width: 80px;
            height: 34px;
            padding: 0 15px;
            background: #ed1c24;
            text-align: center;
            line-height: 34px;
            border-radius: 2px;
            color: #fff;
            font-size: 14px;
            font-weight: 800;
            border-style: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var page = {
                init: function () {
                    fn_Init();
                },
                eventbind: function () {
                    $("#btnModTemp").click(function (e) {
                        fn_ModExcel();
                    });

                    $("#btnNewTemp").click(function (e) {
                        fn_NewExcel();
                    });

                    $("#btnNotTemp").click(function (e) {
                        fn_NotExcel();
                    });

                    $("#btnDel").click(function (e) {
                        fn_Del();
                    });

                    $("#btnClose").click(function (e) {
                        SendParentsForm();
                    });
                }
            };

        });

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
            fn_Month();
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'CABLEMNG';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Del = function () {
            var target = 'CABLE_PAY';
            var gubun = $("#selGb").val();
            var moncd = $("#selMon").val();
            var msg = '';

            msg += $("#selGb option:selected").text() + ' ' + $("#selMon").val() + '월';
            msg += ' Data를 삭제 하시겠습니까?';

            if (confirm(msg)) {
                callGb = 'delTran';
                fn_Transaction(target, gubun, moncd);
            }
        }

        var fn_Transaction = function (target, gubun, moncd) {

            //입력 파라미터
            var aData = [];
            aData[0] = target;
            aData[1] = gubun;
            aData[2] = moncd;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_NewExcel = function () {
            callGb = 'newExcel';

            var pDateGb = $("#selGb").val();

            //입력 파라미터
            var aData = [];
            aData[0] = 'PAY_NEWEXCEL';
            aData[1] = pDateGb;
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ModExcel = function () {
            callGb = 'modExcel';

            var pDate = $("#selMon").val();
            var pDateGb = $("#selGb").val();

            //입력 파라미터
            var aData = [];
            aData[0] = 'PAY_MODEXCEL';
            aData[1] = pDateGb;
            aData[2] = '';
            aData[3] = pDate;
            aData[4] = pDate;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_NotExcel = function () {
            callGb = 'notExcel';
            var pDateGb = $("#selGb").val();

            //입력 파라미터
            var aData = [];
            aData[0] = 'PAY_NOTEXCEL';
            aData[1] = pDateGb;
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_excelDown = function (gubun, json, sheetNm, fileNm, type) {
            var headers;

            // Custom headers mapping   
            if (gubun === 'not') {
                headers = [
                    ['사원번호', '실판매자', '계약내역', '계약내역', '계약내역', '장려금', '장려금', '장려금', '장려금', '실적급', '실적급', '실적급', '실적급', '결합', '결합', '결합', '결합', 'KT현장', 'KT현장', 'KT현장', 'KT현장', '오류메시지', '발생일'],
                    ['', '', '계약번호(SAID)', '구분', '접수일', '일자', '1수수료', '일자', '2수수료', '일자', '실적급', '일자', '실적급', '일자', '1결합장려', '일자', '1결합장려', '일자', '1KT현장', '일자', '2KT현장', '', '']
                ];
            }
            else {
                headers = [
                    ['사원번호', '실판매자', '계약내역', '계약내역', '계약내역', '장려금', '장려금', '장려금', '장려금', '실적급', '실적급', '실적급', '실적급', '결합', '결합', '결합', '결합', 'KT현장', 'KT현장', 'KT현장', 'KT현장'],
                    ['', '', '계약번호(SAID)', '구분', '접수일', '일자', '1수수료', '일자', '2수수료', '일자', '실적급', '일자', '실적급', '일자', '1결합장려', '일자', '1결합장려', '일자', '1KT현장', '일자', '2KT현장']
                ];
            }

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            if (gubun === 'not') {
                data = jsonData.map(item => [item.EMPNO, item.EMPNM, item.SAID, item.GUBUN, item.RECEIPTDT, item.INCENTIVEDT1, item.INCENTIVE_AMT1, item.INCENTIVEDT2, item.INCENTIVE_AMT2, item.PERFORDT1, item.PERFOR_AMT1, item.PERFORDT2, item.PERFOR_AMT2, item.COMBINEDDT1, item.COMBINED_AMT1, item.COMBINEDDT2, item.COMBINED_AMT2, item.KTDT1, item.KT_AMT1, item.KTDT2, item.KT_AMT2, item.EMSG, item.DBCREATEDT]);
            }
            else {
                data = jsonData.map(item => [item.EMPNO, item.EMPNM, item.SAID, item.GUBUN, item.RECEIPTDT, item.INCENTIVEDT1, item.INCENTIVE_AMT1, item.INCENTIVEDT2, item.INCENTIVE_AMT2, item.PERFORDT1, item.PERFOR_AMT1, item.PERFORDT2, item.PERFOR_AMT2, item.COMBINEDDT1, item.COMBINED_AMT1, item.COMBINEDDT2, item.COMBINED_AMT2, item.KTDT1, item.KT_AMT1, item.KTDT2, item.KT_AMT2]);
            }

            // Add headers as the first row
            //data.unshift(headers);
            data.unshift(headers[1]); // Add the second row of headers
            data.unshift(headers[0]); // Add the first row of headers

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            if (gubun === 'not') {
                ws['!merges'] = [
                    { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "사원번호"
                    { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "실판매자"
                    { s: { r: 0, c: 2 }, e: { r: 0, c: 4 } }, // Merge "계약내역"
                    { s: { r: 0, c: 5 }, e: { r: 0, c: 8 } }, // Merge "장려금"
                    { s: { r: 0, c: 9 }, e: { r: 0, c: 12 } }, // Merge "실적급"
                    { s: { r: 0, c: 13 }, e: { r: 0, c: 16 } }, // Merge "결합"
                    { s: { r: 0, c: 17 }, e: { r: 0, c: 20 } }, // Merge "KT현장"
                    { s: { r: 0, c: 21 }, e: { r: 1, c: 21 } }, // Merge "오류메시지"
                    { s: { r: 0, c: 22 }, e: { r: 1, c: 22 } } // Merge "발생일"
                ];

                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 12.89 },
                    { wch: 6.78 },
                    { wch: 10.89 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 19.89 },
                    { wch: 10.89 }
                ];
            }
            else {
                ws['!merges'] = [
                    { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "사원번호"
                    { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "실판매자"
                    { s: { r: 0, c: 2 }, e: { r: 0, c: 4 } }, // Merge "계약내역"
                    { s: { r: 0, c: 5 }, e: { r: 0, c: 8 } }, // Merge "장려금"
                    { s: { r: 0, c: 9 }, e: { r: 0, c: 12 } }, // Merge "실적급"
                    { s: { r: 0, c: 13 }, e: { r: 0, c: 16 } }, // Merge "결합"
                    { s: { r: 0, c: 17 }, e: { r: 0, c: 20 } } // Merge "KT현장"
                ];

                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 12.89 },
                    { wch: 6.78 },
                    { wch: 10.89 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 9.67 }
                ];
            }

            if (gubun === 'mod') {
                if (!ws.A1.c) ws.A3.c = [];
                ws.A3.c.push({ a: "사원번호", t: "업로드 제외 DATA는\n행을 삭제하시면 됩니다." });
            }

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            // Apply styles to headers
            for (var R = 0; R < headers.length; ++R) {
                for (var C = 0; C < headers[R].length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 1) {
                        if (C >= 0 && C <= 3) {
                            if (gubun == 'mod') {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    },
                                    fill: { fgColor: { rgb: "A6A6A6" } }
                                };
                            }
                            else {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    }
                                };
                            }
                        }
                        else if (C == 6 || C == 8 || C == 10 || C == 12 || C == 14 || C == 16 || C == 18 || C == 20) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }

                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

        }

        var ajaxCallBack = function (json) {
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'newExcel') {
                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                }
                else {
                    fn_excelDown('new', json, 'Sheet1', 'Biz정산 유선상품기지급내역 신규양식 일괄등록 Template', 'xlsx');
                }
            }
            else if (callGb == 'modExcel') {

                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert($("#selMon").val() + "월 Data가 존재하지 않아 변경할 수 없습니다.");
                }
                else {
                    fn_excelDown('mod', json, 'Sheet1', 'Biz정산 유선상품기지급내역 변경양식 일괄등록 Template', 'xlsx');
                }
            }
            else if (callGb == 'notExcel') {

                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                }
                else {
                    fn_excelDown('not', json, 'Sheet1', 'Biz정산 유선상품기지급내역 미등록', 'xlsx');
                }
            }
            else if (callGb == 'delTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('삭제되었습니다.');
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <div id="msg" class="tabulator"></div>
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">Biz정산 일괄등록</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <label class="searchrowcount"></label>
                            <div class="pull-right">
                                <div class="btnset">
                                    <span class="inpbox">
                                        <label>구분</label>
                                        <span class="optionbox">
                                            <select id="selGb" style="width:80px">
                                                <option selected="selected" value="J">정산월</option>
                                                <option value="R">접수월</option>
                                            </select>
                                            <select id="selMon" style="width: 110px"></select>
                                        </span>
                                    </span>
                                    <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" type="button" style="float: right;margin: 0px 10px" title="자료등록" class="btn-green last">자료등록</button>
                                    <input id="btnDel" type="button" class="btn-red last" style="float: right;margin: 0px 10px" value="월Data삭제" />
                                    <input id="btnNotTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="미등록다운로드" />
                                    <input id="btnNewTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="신규양식다운로드" />
                                    <input id="btnModTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="변경양식다운로드" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div class="upload-list" style="vertical-align:text-bottom;margin: 50px 0 0 0;">
                            <ul>
                                <li>
                                    <strong style="padding:10px 0px;float:left;margin-left:32px">파일</strong>
                                    <div class="file-field">
                                        <p class="filebox">
                                            <input type="text" id="txtfuReg" style="width:600px" />
                                            <span class="file">
                                                <input type="file" id="fuReg" name="fuReg" onchange="document.getElementById('txtfuReg').value=this.value;" />
                                                <label for="fuReg">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
                                            </span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                         </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUpload" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin: 120px 30px 0 30px; display: grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>