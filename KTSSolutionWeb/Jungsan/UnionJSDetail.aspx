﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnionJSDetail.aspx.cs" Inherits="KTSSolutionWeb.UnionJSDetail" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .headerPay {
            background: #d7d4f073 !important;
        }

        .headerPolicy {
            background: #f0d4d473 !important;
        }
        
        .txtCell {
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
            text-align:right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        #progressBarSet {
            padding:25px;
        }
        
        #progressBarContainer {
            position: fixed;
            display: flex;
            z-index: 99;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: #f9f9f9 !important;
            opacity: 0.8;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        #progressText {
            margin-top: 10px;
            font-weight: bold;
            text-align: center;
            color: #9c340a;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var closeDt1 = '';
        var closeDt2 = '';
        var dateNow;
        var today;
        var todayMon;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();

                        var etMonthValue = $('.etMonth').val();  // etMonth 텍스트박스 값 가져오기
                        var etParts = etMonthValue.split('-');   // etMonth 값을 '-'로 분할
                        var etYear = parseInt(etParts[0], 10);   // etMonth 연도 부분 정수로 변환
                        var etMonth = parseInt(etParts[1], 10);  // etMonth 월 부분 정수로 변환

                        if (etMonth === 1) {
                            etYear -= 1;   // etMonth의 월이 1이면 연도 1 감소
                            etMonth = 12;  // etMonth의 월을 12로 설정
                        } else {
                            etMonth -= 1;  // etMonth의 월을 1 감소
                        }

                        // 새로운 stMonth 값을 'YYYY-MM' 형식으로 설정
                        var newStMonthValue = etYear + '-' + (etMonth < 10 ? '0' + etMonth : etMonth);

                        $('.stMonth').val(newStMonthValue);  // stMonth 텍스트박스에 새로운 값 설정
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field('selField', 'inputValue');
                });

                $("#selField2").change(function (e) {
                    fn_Field('selField2', 'inputValue2');
                });

                $("#selField3").change(function (e) {
                    fn_Field('selField3', 'inputValue3');
                });

                $("#selField4").change(function (e) {
                    fn_Field('selField4', 'inputValue4');
                });

                $("#selField5").change(function (e) {
                    fn_Field('selField5', 'inputValue5');
                });

                $("#inputValue").keyup(function (e) {
                    fn_SetFilter('selField', 'inputValue');
                });

                $("#inputValue2").keyup(function (e) {
                    fn_SetFilter('selField2', 'inputValue2');
                });

                $("#inputValue3").keyup(function (e) {
                    fn_SetFilter('selField3', 'inputValue3');
                });

                $("#inputValue4").keyup(function (e) {
                    fn_SetFilter('selField4', 'inputValue4');
                });

                $("#inputValue5").keyup(function (e) {
                    fn_SetFilter('selField5', 'inputValue5');
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {

                    if ($('.tabnav a.active').text() === '유선상품 조회') {
                        $('.tabnav a').eq(1).click();
                    }
                    else if ($('.tabnav a.active').text() === '유선부가상품 조회') {
                        $('.tabnav a').eq(2).click();
                    }
                    else if ($('.tabnav a.active').text() === '무선상품 조회') {
                        $('.tabnav a').eq(3).click();
                    }
                    else if ($('.tabnav a.active').text() === 'MOT기타 조회') {
                        $('.tabnav a').eq(4).click();
                    }
                    else {
                        $('.tabnav a').eq(0).click();
                    }

                });

                $("#btnExcel").click(function (e) {
                    fn_BatchExcelDown();
                });
                
                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 정산상세내역 개인정산.xlsx", { sheetName: "개인정산" });
                });

                $("#gridExcel2").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 정산상세내역 유선상품.xlsx", { sheetName: "유선상품" });
                });

                $("#gridExcel3").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 정산상세내역 유선부가상품.xlsx", { sheetName: "유선부가상품" });
                });

                $("#gridExcel4").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 정산상세내역 무선상품.xlsx", { sheetName: "무선상품" });
                });

                $("#gridExcel5").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 정산상세내역 MOT기타.xlsx", { sheetName: "MOT기타" });
                });

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    fn_TabSelect(this.text);

                    return false;
                });
            }
        };

        var fn_Init = function () {
            today = new Date();
            dateNow = today.getDate();
            todayMon = today.getFullYear().toString() + ('0' + (today.getMonth() + 1)).slice(-2);


            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());


            $(".table-search").css('display', 'none');

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
            $("#tab02").hide();
            $("#tab03").hide();
            $("#tab04").hide();
            $("#tab05").hide();


            var formattedDate = today.getFullYear().toString() + '-' +
                ('0' + (today.getMonth() + 1)).slice(-2) + '월 ';

            $("#btnExcel").val(formattedDate + $("#btnExcel").val());

        }

        var fn_TabSelect = function (txt) {
            if (txt === '유선상품 조회') {
                fn_Search('CABLE');
            }
            else if (txt === '유선부가상품 조회') {
                fn_Search('ADD');
            }
            else if (txt === '무선상품 조회') {
                fn_Search('WIRELESS');
            }
            else if (txt === 'MOT기타 조회') {
                fn_Search('MOT');
            }
            else {
                fn_Search('EMP');
            }
        }

        var fn_BatchExcelDown = function () {
            startProgress();
            fn_ExcelSearch('excelCable');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }

        var fn_CellTotNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            var jsTotResultAmt = cell.getRow().getData().JS_TOT_RESULT_AMT;
            var totSumAmt = cell.getRow().getData().TOT_SUM_AMT;

            if (jsTotResultAmt != totSumAmt) {
                cell.getElement().style.backgroundColor = '#ff4b4b66';
            } else {
                cell.getElement().style.backgroundColor = '';  // 기본 배경색으로 되돌리기
            }

            value = priceToString(value);

            return value;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellCall = function (cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;

            if (id == null) {
                value = '합 계';
            }

            return value;
        }

        var fn_CellCalNumber = function (cell) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            $("#selField").empty();
            $("#selField").append("<option value=''>선택</option>");
            $("#selField").append("<option value='ORGNM1'>본부</option>");
            $("#selField").append("<option value='ORGNM2'>지사</option>");
            $("#selField").append("<option value='ORGNM3'>지점</option>");
            $("#selField").append("<option value='EMPNM'>판매자(계좌주)</option>");

            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "MDATE", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원번호", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자(계좌주)", field: "EMPNM", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산결과", field: "JSRESULT", sorter: "string", width: 110, minWidth: 110, topCalcFormatter: fn_CellCall },
                {
                    title: '정산처리금액(시스템반영)', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "재원총액", field: "JS_TOT_RESULT_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellTotNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "장려금지출", field: "JS_PAY_INCENTIVE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "급여지출", field: "JS_PERFOR_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "고객지원", field: "JS_CUSTOMER_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "right", title: "정산결과(환수포함)", field: "TOT_SUM_AMT", sorter: "number", width: 160, formatter: fn_CellTotNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                {
                    title: '①홈기본상품', headerHozAlign: "center",
                    columns: [
                        {
                            title: '인터넷', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "INTERNET_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "INTERNET_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                            ]
                        },
                        {
                            title: 'TV', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "TV_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "TV_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                            ]
                        },
                        {
                            title: '음성', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "VOICE_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "VOICE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                            ]
                        },
                    ]
                },
                {
                    title: '②홈부가상품', headerHozAlign: "center",
                    columns: [
                        {
                            title: '기가전환', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "CHANGE_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "CHANGE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                            ]
                        },
                        {
                            title: '재약정', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "UPDATE_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "UPDATE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                            ]
                        },
                        {
                            title: '기타상품', headerHozAlign: "center", width: 220,
                            columns: [
                                { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "ETC_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                                { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "ETC_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                            ]
                        },
                    ]
                },
                {
                    title: '③무선상품', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "WIRELESS_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "WIRELESS_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                    ]
                },
                {
                    title: '④MOT기타(상품외)', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "건수", field: "MOTETC_CNT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "금액", field: "MOTETC_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                    ]
                },
                {
                    title: '판매자(환수포함)', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "MOT실적급", field: "PERFOR_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "MOT장려금", field: "PAY_INCENTIVE_AMT", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                    ]
                },
                {
                    title: '별도송금(비과세)', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "right", title: "고객송금", field: "CUSTOMER_AMT1", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                        { headerHozAlign: "center", hozAlign: "right", title: "선지급송금", field: "CUSTOMER_AMT2", sorter: "number", width: 110, minWidth: 110, formatter: fn_CellNumber, topCalc: "sum", topCalcFormatter: fn_CellCalNumber },
                    ]
                },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }


        var fn_Grid2Init = function () {
            $("#selField2").empty();
            $("#selField2").append("<option value=''>선택</option>");
            $("#selField2").append("<option value='ORGNM1'>본부</option>");
            $("#selField2").append("<option value='ORGNM2'>지사</option>");
            $("#selField2").append("<option value='ORGNM3'>지점</option>");
            $("#selField2").append("<option value='EMPNM'>실판매자</option>");

            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수월", field: "RECEIPT_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산결과", field: "JSRESULT", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "부서명", field: "ORGNM", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "MOT Point", field: "MOT_POINT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본정책", field: "POLICY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "활성화ⅰ", field: "ACTIVATE_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "활성화ⅱ", field: "ACTIVATE_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "결합", field: "COMBINED_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "KT현장", field: "KT_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "각종차감", field: "VAR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기지급 장려금", field: "INCENTIVE_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "적용정책코드", field: "POLICYCD", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "적용정책(만원)", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICEGBCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICECD", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "RECEIPT_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자명", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품명 상세내역코드", field: "PRODUCTCD", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "약정", field: "PRODUCT_PERIOD", sorter: "string", width: 80 },
                {
                    title: '장려금', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "INCENTIVEDT1", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "1수수료", field: "INCENTIVE_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "INCENTIVEDT2", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "2수수료", field: "INCENTIVE_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '실적급', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "PERFORDT1", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "1실적급", field: "PERFOR_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "PERFORDT2", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "2실적급", field: "PERFOR_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '결합', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "COMBINEDDT1", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "1결합장려", field: "COMBINED_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "COMBINEDDT2", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "2결합장려", field: "COMBINED_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: 'KT현장', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "KTDT1", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "1KT현장", field: "KT_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "KTDT2", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "2KT현장", field: "KT_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200, visible: false },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid2', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Grid3Init = function () {
            $("#selField3").empty();
            $("#selField3").append("<option value=''>선택</option>");
            $("#selField3").append("<option value='ORGNM1'>본부</option>");
            $("#selField3").append("<option value='ORGNM2'>지사</option>");
            $("#selField3").append("<option value='ORGNM3'>지점</option>");
            $("#selField3").append("<option value='EMPNM'>실판매자</option>");

            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수월", field: "RECEIPT_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산결과", field: "JSRESULT", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "부서명", field: "ORGNM", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "MOT Point", field: "MOT_POINT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본장려금", field: "BASE_INCENTIVE_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "프로모션", field: "PROMOTION_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "SM패스 이미징 차감", field: "SMPASS_AMT", sorter: "number", width: 160, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기지급 장려금", field: "INCENTIVE_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "적용정책코드", field: "POLICYCD", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "적용정책(만원)", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICEGBCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICECD", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "RECEIPT_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자명", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품명 상세내역코드", field: "PRODUCTCD", sorter: "string", width: 100, visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "약정", field: "PRODUCT_PERIOD", sorter: "string", width: 80 },
                {
                    title: '장려금', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "INCENTIVEDT1", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "1수수료", field: "INCENTIVE_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "INCENTIVEDT2", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "2수수료", field: "INCENTIVE_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                    ]
                },
                {
                    title: '실적급', headerHozAlign: "center",
                    columns: [
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "PERFORDT1", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "1실적급", field: "PERFOR_AMT1", sorter: "number", width: 120, formatter: fn_CellNumber },
                        { headerHozAlign: "center", hozAlign: "center", title: "일자", field: "PERFORDT2", sorter: "string", width: 100 },
                        { headerHozAlign: "center", hozAlign: "right", title: "2실적급", field: "PERFOR_AMT2", sorter: "number", width: 120, formatter: fn_CellNumber },
                    ]
                },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200, visible: false },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid3', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Grid4Init = function () {
            $("#selField4").empty();
            $("#selField4").append("<option value=''>선택</option>");
            $("#selField4").append("<option value='ORGNM1'>본부</option>");
            $("#selField4").append("<option value='ORGNM2'>지사</option>");
            $("#selField4").append("<option value='ORGNM3'>지점</option>");
            $("#selField4").append("<option value='EMPNM'>실판매자</option>");

            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수월", field: "RECEIPT_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산결과", field: "JSRESULT", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "부서명", field: "ORGNM", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "획득 Point", field: "MOT_POINT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "지급액", field: "PAY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본정책", field: "POLICY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "Pass활성화", field: "PASS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "주력정책", field: "MAIN_POLICY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "B2C 5G", field: "B2C5G_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "B2C LTE", field: "B2CLTE_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "결합", field: "COMBINED_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기타", field: "ETC_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "차감", field: "VAR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "특이사항", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "PRODUCTCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "PRODUCTNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "(-1)고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "SA_ID(-2)", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "SA_ID(-2)원본", field: "SAID_T", sorter: "string", width: 130, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "RECEIPT_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자명", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTDESC", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "약정", field: "PRODUCT_PERIOD", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200, visible: false },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid4', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Grid5Init = function () {
            $("#selField5").empty();
            $("#selField5").append("<option value=''>선택</option>");
            $("#selField5").append("<option value='ORGNM1'>본부</option>");
            $("#selField5").append("<option value='ORGNM2'>지사</option>");
            $("#selField5").append("<option value='ORGNM3'>지점</option>");
            $("#selField5").append("<option value='EMPNM'>실판매자</option>");

            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "접수월", field: "RECEIPT_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산결과", field: "JSRESULT", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부코드", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사코드", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점코드", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서코드", field: "ORGCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "부서명", field: "ORGNM", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "IDMS사번", field: "IDMSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기본장려금", field: "BASE_INCENTIVE_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "기타추가", field: "ETC_ADD_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(적용정책)", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "PRODUCTCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명/지급명", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)원본", field: "SAID_T", sorter: "string", width: 130, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "RECEIPT_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "담당자", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTDESC", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "약정", field: "PRODUCT_PERIOD", sorter: "string", width: 80 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200, visible: false },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid5', 'fitData', true, 'local'); // div의 grid id 값
        }
    
        var fn_Load = function () {
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pType: "jungsan"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function () {
            return true;
        }

        var fn_Field = function (field, value) {
            var fieldEl = document.getElementById(field);
            var valueEl = document.getElementById(value);
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_SetFilter = function (field, value) {
            var fieldEl = document.getElementById(field);
            var valueEl = document.getElementById(value);

            ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
        }

        var excelTitle = function (pTitle) {
            var title = '';
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2) +
                ('0' + today.getDate()).slice(-2);

            title += formattedDate + '_';
            title += pTitle;

            return title;
        }

        function getLastDayOfMonth(year, month) {
            return new Date(year, month, 0).getDate();
        }

        var ws2;
        var ws3;
        var ws4;
        var ws5;
        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }//excelEmp
            else if (callGb == 'excelCable') {
                callGb = '';
                ws2 = fn_excelSheet2(json);
                fn_ExcelSearch('excelAdd');
            }
            else if (callGb == 'excelAdd') {
                callGb = '';
                ws3 = fn_excelSheet3(json);
                fn_ExcelSearch('excelWireless');
            }
            else if (callGb == 'excelWireless') {
                callGb = '';
                ws4 = fn_excelSheet4(json);
                fn_ExcelSearch('excelMot');
            }
            else if (callGb == 'excelMot') {
                callGb = '';
                ws5 = fn_excelSheet5(json);
                fn_ExcelSearch('excelEmp');
            }
            else if (callGb == 'excelEmp') {
                callGb = '';

                var sheetNm1 = '개인정산';
                var sheetNm2 = '① 유선상품';
                var sheetNm3 = '② 유선부가상품';
                var sheetNm4 = '③ 무선상품';
                var sheetNm5 = '④ MOT기타';

                var title = excelTitle('통합정산 정산상세내역');
                fn_excelDown('', json, sheetNm1, sheetNm2, sheetNm3, sheetNm4, sheetNm5, title, 'xlsx');
            }

        }

        var fn_ExcelHeaderStyle = function (hRow, ws, headers) {
            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center", wrapText: true },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            if (hRow === 1) {
                // Apply styles to headers
                for (var C = 0; C < headers.length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }
            else {
                // Apply styles to headers
                for (var R = 0; R < headers.length; ++R) {
                    for (var C = 0; C < headers[R].length; ++C) {
                        var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                        if (!ws[cell_address]) continue;
                        ws[cell_address].s = headerStyle;
                    }
                }
            }

            return ws;
        }

        var fn_excelDown = function (gubun, json, sheetNm1, sheetNm2, sheetNm3, sheetNm4, sheetNm5, fileNm, type) {
            var headers;

            headers = [
                ['본부', '지사', '지점', '직무', '사원번호', '판매자\n(계좌주)', '정산 결과', '정산처리금액 (시스템반영)', '정산처리금액 (시스템반영)', '정산처리금액 (시스템반영)', '정산처리금액 (시스템반영)', '정산결과\n(환수포함)', '①홈기본상품', '①홈기본상품', '①홈기본상품', '①홈기본상품', '①홈기본상품', '①홈기본상품', '②홈부가상품', '②홈부가상품', '②홈부가상품', '②홈부가상품', '②홈부가상품', '②홈부가상품', '③무선상품', '③무선상품', '④MOT기타(상품외)', '④MOT기타(상품외)', '', '판매자(환수포함)', '판매자(환수포함)', '별도송금(비과세)', '별도송금(비과세)'],
                ['', '', '', '', '', '', '', '재원총액', '장려금지출', '급여지출', '고객지원', '', '인터넷', '인터넷', 'TV', 'TV', '음성', '음성', '기가전환', '기가전환', '재약정', '재약정', '기타상품', '기타상품', '', '', '', '', '', 'MOT실적급', 'MOT장려금', '고객송금', '선지급송금']
            ];

            var jsonData = JSON.parse(json);

            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.JSRESULT, item.JS_TOT_RESULT_AMT, item.JS_PAY_INCENTIVE_AMT, item.JS_PERFOR_AMT, item.JS_CUSTOMER_AMT, item.TOT_SUM_AMT, item.INTERNET_CNT, item.INTERNET_AMT, item.TV_CNT, item.TV_AMT, item.VOICE_CNT, item.VOICE_AMT, item.CHANGE_CNT, item.CHANGE_AMT, item.UPDATE_CNT, item.UPDATE_AMT, item.ETC_CNT, item.ETC_AMT, item.WIRELESS_CNT, item.WIRELESS_AMT, item.MOTETC_CNT, item.MOTETC_AMT, '', item.PERFOR_AMT, item.PAY_INCENTIVE_AMT, item.CUSTOMER_AMT1, item.CUSTOMER_AMT2]);

            

            data.unshift(headers[1]); // Add the second row of headers
            data.unshift(headers[0]); // Add the first row of headers

            // Convert array of arrays to worksheet
            var ws1 = XLSX.utils.aoa_to_sheet(data);

            ws1['!merges'] = [
                { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },
                { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } },
                { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } },
                { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } },
                { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } },
                { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } },
                { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } },
                { s: { r: 0, c: 7 }, e: { r: 0, c: 10 } },
                { s: { r: 0, c: 11 }, e: { r: 1, c: 11 } },
                { s: { r: 0, c: 12 }, e: { r: 0, c: 17 } },
                { s: { r: 0, c: 18 }, e: { r: 0, c: 23 } },
                { s: { r: 0, c: 24 }, e: { r: 1, c: 25 } },
                { s: { r: 0, c: 26 }, e: { r: 1, c: 27 } },
                { s: { r: 1, c: 28 }, e: { r: 1, c: 28 } }, //공백
                { s: { r: 0, c: 29 }, e: { r: 0, c: 30 } },
                { s: { r: 0, c: 31 }, e: { r: 0, c: 32 } },
                { s: { r: 1, c: 12 }, e: { r: 1, c: 13 } },
                { s: { r: 1, c: 14 }, e: { r: 1, c: 15 } },
                { s: { r: 1, c: 16 }, e: { r: 1, c: 17 } },
                { s: { r: 1, c: 18 }, e: { r: 1, c: 19 } },
                { s: { r: 1, c: 20 }, e: { r: 1, c: 21 } },
                { s: { r: 1, c: 22 }, e: { r: 1, c: 23 } }
            ];

            // Add styles (example: set column width)
            ws1['!cols'] = [
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center", wrapText: true },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            var headerNumStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "right", vertical: "center", wrapText: true },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            // Apply styles to headers
            for (var R = 0; R < headers.length; ++R) {
                for (var C = 0; C < headers[R].length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                    if (!ws1[cell_address]) continue;
                    if (C != 28) {
                        ws1[cell_address].s = headerStyle;
                    }
                }
            }

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws1['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws1[cell_ref]) continue;

                    if (R == 2) {
                        if (C >= 7 && C <= 33) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            if (C != 28) {
                                ws1[cell_ref].s = headerNumStyle;
                            }
                        }
                        else {
                            ws1[cell_ref].s = {
                                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                    else if (R > 2) {
                        if (C >= 7 && C <= 33) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else {
                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();

            XLSX.utils.book_append_sheet(wb, ws1, sheetNm1);
            XLSX.utils.book_append_sheet(wb, ws2, sheetNm2);
            XLSX.utils.book_append_sheet(wb, ws3, sheetNm3);
            XLSX.utils.book_append_sheet(wb, ws4, sheetNm4);
            XLSX.utils.book_append_sheet(wb, ws5, sheetNm5);

            ws2 = null;
            ws3 = null;
            ws4 = null;
            ws5 = null;

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

            endProgress('');
        }

        var fn_excelSheet2 = function (json) {
            var headers;
            //5
            headers = ['정산결과', '본부', '지사', '지점', '직무', '사원번호', '실판매자', 'MOT Point', '정산총액', '기본정책', 'MOT 실적급', '활성화ⅰ', '활성화ⅱ', '결합', 'KT현장', '각종차감', '기지급 장려금', '적용정책(만원)', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', '계약번호(SAID)', '접수자명', '실판매자명', 'KT제공사은품', '상품명 상세내역', '약정', '일자', '1수수료', '일자', '2수수료', '일자', '1실적급', '일자', '2실적급', '일자', '1결합장려', '일자', '2결합장려', '일자', '1KT현장', '일자', '2KT현장'];

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.JSRESULT, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.MOT_POINT, item.TOT_JS_AMT, item.POLICY_AMT, item.MOT_PERFOR_AMT, item.ACTIVATE_AMT1, item.ACTIVATE_AMT2, item.COMBINED_AMT, item.KT_AMT, item.VAR_AMT, item.INCENTIVE_AMT, item.POLICYNM, item.JSCD, item.SERVICEGBCD, item.RECEIPTDT, item.SERVICENM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTNM, item.PRODUCT_PERIOD, item.INCENTIVEDT1, item.INCENTIVE_AMT1, item.INCENTIVEDT2, item.INCENTIVE_AMT2, item.PERFORDT1, item.PERFOR_AMT1, item.PERFORDT2, item.PERFOR_AMT2, item.COMBINEDDT1, item.COMBINED_AMT1, item.COMBINEDDT2, item.COMBINED_AMT2, item.KTDT1, item.KT_AMT1, item.KTDT2, item.KT_AMT2]);

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];

            ws = fn_ExcelHeaderStyle(1, ws, headers);

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 0) {
                        if (C == 7) {
                            ws[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else if ((C >= 8 && C <= 16) || C == 37 || C == 39 || C == 41 || C == 43 || C == 45 || C == 47 || C == 49 || C == 51) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 35) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            return ws;
        }

        var fn_excelSheet3 = function (json) {
            var headers;

            headers = ['정산결과', '본부', '지사', '지점', '직무', '사원번호', '실판매자', 'MOT Point', '정산총액', '기본장려금', 'MOT 실적급', '프로모션', 'SM패스 이미징 차감', '기지급 장려금', '적용정책(만원)', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', '계약번호(SAID)', '접수자명', '실판매자명', 'KT제공사은품', '상품명 상세내역', '약정', '일자', '1수수료', '일자', '2수수료', '일자', '1실적급', '일자', '2실적급'];

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.JSRESULT, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.MOT_POINT, item.TOT_JS_AMT, item.BASE_INCENTIVE_AMT, item.MOT_PERFOR_AMT, item.PROMOTION_AMT, item.SMPASS_AMT, item.INCENTIVE_AMT, item.POLICYNM, item.JSCD, item.SERVICEGBCD, item.RECEIPTDT, item.SERVICENM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTNM, item.PRODUCT_PERIOD, item.INCENTIVEDT1, item.INCENTIVE_AMT1, item.INCENTIVEDT2, item.INCENTIVE_AMT2, item.PERFORDT1, item.PERFOR_AMT1, item.PERFORDT2, item.PERFOR_AMT2]);

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 18.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 }
            ];

            ws = fn_ExcelHeaderStyle(1, ws, headers);

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 0) {
                        if (C == 7) {
                            ws[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else if ((C >= 8 && C <= 13) || C == 34 || C == 36 || C == 38 || C == 40) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 32) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            return ws;
        }

        var fn_excelSheet4 = function (json) {
            var headers;

            headers = ['정산결과', '본부', '지사', '지점', '직무', '사원번호', '실판매자', '획득 Point', '지급액', '기본정책', 'MOT실적급', 'Pass활성화', '주력정책', 'B2C 5G', 'B2C LTE', '결합', '기타', '차감', '특이사항', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '(-1)고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', 'SA_ID(-2)', '접수자명', '실판매자명', 'KT제공사은품', '상품명 상세내역', '약정'];

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.JSRESULT, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.MOT_POINT, item.PAY_AMT, item.POLICY_AMT, item.MOT_PERFOR_AMT, item.PASS_AMT, item.MAIN_POLICY_AMT, item.B2C5G_AMT, item.B2CLTE_AMT, item.COMBINED_AMT, item.ETC_AMT, item.VAR_AMT, item.POLICYNM, item.JSCD, item.PRODUCTCD, item.RECEIPTDT, item.PRODUCTNM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTDESC, item.PRODUCT_PERIOD]);

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            ws = fn_ExcelHeaderStyle(1, ws, headers);

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 0) {
                        if (C == 7) {
                            ws[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else if (C >= 8 && C <= 17) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 36) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            return ws;
        }

        var fn_excelSheet5 = function (json) {
            var headers;

            headers = ['정산결과', '본부', '지사', '지점', '직무', '사원번호', '실판매자', '정산총액', '기본장려금', 'MOT 실적급', '기타추가', '비고(적용정책)', '정산코드', '상품코드', '접수일', '상품명/지급명', '서비스번호', '고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', '계약번호(SAID)', '접수자명', '담당자', 'KT제공사은품', '상품명 상세내역', '약정'];

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.JSRESULT, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.TOT_JS_AMT, item.BASE_INCENTIVE_AMT, item.MOT_PERFOR_AMT, item.ETC_ADD_AMT, item.POLICYNM, item.JSCD, item.PRODUCTCD, item.RECEIPTDT, item.PRODUCTNM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTDESC, item.PRODUCT_PERIOD]);

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            ws = fn_ExcelHeaderStyle(1, ws, headers);

            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 0) {
                        if (C >= 7 && C <= 10) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 29) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            return ws;
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'JSDETAIL';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ExcelSearch = function (gubun) {
            callGb = gubun;

            var pJsResult = '';
            var aData = []; //입력 파라미터
            var ajaxUrl = '';
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2);

            var pDate = formattedDate;

            if (gubun === 'excelCable') {
                gubun = 'SEARCH';
                aData[0] = gubun;
                aData[1] = pDate;

                ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridDataCable');
            }
            else if (gubun === 'excelAdd') {
                gubun = 'SEARCH';
                aData[0] = gubun;
                aData[1] = pDate;

                ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridDataAdd');
            }
            else if (gubun === 'excelWireless') {
                gubun = 'SEARCH';
                aData[0] = gubun;
                aData[1] = pDate;

                ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridDataWireless');
            }
            else if (gubun === 'excelMot') {
                gubun = 'SEARCH';
                aData[0] = gubun;
                aData[1] = pDate;

                ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridDataCableMot');
            }
            else {
                gubun = 'ERP';
                aData[0] = gubun;
                aData[1] = pDate;
                aData[2] = pJsResult;
                
                ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridDataEmp');
            }

            var jsonData = JSON.stringify({ aData: aData });

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function (gubun) {
            if (fn_SearchChk()) {

                var pDate1 = $("#selMon").val().replace(/-/gi, "");
                var pJsResult = '';
                var aData = []; //입력 파라미터

                if (gubun === 'CABLE') {
                    aData[0] = 'SEARCH';
                    aData[1] = pDate1;

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridDataCable');

                    fn_Grid2Init();
                    ktsRowCountClass = 'searchrowcount2';
                }
                else if (gubun === 'ADD') {
                    aData[0] = 'SEARCH';
                    aData[1] = pDate1;

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridDataAdd');

                    fn_Grid3Init();
                    ktsRowCountClass = 'searchrowcount3';
                }
                else if (gubun === 'WIRELESS') {
                    aData[0] = 'SEARCH';
                    aData[1] = pDate1;

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridDataWireless');

                    fn_Grid4Init();
                    ktsRowCountClass = 'searchrowcount4';
                }
                else if (gubun === 'MOT') {
                    aData[0] = 'SEARCH';
                    aData[1] = pDate1;

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridDataCableMot');

                    fn_Grid5Init();
                    ktsRowCountClass = 'searchrowcount5';
                }
                else {
                    aData[0] = 'SEARCH';
                    aData[1] = pDate1;
                    aData[2] = pJsResult;

                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridDataEmp');

                    fn_GridInit();
                    ktsRowCountClass = 'searchrowcount';
                }

                var jsonData = JSON.stringify({ aData: aData });

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
                $(".table-option").css('display', 'block');
                $(".table-search").css('display', 'block');
            }
        }

        function endProgress(msg) {
            $("#progressBarSet").hide();

            if (msg != '') {
                alert(msg);
            }
        }

        function startProgress() {
            $("#progressBarSet").show();
            $("#progressText").html("잠시만 기다려주세요.");
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->

	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>정산월</label>
                <select id="selDt" style="width:80px;display:none;">
                    <option selected="selected" value="m">정산월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnExcel" type="button" class="btn-black last" style="float: right;margin-right:30px" value="정산상세내역내려받기" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                    <input id="hdnRm" type="hidden" runat="server" />
                    <input id="hdnAuthId" type="hidden" runat="server" />
                    <input id="hdnAuthNm" type="hidden" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <ul class="tabnav">
                <li><a href="#tab01">개인정산 조회</a></li>
                <li><a href="#tab02">유선상품 조회</a></li>
                <li><a href="#tab03">유선부가상품 조회</a></li>
                <li><a href="#tab04">무선상품 조회</a></li>
                <li><a href="#tab05">MOT기타 조회</a></li>
            </ul>
            <div class="tabcontent">
                <div id="tab01">
                    <div class="list-top">
                        <strong>개인정산 결과</strong>
                        <label class="searchrowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField"></select>
                                : 
                                 <input id="inputValue" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid"></div>
                    </div>
                </div>
                <div id="tab02">
                    <div class="list-top">
                        <strong>유선상품 결과</strong>
                        <label class="searchrowcount2"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel2" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField2"></select>
                                : 
                                 <input id="inputValue2" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid2"></div>
                    </div>
                </div>
                <div id="tab03">
                    <div class="list-top">
                        <strong>유선부가상품 결과</strong>
                        <label class="searchrowcount3"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel3" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField3"></select>
                                : 
                                 <input id="inputValue3" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid3"></div>
                    </div>
                </div>
                <div id="tab04">
                    <div class="list-top">
                        <strong>무선상품 결과</strong>
                        <label class="searchrowcount4"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel4" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField4"></select>
                                : 
                                 <input id="inputValue4" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid4"></div>
                    </div>
                </div>
                <div id="tab05">
                    <div class="list-top">
                        <strong>MOT기타 결과</strong>
                        <label class="searchrowcount5"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel5" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField5"></select>
                                : 
                                 <input id="inputValue5" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid5"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- E:datalist -->
        <div id="progressBarSet" style="display:none">
            <div id="progressBarContainer">
            <img src="../Resource/images/icon_loadingbar.gif" />
            <h2>다운로드중...</h2>
                <div id="progressText"></div>
            </div>
        </div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>