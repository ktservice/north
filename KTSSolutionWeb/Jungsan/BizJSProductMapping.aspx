﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BizJSProductMapping.aspx.cs" Inherits="KTSSolutionWeb.BizJSProductMapping" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .selUse {
            width: 100%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: border-box;
            background: transparent!important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        .plusicon {
            background:#fff url('/Resource/Mobile_images/icon_plus_01.png')no-repeat 50%;
            width:11px;
            height:11px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var initYn = 'N';
        var ktsGrid1;
        var ktsGrid2;
        var ktsGrid2RowCnt = 0;
        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var cellUnit;
        var cellUnitCnt;

        var page = {
            init: function () {
                fn_Init();
                fn_LeftGridInit();
                fn_RightGridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#selProductGb").change(function (e) {
                    $("#hdnProductGb").val(this.value);
                    fn_Class('selService', this.value);
                });

                $("#selService").change(function (e) {
                    $("#hdnService").val(this.value);
                    fn_Search('LEFT', $("#selProductGb").val(), this.value);
                });

                $("#selPolicy").change(function (e) {
                    $("#hdnPolicy").val(this.value);
                    fn_Search('RIGHT', this.value, '');
                });

                $("#selField").change(function (e) {
                    fn_Field('1', 'selField', 'inputValue');
                });

                $("#selField2").change(function (e) {
                    fn_Field('2', 'selField2', 'inputValue2');
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#inputValue2").keyup(function (e) {
                    var fieldEl = document.getElementById("selField2");
                    var valueEl = document.getElementById("inputValue2");

                    ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $("#btnProductReg").click(function (e) {
                    fn_ProductReg('I', '');
                });

                $("#btnProductCode").click(function (e) {
                    fn_ProductCode();
                });

                $("#btnPolicyReg").click(function (e) {
                    fn_PolicyReg('M');
                });

                $("#btnPolicyCode").click(function (e) {
                    fn_PolicyCode();
                });

            }
        };

        var fn_Init = function () {
            $(".table-search").css('display', 'none');
            $("#divProductListData").hide();
        }

        var fn_CellFunction = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnMappingReg' type='button' onclick='fn_MappingRegTran(\"" + cellNo + "\");' value='매핑' class='btn-save' style='height:23px;line-height:0px;font-size:15px;15px;margin-right:10px;'>";
            html += "<input id='btnMappingMo' type='button' onclick='fn_ProductReg(\"M\",\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
            html += "<input id='btnMappingDel' type='button' onclick='fn_DelTran(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";

            return html;
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellDelBo = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnMappingReg' type='button' onclick='fn_MappingDelTran(\"" + cellNo + "\");' value='매핑제거' class='btn-del' style='height:23px;line-height:0px;font-size:15px;'>";
            return html;
        }

        var fn_CellUseReg = function (cell, formatterParams, onRendered) {
            var html = '';
            var cellValue = cell.getValue();
            var id = cell.getRow().getData().id;


            html = "<select id='selReg' class='selUse' onchange='fn_UseReg(this, \"" + id + "\");'>";
            html += "<option value='Y'>Y</opiton>";
            html += "<option value='N'>N</opiton>";
            html += "</select>";

            if (cellValue == 'Y') {
                html = html.replace("<option value='Y'>", "<option value='Y' selected='selected'>");
            }
            else if (cellValue == 'N') {
                html = html.replace("<option value='N'>", "<option value='N' selected='selected'>");
            }


            return html;
        }

        var cellUseYn = '';
        var fn_UseReg = function (obj, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            if (confirm("변경을 진행하시겠습니까?")) {
                var useYn = obj.value;
                cellUseYn = useYn;
                cellNo = id;

                callGb = 'MappingModTran';
                fn_Transaction2('POLICY', 'M', id);
            }
            else {
                var originValue = rowData.USEYN;

                //원본으로 복귀
                obj.value = originValue;
            }
        }

        var fn_LeftGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { formatter: fn_CellFunction, width: 210, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "PRODUCTCD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스구분코드", field: "SERVICEGBCD", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스코드", field: "SERVICECD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "서비스명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분코드", field: "PRODUCTGBCD", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품구분명", field: "PRODUCTGBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "내용", field: "MEMO", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "사용유무", field: "USEYN", sorter: "string", width: 100 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid1 = ktsGrid;
        }

        var fn_RightGridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { formatter: fn_CellDelBo, width: 74, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "PRODUCTCD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스구분코드", field: "SERVICEGBCD", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스코드", field: "SERVICECD", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "서비스명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품구분코드", field: "PRODUCTGBCD", sorter: "string", width: 110 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품구분명", field: "PRODUCTGBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "정책코드", field: "POLICYCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "left", title: "정책명", field: "POLICYNM", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "사용유무", field: "USEYN", sorter: "string", width: 100, formatter: fn_CellUseReg },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid2', 'fitColumns', true, 'local'); // div의 grid id 값

            ktsGrid2 = ktsGrid;
        }

        var fn_Load = function () {
            fn_Class('selProductGb', '');

            $("#lblServiceRlt").text('정책없음');
        }

        var fn_MappingRegTran = function (id) {
            if (confirm("매핑을 하시겠습니까?")) {
                callGb = 'MappingRegTran';
                fn_Transaction2('POLICY', 'I', id);
            }
        }

        var fn_DelTran = function (id) {
            if (confirm("삭제를 하시겠습니까?")) {
                callGb = 'DelTran';

                ktsGridRowCnt = ktsGrid1.getDataCount();
                fn_Transaction('PRODUCT', 'D', id);
            }
        }

        var fn_MappingDelTran = function (id) {
            if (confirm("매핑제거를 하시겠습니까?")) {
                callGb = 'MappingDelTran';

                ktsGrid2RowCnt = ktsGrid2.getDataCount();
                fn_Transaction2('POLICY', 'D', id);
            }
        }

        var fn_Search = function (gubun, cd1, cd2) {


            if (gubun == 'LEFT') {
                fn_LeftGridInit();
            }
            else {
                fn_RightGridInit();

                var resultTxt = '';
                resultTxt = $("#selPolicy option:selected").text();
                $("#lblServiceRlt").text(resultTxt);
            }

            callGb = gubun;

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = cd1;
            aData[2] = cd2;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

            //grid Bind
            if (gubun == 'LEFT') {
                ktsRowCountClass = 'searchrowcount';
                grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid');
            }
            else {
                ktsRowCountClass = 'searchrowcount2';
                grid.getDataBind(jsonData, ajaxUrl, 'ktsGrid2');
            }

            $(".table-option").css('display', 'block');
        }

        var fn_Transaction = function (jobGb, gubun, id) {
            var row = ktsGrid1.getRow(id);
            var rowData = row.getData();
            var productgbCd = '';
            var productgbNm = '';
            var serviceGbCd = rowData.SERVICEGBCD;
            var serviceCd = rowData.SERVICECD;
            var serviceNm = '';
            var productCd = rowData.PRODUCTCD;
            var productNm = '';
            var policyCd = '';
            var policyNm = '';
            var memo = '';
            var useYn = '';
            var useYn2 = '';

            //입력 파라미터
            var aData = [];
            aData[0] = jobGb;
            aData[1] = gubun;
            aData[2] = productgbCd;
            aData[3] = productgbNm;
            aData[4] = serviceGbCd;
            aData[5] = serviceCd;
            aData[6] = serviceNm;
            aData[7] = productCd;
            aData[8] = productNm;
            aData[9] = memo;
            aData[10] = useYn;
            aData[11] = policyCd;
            aData[12] = policyNm;
            aData[13] = useYn2;


            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction2 = function (jobGb, gubun, id) {
            var row;

            if (gubun == 'I') row = ktsGrid1.getRow(id);
            else row = ktsGrid2.getRow(id);

            var rowData = row.getData();
            var productgbCd = '';
            var productgbNm = '';
            var serviceGbCd = '';
            var serviceCd = '';
            var serviceNm = '';
            var productCd = '';
            var productNm = '';
            var policyCd = '';
            var policyNm = '';
            var memo = '';
            var useYn = '';
            var useYn2 = '';

            if (gubun == 'I') {
                productgbCd = rowData.PRODUCTGBCD;
                productgbNm = rowData.PRODUCTGBNM;
                serviceGbCd = rowData.SERVICEGBCD;
                serviceCd = rowData.SERVICECD;
                serviceNm = rowData.SERVICENM;
                productCd = rowData.PRODUCTCD;
                productNm = rowData.PRODUCTNM;
                policyCd = $("#selPolicy").val();
                policyNm = $("#selPolicy option:selected").text();
                useYn2 = 'Y';
            }
            else {
                productgbCd = rowData.PRODUCTGBCD;
                productgbNm = rowData.PRODUCTGBNM;
                serviceGbCd = rowData.SERVICEGBCD;
                serviceCd = rowData.SERVICECD;
                serviceNm = rowData.SERVICENM;
                productCd = rowData.PRODUCTCD;
                productNm = rowData.PRODUCTNM;
                policyCd = rowData.POLICYCD;
                policyNm = rowData.POLICYNM;
                useYn2 = rowData.USEYN;;
            }

            if (gubun == 'M') useYn2 = cellUseYn;

            //입력 파라미터
            var aData = [];
            aData[0] = jobGb;
            aData[1] = gubun;
            aData[2] = productgbCd;
            aData[3] = productgbNm;
            aData[4] = serviceGbCd;
            aData[5] = serviceCd;
            aData[6] = serviceNm;
            aData[7] = productCd;
            aData[8] = productNm;
            aData[9] = memo;
            aData[10] = useYn;
            aData[11] = policyCd;
            aData[12] = policyNm;
            aData[13] = useYn2;


            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_PolicyCode = function () {
            var form = "/Jungsan/BizJSPolicyCodeMappingPopup";
            var target = "BizJSPolicyCodeMappingPopup";
            var param = {
            };

            var nWidth = 900;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PolicyReg = function (gubun) {
            var policyCd = $("#selPolicy").val();

            var form = "/Jungsan/BizJSPolicyInfoRegPopup";
            var target = "BizJSPolicyInfoRegPopup";

            var param = {
                pStatus: gubun,
                pPolicyCd: policyCd
            };

            var nWidth = 720;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_ProductCode = function () {
            var form = "/Jungsan/BizJSProductCodePopup";
            var target = "BizJSProductCodePopup";
            var param = {
            };

            var nWidth = 900;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_ProductReg = function (gubun, id) {
            var productgbCd = '';
            var productgbNm = '';
            var serviceGbCd = '';
            var serviceCd = '';
            var serviceNm = '';
            var productCd = '';
            var productNm = '';
            var memo = '';
            var useYn = '';

            if (gubun === 'M') {
                var row = ktsGrid1.getRow(id);
                var rowData = row.getData();

                productgbCd = rowData.PRODUCTGBCD;
                productgbNm = rowData.PRODUCTGBNM;
                serviceGbCd = rowData.SERVICEGBCD;
                serviceCd = rowData.SERVICECD;
                serviceNm = rowData.SERVICENM;
                productCd = rowData.PRODUCTCD;
                productNm = rowData.PRODUCTNM;
                memo = rowData.MEMO;
                useYn = rowData.USEYN;

                if (productgbCd == null) productgbCd = '';
                if (productgbNm == null) productgbNm = '';
                if (serviceGbCd == null) serviceGbCd = '';
                if (serviceCd == null) serviceCd = '';
                if (serviceNm == null) serviceNm = '';
                if (productCd == null) productCd = '';
                if (productNm == null) productNm = '';
                if (memo == null) memo = '';
                if (useYn == null) useYn = '';
            }


            var form = "/Jungsan/BizJSProductRegPopup";
            var target = "BizJSProductRegPopup";

            var param = {
                pCellNo: id,
                pStatus: gubun,
                pProductgbCd: productgbCd,
                pProductgbNm: productgbNm,
                pServiceGbCd: serviceGbCd,
                pServiceCd: serviceCd,
                pServiceNm: serviceNm,
                pProductCd: productCd,
                pProductNm: productNm,
                pMemo: memo,
                pUse: useYn
            };

            var nWidth = 720;
            var nHeight = 760;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var SetDispClass = function (check, cellNo, productgbCd, productgbNm, serviceCd, serviceNm, productCd, productNm, memo, useYn) {

            if (check == 'ok' && cellNo != '') {
                var row = ktsGrid1.getRow(cellNo);
                row.select();

                ktsGrid1.updateData([{
                    id: cellNo, PRODUCTNM: productNm, SERVICECD: serviceCd, SERVICENM: serviceNm, PRODUCTGBCD: productgbCd, PRODUCTGBNM: productgbNm, MEMO: memo, USEYN: useYn
                }])
                    .then(function () {
                        //run code after row
                    })
                    .catch(function (error) {
                        //handle error
                    });

                fn_Search('RIGHT', $("#hdnPolicy").val(), '');
            }
            else if (check == 'ok' && cellNo == '') {
                fn_Class('selProductGb', '');
            }
        }

        var SetDispClass2 = function (check, policyCd) {
            if (check == 'ok') {
                fn_Class('selPolicy', '');
            }
        }

        var fn_Field = function (gubun, fieldEl, valueEl) {
            var vFieldEl = document.getElementById(fieldEl);
            var vValueEl = document.getElementById(valueEl);
            valueEl.value = '';

            if (vFieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');

                if (gubun == '1') ktsGrid1.setFilter('NUM', 'like', '');
                else ktsGrid2.setFilter('NUM', 'like', '');

            }
            else {
                vValueEl.removeAttribute('disabled');

                if (gubun == '1') ktsGrid1.setFilter(fieldEl.value, 'like', valueEl.value);
                else ktsGrid2.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }


        var fn_Class = function (gb, code) {
            callGb = gb;

            var gubun = '';

            if (gb == 'selProductGb') gubun = 'PRODUCTGB';
            else if (gb == 'selPolicy') gubun = 'POLICY';
            else if (gb == 'selService') gubun = 'SERVICE';

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = code;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetBizJsDll');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var ktsGridSearch = function () {
            $(".table-search").css('display', 'block');

            if (initYn == 'N') {
                initYn = 'Y';
                if (callGb == 'LEFT') fn_Class('selPolicy', '');
            }
        }

        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selProductGb' || callGb == 'selService' || callGb == 'selPolicy') {
                obj = $('#' + callGb);
                obj.empty();

                if (callGb == 'selService') obj.append("<option value='all'>전체</option>");

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.CD + "'>" + item.NM + "</option>");
                });


                if (callGb == 'selProductGb') {
                    if ($("#hdnProductGb").val() == '' || $("#hdnProductGb").val() == null) {
                        $('#selProductGb option:first').prop('selected', true);
                    }
                    else {
                        obj.val($("#hdnProductGb").val()).prop("selected", true);
                    }

                    if (obj.val() == '' || obj.val() == null) {
                        $('#selProductGb option:first').prop('selected', true);
                    }
                }
                else if (callGb == 'selService') {
                    if ($("#hdnService").val() == '' || $("#hdnService").val() == null) {
                        $('#selService option:first').prop('selected', true);
                    }
                    else {
                        obj.val($("#hdnService").val()).prop("selected", true);
                    }

                    if (obj.val() == '' || obj.val() == null) {
                        $('#selService option:first').prop('selected', true);
                    }
                }
                else if (callGb == 'selPolicy') {
                    if ($("#hdnPolicy").val() == '' || $("#hdnPolicy").val() == null) {
                        $('#selPolicy option:first').prop('selected', true);
                    }
                    else {
                        obj.val($("#hdnPolicy").val()).prop("selected", true);
                    }

                    if (obj.val() == '' || obj.val() == null) {
                        $('#selPolicy option:first').prop('selected', true);
                    }
                }

                obj.val(obj.val()).trigger('change');

            }
            else if (callGb == 'DelTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridId = 'ktsGrid';
                    ktsGridAlert("삭제를 완료했습니다.");
                    initYn = 'N';
                    setTimeout(function () {
                        fn_Class('selProductGb', '');
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'MappingRegTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridId = 'ktsGrid2';
                    ktsGridAlert("매핑을 완료했습니다.");
                    setTimeout(function () {
                        fn_Search('RIGHT', $("#hdnPolicy").val(), '');
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'MappingDelTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridId = 'ktsGrid2';
                    ktsGridAlert("삭제를 완료했습니다.");
                    setTimeout(function () {
                        fn_Search('RIGHT', $("#hdnPolicy").val(), '');
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'MappingModTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridId = 'ktsGrid2';
                    ktsGridAlert("변경을 완료했습니다.", 1000);

                    var row = ktsGrid2.getRow(cellNo);
                    row.select();

                    ktsGrid2.updateData([{
                        id: cellNo, USEYN: cellUseYn
                    }])
                    .then(function () {
                        //run code after row
                    })
                    .catch(function (error) {
                        //handle error
                    });
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }

        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
    <!-- S: contentsarea -->
    <div class="contentsarea">
        <!-- S:multiplebox -->
        <div class="multiplebox">
            <ul>
                <li>
                    <!-- S:leftbox -->
                    <div class="leftbox">
                        <!-- S:searchbox -->
                        <div class="searchbox">
                            <fieldset>
                                <span class="optionbox">
                                    <label>상품구분</label>
                                    <select id="selProductGb"></select>
                                    <select id="selService" style="width:246px"></select>
                                    <input type="hidden" id="hdnProductGb" />
                                    <input type="hidden" id="hdnService" />
                                </span>
                                <div class="btnSearchWrap">
                                    <input id="btnProductCode" type="button" class="btn-green last" style="float: right;" value="상품코드정보" />
                                    <input id="btnProductReg" type="button" class="btn-green last" style="float: right;" value="상품코드등록" />
                                </div>
                            </fieldset>
                        </div>
                        <!-- E:searchbox -->
                    </div>
                    <!-- E:leftbox -->
                </li>
                <li>
                    <!-- S:rightbox -->
                    <div class="rightbox">
                        <!-- S:searchbox -->
                        <div class="searchbox">
                            <fieldset>
                                <span class="optionbox">
                                    <label>정책</label>
                                    <select id="selPolicy" style="width:240px"></select>
                                    <input type="hidden" id="hdnPolicy" />
                                </span>
                                <div class="btnSearchWrap">
                                    <input id="btnPolicyCode" type="button" class="btn-green last" style="float: right;" value="정책매핑정보" />
                                    <input id="btnPolicyReg" type="button" class="btn-green last" style="float: right;" value="정책코드등록" />
                                </div>
                            </fieldset>
                        </div>
                        <!-- E:searchbox -->
                    </div>
                    <!-- E:rightbox -->
                </li>
            </ul>
            <ul>
                <li>
                    <!-- S:leftbox -->
                    <div class="leftbox">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <!-- S:datalist -->
                                <div class="datalist">
                                    <!-- S:list-top -->
                                    <div class="list-top">
                                        <strong>상품코드 조회 결과</strong>
                                        <label class="searchrowcount"></label>
                                    </div>
                                    <!-- //E:list-top -->
                                    <!-- S:scrollbox -->
                                    <div class="scrollbox">
                                        <div class="table-search" style="display: none;">
                                            <span>
                                                <select id="selField">
                                                    <option value="">선택</option>
                                                    <option value="PRODUCTCD">상품코드</option>
                                                    <option value="PRODUCTNM">상품명</option>
                                                    <option value="SERVICECNM">서비스명</option>
                                                    <option value="PRODUCTGBNM">상품구분명</option>
                                                </select>
                                                : 
                                        <input id="inputValue" disabled="disabled" type="text" />
                                            </span>
                                        </div>
                                        <div id="ktsGrid"></div>
                                    </div>
                                    <!-- E:scrollbox -->
                                </div>
                                <!-- E:datalist -->
                            </div>
                        </div>
                    </div>
                    <!-- E:leftbox -->
                </li>
                <li>
                    <!-- S:rightbox -->
                    <div class="rightbox">
                        <!-- S:datalist -->
                        <div class="datalist">
                            <!-- S:list-top -->
                            <div class="list-top">
                                <strong>
                                <span id="lblServiceRlt"></span></strong>
                                <label class="searchrowcount2"></label>
                            </div>
                            <!-- //E:list-top -->
                            <!-- S:scrollbox -->
                            <div class="scrollbox">
                                <div class="table-search" style="display: none;">
                                    <span>
                                        <select id="selField2">
                                            <option value="">선택</option>
                                            <option value="PRODUCTCD">상품코드</option>
                                            <option value="PRODUCTNM">상품명</option>
                                            <option value="SERVICECNM">서비스명</option>
                                            <option value="PRODUCTGBNM">상품구분명</option>
                                        </select>
                                        : 
                                <input id="inputValue2" disabled="disabled" type="text" />
                                    </span>
                                </div>
                                <div id="ktsGrid2"></div>
                            </div>
                            <!-- E:scrollbox -->
                        </div>
                        <!-- E:datalist -->
                    </div>
                    <!-- E:rightbox -->
                </li>
            </ul>
        </div>
        <!-- E:multiplebox -->
    </div>
    <!-- E: contentsarea -->
</asp:Content>