﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BizJSInDbMng.aspx.cs" Inherits="KTSSolutionWeb.BizJSInDbMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .txtCell {
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
            text-align:right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();

                        var etMonthValue = $('.etMonth').val();  // etMonth 텍스트박스 값 가져오기
                        var etParts = etMonthValue.split('-');   // etMonth 값을 '-'로 분할
                        var etYear = parseInt(etParts[0], 10);   // etMonth 연도 부분 정수로 변환
                        var etMonth = parseInt(etParts[1], 10);  // etMonth 월 부분 정수로 변환

                        if (etMonth === 1) {
                            etYear -= 1;   // etMonth의 월이 1이면 연도 1 감소
                            etMonth = 12;  // etMonth의 월을 12로 설정
                        } else {
                            etMonth -= 1;  // etMonth의 월을 1 감소
                        }

                        // 새로운 stMonth 값을 'YYYY-MM' 형식으로 설정
                        var newStMonthValue = etYear + '-' + (etMonth < 10 ? '0' + etMonth : etMonth);

                        $('.stMonth').val(newStMonthValue);  // stMonth 텍스트박스에 새로운 값 설정
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field();
                });

                $("#inputValue").keyup(function (e) {
                    var fieldEl = document.getElementById("selField");
                    var valueEl = document.getElementById("inputValue");

                    ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnUpload").click(function (e) {
                    fn_PopupUpload();
                });

                $("#btnSelect").click(function (e) {
                    fn_Search('SEARCH');
                });

                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "Biz정산 수입DB관리.xlsx", { sheetName: "Biz정산 수입DB관리", rowData: preparedData });
                });

            }
        };

        var fn_Init = function () {
            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());

            $("#lblOrg").hide();
            $("#<%= txbTeam.ClientID %>").hide();
            $("#<%= btnOrgCd.ClientID %>").hide();

            $(".table-search").css('display', 'none');
            $(".stMonth").show();
            $(".etMonth").show();
            $("#lblDt").show();

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
            $("#tab02").hide();
        }

        var fn_CellInput = function (cell, formatterParams, onRendered) { //텍스트박스
            var cellValue = cell.getValue();
            var cellNo = cell.getRow().getData().id;
            var fieldNm = cell.getField();

            return fn_CellInputBind(cellNo, fieldNm, cellValue);
        }

        var fn_CellInputBind = function (cellNo, fieldNm, value) { //텍스트박스
            var html = '';

            if (fieldNm == 'CAMERA_CNT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, CAMERA_CNTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtPCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'IN_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, IN_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'SUPPLY_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, SUPPLY_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'PAY_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, PAY_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'ADD_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, ADD_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'COMBINED_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, COMBINED_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }
            else if (fieldNm == 'TOTAL_AMT') {
                value = priceToString(value);
                ktsGrid.updateData([{ id: cellNo, TOTAL_AMTTEXT: value }]);
                html = "<input type='text' id='txt" + fieldNm + "' class='txtCell' maxlength='12' onchange='fn_MemoChg(this, \"" + cellNo + "\", \"" + fieldNm + "\");' value='" + value + "'>";
            }

            return html;
        }

        var fn_MemoChg = function (obj, cellNo, fieldNm) {
            var value = obj.value.replace(/[,]/g, '');
            obj.style.setProperty('background-color', '#cfd2d7', 'important');

            value = getNumberOrZero(value);

            // Remove all non-digit characters except for dot
            value = value.replace(/[^\d.]/g, '');

            // Restrict to two decimal places
            var parts = value.split('.');
            if (parts.length > 2) {
                parts = [parts[0], parts[1].substring(0, 2)];
            }
            if (parts[1] && parts[1].length > 2) {
                parts[1] = parts[1].substring(0, 2);
            }
            value = parts.join('.');

            // Add commas for every three digits before the decimal point
            if (parts[0].length > 3) {
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
            value = parts.join('.');

            // Set maximum length (e.g., 15 characters including commas and decimal point)
            var maxLength = 12;
            if (value.length > maxLength) {
                value = value.slice(0, maxLength);
            }

            // Update the input value
            obj.value = value;


            if (fieldNm == 'CAMERA_CNT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, CAMERA_CNTTEXT: obj.value }]);
            }
            else if (fieldNm == 'IN_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, IN_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'SUPPLY_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, SUPPLY_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'PAY_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, PAY_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'ADD_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, ADD_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'COMBINED_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, COMBINED_AMTTEXT: obj.value }]);
            }
            else if (fieldNm == 'TOTAL_AMT') {
                obj.value = value.split('.')[0].replace(/[.]/g, "");
                ktsGrid.updateData([{ id: cellNo, TOTAL_AMTTEXT: obj.value }]);
            }
        }

        //숫자 체크
        function getNumberOrZero(value) {
            // 입력 값이 숫자인지 확인합니다.
            if (typeof value === 'number' && !isNaN(value)) {
                return String(value);
            }
            // 입력 값이 문자열로 되어 있는지 확인하고, 숫자로 변환합니다.
            if (typeof value === 'string') {
                // 첫 번째 소수점만 남기고 이후의 값은 삭제합니다.
                var firstDecimalIndex = value.indexOf('.');
                if (firstDecimalIndex !== -1) {
                    var secondDecimalIndex = value.indexOf('.', firstDecimalIndex + 1);
                    if (secondDecimalIndex !== -1) {
                        value = value.substring(0, secondDecimalIndex);
                    }
                }

                var number = Number(value);
                if (!isNaN(number)) {
                    return String(number);
                }
            }
            // 위 조건을 모두 만족하지 않으면 0을 반환합니다.
            return '0';
        }

        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_CellPopup = function (e, cell) {
            var value = cell.getValue();
            var id = cell.getRow().getData().id;
            var fieldNm = cell.getField();

            if (value != "0") {
                fn_PopupDetail(id, fieldNm);
            }

            return '';
        }


        var fn_CellICnt = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();

            if (value == null) value = '0';

            value = priceToString(value);

            if (value != '0') {
                cell.getElement().style.color = "#077078";
            }
            return value;
        }

        var fn_CellReg = function (cell, formatterParams, onRendered) {
            var html = '';
            cellNo = cell.getRow().getData().id;

            html = "<input id='btnReg' type='button' onclick='fn_Mod(\"" + cellNo + "\");' value='변경' class='btn-save' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";
            html += "<input id='btnDel' type='button' onclick='fn_Del(\"" + cellNo + "\");' value='삭제' class='btn-del' style='height:23px;line-height:0px;font-size:15px;margin-right:10px;'>";

            return html;
        }

        var fn_GridInit = function () {
            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "등록월", field: "REG_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스계약아이디", field: "SAID", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객ID", field: "CUSTOMERID", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 170 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "정책", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "수수료코드", field: "FEECDNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "left", title: "위탁상품명", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "카메라", field: "CAMERA_CNT", sorter: "number", width: 120, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "카메라입력값", field: "CAMERA_CNTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "수입", field: "IN_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "수입입력값", field: "IN_AMTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "공급가", field: "SUPPLY_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "공급가입력값", field: "SUPPLY_AMTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "지급", field: "PAY_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "지급입력값", field: "PAY_AMTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "추가", field: "ADD_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "추가입력값", field: "ADD_AMTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "결합", field: "COMBINED_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "결합입력값", field: "COMBINED_AMTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "right", title: "총지급액", field: "TOTAL_AMT", sorter: "number", width: 120, formatter: fn_CellInput },
                { headerHozAlign: "center", hozAlign: "right", title: "총지급액입력값", field: "TOTAL_AMTTEXT", sorter: "number", width: 120, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "기준", field: "STANDARD", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "등록일", field: "DBCREATEDT", sorter: "string", width: 200, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "변경일", field: "DBLASTUPDATEDT", sorter: "string", width: 200, visible: false },
                { formatter: fn_CellReg, width: 150, minWidth: 50, hozAlign: "center", resizable: false, headerSort: false }
            ];


            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Load = function () {
        }

        var fn_PopupUpload = function () {
            var form = "/Jungsan/BizJSInDbMngUploadPopup";
            var target = "BizJSInDbMngUploadPopup";
            var param = {
            };
            var nWidth = 892;
            var nHeight = 400;

            cmPostPopup(form, target, param, nWidth, nHeight);
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pType: "jungsan"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_Mod = function (id) {
            if (confirm("변경 하시겠습니까?")) {
                callGb = 'modTran';
                fn_Transaction('INDB', 'M', id);
            }
        }

        var fn_Del = function (id) {
            if (confirm("삭제 하시겠습니까?")) {
                callGb = 'delTran';
                fn_Transaction('INDB', 'D', id);
            }
        }

        var fn_SearchChk = function () {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val();
            var OrgCd = $("#<%= hfOrgCd.ClientID %>").val();

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            }

            return true;
        }

        var fn_Field = function () {
            var fieldEl = document.getElementById("selField");
            var valueEl = document.getElementById("inputValue");
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'delTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert("삭제를 완료했습니다.", 1000);
                    setTimeout(function () {
                        fn_Search('SEARCH');
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
            else if (callGb == 'modTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('변경되었습니다.', 1000);
                    setTimeout(function () {
                        fn_Search('SEARCH');
                    }, 1000); // 1000 밀리초(1초) 대기
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'INDBMNG';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Transaction = function (target, jobType, id) {
            var row = ktsGrid.getRow(id);
            var rowData = row.getData();

            var said = rowData.SAID;
            var policyNm = rowData.POLICYNM;

            var cameraCnt = '';
            var inAmt = '';
            var supplyAmt = '';
            var payAmt = '';
            var addAmt = '';
            var combinedAmt = '';
            var totalAmt = '';

            if (target === 'INDB') {
                cameraCnt = rowData.CAMERA_CNTTEXT;
                inAmt = rowData.IN_AMTTEXT;
                supplyAmt = rowData.SUPPLY_AMTTEXT;
                payAmt = rowData.PAY_AMTTEXT;
                addAmt = rowData.ADD_AMTTEXT;
                combinedAmt = rowData.COMBINED_AMTTEXT;
                totalAmt = rowData.TOTAL_AMTTEXT;
            }

            //입력 파라미터
            var aData = [];
            aData[0] = target;
            aData[1] = jobType;
            aData[2] = said;
            aData[3] = policyNm;
            aData[4] = cameraCnt.replace(/,/g, "");
            aData[5] = inAmt.replace(/,/g, "");
            aData[6] = supplyAmt.replace(/,/g, "");
            aData[7] = payAmt.replace(/,/g, "");
            aData[8] = addAmt.replace(/,/g, "");
            aData[9] = combinedAmt.replace(/,/g, "");
            aData[10] = totalAmt.replace(/,/g, "");

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        
        var fn_Search = function (gubun) {
            if (fn_SearchChk()) {
                var pDate1 = '';
                var pDate2 = '';

                if ($("#selDt").val() == 'm') {
                    pDate1 = $("#selMon").val();
                    pDate2 = $("#selMon").val();
                }
                else {
                    pDate1 = $(".stMonth").val();
                    pDate2 = $(".etMonth").val();
                }
          
                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = pDate1;
                aData[2] = pDate2;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);

                $(".table-search").css('display', 'block');
            }
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>구분</label>
                <select id="selDt" style="width:70px">
                    <option selected="selected" value="m">월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label id="lblOrg">조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnUpload" type="button" class="btn-green last" style="float: right;" value="일괄등록" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <div class="list-top">
                <strong>수입DB 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                    </div>
                </div>
            </div>
            <div class="scrollbox">
                <div class="table-search" style="display: none;">
                    <span>
                        <select id="selField">
                            <option value="">선택</option>
                            <option value="REG_MON">등록월</option>
                            <option value="SAID">서비스계약아이디</option>
                            <option value="CUSTOMERNM">고객명</option>
                            <option value="POLICYNM">정책</option>
                            <option value="ORGNM4">국사</option>
                            <option value="FEECDNM">수수료코드</option>
                        </select>
                        : 
                <input id="inputValue" disabled="disabled" type="text" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>