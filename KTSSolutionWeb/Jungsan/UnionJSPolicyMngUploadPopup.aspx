﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnionJSPolicyMngUploadPopup.aspx.cs" Inherits="KTSSolutionWeb.UnionJSPolicyMngUploadPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>정책관리일괄등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-error {
          border: 4px solid #D00;
          color: #590000;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var page = {
                init: function () {
                    fn_Init();
                },
                eventbind: function () {
                    $("#btnModTemp").click(function (e) {
                        fn_ModExcel();
                    });

                    $("#btnNewTemp").click(function (e) {
                        fn_NewExcel();
                    });

                    $("#btnClose").click(function (e) {
                        SendParentsForm();
                    });
                }
            };

        });

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
            fn_Month();
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'POLICYMNG';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_NewExcel = function () {
            callGb = 'newExcel';

            //입력 파라미터
            var aData = [];
            aData[0] = 'NEWEXCEL';
            aData[1] = '';
            aData[2] = '';
            aData[3] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ModExcel = function () {
            callGb = 'modExcel';

            var pDate = $("#selMon").val();

            //입력 파라미터
            var aData = [];
            aData[0] = 'MODEXCEL';
            aData[1] = pDate;
            aData[2] = pDate;
            aData[3] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_excelDown = function (gubun, json, sheetNm, fileNm, type) {
            // Custom headers mapping   
            //var headers = ['기준월', '정책코드', '정책명', '서비스코드', '서비스명', '상품코드', '상품명', '정산명', '기술직지급액', '기술직정책금', '기술직실적급', '기술직포인트', '일반직지급액', '일반직정책금', '일반직실적급', '일반직포인트'];
            var headers = [
                ['기준월', '정책코드', '정책명', '서비스코드', '서비스명', '상품코드', '상품명', '기술직', '기술직', '기술직', '기술직', '기술직', '일반직', '일반직', '일반직', '일반직', '일반직', '추가지급', '추가지급', '추가지급', '추가지급', '추가지급', '포인트', '포인트', '포인트', '포인트', '포인트', '계산기준금액'],
                ['', '', '', '', '', '', '', '무약정', '1년', '2년', '3년', '5년', '무약정', '1년', '2년', '3년', '5년', '무약정', '1년', '2년', '3년', '5년', '무약정', '1년', '2년', '3년', '5년', '']
            ];

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data = jsonData.map(item => [item.MDATE, item.POLICYCD, item.POLICYNM, item.SERVICECD, item.SERVICENM, item.PRODUCTCD, item.PRODUCTNM, item.TECH_AMT0, item.TECH_AMT1, item.TECH_AMT2, item.TECH_AMT3, item.TECH_AMT5, item.GEN_AMT0, item.GEN_AMT1, item.GEN_AMT2, item.GEN_AMT3, item.GEN_AMT5, item.ADD_AMT0, item.ADD_AMT1, item.ADD_AMT2, item.ADD_AMT3, item.ADD_AMT5, item.POINT0, item.POINT1, item.POINT2, item.POINT3, item.POINT5, item.CAL_AMT]);


            // Add headers as the first row
            //data.unshift(headers);
            data.unshift(headers[1]); // Add the second row of headers
            data.unshift(headers[0]); // Add the first row of headers

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            ws['!merges'] = [
                { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "기준월"
                { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "정책코드"
                { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } }, // Merge "정책명"
                { s: { r: 0, c: 3 }, e: { r: 1, c: 3 } }, // Merge "서비스코드"
                { s: { r: 0, c: 4 }, e: { r: 1, c: 4 } }, // Merge "서비스명"
                { s: { r: 0, c: 5 }, e: { r: 1, c: 5 } }, // Merge "상품코드"
                { s: { r: 0, c: 6 }, e: { r: 1, c: 6 } }, // Merge "상품명"
                { s: { r: 0, c: 7 }, e: { r: 0, c: 11 } }, // Merge "기술직"
                { s: { r: 0, c: 12 }, e: { r: 0, c: 16 } },  // Merge "일반직"
                { s: { r: 0, c: 17 }, e: { r: 0, c: 21 } }, // Merge "추가지급"
                { s: { r: 0, c: 22 }, e: { r: 0, c: 26 } },  // Merge "포인트"
                { s: { r: 0, c: 27 }, e: { r: 1, c: 27 } } // Merge "CAL_AMT"
            ];

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 6.78 },
                { wch: 10.89 },
                { wch: 19.44 },
                { wch: 11.11 },
                { wch: 27.22 },
                { wch: 11.11 },
                { wch: 27.22 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 9.67 },
                { wch: 13.67 }
            ];

            if (!ws.A1.c) ws.A3.c = [];
            ws.A3.c.push({ a: "기준월", t: "업로드 제외 DATA는\n행을 삭제하시면 됩니다." });

            if (gubun == 'new') {
                if (!ws.E1.c) ws.E3.c = [];
                ws.E3.c.push({ a: "서비스명", t: "[정책코드][정책명][서비스코드]\n[서비스명][상품코드][상품명]은\n변경하시면 안됩니다." });
            }
            else {
                if (!ws.E1.c) ws.E3.c = [];
                ws.E3.c.push({ a: "서비스명", t: "[기준월][정책코드][정책명][서비스코드]\n[서비스명][상품코드][상품명]은\n변경하시면 안됩니다." });
            }
            

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            // Apply styles to headers
            for (var R = 0; R < headers.length; ++R) {
                for (var C = 0; C < headers[R].length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }

            //0~6  7~20   21_ 26
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 1) {
                        if (C === 0) {
                            if (gubun == 'new') {
                                ws[cell_ref].s = {
                                    font: { sz: 10 }
                                };
                            }
                            else {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    fill: { fgColor: { rgb: "A6A6A6" } }
                                };
                            }
                        }
                        else if (C >= 1 && C <= 6) {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                fill: { fgColor: { rgb: "A6A6A6" } }
                            };
                        }
                        else if (C >= 7 && C <= 20) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C >= 21 && C <= 26) {
                            ws[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 27) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

        }

        var ajaxCallBack = function (json) {
            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'newExcel') {
                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert("기준정보 Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                }
                else {
                    fn_excelDown('new', json, 'Sheet1', '통합정산 정책관리 신규양식 일괄등록 Template', 'xlsx');
                }
            }
            else if (callGb == 'modExcel') {

                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert($("#selMon").val() + "월 Data가 존재하지 않아 변경할 수 없습니다.");
                }
                else {
                    fn_excelDown('mod', json, 'Sheet1', '통합정산 정책관리 변경양식 일괄등록 Template', 'xlsx');
                }
            }
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <div id="msg" class="tabulator"></div>
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">통합정산 일괄등록</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <label class="searchrowcount"></label>
                            <div class="pull-right">
                                <div class="btnset">
                                    <span class="inpbox">
                                        <label>일자</label>
                                        <span class="optionbox">
                                            <select id="selMon" style="width: 110px"></select>
                                        </span>
                                    </span>
                                    <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" type="button" style="float: right;margin: 0px 10px" title="자료등록" class="btn-green last">자료등록</button>
                                    <input id="btnNewTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="신규양식다운로드" />
                                    <input id="btnModTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="변경양식다운로드" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div class="upload-list" style="vertical-align:text-bottom;margin: 50px 0 0 0;">
                            <ul>
                                <li>
                                    <strong style="padding:10px 0px;float:left;margin-left:32px">파일</strong>
                                    <div class="file-field">
                                        <p class="filebox">
                                            <input type="text" id="txtfuReg" style="width:600px" />
                                            <span class="file">
                                                <input type="file" id="fuReg" name="fuReg" onchange="document.getElementById('txtfuReg').value=this.value;" />
                                                <label for="fuReg">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
                                            </span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                         </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUpload" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin: 120px 30px 0 30px; display: grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>