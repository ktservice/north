﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BizJSAddMngUploadPopup.aspx.cs" Inherits="KTSSolutionWeb.BizJSAddMngUploadPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>유선부가상품관리일괄등록</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-error {
          border: 4px solid #D00;
          color: #590000;
        }

        .btn-red {
            display: inline-block;
            min-width: 80px;
            height: 34px;
            padding: 0 15px;
            background: #ed1c24;
            text-align: center;
            line-height: 34px;
            border-radius: 2px;
            color: #fff;
            font-size: 14px;
            font-weight: 800;
            border-style: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ready(function () {
                page.init();
                page.eventbind();
            });

            var page = {
                init: function () {
                    fn_Init();
                },
                eventbind: function () {
                    $("#btnModTemp").click(function (e) {
                        fn_ModExcel();
                    });

                    $("#btnNewTemp").click(function (e) {
                        fn_NewExcel();
                    });

                    $("#btnNotTemp").click(function (e) {
                        fn_NotExcel();
                    });

                    $("#btnDel").click(function (e) {
                        fn_Del();
                    });

                    $("#btnClose").click(function (e) {
                        SendParentsForm();
                    });
                }
            };

        });

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
            fn_Month();
        }

        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'ADDMNG';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Del = function () {
            var target = 'ADD';
            var gubun = $("#selGb").val();
            var moncd = $("#selMon").val();
            var msg = '';

            msg += $("#selGb option:selected").text() + ' ' + $("#selMon").val() + '월';
            msg += ' Data를 삭제 하시겠습니까?';

            if (confirm(msg)) {
                callGb = 'delTran';
                fn_Transaction(target, gubun, moncd);
            }
        }

        var fn_Transaction = function (target, gubun, moncd) {

            //입력 파라미터
            var aData = [];
            aData[0] = target;
            aData[1] = gubun;
            aData[2] = moncd;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'SetTransaction');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_NewExcel = function () {
            callGb = 'newExcel';

            var pDateGb = $("#selGb").val();

            //입력 파라미터
            var aData = [];
            aData[0] = 'ADD_NEWEXCEL';
            aData[1] = pDateGb;
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ModExcel = function () {
            callGb = 'modExcel';

            var pDate = $("#selMon").val();
            var pDateGb = $("#selGb").val();

            //입력 파라미터
            var aData = [];
            aData[0] = 'ADD_MODEXCEL';
            aData[1] = pDateGb;
            aData[2] = '';
            aData[3] = pDate;
            aData[4] = pDate;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_NotExcel = function () {
            callGb = 'notExcel';
            var pDateGb = $("#selGb").val();

            //입력 파라미터
            var aData = [];
            aData[0] = 'ADD_NOTEXCEL';
            aData[1] = pDateGb;
            aData[2] = '';
            aData[3] = '';
            aData[4] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetExcelData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_excelDown = function (gubun, json, sheetNm, fileNm, type) {
            var headers;

            // Custom headers mapping   
            if (gubun === 'not') {
                headers = [
                    ['정산월', '사원번호', '실판매자', '정산내역', '정산내역', '정산내역', '정산내역', '정산내역', '정산내역', '정산내역', '정책', '정산코드', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '오류메시지', '발생일'],
                    ['', '', '', '정산총액', '기본장려금', 'MOT Point', 'MOT 실적급', '프로모션', 'SM패스이미징차감', '기지급 장려금', '적용정책(만원)', '', '계약번호(SAID)', '서비스번호', '판매자조직', '수용국(회선수)', '실판매자명', '고객명', '진행상태', '구분', '접수일', '개통일자', '해지일자', '상품구분코드', '서비스명', '상품명', '약정', 'KT제공사은품', '', '']
                ];
            }
            else {
                headers = [
                    ['정산월', '사원번호', '실판매자', '정산내역', '정산내역', '정산내역', '정산내역', '정산내역', '정산내역', '정산내역', '정책', '정산코드', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역', '계약내역'],
                    ['', '', '', '정산총액', '기본장려금', 'MOT Point', 'MOT 실적급', '프로모션', 'SM패스이미징차감', '기지급 장려금', '적용정책(만원)', '', '계약번호(SAID)', '서비스번호', '판매자조직', '수용국(회선수)', '실판매자명', '고객명', '진행상태', '구분', '접수일', '개통일자', '해지일자', '상품구분코드', '서비스명', '상품명', '약정', 'KT제공사은품']
                ];
            }

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            if (gubun === 'not') {
                data = jsonData.map(item => [item.JS_MON, item.EMPNO, item.EMPNM, item.TOT_JS_AMT, item.BASE_INCENTIVE_AMT, item.MOT_POINT, item.MOT_PERFOR_AMT, item.PROMOTION_AMT, item.SMPASS_AMT, item.INCENTIVE_AMT, item.POLICYNM, item.JSCD, item.SAID, item.SERVICENO, item.BUY_ORGNM, item.ACCEPT_ORGNM, item.BUY_EMPNM, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.RECEIPTDT, item.OPENDT, item.CANCELDT, item.SERVICEGBCD, item.SERVICENM, item.PRODUCTNM, item.PRODUCT_PERIOD, item.KT_GIFT, item.EMSG, item.DBCREATEDT]);
            }
            else {
                data = jsonData.map(item => [item.JS_MON, item.EMPNO, item.EMPNM, item.TOT_JS_AMT, item.BASE_INCENTIVE_AMT, item.MOT_POINT, item.MOT_PERFOR_AMT, item.PROMOTION_AMT, item.SMPASS_AMT, item.INCENTIVE_AMT, item.POLICYNM, item.JSCD, item.SAID, item.SERVICENO, item.BUY_ORGNM, item.ACCEPT_ORGNM, item.BUY_EMPNM, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.RECEIPTDT, item.OPENDT, item.CANCELDT, item.SERVICEGBCD, item.SERVICENM, item.PRODUCTNM, item.PRODUCT_PERIOD, item.KT_GIFT]);
            }

            // Add headers as the first row
            //data.unshift(headers);
            data.unshift(headers[1]); // Add the second row of headers
            data.unshift(headers[0]); // Add the first row of headers

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            if (gubun === 'not') {
                ws['!merges'] = [
                    { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "정산월"
                    { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "사원번호"
                    { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } }, // Merge "실판매자"
                    { s: { r: 0, c: 3 }, e: { r: 0, c: 9 } }, // Merge "정산내역"
                    { s: { r: 0, c: 10 }, e: { r: 0, c: 10 } }, // Merge "정책"
                    { s: { r: 0, c: 11 }, e: { r: 1, c: 11 } }, // Merge "정산코드"
                    { s: { r: 0, c: 12 }, e: { r: 0, c: 27 } }, // Merge "계약내역"
                    { s: { r: 0, c: 28 }, e: { r: 1, c: 28 } }, // Merge "오류메시지"
                    { s: { r: 0, c: 29 }, e: { r: 1, c: 29 } } // Merge "발생일"
                ];


                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 10.67 },
                    { wch: 10.67 },
                    { wch: 9.67 },
                    { wch: 16.67 },
                    { wch: 12.67 },
                    { wch: 19.44 },
                    { wch: 11.11 },
                    { wch: 12.89 },
                    { wch: 11.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 18.89 },
                    { wch: 12.89 },
                    { wch: 10.89 },
                    { wch: 6.78 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 20.89 },
                    { wch: 20.89 },
                    { wch: 6.78 },
                    { wch: 35.44 },
                    { wch: 30.89 },
                    { wch: 10.89 }
                ];

            }
            else {
                ws['!merges'] = [
                    { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } }, // Merge "정산월"
                    { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // Merge "사원번호"
                    { s: { r: 0, c: 2 }, e: { r: 1, c: 2 } }, // Merge "실판매자"
                    { s: { r: 0, c: 3 }, e: { r: 0, c: 9 } }, // Merge "정산내역"
                    { s: { r: 0, c: 10 }, e: { r: 0, c: 10 } }, // Merge "정책"
                    { s: { r: 0, c: 11 }, e: { r: 1, c: 11 } }, // Merge "정산코드"
                    { s: { r: 0, c: 12 }, e: { r: 0, c: 27 } } // Merge "계약내역"
                ];

                // Add styles (example: set column width)
                ws['!cols'] = [
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 9.67 },
                    { wch: 9.67 },
                    { wch: 10.67 },
                    { wch: 10.67 },
                    { wch: 9.67 },
                    { wch: 16.67 },
                    { wch: 12.67 },
                    { wch: 19.44 },
                    { wch: 11.11 },
                    { wch: 12.89 },
                    { wch: 11.89 },
                    { wch: 16.89 },
                    { wch: 16.89 },
                    { wch: 18.89 },
                    { wch: 12.89 },
                    { wch: 10.89 },
                    { wch: 6.78 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 10.89 },
                    { wch: 20.89 },
                    { wch: 20.89 },
                    { wch: 6.78 },
                    { wch: 35.44 }
                ];
            }

            if (gubun === 'mod') {
                if (!ws.A1.c) ws.A3.c = [];
                ws.A3.c.push({ a: "정산월", t: "업로드 제외 DATA는\n행을 삭제하시면 됩니다." });
            }

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            // Apply styles to headers
            for (var R = 0; R < headers.length; ++R) {
                for (var C = 0; C < headers[R].length; ++C) {
                    var cell_address = XLSX.utils.encode_cell({ r: R, c: C });
                    if (!ws[cell_address]) continue;
                    ws[cell_address].s = headerStyle;
                }
            }

            //0~6  7~20   21_ 26
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    if (R > 1) {
                        if (C >= 1 && C <= 2) {
                            if (gubun == 'mod') {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    },
                                    fill: { fgColor: { rgb: "A6A6A6" } }
                                };
                            }
                            else {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    }
                                };
                            }
                        }
                        else if (C == 3 || C == 4) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C >= 6 && C <= 9) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 5) {
                            ws[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 12 || C == 19 || C == 24 || C == 25) {
                            if (gubun == 'mod') {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    },
                                    fill: { fgColor: { rgb: "A6A6A6" } }
                                };
                            }
                            else {
                                ws[cell_ref].s = {
                                    font: { sz: 10 },
                                    alignment: {
                                        vertical: "center",
                                        horizontal: "center"
                                    }
                                };
                            }
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, sheetNm);

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

        }

        var ajaxCallBack = function (json) {
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });
            }
            else if (callGb == 'newExcel') {
                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                }
                else {
                    fn_excelDown('new', json, 'Sheet1', 'Biz정산 유선부가상품관리 신규양식 일괄등록 Template', 'xlsx');
                }
            }
            else if (callGb == 'modExcel') {

                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert($("#selMon").val() + "월 Data가 존재하지 않아 변경할 수 없습니다.");
                }
                else {
                    fn_excelDown('mod', json, 'Sheet1', 'Biz정산 유선부가상품관리 변경양식 일괄등록 Template', 'xlsx');
                }
            }
            else if (callGb == 'notExcel') {

                callGb = '';

                if (JSON.parse(json).length === 0) {
                    ktsGridAlert("Data가 존재하지 않아 파일을 다운로드 할 수 없습니다.");
                }
                else {
                    fn_excelDown('not', json, 'Sheet1', 'Biz정산 유선부가상품관리 미등록', 'xlsx');
                }
            }
            else if (callGb == 'delTran') {
                callGb = '';

                $.each(JSON.parse(json), function (idx, item) {
                    errCd = item.ERRCD;
                    errMsg = item.ERRMSG;
                });

                if (errCd == '00') {
                    ktsGridAlert('삭제되었습니다.');
                }
                else {
                    ktsGridAlert(errMsg, 1000);
                }
            }
        }

        function SendParentsForm() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <div id="msg" class="tabulator"></div>
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">Biz정산 일괄등록</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <label class="searchrowcount"></label>
                            <div class="pull-right">
                                <div class="btnset">
                                    <span class="inpbox">
                                        <label>구분</label>
                                        <span class="optionbox">
                                            <select id="selGb" style="width:80px">
                                                <option selected="selected" value="J">정산월</option>
                                                <option value="R">접수월</option>
                                            </select>
                                            <select id="selMon" style="width: 110px"></select>
                                        </span>
                                    </span>
                                    <button id="btnUpload" runat="server" onserverclick="btnUpload_ServerClick" type="button" style="float: right;margin: 0px 10px" title="자료등록" class="btn-green last">자료등록</button>
                                    <input id="btnDel" type="button" class="btn-red last" style="float: right;margin: 0px 10px" value="월Data삭제" />
                                    <input id="btnNotTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="미등록다운로드" />
                                    <input id="btnNewTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="신규양식다운로드" />
                                    <input id="btnModTemp" type="button" class="btn-green last" style="float: right;margin: 0px 10px" value="변경양식다운로드" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="updPanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div class="upload-list" style="vertical-align:text-bottom;margin: 50px 0 0 0;">
                            <ul>
                                <li>
                                    <strong style="padding:10px 0px;float:left;margin-left:32px">파일</strong>
                                    <div class="file-field">
                                        <p class="filebox">
                                            <input type="text" id="txtfuReg" style="width:600px" />
                                            <span class="file">
                                                <input type="file" id="fuReg" name="fuReg" onchange="document.getElementById('txtfuReg').value=this.value;" />
                                                <label for="fuReg">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
                                            </span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                         </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUpload" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin: 120px 30px 0 30px; display: grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>