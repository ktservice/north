﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UnionJSEmpSupportMng.aspx.cs" Inherits="KTSSolutionWeb.UnionJSEmpSupportMng" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/jquery-ui-1.13.1.js"></script>
    <script type="text/javascript" src="/Resource/js/datepicker-ko.js"></script>
    <script type="text/javascript" src="/Resource/js/jquery.mtz.monthpicker.js"></script>
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.min.js"></script>

    <link href="/Resource/css/jquery-ui-1.13.1.css" rel="stylesheet" />
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    <link href="/Resource/css/tabs.css?20240624" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }

        .headerPay {
            background: #d7d4f073 !important;
        }

        .headerPolicy {
            background: #f0d4d473 !important;
        }
        
        .txtCell {
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
            text-align:right;
            padding-right: 10px !important;
            width: calc(100% - 12px); /* 너비를 2px 줄이기 */
        }

        .txtPCell {
            width: 98%;
            height: 100%;
            text-align:center;
            border: 1px solid #999;
            border-radius: 5px;
            box-sizing: content-box;
            background: #d5d9ab63 !important;
            -webkit-appearance: initial;
            -moz-appearance: initial;
            appearance: auto;
            cursor:pointer;
        }

        #progressBarSet {
            padding:25px;
        }
        
        #progressBarContainer {
            position: fixed;
            display: flex;
            z-index: 99;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: #f9f9f9 !important;
            opacity: 0.8;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        #progressText {
            margin-top: 10px;
            font-weight: bold;
            text-align: center;
            color: #9c340a;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;
        var cellNo = 0;
        var callGb;
        var cellLeft = 0;
        var cellTop = 0;
        var closeDt1 = '';
        var closeDt2 = '';
        var dateNow;
        var today;
        var todayMon;

        var page = {
            init: function () {
                fn_Init();
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                options = {
                    pattern: 'yyyy-mm', // Default is 'mm/yyyy' and separator char is not mandatory
                    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
                };

                $(".stMonth").monthpicker(options);
                $(".etMonth").monthpicker(options);

                $(".etMonth").change(function () {
                });

                $("#selDt").change(function (e) {
                    if (this.value == 'm') {
                        fn_Month();
                        $("#lblDt").hide();
                        $(".stMonth").hide();
                        $(".etMonth").hide();
                        $("#selMon").show();
                    }
                    else {
                        $("#lblDt").show();
                        $(".stMonth").show();
                        $(".etMonth").show();
                        $("#selMon").hide();

                        var etMonthValue = $('.etMonth').val();  // etMonth 텍스트박스 값 가져오기
                        var etParts = etMonthValue.split('-');   // etMonth 값을 '-'로 분할
                        var etYear = parseInt(etParts[0], 10);   // etMonth 연도 부분 정수로 변환
                        var etMonth = parseInt(etParts[1], 10);  // etMonth 월 부분 정수로 변환

                        if (etMonth === 1) {
                            etYear -= 1;   // etMonth의 월이 1이면 연도 1 감소
                            etMonth = 12;  // etMonth의 월을 12로 설정
                        } else {
                            etMonth -= 1;  // etMonth의 월을 1 감소
                        }

                        // 새로운 stMonth 값을 'YYYY-MM' 형식으로 설정
                        var newStMonthValue = etYear + '-' + (etMonth < 10 ? '0' + etMonth : etMonth);

                        $('.stMonth').val(newStMonthValue);  // stMonth 텍스트박스에 새로운 값 설정
                    }
                });

                $("#selMon").change(function (e) {
                    $(".etMonth").val(this.value);
                });

                $("#selField").change(function (e) {
                    fn_Field('selField', 'inputValue');
                });

                $("#selField2").change(function (e) {
                    fn_Field('selField2', 'inputValue2');
                });

                $("#inputValue").keyup(function (e) {
                    fn_SetFilter('selField', 'inputValue');
                });

                $("#inputValue2").keyup(function (e) {
                    fn_SetFilter('selField2', 'inputValue2');
                });

                $(".stMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".stMonth").val($("#hdnStMonth").val());
                    }
                    else {
                        $("#hdnStMonth").val($(".stMonth").val());
                    }

                    return false;
                });

                $(".etMonth").change(function (e) {
                    if (!CompareMonToday(this.value)) {
                        $(".etMonth").val($("#hdnEtMonth").val());
                    }
                    else {
                        $("#hdnEtMonth").val($(".etMonth").val());
                    }

                    return false;
                });

                $("#btnSelect").click(function (e) {

                    if ($('.tabnav a.active').text() === '대상원본 조회') {
                        $('.tabnav a').eq(1).click();
                    }
                    else {
                        $('.tabnav a').eq(0).click();
                    }
                });

                $("#btnExcel").click(function (e) {
                    fn_BatchExcelDown();
                });
                
                $("#gridExcel").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 조사대상관리.xlsx", { sheetName: "통합정산 조사대상관리" });
                });

                $("#gridExcel2").click(function (e) {
                    ktsGrid.download("xlsx", "통합정산 조사대상 대상원본.xlsx", { sheetName: "대상원본" });
                });

                $('.tabnav a').click(function (e) {
                    $('.tabcontent > div').hide().filter(this.hash).fadeIn();
                    $('.tabnav a').removeClass('active');
                    $(this).addClass('active');

                    fn_TabSelect(this.text);

                    return false;
                });
            }
        };

        var fn_Init = function () {
            today = new Date();
            dateNow = today.getDate();
            todayMon = today.getFullYear().toString() + ('0' + (today.getMonth() + 1)).slice(-2);


            $("#hdnStMonth").val($(".stMonth").val());
            $("#hdnEtMonth").val($(".etMonth").val());


            $(".table-search").css('display', 'none');

            fn_Month();
            $("#lblDt").hide();
            $(".stMonth").hide();
            $(".etMonth").hide();
            $("#selMon").show();
            $("#tab02").hide();


            var formattedDate = today.getFullYear().toString() + '-' +
                ('0' + (today.getMonth() + 1)).slice(-2) + '월 ';

            $("#btnExcel").val(formattedDate + $("#btnExcel").val());

        }

        var fn_TabSelect = function (txt) {
            if (txt === '조사대상 조회') {
                fn_Search('CHECK');
            }
            else {
                fn_Search('SEARCH');
            }
        }

        var fn_BatchExcelDown = function () {
            startProgress();
            fn_ExcelSearch('searchExcel');
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        //3자리 콤마
        function priceToString(price) {
            if (numCheck(price)) return price;

            if (price == undefined) {
                return '';
            }
            else {
                if (price.toString().indexOf('.') >= 0) {
                    var priceNum = price.toString().split('.');
                    return priceNum[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + priceNum[1];
                }
                else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }
            }
        }

        //3자리 콤마 소수 2자리
        function priceToString2(price) {
            if (numCheck(price)) return price;

            if (price === undefined) {
                return '';
            } else {
                var formattedPrice = parseFloat(price).toFixed(2);
                return formattedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
        }

        var fn_CellNumber = function (cell, formatterParams, onRendered) {
            var value = cell.getValue();
            value = priceToString(value);
            return value;
        }

        var fn_GridInit = function () {
            $("#selField").empty();
            $("#selField").append("<option value=''>선택</option>");
            $("#selField").append("<option value='ORGNM1'>본부</option>");
            $("#selField").append("<option value='ORGNM2'>지사</option>");
            $("#selField").append("<option value='ORGNM3'>지점</option>");
            $("#selField").append("<option value='EMPNM'>실판매자</option>");

            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서명", field: "ORGNM", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원번호", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "MOT", field: "MOT_POINT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "지원가능", field: "POLICY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(적용정책)", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICEGBCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "RECEIPT_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자명", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "약정", field: "PRODUCT_PERIOD", sorter: "string", width: 80 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
        }

        var fn_Grid2Init = function () {
            $("#selField2").empty();
            $("#selField2").append("<option value=''>선택</option>");
            $("#selField2").append("<option value='ORGNM1'>본부</option>");
            $("#selField2").append("<option value='ORGNM2'>지사</option>");
            $("#selField2").append("<option value='ORGNM3'>지점</option>");
            $("#selField2").append("<option value='EMPNM'>실판매자</option>");

            //field: "id" 명으로 주어야 UPDATE 됨.
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "정산월", field: "JS_MON", sorter: "string", width: 100, frozen: true },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "부서명", field: "ORGNM", sorter: "string", width: 140, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "직무", field: "JOBNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "사원번호", field: "EMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "KTS사번", field: "KTSEMPNO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자", field: "EMPNM", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "MOT", field: "MOT_POINT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "정산총액", field: "TOT_JS_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "지원가능", field: "POLICY_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "right", title: "MOT 실적급", field: "MOT_PERFOR_AMT", sorter: "number", width: 120, formatter: fn_CellNumber },
                { headerHozAlign: "center", hozAlign: "center", title: "비고(적용정책)", field: "POLICYNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "정산코드", field: "JSCD", sorter: "string", width: 140 },
                { headerHozAlign: "center", hozAlign: "center", title: "상품코드", field: "SERVICEGBCD", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수일", field: "RECEIPTDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명", field: "SERVICENM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "서비스번호", field: "SERVICENO", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "고객명", field: "CUSTOMERNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "진행상태", field: "STATUS", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "구분", field: "GUBUN", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "수용국(회선수)", field: "ACCEPT_ORGNM", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "개통일자", field: "OPENDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "해지일자", field: "CANCELDT", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "판매자조직", field: "BUY_ORGNM", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약번호(SAID)", field: "SAID", sorter: "string", width: 130 },
                { headerHozAlign: "center", hozAlign: "center", title: "접수자명", field: "RECEIPT_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "실판매자명", field: "BUY_EMPNM", sorter: "string", width: 150 },
                { headerHozAlign: "center", hozAlign: "center", title: "KT제공사은품", field: "KT_GIFT", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "left", title: "상품명 상세내역", field: "PRODUCTNM", sorter: "string", width: 240 },
                { headerHozAlign: "center", hozAlign: "center", title: "약정", field: "PRODUCT_PERIOD", sorter: "string", width: 80 }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid2', 'fitColumns', true, 'local'); // div의 grid id 값
        }

    
        var fn_Load = function () {
        }

        var fn_PopupOrgTree = function(orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_Oper";
                var target = "OrgTree_Oper";
                var param = {
                    pORGCD: orgcd,
                    pEMPNO: empno,
                    pType: "jungsan"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var fn_OrgTreeBComp = function (orgcd, empno) {
            var BaseMonth = $("#<%= hdnToMonth.ClientID %>").val().substring(0, 7);

            if (BaseMonth.length == 0) {
                ktsGridAlert("기준일자를 선택해주세요.");
                return false;
            } else {
                if (orgcd == "") {
                    orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
                }

                var form = "/Common/OrgTree_BComp";
                var target = "OrgTree_BComp";
                var param = {
                    pCHKNODELV: '9',
                    pOPER: empno,
                    pMdate: BaseMonth,
                    pType: "material"
                };
                var nWidth = 400;
                var nHeight = 720;

                cmPostPopup(form, target, param, nWidth, nHeight);
            }
        }

        var SetOrgCd = function (orgcd, valtype) {
            this.focus();
            document.getElementById("<%=hfOrgCd.ClientID %>").value = orgcd;
            __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }

        var SetOrgCode = function (orgcd) {
            this.focus();
            document.getElementById("<%=hfOfficeCd.ClientID %>").value = orgcd;
           __doPostBack("<%=updPanelOrgCd.ClientID %>", "");
        }
        

        var fn_SearchChk = function () {
            return true;
        }

        var fn_Field = function (field, value) {
            var fieldEl = document.getElementById(field);
            var valueEl = document.getElementById(value);
            valueEl.value = '';

            if (fieldEl.value == "") {
                valueEl.setAttribute('disabled', 'disabled');
                ktsGrid.setFilter('NUM', 'like', '');
            }
            else {
                valueEl.removeAttribute('disabled');
                ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
            }
        }

        var fn_SetFilter = function (field, value) {
            var fieldEl = document.getElementById(field);
            var valueEl = document.getElementById(value);

            ktsGrid.setFilter(fieldEl.value, 'like', valueEl.value);
        }

        var excelTitle = function (pTitle) {
            var title = '';
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2) +
                ('0' + today.getDate()).slice(-2);

            title += formattedDate + '_';
            title += pTitle;

            return title;
        }

        function getLastDayOfMonth(year, month) {
            return new Date(year, month, 0).getDate();
        }

        var ws2;
        var ajaxCallBack = function (json) {
            var obj;
            var errCd = '';
            var errMsg = '';

            if (callGb == 'selMon') {
                obj = $('#' + callGb);
                callGb = '';
                obj.empty();

                $.each(JSON.parse(json), function (idx, item) {
                    obj.append("<option value='" + item.MDATE + "'>" + item.MDATE + "</option>");
                });

            }
            else if (callGb == 'searchExcel') {

                callGb = '';

                ws2 = fn_excelSheet2(json);

                fn_ExcelSearch('checkExcel');
            }
            else if (callGb == 'checkExcel') {
                callGb = '';

                var SheetNm1 = '';
                var SheetNm2 = '';

                SheetNm1 = '조사대상';
                SheetNm2 = '대상원본';

                var title = excelTitle('통합정산 고객지원통합');
                fn_excelDown('', json, SheetNm1, SheetNm2, title, 'xlsx');
            }

        }

        var fn_excelDown = function (gubun, json, sheetNm1, sheetNm2, fileNm, type) {
            var headers;

            headers = ['SA_ID', '본부', '지사', '지점', '직무', '사원번호', '실판매자', 'MOT', '정산총액', '지원가능', 'MOT실적급', '비고(적용정책)', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', '계약번호(SAID)', '접수자명', '실판매자명', 'KT제공사은품', '상품명 상세내역', '약정'];

            var jsonData = JSON.parse(json);

            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.SAID, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.MOT_POINT, item.TOT_JS_AMT, item.POLICY_AMT, item.MOT_PERFOR_AMT, item.POLICYNM, item.JSCD, item.SERVICEGBCD, item.RECEIPTDT, item.SERVICENM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTNM, item.PRODUCT_PERIOD]);

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws1 = XLSX.utils.aoa_to_sheet(data);

            // Add styles (example: set column width)
            ws1['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            for (var C = 0; C < headers.length; ++C) {
                var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                if (!ws1[cell_address]) continue;
                ws1[cell_address].s = headerStyle;
            }

            //0~6  7~20   21_ 26
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws1['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws1[cell_ref]) continue;

                    //if (R > 1)
                    if (R > 0) {
                        if (C == 7) {
                            ws1[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else if (C >= 8 && C <= 10) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 29) {
                            ws1[cell_ref].z = '0'; // 숫자 형식

                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws1[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }


            // Create a new workbook and append the worksheet
            var wb = XLSX.utils.book_new();

            XLSX.utils.book_append_sheet(wb, ws1, sheetNm1);
            XLSX.utils.book_append_sheet(wb, ws2, sheetNm2);

            ws2 = null;

            // Write the workbook to file
            XLSX.writeFile(wb, fileNm + '.' + type);

            endProgress('');
        }

        var fn_excelSheet2 = function (json) {
            var headers;

            headers = ['SA_ID', '본부', '지사', '지점', '직무', '사원번호', '실판매자', 'MOT', '정산총액', '지원가능', 'MOT실적급', '비고(적용정책)', '정산코드', '상품코드', '접수일', '상품명', '서비스번호', '고객명', '진행상태', '구분', '수용국(회선수)', '개통일자', '해지일자', '판매자조직', '계약번호(SAID)', '접수자명', '실판매자명', 'KT제공사은품', '상품명 상세내역', '약정'];

            var jsonData = JSON.parse(json);
            //_PERIOD
            // Convert JSON data to array of arrays
            var data;
            data = jsonData.map(item => [item.SAID, item.ORGNM1, item.ORGNM2, item.ORGNM3, item.JOBNM, item.KTSEMPNO, item.EMPNM, item.MOT_POINT, item.TOT_JS_AMT, item.POLICY_AMT, item.MOT_PERFOR_AMT, item.POLICYNM, item.JSCD, item.SERVICEGBCD, item.RECEIPTDT, item.SERVICENM, item.SERVICENO, item.CUSTOMERNM, item.STATUS, item.GUBUN, item.ACCEPT_ORGNM, item.OPENDT, item.CANCELDT, item.BUY_ORGNM, item.SAID, item.RECEIPT_EMPNM, item.BUY_EMPNM, item.KT_GIFT, item.PRODUCTNM, item.PRODUCT_PERIOD]);

            // Add headers as the first row
            data.unshift(headers);

            // Convert array of arrays to worksheet
            var ws = XLSX.utils.aoa_to_sheet(data);

            // Add styles (example: set column width)
            ws['!cols'] = [
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 22.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 20.89 },
                { wch: 14.89 },
                { wch: 16.89 },
                { wch: 14.89 },
                { wch: 14.89 },
                { wch: 12.89 },
                { wch: 12.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 16.89 },
                { wch: 28.89 },
                { wch: 22.89 },
                { wch: 12.89 }
            ];

            // Style headers
            var headerStyle = {
                font: { name: 'Arial', bold: true, sz: 10, color: { rgb: "000000" } },
                fill: { fgColor: { rgb: "D8E4BC" } },
                alignment: { horizontal: "center", vertical: "center" },
                border: {
                    top: { style: "thin", color: { rgb: "000000" } },
                    bottom: { style: "thin", color: { rgb: "000000" } },
                    left: { style: "thin", color: { rgb: "000000" } },
                    right: { style: "thin", color: { rgb: "000000" } }
                }
            };

            for (var C = 0; C < headers.length; ++C) {
                var cell_address = XLSX.utils.encode_cell({ r: 0, c: C });
                if (!ws[cell_address]) continue;
                ws[cell_address].s = headerStyle;
            }

            //6
            // 셀 색상 지정
            var range = XLSX.utils.decode_range(ws['!ref']);
            for (var R = range.s.r; R <= range.e.r; ++R) {
                for (var C = range.s.c; C <= range.e.c; ++C) {
                    var cell_address = { c: C, r: R };
                    var cell_ref = XLSX.utils.encode_cell(cell_address);
                    if (!ws[cell_ref]) continue;

                    //if (R > 1)
                    if (R > 0) {
                        if (C == 7) {
                            ws[cell_ref].z = '0.00'; // 숫자 형식(소수)

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else if ((C >= 8 && C <= 10)) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "right"
                                }
                            };
                        }
                        else if (C == 29) {
                            ws[cell_ref].z = '0'; // 숫자 형식

                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                        else {
                            ws[cell_ref].s = {
                                font: { sz: 10 },
                                alignment: {
                                    vertical: "center",
                                    horizontal: "center"
                                }
                            };
                        }
                    }
                }
            }

            return ws;

        }


        var fn_Month = function () {
            callGb = 'selMon';

            //입력 파라미터
            var aData = [];
            aData[0] = 'EMPSUPPORT';
            aData[1] = '';

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetDdlMonth');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_ExcelSearch = function (gubun) {
            callGb = gubun;
            var formattedDate = today.getFullYear().toString() +
                ('0' + (today.getMonth() + 1)).slice(-2);

            var pDate = formattedDate;

            if (gubun === 'checkExcel') {
                gubun = 'CHECK';
            }
            else {
                gubun = 'SEARCH';
            }

            //입력 파라미터
            var aData = [];
            aData[0] = gubun;
            aData[1] = pDate;

            var jsonData = JSON.stringify({ aData: aData });
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetGridData');

            ajaxCallEx(jsonData, ajaxUrl, true);
        }

        var fn_Search = function (gubun) {
            if (fn_SearchChk()) {

                var pDate1 = $("#selMon").val().replace(/-/gi, "");

                //입력 파라미터
                var aData = [];
                aData[0] = gubun;
                aData[1] = pDate1;

                var jsonData = JSON.stringify({ aData: aData });

                if (gubun === 'CHECK') {
                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                    fn_GridInit();
                    ktsRowCountClass = 'searchrowcount';

                }
                else {
                    //WebMethod Url
                    var ajaxUrl = grid.getUrl(location.href, 'GetGridData');

                    fn_Grid2Init();
                    ktsRowCountClass = 'searchrowcount2';

                }
                
                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
                $(".table-option").css('display', 'block');
                $(".table-search").css('display', 'block');
            }
        }

        function endProgress(msg) {
            $("#progressBarSet").hide();

            if (msg != '') {
                alert(msg);
            }
        }

        function startProgress() {
            $("#progressBarSet").show();
            $("#progressText").html("잠시만 기다려주세요.");
        }

    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->

	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>
                <span class="optionbox">
                <label>정산월</label>
                <select id="selDt" style="width:80px;display:none;">
                    <option selected="selected" value="m">정산월</option>
                    <option value="b">범위</option>
                </select>
                </span>
                <span class="inpbox">
                    <asp:TextBox ID="txbStDt" runat="server" class="stMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
						<label id="lblDt" style="width:10px">~</label>
			        <asp:TextBox ID="txbEnDt" runat="server" class="etMonth" ReadOnly="true" style="width:30% !important;"></asp:TextBox>
                    <span class="optionbox">
                        <select id="selMon" style="width:110px"></select>
                    </span>
                </span> 
                <span class="inpbox first" style="margin-right:20px">
                    <asp:UpdatePanel ID="updPanelOrgCd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
					        <label>조직</label>
                            <asp:TextBox ID="txbTeam" runat="server" Width="400px" ReadOnly="true"></asp:TextBox>
                            <asp:Button ID="btnOrgCd" runat="server" Visible="false" Enabled="false" class="btn-plus" BorderStyle="None" />
                            <input id="hdnKtsYn" type="hidden" runat="server" />
                            <input id="hdnToMonth" type="hidden" runat="server" />
                            <asp:HiddenField ID="hfOfficeCd" runat="server" OnValueChanged="hfOfficeCd_ValueChanged"  />
                            <asp:HiddenField ID="hfOrgCd" runat="server" OnValueChanged="hfOrgCd_ValueChanged"  />
                            <asp:HiddenField ID="hfBaseDt" runat="server" OnValueChanged="hfBaseDt_ValueChanged"  />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hfOfficeCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfOrgCd" EventName="ValueChanged" />
                            <asp:AsyncPostBackTrigger ControlID="hfBaseDt" EventName="ValueChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" style="float: right;" value="조회" />
                    <input id="btnExcel" type="button" class="btn-black last" style="float: right;margin-right:30px" value="고객지원통합내려받기" />
                    <input id="hdnStMonth" type="hidden" />
                    <input id="hdnEtMonth" type="hidden" />
                    <input id="hdnRm" type="hidden" runat="server" />
                    <input id="hdnAuthId" type="hidden" runat="server" />
                    <input id="hdnAuthNm" type="hidden" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <ul class="tabnav">
                <li><a href="#tab01">조사대상 조회</a></li>
                <li><a href="#tab02">대상원본 조회</a></li>
            </ul>
            <div class="tabcontent">
                <div id="tab01">
                    <div class="list-top">
                        <strong>조사대상 결과</strong>
                        <label class="searchrowcount"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField"></select>
                                : 
                                 <input id="inputValue" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid"></div>
                    </div>
                </div>
                <div id="tab02">
                    <div class="list-top">
                        <strong>대상원본 결과</strong>
                        <label class="searchrowcount2"></label>
                        <div class="pull-right">
                            <div class="btnset">
                                <input id="gridExcel2" type="button" class="btn-green last" value="엑셀" />
                            </div>
                        </div>
                    </div>
                    <div class="scrollbox">
                        <div class="table-search" style="display: none;">
                            <span>
                                <select id="selField2"></select>
                                : 
                                 <input id="inputValue2" disabled="disabled" type="text" />
                            </span>
                        </div>
                        <div id="ktsGrid2"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- E:datalist -->
        <div id="progressBarSet" style="display:none">
            <div id="progressBarContainer">
            <img src="../Resource/images/icon_loadingbar.gif" />
            <h2>다운로드중...</h2>
                <div id="progressText"></div>
            </div>
        </div>
    </div>
	<!-- E: contentsarea -->
</asp:Content>