﻿using KTS.KTSSolution.BSL.ExcelImport;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.Framework.Web;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KTSSolutionWeb
{
    public partial class CertificateUploadPopupByPartners : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "alert('세션이 만료되었습니다.');window.close();", true);
            }
            else
            {
                if (!IsPostBack)
                {
                    PageInit();
                }
            }
        }

        private void PageInit()
        {
            SetDDLYears();

            string strKtsYn = this.Page.Session["KTSYN"].ToString();
            string strAuthId = this.Page.Session["AUTHID"].ToString();
            string strCompanyNm = "";

            if (strKtsYn.Equals("N"))
            {
                strCompanyNm = this.Session["COMPANYNM"].ToString();
            }

            SetDDLCompany(strCompanyNm);

            this.hdfEmpNo.Value = Session["EMPNO"].ToString();
        }

        private void SetDDLYears()
        {
            DataSet ds = new DataSet();

            try
            {
                this.ddlYear.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "CERTIFICATEYEARS";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("Y");
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    this.ddlYear.Items.Add(new ListItem("선택하세요", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        this.ddlYear.Items.Add(new ListItem(ds.Tables[0].Rows[i]["YEARS"].ToString(), ds.Tables[0].Rows[i]["YEARS"].ToString()));
                    }
                }
                else
                {
                    throw new Exception("귀속년도 정보를 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetDDLCompany(string strCompanyNm)
        {
            DataSet ds = new DataSet();

            try
            {
                this.ddlCompany.Items.Clear();

                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "PARTNERSINFO";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add(strCompanyNm); //파라미터 추가 시
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();
                }

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    this.ddlCompany.Items.Add(new ListItem("선택하세요", ""));

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string strCompCd = ds.Tables[0].Rows[i]["COMPCD"].ToString();
                        strCompCd = strCompCd.Substring(0, strCompCd.Length - 2);

                        this.ddlCompany.Items.Add(new ListItem(ds.Tables[0].Rows[i]["COMPNM"].ToString(), strCompCd));
                    }
                }
                else
                {
                    throw new Exception("협력사 정보를 가져오지 못했습니다.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetCertificateNm(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CERTIFICATENMLIST";
            string jobGb = "GET";

            aData.Add("Y");
            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetFileList(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CERTIFICATEFILELISTBYPARTNERS";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCertificateFile(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CERTIFICATEFILEBYPARTNERSDELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}