﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractInfoDetail.aspx.cs" Inherits="KTSSolutionWeb.ContractInfoDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>전자서명 상세 정보</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;

        var page = {
            init: function () {
                fn_GridInit();
                fn_Search();
            },
            eventbind: function () {
                $("#gridExcel").click(function (e) {
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_GridInit = function () {

            var fn_CellPopup = function (cell) {
                var cellValue = cell.getValue();

                var input = "";

                if (cellValue != "" && cellValue != undefined) {
                    input = "<input type='button' onclick='fn_LinkUrl(\"" + cellValue + "\");' value='열기' class='btn-save' style='height:23px;line-height:0px;font-size:15px;'>";
                }

                return input;
            };
            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "string", width: 60 },
                { headerHozAlign: "center", hozAlign: "center", title: "사번", field: "user_key", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약서상태", field: "sign_status", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약중단사유", field: "reject_reson", sorter: "string", width: 120, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약종료사유", field: "end_reson", sorter: "string", width: 120, maxWidth: 400 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약종료일", field: "contract_end_dt", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "링크만료일", field: "link_end_dt", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "PDF링크", field: "link_url", sorter: "string", width: 100, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "PDF파일명", field: "contract_pdf_nm", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "PDF서버명", field: "contract_pdf", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약서파일명", field: "written_file_nm", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약서파일서버명", field: "written_server_file_nm", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "첨부파일명", field: "attached_file_nm", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "첨부파일서버명", field: "attached_server_file_nm", sorter: "string", width: 200 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약키", field: "contract_key", sorter: "string", width: 100, visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("69vh");

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                ValueEl.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');

                    sField = 'NUM';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });
        };

        var fn_Search = function () {
            var contractKey = "<%= Request.Form["pCONTRACT_KEY"] %>";
            var valType = "<%= Request.Form["pVALTYPE"] %>";

            if (contractKey != "" && contractKey != undefined) {
                //입력 파라미터
                var aData = [];
                aData[0] = contractKey;
                aData[1] = valType;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
            else {
                alert("잘못된 접근입니다.");
                window.close();
            }
        };

        var fn_LinkUrl = function (url) {
            var nWidth = 800;
            var nHeight = 1000;

            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var Popupform = createForm(url, null);
            Popupform.target = "ContractPdf";

            var win = window.open("", "ContractPdf", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">계약서 정보 상세 조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
				                <div class="btnset">
                                    <input id="gridExcel" type="button" class="btn-green last" style="float:right;" value="엑셀" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div class="table-search">
                                <span>
                                    <select id="selField" style="font-weight:bold">
                                        <option value="" selected="selected" >선택</option>
                                        <option value="user_key">사번</option>
                                        <option value="sign_status">계약서상태</option>
                                    </select>
                                    <label> : </label>
                                    <input id="inputValue" type="text" value="" disabled="disabled" />
                                </span>
                            </div>
                            <div id="ktsGrid"></div>
                        </div>
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
