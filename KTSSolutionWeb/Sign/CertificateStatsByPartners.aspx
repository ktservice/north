﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CertificateStatsByPartners.aspx.cs" Inherits="KTSSolutionWeb.CertificateStatsByPartners" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#btnAdd").click(function (e) {
                    fn_btnCheck('', 'S');
                });

                $("#gridExcel").click(function (e) {
                    var name = "계약서 현황조회";
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_Load = function () {

        };

        var fn_GridInit = function () {
            var fn_Cell = function (cell) {
                var value = cell.getValue();
                var rowData = cell.getRow().getData();
                var field = cell.getField();

                var compCd = rowData.COMPANYCD;
                var certificateId = rowData.CERTIFICATEID;

                if (compCd == '') {
                    cell.getElement().style.backgroundColor = "#bbffff";
                    cell.getElement().style.fontWeight = "500";
                }

                if (field == "UPLOADCNT") {
                    if (value != "0" && value != "-" && value != "") {
                        value = "<label onclick='fn_DetailPopup(\"" + compCd + "\", \"" + certificateId + "\");' style='cursor:pointer;color:blue;'>" + value + "</label>";
                    }
                }
                else if (field == "NOTYETCNT") {
                    if (value != "0" && value != "-" && value != "") {
                        value = "<label onclick='fn_NotYetDetailPopup(\"" + compCd + "\", \"" + certificateId + "\");' style='cursor:pointer;color:blue;'>" + value + "</label>";
                    }
                }

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "귀속연도", field: "YEARS", sorter: "string", width: 100, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "시작일", field: "STDT", sorter: "string", width: 120, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "인수증명", field: "CERTIFICATENM", sorter: "string", minWidth: 250, maxWidth: 400, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "협력사명", field: "COMPANYNM", sorter: "string", width: 150, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "총계", field: "TOTALCNT", sorter: "number", width: 140, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "등록완료", field: "UPLOADCNT", sorter: "number", width: 140, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", title: "미등록", field: "NOTYETCNT", sorter: "number", width: 140, formatter: fn_Cell },
                { headerHozAlign: "center", hozAlign: "center", field: "COMPANYCD", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "CERTIFICATEID", sorter: "string", visible: false }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            $("#selField").change(function (e) {
                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');

                    sField = 'id';
                    sValue = '';
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            /*----검색 조건 ------- */
            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });

            $("#btnSelect").click(function (e) {
                fn_Search();
            });

            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "협력사 인수증 등록 현황.xlsx", { sheetName: "협력사 인수증 등록 현황" });
            });
        };

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStdt.ClientID %>").val();
            aData[1] = $("#<%= txbEndt.ClientID %>").val();

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

        };

        var fn_UploadPopup = function () {

            var nWidth = 680;
            var nHeight = 765;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {};

            var Popupform = createForm("/Sign/CertificateUploadPopupByPartners", param);

            Popupform.target = "CertificateUploadByPartners";
            var win = window.open("", "CertificateUploadByPartners", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var fn_DetailPopup = function (compCd, certificateId) {

            var nWidth = 1066;
            var nHeight = 628;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pCOMPANYCD: compCd,
                pCERTIFICATEID: certificateId
            };

            var Popupform = createForm("/Sign/CertificateStatsDetailPopupByPartners", param);

            Popupform.target = "CertificateStatsDetailByPartners";
            var win = window.open("", "CertificateStatsDetailByPartners", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var fn_NotYetDetailPopup = function (compCd, certificateId) {
            var nWidth = 766;
            var nHeight = 628;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pCOMPANYCD: compCd,
                pCERTIFICATEID: certificateId
            };

            var Popupform = createForm("/Sign/CertificateNotYetStatsDetailPopupByPartners", param);

            Popupform.target = "CertificateNotYetStatsDetailByPartners";
            var win = window.open("", "CertificateNotYetStatsDetailByPartners", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
					<label>계약시작일</label>
                    <asp:TextBox ID="txbStdt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEndt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                    <input id="btnUpload" type="button" runat="server" class="btn-green last" visible="false" value="인수증등록" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>인수증 등록 현황</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="CERTIFICATENM">인수증명</option>
                            <option value="COMPANYNM">협력사명</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
