﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class CertificateNotYetStatsDetailPopupByPartners : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }

            if (!IsPostBack)
            {
                this.hdfReqCompCd.Value = Request.Form["pCOMPANYCD"].ToString();
                this.hdfReqCertificateId.Value = Request.Form["pCERTIFICATEID"].ToString();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CERTIFICATENOTYETSTATSDETAILBYPARTNERS";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }
    }
}