﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConstractInfoList.aspx.cs" Inherits="KTSSolutionWeb.ConstractInfoList" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>
<%@ Register Src="~/Controls/PagingControl.ascx" TagName="paging" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css" rel="stylesheet" />

    <script type="text/javascript">
        var callGb;

        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_GridInit();
                fn_Load();
            },
            eventbind: function () {
                $("#btnSelect").click(function (e) {
                    fn_Search();
                });

                $("#gridExcel").click(function (e) {
                    var name = "계약서 현황조회";
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                });
            }
        };

        var fn_GridInit = function () {

            var fn_CellPopup = function (cell) {
                var cellValue = cell.getValue();
                var fieldNm = cell.getColumn().getField();
                var rowData = cell.getRow().getData();

                var contractKey = rowData.contract_key;

                var valType = "";

                if (fieldNm == "totalcnt") {
                    valType = "99";
                } else if (fieldNm == "tempcnt") {
                    valType = "0";
                } else if (fieldNm == "sendcnt") {
                    valType = "1";
                } else if (fieldNm == "signcnt") {
                    valType = "2";
                } else if (fieldNm == "fincnt") {
                    valType = "3";
                } else if (fieldNm == "writingcnt") {
                    valType = "4";
                } else if (fieldNm == "dropcnt") {
                    valType = "5";
                } else if (fieldNm == "endcnt") {
                    valType = "6";
                } else if (fieldNm == "terminationcnt") {
                    valType = "7";
                }

                var value;

                if (cellValue == "0" || cellValue == "-" || cellValue == "" || valType == "") {
                    value = cellValue;
                } else {
                    value = "<label onclick='fn_DetailPopup(\"" + contractKey + "\", \"" + valType + "\");' style='cursor:pointer;color:blue;'>" + cellValue + "</label>";
                }

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "string", width: 60 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약키", field: "contract_key", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약서명", field: "contract_nm", sorter: "string", width: 300, maxWidth:400 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약구분", field: "contract_div", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약자구분", field: "contract_user_div", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "귀속년도", field: "vested_year", sorter: "string", width: 100 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약시작일시", field: "contract_start_dt", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "계약종료일시", field: "contract_end_dt", sorter: "string", width: 120 },
                { headerHozAlign: "center", hozAlign: "center", title: "총계", field: "totalcnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "임시저장", field: "tempcnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "발송완료", field: "sendcnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "서명완료", field: "signcnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "계약완료", field: "fincnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "서면등록", field: "writingcnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "계약중단", field: "dropcnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "계약종료", field: "endcnt", sorter: "string", width: 90, formatter: fn_CellPopup },
                { headerHozAlign: "center", hozAlign: "center", title: "계약폐기", field: "terminationcnt", sorter: "string", width: 90, formatter: fn_CellPopup }
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값

            var FieldEl = document.getElementById("selField");
            var ValueEl = document.getElementById("inputValue");

            /*----검색 조건 ------- */
            $("#selField").change(function (e) {

                ValueEl.value = '';

                var sField = FieldEl.value;

                if (sField == "") {
                    ValueEl.setAttribute('disabled', 'disabled');

                    sField = "contract_key";
                    sValue = "";
                }
                else {
                    ValueEl.removeAttribute('disabled');
                }

                ktsGrid.setFilter(sField, 'like', '');
            });

            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
            });
        };

        var fn_Load = function () {
            fn_ControlBind_Years();
        };

        var fn_Search = function () {
            var year = $("#ddlYear option:selected").val();

            //입력 파라미터
            var aData = [];
            aData[0] = year;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');

            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

            $(".table-search").css('display', 'block');
        };

        var fn_ControlBind_Years = function () {
            var selector = $("#ddlYear");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "전체";
            options.selected = true;

            selector.append(options);

            var today = new Date();
            var year = today.getFullYear(); // 년도

            for (var i = 10; i > 0; i--) {
                options = document.createElement("option");

                options.className = "options";
                options.value = year;
                options.text = year;

                selector.append(options);

                year--;

                //2020 이전 데이터 없음
                if (year <= 2019) {
                    break;
                }
            }
        };

        var fn_DetailPopup = function (contractKey, valType) {
            var nWidth = 1200;
            var nHeight = 628;

            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pCONTRACT_KEY: contractKey,
                pVALTYPE: valType
            };

            var Popupform = createForm("/Sign/ContractInfoDetail", param);
            Popupform.target = "ContractInfoDetail";

            var win = window.open("", "ContractInfoDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox">
		    <fieldset>   
				<span class="optionbox">
					<label>귀속연도</label>
                    <select id="ddlYear" style="width:150px;">
                    </select>
                </span>
                <input id="btnSelect" type="button" class="btn-green last" style="float:right;" value="조회" />
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top">
                <strong>조회 결과</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
                    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                    </div>
                </div>
            </div>
            <!-- //E:list-top -->
            
			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search">
                    <span>
                        <select id="selField" style="font-weight:bold">
                            <option value="" selected="selected" >선택</option>
                            <option value="contract_nm">계약서명</option>
                            <option value="contract_user_div">계약자구분</option>
                        </select>
                        <label> : </label>
                        <input id="inputValue" type="text" value="" disabled="disabled" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
	<!-- E: contentsarea -->
</asp:Content>
