﻿using KTS.KTSSolution.BSL.Order;
using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace KTSSolutionWeb.Sign
{
    /// <summary>
    /// ReqFIleUploadHandler의 요약 설명입니다.
    /// </summary>
    public class FileUploadHandler2 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Stream stream = null;

            try
            {
                HttpFileCollection uploadFiles = context.Request.Files;

                for (int i = 0; i < uploadFiles.Count; i++)
                {
                    HttpPostedFile postedFIle = uploadFiles[i];
                    string[] strKey = uploadFiles.AllKeys[i].Split('_');

                    if (strKey.Length != 5)
                    {
                        throw new Exception("잘못된 접근입니다...");
                    }

                    string strCertificateId = strKey[0];
                    string strCompCd = strKey[1];
                    string strOfficeCd = strKey[2];
                    string strEmpNo = strKey[3];

                    stream = postedFIle.InputStream;

                    if (stream.Length == 0)
                        continue;

                    string strFileNm = Path.GetFileName(postedFIle.FileName);
                    string strFileType = postedFIle.ContentType;

                    int nFileLen = Convert.ToInt32(stream.Length);

                    byte[] byteFile = new byte[nFileLen];

                    stream.Read(byteFile, 0, nFileLen);

                    using (OrderMgmt mgmt = new OrderMgmt())
                    {
                        mgmt.InsCertificateFileByPartners(strCertificateId, strFileNm, strCompCd, strOfficeCd, byteFile, strFileType, nFileLen, strEmpNo);
                    }

                    stream.Close();
                    stream.Dispose();
                }

                context.Response.Write(returnValue("00", ""));
            }
            catch(Exception ex)
            {
                context.Response.Write(returnValue("01", ex.Message));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string returnValue(string strResultCd, string strResultMsg)
        {
            string result = string.Empty;

            DataTable dt = new DataTable();

            dt.Columns.Add("SERVICE_ERROR_CD");
            dt.Columns.Add("SERVICE_ERROR_MSG");

            DataRow dr = dt.NewRow();

            dr["SERVICE_ERROR_CD"] = strResultCd;
            dr["SERVICE_ERROR_MSG"] = strResultMsg;

            dt.Rows.Add(dr);

            result = Utility.DataTableToJson(dt);

            return result;
        }
    }
}