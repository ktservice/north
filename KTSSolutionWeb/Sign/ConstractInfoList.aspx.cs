﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KTS.KTSSolution.Framework.Web;
using KTS.KTSSolution.Framework.ExceptionManager;
using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.BSL.Sign;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace KTSSolutionWeb
{
    public partial class ConstractInfoList : PageBase
    {
        #region fields

        #endregion

        #region Constructor

        #endregion

        #region Event

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                Session.Abandon();
                Response.Redirect("/Login");
            }

            if (!IsPostBack)
            {
                MenuTree_Load();
            }
        }
        #endregion

        #endregion

        #region Method

        #region MenuTree_Load
        /// <summary>
        /// MenuTree_Load
        /// </summary>
        private void MenuTree_Load()
        {
            try
            {
                menu.SetMenuData();

                string menuTree = menu.GetMenuTree(Request.Url.PathAndQuery);

                string[] strMenu = menuTree.Split('>');

                for (int i = 0; i < strMenu.Length; i++)
                {
                    if (i == strMenu.Length - 1)
                    {
                        aMenuTree2.InnerText = strMenu[i];
                    }
                    else
                    {
                        aMenuTree1.InnerText += strMenu[i] + " > ";
                    }
                }
            }
            catch (Exception ex)
            {
                KTSException.HandleBSLException(SubSystemType.KTSSolution, ex, this.GetType(), false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
        }
        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = null;

                string strYear = aData[0];

                Regex regex = new Regex("^[0-9]*$");
                bool bChk = regex.IsMatch(strYear);

                if (bChk)
                {
                    string strSql = "";

                    strSql += "select rank() over(order by a.vested_year desc, a.contract_start_dt desc, a.contract_end_dt desc, a.contract_key) as id";
                    strSql += "     , a.contract_key, a.contract_nm, a.contract_div, a.vested_year ";
                    strSql += "     , case when a.contract_user_div = 'W' then '재직' when a.contract_user_div = 'N' then '신규' end contract_user_div ";
                    strSql += "     , to_char(a.contract_start_dt, 'YYYY-MM-DD') as contract_start_dt ";
                    strSql += " , to_char(a.contract_end_dt, 'YYYY-MM-DD') as contract_end_dt ";
                    strSql += "     , coalesce(b.totalcnt, 0) as totalcnt, coalesce(b.tempcnt, 0) as tempcnt, coalesce(b.sendcnt, 0) as sendcnt, coalesce(b.signcnt, 0) as signcnt ";
                    strSql += "     , coalesce(b.writingcnt, 0) as writingcnt, coalesce(b.fincnt, 0) as fincnt, coalesce(b.dropcnt, 0) as dropcnt, coalesce(b.endcnt, 0) as endcnt ";
                    strSql += "     , coalesce(b.terminationcnt, 0) as terminationcnt ";
                    strSql += "  from tb_contract_info a ";
                    strSql += "     left join ( ";
                    strSql += "         select contract_key, sum(totalcnt) as totalcnt, sum(tempcnt) as tempcnt, sum(sendcnt) as sendcnt, sum(signcnt) as signcnt ";
                    strSql += "             , sum(writingcnt) as writingcnt, sum(fincnt) as fincnt, sum(dropcnt) as dropcnt, sum(endcnt) as endcnt, sum(terminationcnt) as terminationcnt ";
                    strSql += "           from( ";
                    strSql += "             select contract_key, 1 as totalcnt ";
                    strSql += "                  , case when sign_status = '0' then 1 else 0 end as tempcnt ";
                    strSql += "                  , case when sign_status = '1' then 1 else 0 end as sendcnt ";
                    strSql += "                  , case when sign_status = '2' then 1 else 0 end as signcnt ";
                    strSql += "                  , case when sign_status = '3' then 1 else 0 end as fincnt ";
                    strSql += "                  , case when sign_status = '4' then 1 else 0 end as writingcnt ";
                    strSql += "                  , case when sign_status = '5' then 1 else 0 end as dropcnt ";
                    strSql += "                  , case when sign_status = '6' then 1 else 0 end as endcnt ";
                    strSql += "                  , case when sign_status = '7' then 1 else 0 end as terminationcnt ";
                    strSql += "               from tb_contract_default_user ";
                    strSql += "           ) a ";
                    strSql += "         group by contract_key ";
                    strSql += "     ) b on a.contract_key = b.contract_key ";
                    strSql += " where 1 = 1 ";
                    strSql += "   and a.contract_div = 'CT200' ";

                    if (!strYear.Equals(""))
                    {
                        strSql += "   and a.vested_year = '" + strYear + "' ";
                    }

                    strSql += " order by a.vested_year desc, a.contract_start_dt desc, a.contract_end_dt desc, a.contract_key; ";

                    using (SignMgmt biz = new SignMgmt())
                    {
                        ds = biz.GetConstractInfo(strSql);
                    }

                    if (ds != null)
                    {
                        result = Utility.DataTableToJson(ds.Tables[0]);
                    }
                    else
                    {
                        bChk = false;
                    }
                }

                if (!bChk)
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }

        #endregion
    }
}