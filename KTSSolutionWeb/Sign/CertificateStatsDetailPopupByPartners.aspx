﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateStatsDetailPopupByPartners.aspx.cs" Inherits="KTSSolutionWeb.CertificateStatsDetailPopupByPartners" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>협력사 인수증 등록현황 상세 조회</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />

    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .list-top {
            font-size: 18px;
            margin: 0px 20px 10px;
        }
        .btn-fileDel {
            border-style:None;
            width: 20px;
            height: 20px;
            line-height: 0;
            color: black;
            padding: 0;
            margin: 0;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var grid;

        var page = {
            init: function () {
                fn_GridInit();
                fn_Search();
            },
            eventbind: function () {
                $("#gridExcel").click(function (e) {
                    ktsGrid.hideColumn("BUTTONS");
                    var name = $("#lblTitle").text();
                    ktsGrid.download("xlsx", name + ".xlsx", { sheetName: name });
                    ktsGrid.showColumn("BUTTONS");
                });

                $("#fileDownLoad").click(function (e) {
                    var compCd = $("#<%= hdfReqCompCd.ClientID %>").val();
                    var certificateId = $("#<%= hdfReqCertificateId.ClientID %>").val();

                    $("#<%= hdfCompCd.ClientID %>").val(compCd);
                    $("#<%= hdfCertificateId.ClientID %>").val(certificateId);
                    $("#<%= hdfSeq.ClientID %>").val('');

                    $("#<%= btnFileDownLoad.ClientID %>").click();
                });

                var FieldEl = document.getElementById("selField");
                var ValueEl = document.getElementById("inputValue");

                $("#selField").change(function (e) {
                    ValueEl.value = '';

                    var sField = FieldEl.value;

                    if (sField == "") {
                        ValueEl.setAttribute('disabled', 'disabled');

                        sField = 'NUM';
                    }
                    else {
                        ValueEl.removeAttribute('disabled');
                    }

                    ktsGrid.setFilter(sField, 'like', '');
                });

                $("#inputValue").keyup(function (e) {
                    ktsGrid.setFilter(FieldEl.value, 'like', ValueEl.value);
                });
            }
        };

        var fn_GridInit = function () {
            var fn_file = function (cell) {
                var rowData = cell.getRow().getData();
                var certificateId = rowData.CERTIFICATEID;
                var compCd = rowData.COMPANYCD;
                var compNm = rowData.COMPANYNM;
                var officeCd = rowData.OFFICECD;
                var officeNm = rowData.OFFICENM;
                var FileInfo = rowData.FILEINFO;

                var FileList = FileInfo.split(',');
                var input = "";

                for (var n = 0; n < FileList.length; n++) {
                    var file = FileList[n].split('/');

                    if (file.length == 4) {

                        if (input.length > 0) {
                            input += "<br />";
                        }
                        var fileNm = file[0];
                        var seq = file[1];
                        var empNo = file[2];
                        var empNm = file[3];

                        input += "<a href='javascript:fn_GetFileData(\"" + certificateId + "\", \"" + compCd + "\", \"" + officeCd + "\", \"" + seq + "\");'>등록자 : " + empNm + "(" + empNo + ")" + fileNm + "</a>";
                        input += "&nbsp<input type='button' class='btn-fileDel' onclick='javascript:return fn_DelFileData(\"" + certificateId + "\", \"" + compCd + "\", \"" + compNm + "\", \"" + officeCd + "\", \"" + officeNm + "\", \"" + seq + "\");' value='X' />";
                    }
                }

                return input;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number" },
                { headerHozAlign: "center", hozAlign: "center", title: "협력사명", field: "COMPANYNM", width: 120, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "국사명", field: "OFFICENM", width: 120, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "center", title: "인수증명", field: "CERTIFICATENM", width: 300, sorter: "string" },
                { headerHozAlign: "center", hozAlign: "left", title: "파일명", field: "FILENM", width: 450, formatter: fn_file },
                { headerHozAlign: "center", hozAlign: "center", title: "CERTIFICATEID", field: "CERTIFICATEID", headerSort: false, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "COMPANYCD", field: "COMPANYCD", headerSort: false, visible: false },
                { headerHozAlign: "center", hozAlign: "center", title: "OFFICECD", field: "OFFICECD", headerSort: false, visible: false },
            ];

            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitData', true, 'local'); // div의 grid id 값
            ktsGrid.setHeight("69vh");
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'DelOk') {
                            alert('선택한 국사의 인수증이 삭제되었습니다.');

                            callGb = '';
                            fn_Search();
                            opener.fn_Search();
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var fn_Search = function () {
            var compCd = $("#<%= hdfReqCompCd.ClientID %>").val();
            var certificateId = $("#<%= hdfReqCertificateId.ClientID %>").val();

            if (certificateId != undefined && certificateId != "") {
                //입력 파라미터
                var aData = [];
                aData[0] = compCd;
                aData[1] = certificateId;

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = grid.getUrl(location.href, 'GetData');

                //grid Bind
                grid.getDataBind(jsonData, ajaxUrl);
            }
            else {
                alert("잘못된 접근입니다.");
                window.close();
            }
        };

        var fn_GetFileData = function (certificateId, compCd, officeCd, seq) {
            $("#<%= hdfCompCd.ClientID %>").val(compCd);
            $("#<%= hdfOfficeCd.ClientID %>").val(officeCd);
            $("#<%= hdfCertificateId.ClientID %>").val(certificateId);
            $("#<%= hdfSeq.ClientID %>").val(seq);

            $("#<%= btnFileDownLoad.ClientID %>").click();
        };

        var fn_DelFileData = function (certificateId, compCd, compNm, officeCd, officeNm, seq) {
            if (certificateId == '' || certificateId == undefined || compCd == '' || compCd == undefined || compNm == '' || compNm == undefined
                || officeCd == '' || officeCd == undefined || officeNm == '' || officeNm == undefined || seq == '' || seq == undefined) {
                alert("잘못된 접근입니다.");
            }
            else {
                if (confirm(compNm + " " + officeNm + " 의 인수증 파일을 삭제하시겠습니까?")) {
                    var aData = [];

                    callGb = "DelOk";

                    aData[0] = certificateId;
                    aData[1] = compCd;
                    aData[2] = officeCd;
                    aData[3] = seq;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "DelCertificateFile");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">협력사 인수증 등록현황 상세 조회</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <strong>조회 결과</strong>
                            <label class="searchrowcount"></label>
			                <div class="pull-right">
				                <div class="btnset">
                                    <input id="fileDownLoad" type="button" class="btn-green" value="일괄다운로드" />
                                    <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
                                    <asp:HiddenField ID="hdfCompCd" runat="server" />
                                    <asp:HiddenField ID="hdfOfficeCd" runat="server" />
                                    <asp:HiddenField ID="hdfCertificateId" runat="server" />
                                    <asp:HiddenField ID="hdfSeq" runat="server" />                                    
                                    <asp:Button ID="btnFileDownLoad" runat="server" OnClick="btnFileDownLoad_Click" style="display:none;" />
				                </div>
			                </div>
                        </div>
                        <!-- //E:list-top -->
                        <!-- S:scrollbox -->
                        <div class="scrollbox">
                            <div id="ktsGrid"></div>
                        </div>
                        <asp:HiddenField ID="hdfReqCompCd" runat="server" />
                        <asp:HiddenField ID="hdfReqCertificateId" runat="server" />
                        <!-- E:scrollbox -->
                    </div>
                    <!-- E:datalist -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/Resource") %>
        </asp:PlaceHolder>
    </form>
</body>
</html>
