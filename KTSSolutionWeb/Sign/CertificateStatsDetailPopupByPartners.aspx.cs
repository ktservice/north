﻿using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;

namespace KTSSolutionWeb
{
    public partial class CertificateStatsDetailPopupByPartners : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }

            if (!IsPostBack)
            {
                this.hdfReqCompCd.Value = Request.Form["pCOMPANYCD"].ToString();
                this.hdfReqCertificateId.Value = Request.Form["pCERTIFICATEID"].ToString();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CERTIFICATESTATSDETAILBYPARTNERS";
            string jobGb = "GET";

            aData.Add("F"); //파라미터 추가 시

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string DelCertificateFile(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "CERTIFICATEFILEBYPARTNERSDELETE";
            string jobGb = "SET";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                result = dbTransaction.GetJsonData();
            }
            else //세션이 끊겼을 경우
            {
                result = resultSession;
            }

            return result;
        }

        protected void btnFileDownLoad_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();

            try
            {
                string resultSession = string.Empty;
                string rptCd = "CERTIFICATEFILEBYPARTNERS";
                string jobGb = "GET";

                string strCompCd = this.hdfCompCd.Value;
                string strOfficeCd = this.hdfOfficeCd.Value;
                string strCertificateId = this.hdfCertificateId.Value;
                string strSeq = this.hdfSeq.Value;

                List<string> aData = new List<string>();

                aData.Add(strCompCd);
                aData.Add(strOfficeCd);
                aData.Add(strCertificateId);
                aData.Add(strSeq);
                aData.Add("F"); //파라미터 추가 시

                DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
                resultSession = dbTransaction.SessionCheck();

                if (resultSession.Equals("")) //세션 유지일 경우 정상
                {
                    ds = dbTransaction.GetDataSet();

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count == 1)
                            {
                                int nFileLen = Convert.ToInt32(ds.Tables[0].Rows[0]["FILESIZE"].ToString());
                                byte[] bFile = new byte[nFileLen];
                                bFile = (byte[])ds.Tables[0].Rows[0]["FILEDATA"];

                                string strFileNm = ds.Tables[0].Rows[0]["FILENM"].ToString();

                                using (FileUtil fileUtil = new FileUtil())
                                {
                                    fileUtil.FileDownLoad(this, bFile, strFileNm, nFileLen);
                                }
                            }
                            else
                            {
                                using (FileUtil fileUtil = new FileUtil())
                                {
                                    fileUtil.FileDownLoad_Multiple(this, ds.Tables[0]);
                                }
                            }
                        }
                    }
                }
                else //세션이 끊겼을 경우
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Error", "alert('오류가 발생했습니다.');", true);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
    }
}