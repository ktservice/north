﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CertificateStats.aspx.cs" Inherits="KTSSolutionWeb.CertificateStats" %>
<%@ Register Src="~/Controls/MenuControl.ascx" TagName="menu" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/tabulator.min.js?20240624"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <script type="text/javascript" src="/Resource/js/xlsx.full.min.js"></script>
    <link href="/Resource/css/tabulator.min.css?20240626" rel="stylesheet" />
    
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .options {
            font-size: 15px;
            font-weight:300;
        }
    </style>
    <script type="text/javascript">
        var grid;
        var callGb;

        $(document).ready(function () {
            var fn_Cellcolor = function (cell, formatterParams, onRendered) {
                var value = cell.getValue();
                var rowData = cell.getData();
                var field = cell.getField();
                var chgyn = rowData.COLORCHGYN;

                if (chgyn == 'Y') {
                    cell.getElement().style.backgroundColor = "#bbffff";
                    cell.getElement().style.fontWeight = "500";
                }

                var orgcd1 = rowData.ORGCD1;
                var orgcd2 = rowData.ORGCD2;
                var orgcd3 = rowData.ORGCD3;
                var orgcd = "";

                if (orgcd3 != "") {
                    orgcd = orgcd3;
                }
                else if (orgcd2 != "") {
                    orgcd = orgcd2;
                }
                else if (orgcd1 != "") {
                    orgcd = orgcd1;
                }

                var certificateId = rowData.CERTIFICATEID;

                if (field == "UPLOADCNT") {
                    if (value != "0" && value != "-" && value != "") {
                        value = "<label onclick='fn_DetailPopup(\"" + orgcd + "\", \"" + certificateId + "\");' style='cursor:pointer;color:blue;'>" + value + "</label>";
                    }
                }
                else if (field == "NOTYETCNT") {
                    if (value != "0" && value != "-" && value != "") {
                        value = "<label onclick='fn_NotYetDetailPopup(\"" + orgcd + "\", \"" + certificateId + "\");' style='cursor:pointer;color:blue;'>" + value + "</label>";
                    }
                }

                return value;
            };

            var Columns = [
                { headerHozAlign: "center", hozAlign: "center", title: "No.", field: "id", sorter: "number", width: 80, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "귀속연도", field: "YEARS", sorter: "string", width: 100, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "시작일", field: "STDT", sorter: "string", width: 120, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "인수증명", field: "CERTIFICATENM", sorter: "string", minWidth: 250, maxWidth: 400, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "본부", field: "ORGNM1", sorter: "string", width: 150, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "지사", field: "ORGNM2", sorter: "string", width: 150, visible: false, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "지점", field: "ORGNM3", sorter: "string", width: 150, visible: false, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "총계", field: "TOTALCNT", sorter: "number", width: 140, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "등록완료", field: "UPLOADCNT", sorter: "number", width: 140, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", title: "미등록", field: "NOTYETCNT", sorter: "number", width: 140, formatter: fn_Cellcolor },
                { headerHozAlign: "center", hozAlign: "center", field: "COLORCHGYN", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "ORGCD1", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "ORGCD2", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "ORGCD3", sorter: "string", visible: false },
                { headerHozAlign: "center", hozAlign: "center", field: "CERTIFICATEID", sorter: "string", visible: false }
            ];
            
            //컬럼 세팅
            ktsColumns = Columns;

            //그리드 초기 세팅
            grid = ktsTabulator;
            grid.gridDefault('ktsGrid', 'fitColumns', true, 'local'); // div의 grid id 값
            //grid.gridDblClick('ktsGrid', 'fitData', true, 'local', rowDoubleClick); // div의 grid id 값

            var ValueEl = document.getElementById("inputValue");

            /*----검색 조건 ------- */
            $("#inputValue").keyup(function (e) {
                ktsGrid.setFilter('CERTIFICATENM', 'like', ValueEl.value);
            });

            $("#btnSelect").click(function (e) {
                fn_Search();
            });            
            
            $("#gridExcel").click(function (e) {
                ktsGrid.download("xlsx", "인수증 등록 현황.xlsx", { sheetName: "인수증 등록 현황" });
            });
        });

        var successCallBack = function (json) {
            if (callGb == 'GetTeam') {
                $("#<%=txbTeam.ClientID %>").val(json);
            }
            callGb = '';
        };

        var ajaxSuccess = function (json) {
            var errCd;
            var errMsg;
            var errRedirect;

            $.each(JSON.parse(json), function (idx, item) {
                if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
                if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
                if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
                //console.log(item.SERVICE_ERROR_CD);
                return false;
            });

            if (errCd == '01') //일반오류
            {
                ktsGridAlert(errMsg);
            }
            else if (errCd == '02') //세션만료
            {
                ktsGridAlert(errMsg);
                location.href = errRedirect;
            }
            else //정상
            {
                $.each(JSON.parse(json), function (key, value) {
                    if (key == 'Table') {
                        ktsGrid.setData(value);
                        ktsGridRowCount(ktsGrid.getDataCount());
                        ktsGridSearch();
                    }
                    else {
                        var orglv = value[0].MINORGLV;

                        if (orglv == "0") {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.hideColumn("ORGNM2");
                            ktsGrid.hideColumn("ORGNM3");
                        }
                        else if (orglv == "1") {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.showColumn("ORGNM2");
                            ktsGrid.hideColumn("ORGNM3");
                        }
                        else if (orglv == "2") {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.showColumn("ORGNM2");
                            ktsGrid.showColumn("ORGNM3");

                        }
                        else {
                            ktsGrid.showColumn("ORGNM1");
                            ktsGrid.showColumn("ORGNM2");
                            ktsGrid.showColumn("ORGNM3");
                        }
                    }
                });

            }
        };

        function fn_Search() {
            //입력 파라미터
            var aData = [];
            aData[0] = $("#<%= txbStdt.ClientID %>").val();
            aData[1] = $("#<%= txbEndt.ClientID %>").val();
            aData[2] = $("#<%= hfOrgCd.ClientID %>").val().replace(/,/gi, "^");

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = grid.getUrl(location.href, 'GetData');
            callGb = 'GetData';
            //grid Bind
            grid.getDataBind(jsonData, ajaxUrl);

        };

        function PopupOrgTree(orgcd, empno) {
            if (orgcd == "") {
                orgcd = document.getElementById("<%=hfOrgCd.ClientID %>").value;
            }

            var nWidth = 400;
            var nHeight = 700;

            var form = "/Common/OrgTree_Oper";
            var target = "OrgTree_Oper";

            var param = {
                pORGCD: orgcd,
                pEMPNO: empno,
                pSHOWORGLV: 3
            };

            cmPostPopup(form, target, param, nWidth, nHeight);
        };

        function SetOrgCd(orgcd, chk) {
            this.focus();
            $("#<%=hfOrgCd.ClientID %>").val(orgcd);

            var aData = [];
            aData[0] = orgcd;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetTeamNm');
            callGb = 'GetTeam';

            ajaxCall(jsonData, ajaxUrl);
        };

        var fn_UploadPopup = function () {

            var nWidth = 680;
            var nHeight = 765;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {};

            var Popupform = createForm("/Sign/CertificateUploadPopup", param);

            Popupform.target = "CertificateUpload";
            var win = window.open("", "CertificateUpload", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var fn_DetailPopup = function (orgcd, certificateId) {

            var nWidth = 1188;
            var nHeight = 628;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pCERTIFICATEID: certificateId
            };

            var Popupform = createForm("/Sign/CertificateStatsDetailPopup", param);

            Popupform.target = "CertificateStatsDetail";
            var win = window.open("", "CertificateStatsDetail", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var fn_NotYetDetailPopup = function (orgcd, certificateId) {

            var nWidth = 885;
            var nHeight = 628;
            var nTop = (window.screen.height / 2) - (nHeight / 2);
            var nLeft = (window.screen.width / 2) - (nWidth / 2);

            if (nTop < 0) nTop = 0;
            if (nLeft < 0) nLeft = 0;

            var param = {
                pORGCD: orgcd,
                pCERTIFICATEID: certificateId
            };

            var Popupform = createForm("/Sign/CertificateNotYetStatsDetailPopup", param);

            Popupform.target = "CertificateNotYetStatsDetailPopup";
            var win = window.open("", "CertificateNotYetStatsDetailPopup", "width=" + nWidth + ",height=" + nHeight + ",top=" + nTop + ",left=" + nLeft);
            Popupform.submit();

            win.focus();
        };

        var ajaxCallBack = function (json) {
            callGb = '';
        };
    </script>
    <uc:menu ID="menu" runat="server" />
    <!--subTitle-->
	<div class="pagenavi">
		<ul>
			<li><a id="aMenuTree1" runat="server"></a></li>
			<li class="last"><a id="aMenuTree2" runat="server" ></a></li><!-- 현재 페이지에 last클래스 추가 -->
		</ul>
	</div>
    <!--end subTitle-->
        
	<!-- S: contentsarea -->
	<div class="contentsarea">
		<!-- S:searchbox -->
        <div class="searchbox" style="margin-top:0;">    
		    <fieldset>       
                <span class="inpbox first">
					<label>계약시작일</label>
                    <asp:TextBox ID="txbStdt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                    <em>~</em>
                    <asp:TextBox ID="txbEndt" runat="server" class="date" Width="120px" ReadOnly="true"></asp:TextBox>
                </span> 
                <span class="inpbox" style="margin-right:20px">
					<label>조직</label>
                    <asp:TextBox ID="txbTeam" runat="server" Width="400px" onclick="PopupOrgTree('', '', '')" ReadOnly="true"></asp:TextBox>
                    <button id="btnOrgCd" runat="server" type="button" class="btn-plus" title="검색">+</button>
                    <asp:HiddenField ID="hfOrgCd" runat="server"/>
				</span>
                <div class="btnSearchWrap">
                    <input id="btnSelect" type="button" class="btn-green last" value="조회" />
                    <input id="btnUpload" type="button" runat="server" class="btn-green last" visible="false" value="인수증등록" />
                </div>
            </fieldset>
        </div>
		<!-- E:searchbox -->

        <!-- S:datalist -->
        <div class="datalist">
            <!-- S:list-top -->
            <div class="list-top" style="margin-bottom:20px">
                <strong>인수증 등록 현황</strong>
                <label class="searchrowcount"></label>
                <div class="pull-right">
				    <div class="btnset">
                        <input id="gridExcel" type="button" class="btn-green last" value="엑셀" />
				    </div>
                </div>
            </div>
            <!-- //E:list-top -->

			<!-- S:scrollbox -->
			<div class="scrollbox">
                <div class="table-search">
                    <span>
                        <label style="font-weight:bold">인수증명</label>
                        <label> : </label>
                        <input id="inputValue" style="margin-top:-4px;" type="text" value="" />
                    </span>
                </div>
                <div id="ktsGrid"></div>
            </div>
			<!-- E:scrollbox -->
        </div>
        <!-- E:datalist -->
    </div>
	<!-- E: contentsarea -->
</asp:Content>
