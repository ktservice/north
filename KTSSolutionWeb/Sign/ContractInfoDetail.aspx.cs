﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using KTS.KTSSolution.BSL.Sign;
using KTS.KTSSolution.Framework.Web;

namespace KTSSolutionWeb
{
    public partial class ContractInfoDetail : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //세션 체크
            if (this.Page.Session.Count <= 5)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SessionCheck", "ktsGridAlert('세션이 만료되었습니다.');window.close();", true);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetData(List<string> aData)
        {
            string result = string.Empty;
            string resultSession = string.Empty;
            string rptCd = "";
            string jobGb = "";

            DbTransaction dbTransaction = new DbTransaction(HttpContext.Current, rptCd, jobGb, aData);
            resultSession = dbTransaction.SessionCheck();

            if (resultSession.Equals("")) //세션 유지일 경우 정상
            {
                DataSet ds = null;

                string strContract_Key = aData[0];
                string strSign_Status = aData[1];

                Regex regex = new Regex("^[0-9]*$");
                bool bChk = false;

                if (!strContract_Key.Equals(""))
                {
                    bChk = true;
                }
                else
                {
                    bChk = regex.IsMatch(strSign_Status);
                }

                if (bChk)
                {
                    string strSql = "";
                    strSql += "select rank() over(order by a.idx) as id, a.contract_key, a.user_key ";
                    strSql += "     , case when a.sign_status = '0' then '임시저장' ";
                    strSql += "            when a.sign_status = '1' then '발송완료' ";
                    strSql += "            when a.sign_status = '2' then '서명완료' ";
                    strSql += "            when a.sign_status = '3' then '계약완료' ";
                    strSql += "            when a.sign_status = '4' then '서면등록' ";
                    strSql += "            when a.sign_status = '5' then '계약중단' ";
                    strSql += "            when a.sign_status = '6' then '계약종료' ";
                    strSql += "            when a.sign_status = '7' then '계약폐기' ";
                    strSql += "       else '' end as sign_status ";
                    strSql += "     , coalesce(a.reject_reson, '') as reject_reson, coalesce(a.end_reson, '') as end_reson ";
                    strSql += "     , to_char(a.contract_end_dt, 'YYYY-MM-DD') as contract_end_dt ";
                    strSql += "     , to_char(a.link_end_dt, 'YYYY-MM-DD') as link_end_dt ";
                    strSql += "     , case when b.link_url is null then '' else 'https://hr-sign.ktservice.co.kr/csl/pdf/download.do?linkUrl=' || b.link_url end as link_url ";
                    strSql += "     , a.contract_pdf_nm, a.contract_pdf, a.written_file_nm, a.written_server_file_nm, a.attached_file_nm, a.attached_server_file_nm ";
                    strSql += "  from tb_contract_default_user a ";
                    strSql += "     left join tb_contract_link b on a.contract_key = b.contract_key and a.user_key = b.user_key ";
                    strSql += " where 1 = 1 ";
                    strSql += "   and a.contract_key = '" + strContract_Key + "' ";

                    if (!strSign_Status.Equals("") && !strSign_Status.Equals("99"))
                    {
                        strSql += "   and a.sign_status = '" + strSign_Status + "' ";
                    }

                    strSql += " order by a.idx; ";

                    using (SignMgmt biz = new SignMgmt())
                    {
                        ds = biz.GetConstractInfo(strSql);
                    }

                    if (ds != null)
                    {
                        result = Utility.DataTableToJson(ds.Tables[0]);
                    }
                    else
                    {
                        bChk = false;
                    }
                }

                if (!bChk)
                {
                    DataTable dt = new DataTable();
                    dt = Utility.DataTableError("01", "잘못된 접근입니다.", "");
                    result = Utility.DataTableToJson(dt);
                }
            }
            else
            {
                result = resultSession;
            }

            return result;
        }
    }
}