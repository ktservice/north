﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertificateUploadPopupByPartners.aspx.cs" Inherits="KTSSolutionWeb.CertificateUploadPopupByPartners" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>인수증 업로드 팝업</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/Common") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <script type="text/javascript" src="/Resource/js/polyfill.js"></script>
    <script type="text/javascript" src="/Resource/js/kts_tabulator.js"></script>
    <link href="/Resource/css/tabulator.min.css" rel="stylesheet" />
    <style type="text/css">
        input[type="button"], input[type="submit"]{
            cursor:pointer;
        }
        .tabulator-alert {
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          z-index: 100;
          height: 100%;
          width: 100%;
          background: rgba(0, 0, 0, 0.4);
          text-align: center;
        }

        .tabulator-alert .tabulator-alert-msg {
          display: inline-block;
          margin: 0 auto;
          padding: 10px 20px;
          border-radius: 10px;
          background: #fff;
          font-weight: bold;
          font-size: 16px;
        }

        .tabulator-alert .tabulator-alert-msg.tabulator-alert-state-msg {
          border: 4px solid #333;
          color: #000;
        }

        .btn-fileDel {
            border-style:None;
            width: 30px;
            height: 30px;
            float:right;
            line-height: 0;
            color: black;
            padding: 0;
            margin: 0;
        }

        .lbl-filelist {
            display: block;
            width: 430px;
            height: 30px;
            background: #fff;
            text-align: left;
            line-height: 30px;
            color: #000;
            font-size: 16px;
            font-weight: 200;
            float:left;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            page.init();
            page.eventbind();
        });

        var page = {
            init: function () {
                fn_Init();
            },
            eventbind: function () {
                $("#btnUpload").click(function (e) {
                    fn_RegFileCheck();
                });

                $("#btnClose").click(function (e) {
                    SendParentsForm();
                });

                $("#<%= ddlYear.ClientID %>").change(function (e) {
                    fn_GetCertificateNm();
                });

                $("#<%= ddlCertificate.ClientID %>").change(function (e) {
                    fn_GetFileList();
                });

                $("#<%= ddlCompany.ClientID %>").change(function (e) {
                    fn_GetFileList();
                });
            }
        };

        var ajaxFileUploadGetUrl = function (url, uploadAddr) {
            var rtnUrl = '';

            if (url.indexOf(':4433') >= 0) {
                rtnUrl = "https://bss.ktservice.co.kr:4433";
            }
            else if (url.indexOf('localhost') >= 0) {
                rtnUrl = "https://localhost:44377";
            }
            else {
                rtnUrl = "https://bss.ktservice.co.kr";
            }

            rtnUrl = rtnUrl + uploadAddr;

            return rtnUrl;
        };

        var successCallBack = function (json) {
            if (callGb == 'GetCertificateNm') {
                callGb = '';
                fn_ControlBind_CertificateNm(json);
            }
            else if (callGb == 'GetFileList'){
                callGb = '';
                fn_ControlBind_FileList(json);
            }
        };

        var ajaxCallBack = function (json) {
            if (callGb != '') {
                $.each(JSON.parse(json), function (idx, item) {
                    if (item.SERVICE_ERROR_CD == '00') {
                        if (callGb == 'DelOk') {
                            alert('선택한 팀의 인수증이 삭제되었습니다.');

                            callGb = '';
                            fn_GetFileList();
                            opener.fn_Search();
                        }
                        else if (callGb == 'FileInsOk') {
                            alert('인수증이 등록 되었습니다.');

                            callGb = '';
                            opener.fn_Search();
                            window.close();
                        }
                    }
                    else {
                        alert(item.SERVICE_ERROR_MSG);
                    }
                });
            }
        };

        var fn_GetCertificateNm = function () {
            var years = $("#<%= ddlYear.ClientID %>").val();

            var aData = [];
            aData[0] = years;

            var jsonData = JSON.stringify({ aData: aData });

            //WebMethod Url
            var ajaxUrl = ajaxCallGetUrl(location.href, 'GetCertificateNm');
            callGb = 'GetCertificateNm';

            ajaxCall(jsonData, ajaxUrl);
        };

        var fn_GetFileList = function () {
            var CertificateId = $("#<%= ddlCertificate.ClientID %>").val();
            var CompInfo = $("#<%= ddlCompany.ClientID %>").val();

            if (CertificateId != "" && CompInfo != "") {
                var Comp = CompInfo.split('_');

                var aData = [];
                aData[0] = CertificateId;
                aData[1] = Comp[0];
                aData[2] = Comp[1];

                var jsonData = JSON.stringify({ aData: aData });

                //WebMethod Url
                var ajaxUrl = ajaxCallGetUrl(location.href, 'GetFileList');
                callGb = 'GetFileList';

                ajaxCall(jsonData, ajaxUrl);
            }
            else {
                $("#divFIleList").empty();
            }
        };

        var fn_RegFileCheck = function () {
            var Years = $("#<%= ddlYear.ClientID %>").val();
            var CertificateId = $("#<%= ddlCertificate.ClientID %>").val();
            var CompInfo = $("#<%= ddlCompany.ClientID %>").val();

            if (Years == "" || Years == undefined) {
                alert("귀속년도를 선택해주세요.");
            }
            else if (CertificateId == "" || CertificateId == undefined) {
                alert("인수증을 선택해주세요.");
            }
            else if (CompInfo == "" || CompInfo == undefined) {
                alert("등록 대상 업체를 선택해주세요.");
            }
            else {
                if (confirm("참조 파일을 저장 하시겠습니까?")) {
                    fn_RegFileData();
                }
            }
        };

        var fn_RegFileData = function () {
            var Files = $("#filebox").find("input[type='file']");

            var Years = $("#<%= ddlYear.ClientID %>").val();
            var CertificateId = $("#<%= ddlCertificate.ClientID %>").val();
            var CompInfo = $("#<%= ddlCompany.ClientID %>").val();
            var EmpNo = $("#<%= hdfEmpNo.ClientID %>").val();

            if (Years == "" || Years == undefined || CertificateId == "" || CertificateId == undefined || CompInfo == "" || CompInfo == undefined) {
                alert(1);
                alert("잘못된 접근입니다.");
            }
            else if (Files.length == 0) {
                alert("선택된 파일이 없습니다.");
            }
            else {
                var formData = new FormData();

                for (var i = 0; i < Files.length; i++) {
                    var keyNm = CertificateId + "_" + CompInfo + "_" + EmpNo + "_" + i;

                    formData.append(keyNm, Files[i].files[0]);
                }

                var ajaxUrl = ajaxFileUploadGetUrl(location.href, "/Sign/FileUploadHandler2.ashx");
                
                callGb = "FileInsOk";

                ajaxCallFileUpload(formData, ajaxUrl);
            }
        };

        var fn_DelFileData = function (certificateId, compCd, compNm, officeCd, officeNm, seq) {
            if (certificateId == '' || certificateId == undefined || compCd == '' || compCd == undefined || compNm == '' || compNm == undefined
                || officeCd == '' || officeCd == undefined || officeNm == '' || officeNm == undefined || seq == '' || seq == undefined) {
                alert("잘못된 접근입니다.");
            }
            else {
                if (confirm(compNm + " " + officeNm + " 의 인수증 파일을 삭제하시겠습니까?")) {
                    var aData = [];

                    callGb = "DelOk";

                    aData[0] = certificateId;
                    aData[1] = compCd;
                    aData[2] = officeCd;
                    aData[3] = seq;

                    var jsonData = JSON.stringify({ aData: aData });

                    //WebMethod Url
                    var ajaxUrl = ajaxCallGetUrl(location.href, "DelCertificateFile");

                    ajaxCallEx(jsonData, ajaxUrl, true);
                }
            }
        };

        var fn_ControlBind_CertificateNm = function (json) {

            var selector = $("#<%= ddlCertificate.ClientID %>");

            selector.empty();

            var options = document.createElement("option");

            options.className = "options";
            options.value = "";
            options.text = "선택해주세요.";

            selector.append(options);

            $.each(JSON.parse(json), function (idx, item) {
                options = document.createElement("option");

                options.className = "options";
                options.value = item.CERTIFICATEID;
                options.text = item.CERTIFICATENM;

                selector.append(options);
            });
        };

        var fn_ControlBind_FileList = function (json) {
            var div = $("#divFIleList");

            div.empty();

            $.each(JSON.parse(json), function (idx, item) {
                var p = document.createElement("p");
                p.setAttribute("style", "margin-left:30px;margin-right:30px;");

                var Label = document.createElement("label");
                Label.setAttribute("class", "lbl-filelist");
                Label.innerText = item.FILENM;

                p.append(Label);

                var Button = document.createElement("input");

                Button.type = "button";
                Button.setAttribute("class", "btn-fileDel");
                Button.setAttribute("onClick", "javascript:return fn_DelFileData(\"" + item.CERTIFICATEID + "\", \"" + item.COMPANYCD + "\", \"" + item.COMPANYNM + "\", \"" + item.OFFICECD + "\", \"" + item.OFFICENM + "\", \"" + item.SEQ + "\");");
                Button.value = "X";

                p.append(Button);

                div.append(p);
            });

        };

        var fn_Init = function () {
            ktsGridId = 'windowpop-wrap';
        };

        function SendParentsForm() {
            window.close();
        };

        var AddFileUpload = function () {
            if (!document.getElementById && !document.createElement)
                return false;

            if (!AddFileUpload.lastAssignedId)
                AddFileUpload.lastAssignedId = 2;

            var divfu = document.getElementById("filebox");

            var newFileBox = document.createElement("p");
            newFileBox.setAttribute("class", "filebox");
            newFileBox.setAttribute("style", "margin-top:5px;margin-left:35px");

            var newFileText = document.createElement("input");
            newFileText.type = "text";
            newFileText.setAttribute("id", "fu" + AddFileUpload.lastAssignedId + "-value");
            newFileText.setAttribute("style", "width:430px;");
            newFileText.style.marginLeft = "0";

            var newFileSpan = document.createElement("span");
            newFileSpan.setAttribute("class", "file");

            var newFile = document.createElement("input");
            newFile.type = "file";

            newFile.setAttribute("onchange", "document.getElementById('fu" + AddFileUpload.lastAssignedId + "-value').value=this.value;");
            newFile.setAttribute("id", "fu" + AddFileUpload.lastAssignedId);
            newFile.setAttribute("name", "fu" + AddFileUpload.lastAssignedId);

            var newFileLabel = document.createElement("label");
            newFileLabel.setAttribute("for", "fu" + AddFileUpload.lastAssignedId);
            newFileLabel.innerText = "찾아보기";

            newFileSpan.appendChild(newFile);
            newFileSpan.appendChild(newFileLabel);

            newFileBox.appendChild(newFileText);
            newFileBox.appendChild(newFileSpan);

            divfu.appendChild(newFileBox);

            AddFileUpload.lastAssignedId++;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div id="windowpop-wrap">
            <!-- S:pop-notice-write -->
            <div class="windowpop pop-notice-write">
                <!-- S:popupwrap -->
                <div class="popupwrap">
                    <div class="title"><strong><span id="lblTitle">인수증 등록</span></strong></div>
                    <button type="button" class="btn-popclose" onclick="window.close(self)">닫기</button>
                </div>
                <!-- E:popupwrap -->
            </div>
            <!-- E:pop-user-registration -->
            <br />

            <!-- S:pop-user-registration -->
            <div class="windowpop">
                <!-- S:popcontents -->
                <div class="popcontents">
                    <!-- S:datalist -->
                    <div class="datalist">
                        <!-- S:list-top -->
                        <div class="list-top">
                            <label class="searchrowcount"></label>
                            <div class="pull-right">
                                <div class="btnset">
                                    <button id="btnUpload" type="button" style="float: right;margin: 0px 10px" title="인수증등록" class="btn-green last">인수증등록</button>
                                </div>
                            </div>
                        </div>
                        <!-- //E:list-top -->
                        <div class="searchbox" style="margin-top:0;">    
		                    <fieldset>
				                <span class="optionbox">
					                <label>귀속년도</label>
                                    <asp:DropDownList ID="ddlYear" runat="server" Width="110px" AutoPostBack="false"></asp:DropDownList>
                                </span>
				                <span class="optionbox">
					                <label>인수증명</label>
                                    <asp:DropDownList ID="ddlCertificate" runat="server" Width="250px" AutoPostBack="false"></asp:DropDownList>
                                </span>
                                <hr class="blank" />
                                <span class="optionbox">
					                <label>업체명</label>
                                    <asp:DropDownList ID="ddlCompany" runat="server" style="width:200px" AutoPostBack="false"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfEmpNo" runat="server" />
                                </span>
                            </fieldset>
                            <div class="upload-list" style="vertical-align:text-bottom;margin: 5px 0 30px 0;height:260px;">
                                <ul>
                                    <li>
                                        <div id="filebox" class="file-field" style="overflow:auto;max-height:250px">
                                            <p class="filebox" style="margin-left:30px;margin-right:30px;">
                                                <input type="text" id="fu1-value" style="width:430px" />
                                                <span class="file">
                                                    <input type="file" id="fu1" name="fu1" onchange="document.getElementById('fu1-value').value=this.value;" />
                                                    <label for="fu1">찾아보기</label><!-- input[file] id와 label의 for 연결 필요 -->
                                                </span>
                                                <button type="button" onclick="AddFileUpload();" class="btn-replysave" style="height:34px;width:40px;float:right">추가</button>
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="upload-list" style="vertical-align:text-bottom;margin: 30px 0 5px 0;height:150px;">
                                <ul>
                                    <li>
                                        <div id="divFIleList" style="overflow:auto;max-height:150px">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- E:datalist -->
                    <!-- S:btncenter -->
                    <div style="margin: 0 30px 0 30px; display: grid;">
                        <input id="btnClose" type="button" class="btn-green last" value="닫기" />
                    </div>
                    <!-- //E:btncenter -->
                </div>
                <!-- E:popcontents -->
            </div>
            <!-- E:pop-user-registration -->
        </div>
    </form>
</body>
</html>