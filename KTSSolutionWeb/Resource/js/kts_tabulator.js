var ktsGrid;
var ktsGridId = 'ktsGrid';
var ktsRowCountClass = 'searchrowcount';
var ktsColumns;
var ktsTabulator = {};
var ktsExcelHeaders = {}; //20221102 엑셀SIZE 관련 추가
var ktsExcelFileNm; //20221102 엑셀SIZE 관련 추가
var ktsExcelSizeYN = "N"; //20221102 엑셀SIZE 관련 추가

ktsTabulator.getUrl = function (url, method) {
    var rtnUrl = '';

    if (url.indexOf('.aspx') >= 0) rtnUrl = url.replace(/#/gi, "") + '/' + method;
    else rtnUrl = url.replace(/#/gi, "") + '.aspx/' + method;

    return rtnUrl;
}


ktsTabulator.gridDefault = function (id, layout, movableColumns, pagination) {
    ktsGrid = new Tabulator('#' + id, {
        height: "50vh",
        layout: layout, //fitColumns, fitData, fitDataTable(fitColumns은 컬럼이 적을 경우, fitData는 컬럼이 많을 경우 또는 Frozen컬럼이 있을 경우)
        tooltips: true,
        tooltipsHeader: true,
        placeholder: "No Data Set",
        columnHeaderVertAlign: "middle",
        columns: ktsColumns,
        movableColumns: movableColumns, //true, false (헤더 이동)
        pagination: pagination, //remote, local, null (페이징구분)
        paginationSize: 50,
        paginationButtonCount: 10,
        paginationSizeSelector: [50, 100, 200, 300],
        //20221102 엑셀SIZE 관련 추가
        downloadReady: function (fileContents, blob) {
            if (ktsExcelSizeYN == "Y") {
                //fileContents - the unencoded contents of the file
                //blob - the blob object for the download
                //custom action to send blob to server could be included here
                /* XLSX content */
                var jsonContent = JSON.parse(fileContents);
                var ws = XLSX.utils.book_new();

                //Starting in the second row to avoid overriding and skipping headers
                jsonContent.unshift(ktsExcelHeaders);

                var filename = 'Data';
                var dataSheet = XLSX.utils.json_to_sheet(jsonContent, { skipHeader: true });

                XLSX.utils.book_append_sheet(ws, dataSheet, filename.replace('/', ''));
                XLSX.writeFile(ws, ktsExcelFileNm + ".xlsx", { bookSST: true, compression: true, bookType: 'xlsx' });

                return null;
            }
            else {
                return blob;
            }
        }
    });
}

ktsTabulator.getDataBind = function (param, url, id, bAsync) {
    if (id != undefined) {
        ktsGridId = id;
    }

    if (bAsync != undefined) {
        bAsync = true;
    }

    $.ajax({
        type: 'post',
        url: url,
        data: param,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //processdata: true,
        //2024.06.11 다중 테이블을 위해 비동기 처리 구분
        async: bAsync,
        success: function (data) {
            var json = data.d;

            ajaxSuccess(json);
        },
        beforeSend: function () {
            $("#updateProgress").css('display', 'block');
        },
        complete: function () {
            $("#updateProgress").css('display', 'none');
        },
        error: function (request, status, error) {
            ajaxFail(request, status, error);
        }
    })
}

ktsTabulator.gridDblClick = function (id, layout, movableColumns, pagination, event) {
    ktsGrid = new Tabulator('#' + id, {
        height: "50vh",
        layout: layout, //fitColumns, fitData, fitDataTable(fitColumns은 컬럼이 적을 경우, fitData는 컬럼이 많을 경우 또는 Frozen컬럼이 있을 경우)
        tooltips: true,
        tooltipsHeader: true,
        placeholder: "No Data Set",
        columnHeaderVertAlign: "middle",
        columns: ktsColumns,
        movableColumns: movableColumns, //true, false (헤더 이동)
        pagination: pagination, //remote, local, null (페이징구분)
        paginationSize: 50,
        paginationButtonCount: 10,
        paginationSizeSelector: [50, 100, 200, 300],
        rowDblClick: event,
        //20221102 엑셀SIZE 관련 추가
        downloadReady: function (fileContents, blob) {
            if (ktsExcelSizeYN == "Y") {
                //fileContents - the unencoded contents of the file
                //blob - the blob object for the download
                //custom action to send blob to server could be included here
                /* XLSX content */
                var jsonContent = JSON.parse(fileContents);
                var ws = XLSX.utils.book_new();

                //Starting in the second row to avoid overriding and skipping headers
                jsonContent.unshift(ktsExcelHeaders);

                var filename = 'Data';
                var dataSheet = XLSX.utils.json_to_sheet(jsonContent, { skipHeader: true });

                XLSX.utils.book_append_sheet(ws, dataSheet, filename.replace('/', ''));
                XLSX.writeFile(ws, ktsExcelFileNm + ".xlsx", { bookSST: true, compression: true, bookType: 'xlsx' });

                return null;
            }
            else {
                return blob;
            }
        }
    });
}

ktsTabulator.gridCollapseM = function (id, layout, movableColumns, pagination) {
    ktsGrid = new Tabulator('#' + id, {
        height: "50vh",
        layout: layout, //fitColumns, fitData, fitDataTable(fitColumns은 컬럼이 적을 경우, fitData는 컬럼이 많을 경우 또는 Frozen컬럼이 있을 경우)
        responsiveLayout: "collapse",
        responsiveLayoutCollapseStartOpen: false,
        tooltips: true,
        tooltipsHeader: true,
        placeholder: "No Data Set",
        columnHeaderVertAlign: "middle",
        columns: ktsColumns,
        movableColumns: movableColumns, //true, false (헤더 이동)
        pagination: pagination, //remote, local, null (페이징구분)
        paginationSize: 50,
        paginationButtonCount: 10,
        paginationSizeSelector: [50, 100, 200, 300],
        //20221102 엑셀SIZE 관련 추가
        downloadReady: function (fileContents, blob) {
            if (ktsExcelSizeYN == "Y") {
                //fileContents - the unencoded contents of the file
                //blob - the blob object for the download
                //custom action to send blob to server could be included here
                /* XLSX content */
                var jsonContent = JSON.parse(fileContents);
                var ws = XLSX.utils.book_new();

                //Starting in the second row to avoid overriding and skipping headers
                jsonContent.unshift(ktsExcelHeaders);

                var filename = 'Data';
                var dataSheet = XLSX.utils.json_to_sheet(jsonContent, { skipHeader: true });

                XLSX.utils.book_append_sheet(ws, dataSheet, filename.replace('/', ''));
                XLSX.writeFile(ws, ktsExcelFileNm + ".xlsx", { bookSST: true, compression: true, bookType: 'xlsx' });

                return null;
            }
            else {
                return blob;
            }
        }
    });
}

ktsTabulator.gridCollapseMtrue = function (id, layout, movableColumns, pagination) {
    ktsGrid = new Tabulator('#' + id, {
        height: "50vh",
        layout: layout, //fitColumns, fitData, fitDataTable(fitColumns은 컬럼이 적을 경우, fitData는 컬럼이 많을 경우 또는 Frozen컬럼이 있을 경우)
        responsiveLayout: "collapse",
        responsiveLayoutCollapseStartOpen: true,
        tooltips: true,
        tooltipsHeader: true,
        placeholder: "No Data Set",
        columnHeaderVertAlign: "middle",
        headerVisible: false,
        columns: ktsColumns,
        movableColumns: movableColumns, //true, false (헤더 이동)
        pagination: pagination, //remote, local, null (페이징구분)
        paginationSize: 50,
        paginationButtonCount: 10,
        paginationSizeSelector: [50, 100, 200, 300],
        //20221102 엑셀SIZE 관련 추가
        downloadReady: function (fileContents, blob) {
            if (ktsExcelSizeYN == "Y") {
                //fileContents - the unencoded contents of the file
                //blob - the blob object for the download
                //custom action to send blob to server could be included here
                /* XLSX content */
                var jsonContent = JSON.parse(fileContents);
                var ws = XLSX.utils.book_new();

                //Starting in the second row to avoid overriding and skipping headers
                jsonContent.unshift(ktsExcelHeaders);

                var filename = 'Data';
                var dataSheet = XLSX.utils.json_to_sheet(jsonContent, { skipHeader: true });

                XLSX.utils.book_append_sheet(ws, dataSheet, filename.replace('/', ''));
                XLSX.writeFile(ws, ktsExcelFileNm + ".xlsx", { bookSST: true, compression: true, bookType: 'xlsx' });

                return null;
            }
            else {
                return blob;
            }
        }
    });
}

var ajaxFail = function (request, status, error) {
    var errmsg = "code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error;
    ktsGridAlert(errmsg, 5000);
}

var ajaxSuccess = function (json) {
    var errCd;
    var errMsg;
    var errRedirect;

    $.each(JSON.parse(json), function (idx, item) {
        if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
        if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
        if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
        //console.log(item.SERVICE_ERROR_CD);
        return false;
    });

    if (errCd == '01') //일반오류
    {
        ktsGridAlert(errMsg);
    }
    else if (errCd == '02') //세션만료
    {
        ktsGridAlert(errMsg);
        location.href = errRedirect;
    }
    else //정상
    {
        ktsGrid.setData(json);
        ktsGridRowCount(ktsGrid.getDataCount());
        ktsGridSearch();
    }
}

var updateFilter = function (fieldEl, typeEl, valueEl) {
    var filterVal = fieldEl.options[fieldEl.selectedIndex].value;
    var typeVal = typeEl.options[typeEl.selectedIndex].value;
    var filter = filterVal;

    if (filterVal == '') {
        typeEl.selectedIndex = 0;
        valueEl.value = '';
        ktsGrid.clearFilter();
    }
    else {
        ktsGrid.setFilter(filter, typeVal, valueEl.value);
    }
}

var ktsGridRowCount = function (count) {
    var val = '';
    val += '(' + count + ' Rows)';
    $("." + ktsRowCountClass).text(val);
}

var ktsGridSearch = function () {
    $(".table-search").css('display', 'block');
}

var ktsGridAlert = function (msg, timeout) {
    var time = 3000;
    if (timeout != undefined) time = timeout;
    $("#" + ktsGridId).append('<div class="tabulator-alert"><div class="tabulator-alert-msg tabulator-alert-state-msg" role="alert">' + msg + '</div></div>');

    setTimeout(function () {
        $(".tabulator-alert").remove();
    }, time);
}

var ktsGridPopup = function (msg, top, left, timeout) {
    var time = 3000;
    if (timeout != undefined) {
        time = timeout;
    }

    $(".tabulator-popup").remove();
    $("#" + ktsGridId).append('<div class="tabulator-popup tabulator-popup-container" style="top: ' + top + 'px; left: ' + left + 'px;">' + msg + '</div>');

    setTimeout(function () {
        $(".tabulator-popup").remove();
    }, time);
}

