﻿var hangleLen = 3; //UTF-8은 3byte, 그 외에는 2byte

var getStringLengthCheck = function (str, checkLength) {
    var getLength = getStringLength(str);

    if (checkLength < getLength) return false;
    else return true;
}


var getStringLength = function (str) {
    var vLength = 0;

    for (var i = 0; i < str.length; ++i) {
        (str.charCodeAt(i) > 127) ? vLength += hangleLen : vLength++;
    }
    return vLength;
}