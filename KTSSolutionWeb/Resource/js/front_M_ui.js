"use strict";

var frontUI = function() {
	var me = this;
	me.body = $("body");
	me.modalStack = [];
	me.modalEl;
	me.recentFocus = []; //modalfocus	
	//me.scrollbarWidth = (window.innerWidth || document.documentElement.offsetWidth) - $("body").width(); //modal Background ScrollBar
	me.scrollTop;
	me.scrollLeft;
	this.firstLoad();

	$(document).on('click', '.modalpop', function(e){
		e.stopImmediatePropagation();
		if($(e.target).closest(".popupwrap").attr("class") !== "popupwrap") {
			me.modalHide();
		}
	});
	$(document).on("keyup", function(e){
		if ( e.keyCode == 27 || e.which == 27 || e.charCode == 27) { //esc
			e.stopImmediatePropagation();
			if(me.modalStack.length < 1) {
				return false; //활성화 된 모달이 없을때 키가눌리면
			}
			me.modalHide();
		}
	});
	
	$(".ickbox").click(function(event){
		event.stopPropagation();
		event.cancelBubble=true;
	});

	$("input.date").datepicker({
		dateFormat: "yy-mm-dd",
		dayNames: [ "일", "월", "화", "수", "목", "금", "토" ],
		dayNamesMin: [ "일", "월", "화", "수", "목", "금", "토" ],
		monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
		monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
		showOtherMonths: false,
		selectOtherMonths: false,
		constrainInput: true,
		prevText: "<",
		nextText: ">",
	});

	$("input.month").monthpicker({
		dateFormat: "yy-mm",
		dayNames: ["일", "월", "화", "수", "목", "금", "토"],
		dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
		monthNames: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
		monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
		changeMonth: true,
		changeYear: true,
		showMonthAfterYear: false,
		constrainInput: false,
		prevText: "<",
		nextText: ">",
		onClose: function (dateText, inst) {
			var month = $("#ui-monthpicker-div .ui-monthpicker-month :selected").val();
			var year = $("#ui-monthpicker-div .ui-monthpicker-year :selected").val();
			$(this).monthpicker("option", "defaultDate", new Date(year, month, 1));
			$(this).monthpicker('setDate', new Date(year, month, 1));
		},
	});
}
frontUI.prototype = {
	firstLoad: function(){
		setTimeout(function(){
			$("#wrap").animate({opacity:1});
		}, 300)
	},
	modalView: function(modalName, recent) {
		var me = this;
		me.modalStack.push(modalName);
		me.modalEl = $("."+me.modalStack.slice(-1)[0]); //마지막 클릭 객체 가져오기
		var modalHeight = me.modalEl.children(".popupwrap").innerHeight();
		var transparentLayer;
		me.recentFocus.push(recent);		
		if($(".modalpop.active").length < 1) {
			me.scrollTop = $(window).scrollTop();
			me.scrollLeft = $(window).scrollLeft();
			me.body.addClass("fixed").css({
				overflow:"hidden", 
				marginTop:"-" + me.scrollTop +"px",
				marginLeft:"-" + me.scrollLeft +"px"
			});
			me.body.append("<div class='pop-transparents-layer'></div>");
			transparentLayer = $(".pop-transparents-layer");
		}
		if(modalHeight > window.innerHeight){
			me.modalEl.css({verticalAlign:"top"});
		}
		me.modalEl.addClass("active").attr("tabindex", 0).focus();
		me.modalScrollbar();
	},
	modalHide: function() {
		var me = this;
		me.modalEl = $("."+me.modalStack.slice(-1)[0]);

		if($(".modalpop.active").length == 1) {
			me.body.removeClass("fixed").css({
				overflow:"auto", 
				marginTop:0,
				marginLeft:0
			});
			$("html, body").scrollTop(me.scrollTop).scrollLeft(me.scrollLeft);
			$(".pop-transparents-layer").remove();
		}
		me.modalStack.pop();
		me.modalEl.removeClass("active");
		me.modalScrollbar();
		me.recentFocus.pop().focus();
	},
	modalResize: function(){
		var me = this;
		var modalEl = $(".modalpop.active");
		var modalHeight = modalEl.children(".popupwrap").innerHeight();
		if(modalHeight > window.innerHeight){
			modalEl.css({verticalAlign:"top"});
		}else {
			modalEl.css({verticalAlign:"middle"});
		}
		me.modalScrollbar();
	},
	modalScrollbar: function() { //browser scrollbar width 
		var me = this;
		if($(".modalpop.active").length > 0){
			if(me.scrollLeft < 1){
				me.body.css({paddingRight:me.scrollbarWidth+"px"});
			}
		}else{
			me.body.css({paddingRight:0});
		}
	},
	
}

var front = new frontUI ();	