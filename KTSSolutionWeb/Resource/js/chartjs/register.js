import {Chart, registerables} from './chart.js';
import Log2Axis from './log2.js';
import './derived-bubble.js';
import analyzer from './analyzer.js';

Chart.register(...registerables);
Chart.register(Log2Axis);
Chart.register(analyzer);
