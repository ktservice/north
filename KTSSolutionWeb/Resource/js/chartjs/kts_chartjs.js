﻿var chartData = [];
var xColNm = [];
var parseJSON;
var chartConfig;
var chartZoomOptions;
var chartOptions;
var chartPlugins = [];
var chartObj;
var chartDataSetNm = '';
var chartResponsive = false; //차트 반응형 유무
var chartDataCnt = 0;

var CHARTTYPET1 = ["바형", "선형", "방사형", "거품형", "극지방형", "레이더형", "도넛형", '파이형'];
var CHARTTYPEV1 = ["bar", "line", "scatter", "bubble", "polarArea", "radar", "doughnut", "pie"];
var CHARTTYPET2 = ["바형", "선형", "방사형", "거품형"];
var CHARTTYPEV2 = ["bar", "line", "scatter", "bubble"];
var CHARTTYPET3 = ["극지방형", "레이더형"];
var CHARTTYPEV3 = ["polarArea", "radar"];
var CHARTTYPET4 = ["선형", "레이더형"];
var CHARTTYPEV4 = ["line", "radar"];

var CHARTDATAGUBUN = ["선로시험기", "속도측정기", "복합형광파워미터", "라쳇렌치", "전동드릴", "위성측정기"];
var CHARTDATAGUBUNCD = "ITEMNM";

var chartObjNm = 'ktsChart';
var chartParentObjNm = 'chartContainer';
var chartComboObjNm = 'chartCombobox';

var chartScrollHeight = '536';
var chartResponsiveHeight = '519';
var chartResponsiveWidth = '0';

var chartWidth;
var chartHeight;
var ctx; //차트 오브젝

var dataSetBdColor = [];
var dataSetBgColor = [];
var dataBdColor;
var dataBgColor;
var datahoverBdColor = "#f5f6fa";

var chartDataBgColor = [
	/*
	'rgba(255, 99, 132, 0.2)',
	'rgba(54, 162, 235, 0.2)',
	'rgba(255, 206, 86, 0.2)',
	'rgba(75, 192, 192, 0.2)',
	'rgba(102, 102, 102, 0.2)',
	'rgba(255, 159, 64, 0.2)',
	'rgba(153, 102, 255, 0.2)'
	*/
	'rgba(78, 121, 167, 0.9)',
	'rgba(233, 137, 41, 0.9)',
	'rgba(225, 87, 89, 0.9)',
	'rgba(118, 183, 178, 0.9)',
	'rgba(89, 161, 79, 0.9)',
	'rgba(210, 114, 148, 0.9)',
	'rgba(176, 122, 161, 0.9)',
	'rgba(237, 201, 72, 0.9)',
	'rgba(156, 117, 95, 0.9)',
	'rgba(186, 179, 172, 0.9)'
];

var chartDataBdColor = [
	/*
	'rgba(255, 99, 132, 1)',
	'rgba(54, 162, 235, 1)',
	'rgba(255, 206, 86, 1)',
	'rgba(75, 192, 192, 1)',
	'rgba(102, 102, 102, 1)',
	'rgba(255, 159, 64, 1)',
	'rgba(153, 102, 255, 1)'
	*/
	'rgba(78, 121, 167, 1)',
	'rgba(233, 137, 41, 1)',
	'rgba(225, 87, 89, 1)',
	'rgba(118, 183, 178, 1)',
	'rgba(89, 161, 79, 1)',
	'rgba(210, 114, 148, 1)',
	'rgba(176, 122, 161, 1)',
	'rgba(237, 201, 72, 1)',
	'rgba(156, 117, 95, 1)',
	'rgba(186, 179, 172, 1)'
];

var chartDataColorIdx = 0;

var objAxesX = '';
var objAxesY = '';

var chartAxesR = {
	display: false,
		angleLines: {
		display: true
	},
	ticks: {
		display: false
	},
	pointLabels: {
		display: true,
			centerPointLabels: true,
				font: {
			size: 10
		}
	}
};

var setChartConfig = function () {
	chartConfig = {
		data: {
			labels: [],
			datasets: []
		},
		plugins: chartPlugins,
		options: chartOptions
	};
}

var setChartZoom = function () {
	chartZoomOptions = {
		limits: {
			x: { },
			y: { }
		},
		pan: {
			enabled: true,
			mode: 'xy',
			onPanStart({ chart, point }) {
				const area = chart.chartArea;
				const w25 = area.width * 0.25;
				const h25 = area.height * 0.25;
				if (point.x < area.left + w25 || point.x > area.right - w25
					|| point.y < area.top + h25 || point.y > area.bottom - h25) {
					return false; // abort
				}
			},
		},
		zoom: {
			wheel: {
				enabled: false,
			},
			pinch: {
				enabled: false
			},
			mode: 'xy',
			onZoomComplete({ chart }) {
				// This update is needed to display up to date zoom level in the title.
				// Without this, previous zoom level is displayed.
				// The reason is: title uses the same beforeUpdate hook, and is evaluated before zoom.
				chart.update('none');
			}
		}
	};
}

var setChartOption = function () {
	chartOptions = {
		maxBarThickness: 30,
		// 컨테이너가 수행 할 때 차트 캔버스의 크기를 조정(dafalut : true)
		responsive: chartResponsive,
		// 크기 조정 이벤트 후 새 크기로 애니메이션하는 데 걸리는 시간(밀리 초) (defalut : 0)
		responsiveAnimationDuration: 1000,
		// (width / height) 크기를 조정할 때 원래 캔버스 종횡비를 유지 (defalut : true)
		maintainAspectRatio: false,
		// 캔버스 종횡비( width / height, 정사각형 캔버스를 나타내는 값) 높이가 속성으로 또는 스타일 통해 명시적으로 정의된 경우이 옵션은 무시
		aspectRatio: 2.4,
		// 크기 조정이 발생할 때 호출
		onResize: function (obj) {
			//console.log(obj.height);
		},
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		onClick: (event, elements, chart) => {
			if (elements[0]) {
				var datasetIndex = elements[0].datasetIndex;
				var index = elements[0].index;
				var msg = '[' + chart.data.labels[index] + ']';
				msg += ' ' + chart.data.datasets[datasetIndex].label;
				msg += ' : ' + chart.data.datasets[datasetIndex].data[index];

				ktsChartAlert(msg);
            }
		},
		layout: {
			padding: {
				left: 0,
				right: 0,
				top: 0,
				bottom: 0
			}
		},
		elements: {
			line: {
				fill: true,
				tension: 0.4
			}
		},
		scales: {
			x: {
				display: true,
				title: {
					display: true,
					text: ''
				},
				offset: true,
				ticks: {
					color: 'rgba(60, 59, 60, 1)',
					font: { // font 설정
						//weight: 'bold',
						size: '12px',
					},
				},
				grid: { // x축을 기준으로 그려지는 선(세로선)에 대한 설정입니다.
					display: true, // 선이 아예 안 그려지게 됩니다.
					drawTicks: true, // 눈금 표시 여부를 지정합니다.
					tickLength: 4, // 눈금 길이를 지정합니다.
					color: '#e8e6e6',
				},
			},
			y: {
				display: true,
				title: {
					display: true,
					text: ''
				},
				offset: true,
				padding: {
					top: 10
				},
				ticks: {
					color: 'rgba(116, 116, 116, 1)',
					font: { // font 설정
						//weight: 'bold',
						size: '12px',
					},
				},
				grid: { // x축을 기준으로 그려지는 선(세로선)에 대한 설정입니다.
					display: true, // 선이 아예 안 그려지게 됩니다.
					drawTicks: true, // 눈금 표시 여부를 지정합니다.
					tickLength: 4, // 눈금 길이를 지정합니다.
					color: '#e8e6e6',
				},
				afterDataLimits: (scale) => {
					// y축의 최대값은 데이터의 최대값에 딱 맞춰져서 그려지므로
					// y축 위쪽 여유공간이 없어 좀 답답한 느낌이 들 수 있는데요,
					// 이와 같이 afterDataLimits 콜백을 사용하여 y축의 최대값을 좀 더 여유있게 지정할 수 있습니다!
					scale.max = scale.max * 1.05;
				},
			},
			r: chartAxesR
		},
		plugins: {
			legend: { // 범례 사용
				display: true,
				labels: {
					render: 'label',
					arc: true,
					fontColor: '#000',
					position: 'outside',
					color: 'rgb(0, 0, 0)',
					padding: 10,
					usePointStyle: true,
					pointStyle:'circle',
					boxWidth: 2,
					boxHeight: 2,
				},
				align: 'start'
			},
			title: {
				display: true,
				text: '',
				font: { // font 설정
					weight: 'bold',
					size: '20px',
				},
				color: '#1a6b63', // font color
				align: 'start',
			},
			tooltip: { // 기존 툴팁 사용
				enabled: true,
				position: 'nearest',
				usePointStyle: true,
			},
			animation: { // 차트 애니메이션 사용 안 함 (옵션)
				duration: 100,
			},
			datalabels: { // datalables 플러그인 세팅
				formatter: function (value, context) {
					var idx = context.dataIndex; // 각 데이터 인덱스
					var val = value;

					//// 출력 텍스트
					//return context.chart.data.labels[idx] + value;
					return val;
				},
				backgroundColor: function (context) {
					return '';
				},
				color: 'black',
				borderColor: '',
				borderRadius: 2,
				borderWidth: 1,
				offset: 2,
				padding: 2,
				align: 'start',
				anchor: function (context) {
					var value = context.dataset.data[context.dataIndex];
					return value > 0 ? 'end' : 'start';
				},
				font: function (context) {
					var w = context.chart.width;
					return {
						size: w < 512 ? 10 : 12,
						family: "'kt', 'Dotum', 'Arial', sans-serif"
						//weight: 'bold',
					}
				},
				display: true
			},
			zoom: chartZoomOptions,
			colorschemes: {
				scheme: 'office.Foundry6',
			},
			annotation: {
				annotations: {
					annotationLabel
				}
			},
		},
	}
}

var annotationLabel = {
	type: 'label',
	backgroundColor: 'rgba(95, 95, 95, 0.5)',
	borderColor: 'rgba(95, 95, 95, 1)',
	color: 'rgba(255, 255, 255, 1)',
	borderRadius: 6,
	content: '',
	drawTime: 'afterDraw',
	borderWidth: (ctx) => autoScaling(ctx, 'borderWidth', 1),
	font: (ctx) => autoScaling(ctx, 'font', 14),
	padding: (ctx) => autoScaling(ctx, 'padding', 10),
	position: {
		x: 'center',
		y: 'end'
	},
	yValue: 0,
};

function autoScaling(ctx, option, origValue) {
	const { chart } = ctx;
	const { width, height } = chart.chartArea;
	const hypo = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
	let size, value;
	if (!ctx.size) {
		ctx.size = size = hypo;
		value = origValue;
	} else {
		size = ctx.size;
		value = hypo / size * origValue;
	}
	if (option === 'font') {
		return { size: value };
	}
	return value;
}


var setChartDataLabels = function (keyNm, labelJson) {

	if (labelJson == undefined || labelJson == null) {
		labelJson = '';
    }

	if (labelJson != '') {
		chartConfig.data.labels = getChartLabelValue(labelJson, keyNm);
	}
	else {
		chartConfig.data.labels = getChartLabelValue(parseJSON, keyNm);
    }
	
	xColNm = chartConfig.data.labels;
}

var setDataSetThema = function (type, label, chartData, yKey, colorSet) {

	var bW = 1;
	var order = 1;

	if (type == 'line') {
		bW = 1;
		order = 1;
	}
	else {
		order = 2;
	}

	if (!colorSet) {
		setColor();
	}

	var newDataset = {
		type: type,
		label: label,
		borderWidth: bW, //경계선 굵기
		data: chartData,
		parsing: {
			yAxisKey: yKey
		},
		fill: true,
		pointStyle: 'rectRounded',
		order: order
	}

	return newDataset;
}

var setDataSet = function (type, label, chartData, yKey, colorSet) {

	var bW = 1;
	var order = 1;

	if (type == 'line') {
		bW = 1;
		order = 1;
	}
	else {
		order = 2;
    }

	if (!colorSet) {
		setColor();
	}

	var newDataset = {
		type: type,
		label: label,
		borderColor: dataBdColor,
		backgroundColor: dataBgColor,
		hoverBorderColor: datahoverBdColor,
		borderWidth: bW, //경계선 굵기
		data: chartData,
		parsing: {
			yAxisKey: yKey
		},
		fill: true,
		pointStyle: 'rectRounded',
		order: order
	}

	return newDataset;
}

//----start combobox
var setSelChartDataSet = function (obj, title, val) {
	$("#" + obj + "").append("<option value='" + val+"'>" + title+"</option>");
}

var setSelChart = function (obj, title, val, firstNm) {
	var arrTitle = title;
	var arrValue = val;

	$("#" + obj + "").empty();

	if (firstNm != '') {
		$("#" + obj + "").append("<option value=''>" + firstNm + "</option>");
	}

	for (var i = 0; i < arrValue.length; i++) {
		$("#" + obj + "").append("<option value='" + arrValue[i] + "'>" + arrTitle[i] + "</option>");
	}

	$("#" + obj + " option:eq(0)").prop("selected", true);
}

var setSelChartDataSetList = function (comboId) {
	$("#" + comboId + "").empty(); //초기화
	$("#" + comboId + "").append("<option value=''>데이터 선택</option>");

	for (var i = 0; i < CHARTDATAGUBUN.length; i++) {
		$("#" + comboId + "").append("<option value='" + i + "'>" + CHARTDATAGUBUN[i] + "</option>");
	}

	$("#" + comboId + " option:eq(0)").prop("selected", true);
}

var setSelColorThema = function (obj, objData, firstNm) {

	$("#" + obj + "").empty();

	if (firstNm != '') {
		$("#" + obj + "").append("<option value=''>" + firstNm + "</option>");
	}

	for (var key in objData) {
		$("#" + obj + "").append("<option value='" + key + "'>" + key + "</option>");
	}

	$("#" + obj + " option:eq(0)").prop("selected", true);
}

var setSelChartType1 = function (obj) {
	setSelChart(obj, CHARTTYPET1, CHARTTYPEV1, '');
}

var setSelChartType2 = function (obj) {
	setSelChart(obj, CHARTTYPET2, CHARTTYPEV2, '유형 선택');
}

var setSelChartFill = function (obj) {
	var arrTitle = ["채우기", "비우기"];
	var arrValue = ["y", "n"];

	setSelChart(obj, arrTitle, arrValue, '');
}

var setSelChartDataLabel = function (obj) {
	var arrTitle = ["Y", "N"];
	var arrValue = ["y", "n"];

	setSelChart(obj, arrTitle, arrValue, '');
}
//----end  combobox

//----start Chart Update
//컬러테마
var setChartColorThema = function (thema, val) {
	var setVal = thema + "." + val;

	chartObj.options.plugins.colorschemes.scheme = setVal;

	chartObj.update();
}
//차트라벨
var setChartDataLabel = function (val) {

	if (val === 'y') {
		chartConfig.options.plugins.datalabels.display = true;
	}
	else {
		chartConfig.options.plugins.datalabels.display = false;
    }

	chartObj.update();
}

//차트라벨박스
var setChartDataLabelBox = function (val) {

	if (val === 'y') {
		chartConfig.options.plugins.datalabels.backgroundColor = function (context) { return context.dataset.borderColor };
		chartConfig.options.plugins.datalabels.color = 'white';
		chartConfig.options.plugins.datalabels.borderColor = 'rgba(128, 128, 128, 0.7)';
	}
	else {
		chartConfig.options.plugins.datalabels.backgroundColor = '';
		chartConfig.options.plugins.datalabels.color = 'black';
		chartConfig.options.plugins.datalabels.borderColor = '';
	}

	chartObj.update();
}

//차트x축
var setChartAxesX = function (val) {

	if (val === 'y') {
		chartConfig.options.scales.x.grid.display = true;
	}
	else {
		chartConfig.options.scales.x.grid.display = false;
	}

	chartObj.update();
}

//차트y축
var setChartAxesY = function (val) {

	if (val === 'y') {
		chartConfig.options.scales.y.grid.display = true;
	}
	else {
		chartConfig.options.scales.y.grid.display = false;
	}

	chartObj.update();
}

//줌모드
var setChartZoomMode = function (val) {

	if (val === 'y') {
		chartConfig.options.plugins.zoom.pan.enabled = true;
		chartConfig.options.plugins.zoom.zoom.wheel.enabled = true;
		chartConfig.options.plugins.zoom.zoom.pinch.enabled = true;
	}
	else {
		chartConfig.options.plugins.zoom.pan.enabled = false;
		chartConfig.options.plugins.zoom.zoom.wheel.enabled = false;
		chartConfig.options.plugins.zoom.zoom.pinch.enabled = false;
	}

	chartObj.update();
}

//줌초기화
var setChartZoomReset = function () {
	chartObj.resetZoom();
}

//차트채우기(전체)
var setChartFillAll = function (val) {
	var bFill = false;

	if (val === 'y') {
		bFill = true;
	}

	for (var i = 0; i < chartConfig.data.datasets.length; i++) {
		chartConfig.data.datasets[i].fill = bFill;
    }

	chartObj.update();
}

//차트채우기
var setChartFill = function (idx, val) {
	try {
		if (val === 'y') {
			chartConfig.data.datasets[idx].fill = true;
		}
		else {
			chartConfig.data.datasets[idx].fill = false;
		}


		chartObj.update();
	}
	catch (err) {
	}
}

//차트타입
var setChartType1 = function (val, obj1, obj2, obj3) {
	try {
		var axesYX = false;
		var axesR = false;

		chartConfig.options.scales.r = chartAxesR;
		chartObj.update();

		for (var i = 0; i < CHARTTYPEV2.length; i++) {
			if (CHARTTYPEV2[i] === val) {
				axesYX = true;
				break;
			}
		}

		for (var i = 0; i < CHARTTYPEV3.length; i++) {
			if (CHARTTYPEV3[i] === val) {
				axesR = true;
				break;
			}
		}

		for (var i = 0; i < chartConfig.data.datasets.length; i++) {
			chartConfig.data.datasets[i].hidden = false;
			chartConfig.data.datasets[i].type = val;
		}

		if (axesYX) {
			if (objAxesX != '') $("#" + objAxesX + "").val("y").prop("selected", true);

			setChartAxesX('y');

			if (objAxesY != '') $("#" + objAxesY + "").val("y").prop("selected", true);

			setChartAxesY('y');

			$("#" + obj1 + "").show();
			$("#" + obj2 + "").show();

		}
		else {
			if (objAxesX != '') $("#" + objAxesX + "").val("n").prop("selected", true);

			setChartAxesX('n');

			if (objAxesY != '') $("#" + objAxesY + "").val("n").prop("selected", true);

			setChartAxesY('n');

			$("#" + obj1 + "").hide();
			$("#" + obj2 + "").hide();

		}

		if (axesR) {
			chartConfig.options.scales.r.display = true;
		}
		else {
			chartConfig.options.scales.r.display = false;
        }

		for (var i = 0; i < CHARTTYPEV4.length; i++) {
			$("#" + obj3 + "").hide();
		}
	
		chartObj.update();
	}
	catch (err) {
	}
}

var setChartType2 = function (obj, idx, val, obj1) {
	try {

		var fill = false;

		for (var i = 0; i < CHARTTYPEV4.length; i++) {
			if (CHARTTYPEV4[i] === val) {
				fill = true;
				break;
			}
		}

		if (fill) {
			$("#" + obj1 + "").show();
		}
		else {
			$("#" + obj1 + "").hide();
		}

		chartConfig.data.datasets[idx].hidden = false;
		chartConfig.data.datasets[idx].type = val;
		chartObj.update();
	}
	catch (err) {
    }
}
//----end  Chart Update

//----start 기타 get, set
var setChartWidthHeight = function () {
	chartWidth = getChartWidth(xColNm.length, chartConfig.data.datasets.length);
	chartHeight = getChartHeight(chartData.length);

	if (xColNm.length > 20) {
		chartConfig.options.responsive = false;
		chartResponsive = false;
	}
	else {
		chartConfig.options.responsive = true;
		chartResponsive = true;
	}

	setChartReObjReset();
}

var setChartReObjReset = function () {

	if (chartConfig.options.responsive) {
		$("." + chartParentObjNm + "").height(chartResponsiveHeight);
		chartHeight = chartResponsiveHeight;
	}
	else {
		$("." + chartParentObjNm + "").height(chartScrollHeight);
	}

	//덧어쓰기 오류 해결
	$("#" + chartObjNm + "").remove(); // this is my <canvas> element
	$("." + chartParentObjNm + "").append('<canvas id="' + chartObjNm + '" width="' + chartWidth + 'px" height="' + chartHeight + 'px"></canvas>');
}

var getChartHeight = function (cnt) {
	var vHeight = cnt * 40;
	if (vHeight => 519) vHeight = 519;

	return vHeight;
}

var getChartWidth = function (cnt, datasetCnt) {
	var x = 20;
	return cnt * x * datasetCnt;
}

var setColor = function () {
	if (chartDataColorIdx < chartDataBgColor.length) {
		dataBdColor = chartDataBdColor[chartDataColorIdx];
		dataBgColor = chartDataBgColor[chartDataColorIdx];
	}
	else {
		var color1 = Math.floor(Math.random() * 256);
		var color2 = Math.floor(Math.random() * 256);
		var color3 = Math.floor(Math.random() * 256);

		dataBdColor = Samples.utils.transparentize(color1, color2, color3, 1);
		dataBgColor = Samples.utils.transparentize(color1, color2, color3, 1);
		//dataBgColor = Samples.utils.transparentize(color1, color2, color3, 0.2);
    }

	chartDataColorIdx++;
}

var setChartTitle = function (title, xTitle, yTitle) {
	if (title === '' || title === undefined || title === null) {
		chartConfig.options.plugins.title.display = false;
		chartConfig.options.plugins.title.text = '';
	}
	else {
		chartConfig.options.plugins.title.display = true;
		chartConfig.options.plugins.title.text = title;
	}

	if (xTitle === '' || xTitle === undefined || xTitle === null) {
		chartConfig.options.scales.x.title.display = false;
		chartConfig.options.scales.x.title.text = '';
	}
	else {
		chartConfig.options.scales.x.title.display = true;
		chartConfig.options.scales.x.title.text = xTitle;
	}

	if (yTitle === '' || yTitle === undefined || yTitle === null) {
		chartConfig.options.scales.y.title.display = false;
		chartConfig.options.scales.y.title.text = '';
	}
	else {
		chartConfig.options.scales.y.title.display = true;
		chartConfig.options.scales.y.title.text = yTitle;
	}
}

var ktsChartAlert = function (msg, timeout) {
	var time = 3000;
	if (timeout != undefined) time = timeout;
	$("." + chartComboObjNm).append('<div class="chart-alert"><div class="chart-alert-msg chart-alert-state-msg" role="alert">' + msg + '</div></div>');

	setTimeout(function () {
		$(".chart-alert").remove();
	}, time);
}

//----end  기타 get, set




//----start Chart Data 관련
//kts_tabulator.js 메서드 오버라이드
var ajaxSuccess = function (json) {
	var errCd;
	var errMsg;
	var errRedirect;

	$.each(JSON.parse(json), function (idx, item) {
		if (item.SERVICE_ERROR_CD != undefined) errCd = item.SERVICE_ERROR_CD;
		if (item.SERVICE_ERROR_MSG != undefined) errMsg = item.SERVICE_ERROR_MSG;
		if (item.SERVICE_ERROR_REDIRECT != undefined) errRedirect = item.SERVICE_ERROR_REDIRECT;
		return false;
	});

	if (errCd == '01') //일반오류
	{
		ktsGridAlert(errMsg);
	}
	else if (errCd == '02') //세션만료
	{
		ktsGridAlert(errMsg);
		location.href = errRedirect;
	}
	else //정상
	{
		ktsGrid.setData(json);
		ktsGridRowCount(ktsGrid.getDataCount());
		ktsGridSearch();

		fn_Chart(json);
		fn_LayerInit();
	}

}

var fn_LayerInit = function () {
}

var fn_Chart = function (ajaxJsonData) {
}

var sortJSON = function (data, key, type) {
	if (type == undefined) {
		type = "asc";
	}
	return data.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		if (type == "desc") {
			return x > y ? -1 : x < y ? 1 : 0;
		} else if (type == "asc") {
			return x < y ? -1 : x > y ? 1 : 0;
		}
	});
}

var parseJsonData = function (json) {
	parseJSON = JSON.parse(json);
}

var dataFilter = function (labelKey, checkKey, checkVal, datakey) {
	var objectData = [];
	objectData = getChartValue(chartConfig.data.labels, parseJSON, labelKey, checkKey, checkVal, datakey);

	return objectData;
}

var arrMDate = new Set();
var dataFilter2 = function (labelKey, datakey, checkKey) {
	var objectData = [];
	objectData = getChartValue2(chartConfig.data.labels, parseJSON, labelKey, datakey, checkKey);

	return objectData;
}

var dataFilter3 = function (labelKey, datakey, checkKey) {
	var objectData = [];
	objectData = getChartValue3(chartConfig.data.labels, parseJSON, labelKey, datakey, checkKey);

	return objectData;
}

var bChartLabelHash = function (hashVal, val) {
	var bRtn = true;
	for (var i = 0; i < hashVal.length; i++) {
		if (hashVal[i] === val) {
			bRtn = false;
			break;
        }
	}
	return bRtn;
}

var getJsonOneValue = function (json, keyNm) {
	var rtnVal = '';

	for (var i = 0; i < json.length; i++) {
		var keyVal = json[i][keyNm];

		if (keyVal != undefined) {
			rtnVal = keyVal;
			break;
		}
	}
	return rtnVal;
}

var getChartLabelValue = function (json, keyNm) {
	var rtnVal = [];

	for (var i = 0; i < json.length; i++) {
		var keyVal = json[i][keyNm];

		if (bChartLabelHash(rtnVal, keyVal)) rtnVal.push(keyVal);
	}
	return rtnVal;
}

var getChartValue = function (labelData, json, labelKey, checkKey, checkVal, datakey) {
	var rtnVal = [];
	var tempObj = {};

	chartDataCnt = 0;

	for (var i = 0; i < json.length; i++) {
		var searchData1 = json[i][labelKey]; //신촌지점
		var searchData2 = json[i][checkKey]; //선로시험기
		var inputData = json[i][datakey];

		if (searchData2 == checkVal) {
			tempObj[searchData1] = inputData;

			chartDataCnt++;
        }
	}

	for (var i = 0; i < labelData.length; i++) {
		var searchData = labelData[i]; //신촌지점
		var data = tempObj[searchData];

		if (data == undefined) {
			data = '';
		}
		else {
			if (data == 'null' || data == 'NULL') {
				data = '';
			}
		}

		if (data == '') data = 'NaN';

		rtnVal.push(data);
	}

	return rtnVal;
}

var getChartValue2 = function (labelData, json, labelKey, datakey, mDateKey) {
	var rtnVal = [];
	var tempObj = {};

	chartDataCnt = 0;

	for (var i = 0; i < json.length; i++) {
		var searchData1 = json[i][labelKey];
		var inputData = json[i][datakey];
		var mDate = json[i][mDateKey];

		arrMDate.add(mDate);

		mDate = mDate.substr(4, 2);
		searchData1 += '(' + mDate + ')';

		if (inputData != undefined) {
			tempObj[searchData1] = inputData;
			chartDataCnt++;
		}
	}

	for (var i = 0; i < labelData.length; i++) {
		var searchData = labelData[i];
		var data = tempObj[searchData];

		if (data == undefined) {
			data = '';
		}
		else {
			if (data == 'null' || data == 'NULL') {
				data = '';
			}
		}

		if (data == '') data = 'NaN';

		rtnVal.push(data);
	}

	return rtnVal;
}

var getChartValue3 = function (labelData, json, labelKey, datakey) {
	var rtnVal = [];
	var tempObj = {};

	chartDataCnt = 0;

	for (var i = 0; i < json.length; i++) {
		var searchData1 = json[i][labelKey];
		var inputData = json[i][datakey];

		if (inputData != undefined) {
			tempObj[searchData1] = inputData;
			chartDataCnt++;
		}
	}

	for (var i = 0; i < labelData.length; i++) {
		var searchData = labelData[i];
		var data = tempObj[searchData];

		if (data == undefined) {
			data = '';
		}
		else {
			if (data == 'null' || data == 'NULL') {
				data = '';
			}
		}

		if (data == '') data = 'NaN';

		rtnVal.push(data);
	}

	return rtnVal;
}
//----end  Chart Data 관련