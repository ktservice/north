var video;
var canvasElement;
var canvas;
var loadingMessage;
var outputContainer;
var outputMessage;
var outputData;
var qrSection;

var ktsQr = function (section) {

    qrSection = section;
    // Use facingMode: environment to attemt to get the front camera on phones
    navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function (stream) {
        video.srcObject = stream;
        video.setAttribute("playsinline", true);  // iOS 사용시 전체 화면을 사용하지 않음을 전달
        video.play();
        requestAnimationFrame(qrTick);
    });
};



var drawLine = function (begin, end, color) {
    canvas.beginPath();
    canvas.moveTo(begin.x, begin.y);
    canvas.lineTo(end.x, end.y);
    canvas.lineWidth = 4;
    canvas.strokeStyle = color;
    canvas.stroke();
};

var qrClose = function () {
    $('#' + qrSection).hide();
    video.srcObject.getTracks().forEach(function (track) { track.stop(); });
};


var qrTick = function () {
    loadingMessage.innerText = "⌛ 스캔 기능을 활성화 중입니다.";

    if (video.readyState === video.HAVE_ENOUGH_DATA) {
        loadingMessage.hidden = true;
        canvasElement.hidden = false;
        outputContainer.hidden = false;

        // 읽어들이는 비디오 화면의 크기
        canvasElement.height = video.videoHeight;
        canvasElement.width = video.videoWidth;
        canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

        var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
        var code = jsQR(imageData.data, imageData.width, imageData.height, {
            inversionAttempts: "dontInvert",
        });

        // QR코드 인식에 성공한 경우
        if (code) {

            // 인식한 QR코드의 영역을 감싸는 사용자에게 보여지는 테두리 생성
            drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF0000");
            drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF0000");
            drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF0000");
            drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF0000");

            outputMessage.hidden = true;
            outputData.parentElement.hidden = false;

            // QR코드 메시지 출력
            outputData.value = code.data;

            qrCallBack(code.data);
            qrClose();

            return;
            
        } else {
            outputMessage.hidden = false;
            outputData.parentElement.hidden = true;
        }
    }
    requestAnimationFrame(qrTick);
};

var qrCallBack = function (data) {
};
