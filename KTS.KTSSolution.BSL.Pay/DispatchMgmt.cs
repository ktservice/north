﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Pay
{
    public class DispatchMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public DispatchMgmt()
        {
        }

        #endregion

        #region Method

        #region GetDispatchRegMonth
        /// <summary>
        /// GetDispatchRegMonth
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetDispatchRegMonth()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_DISPATCHREGMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetDispatchList
        /// <summary>
        /// GetDispatchList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strRegMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetDispatchList(string strOrgCd, string strRegMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_DISPATCHLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pREGMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strRegMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetDispatchListCheck
        /// <summary>
        /// GetDispatchList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetDispatchListCheck(string strEmpNo, string strDispOrgCd)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_DISPATCHLISTCHECK_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pDISPORGCD", MySqlDbType.VarChar, 6);
                parameters[1].Value = strDispOrgCd;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetDispatchInfo
        /// <summary>
        /// SetDispatchInfo
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strEmpNo">string</param>
        /// <param name="strEmpNm">string</param>
        /// <param name="strDispOrgCd">string</param>
        /// <param name="strStdt">string</param>
        /// <param name="strEndt">string</param>
        public void SetDispatchInfo(string strOrgCd, string strEmpNo, string strEmpNm, string strDispOrgCd, string strStdt, string strEndt, string strRegEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[7];

                CommonData data = new CommonData("UP_DISPATCHLIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 6);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strEmpNo;

                parameters[2] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[2].Value = strEmpNm;

                parameters[3] = new MySqlParameter("@pDISPORGCD", MySqlDbType.VarChar, 6);
                parameters[3].Value = strDispOrgCd;

                parameters[4] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 8);
                parameters[4].Value = strStdt;

                parameters[5] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 8);
                parameters[5].Value = strEndt;

                parameters[6] = new MySqlParameter("@pREGEMPNO", MySqlDbType.VarChar, 20);
                parameters[6].Value = strRegEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelDispatchInfo
        /// <summary>
        /// DelDispatchInfo
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        public void DelDispatchInfo(string strEmpNo, string strStDt, string strEnDt)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_DISPATCHLIST_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 6);
                parameters[1].Value = strStDt;

                parameters[2] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 6);
                parameters[2].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
