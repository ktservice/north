﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;
using System.Collections.Generic;

namespace KTS.KTSSolution.BSL.Pay
{
    public class UserConfficientMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public UserConfficientMgmt()
        {
        }

        #endregion

        #region Method

        #region GetSumupMonth
        /// <summary>
        /// GetSumupMonth
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetSumupMonth()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_MERITPAYSUMUPMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetCoefficientLimitVal
        /// <summary>
        /// GetCoefficientLimitVal
        /// </summary>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetCoefficientLimitVal(string strSumupMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_COEFFICIENLIMITTVALUE_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[0].Value = strSumupMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetUserPayConficientList
        /// <summary>
        /// GetUserPayConficientList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserPayConficientList(string strOrgCd, string strSumupMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_USERPAYCONFFICIENT_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region UpdUserPayCofficient
        /// <summary>
        /// UpdUserPayCofficient
        /// </summary>
        public void UpdUserPayCofficient(List<string> lstData)
        {
            try
            {
                List<CommonData> DataList = new List<CommonData>();

                foreach (string strData in lstData)
                {
                    string[] strValue = strData.Split('|');
                    int nLen = strValue.Length;

                    if (nLen < 6)
                        continue;

                    string strSumupMonth = strValue[0].Replace("-", "");
                    string strEmpNo = strValue[1];
                    string strOpenConfficient = strValue[2];
                    string strAsConfficient = strValue[3];
                    string strOpenPoint_C = strValue[4];
                    string strAsPoint_C = strValue[5];

                    MySqlParameter[] parameters = new MySqlParameter[6];

                    parameters[0] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                    parameters[0].Value = strSumupMonth;

                    parameters[1] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                    parameters[1].Value = strEmpNo;

                    parameters[2] = new MySqlParameter("@pOPEN_CONFFICIENT", MySqlDbType.VarChar, 15);
                    parameters[2].Value = strOpenConfficient;

                    parameters[3] = new MySqlParameter("@pAS_CONFFICIENT", MySqlDbType.VarChar, 15);
                    parameters[3].Value = strAsConfficient;

                    parameters[4] = new MySqlParameter("@pOPENPOINT_C", MySqlDbType.VarChar, 15);
                    parameters[4].Value = strOpenPoint_C;

                    parameters[5] = new MySqlParameter("@pASPOINT_C", MySqlDbType.VarChar, 15);
                    parameters[5].Value = strAsPoint_C;

                    CommonData data = new CommonData("UP_USERPAYCONFFICIENT_UPDATE", parameters);
                    data.commandType = CommandType.StoredProcedure;

                    DataList.Add(data);

                }

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.MultiRegister(DataList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
