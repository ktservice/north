﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Pay
{
    public class MeritPayMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public MeritPayMgmt()
        {
        }

        #endregion

        #region Method

        #region GetSumupMonth
        /// <summary>
        /// GetSumupMonth
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetSumupMonth()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_MERITPAYSUMUPMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMaritPayTotalListByOrg
        /// <summary>
        /// GetMaritPayTotalListByOrg
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMaritPayTotalListByOrg(string strOrgCd, string strSumupMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_MERITPAYTOTALLISTBYORG_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMaritPayStatByMonth
        /// <summary>
        /// GetMaritPayStatByMonth
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMaritPayStatByMonth(string strOrgCd, string strStMonth, string strEnMonth, string strValType)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_MERITPAYSTATBYMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSTMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strStMonth;

                parameters[2] = new MySqlParameter("@pENMONTH", MySqlDbType.VarChar, 6);
                parameters[2].Value = strEnMonth;

                parameters[3] = new MySqlParameter("@pVALTYPE", MySqlDbType.VarChar, 20);
                parameters[3].Value = strValType;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMaritPayTotalListByEmp
        /// <summary>
        /// GetMaritPayTotalListByEmp
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMaritPayTotalListByEmp(string strOrgCd, string strSumupMonth, string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYTOTALLISTBYEMP_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMaritPayEmpStatByMonth
        /// <summary>
        /// GetMaritPayEmpStatByMonth
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMaritPayEmpStatByMonth(string strOrgCd, string strStMonth, string strEnMonth, string strEmpNo, string strValType)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_MERITPAYEMPSTATBYMONTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSTMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strStMonth;

                parameters[2] = new MySqlParameter("@pENMONTH", MySqlDbType.VarChar, 6);
                parameters[2].Value = strEnMonth;

                parameters[3] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[3].Value = strEmpNo;

                parameters[4] = new MySqlParameter("@pVALTYPE", MySqlDbType.VarChar, 20);
                parameters[4].Value = strValType;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayOpenDetailList
        /// <summary>
        /// GetMeritPayOpenDetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayOpenDetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYOPENDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayAsDetailList
        /// <summary>
        /// GetMeritPayAsDetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <param name="strSelectLv">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayAsDetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYASDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayManualDetailList
        /// <summary>
        /// GetMeritPayManualDetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayManualDetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYMANUALDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayTerminalDetailList
        /// <summary>
        /// GetMeritPayTerminalDetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayTerminalDetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYTERMINALDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayNonMoveDetailList
        /// <summary>
        /// GetMeritPayNonMoveDetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayNonMoveDetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYNONMOVEDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayDispatchDetailList
        /// <summary>
        /// GetMeritPayDispatchDetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayDispatchDetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYDISPATCHDETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayEvent1DetailList
        /// <summary>
        /// GetMeritPayEvent1DetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayEvent1DetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYEVENT1DETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayEvent2DetailList
        /// <summary>
        /// GetMeritPayEvent2DetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayEvent2DetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYEVENT2DETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMeritPayEvent3DetailList
        /// <summary>
        /// GetMeritPayEvent3DetailList
        /// </summary>
        /// <param name="strOrgCd">string</param>
        /// <param name="strSumupMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetMeritPayEvent3DetailList(string strOrgCd, string strSumupMonth, string strSelectLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_MERITPAYEVENT3DETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strSumupMonth;

                parameters[2] = new MySqlParameter("@pSELECTLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strSelectLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
