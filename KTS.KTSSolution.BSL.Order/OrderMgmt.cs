﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;
using System.Collections.Generic;

namespace KTS.KTSSolution.BSL.Order
{
    public class OrderMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public OrderMgmt()
        {
        }

        #endregion

        #region Method

        #region GetOrderStateInfo
        /// <summary>
        /// GetOrderStateInfo
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetOrderStateInfo(string strOrgCd, string strStDate, string strEnDate, string strOrderCode, string strWorkState)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_ORDERSTATELIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSTDATE", MySqlDbType.VarChar, 8);
                parameters[1].Value = strStDate;

                parameters[2] = new MySqlParameter("@pENDATE", MySqlDbType.VarChar, 8);
                parameters[2].Value = strEnDate;

                parameters[3] = new MySqlParameter("@pORDERCODE", MySqlDbType.VarChar, 1);
                parameters[3].Value = strOrderCode;

                parameters[4] = new MySqlParameter("@pWORKSTATE", MySqlDbType.VarChar, 1);
                parameters[4].Value = strWorkState;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion


        #region GetOrderNumInfo
        /// <summary>
        /// GetOrderNumInfo
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetOrderNumInfo(string strOrderNUm)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_ORDERNUMINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORDERNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNUm;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsertOrderInfo
        /// <summary>
        /// InsertOrderInfo
        /// </summary>
        /// <param name="strRegEmpNo"></param>
        /// <param name="strOrderCode"></param>
        /// <param name="strWorktype"></param>
        /// <param name="strFranchiseNm"></param>
        /// <param name="strCorpNo"></param>
        /// <param name="strAddr"></param>
        /// <param name="strPhoneNo"></param>
        /// <param name="strHopeDt"></param>
        /// <param name="strEquipment"></param>
        /// <param name="strOrderState"></param>
        /// <param name="strNote"></param>
        /// <param name="strWorkerEmpNo"></param>
        public void InsertOrderInfo(string strRegEmpNo, string strOrderCode, string strWorktype, string strFranchiseNm, string strCorpNo, string strAddr
                                , string strPhoneNo, string strHopeDt, string strEquipment, string strOrderState, string strNote, string strWorkerEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[12];

                CommonData data = new CommonData("UP_ORDERSTATELIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pREGEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strRegEmpNo;

                parameters[1] = new MySqlParameter("@pWORKEREMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strWorkerEmpNo;

                parameters[2] = new MySqlParameter("@pORDERCODE", MySqlDbType.VarChar, 1);
                parameters[2].Value = strOrderCode;

                parameters[3] = new MySqlParameter("@pWORKTYPE", MySqlDbType.VarChar, 30);
                parameters[3].Value = strWorktype;

                parameters[4] = new MySqlParameter("@pFRANCHISENM", MySqlDbType.VarChar, 150);
                parameters[4].Value = strFranchiseNm;

                parameters[5] = new MySqlParameter("@pCORPNO", MySqlDbType.VarChar, 20);
                parameters[5].Value = strCorpNo;

                parameters[6] = new MySqlParameter("@pADDR", MySqlDbType.VarChar, 250);
                parameters[6].Value = strAddr;

                parameters[7] = new MySqlParameter("@pPHONENO", MySqlDbType.VarChar, 100);
                parameters[7].Value = strPhoneNo;

                parameters[8] = new MySqlParameter("@pHOPEDT", MySqlDbType.VarChar, 20);
                parameters[8].Value = strHopeDt;

                parameters[9] = new MySqlParameter("@pEQUIPMENT", MySqlDbType.VarChar, 100);
                parameters[9].Value = strEquipment;

                parameters[10] = new MySqlParameter("@pORDERSTATE", MySqlDbType.VarChar, 100);
                parameters[10].Value = strOrderState;

                parameters[11] = new MySqlParameter("@pNOTE", MySqlDbType.VarChar, 1500);
                parameters[11].Value = strNote;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateOrderInfo
        /// <summary>
        /// UpdateOrderInfo
        /// </summary>
        /// <param name="strOrderNUm"></param>
        /// <param name="strOrderCode"></param>
        /// <param name="strWorktype"></param>
        /// <param name="strFranchiseNm"></param>
        /// <param name="strCorpNo"></param>
        /// <param name="strAddr"></param>
        /// <param name="strPhoneNo"></param>
        /// <param name="strHopeDt"></param>
        /// <param name="strEquipment"></param>
        /// <param name="strOrderState"></param>
        /// <param name="strNote"></param>
        /// <param name="strWorkerEmpNo"></param>
        public void UpdateOrderInfo(string strOrderNUm, string strOrderCode, string strWorktype, string strFranchiseNm, string strCorpNo, string strAddr
                                , string strPhoneNo, string strHopeDt, string strEquipment, string strOrderState, string strNote, string strWorkerEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[12];

                CommonData data = new CommonData("UP_ORDERSTATELIST_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORDERNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNUm;

                parameters[1] = new MySqlParameter("@pWORKEREMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strWorkerEmpNo;

                parameters[2] = new MySqlParameter("@pORDERCODE", MySqlDbType.VarChar, 1);
                parameters[2].Value = strOrderCode;

                parameters[3] = new MySqlParameter("@pWORKTYPE", MySqlDbType.VarChar, 30);
                parameters[3].Value = strWorktype;

                parameters[4] = new MySqlParameter("@pFRANCHISENM", MySqlDbType.VarChar, 150);
                parameters[4].Value = strFranchiseNm;

                parameters[5] = new MySqlParameter("@pCORPNO", MySqlDbType.VarChar, 20);
                parameters[5].Value = strCorpNo;

                parameters[6] = new MySqlParameter("@pADDR", MySqlDbType.VarChar, 250);
                parameters[6].Value = strAddr;

                parameters[7] = new MySqlParameter("@pPHONENO", MySqlDbType.VarChar, 100);
                parameters[7].Value = strPhoneNo;

                parameters[8] = new MySqlParameter("@pHOPEDT", MySqlDbType.VarChar, 20);
                parameters[8].Value = strHopeDt;

                parameters[9] = new MySqlParameter("@pEQUIPMENT", MySqlDbType.VarChar, 100);
                parameters[9].Value = strEquipment;

                parameters[10] = new MySqlParameter("@pORDERSTATE", MySqlDbType.VarChar, 100);
                parameters[10].Value = strOrderState;

                parameters[11] = new MySqlParameter("@pNOTE", MySqlDbType.VarChar, 1500);
                parameters[11].Value = strNote;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DeleteOrderInfo
        /// <summary>
        /// DeleteOrderInfo
        /// </summary>
        /// <param name="strOrderNUm"></param>
        public void DeleteOrderInfo(string strOrderNUm)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_ORDERSTATELIST_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORDERNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNUm;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InsertOrderStateInfoExcel
        /// <summary>
        /// InsertOrderStateInfoExcel
        /// </summary>
        public void InsertOrderStateInfoExcel(string strRegEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_ORDERSTATELISTEXCEL_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pREGEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strRegEmpNo;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetOrderStateInfo
        /// <summary>
        /// GetOrderStatByOrg
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetOrderStatByOrg(string strOrgCd, string strStDate, string strEnDate, string strOrderCode)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_ORDERSTATBYORG_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pSTDATE", MySqlDbType.VarChar, 8);
                parameters[1].Value = strStDate;

                parameters[2] = new MySqlParameter("@pENDATE", MySqlDbType.VarChar, 8);
                parameters[2].Value = strEnDate;

                parameters[3] = new MySqlParameter("@pORDERCODE", MySqlDbType.VarChar, 1);
                parameters[3].Value = strOrderCode;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOrderStateInfoByEmp
        /// <summary>
        /// GetOrderStateInfoByEmp
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetOrderStateInfoByEmp(string strStDate, string stEnDate, string strOrderCode, string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_ORDERSTATELISTBYEMP_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pSTDATE", MySqlDbType.VarChar, 8);
                parameters[0].Value = strStDate;

                parameters[1] = new MySqlParameter("@pENDATE", MySqlDbType.VarChar, 8);
                parameters[1].Value = stEnDate;

                parameters[2] = new MySqlParameter("@pORDERCODE", MySqlDbType.VarChar, 8);
                parameters[2].Value = strOrderCode;

                parameters[3] = new MySqlParameter("@pWORKEREMPNO", MySqlDbType.VarChar, 20);
                parameters[3].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region UpdateOrderNumWorkState
        /// <summary>
        /// UpdateOrderNumWorkState
        /// </summary>
        /// <param name="strOrderNum"></param>
        /// <param name="strWorkState"></param>
        public void UpdateOrderNumWorkState(string strOrderNum, string strWorkState, string strReason, string strDate)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_ORDERWORKSTATE_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORDERNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNum;

                parameters[1] = new MySqlParameter("@pWORKSTATE", MySqlDbType.VarChar, 1);
                parameters[1].Value = strWorkState;

                parameters[2] = new MySqlParameter("@pREASON", MySqlDbType.VarChar, 8000);
                parameters[2].Value = strReason;

                parameters[3] = new MySqlParameter("@pDATE", MySqlDbType.VarChar, 10);
                parameters[3].Value = strDate;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetOrderFile
        /// <summary>
        /// GetOrderFile
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetOrderFile(string strOrderNUm)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_ORDERIMAGEFILES_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORDERNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNUm;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsOrderImageFile
        /// <summary>
        /// InsOrderImageFile
        /// </summary>
        public void InsOrderImageFile(string strOrderNum, string strFileNm, byte[] bfile, string strFileType, int nFileSize)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];
                parameters[0] = new MySqlParameter("@pORDERNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNum;
                parameters[1] = new MySqlParameter("@pFILENM", MySqlDbType.VarChar, 150);
                parameters[1].Value = strFileNm;
                parameters[2] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[2].Value = bfile;
                parameters[3] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[3].Value = strFileType;
                parameters[4] = new MySqlParameter("@pFILESIZE", MySqlDbType.VarChar, 20);
                parameters[4].Value = nFileSize;

                CommonData data = new CommonData("UP_ORDERIMAGEFILES_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelOrderImageFile
        /// <summary>
        /// DelOrderImageFile
        /// </summary>
        /// <returns>DataSet</returns>
        public void DelOrderImageFile(string strOrderNum, string strSeq)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pORDERNUM", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNum;
                parameters[1] = new MySqlParameter("@pSEQ", MySqlDbType.VarChar, 20);
                parameters[1].Value = strSeq;

                CommonData data = new CommonData("UP_ORDERIMAGEFILES_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region InsKVanOrderInfo
        /// <summary>
        /// InsKVanOrderInfo
        /// </summary>
        /// <param name="lstData">List<string></param>
        public void InsKVanOrderInfo(List<string> lstData)
        {
            List<CommonData> DataList = new List<CommonData>();

            foreach (string strData in lstData)
            {
                string[] strValue = strData.Split('|');
                int nLen = strValue.Length;

                if (nLen < 19)
                    continue;

                string strOrderNo = strValue[0];
                string strRcvType = strValue[1];
                string strAgencyNm = strValue[2];
                string strFranchiseNm = strValue[3];
                string strCorpNo = strValue[4];
                string strAddr = strValue[5];
                string strAddrDtl = strValue[6];
                string strTelNo = strValue[7];
                string strPhoneNo = strValue[8];
                string strRcvDt = strValue[9];
                string strRsvDt = strValue[10];
                string strDlsvDt = strValue[11];
                string strReceiptNote = strValue[12];
                string strReport = strValue[13];
                string strReqNote = strValue[14];
                string strRtnAddr = strValue[15];
                string strRtnAddrDtl = strValue[16];
                string strWorker = strValue[17];
                string strDispYn = strValue[18];
                string strOrderState = strValue[19];

                MySqlParameter[] parameters = new MySqlParameter[5];

                parameters[0] = new MySqlParameter("@pORDERNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strOrderNo;

                parameters[1] = new MySqlParameter("@pRCVTYPE", MySqlDbType.VarChar, 20);
                parameters[1].Value = strRcvType;

                parameters[2] = new MySqlParameter("@pAGENCYNM", MySqlDbType.VarChar, 150);
                parameters[2].Value = strAgencyNm;

                parameters[3] = new MySqlParameter("@pFRANCHISENM", MySqlDbType.VarChar, 150);
                parameters[3].Value = strFranchiseNm;

                parameters[4] = new MySqlParameter("@pCORPNO", MySqlDbType.VarChar, 20);
                parameters[4].Value = strCorpNo;

                parameters[5] = new MySqlParameter("@pADDR", MySqlDbType.VarChar, 250);
                parameters[5].Value = strAddr;

                parameters[6] = new MySqlParameter("@pADDRDTL", MySqlDbType.VarChar, 250);
                parameters[6].Value = strAddrDtl;

                parameters[7] = new MySqlParameter("@pTELNO", MySqlDbType.VarChar, 20);
                parameters[7].Value = strTelNo;

                parameters[8] = new MySqlParameter("@pPHONENO", MySqlDbType.VarChar, 20);
                parameters[8].Value = strPhoneNo;

                parameters[9] = new MySqlParameter("@pRCVDT", MySqlDbType.VarChar, 10);
                parameters[9].Value = strRcvDt;

                parameters[10] = new MySqlParameter("@pRSVDT", MySqlDbType.VarChar, 10);
                parameters[10].Value = strRsvDt;

                parameters[11] = new MySqlParameter("@pDLSVDT", MySqlDbType.VarChar, 10);
                parameters[11].Value = strDlsvDt;

                parameters[12] = new MySqlParameter("@pRECEIPTNOTE", MySqlDbType.Text);
                parameters[12].Value = strReceiptNote;

                parameters[13] = new MySqlParameter("@pREPORT", MySqlDbType.Text);
                parameters[13].Value = strReport;

                parameters[14] = new MySqlParameter("@pREQNOTE", MySqlDbType.Text);
                parameters[14].Value = strReqNote;

                parameters[15] = new MySqlParameter("@pRTNADDR", MySqlDbType.VarChar, 250);
                parameters[15].Value = strRtnAddr;

                parameters[16] = new MySqlParameter("@pRTNADDRDTL", MySqlDbType.VarChar, 250);
                parameters[16].Value = strRtnAddrDtl;

                parameters[17] = new MySqlParameter("@pWORKER", MySqlDbType.VarChar, 20);
                parameters[17].Value = strWorker;

                parameters[18] = new MySqlParameter("@pDISPYN", MySqlDbType.VarChar, 10);
                parameters[18].Value = strDispYn;

                parameters[19] = new MySqlParameter("@pORDERSTATE", MySqlDbType.VarChar, 10);
                parameters[19].Value = strOrderState;

                CommonData data = new CommonData("UP_KVANORDERINFO_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                DataList.Add(data);
            }

            using (DSBase dsBase = new DSBase())
            {
                dsBase.MultiRegister(DataList);
            }
        }
        #endregion

        #region InsQCImageFile
        /// <summary>
        /// InsReqQCImageFile
        /// </summary>
        public void InsInspectionImageFile(string strInspection_No, string strFileNm, string strTypeCd, byte[] bfile, string strFileType, int nFileSize)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                parameters[0] = new MySqlParameter("@pINSPECTION_NO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strInspection_No;
                parameters[1] = new MySqlParameter("@pIMGNAME", MySqlDbType.VarChar, 150);
                parameters[1].Value = strFileNm;
                parameters[2] = new MySqlParameter("@pTYPECD", MySqlDbType.VarChar, 1);
                parameters[2].Value = strTypeCd;
                parameters[3] = new MySqlParameter("@pIMGDATA", MySqlDbType.LongBlob);
                parameters[3].Value = bfile;
                parameters[4] = new MySqlParameter("@pIMGTYPE", MySqlDbType.VarChar, 100);
                parameters[4].Value = strFileType;
                parameters[5] = new MySqlParameter("@pIMGSIZE", MySqlDbType.Int32);
                parameters[5].Value = nFileSize;

                CommonData data = new CommonData("UP_INSPECTIONIMGDATA_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InsCertificateFile
        /// <summary>
        /// InsCertificateFile
        /// </summary>
        public void InsCertifiCateFile(string strCertificateId, string strFileNm, string strOrgCd, byte[] bfile, string strFileType, int nFileSize, string strEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[7];

                parameters[0] = new MySqlParameter("@pCERTIFICATEID", MySqlDbType.VarChar, 50);
                parameters[0].Value = strCertificateId;
                parameters[1] = new MySqlParameter("@pFILENAME", MySqlDbType.VarChar, 150);
                parameters[1].Value = strFileNm;
                parameters[2] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 6);
                parameters[2].Value = strOrgCd;
                parameters[3] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[3].Value = bfile;
                parameters[4] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[4].Value = strFileType;
                parameters[5] = new MySqlParameter("@pFILESIZE", MySqlDbType.Int32);
                parameters[5].Value = nFileSize;
                parameters[6] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[6].Value = strEmpNo;

                CommonData data = new CommonData("UP_CERTIFICATEFILEDATA_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InsCertificateFileByPartners
        /// <summary>
        /// InsCertificateFileByPartners
        /// </summary>
        public void InsCertificateFileByPartners(string strCertificateId, string strFileNm, string strCompCd, string strOfficeCd, byte[] bfile, string strFileType, int nFileSize, string strEmpNo)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[8];

                parameters[0] = new MySqlParameter("@pCERTIFICATEID", MySqlDbType.VarChar, 50);
                parameters[0].Value = strCertificateId;
                parameters[1] = new MySqlParameter("@pFILENAME", MySqlDbType.VarChar, 150);
                parameters[1].Value = strFileNm;
                parameters[2] = new MySqlParameter("@pCOMPANYCD", MySqlDbType.VarChar, 6);
                parameters[2].Value = strCompCd;
                parameters[3] = new MySqlParameter("@pOFFICECD", MySqlDbType.VarChar, 6);
                parameters[3].Value = strOfficeCd;
                parameters[4] = new MySqlParameter("@pFILEDATA", MySqlDbType.LongBlob);
                parameters[4].Value = bfile;
                parameters[5] = new MySqlParameter("@pFILETYPE", MySqlDbType.VarChar, 100);
                parameters[5].Value = strFileType;
                parameters[6] = new MySqlParameter("@pFILESIZE", MySqlDbType.Int32);
                parameters[6].Value = nFileSize;
                parameters[7] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[7].Value = strEmpNo;

                CommonData data = new CommonData("UP_CERTIFICATEFILEDATABYPARTNERS_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
