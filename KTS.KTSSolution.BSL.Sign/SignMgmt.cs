﻿/*
 *  Copyright ⓒ 2024 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Sign
{
    public class SignMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public SignMgmt()
        {
        }

        #endregion

        #region Method

        #region GetConstractCount
        /// <summary>
        /// GetConstractCount
        /// </summary>
        /// <param name="strSql">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetConstractInfo(string strSql)
        {
            DataSet ds = null;

            try
            {
                //System.Data.Odbc.OdbcParameter[] parameters = new System.Data.Odbc.OdbcParameter[0];

                //parameters[0] = new System.Data.Odbc.OdbcParameter("@YEAR", System.Data.Odbc.OdbcType.VarChar);
                //parameters[0].Value = strYear;

                using (DBConn dbConn = new DBConn())
                {
                    ds = dbConn.ExecuteDataSetByPostgreSql(CommandType.Text, strSql);//, parameters);
                }
            }
            catch
            {
                ds = new DataSet();
                DataTable dt = new DataTable();

                dt.Columns.Add("SERVICE_ERROR_CD", typeof(string));
                dt.Columns.Add("SERVICE_ERROR_MSG", typeof(string));

                dt.Rows.Add("01", "조회 중 오류가 발생했습니다.");
                ds.Tables.Add(dt);
            }

            return ds;
        }

        #endregion
        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
