﻿using KTS.KTSSolution.BSL.Common;
using System;
using System.Data;
using System.Web;
using System.Web.UI;

namespace KTS.KTSSolution.Framework.Web
{
    public class PageUtility
    {
        #region Fields
        private Page _page;
        private Random _random;
        #endregion

        #region Properties
        #endregion

        #region Constructor
        public PageUtility(Page page)
        {
            _page = page;
            _random = new Random(unchecked((int)DateTime.Now.Ticks));
        }
        #endregion

        #region Method

        #region PageMove
        /// <summary>
        /// PageMove
        /// </summary>
        /// <param name="strUrl">string</param>
        public void PageMove(string strUrl)
        {
            string strScript = "window.location.href = '" + strUrl + "';";

            this.ExecuteScript(strScript, false);

        }
        #endregion

        #region PageMoveNDiligAlert
        /// <summary>
        /// PageMove
        /// </summary>
        /// <param name="strUrl">string</param>
        public void PageMoveNDiligAlert(string strUrl)
        {
            string strScript = "fn_DiligAlert(); window.location.href = '" + strUrl + "';";

            this.ExecuteScript(strScript, false);

        }
        #endregion

        #region
        public void ExecuteScript(string strScript, bool bBeforPageLoad)
        {
            strScript = "<script type=\"text/javascript\">" + strScript + "</script>";

            if (bBeforPageLoad)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(strScript);
                HttpContext.Current.Response.Flush();
            }
            else
            {
                string strScriptID = DateTime.Now.ToString("yyyyMMddhhmmssfff") + _random.Next().ToString();
                if (!_page.ClientScript.IsClientScriptBlockRegistered(strScriptID))
                {
                    ScriptManager.RegisterStartupScript(_page, this.GetType(), strScriptID, strScript, false);
                }

            }
        }
        #endregion

        #region DiligAlertMobile
        /// <summary>
        /// DiligAlertMobile
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCookieNm">string</param>
        public void DiligAlertMobile(string strEmpNo, string strPageNm)
        {
            DataSet ds = null;

            using (KTSUser ktsUser = new KTSUser())
            {
                ds = ktsUser.GetDiligInfo(strEmpNo);
            }

            bool bDilig = false;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    bDilig = true;
                }
            }

            string strCookieNm = strPageNm + "_" + strEmpNo;

            HttpCookie Cookie = _page.Request.Cookies[strCookieNm];

            if (Cookie != null)
            {
                if (Cookie["POPUPYN"] == "Y" && Cookie["DATE"].ToString().Equals(DateTime.Now.ToString("yyyyMMdd")))
                {
                    bDilig = false;
                }
            }

            if (bDilig)
            {
                ScriptManager.RegisterStartupScript(_page, this.GetType(), "ModalOpen", "ModalOpen();", true);
            }

        }
        #endregion

        #region SetDiligCookieM
        /// <summary>
        /// SetDiligCookieM
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strCookieNm">string</param>
        public void SetDiligCookieM(string strEmpNo, string strPageNm)
        {
            HttpCookie Cookie = null;
            string strCookieNm = strPageNm + "_" + strEmpNo;

            if (_page.Request.Cookies[strCookieNm] == null)
            {
                Cookie = new HttpCookie(strCookieNm);
            }
            else
            {
                Cookie = _page.Request.Cookies[strCookieNm];
            }

            Cookie["POPUPYN"] = "Y";
            Cookie["DATE"] = DateTime.Now.ToString("yyyyMMdd");
            Cookie.Expires = DateTime.Now.AddDays(1);
            Cookie.HttpOnly = true;
            _page.Response.Cookies.Add(Cookie);
        }
        #endregion

        #endregion
    }
}
