﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;

namespace KTS.KTSSolution.Framework.Web
{
    public static class Utility
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructor
        #endregion

        #region Method

        #region ReplaceChar
        /// <summary>
        /// ReplaceChar
        /// </summary>
        /// <param name="strData">string</param>
        /// <returns>string</returns>
        public static string ReplaceChar(string strData)
        {
            strData = strData.Replace("<", "←");
            strData = strData.Replace(">", "→");
            strData = strData.Replace("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
            strData = strData.Replace("script", "");
            strData = strData.Replace("meta", "");
            strData = strData.Replace("eval\\((.*)\\)", "");

            return strData;
        }
        #endregion

        #region SHA256Hash
        /// <summary>
        /// SHA256Hash
        /// </summary>
        /// <param name="data">string</param>
        /// <returns>string</returns>
        public static string SHA256Hash(string data)
        {
            SHA256 sha = new SHA256Managed();
            byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(data));

            StringBuilder sb = new StringBuilder();
            foreach (byte b in hash)
            {
                sb.AppendFormat("{0:x2}", b);
            }

            return sb.ToString();
        }
        #endregion

        #region GetOrgNm
        /// <summary>
        /// GetOrgNm
        /// </summary>
        /// <param name="OrgCd">string</param>
        /// <returns>string</returns>
        public static string GetOrgNm(string OrgCd, DataTable DtOrgList, bool bFullNmGb = true)
        {
            string strOrgNm = "";
            try
            {
                string[] strOrgCd = OrgCd.Split(',');
                string strWhere = "";

                for (int i = 0; i < strOrgCd.Length; i++)
                {
                    if (strWhere.Length > 0)
                        strWhere += " OR ";

                    strWhere += string.Format("ORGCD = '{0}'", strOrgCd[i]);
                }

                DataRow[] dr = DtOrgList.Select(strWhere);
                DataTable dt = DtOrgList.Clone();

                if (dr.Length > 0)
                {
                    dt = dr.CopyToDataTable();
                }

                if (dt.Rows.Count == 1)
                {
                    if(bFullNmGb.Equals(true)) strOrgNm = dt.Rows[0]["ORGFULLNM"].ToString();
                    else strOrgNm = dt.Rows[0]["ORGNM"].ToString();
                }
                else
                {
                    foreach (DataRow drOrg in dt.Rows)
                    {
                        if (strOrgNm.Length > 0)
                            strOrgNm += ", ";

                        strOrgNm += drOrg["ORGNM"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOrgNm;
        }
        #endregion

        #region GetBCompOrgNm
        /// <summary>
        /// GetOrgNm
        /// </summary>
        /// <param name="officeCd">string</param>
        /// <returns>string</returns>
        public static string GetBCompOrgNm(string officeCd, DataTable DtOrgList)
        {
            string strOrgNm = "";
            try
            {
                string[] strOfficeCd = officeCd.Split(',');

                for (int i = 0; i < strOfficeCd.Length; i++)
                {
                    if (strOrgNm.Length > 0)
                        strOrgNm += ", ";
                    strOrgNm += BCompOrgNmSearch(strOfficeCd[i], DtOrgList);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOrgNm;
        }
        #endregion

        #region BCompOrgNmSearch
        /// <summary>
        /// BCompOrgNmSearch
        /// </summary>
        /// <param name="officeCd"></param>
        /// <returns></returns>
        static string BCompOrgNmSearch(string officeCd, DataTable DtOrgList)
        {
            string strOrgNm = "";
            try
            {

                for (int i = 0; i < DtOrgList.Rows.Count; i++)
                {
                    if (DtOrgList.Rows[i]["ORGLEVEL"].ToString().Equals("5"))
                    {
                        if (officeCd.Equals(DtOrgList.Rows[i]["ORGCD"].ToString()))
                        {
                            strOrgNm = DtOrgList.Rows[i]["ORGNM"].ToString();

                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOrgNm;
        }
        #endregion


        #region GetBCompTeamCd
        /// <summary>
        /// GetBCompTeamCd
        /// </summary>
        /// <param name="officeCd">string</param>
        /// <returns>string</returns>
        public static string GetBCompTeamCd(string officeCd, DataTable DtOrgList)
        {
            string strOrgCd = "";
            try
            {
                string[] strOfficeCd = officeCd.Split(',');

                for (int i = 0; i < strOfficeCd.Length; i++)
                {
                    if (strOrgCd.Length > 0)
                        strOrgCd += ",";
                    strOrgCd += TeamCdSearch(strOfficeCd[i], DtOrgList);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOrgCd;
        }
        #endregion

        #region TeamCdSearch
        /// <summary>
        /// TeamCdSearch
        /// </summary>
        /// <param name="officeCd"></param>
        /// <returns></returns>
        static string TeamCdSearch(string officeCd, DataTable DtOrgList)
        {
            string strOrgCd = "";
            try
            {
                for(int i=0;i< DtOrgList.Rows.Count;i++)
                {
                    if (DtOrgList.Rows[i]["ORGLEVEL"].ToString().Equals("5"))
                    {
                        if (officeCd.Equals(DtOrgList.Rows[i]["ORGCD"].ToString()))
                        {
                            if (strOrgCd.Length > 0)
                                strOrgCd += ",";
                            strOrgCd += DtOrgList.Rows[i]["TEAMCD"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOrgCd;
        }
        #endregion


        #region GetBCompOfficeCd
        /// <summary>
        /// GetBCompTeamCd
        /// </summary>
        /// <param name="orgCd">string</param>
        /// <returns>string</returns>
        public static string GetBCompOfficeCd(string orgCd, DataTable DtOrgList)
        {
            string strOfficeCd = "";
            try
            {
                string[] strOrgCd = orgCd.Split(',');

                for (int i = 0; i < strOrgCd.Length; i++)
                {
                    if (strOfficeCd.Length > 0)
                        strOfficeCd += ",";
                    strOfficeCd += OfficeCdSearch(strOrgCd[i], DtOrgList);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOfficeCd;
        }
        #endregion

        #region OfficeCdSearch
        /// <summary>
        /// OfficeCdSearch
        /// </summary>
        /// <param name="orgCd"></param>
        /// <returns></returns>
        static string OfficeCdSearch(string orgCd, DataTable DtOrgList)
        {
            string strOfficeCd = "";
            try
            {
                for (int i = 0; i < DtOrgList.Rows.Count; i++)
                {
                    if (DtOrgList.Rows[i]["ORGLEVEL"].ToString().Equals("5"))
                    {
                        if (orgCd.Equals(DtOrgList.Rows[i]["TEAMCD"].ToString()))
                        {
                            if (strOfficeCd.Length > 0)
                                strOfficeCd += ",";
                            strOfficeCd += DtOrgList.Rows[i]["ORGCD"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOfficeCd;
        }
        #endregion



        #region DataSetToJson
        /// <summary>
        /// DataSet -> Json String 
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>Json String</returns>
        public static string DataSetToJson(DataSet ds)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            serializer.MaxJsonLength = 2147483647;
            Dictionary<string, List<Dictionary<string, object>>> dsList = new Dictionary<string, List<Dictionary<string, object>>>();
            List<Dictionary<string, object>> rows;
            Dictionary<string, object> row;
            foreach (DataTable dt in ds.Tables)
            {
                rows = new List<Dictionary<string, object>>();
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                dsList.Add(dt.TableName, rows);
            }
            return serializer.Serialize(dsList);
        }
        #endregion

        #region DataTableToJson
        /// <summary>
        /// DataTable -> Json String 
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <returns>Json String</returns>
        public static string DataTableToJson(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            serializer.MaxJsonLength = 2147483647;
            List<Dictionary<string, object>> listRows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                listRows.Add(row);
            }
            return serializer.Serialize(listRows);
        }
        #endregion

        #region DataTableError
        /// <summary>
        /// DataTableError
        /// </summary>
        /// <param name="errCd">에러코드 (00:정상, 01:일반오류, 02:세션만료)</param>
        /// <param name="errMsg">에러메시지</param>
        /// <param name="errorUrl">에러URL</param>
        /// <returns>DataTable</returns>
        public static DataTable DataTableError(string errCd, string errMsg, string errorUrl)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("SERVICE_ERROR_CD", typeof(string));
            dt.Columns.Add("SERVICE_ERROR_MSG", typeof(string));
            dt.Columns.Add("SERVICE_ERROR_REDIRECT", typeof(string));
            dt.Rows.Add(errCd, errMsg, errorUrl);

            return dt;
        }
        #endregion

        #region DataTableError2
        /// <summary>
        /// DataTableError2
        /// </summary>
        /// <param name="errCd">에러코드 (00:정상, 01:일반오류, 02:세션만료)</param>
        /// <param name="errMsg">에러메시지</param>
        /// <param name="errorUrl">에러URL</param>
        /// <returns>DataTable</returns>
        public static DataTable DataTableError2(string errCd, string errMsg)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("ERRCD", typeof(string));
            dt.Columns.Add("ERRMSG", typeof(string));
            dt.Rows.Add(errCd, errMsg);

            return dt;
        }
        #endregion

        #region GetCurrentFileNm
        /// <summary>
        /// GetCurrentFileNm
        /// </summary>
        /// <param name="httpContext">httpContext</param>
        /// <returns>string</returns>
        public static string GetCurrentFileNm(HttpContext httpContext)
        {
            string[] file = httpContext.Request.CurrentExecutionFilePath.Split('/');
            string fileName = file[file.Length - 1];
            string fileExt = httpContext.Request.CurrentExecutionFilePathExtension;
            
            fileName = fileName.Replace(fileExt, "");

            return fileName;
        }
        #endregion

        #region GetUserIp
        /// <summary>
        /// GetUserIp
        /// </summary>
        /// <param name="httpContext">httpContext</param>
        /// <returns>string</returns>
        public static string GetUserIp(HttpContext httpContext)
        {
            string ip = string.Empty;
            ip = httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (ip == "" || ip == null) ip = httpContext.Request.ServerVariables["REMOTE_ADDR"];

            return ip;
        }
        #endregion

        #region SessionChk
        /// <summary>
        /// SessionChk
        /// </summary>
        /// <param name="httpContext">httpContext</param>
        /// <returns>bool</returns>
        public static bool SessionChk(HttpContext httpContext)
        {
            bool check = true;

            if (httpContext.Session.Count <= 5) check = false;

            return check;
        }
        #endregion

        #endregion
    }
}
