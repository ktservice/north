﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Security.AccessControl;
using System.Web.Caching;
using System.Web.UI;
using KTS.KTSSolution.BSL.Common;
using Qportal.SSO;

namespace KTS.KTSSolution.Framework.Web
{
    public class UserInfo :IDisposable
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructor

        public UserInfo()
        {
        }

        #endregion

        #region Method

        #region UserLogOn
        /// <summary>
        /// UserLogOn
        /// </summary>
        /// <param name="userid">string</param>
        /// <param name="userpw">string</param>
        /// <param name="page">Page</param>
        /// <returns>string</returns>
        public string UserLogOn(string userid, string userpw, Page page)
        {
            string msg = "Fail";
            DataSet ds = null;

            try
            {
                using (KTSUser ktsUser = new KTSUser())
                {
                    ds = ktsUser.GetUserLogOn(userid, userpw);

                    if(ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                if (ds.Tables[0].Rows[0]["EXFIREYN"].ToString().Equals("Y"))
                                {
                                    msg = "ExfireUser";
                                }
                                else if (ds.Tables[0].Rows[0]["PWDCHGYN"].ToString().Equals("Y"))
                                {
                                    msg = "ChangePassword";
                                }
                                else
                                {
                                    SetUserInfo(userid, "", page);
                                    msg = "Success";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if(ds != null)
                    ds.Dispose();
            }            

            return msg;
        }
        #endregion

        #region AdminLogOn
        /// <summary>
        /// AdminLogOn
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="userpw"></param>
        /// <param name="page"></param>
        /// <returns>string</returns>
        public string AdminLogOn(string userid, string userpw, Page page)
        {
            string msg = "Fail";
            string strSHAEmpPwd = string.Empty;
            string chkKey = string.Empty;
            DateTime dateTime = DateTime.Today;

            chkKey = dateTime.ToString("yyyyMMdd") + "!";

            if (userpw.LastIndexOf(chkKey) >= 0)
            {
                userpw = userpw.Substring(0, userpw.LastIndexOf(chkKey));
                strSHAEmpPwd = Utility.SHA256Hash(userpw);

                DataSet ds = null;

                try
                {
                    using (KTSUser ktsUser = new KTSUser())
                    {
                        ds = ktsUser.GetAdminLogOn(userid, strSHAEmpPwd);

                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    if (ds.Tables[0].Rows[0]["EMPNO"].ToString().Equals(""))
                                    {
                                        msg = "Fail";
                                    }
                                    else if (ds.Tables[0].Rows[0]["EXFIREYN"].ToString().Equals("Y"))
                                    {
                                        msg = "ExfireUser";
                                    }
                                    else if (ds.Tables[0].Rows[0]["PWDCHGYN"].ToString().Equals("Y"))
                                    {
                                        SetUserInfo(userid, "", page);
                                        msg = "Success";
                                        //msg = "ChangePassword";
                                    }
                                    else
                                    {
                                        SetUserInfo(userid, "", page);
                                        msg = "Success";
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (ds != null)
                        ds.Dispose();
                }
            }

            return msg;
        }
        #endregion

        #region CheckMobileAuth
        /// <summary>
        /// CheckMobileAuth
        /// </summary>
        /// <param name="page">Page</param>
        /// <returns>string</returns>
        public bool CheckMobileAuth(Page page, string strToken, string strCorpFlag, string strPortalSSOCookie)
        {
            bool bCheckAuth = false;

            //try
            //{
            string strUserId = "";
            string strLoginType = "";

            if (strPortalSSOCookie.Length > 0)
            {
                //q-Portal
                bCheckAuth = SSOQPortalAuth(page, strPortalSSOCookie, ref strUserId);
                strLoginType = "q-portal";
            }
            else
            {
                //mkate
                bCheckAuth = SSOmKateAuth(page, strToken, strCorpFlag, ref strUserId);
                strLoginType = "mkate";
            }

            //bCheckAuth = true;

            //strUserId = "91108663";

            if (bCheckAuth)
            {
                SetUserInfo(strUserId, strLoginType, page);
            }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            return bCheckAuth;
        }
        #endregion

        #region CheckMobileAuthTest
        /// <summary>
        /// CheckMobileAuthTest
        /// </summary>
        /// <param name="page">Page</param>
        /// <returns>string</returns>
        public bool CheckMobileAuthTest(Page page)
        {
            bool bCheckAuth = false;

            string strUserId = "";

            bCheckAuth = true;

            strUserId = "91108663";

            if (bCheckAuth)
            {
                SetUserInfo(strUserId, "", page);
            }

            return bCheckAuth;
        }
        #endregion

        #region CheckWebAuth
        /// <summary>
        /// CheckWebAuth
        /// </summary>
        /// <param name="page">Page</param>
        /// <returns>string</returns>
        public bool CheckWebAuth(Page page, string strPortalSSOCookie)
        {
            bool bCheckAuth = false;

            try
            {
                string strUserId = "";

                //q-Portal
                bCheckAuth = SSOQPortalAuth(page, strPortalSSOCookie, ref strUserId);
                //bCheckAuth = true;

                //strUserId = "91108663";
                if (bCheckAuth)
                {
                    SetUserInfo(strUserId, "q-portal", page);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bCheckAuth;
        }
        #endregion

        #region SSOLoginMethod
        /// <summary>
        /// SSOLoginMethod
        /// 1. 모바일 kate Home 화면 내 바로가기 링크를 클릭하여, 타 시스템 간 Token 방식 단일인증 처리 시 발생
        /// 1-1. 모바일 kate로 부터 받은 Token을 그대로 전달
        ///  - corpFlag : 0(그룹사) - (개발) https://gdev.mkate.kt.com/c/portal/ktgroup/checkAuthToken
        ///  - corpFlag : 0(그룹사) - (운영) https://g.mkate.kt.com/c/portal/ktgroup/checkAuthToken
        ///  - corpFlag : 1(kt) - (개발) https://dev.mkate.kt.com/mKate/auth/AuthController/getMkLogin.do
        ///  - corpFlag : 1(kt) - (운영) https://mkate.kt.com/mKate/auth/AuthController/getMkLogin.do
        /// </summary>
        /// <param name="page"></param>
        /// <param name="strToken"></param>
        /// <param name="strCorpFlag"></param>
        /// <param name="strUserId"></param>
        /// <returns></returns>
        protected bool SSOmKateAuth(Page page, string strToken, string strCorpFlag, ref string strUserId)
        {
            bool bAuthCheck = false;

            //try
            //{
                string url = "";
                if (strCorpFlag.Equals("1"))
                {
                    //kt
                    url = ConfigurationSettings.AppSettings["checkMkateAuthKt"];
                }
                else
                {
                    //그룹사
                    url = ConfigurationSettings.AppSettings["checkMkateAuthGrp"];
                }

                //m-kate 연동
                string getData = "?token=" + strToken;
                string getUrl = url + getData;

                var requesst = (HttpWebRequest)WebRequest.Create(getUrl);
                var response = (HttpWebResponse)requesst.GetResponse();

                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //m-kate 연동결과
                dynamic param = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                dynamic param_r = Newtonsoft.Json.JsonConvert.DeserializeObject(Newtonsoft.Json.JsonConvert.SerializeObject(param["result"]));
                string code = Convert.ToString(param_r["code"]);
                string errdesc = Convert.ToString(param_r["errdesc"]);

                //string clientIP = page.Request.UserHostAddress.ToString();

                if ("0".Equals(code))
                {
                    strUserId = Convert.ToString(param["userid"]);
                    bAuthCheck = true;
                }
                else
                {
                    bAuthCheck = false;
                }
            //}
            //catch(Exception ex)
            //{
            //    throw ex;
            //}

            return bAuthCheck;
        }
        #endregion

        protected bool SSOQPortalAuth(Page page, string strPortalSSOCookie, ref string strUserId)
        {
            bool bAuthCheck = false;

            //try
            //{
                //포탈에서 qPortalSSOCookie 를 암호화 했을때 사용하는 AES256 암호화 키
                //string encKey = "12345678901234561234567890123456";
                string encKey = "e3957e6c59014cdd9fb7ff8eca8bec6d";
                //string encKey = string.Empty;

            //암호화된 포탈정보를 SSO 모듈에 적용
            QportalSSO sso = new QportalSSO();
                sso.SSOToken = strPortalSSOCookie;//sso 토큰
                sso.SSOEncKey = encKey;//암호화키
                sso.DecPassType = PasswordDecType.AES256AndRandom;//패스워드 복호화타입 -->  포탈의  Authenticator  인증이 필요할 경우에만 사용함
                                                                  //1.PlainText -->  sptid -->  암호화된 형태로 반환
                                                                  //2.AES256 --> sptid -->  복호화만 처리 후 반환 (난수형태의 sptid)
                                                                  //3.AES256AndRandom --> sptid --> 복호화 후  난수를 풀어서 반환  --> 평문 패스워드
                
                SSOUserInfo ssoUserInfo = sso.GetUserInfo();
                
                if (ssoUserInfo.resultType == ResultType.OK)
                {
                    strUserId = ssoUserInfo.userId;
                    using (KTSUser ktsUser = new KTSUser())
                    {
                        ktsUser.InsSSOUserLogInfo(strUserId, ssoUserInfo.oldEmpNo, ssoUserInfo.sptid, ssoUserInfo.userName);
                    }
                    bAuthCheck = true;
                }
                else
                {
                    bAuthCheck = false;
                }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            return bAuthCheck;
        }

        #region SetUserInfo
        /// <summary>
        /// SetUserInfo
        /// </summary>
        /// <param name="userid">string</param>
        /// <param name="page">Page</param>
        public void SetUserInfo(string userid, string strLoginType, Page page)
        {
            DataSet ds = null;
            try
            {
                using (KTSUser ktsUser = new KTSUser())
                {
                    ds = ktsUser.GetUserLogInfo(userid);

                    page.Session.Remove("EMPNO");
                    page.Session.Remove("KTSEMPNO");
                    page.Session.Remove("ORGFULLNM");
                    page.Session.Remove("COMPANYCD");
                    page.Session.Remove("COMPANYNM");
                    page.Session.Remove("EMPNM");
                    page.Session.Remove("PHONENUM");
                    page.Session.Remove("MOBILE");
                    page.Session.Remove("EMAIL");
                    page.Session.Remove("JOBNM");
                    page.Session.Remove("JOBGRADE");
                    page.Session.Remove("JOBNM2");
                    page.Session.Remove("IDMSEMPNO");
                    page.Session.Remove("PREIDMSEMPNO");
                    page.Session.Remove("DEPTCD");
                    page.Session.Remove("ORGCD");
                    page.Session.Remove("AUTHID");
                    page.Session.Remove("KTSYN");
                    page.Session.Remove("ENTERDT");
                    page.Session.Remove("EXFIREDT");
                    page.Session.Remove("ORGLEVEL");
                    page.Session.Remove("LOGINTYPE");

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        page.Session["EMPNO"] = row["EMPNO"].ToString();
                        page.Session["KTSEMPNO"] = row["KTSEMPNO"].ToString();
                        page.Session["ORGFULLNM"] = row["ORGFULLNM"].ToString();
                        page.Session["COMPANYCD"] = row["COMPANYCD"].ToString();
                        page.Session["COMPANYNM"] = row["COMPANYNM"].ToString();
                        page.Session["EMPNM"] = row["EMPNM"].ToString();
                        page.Session["PHONENUM"] = row["PHONENUM"].ToString();
                        page.Session["MOBILE"] = row["MOBILE"].ToString();
                        page.Session["EMAIL"] = row["EMAIL"].ToString();
                        page.Session["JOBNM"] = row["JOBNM"].ToString();
                        page.Session["JOBGRADE"] = row["JOBGRADE"].ToString();
                        page.Session["JOBNM2"] = row["JOBNM2"].ToString();
                        page.Session["IDMSEMPNO"] = row["IDMSEMPNO"].ToString();
                        page.Session["PREIDMSEMPNO"] = row["PREIDMSEMPNO"].ToString();
                        page.Session["DEPTCD"] = row["DEPTCD"].ToString();

                        if (page.Session["ORGCD"] != null)
                            page.Session["ORGCD"] += ",";

                        page.Session["ORGCD"] += row["ORGCD"].ToString();

                        page.Session["AUTHID"] = row["AUTHID"].ToString();
                        page.Session["KTSYN"] = row["KTSYN"].ToString();

                        page.Session["ENTERDT"] = row["ENTERDT"].ToString();
                        page.Session["EXFIREDT"] = row["EXFIREDT"].ToString();

                        if (page.Session["ORGLEVEL"] != null)
                        {
                            int nOrgLevel1 = 9;
                            int nOrgLevel2 = 9;
                            int.TryParse(page.Session["ORGLEVEL"].ToString(), out nOrgLevel1);
                            int.TryParse(row["ORGLEVEL"].ToString(), out nOrgLevel2);

                            if (nOrgLevel1 > nOrgLevel2)
                            {
                                page.Session["ORGLEVEL"] = row["ORGLEVEL"].ToString();
                            }
                        }
                        else
                        {
                            page.Session["ORGLEVEL"] = row["ORGLEVEL"].ToString();
                        }

                        page.Session["LOGINTYPE"] = strLoginType;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
