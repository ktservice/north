﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTS.KTSSolution.BSL.Common
{
    public class KtsMenuItem
    {
        public string MENUID { get; set; }
        public string MENUNM { get; set; }
        public string UPPERMENUID { get; set; }
        public string MENUORDER { get; set; }
        public string MENULEVEL { get; set; }
        public string URL { get; set; }
        public string LEAFMENUYN { get; set; }

        public KtsMenuItem(DataRow row)
        {
            this.MENUID = row["MENUID"].ToString();
            this.MENUNM = row["MENUNM"].ToString();
            this.UPPERMENUID = row["UPPERMENUID"].ToString();
            this.MENUORDER = row["MENUORDER"].ToString();
            this.MENULEVEL = row["MENULEVEL"].ToString();
            this.URL = row["URL"].ToString();
            this.LEAFMENUYN = row["LEAFMENUYN"].ToString();
        }
    }
    public class KtsMobileMenuItem
    {
        public string MENUID { get; set; }
        public string MENUNM { get; set; }
        public string UPPERMENUID { get; set; }
        public string MENUORDER { get; set; }
        public string MENULEVEL { get; set; }
        public string URL { get; set; }
        public string LEAFMENUYN { get; set; }
        public string TITLE { get; set; }

        public KtsMobileMenuItem(DataRow row)
        {
            this.MENUID = row["MENUID"].ToString();
            this.MENUNM = row["MENUNM"].ToString();
            this.UPPERMENUID = row["UPPERMENUID"].ToString();
            this.MENUORDER = row["MENUORDER"].ToString();
            this.MENULEVEL = row["MENULEVEL"].ToString();
            this.URL = row["URL"].ToString();
            this.LEAFMENUYN = row["LEAFMENUYN"].ToString();
            this.TITLE = row["TITLE"].ToString();
        }
    }
}
