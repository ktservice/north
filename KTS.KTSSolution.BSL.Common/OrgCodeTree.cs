﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Common
{
    public class OrgCodeTree : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public OrgCodeTree()
        {
        }

        #endregion

        #region Method

        #region GetOrgList
        /// <summary>
        /// GetOrgList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperOrgList(string strEmpNo, string strOfficeYn)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_ORGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pOFFICEYN", MySqlDbType.VarChar, 1);
                parameters[1].Value = strOfficeYn;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperOrgMonthList
        /// <summary>
        /// GetOperOrgMonthList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperOrgMonthList(string strEmpNo, string strOfficeYn, string strMdate)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_ORGMONTHLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pOFFICEYN", MySqlDbType.VarChar, 1);
                parameters[1].Value = strOfficeYn;

                parameters[2] = new MySqlParameter("@pMDATE", MySqlDbType.VarChar, 6);
                parameters[2].Value = strMdate;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperOrgListByOrgLv
        /// <summary>
        /// GetOperOrgListByOrgLv
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strOrgLv">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperOrgListByOrgLv(string strEmpNo, string strOrgLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_ORGLISTBYORGLV_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pORGLV", MySqlDbType.VarChar, 1);
                parameters[1].Value = strOrgLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperOrgMonthListByOrgLv
        /// <summary>
        /// GetOperOrgMonthListByOrgLv
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strOrgLv">string</param
        /// <param name="strMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperOrgMonthListByOrgLv(string strEmpNo, string strOrgLv, string strMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_ORGMONTHLISTBYORGLV_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pORGLV", MySqlDbType.VarChar, 1);
                parameters[1].Value = strOrgLv;

                parameters[2] = new MySqlParameter("@pMDATE", MySqlDbType.VarChar, 6);
                parameters[2].Value = strMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperBCompanyOrgList
        /// <summary>
        /// GetOperBCompanyOrgList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperBCompanyOrgList(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_BCOMPANYORGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperBCompanyOrgMonthList
        /// <summary>
        /// GetOperBCompanyOrgMonthList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperBCompanyOrgMonthList(string strEmpNo, string strMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_BCOMPANYORGMONTHLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pMDATE", MySqlDbType.VarChar, 6);
                parameters[1].Value = strMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetBCompanyOrgList
        /// <summary>
        /// GetBCompanyOrgList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetBCompanyOrgList()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_MNGORGLISTBCOMPANY_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOrgNmList
        /// <summary>
        /// GetOrgNmList
        /// </summary>
        /// <param name="strOrgNm">string</param>
        /// <param name="strOrgLv">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOrgNmList(string strOrgCd, string strOrgNm, string strOrgLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_ORGNMLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pORGNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strOrgNm;

                parameters[2] = new MySqlParameter("@pORGLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strOrgLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassOrgList_All
        /// <summary>
        /// GetPassOrgList_All
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetPassOrgList_All()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_PASSORGLIST_ALL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassOrgList
        /// <summary>
        /// GetPassOrgList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetPassOrgList(string strPassType, string strAuthOper, string strUpperYn)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_PASSORGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 10);
                parameters[0].Value = strPassType;

                parameters[1] = new MySqlParameter("@pAUTHOPER", MySqlDbType.VarChar, 20);
                parameters[1].Value = strAuthOper;

                parameters[2] = new MySqlParameter("@pUPPERYN", MySqlDbType.VarChar, 1);
                parameters[2].Value = strUpperYn;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetToolOrgList
        /// <summary>
        /// GetToolOrgList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetToolOrgList(string strAuthOper, string strUpperYn)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_TOOLORGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strAuthOper;

                parameters[1] = new MySqlParameter("@pUPPERYN", MySqlDbType.VarChar, 1);
                parameters[1].Value = strUpperYn;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetToolAuthEmp
        /// <summary>
        /// GetToolAuthEmp
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetToolAuthEmp(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_TOOLAUTHINFOEMP_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetSafetyOrgList
        /// <summary>
        /// GetSafetyOrgList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetSafetyOrgList(string strAuthOper, string strUpperYn)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_SAFETYORGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strAuthOper;

                parameters[1] = new MySqlParameter("@pUPPERYN", MySqlDbType.VarChar, 1);
                parameters[1].Value = strUpperYn;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetSafetyAuthEmp
        /// <summary>
        /// GetSafetyAuthEmp
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetSafetyAuthEmp(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_SAFETYAUTHINFOEMP_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;


                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetPassOperNmList
        /// <summary>
        /// GetPassOperNmList
        /// </summary>
        /// <param name="strOperNm">string</param>
        /// <param name="strPType">string</param>
        /// <param name="strEmpNo">string</param>
        /// <param name="strPassType">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetPassOperNmList(string strOperNm, string strPType, string strEmpNo, string strPassType)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_PASSOPERNMLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pOPERNM", MySqlDbType.VarChar, 50);
                parameters[0].Value = strOperNm;

                parameters[1] = new MySqlParameter("@pPTYPE", MySqlDbType.VarChar, 5);
                parameters[1].Value = strPType;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pPASSTYPE", MySqlDbType.VarChar, 10);
                parameters[3].Value = strPassType;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetOperOrgMonthList
        /// <summary>
        /// GetOperOrgMonthList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetOperOrgMonthList(string strEmpNo, string strMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_BASICORGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pMDATE", MySqlDbType.VarChar, 6);
                parameters[1].Value = strMonth;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetLimitOrgMonthList
        /// <summary>
        /// GetLimitOrgMonthList
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strMonth">string</param>
        /// <param name="strLimitLv">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetLimitOrgMonthList(string strEmpNo, string strMonth, string strLimitLv)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];

                CommonData data = new CommonData("UP_EXTENDORGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pMDATE", MySqlDbType.VarChar, 6);
                parameters[1].Value = strMonth;

                parameters[2] = new MySqlParameter("@pLIMITLV", MySqlDbType.VarChar, 1);
                parameters[2].Value = strLimitLv;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
