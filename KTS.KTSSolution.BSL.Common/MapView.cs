﻿/*
 *  Copyright ⓒ 2022 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using KTS.KTSSolution.Common.DAL;
using System;
using System.Collections.Generic;
using System.Data;

namespace KTS.KTSSolution.BSL.Common
{
    public class MapView : MarshalByRefObject, IDisposable
    {

        #region Constructor

        public MapView()
        {
        }

        #endregion

        #region Method
        #region CallDbDataSet
        /// <summary>
        /// CallDbDataSet
        /// </summary>
        /// <param name="empno">사번</param>
        /// <param name="userIp">ip</param>
        /// <param name="rptCd">파일명</param>
        /// <param name="jobGb">작업구분</param>
        /// <param name="param">파라미터</param>
        /// <returns>DataSet, string, string</returns>
        public (DataSet, string, string) CallDbDataSet(string empno, string userIp, string rptCd, string jobGb, string[] param)
        {
            string errCd = "00"; //정상
            string errMsg = string.Empty;
            string query = string.Empty;
            string queryParam = string.Empty;
            string jobNm = string.Empty;
            string jobType = string.Empty;

            DataSet ds = null;

            List<string> mapView = new List<string>();
            mapView = CallDbMapView(empno, userIp, rptCd, jobGb, param);

            if(mapView.Count > 0)
            {
                errCd =  mapView[0];
                errMsg = mapView[1];
                jobNm = mapView[2];
                jobType = mapView[3];

                if (!errCd.Equals("01"))
                {
                    try
                    {
                        if (param.Length > 0) queryParam = GetParamString(param);

                        if (jobType.ToUpper().Equals("VIEW"))
                        {
                            // TODO: VIEW 호출 개발 시 수정 예정
                            //query = string.Format("CALL {0}{1}", jobNm, queryParam);
                        }
                        else //PROCEDURE
                        {
                            query = string.Format("CALL {0}{1}", jobNm, queryParam);
                        }

                        using (DSBase dsBase = new DSBase())
                        {
                            ds = dsBase.ExecInquiry(query);

                            if(ds.Tables[0].Columns.Contains("ERRCD"))
                            {
                                errCd = ds.Tables[0].Rows[0]["ERRCD"].ToString();
                            }
                            if (ds.Tables[0].Columns.Contains("ERRMSG"))
                            {
                                errMsg = ds.Tables[0].Rows[0]["ERRMSG"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        errCd = "01";
                        errMsg = ex.ToString();
                    }
                }
            }

            return (ds, errCd, errMsg);
        }
        #endregion

        #region CallDbRegister
        /// <summary>
        /// CallDbRegister
        /// </summary>
        /// <param name="empno">사번</param>
        /// <param name="userIp">ip</param>
        /// <param name="rptCd">파일명</param>
        /// <param name="jobGb">작업구분</param>
        /// <param name="param">파라미터</param>
        /// <returns>int, string, string</returns>
        public (int, string, string) CallDbRegister(string empno, string userIp, string rptCd, string jobGb, string[] param)
        {
            string errCd = "00"; //정상
            string errMsg = string.Empty;
            int iResult = 0;
            string query = string.Empty;
            string queryParam = string.Empty;
            string jobNm = string.Empty;

            List<string> mapView = new List<string>();
            mapView = CallDbMapView(empno, userIp, rptCd, jobGb, param);

            if (mapView.Count > 0)
            {
                errCd = mapView[0];
                errMsg = mapView[1];
                jobNm = mapView[2];

                if (!errCd.Equals("01"))
                {
                    try
                    {
                        if (param.Length > 0) queryParam = GetParamString(param);

                        query = string.Format("CALL {0}{1}", jobNm, queryParam);

                        using (DSBase dsBase = new DSBase())
                        {
                            iResult = dsBase.ExecRegister(query);
                        }
                    }
                    catch (Exception ex)
                    {
                        errCd = "01";
                        errMsg = ex.ToString();
                    }
                }
            }

            return (iResult, errCd, errMsg);
        }
        #endregion

        List<string> CallDbMapView(string empno, string userIp, string rptCd, string jobGb, string[] param)
        {
            List<string> result = new List<string>();

            string errCd = string.Empty;
            string errMsg = string.Empty;
            string jobNm = string.Empty;
            string jobType = string.Empty;
            int paramCnt = 0;

            DataSet dsMapView = GetMapView(empno, userIp, rptCd, jobGb, param);

            if (dsMapView != null)
            {
                if (dsMapView.Tables[0].Rows.Count > 0)
                {
                    try
                    {

                        errCd = dsMapView.Tables[0].Rows[0]["ERRCD"].ToString();
                        errMsg = dsMapView.Tables[0].Rows[0]["ERRMSG"].ToString();
                        jobNm = dsMapView.Tables[0].Rows[0]["JOBNM"].ToString();
                        jobType = dsMapView.Tables[0].Rows[0]["JOBTYPE"].ToString();
                        paramCnt = Convert.ToInt32(dsMapView.Tables[0].Rows[0]["PARAMCNT"].ToString());

                        if (!param.Length.Equals(paramCnt))
                        {
                            errCd = "01";
                            errMsg = "파라미터 개수가 일치하지 않습니다.";
                        }

                    }
                    catch (Exception ex)
                    {
                        errCd = "01";
                        errMsg = ex.ToString();
                    }

                    result.Add(errCd);
                    result.Add(errMsg);
                    result.Add(jobNm);
                    result.Add(jobType);
                }
            }

            return result;
        }

        #region GetMapView
        /// <summary>
        /// GetMapView
        /// </summary>
        /// <param name="empno">사번</param>
        /// <param name="userIp">ip</param>
        /// <param name="rptCd">파일명</param>
        /// <param name="jobGb">작업구분</param>
        /// <returns>dataset</returns>
        DataSet GetMapView(string empno, string userIp, string rptCd, string jobGb, string[] param)
        {
            string query = string.Empty;
            string queryParam = string.Empty;
            DataSet ds = null;

            try
            {
                if (param.Length > 0) queryParam = GetMapViewParamString(param);

                query = string.Format("CALL UP_MAPVIEW_SELECT('{0}', '{1}', '{2}', '{3}', '{4}')", empno, userIp, rptCd, jobGb, queryParam);

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.ExecInquiry(query);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetParamString
        /// <summary>
        /// GetParamString
        /// </summary>
        /// <param name="param">파라미터</param>
        /// <returns>string</returns>
        string GetParamString(string[] param)
        {
            string queryParam = string.Empty;

            queryParam = "(";
            for (int i = 0; i < param.Length; i++)
            {
                if (i > 0) queryParam += ",";

                queryParam += "'";
                queryParam += DSUtill.DbStringReplace(param[i]);
                queryParam += "'";
            }

            queryParam += ")";

            return queryParam;
        }
        #endregion

        #region GetMapViewParamString
        /// <summary>
        /// GetMapViewParamString
        /// </summary>
        /// <param name="param">파라미터</param>
        /// <returns>string</returns>
        string GetMapViewParamString(string[] param)
        {
            string queryParam = string.Empty;

            for (int i = 0; i < param.Length; i++)
            {
                if (i > 0) queryParam += "|";

                queryParam += '"';
                queryParam += DSUtill.DbStringReplace(param[i]);
                queryParam += '"';
            }

            return queryParam;
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
