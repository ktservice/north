﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Common
{
    public class KTSUser : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public KTSUser()
        {
        }

        #endregion

        #region Method


        #region GetAdminLogOn
        /// <summary>
        /// GetAdminLogOn
        /// </summary>
        /// <param name="strEmpno"></param>
        /// <param name="strEmpPwd"></param>
        /// <returns>DataSet</returns>
        public DataSet GetAdminLogOn(string strEmpno, string strEmpPwd)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpno;
                parameters[1] = new MySqlParameter("@pEMPPWD", MySqlDbType.VarChar, 256);
                parameters[1].Value = strEmpPwd;

                CommonData data = new CommonData("UP_LOGINADMIN_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion


        #region GetUserLogOn
        /// <summary>
        /// GetUserLogOn
        /// </summary>
        /// <param name="strUserId">string</param>
        /// <param name="strUserPw">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserLogOn(string strEmpno, string strEmpPwd)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpno;
                parameters[1] = new MySqlParameter("@pEMPPWD", MySqlDbType.VarChar, 256);
                parameters[1].Value = strEmpPwd;

                CommonData data = new CommonData("UP_LOGONUSER_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetUserLogInfo
        /// <summary>
        /// GetUserLogInfo
        /// </summary>
        /// <param name="strUserId">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserLogInfo(string strEmpno)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpno;

                CommonData data = new CommonData("UP_LOGINUSERINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetUserLogInfo
        /// <summary>
        /// GetUserLogInfo
        /// </summary>
        /// <param name="strUserId">string</param>
        /// <returns>DataSet</returns>
        public DataSet InsSSOUserLogInfo(string strUserId, string strEmpNo, string strSptid, string strUserName)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];
                parameters[0] = new MySqlParameter("@pUSERID", MySqlDbType.VarChar, 50);
                parameters[0].Value = strUserId;
                parameters[1] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 50);
                parameters[1].Value = strEmpNo;
                parameters[2] = new MySqlParameter("@pSPTID", MySqlDbType.VarChar, 150);
                parameters[2].Value = strSptid;
                parameters[3] = new MySqlParameter("@pUSERNM", MySqlDbType.VarChar, 50);
                parameters[3].Value = strUserName;

                CommonData data = new CommonData("UP_SSOLOGINUSERINFO_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsertLoginLog
        /// <summary>
        /// InsertLoginLog
        /// </summary>
        /// <param name="strUserId">string</param>
        /// <param name="strUserIP">string</param>
        /// <param name="strUserAgent">string</param>
        public void InsertLoginLog(string strUserId, string strUserIP, string strUserAgent)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[3];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strUserId;
                parameters[1] = new MySqlParameter("@pCONNIP", MySqlDbType.VarChar, 20);
                parameters[1].Value = strUserIP;
                parameters[2] = new MySqlParameter("@pUSERAGENT", MySqlDbType.VarChar, 1024);
                parameters[2].Value = strUserAgent;

                CommonData data = new CommonData("UP_USERCONNHIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetUserPageAuth
        /// <summary>
        /// GetUserPageAuth
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strUrl">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserPageAuth(string strEmpNo, string strUrl)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;
                parameters[1] = new MySqlParameter("@pURL", MySqlDbType.VarChar, 250);
                parameters[1].Value = strUrl;

                CommonData data = new CommonData("UP_USERPAGEAUTH_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        #endregion

        #region GetUserMenu
        /// <summary>
        /// GetUserMenu
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserMenu(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                CommonData data = new CommonData("UP_USERMENULIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        #endregion

        #region GetUserViewOrgMonth
        /// <summary>
        /// GetUserViewOrgMonth
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserViewOrgMonth(string strEmpNo, string strMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strMonth;

                CommonData data = new CommonData("UP_USERVIEWORGCD_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        #endregion

        #region GetUserViewOrgMonth2
        /// <summary>
        /// GetUserViewOrgMonth2
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <param name="strMonth">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetUserViewOrgMonth2(string strEmpNo, string strMonth)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];
                parameters[0] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[0].Value = strEmpNo;

                parameters[1] = new MySqlParameter("@pSUMUPMONTH", MySqlDbType.VarChar, 6);
                parameters[1].Value = strMonth;

                CommonData data = new CommonData("UP_USERVIEWORGCD2_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }

        #endregion

        #region GetDiligInfo
        /// <summary>
        /// GetDiligInfo
        /// </summary>
        /// <param name="strEmpNo">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetDiligInfo(string strEmpNo)
        {
            DataSet ds = null;

            try
            {
                System.Data.Odbc.OdbcParameter[] parameters = new System.Data.Odbc.OdbcParameter[1];

                parameters[0] = new System.Data.Odbc.OdbcParameter("@OC_EMP_NO", System.Data.Odbc.OdbcType.VarChar, 13);
                parameters[0].Value = strEmpNo;

                using (KTS.KTSSolution.Common.DAL.DBConn dbConn = new KTS.KTSSolution.Common.DAL.DBConn())
                {
                    ds = dbConn.ExecuteDataSetByMsSql(CommandType.Text, "{CALL [KTS_NORTH].[dbo].[USP_KTS_M_DILIG_KO806](?)}", parameters);
                }
            }
            catch
            {
                ds = null;
            }

            return ds;
        }

        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
