﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Oper
{
    public class MenuMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public MenuMgmt()
        {
        }

        #endregion

        #region Method

        #region GetAuthGroupMenu
        /// <summary>
        /// GetAuthGroupMenu
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetAuthGroupMenu()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_OPERAUTHGROUPMENU_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetMenuList
        /// <summary>
        /// GetMenuList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetMenuList()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_OPERMENULIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsertAuthGroup
        /// <summary>
        /// InsertAuthGroup
        /// </summary>
        public void InsertAuthGroup(string strAutId, string strAuthNm)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_OPERAUTHGROUPLIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pAUTHID", MySqlDbType.VarChar, 8);
                parameters[0].Value = strAutId;

                parameters[1] = new MySqlParameter("@pAUTHNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strAuthNm;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateAuthGroup
        /// <summary>
        /// UpdateAuthGroup
        /// </summary>
        public void UpdateAuthGroup(string strAuthId, string strAuthNm)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_OPERAUTHGROUPLIST_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pAUTHID", MySqlDbType.VarChar, 8);
                parameters[0].Value = strAuthId;

                parameters[1] = new MySqlParameter("@pAUTHNM", MySqlDbType.VarChar, 50);
                parameters[1].Value = strAuthNm;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdateAuthGroup
        /// <summary>
        /// UpdateAuthGroup
        /// </summary>
        public void UpdateAuthMenu(List<string> lstData)
        {
            try
            {
                List<CommonData> DataList = new List<CommonData>();

                foreach (string strData in lstData)
                {
                    string[] strValue = strData.Split('|');
                    int nLen = strValue.Length;

                    if (nLen < 3)
                        continue;

                    string strAuthId = strValue[0];
                    string strMenuId = strValue[1];
                    string strViewYn = strValue[2];

                    MySqlParameter[] parameters = new MySqlParameter[3];

                    parameters[0] = new MySqlParameter("@pAUTHID", MySqlDbType.VarChar, 8);
                    parameters[0].Value = strAuthId;

                    parameters[1] = new MySqlParameter("@pMENUID", MySqlDbType.VarChar, 8);
                    parameters[1].Value = strMenuId;

                    parameters[2] = new MySqlParameter("@pVIEWYN", MySqlDbType.VarChar, 1);
                    parameters[2].Value = strViewYn;

                    CommonData data = new CommonData("UP_OPERAUTHMENU_UPDATE", parameters);
                    data.commandType = CommandType.StoredProcedure;

                    DataList.Add(data);

                }

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.MultiRegister(DataList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
