﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Oper
{
    public class OrgMappingMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public OrgMappingMgmt()
        {
        }

        #endregion

        #region Method

        #region GetOrgMappingList
        /// <summary>
        /// GetOrgMappingList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetOrgMappingList()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_ORGMAPPINGLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetOrgMappingInfo
        /// <summary>
        /// SetOrgMappingInfo
        /// </summary>
        /// <param name="Nodes">Dictionary<string, string></param>
        public void SetOrgMappingInfo(Dictionary<string, string> Nodes)
        {
            List<CommonData> DataList = new List<CommonData>();

            foreach (string strKey in Nodes.Keys.ToList())
            {
                string[] strOrgValue = strKey.Split('/');
                int nLen = strOrgValue.Length;

                if (nLen < 2)
                    continue;

                string strOrgCd = strOrgValue[nLen - 1];
                string strUpperOrgCd = strOrgValue[nLen - 2];
                string strBeforeUpperOrgCd = "";

                if (strOrgCd.Split('|').Length > 1)
                {
                    strBeforeUpperOrgCd = strOrgCd.Split('|')[0];
                    strOrgCd = strOrgCd.Split('|')[1];
                }

                MySqlParameter[] parameters = new MySqlParameter[3];

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 6);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pUPPERORGCD", MySqlDbType.VarChar, 6);
                parameters[1].Value = strUpperOrgCd;

                parameters[2] = new MySqlParameter("@pBEFOREUPPERORGCD", MySqlDbType.VarChar, 6);
                parameters[2].Value = strBeforeUpperOrgCd;

                CommonData data = new CommonData("UP_ORGMAPPINGINFO_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                DataList.Add(data);
            }

            using (DSBase dsBase = new DSBase())
            {
                dsBase.MultiRegister(DataList);
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
