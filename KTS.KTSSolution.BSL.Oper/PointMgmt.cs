﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Oper
{
    public class PointMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public PointMgmt()
        {
        }

        #endregion

        #region Method

        #region GetYearsPointList
        /// <summary>
        /// GetYearsPointList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetYearsPointList()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_YEARSPOINTLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsYearsPointList
        /// <summary>
        /// InsYearsPointList
        /// </summary>
        /// <param name="strYears">string</param>
        /// <param name="strPoint">string</param>
        public void InsYearsPointList(string strYears)//, string strPoint)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_YEARSPOINTLIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                //parameters[1] = new MySqlParameter("@pPOINT", MySqlDbType.VarChar, 10);
                //parameters[1].Value = strPoint;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdYearsPointList
        /// <summary>
        /// UpdYearsPointList
        /// </summary>
        /// <param name="strYears">string</param>
        /// <param name="strPoint">string</param>
        public void UpdYearsPointList(string strYears, string strPoint)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_YEARSPOINTLIST_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINT", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPoint;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelYearsPointList
        /// <summary>
        /// DelYearsPointList
        /// </summary>
        /// <param name="strYears">string</param>
        public void DelYearsPointList(List<string> lstYears)
        {
            try
            {
                List<CommonData> DataList = new List<CommonData>();

                foreach (string strYears in lstYears)
                {
                    MySqlParameter[] parameters = new MySqlParameter[1];

                    CommonData data = new CommonData("UP_YEARSPOINTLIST_DELETE", parameters);
                    data.commandType = CommandType.StoredProcedure;

                    parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                    parameters[0].Value = strYears;

                    DataList.Add(data);
                }

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.MultiRegister(DataList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetProvidePointList
        /// <summary>
        /// GetProvidePointList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetProvidePointList()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_PROVIDEPOINTLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetProvidePointInfo
        /// <summary>
        /// GetProvidePointInfo
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetProvidePointInfo(string strYears)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PROVIDEPOINTINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsProvidePointList
        /// <summary>
        /// InsProvidePointList
        /// </summary>
        /// <param name="strTitle">string</param>
        /// <param name="strStDt">string</param>
        /// <param name="strEnDt">string</param>
        public void InsProvidePointList(string strYears, string strTitle, string strStDt, string strEnDt)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_PROVIDEPOINTLIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pTITLE", MySqlDbType.VarChar, 150);
                parameters[1].Value = strTitle;

                parameters[2] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 8);
                parameters[2].Value = strStDt;

                parameters[3] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 8);
                parameters[3].Value = strEnDt;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetRequestPointListCheck
        /// <summary>
        /// GetRequestPointListCheck
        /// </summary>
        /// <param name="strYears">string</param>
        /// <param name="strPointSeq">string</param>
        /// <returns>DataSet</returns>
        public DataSet GetRequestPointListCheck(string strYears, string strPointSeq)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_REQUESTPOINTLISTCHECK_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelProvidePointList
        /// <summary>
        /// DelProvidePointList
        /// </summary>
        /// <param name="strYears">string</param>
        public void DelProvidePointList(List<string> lstSeq)
        {
            try
            {
                List<CommonData> DataList = new List<CommonData>();

                foreach (string strSeq in lstSeq)
                {
                    string[] strSeqList = strSeq.Split('|');

                    if (strSeqList.Length > 1)
                    {
                        MySqlParameter[] parameters = new MySqlParameter[2];

                        CommonData data = new CommonData("UP_PROVIDEPOINTLIST_DELETE", parameters);
                        data.commandType = CommandType.StoredProcedure;

                        parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                        parameters[0].Value = strSeqList[0];

                        parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                        parameters[1].Value = strSeqList[1];


                        DataList.Add(data);
                    }
                }

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.MultiRegister(DataList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UpdProvidePointList
        /// <summary>
        /// UpdProvidePointList
        /// </summary>
        /// <param name="strYears"></param>
        /// <param name="strPointSeq"></param>
        /// <param name="strStdt"></param>
        /// <param name="strEndt"></param>
        public void UpdProvidePointList(string strYears, string strPointSeq, string strStdt, string strEndt)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_REQUESTPOINTLIST_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                parameters[2] = new MySqlParameter("@pSTDT", MySqlDbType.VarChar, 8);
                parameters[2].Value = strStdt;

                parameters[3] = new MySqlParameter("@pENDT", MySqlDbType.VarChar, 8);
                parameters[3].Value = strEndt;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetProvidePointList_Detail
        /// <summary>
        /// GetProvidePointList_Detail
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetProvidePointList_Detail(string strYears, string strPointSeq)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_PROVIDEPOINTLIST_DETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetProvidePointCheck
        /// <summary>
        /// GetProvidePointCheck
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetProvidePointCheck(string strYears, string strPointSeq)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_PROVIDEPOINTCHECK_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region InsProvidePoint
        /// <summary>
        /// InsProvidePoint
        /// </summary>
        /// <returns>DataSet</returns>
        public void InsProvidePoint(string strYears, string strPointSeq)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_PROVIDEPOINT_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetStandardInfo
        /// <summary>
        /// GetStandardInfo
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetStandardInfo(string strYears, string strPointSeq)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_STANDARDCLOTHLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetRequestStatsList
        /// <summary>
        /// GetRequestStatsList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetRequestStatsList(string strYears, string strPointSeq)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_REQUESTCLOTHLISTBYSTANDARD_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                parameters[1] = new MySqlParameter("@pPOINTSEQ", MySqlDbType.VarChar, 10);
                parameters[1].Value = strPointSeq;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetStandardProvideInfo
        /// <summary>
        /// GetStandardProvideInfo
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetStandardProvideInfo()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_STANDARDPROVIDEINFO_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetProvideInfoByYears
        /// <summary>
        /// GetProvideInfoByYears
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetProvideInfoByYears(string strYears)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PROVIDEINFOBYYEARS_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region DelProvideInfoByYears
        /// <summary>
        /// DelProvideInfoByYears
        /// </summary>
        public void DelProvideInfoByYears(string strYears)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[1];

                CommonData data = new CommonData("UP_PROVIDEINFOBYYEARS_DELETE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                parameters[0].Value = strYears;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region InsProvideInfoByYears
        /// <summary>
        /// InsProvideInfoByYears
        /// </summary>
        public void InsProvideInfoByYears(List<string> lstData)
        {
            try
            {
                List<CommonData> DataList = new List<CommonData>();

                foreach (string strData in lstData)
                {
                    string[] strDataList = strData.Split('|');

                    if (strDataList.Length > 5)
                    {
                        string strYears = strDataList[0];
                        string strJobNm = strDataList[1];
                        string strProvideType = strDataList[2];
                        string strClothGbn = strDataList[3];
                        string strClothType = strDataList[4];
                        string strProvideCnt = strDataList[5];

                        MySqlParameter[] parameters = new MySqlParameter[6];

                        CommonData data = new CommonData("UP_PROVIDEINFOBYYEARS_INSERT", parameters);
                        data.commandType = CommandType.StoredProcedure;

                        parameters[0] = new MySqlParameter("@pYEARS", MySqlDbType.VarChar, 4);
                        parameters[0].Value = strYears;

                        parameters[1] = new MySqlParameter("@pJOBNM", MySqlDbType.VarChar, 20);
                        parameters[1].Value = strJobNm;

                        parameters[2] = new MySqlParameter("@pPROVIDETYPE", MySqlDbType.VarChar, 20);
                        parameters[2].Value = strProvideType;

                        parameters[3] = new MySqlParameter("@pCLOTHGBN", MySqlDbType.VarChar, 20);
                        parameters[3].Value = strClothGbn;

                        parameters[4] = new MySqlParameter("@pCLOTHTYPE", MySqlDbType.VarChar, 20);
                        parameters[4].Value = strClothType;

                        parameters[5] = new MySqlParameter("@pPROVIDECNT", MySqlDbType.VarChar, 11);
                        parameters[5].Value = strProvideCnt;

                        DataList.Add(data);
                    }
                }

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.MultiRegister(DataList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
