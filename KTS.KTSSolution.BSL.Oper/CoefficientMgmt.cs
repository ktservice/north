﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Oper
{
    public class CoefficientMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public CoefficientMgmt()
        {
        }

        #endregion

        #region Method

        #region GetCoefficientList
        /// <summary>
        /// GetCoefficientList
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetCoefficientList()
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_COEFFICIENTLIST_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetCoefficient
        /// <summary>
        /// SetCoefficient
        /// </summary>
        /// <param name="strCoefficientDt">string</param>
        /// <param name="strMinValue">string</param>
        /// <param name="strMaxValue">string</param>
        /// <param name="strSumValue">string</param>
        public void SetCoefficient(string strCoefficientDt, string strMinValue, string strMaxValue, string strSumValue, string strDeadLine)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_COEFFICIENTLIST_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pCOEFDT", MySqlDbType.VarChar, 6);
                parameters[0].Value = strCoefficientDt;

                parameters[1] = new MySqlParameter("@pMIN", MySqlDbType.VarChar, 15);
                parameters[1].Value = strMinValue;

                parameters[2] = new MySqlParameter("@pMAX", MySqlDbType.VarChar, 15);
                parameters[2].Value = strMaxValue;

                parameters[3] = new MySqlParameter("@pSUM", MySqlDbType.VarChar, 15);
                parameters[3].Value = strSumValue;

                parameters[4] = new MySqlParameter("@pDEADLINE", MySqlDbType.VarChar, 8);
                parameters[4].Value = strDeadLine;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
