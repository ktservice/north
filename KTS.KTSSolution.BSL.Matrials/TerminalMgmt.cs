﻿/*
 *  Copyright ⓒ 2020 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */
using System;
using System.Data;
using MySql.Data.MySqlClient;
using KTS.KTSSolution.Framework.SharedType;
using KTS.KTSSolution.Common.DAL;

namespace KTS.KTSSolution.BSL.Matrials
{
    public class TerminalMgmt : MarshalByRefObject, IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public TerminalMgmt()
        {
        }

        #endregion

        #region Method

        #region GetTerminalStatByOrg
        /// <summary>
        /// GetTerminalStatByOrg
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetTerminalStatByOrg(string strOrgCd, string strBaseDate)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_TERMINALSTATBYORG_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pBASEDATE", MySqlDbType.VarChar, 10);
                parameters[1].Value = strBaseDate;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetTerminalStatByEmp
        /// <summary>
        /// GetTerminalStatByEmp
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetTerminalStatByEmp(string strOrgCd, string strBaseDate)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[2];

                CommonData data = new CommonData("UP_TERMINALSTATBYEMP_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pBASEDATE", MySqlDbType.VarChar, 10);
                parameters[1].Value = strBaseDate;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetTerminalInfoDetaill
        /// <summary>
        /// GetTerminalInfoDetaill
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetTerminalInfoDetaill(string strOrgCd, string strBaseDate, string strEmpNo, string strHoldingYn)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_TERMINALINFODETAIL_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pBASEDATE", MySqlDbType.VarChar, 10);
                parameters[1].Value = strBaseDate;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pHOLDINGYN", MySqlDbType.VarChar, 1);
                parameters[3].Value = strHoldingYn;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region GetTerminalSendMMSResult
        /// <summary>
        /// GetTerminalSendMMSResult
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetTerminalSendMMSResult(string strOrgCd, string strBaseDate, string strEmpNo, string strResult)
        {
            DataSet ds = null;

            try
            {
                MySqlParameter[] parameters = new MySqlParameter[4];

                CommonData data = new CommonData("UP_TERMINALSENDMMSRESULT_SELECT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pORGCD", MySqlDbType.VarChar, 8000);
                parameters[0].Value = strOrgCd;

                parameters[1] = new MySqlParameter("@pBASEDATE", MySqlDbType.VarChar, 10);
                parameters[1].Value = strBaseDate;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pRESULT", MySqlDbType.VarChar, 10);
                parameters[3].Value = strResult;

                using (DSBase dsBase = new DSBase())
                {
                    ds = dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ds;
        }
        #endregion

        #region SetTerminalIdmsUserInfo
        /// <summary>
        /// SetTerminalIdmsUserInfo
        /// </summary>
        /// <param name="strBaseDate"></param>
        /// <param name="strIdmsEmpNo"></param>
        /// <param name="strEmpNo"></param>
        /// <param name="strEmpNm"></param>
        /// <param name="strMobile"></param>
        public void SetTerminalIdmsUserInfo(string strBaseDate, string strIdmsEmpNo, string strEmpNo, string strEmpNm, string strMobile)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[5];

                CommonData data = new CommonData("UP_TERMINALIDMSUSERINFO_UPDATE", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pBASEDATE", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBaseDate;

                parameters[1] = new MySqlParameter("@pIDMSEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strIdmsEmpNo;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[3].Value = strEmpNm;

                parameters[4] = new MySqlParameter("@pMOBILE", MySqlDbType.VarChar, 20);
                parameters[4].Value = strMobile;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetTerminalInfoSendMMS
        /// <summary>
        /// SetTerminalInfoSendMMS
        /// </summary>
        /// <param name="strBaseDate"></param>
        /// <param name="strIdmsEmpNo"></param>
        /// <param name="strEmpNo"></param>
        /// <param name="strEmpNm"></param>
        /// <param name="strMobile"></param>
        /// <param name="strSmsMsg"></param>
        public void SetTerminalInfoSendMMS(string strBaseDate, string strIdmsEmpNo, string strEmpNo, string strEmpNm, string strMobile, string strSmsMsg)
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[6];

                CommonData data = new CommonData("UP_TERMINALMMSSEND_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                parameters[0] = new MySqlParameter("@pBASEDATE", MySqlDbType.VarChar, 10);
                parameters[0].Value = strBaseDate;

                parameters[1] = new MySqlParameter("@pIDMSEMPNO", MySqlDbType.VarChar, 20);
                parameters[1].Value = strIdmsEmpNo;

                parameters[2] = new MySqlParameter("@pEMPNO", MySqlDbType.VarChar, 20);
                parameters[2].Value = strEmpNo;

                parameters[3] = new MySqlParameter("@pEMPNM", MySqlDbType.VarChar, 50);
                parameters[3].Value = strEmpNm;

                parameters[4] = new MySqlParameter("@pMOBILE", MySqlDbType.VarChar, 20);
                parameters[4].Value = strMobile;

                parameters[5] = new MySqlParameter("@pMMSMSG", MySqlDbType.VarChar, 200);
                parameters[5].Value = strSmsMsg;


                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SetTerminalInfoReSendMMS
        /// <summary>
        /// SetTerminalInfoReSendMMS
        /// </summary>
        public void SetTerminalInfoReSendMMS()
        {
            try
            {
                MySqlParameter[] parameters = new MySqlParameter[0];

                CommonData data = new CommonData("UP_TERMINALRESENDMMS_INSERT", parameters);
                data.commandType = CommandType.StoredProcedure;

                using (DSBase dsBase = new DSBase())
                {
                    dsBase.Inquiry(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion

    }
}
