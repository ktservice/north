﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace KTS.KTSSolution.SVC.ExcelCreate
{
    static partial class Program
    {
        /// <summary>
        /// 해당 애플리케이션의 주 진입점입니다.
        /// </summary>
        static void Main()
        {
//#if DEBUG
            //var service = new ExcelCreate();

            //service.Start();
//#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ExcelCreate()
            };
            ServiceBase.Run(ServicesToRun);
//#endif
        }
    }
}
