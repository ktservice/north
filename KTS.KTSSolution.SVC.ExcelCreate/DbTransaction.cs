﻿/*
 *  Copyright ⓒ 2022 kt corp. All right reserved.
 *  
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or 
 *  intended publication of such software.
 */

using KTS.KTSSolution.BSL.Common;
using KTS.KTSSolution.Framework.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace KTS.KTSSolution.SVC.ExcelCreate
{
    public class DbTransaction
    {
        string rptCd;
        string jobGb;
        string userIp;
        string empno;

        List<string> param = new List<string>();

        public DbTransaction(string rptCd, string jobGb, List<string> aData)
        {
            this.rptCd = rptCd;
            this.jobGb = jobGb;
            param = aData;
        }

        public string GetJsonData()
        {
            string result = string.Empty;
            string errCd = string.Empty;
            string errMsg = string.Empty;

            DataSet ds = new DataSet();
            (ds, errCd, errMsg) = GetTableRow();

            if (errCd.Equals("01"))
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError(errCd, errMsg, "");
                result = Utility.DataTableToJson(dt);
            }
            else
            {
                if (ds != null)
                {
                    result = Utility.DataTableToJson(ds.Tables[0]);
                    ds.Dispose();
                }
            }

            return result;
        }

        public DataSet GetDataSet()
        {
            string errCd = string.Empty;
            string errMsg = string.Empty;

            DataSet ds = new DataSet();
            (ds, errCd, errMsg) = GetTableRow();

            if (errCd.Equals("01"))
            {
                DataTable dt = new DataTable();
                dt = Utility.DataTableError(errCd, errMsg, "");
                ds.Clear();
                ds.Tables.Add(dt);
            }

            return ds;
        }

        public (DataSet, string, string) GetTableRow()
        {
            string errCd = string.Empty;
            string errMsg = string.Empty;
            rptCd = getRptCd(rptCd);

            DataSet ds = new DataSet();

            try
            {
                using (MapView mapView = new MapView())
                {
                    (ds, errCd, errMsg) = mapView.CallDbDataSet(empno, userIp, rptCd, jobGb, param.ToArray());
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
            }

            return (ds, errCd, errMsg);
        }

        (int, string, string) SetRegister()
        {
            string errCd = string.Empty;
            string errMsg = string.Empty;
            int iResult = 0;
            rptCd = getRptCd(rptCd);

            try
            {
                using (MapView mapView = new MapView())
                {
                    (iResult, errCd, errMsg) = mapView.CallDbRegister(empno, userIp, rptCd, jobGb, param.ToArray());
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
            }

            return (iResult, errCd, errMsg);
        }


        string getRptCd(string value)
        {
            string rtn = value;

            if (rtn.IndexOf(".") >= 0)
            {
                string[] temp = value.Split('.');
                rtn = temp[0];
            }

            return rtn;
        }

    }
}