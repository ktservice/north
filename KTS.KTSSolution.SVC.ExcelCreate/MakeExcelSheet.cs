﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using KTS.KTSSolution.Framework.Util;

namespace KTS.KTSSolution.SVC.ExcelCreate
{
    public class MakeExcelSheet : IDisposable
    {
        #region fields

        #endregion

        #region Constructor

        public MakeExcelSheet()
        {
        }

        #endregion

        #region Method

        #region XlsxCreate
        public bool XlsxCreate()
        {
            bool bResult = false;
            TimeStamp timestamp = new TimeStamp();
            DataSet ds = null;
            DataSet dsList = null;

            string strYear = "";

            string strDir = ConfigurationSettings.AppSettings["XlsxCreatePath"];
            string strZipDir = ConfigurationSettings.AppSettings["ZipPath"];
            
            DirectoryInfo dirinfo = new DirectoryInfo(strDir);
            DirectoryInfo dirinfo2 = new DirectoryInfo(strZipDir);

            if (!dirinfo.Exists)
            {
                dirinfo.Create();
            }

            if (!dirinfo2.Exists)
            {
                dirinfo2.Create();
            }

            try
            {
                timestamp.TimeStampStart();

                string result = string.Empty;
                string rptCd = "CARLOGTEXSTATUS";
                string jobGb = "GET";

                List<string> aData = new List<string>();

                aData.Add("");
                aData.Add("F");

                DbTransaction dbTransaction = new DbTransaction(rptCd, jobGb, aData);
                ds = dbTransaction.GetDataSet();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    RegTexExcelStatus(strYear, "S", "국세청제출용 엑셀파일을 작성 중 입니다.");
                }

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    strYear = dr["REQYEAR"].ToString();
                    string strStatus = dr["CREATESTATUS"].ToString();

                    if (strStatus.Equals("D"))
                    {
                        DelTexRequest(strYear);
                        DelFileList(dirinfo, dirinfo2, strYear);
                    }
                    else if (!strStatus.Equals("E"))
                    {
                        DelFileList(dirinfo, dirinfo2, strYear);

                        string result1 = string.Empty;
                        string rptCd1 = "CARIDLIST";
                        string jobGb1 = "GET";

                        List<string> aData1 = new List<string>();

                        aData1.Add(strYear);
                        aData1.Add("F");

                        DbTransaction dbTransaction1 = new DbTransaction(rptCd1, jobGb1, aData1);

                        dsList = dbTransaction1.GetDataSet();

                        int nTotCnt = dsList.Tables[0].Rows.Count;
                        int nFinCnt = 0;

                        foreach (DataRow drList in dsList.Tables[0].Rows)
                        {
                            string strCarId = drList["CARID"].ToString();
                            string strCarNo = drList["CARNO"].ToString();
                            string strModel = drList["MODELNM"].ToString();

                            DataSet dsTexInfo = null;

                            string result2 = string.Empty;
                            string rptCd2 = "CARLOGTEXINFOLIST";
                            string jobGb2 = "GET";

                            List<string> aData2 = new List<string>();

                            aData2.Add(strYear);
                            aData2.Add(strCarId);
                            aData2.Add("F");

                            DbTransaction dbTransaction2 = new DbTransaction(rptCd2, jobGb2, aData2);

                            dsTexInfo = dbTransaction2.GetDataSet();

                            if (dsTexInfo.Tables[0].Rows.Count > 0)
                            {
                                using (ExcelPackage package = new ExcelPackage())
                                {
                                    // 저장파일명
                                    string xlsxfile = string.Format("업무차량 운행기록부({0}년 {1}).xlsx", strYear, strCarNo);

                                    // 기존파일있을경우 삭제
                                    FileInfo excelFile = new FileInfo(dirinfo + "\\" + xlsxfile);

                                    if (excelFile.Exists)
                                    {
                                        excelFile.Delete();
                                    }

                                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("sheet1");

                                    /* 셀 크기 조절 */

                                    for (int col = 1; col <= 14; col++)
                                    {
                                        double dWidth = 1.43;

                                        if (col == 1)
                                        {
                                            worksheet.Column(col).Width = dWidth;
                                        }
                                        else if (col < 6)
                                        {
                                            dWidth = 11.14;
                                            worksheet.Column(col).Width = dWidth;
                                        }
                                        else if (col == 14)
                                        {
                                            dWidth = 30.86;
                                            worksheet.Column(col).Width = dWidth;
                                        }
                                        else
                                        {
                                            dWidth = 12.71;
                                            worksheet.Column(col).Width = dWidth;
                                        }

                                        worksheet.Column(col).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        worksheet.Column(col).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                        worksheet.Column(col).Style.Font.Name = "맑은 고딕";
                                        worksheet.Column(col).Style.Font.Size = 11;
                                    }

                                    for (int row = 1; row <= 379; row++)
                                    {
                                        double dHeight = 16.50;

                                        if (row == 2 || row == 3)
                                        {
                                            dHeight = 24.75;
                                            worksheet.Row(row).Height = dHeight;
                                        }
                                        else if (row == 6 || row == 7)
                                        {
                                            dHeight = 30.00;
                                            worksheet.Row(row).Height = dHeight;
                                        }
                                        else if (row == 12)
                                        {
                                            dHeight = 49.50;
                                            worksheet.Row(row).Height = dHeight;
                                        }
                                        else
                                        {
                                            worksheet.Row(row).Height = dHeight;
                                        }
                                    }

                                    /* TOP 양식 세팅*/
                                    string strMergeCell1 = worksheet.Cells[1, 2].Address;
                                    string strMergeCell2 = worksheet.Cells[1, 5].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Font.Bold = true;
                                    worksheet.Cells[strMergeCell1].Value = "[별지 제8호 서식]";

                                    strMergeCell1 = worksheet.Cells[1, 13].Address;
                                    strMergeCell2 = worksheet.Cells[1, 14].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Font.Bold = true;
                                    worksheet.Cells[strMergeCell1].Value = "[전산관리용/국세청제출용]";

                                    strMergeCell1 = worksheet.Cells[2, 2].Address;
                                    strMergeCell2 = worksheet.Cells[3, 2].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Style.WrapText = true;
                                    worksheet.Cells[strMergeCell1].Value = "사업" + Environment.NewLine + "연도";

                                    strMergeCell1 = worksheet.Cells[2, 4].Address;
                                    strMergeCell2 = worksheet.Cells[3, 5].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Style.WrapText = true;
                                    worksheet.Cells[strMergeCell1].Value = strYear + ".01.01" + Environment.NewLine + "~" + Environment.NewLine + strYear + ".12.31";

                                    strMergeCell1 = worksheet.Cells[2, 6].Address;
                                    strMergeCell2 = worksheet.Cells[3, 11].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Font.Bold = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Font.Size = 20;
                                    worksheet.Cells[strMergeCell1].Value = "업무용차량 운행기록부(" + strYear + "년)";

                                    strMergeCell1 = worksheet.Cells[2, 12].Address;
                                    strMergeCell2 = worksheet.Cells[2, 13].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Value = "법인명";

                                    strMergeCell1 = worksheet.Cells[3, 12].Address;
                                    strMergeCell2 = worksheet.Cells[3, 13].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Value = "사업자등록번호";

                                    worksheet.Cells[2, 14].Value = "주식회사 케이티 서비스 북부";
                                    worksheet.Cells[3, 14].Value = "201-86-04729";

                                    strMergeCell1 = worksheet.Cells[2, 2].Address;
                                    strMergeCell2 = worksheet.Cells[3, 14].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));

                                    strMergeCell1 = worksheet.Cells[5, 2].Address;
                                    strMergeCell2 = worksheet.Cells[5, 4].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[5, 2].Value = "1. 대상차량정보";

                                    strMergeCell1 = worksheet.Cells[6, 2].Address;
                                    strMergeCell2 = worksheet.Cells[6, 4].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[6, 2].Value = "차종";
                                    worksheet.Cells[6, 5].Value = "차량번호";

                                    strMergeCell2 = worksheet.Cells[6, 5].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(242, 242, 242));

                                    strMergeCell1 = worksheet.Cells[7, 2].Address;
                                    strMergeCell2 = worksheet.Cells[7, 4].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[7, 2].Value = strModel;
                                    worksheet.Cells[7, 5].Value = strCarNo;

                                    strMergeCell2 = worksheet.Cells[7, 5].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                    strMergeCell1 = worksheet.Cells[9, 2].Address;
                                    strMergeCell2 = worksheet.Cells[9, 4].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[9, 2].Value = "2. 운행 내역";

                                    strMergeCell1 = worksheet.Cells[10, 2].Address;
                                    strMergeCell2 = worksheet.Cells[12, 3].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Style.WrapText = true;
                                    worksheet.Cells[strMergeCell1].Value = "운행" + Environment.NewLine + "일자" + Environment.NewLine + "(월/일)";

                                    strMergeCell1 = worksheet.Cells[10, 4].Address;
                                    strMergeCell2 = worksheet.Cells[11, 5].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Value = "운행자";

                                    worksheet.Cells[12, 4].Style.WrapText = true;
                                    worksheet.Cells[12, 4].Value = "지점" + Environment.NewLine + "(부서)";
                                    worksheet.Cells[12, 5].Value = "성명";

                                    strMergeCell1 = worksheet.Cells[10, 6].Address;
                                    strMergeCell2 = worksheet.Cells[10, 13].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[10, 6].Value = "운행 내역";

                                    strMergeCell1 = worksheet.Cells[11, 6].Address;
                                    strMergeCell2 = worksheet.Cells[11, 8].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[11, 6].Value = "업무용";

                                    strMergeCell1 = worksheet.Cells[11, 9].Address;
                                    strMergeCell2 = worksheet.Cells[11, 11].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[11, 9].Value = "현장퇴근제용";

                                    worksheet.Cells[12, 6].Style.WrapText = true;
                                    worksheet.Cells[12, 7].Style.WrapText = true;
                                    worksheet.Cells[12, 8].Style.WrapText = true;
                                    worksheet.Cells[12, 9].Style.WrapText = true;
                                    worksheet.Cells[12, 10].Style.WrapText = true;
                                    worksheet.Cells[12, 11].Style.WrapText = true;
                                    worksheet.Cells[12, 6].Value = "출발전" + Environment.NewLine + "계기판" + Environment.NewLine + "(사무실)";
                                    worksheet.Cells[12, 7].Value = "복귀시" + Environment.NewLine + "계기판" + Environment.NewLine + "(사무실)";
                                    worksheet.Cells[12, 8].Value = "①" + Environment.NewLine + "주행거리" + Environment.NewLine + "[km]";
                                    worksheet.Cells[12, 9].Value = "출발전" + Environment.NewLine + "계기판" + Environment.NewLine + "(업무종료지)";
                                    worksheet.Cells[12, 10].Value = "도착시" + Environment.NewLine + "계기판" + Environment.NewLine + "(사무실)";
                                    worksheet.Cells[12, 11].Value = "②" + Environment.NewLine + "주행거리" + Environment.NewLine + "[km]";

                                    strMergeCell1 = worksheet.Cells[11, 12].Address;
                                    strMergeCell2 = worksheet.Cells[12, 12].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Style.WrapText = true;
                                    worksheet.Cells[strMergeCell1].Value = "총" + Environment.NewLine + "주행거리" + Environment.NewLine + "(①+②)" + Environment.NewLine + "[km]";

                                    strMergeCell1 = worksheet.Cells[11, 13].Address;
                                    strMergeCell2 = worksheet.Cells[12, 13].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Style.WrapText = true;
                                    worksheet.Cells[strMergeCell1].Value = "주유량" + Environment.NewLine + "[ℓ]";

                                    strMergeCell1 = worksheet.Cells[10, 14].Address;
                                    strMergeCell2 = worksheet.Cells[12, 14].Address;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1].Style.WrapText = true;
                                    worksheet.Cells[strMergeCell1].Value = "비고" + Environment.NewLine + "(복무/수리/세차/청소 등)";

                                    strMergeCell1 = worksheet.Cells[10, 2].Address;
                                    strMergeCell2 = worksheet.Cells[12, 14].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(242, 242, 242));

                                    int rows = 13;
                                    int nTotalKm = 0;

                                    foreach (DataRow drTexInfo in dsTexInfo.Tables[0].Rows)
                                    {
                                        int nDistance = 0;

                                        worksheet.Cells[rows, 2].Value = drTexInfo["DATE"].ToString();
                                        worksheet.Cells[rows, 3].Value = drTexInfo["KOR_WEEK"].ToString();

                                        if (drTexInfo["KOR_WEEK"].ToString().Equals("토"))
                                        {
                                            worksheet.Cells[rows, 2].Style.Font.Color.SetColor(Color.Blue);
                                        }
                                        else if (drTexInfo["KOR_WEEK"].ToString().Equals("일"))
                                        {
                                            worksheet.Cells[rows, 2].Style.Font.Color.SetColor(Color.Red);
                                        }
                                        worksheet.Cells[rows, 4].Value = drTexInfo["ORGNM"].ToString();
                                        worksheet.Cells[rows, 5].Value = drTexInfo["EMPNM"].ToString();
                                        worksheet.Cells[rows, 6].Value = int.Parse(drTexInfo["STKM"].ToString());
                                        worksheet.Cells[rows, 7].Value = int.Parse(drTexInfo["RESULTKM"].ToString());
                                        worksheet.Cells[rows, 8].Value = int.Parse(drTexInfo["WORKDISTANCE"].ToString());
                                        worksheet.Cells[rows, 9].Value = drTexInfo["LEAVESTKM"].ToString();
                                        worksheet.Cells[rows, 10].Value = drTexInfo["LEAVEKM"].ToString();
                                        worksheet.Cells[rows, 11].Value = drTexInfo["LEAVEDISTANCE"].ToString();
                                        worksheet.Cells[rows, 12].Value = int.Parse(drTexInfo["TOTALDISTANCE"].ToString());
                                        worksheet.Cells[rows, 13].Value = int.Parse(drTexInfo["FUELAMOUNT"].ToString());
                                        worksheet.Cells[rows, 14].Value = drTexInfo["NOTI"].ToString();

                                        int.TryParse(drTexInfo["TOTALDISTANCE"].ToString(), out nDistance);

                                        rows++;
                                        nTotalKm += nDistance;
                                    }

                                    strMergeCell1 = worksheet.Cells[13, 12].Address;
                                    strMergeCell2 = worksheet.Cells[378, 13].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(226, 239, 218));

                                    strMergeCell1 = worksheet.Cells[379, 2].Address;
                                    strMergeCell2 = worksheet.Cells[380, 5].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(242, 242, 242));

                                    strMergeCell1 = worksheet.Cells[379, 6].Address;
                                    strMergeCell2 = worksheet.Cells[379, 9].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[379, 6].Value = "⑪사업연도 총주행거리(㎞)";

                                    strMergeCell1 = worksheet.Cells[380, 6].Address;
                                    strMergeCell2 = worksheet.Cells[380, 9].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[380, 6].Value = nTotalKm;

                                    strMergeCell1 = worksheet.Cells[379, 10].Address;
                                    strMergeCell2 = worksheet.Cells[379, 13].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[379, 10].Value = "⑫사업연도 업무용 사용거리(㎞)";

                                    strMergeCell1 = worksheet.Cells[380, 10].Address;
                                    strMergeCell2 = worksheet.Cells[380, 13].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Merge = true;
                                    worksheet.Cells[380, 10].Value = nTotalKm;

                                    worksheet.Cells[379, 14].Value = "⑬업무사용비율(⑫/⑪)";
                                    worksheet.Cells[380, 14].Value = "100.0%";

                                    strMergeCell1 = worksheet.Cells[379, 6].Address;
                                    strMergeCell2 = worksheet.Cells[380, 14].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Fill.BackgroundColor.SetColor(Color.Yellow);

                                    strMergeCell1 = worksheet.Cells[10, 2].Address;
                                    strMergeCell2 = worksheet.Cells[380, 14].Address;

                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    worksheet.Cells[strMergeCell1 + ":" + strMergeCell2].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                    package.SaveAs(excelFile);

                                    nFinCnt++;

                                    RegTexExcelStatus(strYear, "S", "국세청제출용 엑셀파일을 작성 중 입니다.(" + nFinCnt.ToString() + "/" + nTotCnt.ToString() + ")");
                                }
                            }
                        }

                        // 기존파일있을경우 삭제
                        string strZipFIleNm = strYear + "년도 국세청제출용 자료.zip";
                        string zipPath = System.IO.Path.Combine(strZipDir, strZipFIleNm);

                        if (File.Exists(zipPath))
                        {
                            File.Delete(zipPath);
                        }

                        ZipFile.CreateFromDirectory(strDir, zipPath);

                        DelFileList(dirinfo, null, strYear);


                        RegTexExcelStatus(strYear, "E", "국세청제출용 엑셀파일 작성이 완료되었습니다.(" + nFinCnt.ToString() + "/" + nTotCnt.ToString() + ")");
                    }
                }

                bResult =true;
            }
            catch (Exception ex)
            {
                bResult = false;
                timestamp = new TimeStamp();
                timestamp.TimeStampStart();
                timestamp.TimeStampEnd(this, "=====오류 발생 >> " + ex.ToString());

                DelFileList(dirinfo, dirinfo2, strYear);

                RegTexExcelStatus(strYear, "F", "국세청제출용 엑셀파일 작성 중 오류가 발생했습니다.");
                //string strFailMsg = "시스템오류 : " + ex.Message.ToString();
                //InsExcelCreateHist("엑셀 양식 제작 중 시스템 오류", "N", "");
            }

            return bResult;
        }
        #endregion

        #region
        private void DelFileList(DirectoryInfo dirinfo, DirectoryInfo dirinfo2, string strYear)
        {
            FileInfo[] files = dirinfo.GetFiles();

            for (int i = 0; i < files.Length; i++)
            {
                string strFileNm = files[i].Name;
                string strExt = files[i].Extension;

                if (strFileNm.Contains(strYear))
                {
                    files[i].Delete();
                }
            }

            if (dirinfo2 != null)
            {
                FileInfo[] zipfiles = dirinfo2.GetFiles();

                for (int i = 0; i < zipfiles.Length; i++)
                {
                    string strFileNm = zipfiles[i].Name;

                    if (strFileNm.Contains(strYear))
                    {
                        zipfiles[i].Delete();
                    }
                }
            }
        }
        #endregion
        #region RegTexExcelStatus
        /// <summary>
        /// RegTexExcelStatus
        /// </summary>
        /// <param name="strYears"></param>
        /// <param name="strStatus"></param>
        /// <param name="strMessage"></param>
        private void RegTexExcelStatus(string strYears, string strStatus, string strMessage)
        {
            try
            {
                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "CARLOGTEXSTATUSUPDATE";
                string jobGb = "SET";

                List<string> aData = new List<string>();

                aData.Add(strYears);
                aData.Add(strStatus);
                aData.Add(strMessage);

                DbTransaction dbTransaction = new DbTransaction(rptCd, jobGb, aData);

                result = dbTransaction.GetJsonData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DelTexRequest
        /// <summary>
        /// DelTexRequest
        /// </summary>
        /// <param name="strYears"></param>
        private void DelTexRequest(string strYears)
        {
            try
            {
                string result = string.Empty;
                string resultSession = string.Empty;
                string rptCd = "CARLOGTEXSTATUSDELETE";
                string jobGb = "SET";

                List<string> aData = new List<string>();

                aData.Add(strYears);

                DbTransaction dbTransaction = new DbTransaction(rptCd, jobGb, aData);

                result = dbTransaction.GetJsonData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable 멤버

        #region IDisposable(bool disposing)
        /// <summary>
        /// IDisposable(bool disposing)
        /// </summary>
        /// <param name="disposing">bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // TODO: 관리형 상태(관리형 개체)를 삭제합니다.
            }
        }
        #endregion

        // // TODO: 비관리형 리소스를 해제하는 코드가 'Dispose(bool disposing)'에 포함된 경우에만 종료자를 재정의합니다.
        // ~ConnDB()
        // {
        //     // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
        //     Dispose(disposing: false);
        // }

        #region Dispose()
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 'Dispose(bool disposing)' 메서드에 정리 코드를 입력합니다.
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #endregion
    }
}
